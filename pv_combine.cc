/************************************************************************/
/*                                                                      */
/*          lux - a viewer for panoramic images                         */
/*                                                                      */
/*          Copyright 2016 - 2023 by Kay F. Jahnke                      */
/*                                                                      */
/*  The git repository for this software is at                          */
/*                                                                      */
/*  https://bitbucket.org/kfj/pv                                        */
/*                                                                      */
/*  Please direct questions, bug reports, and contributions to          */
/*                                                                      */
/*  kfjahnke+pv@gmail.com                                               */
/*                                                                      */
/*  pv is free software: you can redistribute it and/or modify          */
/*  it under the terms of the GNU General Public License as published   */
/*  by the Free Software Foundation, either version 3 of the License,   */
/*  or (at your option) any later version.                              */
/*                                                                      */
/*  pv is distributed in the hope that it will be useful,               */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       */
/*  GNU General Public License for more details.                        */
/*                                                                      */
/*  You should have received a copy of the GNU General Public License   */
/*  along with pv.  If not, see <http://www.gnu.org/licenses/>.         */
/*                                                                      */
/************************************************************************/

/*! \file pv_combine.cc

    \brief lux - a panorama viewer

    For documentation, please see the bitbucket repository at

    https://bitbucket.org/kfj/pv

    This file has the code specific to exposure fusion and image
    stitching. It starts out with a few collateral functors and
    functions. Next is an implementation of a variant of the
    Burt and Adelson image splining algorithm, which is used for
    both stitching and exposure fusion. This variant uses b-spline-
    based pyramids with variable scaling step, the pyramid levels
    are represented by b-splines and the low-pass is a simple
    binomial kernel. The spline-based pyramid is well-suited for
    the job; it makes the downscaling easier because the sample
    points for an 'upper' level needn't be on-grid with a 'lower'
    level. The pyramid code is in pv_rendering_common.cc.
    The fusion routine processes the partial images one at a time,
    accepting a partial and it's associated mask and performing the
    collapse of the weighted laplacian pyramid for each partial,
    optionally 'layering' the result onto previous iterations.
    Especially if the masks at hand are already normalized, this
    enables the program to layer on image after image without
    having to first create all images/masks, normalize and then
    proceed to combine them.
    After the implementation of the pyramid-based algorithm, we
    have the functions which are called from the non-rendering side,
    passing in elaborate parameter sets to steer the process.
    Another modification I use in the fusion and stitching process is
    to 'mount' the partial images. pv can generate 'live' stitches and
    HDR-blends, which I use as 'baseline images', onto which I 'paste'
    or 'mount' the partial images before feeding them into the fusion
    code. This reduces bleed-out and low-frequency artifacts, and it
    makes processing of image data without an alpha channel less
    problematic, even if such images have 'black area' where there is
    no image content.
    Brightness, white balance, black and white point of the 'mother'
    job are honoured as best as the software can, and output can be
    both linear RGB and sRGB, even though the actual fusion is always
    calculated in sRGB.
    For exposure fusions (and faux brackets) there's mask generation
    code in this file as well, it's part of the 'outer' routine called
    from the UI side. So far there's well-exposedness and local contrast
    (implemented via taking the squared gradient magnitude).
*/

#include "pv_rendering_common.h"
#include "vspline/general_filter.h"
#include <stdint.h>

// #define EXPORT_BLENDING_COMPONENTS

// in pv_no_rendering, TODO maybe move here

extern void store_rendered_image ( const job_type * p_job ,
                                   view_type * p_frgb ,
                                   view4_type * p_frgba ,
                                   bool frame_is_linear ,
                                   const std::string & filename ) ;

extern void cleanup ( job_type * p_job ) ;

namespace PV_ARCH
{

// helper class to 'yield' values from a MultiArrayView. This seems strange
// at first sight - shouldn't this be a simple indexing operation? It is,
// but only for scalar access. For SIMD access, we have to perform a gather
// operation. vspline has functors for the purpose, so we merely specialize
// them to the precise purpose we need here: We provide overloads for both
// discrete and real 2D coordinates, which may both occur when the functor
// is combined with other functors.

struct yield_t
: public vspline::yield_type < int2_type , pixel_type , VSIZE >
{
  typedef vspline::yield_type < int2_type , pixel_type , VSIZE > base_t ;

  using base_t::base_t ;
} ;

struct yield4_t
: public vspline::yield_type < int2_type , pixel4_type , VSIZE >
{
  typedef vspline::yield_type < int2_type , pixel4_type , VSIZE > base_t ;

  using base_t::base_t ;
} ;

struct near_t
: public vspline::yield_type < coordinate_type , pixel_type , VSIZE >
{
  typedef vspline::yield_type < coordinate_type , pixel_type , VSIZE > base_t ;

  using base_t::base_t ;
} ;

struct near4_t
: public vspline::yield_type < coordinate_type , pixel4_type , VSIZE >
{
  typedef vspline::yield_type < coordinate_type , pixel4_type , VSIZE > base_t ;

  using base_t::base_t ;
} ;

// shorthand typedefs for 2D float arrays used for masks:

typedef vigra::MultiArray < 2 , float > float_array_type ;
typedef vigra::MultiArrayView < 2 , float > float_view_type ;

// gradient_magnitude_t produces the squared gradient magnitude.
// This is a good measure for local contrast. When used together
// with exposure_weight, the magnitude may be too small to make
// a visual difference - in that case raise the contrast weight
// to several (like 10) times the exposure weight. Most of the
// time, though, this weight will be used by itself.

struct gradient_magnitude_t
: vspline::unary_functor
    < vigra::TinyVector < int , 2 > , float , VSIZE >
{
  // if the incoming spline has a degree of less than two, we
  // build the evaluator with shift, to get useful values.

  const iev_type ev0 ;
  const iev_type ev1 ;
  
  gradient_magnitude_t ( const spline_type & bspl )
  : ev0 ( make_iev ( bspl ,
                    { 1 , 0 } ,
                      ( bspl.spline_degree < 2 )
                    ? ( 2 - bspl.spline_degree )
                    : 0 ) ) ,
    ev1 ( make_iev ( bspl ,
                    { 0 , 1 } ,
                      ( bspl.spline_degree < 2 )
                    ? ( 2 - bspl.spline_degree )
                    : 0  ) )
  { }

  template < typename in_t , typename out_t >
  void eval ( const in_t & in , out_t & out ) const
  {
    auto a = ev0 ( in ) ;
    a *= a ;
    auto a3 = a[0] + a[1] + a[2] ;
    auto b = ev1 ( in ) ;
    b *= b ;
    auto b3 = b[0] + b[1] + b[2] ;
    out = ( a3 + b3 ) / ( 256.0f * 256.0f ) ;
  }
} ;

struct gradient_magnitude4_t
: vspline::unary_functor
    < vigra::TinyVector < int , 2 > , float , VSIZE >
{
  // if the incoming spline has a degree of less than two, we
  // build the evaluator with shift, to get useful values.

  const iev4_type ev0 ;
  const iev4_type ev1 ;
  
  gradient_magnitude4_t ( const spline4_type & bspl )
  : ev0 ( make_iev4 ( bspl ,
                      { 1 , 0 } ,
                        ( bspl.spline_degree < 2 )
                      ? ( 2 - bspl.spline_degree )
                      : 0 ) ) ,
    ev1 ( make_iev4 ( bspl ,
                      { 0 , 1 } ,
                        ( bspl.spline_degree < 2 )
                      ? ( 2 - bspl.spline_degree )
                      : 0  ) )
  { }

  template < typename in_t , typename out_t >
  void eval ( const in_t & in , out_t & out ) const
  {
    // TODO: calculation uses associated data, so the magnitudes
    // will be very large, and low-alpha pixels will be subdued.
    // as long as this quality measure isn't combined with others,
    // this isn't a problem, but if it is, the magnitudes have to
    // be balanced. The factor I use below is a rough estimate.
    auto a = ev0 ( in ) ;
    a *= a ;
    auto a3 = a[0] + a[1] + a[2] ;
    auto b = ev1 ( in ) ;
    b *= b ;
    auto b3 = b[0] + b[1] + b[2] ;
    out = ( a3 + b3 ) / float ( 0x200000 ) ;
  }
} ;

// well_exposed_t evaluates the b-spline it's received as it's
// argument and calculates the well-exposedness of the result with
// a well_exposedness_t functor built with the sigma and mu passed.

struct well_exposed_t
: vspline::unary_functor
    < vigra::TinyVector < int , 2 > , float , VSIZE >
{
  const iev_type ev ;
  const well_exposedness_t wexp ;

  well_exposed_t ( const spline_type & bspl ,
                   const float & _sigma = .2f ,
                   const float & _mu = .5f ,
                   const float & _ceiling = 255.0f )
  : ev ( make_iev ( bspl ) ) ,
    wexp ( _sigma , _mu , _ceiling )
  { }

  template < typename in_t , typename out_t >
  void eval ( const in_t & in , out_t & out ) const
  {
    auto px = ev ( in ) ;
    wexp.eval ( px , out ) ;
  }
} ;

struct well_exposed4_t
: vspline::unary_functor
    < vigra::TinyVector < int , 2 > , float , VSIZE >
{
  const iev4_type ev ;
  const well_exposedness_t wexp ;
  deassociate_type da ;

  well_exposed4_t ( const spline4_type & bspl ,
                    const float & _sigma = .2f ,
                    const float & _mu = .5f ,
                    const float & _ceiling = 255.0f )
  : ev ( make_iev4 ( bspl ) ) ,
    wexp ( _sigma , _mu , _ceiling )
  { }

  // calculating well-exposedness for incoming aRGBA data. 
  template < typename in_t , typename out_t >
  void eval ( const in_t & in , out_t & out ) const
  {
    auto px = ev ( in ) ;
    da.eval ( px , px ) ;
    typedef typename std::remove_reference<decltype(px[0])>::type chn_t ;
    vigra::TinyVector < chn_t , 3 > px3 { px[0] , px[1] , px[2] } ;
    wexp.eval ( px3 , out ) ;
  }
} ;

// variant processing the coefficients instead of the evaluation.
// This works only for splines with degree <= 1.

struct well_exposed_raw_t
: vspline::unary_functor
    < vigra::TinyVector < int , 2 > , float , VSIZE >
{
  const yield_t ev ;
  const well_exposedness_t wexp ;

  well_exposed_raw_t ( const spline_type & bspl ,
                       const float & _sigma = .2f ,
                       const float & _mu = .5f ,
                       const float & _ceiling = 255.0f )
  : ev ( bspl.core ) ,
    wexp ( _sigma , _mu , _ceiling )
  {
    assert ( bspl.spline_degree <= 1 ) ;
  }

  template < typename in_t , typename out_t >
  void eval ( const in_t & in , out_t & out ) const
  {
    auto px = ev ( in ) ;
    wexp.eval ( px , out ) ;
  }
} ;

struct well_exposed_raw4_t
: vspline::unary_functor
    < vigra::TinyVector < int , 2 > , float , VSIZE >
{
  const yield4_t ev ;
  const well_exposedness_t wexp ;
  deassociate_type da ;

  well_exposed_raw4_t ( const spline4_type & bspl ,
                        const float & _sigma = .2f ,
                        const float & _mu = .5f ,
                        const float & _ceiling = 255.0f )
  : ev ( bspl.core ) ,
    wexp ( _sigma , _mu , _ceiling )
  {
    assert ( bspl.spline_degree <= 1 ) ;
  }

  template < typename in_t , typename out_t >
  void eval ( const in_t & in , out_t & out ) const
  {
    auto px = ev ( in ) ;
    da.eval ( px , px ) ;
    typedef typename std::remove_reference<decltype(px[0])>::type chn_t ;
    vigra::TinyVector < chn_t , 3 > px3 { px[0] , px[1] , px[2] } ;
    wexp.eval ( px3 , out ) ;
  }
} ;

// weight_sum_t is just a helper class to add two float-valued
// weights generated with two separate weight functors.

template < typename lhs_t , typename rhs_t >
struct weight_sum_t
: vspline::unary_functor
    < vigra::TinyVector < int , 2 > , float , VSIZE >
{
  const lhs_t lhs ;
  const rhs_t rhs ;

  weight_sum_t ( const lhs_t & _lhs ,
                 const rhs_t & _rhs )
  : lhs ( _lhs ) ,
    rhs ( _rhs )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out_type help ;
    lhs.eval ( in , out ) ;
    rhs.eval ( in , help ) ;
    out += help ;
  }
} ;

// another helper class to multiply a float-values weight with
// a float-valued factor

struct weight_bias_t
: public tf11_type
{
  const float bias ;

  weight_bias_t ( const float & _bias )
  : bias ( _bias )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out = in * bias ;
  }
} ;

// with all the helper classes in place, we can now write the combined
// quality functor. We treat several cases separately, to set up the
// combined functor as lean as possible.

struct quality_t
: vspline::unary_functor
    < vigra::TinyVector < int , 2 > , float , VSIZE >
{
  float exposure_weight ;
  float contrast_weight ;
  float sigma ;
  float mu ;
  float ceiling ;

  // The c'tor will calculate the functor to use here - note that
  // this is a grok_type, so it hides the 'inner' type.

  vspline::grok_type < vigra::TinyVector < int , 2 > , float , VSIZE >
    combined_ev ;

  quality_t ( const spline_type & bspl ,
              const float & _exposure_weight = 1.0f ,
              const float & _contrast_weight = 0.0f ,
              const float & _sigma = .2f ,
              const float & _mu = .5f ,
              const float & _ceiling = 255.0f )
  : exposure_weight ( _exposure_weight ) ,
    contrast_weight ( _contrast_weight ) ,
    sigma ( _sigma) ,
    mu ( _mu ) ,
    ceiling ( _ceiling )
  {
    if ( contrast_weight == 0.0f )
    {
      // no contrast weight, that's easy. Even if exposure_weight
      // is also zero, we use it, and it alone. Here we can easily
      // special-case for spline degrees <= 1 and use direct access
      // to the spline's core instead of evaluation. For the remainder
      // of the cases I don't use the special-casing for now.

      if ( bspl.spline_degree <= 1 )
        combined_ev = well_exposed_raw_t ( bspl , sigma , mu , ceiling ) ;
      else
        combined_ev = well_exposed_t ( bspl , sigma , mu , ceiling ) ;
    }
    else if ( exposure_weight == 0.0f )
    {
      // no exposure weight, that's also easy.
      // We use contrast_weight alone:

      combined_ev = gradient_magnitude_t ( bspl ) ;
    }
    else
    {
      // both weights are non-zero. Make as many of them equal to
      // 1.0 as possible, to save on multiplications.

      if ( contrast_weight == exposure_weight )
      {
        // equal weight, we don't need any factors, and normalization
        // will sort out the final result, just add both weights up

        combined_ev = weight_sum_t < well_exposed_t ,
                                     gradient_magnitude_t >
                        ( well_exposed_t ( bspl , sigma , mu , ceiling ) ,
                          gradient_magnitude_t ( bspl ) ) ;
      }
      else if ( contrast_weight > exposure_weight )
      {
        // contrast weight is larger than exposure weight, so we'll
        // use contrast weight 1 and a proportionally smaller
        // exposure weight. again, normalization will sort out that
        // the combined values have greater-than-one magnitude.

        exposure_weight /= contrast_weight ;
        contrast_weight = 1.0f ;

        auto lhs =   well_exposed_t ( bspl , sigma , mu , ceiling )
                   + weight_bias_t ( exposure_weight ) ;

        combined_ev = weight_sum_t < decltype(lhs) ,
                                     gradient_magnitude_t >
                        ( lhs   ,
                          gradient_magnitude_t ( bspl ) ) ;
      }
      else
      {
        // this case is just the other way around.

        contrast_weight /= exposure_weight ;
        exposure_weight = 1.0f ;

        auto rhs =   gradient_magnitude_t ( bspl )
                   + weight_bias_t ( contrast_weight ) ;
  
        combined_ev = weight_sum_t < well_exposed_t ,
                                     decltype(rhs) >
                        ( well_exposed_t ( bspl , sigma , mu , ceiling ) ,
                          rhs ) ;
      }
    }
      
  }

  template < typename in_t , typename out_t >
  void eval ( const in_t & in , out_t & out ) const
  {
    combined_ev.eval ( in , out ) ;
  }
} ;

struct quality4_t
: vspline::unary_functor
    < vigra::TinyVector < int , 2 > , float , VSIZE >
{
  float exposure_weight ;
  float contrast_weight ;
  float sigma ;
  float mu ;
  float ceiling ;

  // The c'tor will calculate the functor to use here - note that
  // this is a grok_type, so it hides the 'inner' type.

  vspline::grok_type < vigra::TinyVector < int , 2 > , float , VSIZE >
    combined_ev ;
  vspline::grok_type < vigra::TinyVector < int , 2 > , pixel4_type , VSIZE >
    ev4 ;

  
  quality4_t ( const spline4_type & bspl ,
               const float & _exposure_weight = 1.0f ,
               const float & _contrast_weight = 0.0f ,
               const float & _sigma = .2f ,
               const float & _mu = .5f ,
               const float & _ceiling = 255.0f )
  : exposure_weight ( _exposure_weight ) ,
    contrast_weight ( _contrast_weight ) ,
    sigma ( _sigma) ,
    mu ( _mu ) ,
    ceiling ( _ceiling )
  {
    if ( contrast_weight == 0.0f )
    {
      // no contrast weight, that's easy. Even if exposure_weight
      // is also zero, we use it, and it alone. Here we can easily
      // special-case for spline degrees <= 1 and use direct access
      // to the spline's core instead of evaluation. For the remainder
      // of the cases I don't use the special-casing for now.

      if ( bspl.spline_degree <= 1 )
        combined_ev = well_exposed_raw4_t ( bspl , sigma , mu , ceiling ) ;
      else
        combined_ev = well_exposed4_t ( bspl , sigma , mu , ceiling ) ;
    }
    else if ( exposure_weight == 0.0f )
    {
      // no exposure weight, that's also easy.
      // We use contrast_weight alone:

      combined_ev = gradient_magnitude4_t ( bspl ) ;
    }
    else
    {
      // both weights are non-zero. Make as many of them equal to
      // 1.0 as possible, to save on multiplications.

      if ( contrast_weight == exposure_weight )
      {
        // equal weight, we don't need any factors, and normalization
        // will sort out the final result, just add both weights up

        combined_ev = weight_sum_t < well_exposed4_t ,
                                     gradient_magnitude4_t >
                        ( well_exposed4_t ( bspl , sigma , mu , ceiling ) ,
                          gradient_magnitude4_t ( bspl ) ) ;
      }
      else if ( contrast_weight > exposure_weight )
      {
        // contrast weight is larger than exposure weight, so we'll
        // use contrast weight 1 and a proportionally smaller
        // exposure weight. again, normalization will sort out that
        // the combined values have greater-than-one magnitude.

        exposure_weight /= contrast_weight ;
        contrast_weight = 1.0f ;

        auto lhs =   well_exposed4_t ( bspl , sigma , mu , ceiling )
                   + weight_bias_t ( exposure_weight ) ;

        combined_ev = weight_sum_t < decltype(lhs) ,
                                     gradient_magnitude4_t >
                        ( lhs   ,
                          gradient_magnitude4_t ( bspl ) ) ;
      }
      else
      {
        // this case is just the other way around.

        contrast_weight /= exposure_weight ;
        exposure_weight = 1.0f ;

        auto rhs =   gradient_magnitude4_t ( bspl )
                   + weight_bias_t ( contrast_weight ) ;
  
        combined_ev = weight_sum_t < well_exposed4_t ,
                                     decltype(rhs) >
                        ( well_exposed4_t ( bspl , sigma , mu , ceiling ) ,
                          rhs ) ;
      }
    }
  }

  template < typename in_t , typename out_t >
  void eval ( const in_t & in , out_t & out ) const
  {
    combined_ev.eval ( in , out ) ;
  }
} ;

// functor to produce masks.
// normalized_quality_type takes a set of quality functors and a set of
// views to float arrays. Then for each coordinate, the quality functors
// are invoked to yield quality values for the pixels at that coordinate,
// these values are summed up, and the individual pixel values divided
// by the sum and written to the float arrays. If all pixels are zero,
// they remain so, and in these places, the result (the 'anti mask') is
// set to 1.0 - in all other places the result is 1.0.
// This way, there's no need for an intermediate 'norm' array which has
// to be updated with every mask, then used to normalize the masks: the
// adding up and division are done on the pixel level, right after the
// mask values were generated, which also saves the step of first saving
// the 'raw' mask data to an array to be normalized later on in a separate
// step. Altogether we avoid a lot of memory traffic - saving the masks
// individually can't be avoided, because we have to pass them separately
// to the fusion routine, but with this functor in place, we access them
// once to deposit the mask values and once when building the pyramids.
// Below, there's a version for images with alpha channel. That version
// multiplies the masks with the image's alpha channel before the
// normalization step, otherwise the code is the same (TODO factor out?)
// After the mask data have been produced, normalized and stored the
// eval routines produce output: Where the norm was zero, the output
// is one, and where the norm was not zero, the output is zero. This
// is the 'anti mask', with value one for all pixels which aren't
// covered by any masks at all.

struct normalized_quality_type
: vspline::unary_functor
    < vigra::TinyVector < int , 2 > , float , VSIZE >
{
  typedef vigra::MultiArrayView < 2 , float > float_view_type ;

  std::vector < iaev_type > & qevv ;
  std::vector < float_view_type * > & qv ;
  const int nfacets ;
  const vigra::TinyVector < int , 2 > stride ;

  normalized_quality_type ( std::vector < iaev_type > & _qevv ,
                            std::vector < float_view_type * > & _qv )
  : qevv ( _qevv ) ,
    qv ( _qv ) ,
    nfacets ( _qv.size() ) ,
    stride ( _qv[0]->stride() ) // assumes all are equally strided
  {
    assert ( qv.size() == qevv.size() ) ;
  }

  void eval ( const in_type & in , out_type & out ) const
  {
    std::vector < float > quality ( nfacets ) ;
    float norm = 0.0f ;
    for ( int i = 0 ; i < nfacets ; i++ )
    {
      // for each partial image, evaluate the quality functor at the
      // current coordinate (storing in quality[i]) and add the result
      // to 'norm'

      qevv [ i ] . eval ( in , quality [ i ] ) ;
      norm += quality [ i ] ;
    }
    // TODO: the test here was for == 0.0f, but this produced wrong
    // output. investigate!
    if ( ! ( norm >= 0.00001f ) )
    {
      // if 'norm' turns out zero, indicating that no mask has non-zero
      // value at the current coordinate, set the values of the individual
      // masks to zero.

      for ( int i = 0 ; i < nfacets ; i++ )
      {
        (*(qv[i])) [ in ] = 0.0f ;
      }
    }
    else
    {
      // a non-zero value in 'norm' indicates that we had at least one
      // non-zero mask value. We store the individual mask values to their
      // respective arrays after applying the norm, and the anti mask is
      // set to zero.

      for ( int i = 0 ; i < nfacets ; i++ )
      {
        (*(qv[i])) [ in ] = quality [ i ] / norm ;
      }
    }
  }

#ifdef VECTORIZE

  template < typename in_t >
  void eval ( const in_t & in , channel_v & out ) const
  {
    channel_v quality [ nfacets ] ;
    channel_v norm = 0.0f ;
    auto offsets = in [ 0 ] * stride [ 0 ] ;
    offsets += in [ 1 ] * stride [ 1 ] ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      qevv [ i ] . eval ( in , quality [ i ] ) ;
      norm += quality [ i ] ;
    }
    // here, the mask with == .0f works...
    for ( int i = 0 ; i < nfacets ; i++ )
    {
      channel_v nq = quality [ i ] / norm ;
      nq ( ! ( norm >= 0.00001f ) ) = 0.0f ;
      nq.scatter ( qv[i]->data() , offsets ) ;
    }
  }

#endif

} ;

struct normalized_quality_alpha_type
: vspline::unary_functor
    < vigra::TinyVector < int , 2 > , float , VSIZE >
{
  typedef vigra::MultiArrayView < 2 , float > float_view_type ;

  const std::vector < alpha_ev_type > & qevv ;
  std::vector < float_view_type * > & qv ;
  std::vector < float_array_type * > & av ;
  const int nfacets ;
  const vigra::TinyVector < int , 2 > stride ;
  const vigra::TinyVector < int , 2 > alpha_stride ;

  normalized_quality_alpha_type
      ( const std::vector < alpha_ev_type > & _qevv ,
        std::vector < float_view_type * > & _qv ,
        std::vector < float_array_type * > & _av )
  : qevv ( _qevv ) ,
    qv ( _qv ) ,
    av ( _av ) ,
    nfacets ( _qv.size() ) ,
    stride ( _qv[0]->stride() ) , // assumes all are equally strided
    alpha_stride ( _av[0]->stride() ) // assumes all are equally strided
  {
    assert ( qv.size() == qevv.size() ) ;
    assert ( av.size() == qv.size() ) ;
  }

  void eval ( const in_type & in , out_type & out ) const
  {
    std::vector < float > quality ( nfacets ) ;
    float norm ;
    for ( int i = 0 ; i < nfacets ; i++ )
    {
      qevv [ i ] . eval ( in , quality [ i ] ) ;

      // this step differs from the non-alpha code: we apply the alpha
      // channel to the mask, lessening the contribution with increasing
      // transparency, so that complete transparency results in zero
      // weight. We simply multiply with the alpha channel, even though
      // it's in the range of [0:255], because normalization will force
      // the range to [0:1] anyway.

      quality [ i ] *= ( (*(av[i])) [ in ] ) ;
      norm += quality [ i ] ;
    }
    if ( ! ( norm >= 0.00001f ) )
    {
      for ( int i = 0 ; i < nfacets ; i++ )
      {
        (*(qv[i])) [ in ] = 0.0f ;
      }
      out = 1.0f ;
    }
    else
    {
      for ( int i = 0 ; i < nfacets ; i++ )
      {
        (*(qv[i])) [ in ] = quality [ i ] / norm ;
      }
      out = 0.0f ;
    }
  }

#ifdef VECTORIZE

  template < typename in_t >
  void eval ( const in_t & in , channel_v & out ) const
  {
    channel_v quality [ nfacets ] ;
    channel_v norm = 0.0f ;
    auto offsets = in [ 0 ] * stride [ 0 ] ;
    offsets += in [ 1 ] * stride [ 1 ] ;
    auto alpha_offsets = in [ 0 ] * alpha_stride [ 0 ] ;
    alpha_offsets += in [ 1 ] * alpha_stride [ 1 ] ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      qevv [ i ] . eval ( in , quality [ i ] ) ;
      channel_v alpha ;
      alpha.gather ( av[i]->data() , alpha_offsets ) ;
      quality [ i ] *= ( alpha ) ;
      norm += quality [ i ] ;
    }
    for ( int i = 0 ; i < nfacets ; i++ )
    {
      channel_v nq = quality [ i ] / norm ;
      nq ( ! ( norm >= 0.00001f ) ) = 0.0f ;
      nq.scatter ( qv[i]->data() , offsets ) ;
    }
    out = 0.0f ;
    out ( ! ( norm >= 0.00001f ) ) = 1.0f ;
  }

#endif

} ;

// clamp incoming values to an interval of [0:255]. We use vspline's
// min and max, which work both for scalar and vector data.

struct clamp_t
: public tf33_type
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    for ( int i = 0 ; i < 3 ; i++ )
    {
      out[i] = vspline::max ( 0.0f , in[i] ) ;
      out[i] = vspline::min ( 255.0f , out[i] ) ;
    }
  }
} ;

// clamp an aRGBA value to aSRGB range

struct clamp4_t
: public tf44_type
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    auto ceil = in[3] * 255.0f ;

    for ( int i = 0 ; i < 3 ; i++ )
    {
      out[i] = vspline::max ( 0.0f , in[i] ) ;
      out[i] = vspline::min ( ceil , out[i] ) ;
    }
    out[3] = in[3] ;
  }
} ;

struct diff1_t
: public tf21_type
{
  const yield1_t lhs ;
  const yield1_t rhs ;

  diff1_t ( const yield1_t & _lhs ,
            const yield1_t & _rhs )
  : lhs ( _lhs ) ,
    rhs ( _rhs )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    lhs.eval ( in , out ) ;
    out_type help ;
    rhs.eval ( in , help ) ;
    out -= help ;
  }
} ;

struct diff4_t
: public tf24_type
{
  const ev4_type lhs ;
  const ev4_type rhs ;

  diff4_t ( const ev4_type & _lhs ,
            const ev4_type & _rhs )
  : lhs ( _lhs ) ,
    rhs ( _rhs )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out = lhs ( in ) - rhs ( in ) ;
  }
} ;

struct sum1_t
: public tf21_type
{
  const yield1_t lhs ;
  const yield1_t rhs ;

  sum1_t ( const yield1_t & _lhs ,
            const yield1_t & _rhs )
  : lhs ( _lhs ) ,
    rhs ( _rhs )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    lhs.eval ( in , out ) ;
    out_type help ;
    rhs.eval ( in , help ) ;
    out += help ;
    out = vspline::min ( 1.0f , out ) ;
    out = vspline::max ( 0.0f , out ) ;
  }
} ;

struct sum4_t
: public tf24_type
{
  const ev4_type lhs ;
  const ev4_type rhs ;

  sum4_t ( const ev4_type & _lhs ,
           const ev4_type & _rhs )
  : lhs ( _lhs ) ,
    rhs ( _rhs )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out = lhs ( in ) + rhs ( in ) ;
  }
} ;

// given two functors yielding pixels for discrete coordinates,
// produce the result of the first functor, unless that is
// zero, in which case the result of the second functor is
// produced.

struct paste_type
: vspline::unary_functor < int2_type , pixel_type , VSIZE >
{
  const yield_t ev1 ;
  const yield_t ev2 ;

  paste_type ( view_type * p_top ,    // top image
               view_type * p_bottom ) // bottom image
  : ev1 ( *p_top ) ,
    ev2 ( *p_bottom )
  { }

  void eval ( const int2_type & crd , pixel_type & out ) const
  {
    ev1.eval ( crd , out ) ;
    if ( out == pixel_type ( 0.0f ) )
    {
      ev2.eval ( crd , out ) ;
    }
  }

  template < typename in_t , typename out_t >
  void eval ( const in_t & crd , out_t & out ) const
  {
    ev1.eval ( crd , out ) ;
    auto sum = out[0] + out[1] + out[2] ;
    auto black = ( sum == 0.0f ) ;
    if ( any_of ( black ) )
    {
      out_t help ;
      ev2.eval ( crd , help ) ;
      out[0] ( black ) = help[0] ;
      out[1] ( black ) = help[1] ;
      out[2] ( black ) = help[2] ;
    }
  }
} ;

struct bias4_functor
: public vspline::unary_functor < pixel4_type , pixel4_type , VSIZE >
{
  const float bias ;

  // constructor initializes state
  bias4_functor ( float _bias = 1.0f )
  : bias ( _bias )
  { } ;

  template < class PT >
  void eval ( const PT & c ,
                    PT & result ) const
  {
    result[0] = c[0] * bias ;
    result[1] = c[1] * bias ;
    result[2] = c[2] * bias ;
    result[3] = c[3] ;
  }
} ;

// this functor yields the product of two evaluators. The difference to
// the normal 'prod_t' is that the evaluators take discrete coordinates;
// here we simply have yield_type objects, the first evaluator yields
// pixel_type, the seconds yields float. This is quite a 'custom'
// object and only used here in pv_combine.cc.

struct qprod_t
: public vspline::unary_functor
  < vigra::TinyVector < int , 2 > ,
    vigra::TinyVector < float , 3 > ,
    VSIZE >
{
  const iev_type lhs ;
  const iaev_type rhs ;

  qprod_t ( const iev_type & _lhs ,
            const iaev_type & _rhs )
  : lhs ( _lhs ) ,
    rhs ( _rhs )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out = lhs ( in ) * rhs ( in ) ;
  }
} ;

struct qprod4_t
: public vspline::unary_functor
  < vigra::TinyVector < int , 2 > ,
    vigra::TinyVector < float , 4 > ,
    VSIZE >
{
  const iev4_type lhs ;
  const iaev_type rhs ;

  qprod4_t ( const iev4_type & _lhs ,
             const iaev_type & _rhs )
  : lhs ( _lhs ) ,
    rhs ( _rhs )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out = lhs ( in ) * rhs ( in ) ;
  }
} ;

// one more 'custom' functor. It's like the previous one, but taking
// float coordinates.

struct qprodf_t
: public tf23_type
{
  const ev_type lhs ;
  const alpha_ev_type rhs ;

  qprodf_t ( const ev_type & _lhs ,
             const alpha_ev_type & _rhs )
  : lhs ( _lhs ) ,
    rhs ( _rhs )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out = lhs ( in ) * rhs ( in ) ;
  }
} ;

struct qprodf4_t
: public tf24_type
{
  const ev4_type lhs ;
  const alpha_ev_type rhs ;

  qprodf4_t ( const ev4_type & _lhs ,
              const alpha_ev_type & _rhs )
  : lhs ( _lhs ) ,
    rhs ( _rhs )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out = lhs ( in ) * rhs ( in ) ;
  }
} ;

// functor to make a laplacian value from two gaussian values.
// the current implementation is a straight subtraction of the aRGBA
// values, which isn't right and needs more thought.

struct mkl_t
: public tf24_type
{
  const ev4_type lhs ;
  const ev4_type rhs ;
  deassociate_type da ;
  associate_type a ;

  mkl_t ( const ev4_type & _lhs ,
          const ev4_type & _rhs )
  : lhs ( _lhs ) ,
    rhs ( _rhs )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    auto lb = lhs ( in ) ;
    auto lt = rhs ( in ) ;
    out[0] = lb[0] - lt[0] ;
    out[1] = lb[1] - lt[1] ;
    out[2] = lb[2] - lt[2] ;
    out[3] = lb[3] - lt[3] ;
  }
} ;

// functor to apply weighting to a laplacian value.
// lhs is the functor yielding the laplacian, and rhs the functor
// yielding the weighting factor - the value of the corresponding
// level of the 'quality' pyramid. The laplacian is expensive to
// calculate, so we check the mask first: if it's zero, we can
// skip over the location. The mask is actually more likely to be
// zero than not, so it's a big time-saver.

struct wlf_t
: public tf24_type
{
  const ev4_type lhs ;
  const alpha_ev_type rhs ;

  wlf_t ( const ev4_type & _lhs ,
          const alpha_ev_type & _rhs )
  : lhs ( _lhs ) ,
    rhs ( _rhs )
  { }

  void eval ( const in_type & in , out_type & out ) const
  {
    auto factor = rhs ( in ) ;
    if ( factor <= .00001f )
    {
      out = 0.0f ;
    }
    else
    {
      out = lhs ( in ) * factor ;
    }
  }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    auto factor = rhs ( in ) ;
    auto mask ( factor > .00001f ) ;
    if ( none_of ( mask ) )
    {
      out = 0.0f ;
    }
    else
    {
      out = lhs ( in ) * factor ;
    }
  }
} ;

// this functor creates a mask with values ranging in [0:1]
// from an RGBA image made by the 'mask_for' process, using the
// value in the red channel and applying the alpha channel.

struct make_mask_t
: public tf41_type
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out = in[0] * in[3] / 255.0f ;
  }
} ;

// replace the alpha channel of _ev4's evaluate by the biased result of
// evaluating the alpha functor (_aev) at the corresponding position.
// Note how incoming coordinates are discrete: this functor is meant
// to be run with yield_type functors which take discrete coordinates.

struct dub_alpha
: public vspline::unary_functor < int2_type , pixel4_type , VSIZE >
{
  const iev4_type ev4 ;
  const iaev_type aev ;
  const float bias ;

  dub_alpha ( const iev4_type _ev4 ,
              const iaev_type & _aev ,
              float _bias )
  : ev4 ( _ev4 ) ,
    aev ( _aev ) ,
    bias ( _bias )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    ev4.eval ( in , out ) ;
    aev.eval ( in , out[3] ) ;
    out [ 3 ] *= bias ;
  }
} ;

// add the evaluate of an RGB functor to an alpha evaluator's evaluate
// at the same position. Used to layer on partial results.

struct add_rgb
: public tf24_type
{
  const ev4_type ev4 ;
  const ev_type ev ;

  add_rgb ( const ev4_type _ev4 ,
            const ev_type & _ev )
  : ev4 ( _ev4 ) ,
    ev ( _ev )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    ev4.eval ( in , out ) ;
    auto px = ev ( in ) ;
    out [ 0 ] += px [ 0 ] ;
    out [ 1 ] += px [ 1 ] ;
    out [ 2 ] += px [ 2 ] ;
  }
} ;

// like the functor above, but without using ev4 - the RGB value
// simply sets RGB in the target pixel, alpha is set to zero.

struct set_rgb
: public tf24_type
{
  const ev_type ev ;

  set_rgb ( const ev_type & _ev )
  : ev ( _ev )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    auto px = ev ( in ) ;
    out [ 0 ] = px [ 0 ] ;
    out [ 1 ] = px [ 1 ] ;
    out [ 2 ] = px [ 2 ] ;
    out [ 3 ] = 0.0f ;
  }
} ;

struct add_alpha
: public tf24_type
{
  const ev_type ev ;
  const alpha_ev_type aev ;

  add_alpha ( const ev_type _ev ,
              const alpha_ev_type & _aev )
  : ev ( _ev ) ,
    aev ( _aev )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    auto px3 = ev ( in ) ;
    auto alpha = aev ( in ) ;
    out[0] = px3[0] ;
    out[1] = px3[1] ;
    out[2] = px3[2] ;
    out[3] = alpha ;
  }
} ;

struct over_alpha
: public tf24_type
{
  const ev4_type ev4 ;
  const alpha_ev_type aev ;
  deassociate_type da ;
  associate_type a ;

  over_alpha ( const ev4_type _ev4 ,
               const alpha_ev_type & _aev )
  : ev4 ( _ev4 ) ,
    aev ( _aev )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    ev4.eval ( in , out ) ;
    aev.eval ( in , out[3] ) ;
  }
} ;

struct mul_alpha
: public tf44_type
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out[0] = in[0] * in[3] / 255.0f ;
    out[1] = in[1] * in[3] / 255.0f ;
    out[2] = in[2] * in[3] / 255.0f ;
    out[3] = 0.0f ;
  }
} ;

struct clear_alpha
: public tf44_type
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out[0] = in[0] ;
    out[1] = in[1] ;
    out[2] = in[2] ;
    out[3] = .0f ;
  }
} ;

struct full_alpha
: public tf34_type
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out[0] = in[0] ;
    out[1] = in[1] ;
    out[2] = in[2] ;
    out[3] = 255.0f ;
  }
} ;

struct full_alpha4
: public tf44_type
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out[0] = in[0] ;
    out[1] = in[1] ;
    out[2] = in[2] ;
    out[3] = 255.0f ;
  }
} ;

// this functor pulls up semi-transparent pixels to fully opaque.

struct pull_up_alpha
: public tf44_type
{
  void eval ( const in_type & in , out_type & out ) const
  {
    if ( in[3] >= 0.00001f )
    {
      auto factor = 255.0f / in[3] ;
      out[0] = in[0] * factor ;
      out[1] = in[1] * factor ;
      out[2] = in[2] * factor ;
      out[3] = 255.0f ;
    }
    else
    {
      out = 0.0f ;
    }
  }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out = 0.0f ;
    auto mask = ( in[3] >= 0.00001f ) ;
    if ( any_of ( mask ) )
    {
      auto factor = 255.0f / in[3] ;
      out[0] ( mask ) = in[0] * factor ;
      out[1] ( mask ) = in[1] * factor ;
      out[2] ( mask ) = in[2] * factor ;
      out[3] ( mask ) = 255.0f ;
    }
  }
} ;

struct sweep_t
: public tf44_type
{
  void eval ( const in_type & in , out_type & out ) const
  {
    if ( in[3] > 0.001f )
    {
      out = in ;
    }
    else
    {
      out = 0 ;
    }
  }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out = in ;
    auto mask ( in[3] <= 0.001f ) ;
    if ( any_of ( mask ) )
    {
      out[0] ( mask ) = 0.0f ;
      out[1] ( mask ) = 0.0f ;
      out[2] ( mask ) = 0.0f ;
      out[3] ( mask ) = 0.0f ;
    }
  }
} ;

struct anti_bleed_t
: vspline::unary_functor < int2_type , float , VSIZE >
{
  const yield1_t quality ;
  const yield1_t bleed ;

  anti_bleed_t ( const float_view_type & quality_view ,
                 const float_view_type & bleed_view )
  : quality ( quality_view ) ,
    bleed ( bleed_view )
  { }

  void eval ( const in_type & in , out_type & out ) const
  {
    auto q = quality ( in ) ;
    if ( q > 0.00001f )
    {
      auto b = bleed ( in ) ;
      if ( b > 0.00001f )
        out = q / b ;
      else
        out = 0.0 ;
    }
    else
    {
      out = 0 ;
    }
  }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out = 0.0f ;
    auto q = quality ( in ) ;
    auto mask ( q > 0.00001f ) ;
    if ( any_of ( mask ) )
    {
      auto b = bleed ( in ) ;
      mask &= ( b > 0.00001f ) ;
      if ( any_of ( mask ) )
      {
        out ( mask ) = q / b ;
      }
    }
  }
} ;

// The stitching/exposure fusion code will yield float RGB(A) images,
// and for on-screen display we need 8-bit RGBA. This conversion is
// implemented by these conversion functors:

struct rgb2rgba8_type
: public vspline::unary_functor < pixel_type , int , VSIZE >
{
  void eval ( const pixel_type & in ,
                           int & out ) const
  {
    float channel ;
    out = 255u ;
    for ( int ch = 2 ; ch >= 0 ; ch-- )
    {
      channel = in [ ch ] ;
      out <<= 8 ;

      if ( channel > 0.0f )
      {
        // for screen display, we unconditionally cap at 255
        if ( channel > 255.0 )
          out |= 255 ;
        else
        {
          out |= int ( channel ) ;
        }
      }
    }
  }

#ifdef VECTORIZE

  void eval ( const pixel_v & in ,
                    int_v & out ) const
  {
    channel_v channel ;
    out = 255u ;
    for ( int ch = 2 ; ch >= 0 ; ch-- )
    {
      channel = in [ ch ] ;

      channel ( channel < 0.0f ) = 0.0f ;

      // for screen display, we unconditionally cap at 255

      channel ( channel > 255.0f ) = 255.0f ;

      out <<= 8 ;

      out |= int_v ( channel ) ;
    }
  }

#endif

} ;

struct rgba2rgba8_type
: public vspline::unary_functor < pixel4_type , int , VSIZE >
{
  void eval ( const pixel4_type & in ,
                            int & out ) const
  {
    float channel ;
    out = std::max ( 0.0f , std::min ( 255.0f , in [ 3 ] ) ) ;

    for ( int ch = 2 ; ch >= 0 ; ch-- )
    {
      channel = in [ ch ] ;
      out <<= 8 ;

      if ( channel > 0.0f )
      {
        // for screen display, we unconditionally cap at 255
        if ( channel > 255.0 )
          out |= 255 ;
        else
        {
          out |= int ( channel ) ;
        }
      }
    }
  }

#ifdef VECTORIZE

  void eval ( const pixel4_v & in ,
                    int_v & out ) const
  {
    channel_v channel ;
    out = in [ 3 ] ;
    out ( in [ 3 ] < 0.0f ) = 0u ;
    out ( in [ 3 ] > 255.0f ) = 255u ;

    for ( int ch = 2 ; ch >= 0 ; ch-- )
    {
      channel = in [ ch ] ;

      channel ( channel < 0.0f ) = 0.0f ;

      // for screen display, we unconditionally cap at 255

      channel ( channel > 255.0f ) = 255.0f ;

      out <<= 8 ;

      out |= int_v ( channel ) ;
    }
  }

#endif

} ;

// to use these functors, we use short 'feeder' functions, which take
// care of casting the pointer to the output data, which interprets
// them as TinyVectors of four unsigned char, to int instead, which the
// functors use as output.

void to_screen ( view_type * p_frgb ,
                 rgba_image_type * p_rgba )
{
  vigra::MultiArrayView < 2 , int >
    target ( p_rgba->shape() ,
             p_rgba->stride() ,
             (int*) ( p_rgba->data() ) ) ;

  vspline::transform ( rgb2rgba8_type() , *p_frgb , target ) ;
}

void to_screen ( view4_type * p_frgba ,
                 rgba_image_type * p_rgba )
{
  vigra::MultiArrayView < 2 , int >
    target ( p_rgba->shape() ,
             p_rgba->stride() ,
             (int*) ( p_rgba->data() ) ) ;

  vspline::transform ( rgba2rgba8_type() , *p_frgba , target ) ;
}

// class blend_type serves as a 'scaffolding' object for several functions
// performing image splining or exposure fusion with lux' modified version
// of the Burt & Adelson image splining algorithm. These functions all need
// a common environment of settings, which is built up during blend_type's
// construction. Once set up, the specific blending function is called,
// using the 'scaffolding'. After the call, the blend_type object is
// destroyed, so the code follows RAII.
// The separate blending routines all follow a similar pattern: they
// create pairs of a partial image and a corresponding mask, and call
// a common routine 'fuse' which uses the mask and partial image to
// add a layer to the final result. Once all partial images have been
// layered on, the result is ready and it's disposed of either by writing
// it to a file, or by putting the data into an 8bit RGBA frame for
// on-screen display, which is pushed to the frame queue together with
// the job which set the whole process in motion.

struct blend_type
{
  // to set up the splines and pyramids, we need a few parameters.
  // I used spline degree 3 for a while, but now I've reverted to
  // simple bilinear interpolation, which seems perfectly good enough
  // and is faster. The spline degree can simply be chosen here and will
  // be used throughout the code. Note that the partial images are not
  // rendered with this spline degree: they use whatever is set as the
  // HQ interpolator. The parameters here pertain to the levels of the
  // pyramids used by the modified B&A algorithm.
  // smoothing level is the parameter steering the decimator used for
  // creating the pyramids - again, this pertains to the pyramids used
  // by the modified B&A algorithm only. Here I also used a 'better'
  // decimator for some time, namely a 'convolving basis functor'
  // (code -2) applying a small binomial filter. I've now reverted to
  // the use of 'area decimation' (code -1), which is faster.

  const int i_spline_degree ;   // spline degree of image splines
  int i_headroom ;        // headroom for image splines
  const int i_smoothing_level ; // decimation with area decimator

  // This is the degree to which a top level spline in the 'gaussian'
  // pyramid will be shifted before the 'laplacian' is formed. Shifting
  // to at least degree 2 guarantees a 'smooth' function - within the
  // smoothness limits for the spline (i.e. up to which derivative it is
  // continuous)

  const int i_shift_to ;        // spline degree for expanded top level

  const int q_spline_degree ;   // spline degree of quality splines
  int q_headroom ;              // headroom for quality splines
  const int q_smoothing_level ; // decimation with area decimator
  const int q_shift_to ;        // spline degree for expanded top level

  // These variables are all 'scaffolding' to be used by the blending code.
  // Note again that the blend_type object is just RAII-style scaffolding
  // and only meant for a single use.

  job_type * p_job ;
  frame_type & frame ;
  target_type & target ;
  blending_settings_type & bls ;
  std::vector < int > active_facets ;
  int nactive ;
  int njobs ;
  vigra::Shape2 image_shape ;
  vspline::bcv_type < 2 > bcv ;
  bool wrap_horizontal ;
  bool full_spherical ;

  static int get_i_headroom ( const job_type * p_job )
  {
    auto degree = p_job->frame.blending_settings.i_spline_degree ;
    auto decimator = p_job->frame.blending_settings.i_spline_decimator ;
    int headroom ;
    if ( decimator <= -5 )
    {
      int ksz = - decimator ;
      headroom = ( ksz - 1 ) / 2 ;
    }
    else if ( decimator == -3 || decimator == -4 )
      headroom = 2 ;
    else if ( decimator < 0 )
      headroom = 1 ;
    else
      headroom = ( decimator + 1 ) / 2 ;
    return std::max ( ( degree + 1 ) / 2 , headroom ) ;
  }

  static int get_q_headroom ( const job_type * p_job )
  {
    auto degree = p_job->frame.blending_settings.q_spline_degree ;
    auto decimator = p_job->frame.blending_settings.q_spline_decimator ;
    int headroom ;
    if ( decimator <= -5 )
    {
      int ksz = - decimator ;
      headroom = ( ksz - 1 ) / 2 ;
    }
    else if ( decimator == -3 || decimator == -4 )
      headroom = 2 ;
    else if ( decimator < 0 )
      headroom = 1 ;
    else
      headroom = ( decimator + 1 ) / 2 ;
    return std::max ( ( degree + 1 ) / 2 , headroom ) ;
  }

  blend_type ( job_type * _p_job )
  : p_job ( _p_job ) ,
    frame ( _p_job->frame ) ,
    target ( _p_job->target ) ,
    bls ( _p_job->frame.blending_settings ) ,
    i_spline_degree ( _p_job->frame.blending_settings.i_spline_degree ) ,
    i_headroom ( get_i_headroom ( _p_job ) ) ,
    i_smoothing_level ( _p_job->frame.blending_settings.i_spline_decimator ) ,
    i_shift_to ( _p_job->frame.blending_settings.i_spline_shift_to ) ,
    q_spline_degree ( _p_job->frame.blending_settings.q_spline_degree ) ,
    q_headroom ( get_q_headroom ( _p_job ) ) ,
    q_smoothing_level ( _p_job->frame.blending_settings.q_spline_decimator ) ,
    q_shift_to ( _p_job->frame.blending_settings.q_spline_shift_to ) ,
    bcv { REFLECT , REFLECT }
  {
    // if we want to shift the splines up for evaluation, we need extra headroom:

    if ( i_shift_to > 1 )
      i_headroom += i_shift_to / 2 ;

    if ( q_shift_to > 1 )
      q_headroom += q_shift_to / 2 ;

    // make sure we have a good job count

    njobs = p_job->njobs ;
    if ( njobs == 0 )
      njobs = vspline::default_njobs ;

    if ( i_smoothing_level == -1 || q_smoothing_level == -1 )
    {
      // area decimation can't handle scaling steps larger than two
      // TODO: this should be noticed earlier on during CL processing
      assert ( bls.scaling_step <= 2.0 ) ;
    }

    // find out which facets are visible with the current view. This
    // narrows down the amount of partials we have to render: if we're
    // only looking at a part of the whole facet map, we can omit facets
    // which are definitely not visible. Note that the active-facet-finding
    // code is conservative and may include some invisible facets, which
    // increases processing time but is, otherwise, no problem.

    active_facets = frame.p_itp->active_facets ( p_job ) ;
    nactive = active_facets.size() ;

    // check if we need to 'wrap around'. One advantage of working with
    // boundary-condition-preserving splines as pyramid levels is the simple
    // handling of periodic pyramid levels without having to provide ample
    // support.
    
    wrap_horizontal
      = (    ( fabs ( target.hfov - 2.0 * M_PI ) < .000001 )
          && (    ( target.projection == SPHERICAL )
               || ( target.projection == CYLINDRIC ) ) ) ;

    full_spherical
      = (    ( target.projection == SPHERICAL )
          && ( fabs ( target.hfov - 2.0 * M_PI ) < .000001 )
          && ( fabs ( target.vfov - M_PI ) < .000001 ) ) ;

    // set the boundary conditions for the splines we'll use. For most images
    // we'll use REFLECT, but for images which are horizontally periodic, we
    // specify PERIODIC for the horizontal.

    if ( wrap_horizontal )
      bcv[0] = PERIODIC ;

    // we'll render many images to this shape:

    if ( target.cropping_active )
      image_shape = vigra::Shape2 ( target.crop_width , target.crop_height ) ;
    else
      image_shape = vigra::Shape2 ( target.width , target.height ) ;
  }

  // Now the fusion code: This first function is the 'heart' of the
  // operation, implementing the collapse of a weighted laplacian
  // pyramid to produce a partial result of the fusion process.
  // This function produces a 'layer' of an exposure fusion/stitch,
  // which is added to previous results. Processing the layers, which
  // correspond to partial images, separately, is easier on memory,
  // especially for stitches, where lux can create normalized masks
  // directly, without having to sum up all partial masks first.
  // The method used here is similar to the process given in the paper
  // "Exposure Fusion" by Tom Mertens, Jan Kautz, and Frank Van Reeth
  // which is also the method which 'enfuse' by Andrew Mihal uses.
  // This method is based on the method published by Peter J. Burt
  // and Edward H. Adelson in their article 'A Multiresolution Spline
  // With Application to Image Mosaics', which describes the method's
  // application with spatial masks for stitching jobs.
  // Here we use it with a few slight modifications which I found
  // to work well. The code is the same for stitching and exposure
  // fusion: the function receives a partial image and the corresponding
  // mask as pointers to vspline::bspline objects, which are used
  // directly as level-0 splines of the image and quality pyramids.
  // p-canvas is initially zero, and later it points to the summed-up
  // results, onto which the current iteration is layered.
  // Instead of using 'classic' pyramids, I use a b-spline-based
  // image pyramid. The pyramid levels of this pyramid type are continuous
  // functions of real coordinates, which makes off-grid access possible
  // and, together with vspline's functor composition code, leads to a
  // compact, efficient implementation.
  // For the pyramids, I used a 'convolving basis functor' on a cubic
  // b-spline. The spline 'substrate' makes makes for sufficient
  // 'smoothness', and the convolving basis functor is set up with a
  // simple binomial kernel (1/4,1/2,1/4) which reliably removes the
  // Nyquist frequency, but it's not a true half band filter. Still
  // the result seems perfectly good enough, and during the 'collapsing'
  // phase, the 'expanded' upper level of the pyramid is interpolated
  // nicely due to the cubic b-spline, avoiding artifacts of lower-level
  // splines. Once the code was stable, I noticed that I can revert to
  // using a degree-1 spline and area decimation to just about the same
  // effect but with good speed improvements, so that's what the
  // algorithm now uses - the code is flexible and can handle either
  // setting, so I might make this user-selectable.
  // Using spline pyramids - with their scalable decimator - produces
  // another degree of freedom: it makes the scaling step variable,
  // instead of being limited to a step of two. Using scaling steps of
  // less than two can enhance the result.
  // TODO: scaling step != 2 is dodgy with a fixed binomial cbf!
  // TODO: make decimator a parameter
  // The parameter p_canvas can be used to pass in an frgb_image_type
  // object onto which results from this function should be layered.
  // If nullptr is passed, a new frame is allocated. The layering is
  // especially useful for panoramas, where this function is used to
  // create image contributions one by one from the partial images
  // and associated masks: the panorama code does not need a mask
  // normalization stage, so this layer-by-layer approach is possible.

  // new fuse variant for input in associated alpha. This version
  // replaces my initial implementation in full scale. Together with
  // a bug fix in the pyramid building code for full sphericals, this
  // finally does seem to do the trick 'all the way', taking arbitrarily
  // shaped exclude masks and working without 'fill_in'.
  // There seems to be a problem with certain decimators, though:
  // area decimation and some half-band decimators result in artifacts
  // along the edges of the masked area, which occur sporadically,
  // depending on the current view orientation.
  // As opposed to other approaches at blending multiple images, the fuse
  // process here does not proceed by blending one image after the other
  // with the result of the blending process up to then, but produces
  // individual layers for each input image, which are made so that a
  // simple addition produces the result. This makes the process
  // independent of sequence or position of the partials, which is a
  // desirable property. Note how the blending is sensitive to the
  // 'floor' value of the pyramids: if this is very small, the blending
  // has large 'globality', where each partial can affect as much as the
  // entire output area (apart from unmasked parts). High floor values
  // produce low globality, up to the point where there is no more
  // multi-level blending, but instead the partials are simply blended
  // with hard masks. The default is quite 'global', namely a floor value
  // of four. You can experiment with different values by setting
  // exposure_pyramid_floor on the command line. This is akin to setting
  // the number of blending levels in other image fusion software and
  // has the same effect - just the parametrization is different.

  // here we go. The final bit I was missing was the 'double fade out' which
  // happened because the fade-out information was used twice: once as part of
  // the mask and once in the partial's alpha channel. Pulling up image pixels
  // with non-zero alpha to fully opaque did the trick.

  frgba_image_type * fuse ( spline4_type * p_image ,
                            alpha_spline_type * p_quality ,
                            frgba_image_type * p_canvas ,
                            float_pyramid_type & summed_mask_pyramid ,
                            bool ultimate = false )
  {
    frgba_image_type * result = p_canvas ;

    // just to have these two handy:

    deassociate_type da ;
    associate_type a ;

    // the partial image was calculated in aRGBA. The resulting
    // pyramid will have layers of aRGBA data, which are valid
    // image splines with valid transparency. So, around the edges,
    // the partial image may have areas which are semi-transparent.
    // The same information is contained in the mask, which fades
    // out from 1.0 to 0.0. If we were to apply the mask to the
    // partial, we'd 'fade out twice'. So after building the image
    // pyramid, we'll pull up all pixels in the pyramid which aren't
    // totally transparent to fully opaque, and leave it to the
    // corresponding mask values in the quality pyramid to affect
    // the fading out.

    pixel4_pyramid_type image_pyramid ( p_image , bls.scaling_step ,
                                        i_smoothing_level ,
                                        bls.pyramid_floor ,
                                        full_spherical , njobs ) ;

//     if ( p_image->spline_degree > 1 )
//       image_pyramid.restore() ;

    image_pyramid.restore ( 1 , pull_up_alpha() , njobs ) ;

    // number of pyramid levels

    auto psz = image_pyramid.level.size() ;

    // now we 'pull up' semitransparent pixels to fully opaque.
    // TODO: inlining may be faster, even though it would apply the
    // functor more often (namely to each evaluation of a gaussian,
    // which is twice per laplacian) - depends on the cost of memory
    // access vs. application of the functor.
    // TODO: but: it doesn't work inlined. The pull-up has to be
    // applied to the spline coefficients before evaluation. why?

//     for ( int i = 0 ; i < psz ; i++ )
//     {
//       auto p_image = image_pyramid.level[i] ;
//       vspline::apply ( pull_up_alpha() , p_image->container ) ;
//     }

    // for emitting test images

    int imgno = ++counter ;
    auto image_shape = p_image->core.shape() ;

    // build the 'quality' pyramid in the same way. Note that the
    // 'quality' pyramid is float-based, whereas the image pyramid
    // is pixel-based. The shape of both pyramids is the same.

    float_pyramid_type quality_pyramid ( p_quality , bls.scaling_step ,
                                         q_smoothing_level ,
                                         bls.pyramid_floor ,
                                         full_spherical , njobs ) ;

    if ( p_quality->spline_degree > 1 )
      quality_pyramid.restore ( 1 ) ;

    // now 'pull up' the quality values using the information in
    // summed_mask_pyramid. why so? The quality values 'leak' energy
    // to the area which is ouside the combined masks by 'blooming'
    // due to the low-pass. This only affects layers 1..N because the
    // bottom layer is not low-pass-filtered, so luckily we only need
    // to apply this compensation to levels from 1 upwards.
    // TODO: use vspline::transform

    for ( int i = 1 ; i < psz ; i++ )
    {
      auto p_quality = quality_pyramid.level[i] ;
      auto p_bleed = summed_mask_pyramid.level[i] ;

      assert ( p_quality->container.size() == p_bleed->container.size() ) ;

      auto sz = p_quality->container.size() ;
      auto nc = sz / 4096 ;
      if ( nc == 0 )
        nc = 1 ;
      if ( nc > njobs )
        nc = njobs ;

      anti_bleed_t ab ( p_quality->container , p_bleed->container ) ;
      vspline::transform ( ab , p_quality->container , nc ) ;

//       for ( int p = 0 ; p < p_quality->container.size() ; p++ )
//       {
//         // 'bleed compensation'
//         auto & q ( (p_quality->container)[p] ) ;
//         auto & b ( (p_bleed->container)[p] ) ;
//         if ( b > 0.00001f )
//         {
//           q /= b ;
//         }
//       }
    }

    // p_help initially points to a spline with the same size as the
    // top-level splines of the two pyramids. The algorithm proceeds in
    // cycles: the current level of the laplacian pyramid (which is not
    // manifest but calculated on the fly) is weighted with the current
    // level of the quality pyramid, and the result is added to the
    // expanded level above the current level. The result is stored in
    // p_help, to be expanded in the next iteration to be summed up to
    // the next iteration's weighted laplacian...

    spline4_type * p_help = mem_in() << new spline4_type
          ( image_pyramid.level[psz-1]->core.shape() ,
            1 , bcv , -1 , i_headroom ) ;

    // the pyramids hold the splines for the pyramid levels as pointers,
    // to make the code clearer I manifest the pointers as p_..., then
    // create the evaluators from them.
    // the 'tip' level laplacian must be the same as the tip level gaussian.
    // We work in associated RGB, but with all image pixels either fully
    // opaque or fully transparent - the masking will sort out the crossfade.

    auto p_itip = image_pyramid.level[psz-1] ;
    auto ev_itip = make_ev4 ( *p_itip ) ;

    auto p_qtip = quality_pyramid.level[psz-1] ;
    auto ev_qtip = make_aev ( *p_qtip ) ;

    // tip-level laplacian is tip-level gaussian

    auto laplacian = ev_itip ;

    // weighting applies the mask information. See the remarks about wlf_t
    // further down in the cascade code, here we might as well use a plain
    // multiplication to the same effect with no performace penalty: the tip
    // pyramid level typically isn't very large.

    auto weighted_laplacian = wlf_t ( laplacian , ev_qtip ) ;

    // this functor yields the top level for collapsing the laplacian pyramid,
    // which we evaluate into *p_help. We use only one job, assuming that the
    // top level is small. This avoids undue multithreading overhead.

    vspline::transform ( weighted_laplacian , p_help->core , 1 ) ;

    // the resulting spline has to be prefiltered

    if ( full_spherical )
      spherical_prefilter ( *p_help , p_help->core , 1 ) ;
    else
      p_help->prefilter ( 1 , 1 ) ;

    for ( int top = psz - 1 ;
          top > 0 ;
          --top )
    {
      // bottom and top are the two levels involved. Naming is as a
      // pyramid suggests: larger levels are below smaller levels.
      // the pyramids hold the splines for the pyramid levels as pointers,
      // to make the code clearer I manifest the pointers as p_..., then
      // create the evaluators from them.

      auto bottom = top - 1 ;

      // usually we shift the top splines up to get a smooth curve, the
      // degree of shift depends on the parameters i_shift_to and q_shift_to
      // which, in turn, are set by CL parameters bls_i_spline_shift_to and
      // bls_q_spline_shift_to. The { 0 , 0 } parameter in the make_ev4
      // invocation is needed because the spline's derivative has to be
      // passed before the shift, so here we evaluate the plain value, not
      // a derivative. The larger the shift we pick, the smoother the curve
      // will be - but it will also have ever less high frequency content
      // of those high frequencies which 'should' go into the current band.
      // The default of two is a good compromise.

      int ishift = i_shift_to - 1 ;
      int qshift = q_shift_to - 1 ;

      auto p_itop = image_pyramid.level[top] ;
      auto ev_itop = make_ev4 ( *p_itop , { 0 , 0 } , ishift ) ;

      auto p_ibottom = image_pyramid.level[bottom] ;
      auto ev_ibottom = make_ev4 ( *p_ibottom ) ;

      auto p_qbottom = quality_pyramid.level[bottom] ;
      auto ev_qbottom = make_aev ( *p_qbottom , { 0 , 0 } , qshift ) ;

      // we're feeding 'bottom' coordinates (with vspline::transform)
      // so we need a 'domain' to evaluate the top level spline at
      // equivalent positions. As we have two splines, obtaining
      // the domain is especially easy:

      auto dm = domain_t ( *p_ibottom , *p_itop ) ;

      // the laplacian is calculated as aRGBA difference (!)

      auto laplacian = diff4_t ( ev_ibottom , dm + ev_itop ) ;

      // and weighted. The dedicated functor wlf_t exploits the fact that
      // the mask (delivered by evaluating ev_qbottom) is likely to be zero
      // and therefore evaluates the mask first. If it is in fact zero, the
      // laplacian isn't evaluated at all, which saves a great deal of cycles.

      auto weighted_laplacian = wlf_t ( laplacian , ev_qbottom ) ;

      // the next functor yields the expanded 'help'. The values are in
      // aRGBA and constitute the result of collapsing the pyramid down to
      // the current top level. By prefixing the domain, we can access the
      // data with bottom level coordinates, whereas the size of the spline
      // in *p_help is that of the current top level. make_ev4 creates an
      // evaluator in aRGBA which interpolates over the data in *p_help,
      // in just the same way as we interpolated the higher of the two
      // gaussian levels to form the laplacian - the expansion has to be
      // done in precisely the same way.
  
      auto qev_top = dm + make_ev4 ( *p_help , { 0 , 0 } , ishift ) ;

      // the final stage adds the values from the expanded top level
      // with the current (bottom) weighted laplacian. If we were to
      // evaluate this functor, it would preduce the 'image so far',
      // a composite of the frequency bands up to what is currently
      // in the 'bottom' level. wl_plus_above is short for 'weighted
      // laplacian plus (expanded) collapse of the levels above'

      auto wl_plus_above = sum4_t ( weighted_laplacian , qev_top ) ;

      // first tweak number of threads. If the pyramid level is quite small,
      // multithreading overhead may make it counterproductive
      // TODO informed guess, find good heuristic

      auto sz = p_help->core.size() ;
      auto nc = sz / 4096 ;
      if ( nc == 0 )
        nc = 1 ;
      if ( nc > njobs )
        nc = njobs ;

      // the final distinction is where to put the resulting data. If
      // we've reached level 0, we don't write to *p_help but instead
      // straight to 'result': if we're processing the first partial
      // image, we initialize 'result', otherwise we add to what's
      // already there.

      if ( bottom > 0 )
      {
        // We're not yet at level 0.
        // first we stash p_help, which holds the spline with the result of
        // the pyramid collapse up til now. We assign a new spline to p_help
        // to accomodate the result of the current iteration, but of course
        // we still need the previous content as input for the current
        // iteration (it's accessed by wl_plus_above). We'll discard it once
        // we have filled the current incarnation of *p_help

        auto p_backup = p_help ;

        // set up the spline for the 'next incarnation'

        p_help = mem_in() << new spline4_type
          ( p_ibottom->core.shape() , 1 , bcv , -1 , i_headroom ) ;

        // now comes the important step where the functor construct is
        // evaluated into the core of the new 'incarnation' of p_help.
        // After prefiltering, we have the new 'help' for the next
        // iteration.

        vspline::transform ( wl_plus_above , p_help->core ) ;

        if ( full_spherical )
          spherical_prefilter ( *p_help , p_help->core , nc ) ;
        else
          p_help->prefilter ( 1 , nc ) ;

        // Now we can safely discard the previous content of *p_help,
        // which we had stashed above.

        if ( p_backup != nullptr )
          memlog >> p_backup ;
      }
      else
      {
        // set this conditional to true to get snapshots of the partial results:

        if ( false )
        {
          // to export the data, we need unattached RGBA,

          vigra::MultiArray < 2 , pixel4_type > export_view ( image_shape ) ;
          vspline::transform ( wl_plus_above + da , export_view ) ;

          std::string next_name =   "partial_result"
                                  + std::to_string ( imgno )
                                  + ".tif" ;

          vigra::ImageExportInfo imageInfo ( next_name.c_str() );

          vigra::exportImage ( export_view ,
                               imageInfo
                               .setPixelType("UINT16")
                               .setCompression("100")
                               .setForcedRangeMapping ( 0.0 , 255.0 , 0 , 65535 ) ) ;
        }

        // 'bottom' is now level 0, the final transformation yields the
        // 'contribution' derived from the current image, which is used
        // to either initialize or augment the result. This is the most
        // efficient way of dealing with the issue - rather than storing
        // an intermediate image, we direct the functor's output to
        // it's final destination.

        if ( result == nullptr )
        {
          // if there is no 'result' yet to add to, we're dealing with the
          // first partial image. Allocate memory to hold the result.

          result = mem_in() << new
            frgba_image_type ( p_image->core.shape() ) ;

          // use the very first level-0 result to initialize the result.
          // if this is also the last result, it's a degenerate case, but
          // we should handle it gracefully as well.

          if ( ultimate )
          {
            auto & summed_mask ( summed_mask_pyramid.level[0]->core ) ;

            auto final_mask = dub_alpha ( wl_plus_above + deassociate_type() ,
                                          yield1_t ( summed_mask ) ,
                                          255.0f ) ;

            vspline::transform ( final_mask , *result , nc ) ;
          }
          else
          {
            vspline::transform ( wl_plus_above , *result , nc ) ;
          }
        }
        else
        {
          // further level-0 results are always added to 'result'.
          // We 'extend' wl_plus_above by forming a summation functor
          // from what's currently in 'result' and wl_plus_above.
          // This functor yields the desired sum.

          auto final_sum = sum4_t ( near4_t ( *result ) , wl_plus_above ) ;

          // if this is the last partial image to be added to 'result',
          // we append the application of the bottom level of the summed
          // mask pyramid, which has alpha values for the final image.
          // otherwise we just form the sum.

          if ( ultimate )
          {
            auto & summed_mask ( summed_mask_pyramid.level[0]->core ) ;

            auto final_mask = dub_alpha ( final_sum + deassociate_type() ,
                                          yield1_t ( summed_mask ) ,
                                          255.0f ) ;

            vspline::transform ( final_mask , *result , nc ) ;
          }
          else
          {
            vspline::transform ( final_sum , *result , nc ) ;
          }
        }
      }
    }

    // discard the contents of p_help, we're done with the data.

    memlog >> p_help ;

    // result points to what we've accreted so far

//     std::cout << "fuse is done, returning" << std::endl ;

    // result points to what we've accreted so far, and we return it
    // to the caller.

    return result ;
  }

  // if the view only has zero or one active facets, we don't have to
  // do any blending. For zero facets, we return a black frame, and for
  // one facet, we simply render the job and process the result.

  void handle_low_facet_count()
  {
    if ( nactive == 0 )
    {
      std::cout << "handle_low_facet_count, nactive == 0" << std::endl ;
      // if there are no active facets, simply produce an all-black image.

      if ( bls.routing == ROUTE_TO_FRAME )
      {
        // the caller will take care of the data attached at dock.p_frgb

        if ( frame.p_itp->mode == WITH_ALPHA )
        {
          auto p_black = mem_in()
            << new frgba_image_type ( image_shape , pixel4_type ( 0.0f ) ) ;
          p_job->dock.p_frgba = p_black ;
        }
        else
        {
          auto p_black = mem_in()
            << new frgb_image_type ( image_shape , pixel_type ( 0.0f ) ) ;
          p_job->dock.p_frgb = p_black ;
        }
      }
      else if ( bls.filename == std::string() )
      {
        auto p_rgba = p_job->frame.p_frame ;
        assert ( p_rgba != nullptr ) ;
        assert ( p_rgba->shape() == image_shape ) ;
        p_rgba = { 0u } ;
        p_job->ready = pv_clock::now() ;

        // push this job to the frame queue

        push_frame ( p_job ) ;
      }
      else if ( frame.p_itp->mode == WITH_ALPHA )
      {
        frgba_image_type black ( image_shape , pixel4_type ( 0.0f ) ) ;
        store_rendered_image ( p_job ,
                              nullptr ,
                              & black ,
                              false ,
                              bls.filename ) ;

        std::cout << "saved stitched result as "
                  << bls.filename << std::endl ;
      }
      else
      {
        frgb_image_type black ( image_shape , pixel_type ( 0.0f ) ) ;
        store_rendered_image ( p_job ,
                              & black ,
                              nullptr ,
                              false ,
                              bls.filename ) ;

        std::cout << "saved stitched result as "
                  << bls.filename << std::endl ;
      }
    }
    else if ( nactive == 1 )
    {
      std::cout << "handle_low_facet_count, nactive == 1" << std::endl ;
      // for a single facet, just render the job.

      if ( bls.routing == ROUTE_TO_FRAME )
      {
        // the caller will take care of the data attached at dock.p_frgb

//         if ( frame.p_itp->mode == WITH_ALPHA )
        if ( frame.format == FRAME_FRGBA )
        {
          auto p_frame = mem_in()
            << new frgba_image_type ( image_shape , pixel4_type ( 0.0f ) ) ;
//           frame.format = FRAME_FRGBA ;
          frame.p_frgba = p_frame ;
          frame.p_itp->process_job ( p_job ) ;
          local_dispatch.rgba2srgba ( *p_frame , p_job->njobs ) ;
          frame.p_frgba = nullptr ;
          p_job->dock.p_frgba = p_frame ;
        }
        else
        {
          auto p_frame = mem_in()
            << new frgb_image_type ( image_shape , pixel_type ( 0.0f ) ) ;
//           frame.format = FRAME_FRGB ;
          frame.p_frgb = p_frame ;
          frame.p_itp->process_job ( p_job ) ;
          local_dispatch.rgb2srgb ( *p_frame , p_job->njobs ) ;
          frame.p_frgb = nullptr ;
          p_job->dock.p_frgb = p_frame ;
        }
      }
      else if ( bls.filename == std::string() )
      {
        frame.format = FRAME_RGBA8 ;
        frame.light_settings.apply_gradation = bls.process_linear ;
        frame.light_settings.cap_brightness = true ;

        frame.p_itp->process_job ( p_job ) ;
        p_job->ready = pv_clock::now() ;

        // push this job to the frame queue

        push_frame ( p_job ) ;
      }
      else if ( frame.p_itp->mode == WITH_ALPHA )
      {
        frgba_image_type image ( image_shape ) ;
        frame.format = FRAME_FRGBA ;
        frame.p_frgba = & image ;
        frame.p_itp->process_job ( p_job ) ;
        frame.p_frgba = nullptr ;

        store_rendered_image ( p_job ,
                              nullptr ,
                              & image ,
                              bls.process_linear ,
                              bls.filename ) ;

        std::cout << "saved stitched result as "
                  << bls.filename << std::endl ;
      }
      else
      {
        frgb_image_type image ( image_shape ) ;
        frame.format = FRAME_FRGB ;
        frame.p_frgb = & image ;
        frame.p_itp->process_job ( p_job ) ;
        frame.p_frgb = nullptr ;

        store_rendered_image ( p_job ,
                              & image ,
                              nullptr ,
                              bls.process_linear ,
                              bls.filename ) ;

        std::cout << "saved stitched result as "
                  << bls.filename << std::endl ;
      }
    }
  }

  // overload of side_exit for stitching jobs

  template < typename image_type >
  bool side_exit ( image_type * result ,
                   job_type * p_job )
  {
    // take the side exit only if 'aborted' is set and the job is
    // rendering for on-screen display

    if ( aborted && ( bls.filename == std::string() ) )
    {
      if ( result != nullptr )
        memlog >> result ;

      cleanup ( p_job ) ;
      return true ;
    }
    return false ;
  }

  // overload of side_exit for exposure fusion jobs

  template < typename image_type ,
             typename spline_type ,
             typename alpha_spline_type >
  bool side_exit ( image_type * result ,
                   job_type * p_job ,
                   std::vector < spline_type * > & iv ,
                   std::vector < alpha_spline_type * > & qv )
  {
    // take the side exit only if 'aborted' is set and the job is
    // rendering for on-screen display

    if ( aborted && ( bls.filename == std::string() ) )
    {
      for ( spline_type * p_spline : iv )
      {
        if ( p_spline )
          memlog >> p_spline ;
      }

      for ( alpha_spline_type * p_spline : qv )
      {
        if ( p_spline )
          memlog >> p_spline ;
      }

      if ( result != nullptr )
        memlog >> result ;

      cleanup ( p_job ) ;
      return true ;
    }
    return false ;
  }

  // overload of side_exit for exposure fusion jobs with alpha channel

  template < typename image_type ,
             typename spline_type ,
             typename alpha_spline_type >
  bool side_exit ( image_type * result ,
                   job_type * p_job ,
                   std::vector < spline_type * > & iv ,
                   std::vector < alpha_spline_type * > & qv ,
                   std::vector < float_array_type * > & av )
  {
    // take the side exit only if 'aborted' is set and the job is
    // rendering for on-screen display

    if ( aborted && ( bls.filename == std::string() ) )
    {
      for ( spline_type * p_spline : iv )
      {
        if ( p_spline )
          memlog >> p_spline ;
      }

      for ( alpha_spline_type * p_spline : qv )
      {
        if ( p_spline )
          memlog >> p_spline ;
      }

      for ( float_array_type * p_array : av )
      {
        if ( p_array )
          memlog >> p_array ;
      }

      if ( result != nullptr )
        memlog >> result ;

      cleanup ( p_job ) ;
      return true ;
    }
    return false ;
  }


  // perform an image stitch from a set of 'partial' images derived from
  // the baseline job.
  // Here's the strategy: We start out by generating 'mask' information.
  // For every partial image, a mask is calculated which is used as the
  // 'quality' criterion in the image splining algorithm. For stitching,
  // the masks are spatial and cover the parts of the partial images
  // which would be seen *in the live stitch*. So lux uses no 'seam
  // optimization' but instead relies on the 'ranking' information
  // which it also uses to determine which part of which partial image
  // is displayed in synoptic views which don't use image splining.
  // The ranking code readily produces 'weights' which are, at every
  // pixel position, a partition of unity, hence the masks we obtain
  // in this fashion needn't be normalized. While we might hope to
  // be able to produce a mask and a partial image for each 'fuse'
  // iteration, in my current implementation this turned out insufficient;
  // I found I needed the sum of all masks in the fusion code, to avoid
  // artifacts in stitches where the partials do not cover the entire
  // canvas. So the masks are all calcualted in the beginning, their sum
  // is formed, and only then the 'next stage' is reached:
  // Now, the partial images are rendered one after the other, and as
  // soon as a new partial is available, it's sent to the fusion code,
  // to generate a layer of the final image. This layer is made so that
  // all the layers can simply be added up to yield the final result.
  // This strategy allows us to discard the partial image after it's
  // been layered onto the final canvas, reducing memory load. This
  // method differs from enblend's method, where the result-so-far
  // and the next-partial-in-line are blended as two separate images,
  // forming the new result-so-far - the layers we produce here are
  // independent of each other, and their sequence is irrelevant.
  // In my switch to associated-alpha processing, I figured out that
  // the formulation of the code is straightforward using transparency;
  // even partial images which don't intrinsically have alpha information
  // have transparent parts as well, namely those areas where they don't
  // 'cover' the canvas. Additionally, I added processing of PTO masks
  // and source image cropping - and, additionally, facet boundary
  // fade-out, which is a lux feature. All of these are done by alpha
  // channel manipulations. So now all stitching is done with the
  // alpha-aware code, which simplifies matters because there is no
  // longer a non-alpha version to be maintained, and some rather
  // unwieldy constructs (like the 'elevation' of facets) could go.
  // The last novelty in the code is stitching of panoramas with
  // stacks. Animated views and stills without snap-to-stitch only
  // ever show the 'stack parent' - the first of the stacked images,
  // which is assumed to be the 'middle exposure'. This is for speed;
  // If such views had to fuse stacks on top of all that needs to
  // be done, the frame rates would drop way too much. But if lux
  // produces a still image and snap-to-stitch is set, it can now
  // fuse the stacks. There are three modes for this, (use --stack=...)
  // 'first' only uses the 'stack parent', like in animated sequences,
  // 'fusion' (the default) exposure-fuses the stacks, and 'hdr'
  // HDR-merges them. The latter may produce ungainly results if there
  // are masks on the partials where they show in the final result.
  // When the stacks are exposure-fused, exposure-fusion-related
  // parameters will be honoured. Parameters which are common to both
  // expsoure fusion and stitching will be shared - there is no way
  // currently to use separate values for them.

  void process_stitching_job_alpha()
  {
    // with less than two active facets, we can take a shortcut:

    if ( nactive == 0 )
    {
      handle_low_facet_count() ;
      return ;
    }

    if ( nactive == 1 )
    {
      int nstack = 1 ;
      if ( bls.stacking == STACK_FUSION || bls.stacking == STACK_HDR )
      {
        int i = active_facets[0] ;
        nstack = frame.p_itp->in_stack_facets ( i ) ;
      }
      if ( nstack <= 1 )
      {
        handle_low_facet_count() ;
        return ;
      }
    }

    // blending mode for the partial images

    frame.blending = BLEND_RANKED ;
    frame.format = FRAME_FRGBA ;

    // take note of the light settings used originally for this
    // job. We'll use them for the baseline and partial images.

    auto brightness = frame.light_settings.brighten ;
    auto mother_wb = frame.light_settings.white_balance ;
    auto mother_bp = frame.light_settings.black_point ;
    auto mother_wp = frame.light_settings.white_point ;

    frgba_image_type * result = nullptr ;

    // for this version, we work without an 'anti mask', but we need
    // the sum of all masks. This sum will be used in the fusion
    // subroutine to 'pull up' mask values. We need a pyramid for
    // these values, which we'll build a bit further down, before the
    // first call to 'fuse'.
    // In a way, what we're doing is using a mask with alpha channel;
    // The same effect could be achieved by forming two-channel pixels
    // (grey and alpha) and working them the same way we work RGBA
    // pixels. The grey component would code for the weight of the
    // mask, and the alpha for it's opacity. With such a scheme we could
    // code that there are areas where the mask value is unknown, e.g.
    // in areas where all masks are zero.

    alpha_spline_type * p_summed_mask_spline = mem_in() << new
      alpha_spline_type ( image_shape , q_spline_degree ,
                          bcv , -1 , q_headroom ) ;

    // we use a reference to this spline's core, and we'll add all
    // masks to this MultiArrayView to obtain the sum

    auto & summed_mask = p_summed_mask_spline->core ;

    {
      // scope for B&A algorithm 'proper', the arrays can perish after that.

      // we'll use this RGBA array to obtain mask data, even though we'll
      // only use a single colour channel and the alpha channel.

      frgba_image_type mask4 ( image_shape ) ;

      // because we need the sum of all masks for the 'fuse' subroutine,
      // we first have to obtain all masks and sum them up before we can
      // call fuse for the first time. We'll need the masks later on for
      // the individual calls to fuse (one for each partial image), so we
      // save pointers to them in this vector - note that we don't need to
      // deallocate them, because they will serve as bottom level splines
      // for the quality pyramids and perish together with these pyramids.
  
      std::vector < alpha_spline_type * > maskv ( nactive ) ;

      for ( int a = 0 ; a < nactive ; a++ )
      {
        // allocate memory for the current mask's spline

        maskv[a] = mem_in() << new alpha_spline_type
                        ( image_shape , q_spline_degree ,
                          bcv , -1 , q_headroom ) ; 

        // obtain the actual facet number from it's ordinal value

        int i = active_facets [ a ] ;

        frame.mask_for = i ;
        
        // for stacking modes which involve several stack members,
        // we set 'mask_stack' to signal to the rendering code that we
        // need a mask derived from all stack members. This mask will
        // be the union of the masks for all stack members.

        if (    bls.stacking == STACK_FUSION
             || bls.stacking == STACK_HDR )
        {
          frame.mask_stack = i ;
        }

        // render the frame for the mask. For this task, we do want
        // linear response and a brightness range in [0:1], so we
        // *don't* apply 'brightness' etc. for mask generation

        frame.light_settings.brighten = 1.0f ;
        frame.light_settings.black_point = 0.0f ;
        frame.light_settings.white_point = 255.0f ;
        frame.light_settings.white_balance = 1.0f ;
        frame.yield_argba = false ;

        // the mask will be just as large as what would be seen of the
        // partial image in the 'live' stitch, with the image content
        // replaced by white - fading at overlaps due to feathering.
        // It's alpha is meaningful, we'll apply it a bit further down.
        // It's roughly the shape of a voronoi cell.

        frame.p_frgba = & mask4 ;
        frame.p_itp->process_job ( p_job ) ;
        frame.p_frgba = nullptr ;

        frame.mask_stack = -1 ; // false ;

        // extract the mask from the R channel, apply alpha to it.
        // the resulting data in maskv[a]->core will be prefiltered
        // further down the line (as '*p_quality', before fuse() is
        // invoked). For the next step, the creation of the summed mask,
        // we only need the data in the spline's core.

        auto raw_mask = yield4_t ( mask4 ) ;
        vspline::transform ( raw_mask + make_mask_t() , maskv[a]->core ) ;

        // set up a functor to add the current mask to the summed mask

        sum1_t add_mask ( ( yield1_t ( summed_mask ) ) ,
                          ( yield1_t ( maskv[a]->core ) ) ) ;

        // call transform to affect the addition

        vspline::transform ( add_mask , summed_mask ) ;

        // set this conditional to true to export the mask - this is precisely
        // what can be fed to enblend as an externally made mask (use it both
        // for the hard and soft mask) which is helpful when the results of
        // lux and enfuse are to be compared with identical masks. Normally,
        // enblend will create it's own masks, looking at the pre-warped
        // images and performing 'seam optimization'. lux instead works from
        // the 'draped' images in model space and uses geometry-based ranking
        // to come up with a mask resembling a Voronoi cell. This avoids seam
        // optimization and will therefore create a different result. If enblend
        // is invoked with the masks which lux uses, the results become more
        // comparable.

        if ( false )
        {
          std::string next_name =   "hardmask-"
                                  + std::to_string ( a + 1 )
                                  + ".tif" ;
    
          vigra::ImageExportInfo imageInfo ( next_name.c_str() );
    
          vigra::exportImage (  maskv[a]->core ,
                                imageInfo
                                .setPixelType("UINT16")
                                .setCompression("100")
                                .setForcedRangeMapping ( 0.0 , 1.0 , 0 , 65535 ) ) ;
        }
      }

      // we have the summed mask in summed_mask (aka p_summed_mask->core),
      // now we prefilter this spline

      if ( full_spherical )
      {
        spherical_prefilter ( *p_summed_mask_spline , summed_mask , njobs ) ;
      }
      else
      {
        p_summed_mask_spline->prefilter ( 1 , njobs ) ;
      }

      // and now the we create the 'summed mask' pyramid:

      float_pyramid_type summed_mask_pyramid ( p_summed_mask_spline ,
                                               bls.scaling_step ,
                                               q_smoothing_level ,
                                               bls.pyramid_floor ,
                                               full_spherical ,
                                               njobs ) ;

      // if the pyramid was built with spline degree greater than one,
      // we need to 'restore' it to degree one, aka plain image data.

      if ( q_spline_degree > 1 )
        summed_mask_pyramid.restore() ;

      // another place where we can fork out an imgae. To get the
      // summed mask, set this conditional to true.

      if ( false )
      {
        // To doublecheck on the summed mask (it's assumed that
        // the sum is precisely 1.0 throughout) you can uncomment
        // this code:

//         float vmin = 100.0f ;
//         float vmax = -100.0f ;
//         for ( int p = 0 ; p < summed_mask.size() ; p++ )
//         {
//           vmin = std::min ( vmin , summed_mask[p] ) ;
//           vmax = std::max ( vmax , summed_mask[p] ) ;
//         }
//         std::cout << "****** summed mask min " << vmin
//                   << " max " << vmax << std::endl ;

        std::string next_name = "summed_mask.tif" ;
  
        vigra::ImageExportInfo imageInfo ( next_name.c_str() );
  
        vigra::exportImage (  summed_mask ,
                              imageInfo
                              .setPixelType("UINT16")
                              .setCompression("100")
                              .setForcedRangeMapping ( 0.0 , 3.0 , 0 , 65535 ) ) ;
      }

      for ( int a = 0 ; a < nactive ; a++ )
      {
        // we halt proceedings briefly to check if the user has maybe affected
        // a premature abort of the blending job, in which case we deallocate
        // dynamic memory held in this scope and return to the caller.

        if ( side_exit ( result , p_job ) )
        {
          for ( auto p_mask : maskv )
          {
            if ( p_mask != nullptr )
              memlog >> p_mask ;
          }
          return ;
        }

        // if there was no premature abort, we now proceed to create the partial
        // images one by one and process them into layers to be added up to
        // the final blended result.
        // partial image data will be held in this spline. It's a spline with
        // degree i_spline_degree when it is 'fed' to the pyramid-building
        // code; after the decimation it is 'restored' to degree 1.
        // I found that using degrees greater than one is problematic when
        // hard discontinuities occur. I suspect that the spline overshoots
        // (or undershoots) in places, and if this happens in the 'wrong'
        // place, this can produce a cascading effect culminating in
        // localized ringing artifacts. So the default for i_spline_degree
        // (and q_spline_degree) is 1 - the values can be changed via the CL,
        // but the results for values above one may exhibit the unwanted
        // behaviour. Oftentimes the result seems okay, but if the view
        // is oriented in just one particular direction, the errors occur,
        // so the 'okay' results are treacherous. The flaws are usually
        // where there are hard discontinuities due to masking.

        spline4_type * p_image_spline = mem_in() << new spline4_type
              ( image_shape , i_spline_degree , bcv , -1 , i_headroom ) ;

        // partial is a shorthand for the 'core' of this spline:

        auto partial = p_image_spline->core ;
 
        // now take a snapshot of the individual facet, to get a
        // 'partial image'. First unset 'mask_for' and 'mask_stack'
        // in the job:

        frame.mask_for = -1 ;
        frame.mask_stack = false ;

        // get the actual facet number instead of it's ordinal value.
        // Note that the job that we're in currently is the stitching job
        // triggered by the caller, and this job has only marked those
        // facets as 'active' which are stack parents. So every value
        // which i assumes is the facet number of a stack parent.

        int i = active_facets [ a ] ;

        // we test how many facets are in the stack. If there is only
        // one facet, we can route to single-image code. Otherwise it's
        // a true stack and we call a fusion job to fuse it. The fusion
        // code can also deal with stacks which only hold one image, but
        // processing then goes via a separate pixel array and is therefore
        // more memory-intensive - processing single images separately
        // is faster. If the stacking mode is STACK_FIRST, we also employ
        // the single-image route.

        int nstack = frame.p_itp->in_stack_facets ( i ) ;

        // just to make sure our logic isn't flawed:

        assert ( nstack > 0 ) ;

        bool partial_is_srgba ;

        // now we generate the partial image for the current stack, which
        // depends on the stacking mode (bls.stacking)

        if ( nstack == 1 || bls.stacking == STACK_FIRST )
        {
          // we 'solo' the current facet. This results in a partial image which
          // holds the source image 'warped' to the target projection. Where the
          // source image can't provide content the result will be 4X0.

          frame.solo = i ;
          frame.p_frgba = & partial ;

          // render the frame for the partial image. render in linear aRGBA.

          frame.light_settings.brighten = brightness ;
          frame.light_settings.black_point = mother_bp ;
          frame.light_settings.white_point = mother_wp ;
          frame.light_settings.white_balance = mother_wb ;
          frame.yield_argba = true ;
          frame.p_itp->process_job ( p_job ) ;
          frame.p_frgba = nullptr ;

          // end the current facet's 'solo'

          frame.solo = -1 ;
          frame.yield_argba = false ;

          partial_is_srgba = false ;

          // set this conditional to true to get the partial image

          if ( false )
          {
            std::string next_name =   "stack_first_"
                                    + std::to_string ( i )
                                    + ".tif" ;
            frgba_image_type expo ( image_shape ) ;
            vspline::transform ( deassociate_type() , partial , expo ) ;
            vigra::ImageExportInfo imageInfo ( next_name.c_str() );

            vigra::exportImage (  expo ,
                                  imageInfo
                                  .setPixelType("UINT16")
                                  .setCompression("100")
                                  .setForcedRangeMapping ( 0.0 , 255.0 , 0 , 65535 ) ) ;
          }
        }

// initially I used the HDR fusion code which is also used for 'live'
// viewing to create HDR partials from stacks. But this code is pixel-based
// and can produce small, but annoying artifacts, especially if the stack
// images aren't fitting well. Now I route the HDR fusion to a sepearte
// exposure fusion job with adapted parametrization, which generates an
// HDR image with 'multilevel blending', which is better and does not
// suffer of the artifacts occuring with the pixel-based code.
// but: TODO: the fusion code is working on sRGB data, and it might be better
// to do an HDR fusion on linear data. The current process using sRGB data
// does produce pleasant results, but it may not be 'technically' optimal,
// i.e. the best approximation of the scene illuminant. When using linear
// data, one might consider adapting the hdr fitness curve. An alternative
// to sRGB would be to work in a logarithmic representation.

//         else if ( bls.stacking == STACK_HDR )
//         {
//           // STACK_HDR (--stack=hdr) means 'HDR-merge the stack'.
// 
//           frame.format == FRAME_FRGBA ;
//           auto bup_blending = frame.blending ;
//           frame.blending = BLEND_HDR ;
// 
//           // frame.stack_only and frame.solo will affest the hdr-merging
//           // job we're about to launch, so that this job will process only
//           // images from the current stack. The relevant code is in the
//           // c'tor of multi_facet_helper_type in pv_rendering.cc.
// 
//           frame.stack_only = i + 1 ;
//           frame.solo = -1 ;
// 
//           frame.light_settings.brighten = brightness ;
//           frame.light_settings.black_point = mother_bp ;
//           frame.light_settings.white_point = mother_wp ;
//           frame.light_settings.white_balance = mother_wb ;
// 
//           // producting the HDR-merged partial image is a 'simple' job,
//           // it can't abort prematurely. Sow we just call process_job
//           // and, afterwards, we have the HDR-merged frame in 'partial'.
// 
//           frame.p_frgba = & partial ;
//           frame.p_itp->process_job ( p_job ) ;
//           frame.p_frgba = nullptr ;
// 
//           frame.blending = bup_blending ;
//           frame.stack_only = 0 ;
// 
//           partial_is_srgba = false ;
// 
//           // set this conditional to true to get the partial image
// 
//           if ( false )
//           {
//             std::string next_name =   "stack_hdr_"
//                                     + std::to_string ( i )
//                                     + ".tif" ;
// 
//             vigra::ImageExportInfo imageInfo ( next_name.c_str() );
// 
//             vigra::exportImage (  partial ,
//                                   imageInfo
//                                   .setPixelType("UINT16")
//                                   .setCompression("100")
//                                   .setForcedRangeMapping ( 0.0 , 255.0 , 0 , 65535 ) ) ;
//           }
// 
//           // incoming is in RGBA, we need to associate alpha
// 
//           vspline::apply ( associate_type() , partial ) ;
//         }
        else if ( bls.stacking == STACK_FUSION || bls.stacking == STACK_HDR )
        {
          // if the stacking mode is 'fusion' or 'hdr', the partial image
          // is rendered as an exposure fusion. this requires a separate
          // invocation of the exposure fusion routine (the local dispatch
          // further down), and it's the most elaborate (and time-consuming)
          // processing mode for stacks. Because we simply overwrite a few
          // relevant member variables in the job_type object, we make backups
          // here and reset the member variables after the fusion job returns.
          // TODO: may now be unnecessary

          auto bup = bls.routing ;
          bls.routing = ROUTE_TO_FRAME ;
          auto bup_blending = frame.blending ;

          frame.blending = BLEND_HDR ;
          frame.format = FRAME_FRGBA ;
          frame.light_settings.brighten = brightness ;
          frame.light_settings.black_point = mother_bp ;
          frame.light_settings.white_point = mother_wp ;
          frame.light_settings.white_balance = mother_wb ;

          // frame.stack_only and frame.solo will affest the exposure fusion
          // job we're about to launch, so that this job will process only
          // images from the current stack. The relevant code is in the
          // c'tor of multi_facet_helper_type in pv_rendering.cc.

          frame.stack_only = i + 1 ;
          frame.solo = -1 ;

          // now we call the exposure fusion code. The frame datum
          // 'fuse_for_hdr' will tell the expsoure fusion code whether to
          // blend the stack as expsoure fusion or HDR merge.

          float hdr_spread_bup = frame.light_settings.hdr_spread ;
          if ( bls.stacking == STACK_HDR )
          {
            // stack-hdr sets hdr_spread to 1.0, fusing the stack to
            // contain the full dynamic range of the bracket.
            // If bls.stacking is STACK_FUSION, the command line
            // parameter hdr_spread is used, which alow the entire
            // range from zero to one.

            frame.light_settings.hdr_spread = 1.0 ;
          }
//           else
//           {
//             frame.fuse_for_hdr = false ;
//           }

          // launch the sub-job, and receive the result in the job
          // objects's 'dock' area

          local_dispatch.process_fusion_job ( p_job ) ;

//           frame.fuse_for_hdr = false ;
          frame.light_settings.hdr_spread = hdr_spread_bup ;

          if ( p_job->dock.p_frgba == nullptr )
          {
            // the splining code may abort prematurely, in which case
            // the slot in the dock contains nullptr. Now we need to
            // deallocate memory we've allocated so far.

            for ( auto p_mask : maskv )
            {
              if ( p_mask != nullptr )
                memlog >> p_mask ;
            }
            memlog >> p_image_spline ;
            if ( result != nullptr )
              memlog >> result ;

            // Deallocation done, we can also abort prematurely.

            return ;
          }

          // we're good, copy the baseline image from the dock and
          // clean up, restore bls.routing

          partial = *(p_job->dock.p_frgba) ;
          memlog >> p_job->dock.p_frgba ;
          p_job->dock.p_frgba = nullptr ;
          bls.routing = bup ;
          frame.blending = bup_blending ;
          frame.stack_only = 0 ;

          // set this conditional to true to get the partial image

          if ( false )
          {
            std::string next_name =   "stack_fused_"
                                    + std::to_string ( i )
                                    + ".tif" ;

            vigra::ImageExportInfo imageInfo ( next_name.c_str() );

            vigra::exportImage (  partial ,
                                  imageInfo
                                  .setPixelType("UINT16")
                                  .setCompression("100")
                                  .setForcedRangeMapping
                                    ( 0.0 , 255.0 , 0 , 65535 ) ) ;
          }

          // incoming is in sRGBA, we need associated alpha

          partial_is_srgba = true ;
          vspline::apply ( associate_type() , partial ) ;
        }
        else
        {
          // this should not happen:

          std::cerr << "fatal: unknown stack processing mode" << std::endl ;
          exit ( -1 ) ;
        }

        if ( bls.process_linear && ( partial_is_srgba == false ) )
        {
          // process_linear is normally set for facet maps,
          // so the if... is just a precaution. We use the 'true'
          // RGB->sRGB conversion, which does not use linear interpolation
          // (which the 'fast' method uses) and is not limited to [0:255],
          // so we can safely process brighter values as well.
          // The blending can also be done in linear RGBA, but I find there
          // are more unwanted artifacts where partials don't agree well
          // when the blend is done in linear RGBA.

          local_dispatch.argba2sargba ( partial , p_job->njobs ) ;
        }

        // now partial has aRGBA data, ready to be processed with the
        // fusion function. Again we can export the image here:

        if ( false )
        {
          // to export the data, we need unattached RGBA,

          frgba_image_type expo ( image_shape ) ;
          vspline::transform ( deassociate_type() , partial , expo ) ;

          std::string next_name =   "partial-"
                                  + std::to_string ( a + 1 )
                                  + ".tif" ;
    
          vigra::ImageExportInfo imageInfo ( next_name.c_str() );
    
          vigra::exportImage ( expo ,
                               imageInfo
                               .setPixelType("UINT16")
                               .setCompression("100")
                               .setForcedRangeMapping
                                 ( 0.0 , 255.0 , 0 , 65535 ) ) ;
        }

        // we already have the 'quality' spline in the spline maskv[a]
        // points to, but we haven't yet prefiltered it, so we do that
        // now, as we're also prefiltering the image spline

        auto p_quality = maskv[a] ;

        // maskv[a] will be deallocated as bottom-level spline in the
        // quality pyramid. Set it to zero here in case there is a side
        // exit, which would, otherwise, try to deallocate it again

        maskv[a] = nullptr ;

        if ( full_spherical )
        {
          spherical_prefilter ( *p_image_spline , partial , njobs ) ;
          spherical_prefilter ( *p_quality , p_quality->core , njobs ) ;
        }
        else
        {
          p_image_spline->prefilter ( 1 , njobs ) ;
          p_quality->prefilter ( 1 , njobs ) ;
        }

        // now we call fuse to either create the first contributing
        // layer, or to layer on top of what we already have.
        // We have the image data in *p_image_spline, in aRGBA,
        // and the mask data in *p_quality. Both are b-splines of the same
        // shape, and result is either nullptr (on the first call) or
        // a pointer to a pixel4_type array, again of same shape.
        // The summed_mask_pyramid has data to 'pull up' quality values,
        // which can only be done once the quality pyramid is built in
        // 'fuse', so we pass it as well. The last parameter tells 'fuse'
        // whether this is the last partial image to be blended in or not.
        // When the last image is blended in, the summed mask is applied
        // (gleaned from the bottom level of summed_mask_pyramid) to the
        // final result.

        result = fuse ( p_image_spline , p_quality ,
                         result , summed_mask_pyramid ,
                         a == nactive - 1 ) ;

        // now we have the current partial result, or 'contribution', either
        // as the initial content of 'result', or layered on top of what
        // it already contained.
      }
    }

    if ( false )
    {
      // to export the data, we need unattached RGBA,

//       frgba_image_type expo ( image_shape ) ;
//       vspline::transform ( deassociate_type() , partial , expo ) ;

      std::string next_name =   "stitching_result.tif" ;

      vigra::ImageExportInfo imageInfo ( next_name.c_str() );

      vigra::exportImage ( *result ,
                            imageInfo
                            .setPixelType("UINT16")
                            .setCompression("100")
                            .setForcedRangeMapping
                              ( 0.0 , 255.0 , 0 , 65535 ) ) ;
    }

    // and that's us done - what is left to do is to dispose of the
    // result, depending on the requested destination.

    p_job->ready = pv_clock::now() ;

    if ( bls.routing == ROUTE_TO_FRAME )
    {
      // the caller will take care of the data attached at dock.p_frgb

      p_job->dock.p_frgba = result ;
    }
    else if ( bls.filename == std::string() )
    {
      // this path is taken if the result is meant for on-screen display.
      // Let's see if the user has called for a premature abort, in which
      // case we can stop here

      if ( side_exit ( result , p_job ) )
        return ;

      // no filename given. This means we're processing a job for display.
      // we can assume that we have a valid 8-bit RGBA frame ready to accept
      // the data:
      
      auto p_rgba = p_job->frame.p_frame ;
      assert ( p_rgba != nullptr ) ;
      assert ( p_rgba->shape() == image_shape ) ;
      p_job->frame.format = FRAME_RGBA8 ;

      // the frame is in sRGBA, so if screen_needs_srgb is off
      // we need to convert to linear light to display it right.

      if ( ! frame.screen_needs_srgb )
      {
        local_dispatch.srgba2rgba ( *result , p_job->njobs ) ;
      }

      // move the float RGBA data to the 8-bit RGBA frame
      
      to_screen ( result , p_rgba ) ;
  
      memlog >> result ;

      // push this job to the frame queue

      push_frame ( p_job ) ;
    }
    else
    {
      // this path is taken for snapshots, creating an image file.
      // store the result and free it's memory. If ui::process_linear
      // were false here, the frame would be stored as-is for sRGB
      // output and converted to linear RGB for linear output.

      store_rendered_image ( p_job ,
                             nullptr ,
                             result ,
                             false ,
                             bls.filename ) ;

      memlog >> result ;

      std::cout << "saved stitched result as "
                << bls.filename << std::endl ;
    }
  }

  int counter = 0 ;

  // exposure fusion of images with alpha channel, new implementation
  // using aRGBA data. This function is now used both for images with
  // and without alpha channel - for images without alpha channel a
  // fully opaque alpha channel is added. As for the stitching code,
  // the reduction to one common function makes maintainance and
  // further development easier.

  void process_fusion_job_alpha()
  {
    // with less than two facets, we can take a shortcut.

    if ( bls.mode != FAUX_BRACKET && nactive == 0 )
    {
      handle_low_facet_count() ;
      return ;
    }

    if ( nactive == 1 )
    {
      int nstack = 1 ;
      if ( bls.stacking == STACK_FUSION || bls.stacking == STACK_HDR )
      {
        int i = active_facets[0] ;
        nstack = frame.p_itp->in_stack_facets ( i ) ;
      }
      if ( bls.mode != FAUX_BRACKET && nstack <= 1 )
      {
        handle_low_facet_count() ;
        return ;
      }
    }

    auto brightness = frame.light_settings.brighten ;
    frame.format = FRAME_FRGBA ;
    frame.tonemap = false ;
    float hdr_spread = frame.light_settings.hdr_spread ;
    assert ( hdr_spread >= 0.0f && hdr_spread <= 1.0f ) ;

    // first, create a 'baseline' image. This will be the image
    // used for faux bracketing, so this only needed for faux brackets

    frgba_image_type baseline_image ( image_shape ) ;
    frame.yield_argba = true ;

    if ( frame.blending == BLEND_RANKED )
    {
      // if the input facet map has blending=ranked, we use image
      // splining to create the baseline image. We need the result
      // of the splining, so we set routing to put the frame into
      // the job's 'dock' area.

      auto bup = bls.routing ;
      bls.routing = ROUTE_TO_FRAME ;

      // now we call the image splining code

      local_dispatch.process_stitching_job ( p_job ) ;

      if ( p_job->dock.p_frgba == nullptr )
      {
        // the splining code may abort prematurely, in which case
        // the slot in the dock contains nullptr
        return ;
      }

      // we're good, copy the baseline image from the dock and
      // clean up, restore bls.routing

      baseline_image = *(p_job->dock.p_frgba) ;
      memlog >> p_job->dock.p_frgba ;
      p_job->dock.p_frgba = nullptr ;
      bls.routing = bup ;

      // incoming is in sRGBA, we need RGBA, and associated alpha

      local_dispatch.srgba2rgba ( baseline_image , p_job->njobs ) ;
      vspline::apply ( associate_type() , baseline_image ) ;
    }
    else if ( bls.mode == FAUX_BRACKET )
    {
      // for faux brackets, we need a baseline image from which we'll
      // derive the several differently exposed partials

      frame.p_frgba = & baseline_image ;
      frame.p_itp->process_job ( p_job ) ;
      p_job->frame.p_frgba = 0 ;
    }

    frame.yield_argba = false ;

    if ( false )
    {
      frgba_image_type expo ( baseline_image.shape() ) ;
      vspline::transform ( deassociate_type() , baseline_image , expo ) ;

      std::string next_name = "baseline.tif" ;

      vigra::ImageExportInfo imageInfo ( next_name.c_str() );

      vigra::exportImage (  expo ,
                            imageInfo
                            .setPixelType("UINT16")
                            .setCompression("100")
                            .setForcedRangeMapping ( 0.0 , 255.0 , 0 , 65535 ) ) ;
    }

    // for exposure fusion, we can't avoid summing up the masks for
    // normalization, so we save the partials in vectors, then normalize
    // the masks and finally proceed to repeatedly call the fusion code.
    // for faux brackets, we pick the size of bls.partial_brightness to
    // determine the number pf partials - for other uses we have a valid
    // figure in nactive, the number of 'active' facets.

    int npartial =   ( bls.mode == FAUX_BRACKET )
                   ? bls.partial_brightness.size()
                   : nactive ;

    // we set up two vectors, one for the partial images and one for
    // the 'quality' arrays. The vectors are made to refer to b-splines,
    // because we'll use them later on to build pyramids, and the
    // pyramid constructors preferably process b-splines.

    std::vector < spline4_type * > iv ( npartial ) ;
    std::vector < alpha_spline_type * > qv ( npartial ) ;

    frgba_image_type * result = nullptr ;

    // In the loops in the next part of the code we need to take good
    // care to keep two types of indices apart: We'll use the index a
    // to index the vectors of partials and quality arrays, whereas
    // the index i will be used for facet numbers.

    for ( int a = 0 ; a < npartial ; a++ )
    {
      if ( side_exit ( result , p_job , iv , qv ) )
        return ;

      // allocate a spline for the image and create a reference to it's
      // core, which will be used to store and manipulate the partial
      // image's data.

      spline4_type * p_image_spline = mem_in() << new spline4_type
          ( image_shape , i_spline_degree , bcv , -1 , i_headroom ) ;

      // set up a reference to the spline's core, which will be used as
      // the target for code which provides partial images

      view4_type & partial_image ( p_image_spline->core ) ;

      // store the pointer to the newly-made spline in the partial
      // image vector at position a.

      iv[a] = p_image_spline ;

      // obtain the facet number 'i' from it's ordinal number 'a'
      // for faux brackets, the two numbers coincide: only one facet
      // is involved, the partials are synthetic derivatives of it.

      int i ;

      if ( bls.mode == FAUX_BRACKET )
        i = a ;
      else
        i = active_facets [ a ] ;

      if ( bls.mode == FAUX_BRACKET )
      {
        // faux bracketing does not produce 'solo' frames but
        // generates 'fake' solo frames from darkened/brightened
        // versions of the baseline image.

        yield4_t yield ( baseline_image ) ;

        if ( bls.partial_brightness [ a ] == 1.0f )
        {
          vspline::transform ( yield , partial_image ) ;
        }
        else
        {
          bias4_functor bias ( bls.partial_brightness [ a ] ) ;
          vspline::transform ( yield + bias , partial_image ) ;
        }

        if ( false )
        {
          frgba_image_type expo ( partial_image.shape() ) ;
          vspline::transform ( deassociate_type() , partial_image , expo ) ;

          std::string next_name =   "fb_partial"
                                  + std::to_string ( i )
                                  + ".tif" ;
    
          vigra::ImageExportInfo imageInfo ( next_name.c_str() );
    
          vigra::exportImage (  expo ,
                                imageInfo
                                .setPixelType("UINT16")
                                .setCompression("100")
                                .setForcedRangeMapping ( 0.0 , 255.0 , 0 , 65535 ) ) ;
        }

      }
      else
      {
        // not a faux bracket. we want 'solo' snapshots of the
        // individual facets.

        frame.solo = i ;
        
        // for exposure fusion, we want 'solo' images
        // with the 'original' brightness, cancelling out the
        // facet_brightness values 'built into' the interpolator,
        // which were applied to produce equally bright partial
        // images for HDR viewing/merging.
        // For HDR merging, we only need to adapt the brightness,
        // because the partials, brought to common intensity
        // values, are precisely what we need to combine.
        // Where the partial image has no content (zero value),
        // the weight will come out zero, and if a target pixel
        // has no partials contributing to it, it will also come
        // out zero.
        // In both cases, we want the overall brightness applied
        // to get the same overall look as the 'live' view.

        float brighten_hdr = 1.0 ;
        float brighten_fusion = bls.partial_brightness [ i ] ;
        float brighten_m = (   hdr_spread * brighten_hdr
                            + ( 1.0f - hdr_spread ) * brighten_fusion ) ;

        frame.light_settings.brighten = brighten_m * brightness ;

        // render the partial image. Once rendering is done, we'll
        // have a partial image, either with common intensity values,
        // for HDR merging, or with original intensity values, for
        // exposure fusion.

        frame.p_frgba = & partial_image ;
        frame.yield_argba = true ;
        frame.p_itp->process_job ( p_job ) ;
        frame.yield_argba = false ;
        frame.p_frgba = 0 ;
        frame.solo = -1 ;

        if ( false )
        {
          frgba_image_type expo ( partial_image.shape() ) ;
          vspline::transform ( deassociate_type() , partial_image , expo ) ;

          std::string next_name =   "fusion_job_partial"
                                  + std::to_string ( i )
                                  + ".tif" ;
    
          vigra::ImageExportInfo imageInfo ( next_name.c_str() );
    
          vigra::exportImage (  expo ,
                                imageInfo
                                .setPixelType("UINT16")
                                .setCompression("100")
                                .setForcedRangeMapping ( 0.0 , 255.0 , 0 , 65535 ) ) ;
        }

      }

      if ( bls.process_linear )
      {
        // process_linear is normally set, but if it's not set,
        // the data are already in sRGB. So convert only if
        // process_linear is true.

        local_dispatch.argba2sargba ( partial_image , p_job->njobs ) ;

        // partials for faux brackets are clamped to normal sRGB range.

        if ( bls.mode == FAUX_BRACKET )
          vspline::apply ( clamp4_t() , partial_image , njobs ) ;
      }

      // prefilter the image spline
      // This is a dicey issue - if we prefilter the spline, we commit to
      // evaluating it with a b-spline evaluator even if the quality measure
      // we want to calculate would 'look at' the un-prefiltered data.
      // To deal with the issue, we might postpone prefiltering until the
      // part of the quality measurement which relies on 'raw' image data
      // has been accomplished. For now, for simplicity, I prefilter
      // unconditionally and require that quality measurement uses
      // b-spline evaluation if the spline's degree is greater than one.

      if ( full_spherical )
      {
        spherical_prefilter ( *p_image_spline , partial_image , njobs ) ;
      }
      else
      {
        p_image_spline->prefilter ( 1 , njobs ) ;
      }

    }

    // now we have all partials stored in the partial image b-splines.
    // next we derive 'quality' information from them. This is the
    // big difference to the stitching code: the stitching code uses
    // spatial information to create masks which are then used as
    // 'quality' measure for the image splining algorithm, whereas
    // here the 'quality' information is derived from the image
    // content - specifically, some measure of each pixel's
    // well-exposedness or local contrast. While the stitching
    // code can rely on lux' 'ranking' code to produce the masks,
    // the stitching code uses dedicated functors and needs
    // normalization of the 'quality' data.

    std::vector < iaev_type > qevv ( npartial ) ;
    std::vector < float_view_type * > qvv ( npartial ) ;

    for ( int a = 0 ; a < npartial ; a++ )
    {
      if ( side_exit ( result , p_job , iv , qv ) )
        return ;

      // obtain the facet number from it's ordinal value

      int i ;

      if ( bls.mode == FAUX_BRACKET )
        i = a ;
      else
        i = active_facets [ a ] ;

      // set up a level-0 b-spline for the quality pyramid

      alpha_spline_type * p_quality = mem_in() << new alpha_spline_type
          ( image_shape , q_spline_degree , bcv , -1 , q_headroom ) ;

      // we postpone prefiltering until the norm has been applied.
      // the pointer to the newly allocated spline is stored in qv.

      qv[a] = p_quality ;

      // we also hold a pointer to the spline's core for convenience

      qvv[a] = &(qv[a]->core) ;

      // now we use a bit of trickery: for exposure fusion, we
      // use a 'ceiling' value of 255.0f, the maximal brightness,
      // for the quality functor. This will produce maximal
      // (well-exposedness) quality for pixels at half this
      // level. For HDR merging, we *adapt* the 'ceiling value
      // to the expected maximum of the given partial: because
      // the partials, for HDR merging, have been darkened or
      // brightened to have the same intensity value where they
      // hold viable data, a 'dark' exposure will be 'pulled up'
      // which results in a higher 'ceiling' because pixels with
      // maximal brightness in the original partial will be pulled
      // up to more than 255.0. A 'bright' exposure on the other
      // hand is darkened, pixels which were overexposed (255 or
      // above) in the original partial will now have lower values,
      // and the ceiling has to be accordingly lower.

      float facet_brightness = bls.partial_brightness [ i ] ;
      float ceiling_hdr = 255.0f / facet_brightness ;
      float ceiling_fusion = 255.0f ;
      float ceiling_m = (   hdr_spread * ceiling_hdr
                          + ( 1.0f - hdr_spread ) * ceiling_fusion ) ;

      // in linear RGB we could now simply multiply 'ceiling_m' with
      // 'brightness' to get the appropriate ceiling value, but here
      // we're in sRGB, so we have to calculate a factor which will
      // produce the corresponding ceiling value in sRGB:

      float max_brite = 255.0f * brightness ;
      float srgb_brite = RGB2sRGB ( max_brite , 255.0f ) / 255.0f ;

      // we apply this factor to ceiling_m to get a ceiling value which
      // matches the maximum brightness of the partial image.

      float ceiling = ceiling_m * srgb_brite ;

      // with the ceiling set correctly, we now have all the information
      // we need to generate the quality functor.

      qevv[a] = quality4_t ( *(iv[a]) ,
                             bls.exposure_weight ,
                             bls.contrast_weight ,
                             bls.exposure_sigma ,
                             bls.exposure_mu ,
                             ceiling ) ;
    }

    // now we have all the quality functors set up.

    // we normalize the weights here, then, in a second step, we apply
    // the alpha channel of each partial image to the corresponding mask,
    // to make the masks fade out just like the content does - the fuse
    // routine will only look at plain RGB. While we're processing the
    // partial image's alpha, we also set the covered area to the max
    // of all alpha values encountered at the position. This ensures
    // proper fade-out when the final masking is applied to the result,
    // and we also need this information for the 'summed mask pyramid'
    // which is passed to the fuse function.

    alpha_spline_type * p_covered = mem_in() << new alpha_spline_type
        ( image_shape , q_spline_degree , bcv , -1 , q_headroom ) ;
    auto & covered_area ( p_covered->core ) ;

    // quick fix: use 'covered_area' as the target for the transform.
    // Currently, the transform does only produce side effects, the
    // target is left unmodified. The code below, which applies alpha
    // to the mask and calculates the covered area, might be inlined
    // to normalized_quality_type for performance.

    normalized_quality_type nqt ( qevv , qvv ) ;
    vspline::transform ( nqt , covered_area ) ;

    // TODO: inline

    for ( int p = 0 ; p < qvv[0]->size() ; p++ )
    {
      auto & pxa ( (iv[0]->core)[p][3] ) ;
      auto & ca ( (*qvv[0])[p] ) ;
      // apply alpha to the mask
      ca *= pxa / 255.0f ;
      // pick the alpha at this spot
      covered_area[p] = pxa / 255.0f ;
    }
    for ( int i = 1 ; i < npartial ; i++ )
    {
      for ( int p = 0 ; p < qvv[i]->size() ; p++ )
      {
        auto & pxa ( (iv[i]->core)[p][3] ) ;
        auto & ca ( (*qvv[i])[p] ) ;
        // apply alpha to the mask
        ca *= pxa / 255.0f ;
        // pick the largest alpha at this spot
        covered_area[p] = std::max ( covered_area[p] , pxa / 255.0f ) ;
      }
    }

    if ( full_spherical )
    {
      spherical_prefilter ( *(p_covered) , p_covered->core , njobs ) ;
    }
    else
    {
      p_covered->prefilter ( 1 , njobs ) ;
    }

    // now we can build the summed mask pyramid. It's use is
    // twofold: it's used for 'bleed compensation' which is needed
    // if the partials don't cover the target canvas completely, and
    // it's base level will be used to provide an alpha channel for
    // the final result.

    float_pyramid_type summed_mask_pyramid ( p_covered ,
                                             bls.scaling_step ,
                                             q_smoothing_level ,
                                             bls.pyramid_floor ,
                                             full_spherical ,
                                             njobs ) ;
    
    if ( q_spline_degree > 1 )
      summed_mask_pyramid.restore() ;

    for ( int a = 0 ; a < npartial ; a++ )
    {
      if ( side_exit ( result , p_job , iv , qv ) )
        return ;

      if ( false )
      {
        std::string next_name =   "quality"
                                + std::to_string ( a )
                                + ".tif" ;
  
        vigra::ImageExportInfo imageInfo ( next_name.c_str() );
  
        vigra::exportImage (  qv[a]->core ,
                              imageInfo
                              .setPixelType("UINT16")
                              .setCompression("100")
                              .setForcedRangeMapping ( 0.0 , 1.0 , 0 , 65535 ) ) ;
      }

      // now apply prefilter to make the quality spline suitable as
      // bottom-level spline of the quality pyramid. The image data
      // were already prefiltered earlier on. Note that the quality
      // data are usually in degree-1 splines, so the 'prefilter'
      // call merely 'braces' them.

      if ( full_spherical )
      {
        spherical_prefilter ( *(qv[a]) , qv[a]->core , njobs ) ;
      }
      else
      {
        qv[a]->prefilter ( 1 , njobs ) ;
      }

      // invoke the fusion code to initialize or layer onto 'result'.
      // This is the core of the image splining operation, where the
      // 'quality' measure is applied to the image data in a pyramid
      // scheme.

      result = fuse ( iv[a] , qv[a] , result ,
                      summed_mask_pyramid , a == npartial - 1 ) ;

      // the splines were 'consumed' by the fuse function, we must set
      // them to nullptr so that a potential side_exit doesn't delete
      // them again

      iv[a] = nullptr ;
      qv[a] = nullptr ;
    }

    if ( false )
    {
      // to export the data, we need unattached RGBA,

//       frgba_image_type expo ( image_shape ) ;
//       vspline::transform ( deassociate_type() , partial , expo ) ;

      int i = active_facets [ 0 ] ;
      std::string next_name =   "fusion_result"
                              + std::to_string ( i )
                              + ".tif" ;

      vigra::ImageExportInfo imageInfo ( next_name.c_str() );

      vigra::exportImage ( *result ,
                            imageInfo
                            .setPixelType("UINT16")
                            .setCompression("100")
                            .setForcedRangeMapping
                              ( 0.0 , 255.0 , 0 , 65535 ) ) ;
    }

    // and that's us done! What's left to do is to dispatch the
    // final image to the required destination.

    p_job->ready = pv_clock::now() ;

    if ( bls.routing == ROUTE_TO_FRAME )
    {
      // the caller will take care of the data attached at dock.p_frgb

      p_job->dock.p_frgba = result ;
    }
    else if ( bls.filename == std::string() )
    {
      // no filename given. This means we're processing a job for display.
      // we can assume that we have a valid 8-bit RGBA frame ready to accept
      // the data:
      
      auto p_rgba = p_job->frame.p_frame ;
      assert ( p_rgba != nullptr ) ;
      assert ( p_rgba->shape() == image_shape ) ;
      p_job->frame.format = FRAME_RGBA8 ;

      // the frame is in sRGBA, so if screen_needs_srgb is off
      // we need to convert to linear light to display it right.

      if ( ! frame.screen_needs_srgb )
      {
        local_dispatch.srgba2rgba ( *result , p_job->njobs ) ;
      }

      // move the float RGBA data to the 8-bit RGBA frame
      
      to_screen ( result , p_rgba ) ;

      // push this job to the frame queue

      push_frame ( p_job ) ;

      memlog >> result ;
    }
    else
    {
      // store the result and free it's memory

      store_rendered_image ( p_job ,
                             nullptr ,
                             result ,
                             false ,   // data are always sRGB
                             bls.filename ) ;

      std::cout << "saved exposure fusion result as "
                << bls.filename << std::endl ;

      memlog >> result ;
    }
  }
} ;

// depending on the mode of the stitching job, pick the alpha or
// non-alpha version

void dispatch::process_stitching_job ( job_type * p_job ) const
{
  auto start = pv_clock::now() ;

  blend_type blender ( p_job ) ;

  // new routing: previously (as in the commented-out code below)
  // I was using a separate routine for non-alpha stitching jobs.
  // This required a lot of finessing to get good results, and it
  // required a second resonably complex function to get the job
  // done. It did have some advantages: the masks could be generated
  // just-in-time for every partial, vs. the need for the
  // full set of masks in the alpha version, and of course the
  // buffers used for RGB images use less memory than RGBA.
  // Speed-wise I saw little difference. So for now I'm simply
  // switching off the separate non-alpha code and focus on
  // tweaking the alpha code.

  blender.process_stitching_job_alpha() ;

  auto end = pv_clock::now() ;
  auto elapsed
      = float ( std::chrono::duration_cast<std::chrono::milliseconds>
                    ( end - start ) . count() ) ;
  std::cout << "stitching job (" << p_job->target.width
            << "*" << p_job->target.height << ") took "
            << elapsed / 1000.0 << " sec" << std::endl ;
}

void dispatch::process_fusion_job ( job_type * p_job ) const
{
  auto start = pv_clock::now() ;

  blend_type blender ( p_job ) ;

  // new code: use alpha processing throughout, like for stitching

  blender.process_fusion_job_alpha() ;

  auto end = pv_clock::now() ;
  auto elapsed
      = float ( std::chrono::duration_cast<std::chrono::milliseconds>
                    ( end - start ) . count() ) ;
  std::cout << "fusion job took "
            << elapsed / 1000.0 << " sec" << std::endl ;
}

} ; // namespace PV_ARCH
