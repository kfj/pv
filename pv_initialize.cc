/************************************************************************/
/*                                                                      */
/*          pv - a viewer for panoramic images                          */
/*                                                                      */
/*          Copyright 2016 - 2023 by Kay F. Jahnke                      */
/*                                                                      */
/*  The git repository for this software is at                          */
/*                                                                      */
/*  https://bitbucket.org/kfj/pv                                        */
/*                                                                      */
/*  Please direct questions, bug reports, and contributions to          */
/*                                                                      */
/*  kfjahnke+pv@gmail.com                                               */
/*                                                                      */
/*  pv is free software: you can redistribute it and/or modify          */
/*  it under the terms of the GNU General Public License as published   */
/*  by the Free Software Foundation, either version 3 of the License,   */
/*  or (at your option) any later version.                              */
/*                                                                      */
/*  pv is distributed in the hope that it will be useful,               */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       */
/*  GNU General Public License for more details.                        */
/*                                                                      */
/*  You should have received a copy of the GNU General Public License   */
/*  along with pv.  If not, see <http://www.gnu.org/licenses/>.         */
/*                                                                      */
/************************************************************************/

/*! \file pv_initialize.cc
 * 
 *  \brief code to process command line options
 * 
 */


#include <map>
#include <fstream>
#include <regex>
#include "pv_common.h"
#include "pv_initialize.h"

// we need access to the used file queue to process back-references

namespace ui
{
  extern std::deque < std::string > used_files ;
} ;

// namespace ini has code to obtain and process command line arguments.
// each argument is introduced by a macro which places a variable in
// args_type to contain the gleaned value, and, in a second run
// with redefined macro, emplaces a functor in a std::map so that the
// key is the option's name and the value is the functor. On lookup,
// the resulting functor is executed, and in turn sets the variable.
// The functors come in handy for processing 'short' arguments as
// well, because they do some value-checking and reject wrong arguments.
// The code here might be extended to do stuff like ini file processing
// or initialization from a scripting language. It's a bit of a jumble;
// it evolved from a simple getopt rewrite with single-character args,
// then was extended with long (--style) arguments and a more structured
// approach.

namespace ini
{
// The first bit has argument-processing code. These methods are called
// by the argument functors in the map and return the value of the argument
// in question - or they exit the program if there is an error. The first
// argument to these functions is the name of the option (to provide a
// meaningful error message), followed by the value found on the command
// line (as a std::string), and, optionally, further arguments.

// obtain a yes/no type flag. If 'value' is an empty string, we take
// it as an affirmative. Other strings are common words to affirm or
// deny something. Return value is a boolen, unknown values terminate
// the program.

bool affirm ( const std::string & key ,
              const std::string & value )
{
  static std::map < std::string , bool > keywords
  {
    { "" , true } ,
    { "yes" , true } ,
    { "on" , true } ,
    { "true" , true } ,
    { "1" , true } ,
    { "+" , true } ,
    { "positive" , true } ,
    { "no" , false } ,
    { "off" , false } ,
    { "false" , false } ,
    { "negative" , false } ,
    { "-" , false } ,
    { "0" , false }
  } ;

  auto found = keywords.find ( value ) ;
  if ( found == keywords.end() )
  {
    std::string error ( key + ": expecting yes/no, got: " + value ) ;
    abort_lux ( error ) ;
  }

  return found->second ;
}

// obtain a value from a given set. If 'value' is not in 'choices',
// the program is terminated.

std::string select ( const std::string & key ,
                     const std::string & value ,
                     const std::vector < std::string > & choices )
{
  for ( auto const & c : choices )
  {
    if ( c == value )
      return value ;
  }

  std::string error ( key + ": expecting one of [" ) ;
  for ( auto const & c : choices )
    error += ( " " + c ) ;
  error += ( " ], got: " + value ) ;
  abort_lux ( error ) ;
  return std::string() ; // just to silence warmings
}

// obtain a real value.

double get_real ( const std::string & key , const std::string & value )
{
  double x ;
  try
  {
    x = stod ( value ) ;
  }
  catch ( ... )
  {
    std::string error ( key + ": expecting real value, got: " + value ) ;
    abort_lux ( error ) ;
  }
  return x ;
}

// obtain an angle value.

double get_angle ( const std::string & key , const std::string & value )
{
  double x ;
  try
  {
    x = stod ( value ) ;
  }
  catch ( ... )
  {
    std::string error ( key + ": expecting angle in degrees, got: " + value ) ;
    abort_lux ( error ) ;
  }
  return x * M_PI / 180.0 ;
}

// obtain an int value.

long get_int ( const std::string & key , const std::string & value )
{
  long x ;
  try
  {
    x = stol ( value ) ;
  }
  catch ( ... )
  {
    std::string error ( key + ": expecting integral value, got: " + value ) ;
    abort_lux ( error ) ;
  }
  return x ;
}

// state_type's c'tor initializes all member variables to the default
// values given in options.h

state_type::state_type()
{
  // Note that options.h ends with #undefs for all macros,
  // which is why we can simply #define the macros again

  #define option(NAME,DEFAULT) NAME = DEFAULT ;

  #define real(NAME,DEFAULT) NAME = DEFAULT ;

  #define angle(NAME,DEFAULT) NAME = DEFAULT ;

  #define integer(NAME,DEFAULT) NAME = DEFAULT ;

  #define yes_no(NAME,DEFAULT) NAME = DEFAULT ;

  #define list(NAME,DEFAULT) NAME = DEFAULT ;

  #define rlist(NAME,DEFAULT) NAME = DEFAULT ;

  #define ilist(NAME,DEFAULT) NAME = DEFAULT ;

  #define alist(NAME,DEFAULT) NAME = DEFAULT ;

  #define blist(NAME,DEFAULT) NAME = DEFAULT ;

  #define one_of(NAME,DEFAULT,...) NAME = DEFAULT ;

  // the c'tor code is generated by the macro invocations in
  // options.h, now with the macro definions just given.

  #include "options.h"
}

// 'listen' clears the 'touched' flag for all list types (used for
// facet maps). This modifies the behaviour of the vectors holding
// the arguments (see class arg_vector) to issue a clear() on first
// push_back.

void state_type::listen()
{
  #define option(NAME,DEFAULT) ;

  #define real(NAME,DEFAULT) ;

  #define angle(NAME,DEFAULT) ;

  #define integer(NAME,DEFAULT) ;

  #define yes_no(NAME,DEFAULT) ;

  #define list(NAME,DEFAULT) NAME.listen() ;

  #define rlist(NAME,DEFAULT) NAME.listen() ;

  #define ilist(NAME,DEFAULT) NAME.listen() ;

  #define alist(NAME,DEFAULT) NAME.listen() ;

  #define blist(NAME,DEFAULT) NAME.listen() ;

  #define one_of(NAME,DEFAULT,...) ;

  // the c'tor code is generated by the macro invocations in
  // options.h, now with the macro definions just given.

  #include "options.h"
}

// clear all vector arguments and all masks

void state_type::clear()
{
  #define option(NAME,DEFAULT) ;

  #define real(NAME,DEFAULT) ;

  #define angle(NAME,DEFAULT) ;

  #define integer(NAME,DEFAULT) ;

  #define yes_no(NAME,DEFAULT) ;

  #define list(NAME,DEFAULT) NAME.clear() ;

  #define rlist(NAME,DEFAULT) NAME.clear() ;

  #define ilist(NAME,DEFAULT) NAME.clear() ;

  #define alist(NAME,DEFAULT) NAME.clear() ;

  #define blist(NAME,DEFAULT) NAME.clear() ;

  #define one_of(NAME,DEFAULT,...) ;

  // the c'tor code is generated by the macro invocations in
  // options.h, now with the macro definions just given.

  #include "options.h"

  maskv.clear() ;
}

// ini::state contains the initailization information. We use a global
// variable here since there can only be one currently valid initialization
// at a time. The structure definition has to be visible to pv_no_rendering.cc
// so that it can glean the initial values. The internal workings are
// local to pv_initialize.cc

state_type state ;

// alias, we'll use this often during program initialization

state_type & args ( ini::state ) ;

// now comes the third set of macro invocations, where we define
// the macros so that they create entries in 'opttab'. opttab is
// a std::map where the keys are option names and the values are
// functors transporting the part after the '=' (a string) to
// the corresponding member variable in ini::state (which is of
// a type that can be used by the program, like double or int).
// Note that we define the functors (saved as std::function) via
// lambda expressions, which is a terse and efficient way of
// initializing std::functions.

#define option(NAME,DEFAULT) \
{ #NAME , [] ( const std::string & value ) \
          { state.NAME = value ; } } ,

#define real(NAME,DEFAULT) \
{ #NAME , [] ( const std::string & value ) \
          { state.NAME = get_real ( #NAME , value ) ; } } ,

#define angle(NAME,DEFAULT) \
{ #NAME , [] ( const std::string & value ) \
          { state.NAME = get_angle ( #NAME , value ) ; } } ,

#define integer(NAME,DEFAULT) \
{ #NAME , [] ( const std::string & value ) \
          { state.NAME = get_int ( #NAME , value ) ; } } ,

#define yes_no(NAME,DEFAULT) \
{ #NAME , [] ( const std::string & value ) \
          { state.NAME = affirm ( #NAME , value ) ; } } ,

#define list(NAME,DEFAULT) \
{ #NAME , [] ( const std::string & value ) \
          { state.NAME.push_back ( value ) ; } } ,

#define rlist(NAME,DEFAULT) \
{ #NAME , [] ( const std::string & value ) \
          { state.NAME.push_back ( get_real ( #NAME , value ) ) ; } } ,

#define ilist(NAME,DEFAULT) \
{ #NAME , [] ( const std::string & value ) \
          { state.NAME.push_back ( get_int ( #NAME , value ) ) ; } } ,

#define alist(NAME,DEFAULT) \
{ #NAME , [] ( const std::string & value ) \
          { state.NAME.push_back ( get_angle ( #NAME , value ) ) ; } } ,

#define blist(NAME,DEFAULT) \
{ #NAME , [] ( const std::string & value ) \
          { state.NAME.push_back ( affirm ( #NAME , value ) ) ; } } ,

#define one_of(NAME,DEFAULT,...) \
{ #NAME , [] ( const std::string & value ) \
          { state.NAME = select ( #NAME , value , state.NAME##_set ) ; } } ,

// now we define the map 'opttab' itself:

typedef std::function < void ( const std::string & ) > argf_t ;

std::map < std::string , argf_t > opttab
{
  // again we invoke the macros:

  #include "options.h"

  // processing an ini file is a special case which does not in itself
  // have any effect on 'state'; instead the ini file is instantly
  // processed, which may or may not have an effect on 'state'.
  // Note that this is recursive: if the ini file conatins yet another
  // ini_file directive, it will in turn be processed right then.
  // The recursion is depth-first, and the effect on 'state' is
  // potentially cumulative: most directives just overwrite a
  // previous value, but 'list' type entries (like 'image')
  // will keep on being added to their target vector in 'state'.
  // all of these invocations don't modify 'path', this is only
  // done for lux files 'standing in for' images.
  
  { "ini_file" , [] ( const std::string & value )
    { read_ini_file ( value , false ) ; } } ,

  { "lux_file" , [] ( const std::string & value )
    { read_ini_file ( value , false ) ; } } ,

  // just to help with the logic, echo a string to cout. Again we don't
  // want any effect on 'state', just the echo there and then.

  { "echo" , [] ( const std::string & value )
    { std::cout << value << std::endl ; } } ,

  // 'clear' ignores it's argument and resets state to default values.

  { "clear" , [] ( const std::string & value )
    { state = state_type() ; } } ,

} ;

// As we have the long arguments in a nicely structured form, we can
// easily write a function to display all arguments together with
// the allowed values, so here's yet another set of definitions and
// invocations of the macros in options.h:

void list_arguments ( std::ostream & str )
{
  str << std::endl ;
  str << "all options can be passed in 'long form' with two" << std::endl ;
  str << "'-' characters before the option. Some options also" << std::endl ;
  str << "have a short version, please refer to the documentation." << std::endl ;
  str << std::endl ;
  str << "long options can be passed 'ini file style' with the" << std::endl ;
  str << "option and it's value separated by '=' or ':' and no" << std::endl ;
  str << "white space, or separated by white space." << std::endl ;
  str << "so pv --hfov=90 is the same as pv --hfov 90" << std::endl ;
  str << "white space after the '=' or ':' is interpreted as if" << std::endl ;
  str << "an empty string had been passed as the option's value." << std::endl ;
  str << "Some options can be passed several times, they are marked" << std::endl ;
  str << "with 'adds value to a list'. this is mainly used for" << std::endl ;
  str << "facet maps, to pass one value per facet, but also for" << std::endl ;
  str << "multiple 'image' arguments." << std::endl ;
  str << std::endl ;
  str << "valid long options are (default given in round brackets):" << std::endl << std::endl ;
  
  #define option(NAME,DEFAULT) \
    str << "--" << #NAME << "=<string>  (" << DEFAULT << ")" << std::endl ;

  #define real(NAME,DEFAULT) \
    str << "--" << #NAME << "=<real number>  (" << DEFAULT << ")" << std::endl ;

  #define angle(NAME,DEFAULT) \
    str << "--" << #NAME << "=<angle in degrees>  (" << DEFAULT << ")" << std::endl ;

  #define integer(NAME,DEFAULT) \
    str << "--" << #NAME << "=<whole number>  (" << DEFAULT << ")" << std::endl ;

  #define yes_no(NAME,DEFAULT) \
    str << "--" << #NAME << "=<yes|no>  (" << (DEFAULT?"yes":"no") << ")" << std::endl ;

  #define list(NAME,DEFAULT) \
    str << "--" << #NAME << "=<string>; adds value to a list " << std::endl ;

  #define rlist(NAME,DEFAULT) \
    str << "--" << #NAME << "=<real>; adds value to a list " << std::endl ;

  #define ilist(NAME,DEFAULT) \
    str << "--" << #NAME << "=<integer>; adds value to a list " << std::endl ;

  #define alist(NAME,DEFAULT) \
    str << "--" << #NAME << "=<angle in degrees>; adds value to a list " << std::endl ;

  #define blist(NAME,DEFAULT) \
    str << "--" << #NAME << "=<yes|no> ; adds boolan to a list " << std::endl ;

  #define one_of(NAME,DEFAULT,...) { \
    str << "--" << #NAME << "={" ; \
    auto v = std::begin ( state.NAME##_set ) ; \
    str << *v ; \
    ++v ; \
    while ( v != std::end ( state.NAME##_set ) ) \
      str << "|" << *(v++) ; \
    str << "}  (" << DEFAULT << ")" << std::endl ; }

  #include "options.h"

  str << "--ini_file=<filename> reads argument file <filename>" << std::endl ;
  str << std::endl ;
  str << "trailing arguments which can't be associated with an" << std::endl ;
  str << "option are taken as image files to be displayed." << std::endl ;
}

// synonym makes an option available under a different (alias) name.
// This is merely syntactic sugar, and the resulting additional entries
// in opttab are not documented.

void synonym ( const std::string & source , const std::string & alias )
{
  auto it = ini::opttab.find ( source ) ;

  if ( it == ini::opttab.end() )
  {
    std::cerr << "synonym fails: " << source << " not found" << std::endl ;
  }

  opttab [ alias ] = it->second ;
}

// We have some legacy code here for the short arguments which uses the
// common optarg/optind pair and a functio called pv_getopt (further down)

const char * optarg = nullptr ;
int optind = 1 ;

// get_long_option is called by pv_getopt if it finds '--', the
// sequence initiating a long argument.

bool get_long_option ( const char * const arg ,
                       int & optind ,
                       int argc = 0 ,
                       const char ** argv = nullptr )
{
  // we start out after the second '-' character

  const char * optarg ;

  const char * cp0 = arg ;
  const char * cp = cp0 ;
  bool has_value = false ;

  while ( true )
  {
    if ( *cp == '=' || *cp == ':' )
    {
      has_value = true ;
      break ;
    }
    if ( *cp == 0 )
    {
      optarg = cp ;
      break ;
    }
    ++cp ;
  }

  int keylen = cp - cp0 ;
  char key [ keylen + 1 ] ;
  for ( int i = 0 ; i < keylen ; i++ )
    key [ i ] = cp0 [ i ] ;
  key [ keylen ] = 0 ;
  std::string key_string ( key ) ;

  if ( has_value )
  {
    // found a '=' or ':' right after the option name, the remainder
    // of the CL argument constitutes it's value

    optarg = cp + 1 ;
  }
  else if ( argv )
  {
    has_value = true ;

    // alternatively, we accept the value as a separate argument.
    // When get_long_option is called to process an ini file,
    // argv is passed 0. Only when processing 'genuine' command line
    // options - from the command line or 'overrides' - argv is
    // passed != 0.

    if ( optind + 1 >= argc )
    {
      // a trailing long option without an argument is treated
      // as if an empty string had been passed as it's argument,
      // which is what happens when a long option is passed with
      // white space after the ':' or '='.

      optarg = "" ;
    }
    else
    {
      // if the option is not the last command line argument, we
      // advance optind and take the next command line argument
      // as it's value.

      ++optind ;
      optarg = argv[optind] ;
    }
  }

  // now we get to use opttab. We find the functor associated with a given
  // key - or we don't, which is an error and terminates the program.

  auto it = ini::opttab.find ( key ) ;

  if ( it == ini::opttab.end() )
  {
    std::string error ( "error while processing this option specification:\n" ) ;
    error += ( std::string ( arg ) + "\n" ) ;
    error += ( std::string ( key ) + " is not a valid option name" ) ;
    abort_lux ( error ) ;
  }
  else
  {
    // we've found the functor, so we execute it, passing the value
    // of the option (which may be an empty string). The functor
    // takes care of processing and storing the value - or exits
    // the program if something was wrong.

    std::cout << "argument: " << key ;
    if ( has_value )
      std::cout << " value: '" << optarg << "'" ;
    std::cout << std::endl ;

    it->second ( optarg ) ;
  }

  // We return true to indicate that a long argument was found.

  return true ;
}

// locate_file helps finding a file on disk. images and ini files
// may come with or without a path. If no path is present, locate_file
// prepends script_path to the filename. If a path is present in the
// filename, this path takes preferece. If script_path is empty,
// the filename is used as is. An empty filename as input and files
// which can't be found on disk result in return of an empty string.
// If all is well, the filename, optionally prepended with script_path,
// is returned to the caller.

std::string script_path ;

std::string locate_file ( std::string filename )
{
  // if the filename is empty, we return straight away

  if ( filename == std::string() )
  {
    return filename ;
  }

  // does the filename start with a dollar sign? this is a
  // file queue reference. undocumented feature for now.

  if ( filename.substr ( 0 , 1 ) == std::string ( "$" ) )
  {
    auto str = filename.substr ( 1 , filename.size() - 1 ) ;
    try
    {
      auto fileno = std::stoi ( str ) ;
      if ( fileno < 0 )
      {
        // negative index? python-style, from end of queue
        fileno = int(ui::used_files.size()) + fileno - 1 ;
      }
      filename = ui::used_files [ fileno ] ;

      std::cout << "resolved back reference $" << fileno
                << " as '" << filename << "'" << std::endl ;
    }
    catch ( ... )
    {
      std::cerr << "failed to process '$"
                << str << "' as a back reference"
                << std::endl ;
    }           
  }

  // if script_path is empty, return filename unchanged

  if ( script_path.length() )
  {
    // see if the filename contains a path

    bool has_path = false ;
    for ( int i = 0 ; i < filename.size() ; i++ )
    {
      if ( filename[i] == '\\' | filename [i] == '/' )
      {
        has_path = true ;
        break ;
      }
    }
    if ( ! has_path )
    {
      // no path characters found in filename, prepend script_path
      filename = script_path + filename ;
    }
  }

  // so far so good - now, does this file exist?

  std::ifstream ifs ( filename ) ;

  if ( ! ifs.good() )
  {
    std::cout << "failed to locate '" << filename << "'" << std::endl ;
    filename = std::string() ;
  }

  return filename ;
}

// read_ini_file has a simple parser for ini files. All lines starting
// with '#', '[' or white space are treated as comments. Only lines containing
// a single long argument specification are scanned and processed like
// long arguments on the command line, minus the '--'. So a valid entry is
//
// hfov=360
//
// Note that, like in long command line arguments, white space is not allowed
// before the = or : separating argument name and value. White space after the
// separator becomes *part of the value*, so if you pass, e.g. file names, make
// sure there is no intervening space.
//
// reading ini files is recursive: if the ini file itself has an ini_file=...
// entry, this entry is processed next. The capacity for recursion is due
// to the entry for 'read_ini' in optarg, not due to some 'special magic'.
//
// Note also that ini files may be passed *instead of image files*. The effect
// is that the ini file is processed when 'it's time comes' (it may be queued,
// like any other file specified with --image or as trailing parameter), and
// args.show must contain a 'real' image file afterwards. This mechanism is
// used to 'bundle' an image with arguments intended specifically for it. The
// non-image 'code' in the ini file can set up the arguments as it sees fit,
// and the image (which is introduced by a 'show' command) will be displayed
// as intended. After display, all the changes to the arguments are undone
// (a fresh copy of the initial state right after pv's invocation is copied
// to 'state'), and the ini file which was used 'instead of an image' passes
// through the usual queues like any other image, so that when it's viewed
// again, it will again load it's specific arguments.

void set_path ( const std::string & filename )
{
  // does the filename have a path? If so, we take note of this path
  // and save it in script_path. We'll use this value when an ini file
  // is used instead of an image file to prepend it to the image file
  // if that, in turn, does not have a path.
  // We use a simple test for a path: if any path separators occur
  // in the filename, everything up to and including the last path
  // separator is considered the path and stored in script_path.

  int length = filename.size() ;
  int path_ends = -1 ;
  for ( int pos = length - 1 ; pos >= 0 ; pos-- )
  {
    if ( filename[pos] == '\\' || filename[pos] == '/' )
    {
      path_ends = pos ;
      break ;
    }
  }
  if ( path_ends >= 0 )
  {
    // it has a path. save this in script_path.

    script_path = filename.substr ( 0 , path_ends + 1 ) ;
    std::cout << "detected input file's path: " << script_path << std::endl ;
  }
}

bool read_ini_file ( const std::string & filename , bool fix_path )
{
  try
  {
    std::ifstream str ( filename ) ;
    if ( ! str )
    {
      std::cerr << "could not open ini file " << filename << std::endl ;
      return false ;
    }

    if ( fix_path )
      set_path ( filename ) ;

    int save_optind = optind ;
    char buffer [ 1024 ] ;
    int options = 0 ;
    while ( str.getline ( buffer , 1024 ) )
    {
      if (    buffer[0] == '#' || buffer[0] == ' ' || buffer[0] == '['
          || buffer[0] == '\t' || buffer[0] == 0 )
        continue ;
      if ( get_long_option ( buffer , optind ) )
        ++options ;
      else
        break ;
    }
    if ( ! options )
    {
      abort_lux
        (   std::string ( "unrecoverable error processing: " )
          + filename
          + ":\nno valid content found in this file"
          + "\nis this a valid lux ini file?"
        ) ;
    }
    return str.eof() ? true : false ;
  }
  catch ( ... )
  {
    abort_lux
      (   std::string ( "unrecoverable error processing: " )
        + filename
        + ":\nan exception occured when accessing this file"
        + "\nis this a valid lux ini file?"
      ) ;
  }
  return false ;
}

struct pto_line_type
{
  std::string head ; // will contain the 'header', like "i" for image lines
  std::map < std::string , std::string > field_map ; // map of items in the line
} ;

struct pto_parser_type
{
  const std::regex pto_line_regex ; // matches a whole pto line
  const std::regex pto_item_regex ; // matches a single item in a line

  // parse_pto_line receives a line from a PTO file and scans it's content.
  // the result is returned as a pto_line_type object.

  pto_line_type parse_pto_line ( const std::string & s )
  {
    pto_line_type pto_line ;

    // split the line into head and remainder

    std::smatch parts ;
    std::regex_match ( s , parts , pto_line_regex ) ;

    std::ssub_match part = parts[1] ;
    pto_line.head = part.str() ;

    std::string tail = parts[2].str() ;

    // now split the remainder into individual items

    auto start = std::sregex_iterator ( tail.begin() ,
                                        tail.end() ,
                                        pto_item_regex ) ;
    auto end = std::sregex_iterator() ;

    for ( auto i = start ; i != end ; ++i )
    {
      // iterate over the items and separate name and value

      auto item = i->str() ;
      std::regex_match ( item , parts , pto_item_regex ) ;

      auto field_name = parts[1].str() ;
      auto field_value = parts[2].str() ;

      // store the field, value pair in the field map

      pto_line.field_map [ field_name ] = field_value ;
    }

    return pto_line ;
  }

  // TODO: handle escaped quotation marks

  pto_parser_type()
  : pto_line_regex ( "([a-zA-Z])\\s(.+)[\n\r]*" ) ,
    pto_item_regex ( "([A-Za-z]+)((\"[^\"]+\")|(\\S*))" )
  { }
} ;

pto_parser_type pto_parser ;

// read a pto file. the effect is the same as reading an ini file
// defining a facet map.

bool _read_pto_file ( const std::string & filename )
{
  // this map 'translates' PTO item codes into pv option names

  static std::map < std::string , std::string > pto_to_pv
  {
    { "Eev" , "facet_brightness" } ,
    { "f" , "facet_projection" } ,
    { "v" , "facet_hfov" } ,
    { "y" , "facet_yaw" } ,
    { "p" , "facet_pitch" } ,
    { "r" , "facet_roll" } ,
    { "a" , "facet_lca" } ,
    { "b" , "facet_lcb" } ,
    { "c" , "facet_lcc" } ,
    { "d" , "facet_lch" } ,
    { "e" , "facet_lcv" } ,
    { "Va" , "facet_vca" } ,
    { "Vb" , "facet_vcb" } ,
    { "Vc" , "facet_vcc" } ,
    { "Vd" , "facet_vcd" } ,
    { "Vx" , "facet_vcx" } ,
    { "Vy" , "facet_vcy" } ,
    { "n" , "facet" } ,
    { "j" , "facet_stack_parent" }
  } ;

  std::ifstream str ( filename ) ;
  if ( ! str )
  {
    std::cerr << "could not open pto file " << filename << std::endl ;
    return false ;
  }

  // we'll store the image lines we extract in a vector, so that we can
  // resolve 'back references': if an item in a pto file has a value
  // starting with a '=N', it means to copy the value from image N

  std::vector < pto_line_type > pto_file ;
  pto_line_type p_line ;
  std::vector < pto_line_type > k_line ;

  set_path ( filename ) ;

  // ought to suffice

  char buffer [ 2048 ] ;

  while ( str.getline ( buffer , 2048 ) )
  {
    // for now, we're only interested in image lines. It's easy to
    // pick specific line types, all we need to look at is the first
    // letter of the line. Pre-selecting this way saves a lot of time,
    // because we needn't do any RE processing at all.
    // v lines come after i lines, so we can stop scanning once we
    // see the first v line. TODO is this a rule?

//     if ( buffer[0] == 'v' )
//     {
//       break ;
//     }
//     else
    if ( buffer[0] == 'p' )
    {
      p_line = pto_parser.parse_pto_line ( buffer ) ;
      args.have_p_line = true ;
    }
    else if ( buffer[0] == 'i' )
    {
      // parse the line
      auto pto_line = pto_parser.parse_pto_line ( buffer ) ;
      // save the result
      pto_file.push_back ( pto_line ) ;
    }
    else if ( buffer[0] == 'k' )
    {
      // parse the line
      auto pto_line = pto_parser.parse_pto_line ( buffer ) ;
      // save the result
      k_line.push_back ( pto_line ) ;
    }
  }

  if ( ! pto_file.size() )
  {
    abort_lux
      (   std::string ( "unrecoverable error processing: " )
        + filename
        + ":\nno valid PTO content found"
        + "\nis this a valid PTO file?"
      ) ;
  }

  // let's process what we have gleaned:

  // first, we process the p-line

  for ( const auto & it : p_line.field_map )
  {
    // for all fields, look up the item code

    auto item_code = it.first ;
    auto value = it.second ;

    if ( item_code == "f" )
    {
      auto prj = std::stoi ( value ) ;

      if ( prj == 0 )
        args.p_projection = RECTILINEAR ;
      else if ( prj == 1 )
        args.p_projection = CYLINDRIC ;
      else if ( prj == 2 )
        args.p_projection = SPHERICAL ;
      else if ( prj == 3 )
        args.p_projection = FISHEYE ;
      else if ( prj == 4 )
         args.p_projection = STEREOGRAPHIC ;
      else
      {
        // TODO: allow override, better error message, check
        // abort code (currently produces a zombie process)
        std::cerr << "can't handle PTO projection code "
                  << prj << " in p-line" << std::endl ;

        args.p_projection = PRJ_UNSUPPORTED ;
      }
    }
    else if ( item_code == "v" )
    {
      args.p_hfov = std::stod ( value ) ;
    }
    else if ( item_code == "w" )
    {
      args.p_width = std::stoi ( value ) ;
    }
    else if ( item_code == "h" )
    {
      args.p_height = std::stoi ( value ) ;
    }
    else if ( item_code == "S" )
    {
      args.have_crop = true ;
      std::regex crop_regex ( "([0-9]+),([0-9]+),([0-9]+),([0-9]+)" ) ;
      std::smatch parts ;
      std::regex_match ( value , parts , crop_regex ) ;
      args.p_crop_x0 = std::stoi ( parts[1].str() ) ;
      args.p_crop_x1 = std::stoi ( parts[2].str() ) ;
      args.p_crop_y0 = std::stoi ( parts[3].str() ) ;
      args.p_crop_y1 = std::stoi ( parts[4].str() ) ;
    }
    else
    {
      std::cout << "p-line ignore: " << item_code
                << " = " << value << std::endl ;
    }

  }
  
  std::cout << "p-line projection: "
            << projection_name [ args.p_projection ]
            << std::endl ;
  std::cout << "p-line hfov: " << args.p_hfov << std::endl ;
  std::cout << "p-line width: " << args.p_width << std::endl ;
  std::cout << "p-line height: " << args.p_height << std::endl ;
  if ( args.have_crop )
  {
    std::cout << "p-line crop x0: " << args.p_crop_x0 << std::endl ;
    std::cout << "p-line crop x1: " << args.p_crop_x1 << std::endl ;
    std::cout << "p-line crop y0: " << args.p_crop_y0 << std::endl ;
    std::cout << "p-line crop y1: " << args.p_crop_y1 << std::endl ;
  }

  int line = 0 ;
  double eev_sum = 0.0 ;

  for ( const auto & l : pto_file )
  {
    double crop_x0 = 0.0 ;
    double crop_x1 = 0.0 ;
    double crop_y0 = 0.0 ;
    double crop_y1 = 0.0 ;
    bool facet_crop = false ;

    for ( const auto & it : l.field_map )
    {
      // for all fields, look up the item code

      auto item_code = it.first ;
      auto value = it.second ;

      // unquote strings, resolve back references

      if ( item_code[0] == 'j' )
      {
        // stack assignements in PTO have uncommon logic
        if ( value[0] == '=' )
        {
          value = value.substr ( 1 , value.size() - 1 ) ;
        }
        else
        {
          value = std::to_string ( line ) ;
        }
      }
      else
      {
        if ( value[0] == '=' )
        {
          auto text = value.substr ( 1 , value.size() - 1 ) ;
          auto line_no = std::stoi ( text ) ;
          auto replace_from = pto_file [ line_no ] ;
          value = replace_from.field_map [ item_code ] ;
        }
        else if ( value[0] == '"' )
        {
          value = value.substr ( 1 , value.size() - 2 ) ;
        }
      }

      // is there a mapping to a lux option?

      auto pv_option = pto_to_pv.find ( item_code ) ;

      if ( pv_option == pto_to_pv.end() )
      {
        // there is no mapping to a lux option, but the PTO item may
        // refer to image geometry, in which case we can't simply ignore
        // it: the image would come out wrong. So for such item codes, we
        // check if any of these parameters are set. this used to be
        // ignored silently, now it's detected and considered an error,
        // terminating the program.

        if ( item_code[0] == 'T' )
        {
          if ( std::stod ( value ) != 0.0 )
          {
            abort_lux ( "cannot handle translation parameters in PTO file" ) ;
          }
        }
        else if ( item_code[0] == 'g' || item_code[0] == 't' )
        {
          if ( std::stod ( value ) != 0.0 )
          {
            abort_lux ( "cannot handle lens shear parameters in PTO file" ) ;
          }
        }
        else if ( item_code == "S" )
        {
          std::regex crop_regex ( "([0-9]+),([0-9]+),([0-9]+),([0-9]+)" ) ;
          std::smatch parts ;
          std::regex_match ( value , parts , crop_regex ) ;
          crop_x0 = std::stoi ( parts[1].str() ) ;
          crop_x1 = std::stoi ( parts[2].str() ) ;
          crop_y0 = std::stoi ( parts[3].str() ) ;
          crop_y1 = std::stoi ( parts[4].str() ) ;
          std::cout << "found cropping info in i-line:" << std::endl ;
          std::cout << "x0 " << crop_x0 << " x1 " << crop_x1
                    << "y0 " << crop_y0 << " y1 " << crop_y1 << std::endl ;
          facet_crop = true ;
        }


        // codes T..., g, t == 0 are simply ignored

        continue ;
      }

      // we now save the pto option name in 'key' and the string
      // holding the value in 'value'

      auto key = pv_option->second ;

      // optionally add a path to a filename. If the filename given
      // in the pto file contains path delimiters, we assume it's an
      // absolute path. If not, we feed it to 'locate_file' to obtain
      // a usable path.

      if ( item_code == "n" )
      {
        bool contains_delimiter = false ;

        for ( const auto & c : value )
        {
          if ( c == '/' || c == '\\' )
          {
            contains_delimiter = true ;
            break ;
          }
        }

        if ( ! contains_delimiter )
          value = locate_file ( value ) ;
      }

      // replace pto numeric projection code by pv string

      else if ( item_code == "f" )
      {
        bool crop_elliptic = false ;
        auto prj = std::stoi ( value ) ;

        if ( prj == 0 )
          value = "rectilinear" ;
        else if ( prj == 1 )
          value = "cylindric" ;
        // the two types of fisheye:
        // #                  2 - Circular fisheye
        // #                  3 - full-frame fisheye
        // TODO: 3 is for full-frame fisheyes,
        // which don't need masking. 2 neeeds masking, which is
        // specified in a cropping directive (like S3551,6814,89,3352)
        // which specifies a *round* cropping area
        // with these limits. To process such images correctly, an alpha
        // channel must be created and initialized accordingly.
        // For other projections, the masking should mean a rectangular
        // mask.
        else if ( prj == 2 )
        {
          value = "fisheye" ;
          crop_elliptic = true ;
        }
        else if ( prj == 3 )
          value = "fisheye" ;
        else if ( prj == 4 )
          value = "spherical" ;
        else if ( prj == 10 )
          value = "stereographic" ;
        else
        {
          std::cerr << "can't handle PTO projection code "
                    << prj << std::endl ;
          return false ;
        }
        
        auto opt = ini::opttab.find ( "facet_crop_elliptic" ) ;
        opt->second ( crop_elliptic ? "yes" : "no" ) ;
      }

      // sum up Eev values to get the average later on

      else if ( item_code == "Eev" )
      {
        auto eev = std::stof ( value ) ;
        eev_sum += eev ;
      }

      // now we get to use opttab. We find the functor associated with a
      // given key - or we don't, which is an error and terminates the
      // program. So far, we have all pto item values as strings. This
      // is deliberate: we can simply use the opttab machinery which
      // provides processing functions for each option name, and the
      // processing functions all take string arguments. So we needn't
      // concern ourselves with argument types at all.

      auto opt = ini::opttab.find ( key ) ;

      if ( opt == ini::opttab.end() )
      {
        // this should not ever happen...

        std::cerr << "error while processing this option specification"
                  << std::endl ;
        std::cerr << "'" << key << "' is not a valid option name" << std::endl ;
        return false ;
      }
      else
      {
        // we've found the functor, so we execute it, passing the value
        // of the option. The functor takes care of processing and storing
        // the value - or exits the program if something was wrong.

        std::cout << "argument: " << key ;
        std::cout << " value: " << value ;
        std::cout << std::endl ;

        opt->second ( value ) ;
      }
    }
    {
      auto opt = ini::opttab.find ( "facet_crop_active" ) ;
      opt->second ( facet_crop ? "yes" : "no" ) ;
    }
    {
      auto opt = ini::opttab.find ( "facet_crop_x0" ) ;
      opt->second ( std::to_string ( crop_x0 ) ) ;
    }
    {
      auto opt = ini::opttab.find ( "facet_crop_x1" ) ;
      opt->second ( std::to_string ( crop_x1 ) ) ;
    }
    {
      auto opt = ini::opttab.find ( "facet_crop_y0" ) ;
      opt->second ( std::to_string ( crop_y0 ) ) ;
    }
    {
      auto opt = ini::opttab.find ( "facet_crop_y1" ) ;
      opt->second ( std::to_string ( crop_y1 ) ) ;
    }
    ++line ;
  }

  const std::regex mask_corner_regex ( "([+-]?[0-9.]+)\\s([+-]?[0-9.]+)" ) ;

  for ( const auto & l : k_line )
  {
    mask_type mask ;
    for ( const auto & it : l.field_map )
    {
      // for all fields, look up the item code

      auto item_code = it.first ;
      auto value = it.second ;

      // unquote strings, resolve back references

      if ( value[0] == '=' )
      {
        auto text = value.substr ( 1 , value.size() - 1 ) ;
        auto line_no = std::stoi ( text ) ;
        auto replace_from = pto_file [ line_no ] ;
        value = replace_from.field_map [ item_code ] ;
      }
      else if ( value[0] == '"' )
      {
        value = value.substr ( 1 , value.size() - 2 ) ;
      }

      if ( item_code == "i" )
        mask.image = std::stoi ( value ) ;
      else if ( item_code == "t" )
        mask.variant = std::stoi ( value ) ;
      else if ( item_code == "p" )
        mask.vertex_list = value ;

    }

    std::cout << "mask for image " << mask.image
              << " of type " << mask.variant
              << " with vertices " << mask.vertex_list << std::endl ;
              
    auto start = std::sregex_iterator ( mask.vertex_list.begin() ,
                                        mask.vertex_list.end() ,
                                        mask_corner_regex ) ;
    auto end = std::sregex_iterator() ;
    for ( auto i = start ; i != end ; ++i )
    {
      // iterate over the items and separate name and value

      auto item = i->str() ;
      std::smatch parts ;
      std::regex_match ( item , parts , mask_corner_regex ) ;

      auto xs = parts[1].str() ;
      auto ys = parts[2].str() ;

      mask.vx.push_back ( std::stod ( xs ) ) ;
      mask.vy.push_back ( std::stod ( ys ) ) ;

      // TODO: this works only for natively landscape images, for portrait
      // the coordinates have to be modified to refer to the rotated image
    }
    
    if ( mask.variant == 4 )
    {
      std::cerr << "warning: mask type not implemented: " << mask.variant
                << " this mask will be ignored" << std::endl ;
    }

    ini::args.maskv.push_back ( mask ) ;
  }

  // get the average Eev as bias. Making the Eev values relative
  // to the average of all images' Eev values gives a good starting
  // point and is oftentimes ideal.

  double eev_ref = eev_sum /= line ;

  // convert biased PTO Eev values to pv linear brightness. facets
  // with reference Eev will come out with brightness == 1. If there
  // is a brightness override, the override values are prepended and
  // the vector is twice as long. In this case the conversion is
  // omitted because the incoming values are already multiplicative
  // factors.

  if ( ini::args.facet_brightness.size() == line )
  {
    for ( int i = 0 ; i < line ; i++ )
    {
      double eev = ini::args.facet_brightness[i] ;
      double deev = eev - eev_ref ;

      ini::args.facet_brightness[i] = pow ( 2.0 , deev ) ;
    }
  }

  std::cout << "pto file parse terminates normally" << std::endl ;
  return true ;
}

bool read_pto_file ( const std::string & filename )
{
  std::ifstream str ( filename ) ;
  if ( ! str )
  {
    std::cerr << "could not open pto file " << filename << std::endl ;
    return false ;
  }

  try
  {
    bool success = _read_pto_file ( filename ) ;
    return success ;
  }
  catch ( ... )
  {
    abort_lux
      (   std::string ( "unrecoverable error processing: " )
        + filename
        + ":\nan exception occured when accessing this file"
        + "\nis this a valid PTO file?"
      ) ;
  }
  return false ;
}

// In the ongoing effort to port pv to other systems, we noticed that some
// systems don't have getopt(). So I wrote the function pvgetopt() which
// is sufficient to process the type of command line pv anticipates. This
// implementation may not perform precisely as the GNU getopt() function
// declared by unistd.h but for pv it's sufficient.

// pvgetopt works like classical getopt, but delegates long arguments
// to get_long_option.

int pvgetopt ( int argc , const char * argv[] , const char * const optstring )
{
  if ( optind >= argc )
  {
    return -1 ; // signal end-of-options, optarg remains nullptr
  }

  if ( argv[optind][0] != '-' ) // argument doesn't begin with '-'
  {
    optarg = argv[optind] ;     // signal end-of-options, but set optarg
    return -1 ;
  }

  // argument does begin with '-', see if it's valid a valid option

  // extract the flag character after the '-'
  int opt = argv[optind][1] ;

  // if it's another '-', it must be a long argument

  if ( opt == '-' )
  {
    // here we pass argc and argv on to get_long_option, to allow
    // it to read the next argument as the option's value if the
    // option is not passed 'ini file style' with an internal
    // ':' or '=' as separator

    bool success = get_long_option ( argv[optind] + 2 ,
                                     optind , argc , argv ) ;
    if ( ! success )
    {
      std::cerr << "get_long_option failed" << std::endl ;
      return -2 ;
    }
    ++optind ;
    return 0 ;
  }

  // try and find it in 'optstring'
  const char *p = strchr ( optstring , opt ) ;

  if ( p == nullptr )
  {
    return -3 ; // no such option
  }

  // flag appears in 'optstring' where p points to, see if it's
  // followed by ':'
  if ( p[1] == ':' )
  {
    // if the flag is followed by ':' this means it's an argument
    // taking a value. Check if the value follows immediately
    if ( argv[optind][2] != 0 )
    {
      // value follows straight away, make 'optarg' point to it
      optarg = argv[optind] + 2 ;
    }
    else
    {
      // value doesn't follow immediately, should be in next argument
      optind++ ;
      if (optind >= argc)
      {
        return '?' ; // no next argument? error!
      }
      // set optarg to the next argument in argv
      optarg = argv[optind] ;
    }
    // increment optarg for next call to getopt()
  }
  optind++ ;
  // return the value of the option flag
  return opt;
}

  // process command line arguments. This is following the standard
  // 'getopt' method, but additionally looks at 'long' arguments
  // introduced with '--' by calling get_long_option when '--' is
  // encountered.

int initialize ( int argc , const char * argv[] )
{
  args.have_crop = false ;
  args.have_p_line = false ;
  args.p_projection = PRJ_NONE ;
  args.p_hfov = 0.0 ;
  args.p_width = 0 ;
  args.p_height = 0 ;

  optind = 1 ;
  int opt ;

  // check if there is a file named .lux.ini in the user's home folder.
  // If so, read this file now, before any command line arguments are
  // processed. read_ini_file does the check; it's not an error if
  // read_ini_file returns 'false' indicating the file could not be
  // opened - lux assumes it's simply not there which is okay, but
  // (TODO) the failure might be due to other reasons

  const char * home = std::getenv ( "HOME" ) ;
  if ( home )
  {
    std::string lux_ini ( home ) ;
    lux_ini += std::string ( "/.lux.ini" ) ;
    read_ini_file ( lux_ini , false ) ;
  }

  // now we call pvgetopt(), which picks out and processes long arguments
  // (starting with --), for which it returns zero, or returns the integer
  // value of the short argument in 'opt'.

  while (
          ( opt = pvgetopt ( argc ,
                             argv ,
                             "d:e:x:y:f:q:H:h:r:v:p:m:t:b:w:z:i:ac:CGglnsuB:I:E:F:S:A:M:N:LWPRX:Z?"
                           )
          ) != -1
        )
  {
    switch (opt)
    {
      case 0:
        // long argument, was already processed by getopt
        break ;
      case 'a': // ignore alpha channel in source image
        args.alpha = "no" ;
        break ;
      case 'b': // sets the frame rendering time budget (in msec).
        // note that this only has an effect when auto_quality is on.
        args.budget = ini::get_real ( "-b" , optarg ) ;
        break ;
      case 'c':
      {
        // ini files passed with -c don't set the path
        if ( ! read_ini_file ( optarg , false ) )
          return 0 ;
        else
          std::cout << "successfully read lux ini file " << optarg << std::endl ;
        break ;
      }
      case 'G':
        args.grey_edge = false ;
        break ;
      case 'd': // slide interval in seconds
        args.slide_interval = ini::get_real ( "-d" , optarg ) ;
        args.slideshow_on = true ;
        break;
      case 'f': // degree of b-spline to use for the 'fast interpolator'
        args.fast_interpolator_degree = ini::get_int ( "-f" , optarg ) ;
        break;
      case 'h': // specification of panoramic image's horizontal field of view as angle.
        args.hfov = ini::get_angle ( "-h" , optarg ) ;
        break;
      case 'i': // pick a specific ISA
        args.isa = ini::select ( "-i" , optarg , args.isa_set ) ;
        break;
      case 'l': // switches off internal processing in linear RGB
        args.process_linear = false ;
        break;
      case 'g': // sets global scaling to automatic
        args.auto_quality = true ;
        break;
      case 'm': // sets global scaling factor to specific value
        args.moving_image_scaling = ini::get_real ( "-m" , optarg ) ;
        break;
      case 'M': // sets global scaling factor to specific value
        args.still_image_scaling = ini::get_real ( "-M" , optarg ) ;
        break;
      case 'n': // do not display the frames (used for benchmarking)
        args.suppress_display = true ;
        break;
      case 'p': // set projection of the panoramic image
        args.projection
          = ini::select ( "-p" , optarg , args.projection_set ) ;
        break ;
      case 'q': // same for the interpolator used in single-image mode.
        args.quality_interpolator_degree = ini::get_int ( "-q" , optarg ) ;
        break;
      case 'r': // set a frame rate limit.
        args.frame_rate_limit = ini::get_int ( "-r" , optarg ) ;
        break;
      case 's': // use simple interpolator (bilinear, no image pyramids)
        args.build_pyramids = false ;
        break;
      case 'u': // 'unsync' - switch off synchronization with vsync.
        args.use_vsync = false ;
        break;
      case 'v': // panoramic image's vertical field of view as angle.
        args.vfov = ini::get_angle ( "-v" , optarg ) ;
        break;
      case 'x': // horizontal offset of the image data
        args.horizontal_offset = ini::get_angle ( "-x" , optarg ) ;
        break;
      case 'y': // ditto for vertical offset.
        args.vertical_offset = ini::get_angle ( "-y" , optarg ) ;
        break;
      case 'z': // set a frame number limit.
        args.stop_after = ini::get_int ( "-z" , optarg ) ;
        break;
      case 'A': // wether to 'start running'.
        args.autopan = ini::get_real ( "-A" , optarg ) ;
        break ;
      case 'B': // apply a 'level bias'
        args.level_bias = ini::get_real ( "-B" , optarg ) ;
        break;
      case 'E': // extracted single image prefix (default:filename)
        args.snapshot_prefix = std::string ( optarg ) ;
        break ;
      case 'e': // save snapshots in specific format (default is 95% JPEG)
        args.snapshot_extension
          = ini::select  ( "-e" , optarg , args.snapshot_extension_set ) ;
        break ;
      case 'w': // use this font for the GUI
        args.gui_font = std::string ( optarg ) ;
        break ;
      case 'F': // smoothing level.
        args.pyramid_smoothing_level = ini::get_int ( "-F" , optarg ) ;
        break ;
      case 'H': // horizontal field of view of the viewing window.
        args.hfov_view = ini::get_angle ( "-H" , optarg ) ;
        break;
  //       case 'J': // sets the job queue fill ceiling.
  //         job_threshold = atoi ( optarg ) ;
  //         break ;
      case 'I': // sets magnification factor for 'magnifying glass' ('I').
        args.magnifying_glass_factor = ini::get_real ( "-I" , optarg ) ;
        break ;
      case 'L': // image is in linear RGB.
        args.is_linear = true ;
        break;
      case 'N': // 'snappiness' steers the overall responsiveness of the UI.
        args.snappiness = ini::get_real ( "-N" , optarg ) ;
        break ;
      case 'P': // start with disabled frame rendering in pan mode (F8)
        args.allow_pan_mode = false ;
        break;
      case 'R': // 'reverse' - inverts the effect of vertical click+drag
        args.reverse_drag = true ;
        break ;
      case 'S': // 'shrink factor' from one pyramid level to the next.
        args.pyramid_scaling_step = ini::get_real ( "-S" , optarg ) ;
        break ;
  //       case 'T': // sets the frame queue fill ceiling.
  //         frame_threshold = atoi ( optarg ) ;
  //         break ;
      case 'X': // standard magnification for snapshots
        args.snapshot_magnification = ini::get_real ( "-X" , optarg ) ;
        break ;
      case 'W': // start in a window (default is to start in full-screen mode)
        args.fullscreen = false ;
        break ;
      case 'Z': // 'reverse zoom' - inverts the effect of secondary vertical click+drag
        args.reverse_secondary_drag = true ;
        break ;
      case '?':
        args.help = true ;
        break ;
      case -2:
        // long argument scan failure
        std::cerr << "long argument error" << std::endl ;
        return 0 ;
      default:
        std::cerr << "unknown short argument -"
                  << char ( opt ) << std::endl ;
        return 0 ;
    }
  }

  if ( optind < argc )
  {
    std::cout << "interpreting trailing args as image files..." << std::endl ;
  
    for ( int arg = ini::optind ; arg < argc ; arg++ )
      state.image.push_back ( std::string ( argv [ arg ] ) ) ;
  }

  for ( auto const & file : state.image )
    std::cout << "image file: " << file << std::endl ;

  return 1 ;
} ;

} ; // namespace ini ;

