/************************************************************************/
/*                                                                      */
/*  echo_simd_type - echo the best SIMD implementation available        */
/*                                                                      */
/*          Copyright 2023 by Kay F. Jahnke                             */
/*                                                                      */
/*  this is s helper program to go with pv, the panorama viewer         */
/*  The git repository for this software is at                          */
/*                                                                      */
/*  https://bitbucket.org/kfj/pv                                        */
/*                                                                      */
/*  Please direct questions, bug reports, and contributions to          */
/*                                                                      */
/*  kfjahnke+pv@gmail.com                                               */
/*                                                                      */
/*    Permission is hereby granted, free of charge, to any person       */
/*    obtaining a copy of this software and associated documentation    */
/*    files (the "Software"), to deal in the Software without           */
/*    restriction, including without limitation the rights to use,      */
/*    copy, modify, merge, publish, distribute, sublicense, and/or      */
/*    sell copies of the Software, and to permit persons to whom the    */
/*    Software is furnished to do so, subject to the following          */
/*    conditions:                                                       */
/*                                                                      */
/*    The above copyright notice and this permission notice shall be    */
/*    included in all copies or substantial portions of the             */
/*    Software.                                                         */
/*                                                                      */
/*    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND    */
/*    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES   */
/*    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND          */
/*    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT       */
/*    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,      */
/*    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING      */
/*    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR     */
/*    OTHER DEALINGS IN THE SOFTWARE.                                   */
/*                                                                     */
/************************************************************************/

/*! \file echo_simd_type.cc

    \brief echo the best SIMD implementation supported by Vc on this system
    
    While Vc's differentiation of the SIMD implementations is a bit more
    fine-grained, this program only looks at a subset which should be
    sufficient for most use cases. If you need more (or want less) please
    consult Vc's documentation.
    
    The abbreviation for the SIMD implementation is echoed to cout.
    Currently, there is no distinction in the strings emitted for different
    SSE versions, the string emitted is always 'sse'. If there  are any
    arguments to echo_simd_type, they will be prepended to the output.
    This is intended to make it easy for a shell script launching pv to pick
    the appropriate version; use would be something like
    
    BEST_PV=$(echo_simd_type pv_)
    $BEST_PV my_image.jpg
    
    While on Linux systems, using lscpu will provide similar coding options,
    Other systems may provide less covenient access to the information needed.
    Hence this program, to provide a uniform cross-platform way to do the job.
    
    The enum value returned by Vc::bestImplementationSupported is also
    returned as exit code. Here, the value Vc delivers is used as-is,
    differentiating between the different SSE implementations as well.
    
    Note that the default option is to produce scalar code. If new SIMD
    architectures become available, this program has to be changed to
    use them.
*/ 

#include <Vc/Vc>
#include <Vc/support.h>
#include <iostream>

int main ( int argc , char * argv[] )
{
  Vc::Implementation best_supported = Vc::bestImplementationSupported() ; 
  const char * out ;
  
  switch ( best_supported )
  {
    case Vc::AVXImpl:	      // AVX
      out = "avx" ;
      break ;

    case Vc::AVX2Impl:	    // AVX2
      out = "avx2" ;
      break ;
      
    default:    // minimal requirement
      out = "sse" ;
      break ;

  }
  
  for ( int a = 1 ; a < argc ; a++ )
    std::cout << argv[a] ;
  
  std::cout << out << std::endl ;
  std::exit ( best_supported ) ;
}
