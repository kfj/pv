/************************************************************************/
/*                                                                      */
/*          lux - panora and image viewer                               */
/*                                                                      */
/*          Copyright 2016 - 2023 by Kay F. Jahnke                      */
/*                                                                      */
/*  The git repository for this software is at                          */
/*                                                                      */
/*  https://bitbucket.org/kfj/pv                                        */
/*                                                                      */
/*  Please direct questions, bug reports, and contributions to          */
/*                                                                      */
/*  kfjahnke+pv@gmail.com                                               */
/*                                                                      */
/*  pv is free software: you can redistribute it and/or modify          */
/*  it under the terms of the GNU General Public License as published   */
/*  by the Free Software Foundation, either version 3 of the License,   */
/*  or (at your option) any later version.                              */
/*                                                                      */
/*  pv is distributed in the hope that it will be useful,               */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       */
/*  GNU General Public License for more details.                        */
/*                                                                      */
/*  You should have received a copy of the GNU General Public License   */
/*  along with pv.  If not, see <http://www.gnu.org/licenses/>.         */
/*                                                                      */
/************************************************************************/

/*! \file dispatch.h

    \brief code to dispatch to ISA-specific TUs

    This file is #included by the rendering code of lux and provides
    the mechanism to dispatch to ISA-specific rendering code. It
    requires that 'PV_ARCH' is defined by the code #including it.

    This file provides one class definition:

    - a class derived from class dispatch living in namespace PV_ARCH
      which overrides the pure virtual member functions in base class
      dispatch with ISA-specific code

    the signatures for the member functions are kept in a separate
    header "interface.h", to ensure they are consistent. The macro
    SET_PURE is #defined here so that it produces *declarations*
    of overrides. The *definition* of the overrides happens in
    pv_rendering.cc and pv_combine.cc, which are compiled several times,
    each time with PV_ARCH defined to a different value like PV_AVX2 or
    PV_PLAIN, and with corresponding architecture-specific compiler
    flags, like -mavx2, yielding ISA-specific object files (pv_avx2.o,
    pv_plain.o...). In pv_no_rendering.cc, there is code to perform the
    actual dispatch via a base class pointer.
*/

#include "pv_common.h"

namespace PV_ARCH
{
  // declaration of the flavour-specific dispatch object.
  // Here the overrides of the pure virtual methods in the
  // base class are declared.

  struct dispatch : public ::dispatch
  {
    #define SET_PURE(dcl) dcl
    #include "interface.h"
    #undef SET_PURE
  } ;

  // We also declare an object of the flavour-specific dispatch
  // class, as a static const, so it can occur in every TU
  // using this flavour, without conflict.

  static const dispatch _dispatch ;
} ;
