#include <vigra/tinyvector.hxx>

namespace fileio
{
  bool is_image ( std::string filename ) ;

  bool gyrate ( const int & orientation ,
                const long & target_width ,
                const long & target_height ,
                long & xstride ,
                long & ystride ,
                long & _offset ) ;

#ifdef USE_OIIO

  struct image_info
  {
    private:

    std::size_t _w = 0 , _h = 0 ;
    std::string _typestr , _fn ;
    int _nch , _fsz ;

    public:

    image_info ( std::string filename ) ;
    std::size_t width() const ;
    std::size_t height() const ;
    vigra::TinyVector < std::size_t , 2 > shape() const ;
    const char * getFileType() const ;
    const char * getFileName() const ;
    int numExtraBands() const ;
    int pixelType() const ;
    bool isGrayscale() const ;
  } ;

  void import_image ( const image_info & info ,
                      void * data ,
                      std::size_t bytes_per_channel ,
                      std::size_t nchannels ,
                      long x_stride ,
                      long y_stride ) ;

  void import_image_alpha ( const image_info & info ,
                            void * rgb , void * alpha ,
                            std::size_t bytes_per_channel ,
                            std::size_t nchannels ,
                            long x_stride ,
                            long y_stride ,
                            long xa_stride ,
                            long ya_stride ,
                            std::size_t bytes_per_alpha_channel = 0 ) ;

  template < class T , class S >
  void
  importImage ( image_info const & import_info ,
                vigra::MultiArrayView < 2 , T , S > image )
  {
    constexpr std::size_t nchannels = vigra::ExpandElementResult < T > :: size ;
    typedef typename vigra::ExpandElementResult < T > :: type ele_type ;
    constexpr size_t bytes_per_channel = sizeof ( ele_type ) ;

    import_image ( import_info ,
                   image.data() ,
                   bytes_per_channel ,
                   nchannels ,
                   image.stride ( 0 ) ,
                   image.stride ( 1 ) ) ;
                  
  }

  template < class T1 , class T2 , class S1 , class S2 >
  void
  importImageAlpha ( image_info const & import_info ,
                    vigra::MultiArrayView < 2 , T1 , S1 > image ,
                    vigra::MultiArrayView < 2 , T2 , S2 > alpha )
  {
    constexpr std::size_t nchannels = vigra::ExpandElementResult < T1 > :: size ;
    typedef typename vigra::ExpandElementResult < T1 > :: type ele_type ;
    constexpr size_t bytes_per_channel = sizeof ( ele_type ) ;
    constexpr size_t bytes_per_alpha_channel = sizeof ( T2 ) ;

    assert ( image.shape() == alpha.shape() ) ;

    import_image_alpha ( import_info ,
                         image.data() ,
                         alpha.data() ,
                         bytes_per_channel ,
                         nchannels ,
                         image.stride ( 0 ) ,
                         image.stride ( 1 ) ,
                         alpha.stride ( 0 ) ,
                         alpha.stride ( 1 ) ,
                         bytes_per_alpha_channel ) ;
  }

#else

  // when using vigraimpex to provide image_info, we override
  // pixelType(): newer lux code only expects the size of the pixel
  // from this function, as it is returned from the OIIO-based version.

  struct image_info
  : public vigra::ImageImportInfo
  {
    typedef vigra::ImageImportInfo base_t ;

    image_info ( std::string filename )
    : base_t ( filename.c_str() )
    { }

    int pixelType() const
    {
      auto pixel_type = base_t::pixelType() ;
      switch ( pixel_type )
      {
        case vigra::ImageImportInfo::UINT8:
          return 1 ;
        case vigra::ImageImportInfo::UINT16:
          return 2 ;
        case vigra::ImageImportInfo::FLOAT:
        default:
          return 4 ;
      }
    }

  } ;

#endif

} ;
