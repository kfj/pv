/************************************************************************/
/*                                                                      */
/*          pv - a viewer for panoramic images                          */
/*                                                                      */
/*          Copyright 2016 - 2023 by Kay F. Jahnke                      */
/*                                                                      */
/*  The git repository for this software is at                          */
/*                                                                      */
/*  https://bitbucket.org/kfj/pv                                        */
/*                                                                      */
/*  Please direct questions, bug reports, and contributions to          */
/*                                                                      */
/*  kfjahnke+pv@gmail.com                                               */
/*                                                                      */
/*  pv is free software: you can redistribute it and/or modify          */
/*  it under the terms of the GNU General Public License as published   */
/*  by the Free Software Foundation, either version 3 of the License,   */
/*  or (at your option) any later version.                              */
/*                                                                      */
/*  pv is distributed in the hope that it will be useful,               */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       */
/*  GNU General Public License for more details.                        */
/*                                                                      */
/*  You should have received a copy of the GNU General Public License   */
/*  along with pv.  If not, see <http://www.gnu.org/licenses/>.         */
/*                                                                      */
/************************************************************************/

/*! \file pv_metadata.h

    \brief pv - a panorama viewer

    For documentation, please see the bitbucket repository at

    https://bitbucket.org/kfj/pv

    This file implements source image metadata extraction, using libexiv2.
*/

#include "pv_common.h"
#include <exiv2/exiv2.hpp>
#include <regex>
#include <OpenImageIO/imageio.h>
#include <OpenImageIO/typedesc.h>
#include <vigra/tinyvector.hxx>
using namespace OIIO;

#ifndef PV_METADATA_H
#define PV_METADATA_H

// there's an API change with libexiv2 0.28. I detect the version in
// CMakeLists.txt and set EXIV2_USE_TOLONG for minor versions less than 28

#ifdef EXIV2_USE_TOLONG
#define toInt64 toLong
#endif

/// struct metadata is a helper object gleaning source image metrics from
/// an image's metadata. There is one datum which is always extracted
/// (provided it's present): the EXIF orientation. Additional metadata
/// which may be extracted are the image's projection, FOV and offsets.

class process_cropping
{
  static
  double get_ofs ( double fov ,          // uncropped field of view
                  projection_type prj , // image projection
                  long uncropped_size , // uncropped size in pixels
                  long cutoff )         // cutoff in pixels
  {
    double ofs ;
    switch ( prj )
    {
      case RECTILINEAR:
      {
        double half_extent = std::tan ( fov / 2.0 ) ;
        double factor =   ( uncropped_size / 2.0 - cutoff )
                        / ( uncropped_size / 2.0 ) ;
        double cropped_extent = half_extent * factor ;
        ofs = std::atan ( cropped_extent ) ;
        break ;
      }
      case STEREOGRAPHIC:
      {
        double half_extent = std::tan ( fov / 4.0 ) ;
        double factor =   ( uncropped_size / 2.0 - cutoff )
                        / ( uncropped_size / 2.0 ) ;
        double cropped_extent = half_extent * factor ;
        ofs = 2.0 * std::atan ( cropped_extent ) ;
        break ;
      }
      case SPHERICAL:
      {
        double half_extent = fov / 2.0 ;
        double factor =   ( uncropped_size / 2.0 - cutoff )
                        / ( uncropped_size / 2.0 ) ;
        double cropped_extent = half_extent * factor ;
        ofs = cropped_extent ;
        break ;
      }
      default:
        break ;
    }
    return ofs ;
  }

  static
  void set_ofs_and_angle ( double fov ,          // uncropped field of view
                          projection_type prj , // image projection
                          long uncropped_size , // uncropped size in pixels
                          long c0 , long c1 ,   // beginning, end of crop
                          double & ofs ,
                          double & angle )
  {
    ofs = get_ofs ( fov , prj , uncropped_size , c0 ) ;
    angle = ofs + get_ofs ( fov , prj , uncropped_size , uncropped_size - c1 ) ;
    ofs = M_PI - ofs ;
  }

public:

  static
  void set_ofs_and_angle ( double u_hfov ,         // uncropped field of view
                          double u_vfov ,
                          projection_type prj , // image projection
                          long uncropped_width , // uncropped size in pixels
                          long uncropped_height , // uncropped size in pixels
                          long x0 , long x1 ,   // beginning, end of crop
                          long y0 , long y1 ,   // beginning, end of crop
                          double & hofs ,
                          double & hfov ,
                          double & vofs ,
                          double & vfov )
  {
    switch ( prj )
    {
      case RECTILINEAR:
      {
        set_ofs_and_angle ( u_hfov , RECTILINEAR ,
                            uncropped_width , x0 , x1 ,
                            hofs , hfov ) ;
        set_ofs_and_angle ( u_vfov , RECTILINEAR ,
                            uncropped_height , y0 , y1 ,
                            vofs , vfov ) ;
        break ;
      }
      case CYLINDRIC:
      {
        set_ofs_and_angle ( u_hfov , SPHERICAL ,
                            uncropped_width , x0 , x1 ,
                            hofs , hfov ) ;
        set_ofs_and_angle ( u_vfov , RECTILINEAR ,
                            uncropped_height , y0 , y1 ,
                            vofs , vfov ) ;
        break ;
      }
      case STEREOGRAPHIC:
      {
        set_ofs_and_angle ( u_hfov , STEREOGRAPHIC ,
                            uncropped_width , x0 , x1 ,
                            hofs , hfov ) ;
        set_ofs_and_angle ( u_vfov , STEREOGRAPHIC ,
                            uncropped_height , y0 , y1 ,
                            vofs , vfov ) ;
        break ;
      }
      case SPHERICAL:
      case FISHEYE:
      default:
      {
        set_ofs_and_angle ( u_hfov , SPHERICAL ,
                            uncropped_width , x0 , x1 ,
                            hofs , hfov ) ;
        set_ofs_and_angle ( u_vfov , SPHERICAL ,
                            uncropped_height , y0 , y1 ,
                            vofs , vfov ) ;
        break ;
      }
    }
  }
} ;

namespace fileio
{
  void add_oiio_args ( ImageSpec & config ) ;
}

struct metadata_type
{
  projection_type projection ;
  int subimages ;

  double hugin_hfov ;
  double hugin_vfov ;
  double gpano_hfov ;
  double gpano_vfov ;
  double gpano_hofs ;
  double gpano_vofs ;

  long exif_orientation ;
  double exif_hfov ;
  double exif_vfov ;
  double exif_se_f ;

  std::string lux_version_string ;
  std::string lux_projection ;
  double lux_uncropped_hfov ;
  double lux_uncropped_vfov ;
  long lux_uncropped_width ;
  long lux_uncropped_height ;
  bool lux_cropping_active ;
  long lux_cropped_width ;
  long lux_cropped_height ;
  long lux_crop_x0 ;
  long lux_crop_y0 ;
  long lux_crop_x1 ;
  long lux_crop_y1 ;

  double lux_hofs ;
  double lux_hfov ;
  double lux_vofs ;
  double lux_vfov ;

  bool get_float_value ( Exiv2::ExifData & exifData ,
                         const std::string & key_string ,
                         float & _result )
  {
    auto key = Exiv2::ExifKey ( key_string ) ;

    auto result = exifData.findKey ( key ) ;
    if ( result != exifData.end() )
    {
      std::cout << result->key() << ": "
                << result->value() << std::endl ;

      _result = result->value().toFloat() ;
      return true ;
    }
    return false ;
  }

  bool get_long_value ( Exiv2::ExifData & exifData ,
                        const std::string & key_string ,
                        long & _result )
  {
    auto key = Exiv2::ExifKey ( key_string ) ;

    auto result = exifData.findKey ( key ) ;
    if ( result != exifData.end() )
    {
      std::cout << result->key() << ": "
                << result->value() << std::endl ;

      _result = result->value().toInt64() ;
      return true ;
    }
    return false ;
  }

  bool get_string_value ( Exiv2::ExifData & exifData ,
                          const std::string & key_string ,
                          std::string & _result )
  {
    auto key = Exiv2::ExifKey ( key_string ) ;

    auto result = exifData.findKey ( key ) ;
    if ( result != exifData.end() )
    {
      std::cout << result->key() << ": "
                << result->value() << std::endl ;

      _result = result->value().toString() ;
      return true ;
    }
    return false ;
  }

  std::vector < std::string > metadata_value_v ;

  metadata_type ( const char * filename ,
                  bool read_geometry ,
                  const std::vector < std::string > & metadata_query_v ,
                  double crop_factor = 0.0 )
  : metadata_value_v ( metadata_query_v.size() )
  {
    projection = PRJ_NONE ;
    hugin_hfov = 0.0 ;
    hugin_vfov = 0.0 ;
    gpano_hfov = 0.0 ;
    gpano_vfov = 0.0 ;
    gpano_hofs = -1.0 ;
    gpano_vofs = -1.0 ;
    exif_orientation = 0 ;
    exif_hfov = 0.0 ;
    exif_vfov = 0.0 ;
    exif_se_f = 0.0 ;

    for ( int i = 0 ; i < metadata_query_v.size() ; i++ )
    {
      metadata_value_v [ i ] = "---" ;
    }

    try
    {
      // get the EXIF orientation with OpenImageIO which has a wider
      // scope than libexiv2. We need to set up an ImageSpec for
      // configuration, incliding the oiio args from the CL or GUI.

      ImageSpec config ;
      config [ "oiio:UnassociatedAlpha" ] = 1 ;
      config [ "raw:auto_bright" ] = 1 ;
      fileio::add_oiio_args ( config  ) ;

      auto inp = ImageInput::open ( filename , &config ) ;
      const ImageSpec &spec = inp->spec();

      exif_orientation = spec.get_int_attribute ( "Orientation" , 1 ) ;
      std::cout << "OIIO provides this Orientation: "
                << exif_orientation << std::endl ;

      subimages = spec.get_int_attribute ( "oiio:subimages" , 1 ) ;
      std::cout << "OIIO provides this no. of subimages: "
                << subimages << std::endl ;

      if ( read_geometry )
      {
        float foc_l , foc_l_35 ;

        bool have_foc_l = spec.getattribute
          ( "Exif:FocalLength" , TypeFloat , &foc_l ) ;

        bool have_foc_l_35 = spec.getattribute
          ( "Exif:FocalLengthIn35mmFilm" , TypeFloat , &foc_l_35 ) ;

        // if the camera has a lens mounted which provides no information,
        // the focal length can come out 0.0. We handle this as if the
        // focal length actually wasn't present:

        if ( have_foc_l && foc_l == 0.0 )
          have_foc_l = false ;

        if ( have_foc_l_35 && foc_l_35 == 0.0 )
          have_foc_l = false ;

        // if we don't have a crop factor override, but both focal length
        // tags, we can calculate the crop factor. If we do have a crop
        // factor override, one of the focal length tags is enough to figure
        // out the other.

        if ( crop_factor == 0.0 )
        {
          if ( have_foc_l_35 && have_foc_l )
            crop_factor = foc_l_35 / foc_l ;
        }
        else
        {
          if ( have_foc_l_35 && have_foc_l )
          {
            double exif_crop_factor = foc_l_35 / foc_l ;
            if ( fabs ( exif_crop_factor - crop_factor ) > .01 )
            {
              std::cout << "warning: exif tags yield crop factor: "
                        << exif_crop_factor << std::endl ;
              std::cout << "lux will use the override value: "
                        << crop_factor << std::endl ;
            }
          }

          if ( have_foc_l && ( ! have_foc_l_35 ) )
          {
            foc_l_35 = foc_l * crop_factor ;
            have_foc_l_35 = true ;
          }

          if ( have_foc_l_35 && ( ! have_foc_l ) )
          {
            foc_l = foc_l_35 / crop_factor ;
            have_foc_l = true ;
          }
        }

        // we want to figure out the FOV from the image metadata.
        // First we try the route via exif tags written by the camera.
        // In any case, we need the sensor's extent in pixel units.
        // First attempt: go via Iop.RelatedImageXXX

        long w = 0 , h = 0 ;

        bool have_wh = spec.getattribute
          ( "Exif:RelatedImageWidth" , TypeInt , &w ) ;

        have_wh &= spec.getattribute
          ( "Exif:RelatedImageLength" , TypeInt , &h ) ;

        // if that didn't work, we try ImageWidth/Length

        if ( ! have_wh )
        {
          have_wh = spec.getattribute
            ( "Exif:ImageWidth" , TypeInt , &w ) ;

          have_wh &= spec.getattribute
            ( "Exif:ImageLength" , TypeInt , &h ) ;
        }

        // if that did not work, we try Photo.PixelDimension

        if ( ! have_wh )
        {
          have_wh = spec.getattribute
            ( "Exif:PixelXDimension" , TypeInt , &w ) ;

          have_wh &= spec.getattribute
            ( "Exif:PixelYDimension" , TypeInt , &h ) ;
        }

        if ( have_wh )
        {
          std::cout << "found image w, h in metadata "
                    << w << " " << h << std::endl ;

          // we have the sensor's width and height in pixel units.
          // If we have the crop factor already - from exif data or
          // passed in as an override - we can calculate the FOV:

          if ( crop_factor != 0.0 )
          {
            assert ( have_foc_l_35 ) ;

            exif_hfov = 2.0 * atan ( 35.0 / ( 2.0 * foc_l_35 ) ) ;
            exif_vfov = 2.0 * atan ( ( 35.0 * h / w ) / ( 2.0 * foc_l_35 ) ) ;
            exif_se_f = 35.0 / foc_l_35 ;

            std::cout << "exif_hfov: " << exif_hfov* 180.0 / M_PI 
                      << std::endl ;

            std::cout << "exif_vfov: " << exif_vfov* 180.0 / M_PI
                      << std::endl ;

            std::cout << "exif_se_f: " << exif_se_f << "f"
                      << std::endl ;
          }
          else
          {
            // If we don't have the crop factor, we try and get the focal
            // plane resolution to figure out the sensor extent in mm,
            // which, together with the focal length, also gives the FOV.

            // first we get the unit: 2 is for inches, 3 for mm. If this
            // tag is not found, we assume inches.

            long res_unit = 2 ; // inches per default
            float to_mm , xres , yres ;

            spec.getattribute
              ( "Exif:ResolutionUnit" , TypeInt , &res_unit ) ;

            if ( res_unit == 2 )
              to_mm = 25.4 ;
            else if ( res_unit == 3 )
              to_mm = 10.0 ;

            std::cout << "setting factor to covert to mm to " << to_mm << std::endl ;

            bool have_res = spec.getattribute
              ( "Exif:FocalPlaneXResolution" , TypeFloat , &xres ) ;

            have_res &= spec.getattribute
              ( "Exif:FocalPlaneYResolution" , TypeFloat , &yres ) ;

            if ( have_res )
            {
              // we have FocalPlaneResolution and sensor extent in pixel
              // units, so we can infer the sensor extent in mm. With it,
              // we can find the focal length, and then we can calculate
              // hfov and vfov

              float sens_w = to_mm * w / xres ;
              float sens_h = to_mm * h / yres ;

              std::cout << "sensor width  in mm: " << sens_w << std::endl ;
              std::cout << "sensor height in mm: " << sens_h << std::endl ;

              if ( have_foc_l )
              {
                std::cout << "have_foc_l is true, foc_l = " << foc_l
                          << "mm" << std::endl ;

                exif_hfov = 2.0 * atan ( sens_w / ( 2.0 * foc_l ) ) ;
                exif_vfov = 2.0 * atan ( sens_h / ( 2.0 * foc_l ) ) ;
                exif_se_f = sens_w / foc_l ;

                std::cout << "exif_hfov: " << exif_hfov* 180.0 / M_PI << std::endl ;
                std::cout << "exif_vfov: " << exif_vfov* 180.0 / M_PI << std::endl ;
                std::cout << "exif_se_f: " << exif_se_f << "f" << std::endl ;
              }
            }
          }
        }

        // if we did get exif_hfov, we assume it's a rectilinear image.
        // We can't really tell, because there is no exif tag to tell
        // us. We'd have to check for a specific lens model, but this
        // is - for now - beyond our scope; we might employ a lens
        // database, but even that isn't certain to work: if the image
        // was taken with a lens the camera doesn't know about (like the
        // Samyang fisheye on my Canon EOS) the images don't contain
        // lens information unless I add it 'manually' - in which case
        // I'd rather add, e.g., the UserComment exif tag with hfov,
        // vfov, and projection values as hugin would provide them.
        // That's a better solution to get a correct display in lux.
        // This is one reason why gleaning the FOV from camera-provided
        // exif data is treated as a 'last resort' in lux - the other
        // ways (UserComment, GPano, lux' own) are preferred.

        if ( exif_hfov != 0.0 )
          projection = RECTILINEAR ;
      }
      inp->close() ;

      // for the remainder of the metadata, we switch to libexiv2
      // libexiv2 has good documentation for all the available tags,
      // and it can also handle XMP metadata, which we use for the
      // lux-specific projection and extent metadata.

      auto image = Exiv2::ImageFactory::open ( filename ) ;
      assert ( image.get() != 0 ) ;
      image->readMetadata() ;

      Exiv2::ExifData &exifData = image->exifData() ;

      if ( ! exifData.empty() )
      {
        // get more metadata if they are required for the status line

        for ( int i = 0 ; i < metadata_query_v.size() ; i++ )
        {
          auto key = Exiv2::ExifKey ( metadata_query_v [ i ] ) ;

          try
          {
            auto result = exifData.findKey ( key ) ;
            if ( result != exifData.end() )
            {
              std::cout << metadata_query_v [ i ] << ": "
                        << result->value() << std::endl ;
              metadata_value_v [ i ] = result->value().toString() ;
            }
            else
            {
              std::cout << metadata_query_v [ i ] << ": "
                        << "key not found" << std::endl ;
            }
          }
          catch ( ... )
          {
            std::cout << "metadata access failed ("
                      << metadata_query_v [ i ] << ")" << std::endl ;
          }
        }

        if ( ! read_geometry )
        {
          // if read_geometry is false, we're done now.

          return ;
        }

        // hugin provides the UserComment exif tag, which gives two fov values,
        // but when the panorama is cropped, these values are not usable.
        // There is no way of extracting or regenerating the cropping
        // information from the image with the data at hand. I proposed to
        // extend the field to give the extent corresponding to the uncropped
        // area, plus the cropping rectangle, but so far this has not been
        // implemented.
        // I allow passing the hfov only, omitting the part giving the vfov.
        // This makes it easier for users who want to add this tag for a given
        // panorama, where it's quite easy to figure out a useful hfov, and the
        // vfov should be calculated automatically, given the projection.
        // Let's see if we can find a UserComment tag:

        auto key = Exiv2::ExifKey ( std::string ( "Exif.Photo.UserComment" ) ) ;
        auto result = exifData.findKey ( key ) ;
        if ( result != exifData.end() )
        {
          auto user_comment = result->value().toString() ;

          // we'll scan the UserComment tag with two regular expressions.
          // First we look for a field of view value, afterwards, we look
          // for a projection value. Only if both are found and recognized,
          // we set the 'projection', 'hugin_hfov', and, optionally 'hugin_vfov'
          // member variables.

          // the RE for the fov is quite complex, in words it's 'Fov' followed
          // by the hfov and, optionally, letter X followed by the vfov. If the
          // vfov part is omitted, lux will calculate the vfov from the hfov
          // and the projection 'further down the line'.

          std::regex re_fov ( "[Ff][Oo][Vv]:?\\W*([0-9]*[.][0-9]*|[0-9]+)(\\W*[Xx]\\W*([0-9]*[.][0-9]*|[0-9]+))?" ) ;
          std::smatch match ;

          if ( std::regex_search ( user_comment , match , re_fov ) )
          {
            hugin_hfov = std::stod ( match[1] ) * M_PI / 180.0 ;
            if ( match[3].length() )
              hugin_vfov = std::stod ( match[3] ) * M_PI / 180.0 ;

            // we found usable fov values, let's see if the projection is
            // also there. If so, we directly set 'projection' - this is our
            // 'first shot' at the projection and may be overruled if GPano
            // or lux metadata are found as well.

            std::regex re_projection ( "[Pp]rojection:?\\W*([A-Za-z]+)" ) ;

            if ( std::regex_search ( user_comment , match , re_projection ) )
            {
              if ( match[1] == "Equirectangular" )
              {
                projection = SPHERICAL ;
              }
              else if ( match[1] == "Rectilinear" )
              {
                projection = RECTILINEAR ;
              }
              else if ( match[1] == "Cylindrical" )
              {
                projection = CYLINDRIC ;
              }
              else if ( match[1] == "Stereographic" )
              {
                projection = STEREOGRAPHIC ;
              }
              else if ( match[1] == "Fisheye" )
              {
                projection = FISHEYE ;
              }
              // TODO: ad hoc
              else if ( match[1] == "Mosaic" )
              {
                projection = MOSAIC ;
              }
            }
          }
        }

        // Now we see if we can find XMP metadata. There are two 'flavours'
        // of XMP metadata which can help us figure out the geometry: GPano
        // metadata are good for images in spherical projection, and lux
        // metadata are good for all projections lux can handle.

        Exiv2::XmpData &xmpData = image->xmpData();
        if ( ! xmpData.empty() )
        {
          // First we look for lux metadata. Images made with lux have these
          // metadata set, and they are the best source of geometry data,
          // because they work for all projections.

          lux_version_string = std::string() ;
          Exiv2::XmpKey key ( std::string ( "Xmp.dc.lux_version" ) ) ;
          auto result = xmpData.findKey ( key ) ;
          if ( result != xmpData.end() )
          {
            lux_version_string = result->value().toString() ;
          }

          if ( lux_version_string != std::string() )
          {
            // std::cout << "found lux metadata, version "
            //           << lux_version_string << std::endl ;

            lux_projection = std::string() ;
            key = Exiv2::XmpKey ( std::string ( "Xmp.dc.projection" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              lux_projection = result->value().toString() ;
              for ( auto & ch : lux_projection )
                ch = std::tolower ( ch ) ;
              if ( lux_projection == "spherical" )
                projection = SPHERICAL ;
              else if ( lux_projection == "cylindric" )
                projection = CYLINDRIC ;
              else if ( lux_projection == "rectilinear" )
                projection = RECTILINEAR ;
              else if ( lux_projection == "mosaic" )
                projection = MOSAIC ;
              else if ( lux_projection == "stereographic" )
                projection = STEREOGRAPHIC ;
              else if ( lux_projection == "fisheye" )
                projection = FISHEYE ;
              else
                projection = PRJ_NONE ;
            }

            lux_uncropped_hfov = 0.0 ;
            key = Exiv2::XmpKey ( std::string ( "Xmp.dc.uncropped_hfov" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              lux_uncropped_hfov = result->value().toFloat() * M_PI / 180.0 ;
            }

            lux_uncropped_vfov = 0.0 ;
            key = Exiv2::XmpKey ( std::string ( "Xmp.dc.uncropped_vfov" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              lux_uncropped_vfov = result->value().toFloat() * M_PI / 180.0 ;
            }

            lux_uncropped_width = -1 ;
            key = Exiv2::XmpKey ( std::string ( "Xmp.dc.uncropped_width" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              lux_uncropped_width = result->value().toInt64() ;
            }

            lux_uncropped_height = -1 ;
            key = Exiv2::XmpKey ( std::string ( "Xmp.dc.uncropped_height" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              lux_uncropped_height = result->value().toInt64() ;
            }

            lux_cropping_active = false ;
            key = Exiv2::XmpKey ( std::string ( "Xmp.dc.cropping_active" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              lux_cropping_active = result->value().toInt64() ;
            }

            lux_cropped_width = -1 ;
            key = Exiv2::XmpKey ( std::string ( "Xmp.dc.cropped_width" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              lux_cropped_width = result->value().toInt64() ;
            }

            lux_cropped_height = -1 ;
            key = Exiv2::XmpKey ( std::string ( "Xmp.dc.cropped_height" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              lux_cropped_height = result->value().toInt64() ;
            }

            lux_crop_x0 = -1 ;
            key = Exiv2::XmpKey ( std::string ( "Xmp.dc.crop_x0" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              lux_crop_x0 = result->value().toInt64() ;
            }

            lux_crop_y0 = -1 ;
            key = Exiv2::XmpKey ( std::string ( "Xmp.dc.crop_y0" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              lux_crop_y0 = result->value().toInt64() ;
            }

            lux_crop_x1 = -1 ;
            key = Exiv2::XmpKey ( std::string ( "Xmp.dc.crop_x1" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              lux_crop_x1 = result->value().toInt64() ;
            }

            lux_crop_y1 = -1 ;
            key = Exiv2::XmpKey ( std::string ( "Xmp.dc.crop_y1" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              lux_crop_y1 = result->value().toInt64() ;
            }

            if ( lux_cropping_active )
            {
              process_cropping::set_ofs_and_angle
              (
                lux_uncropped_hfov ,
                lux_uncropped_vfov ,
                projection ,
                lux_uncropped_width ,
                lux_uncropped_height ,
                lux_crop_x0 ,
                lux_crop_x1 ,
                lux_crop_y0 ,
                lux_crop_y1 ,
                lux_hofs ,
                lux_hfov ,
                lux_vofs ,
                lux_vfov
              ) ;
            }
            else
            {
              lux_hofs = M_PI - lux_uncropped_hfov / 2.0 ;
              lux_vofs = M_PI - lux_uncropped_vfov / 2.0 ;
              lux_hfov = lux_uncropped_hfov ;
              lux_vfov = lux_uncropped_vfov ;
            }
          }
          else
          {
            // next we look for GPano XMP data. Later versions of hugin can set
            // these for equirectangular panoramas, and if we find them,
            // we give them preference, because they give correct cropping info.
            // Sadly, the specs for GPano metadata don't quite work for other
            // projections.

            // TODO: doublecheck, currently has no effect:

            key = Exiv2::XmpKey ( std::string ( "Xmp.GPano.UsePanoramaViewer" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              if ( result->value().toString() == std::string ( "True" ) )
                std::cout << "Xmp.GPano.UsePanoramaViewer is True" << std::endl ;
            }

            // let's extract the GPano data

            long FullPanoHeightPixels = -1 ;
            long FullPanoWidthPixels = -1 ;
            long CroppedAreaImageHeightPixels = -1 ;
            long CroppedAreaImageWidthPixels = -1 ;
            long CroppedAreaLeftPixels = -1 ;
            long CroppedAreaTopPixels = -1 ;

            key = Exiv2::XmpKey ( std::string ( "Xmp.GPano.FullPanoHeightPixels" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              FullPanoHeightPixels = result->value().toInt64() ;
            }

            key = Exiv2::XmpKey ( std::string ( "Xmp.GPano.FullPanoWidthPixels" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              FullPanoWidthPixels = result->value().toInt64() ;
            }

            key = Exiv2::XmpKey ( std::string ( "Xmp.GPano.CroppedAreaImageHeightPixels" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              CroppedAreaImageHeightPixels = result->value().toInt64() ;
            }

            key = Exiv2::XmpKey ( std::string ( "Xmp.GPano.CroppedAreaImageWidthPixels" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              CroppedAreaImageWidthPixels = result->value().toInt64() ;
            }

            key = Exiv2::XmpKey ( std::string ( "Xmp.GPano.CroppedAreaLeftPixels" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              CroppedAreaLeftPixels = result->value().toInt64() ;
            }

            key = Exiv2::XmpKey ( std::string ( "Xmp.GPano.CroppedAreaTopPixels" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              CroppedAreaTopPixels = result->value().toInt64() ;
            }

            // currently we only process GPano data if projection is equirectangular

            key = Exiv2::XmpKey ( std::string ( "Xmp.GPano.ProjectionType" ) ) ;
            result = xmpData.findKey ( key ) ;
            if ( result != xmpData.end() )
            {
              auto ProjectionType = result->value().toString() ;
              if ( ProjectionType == std::string ( "equirectangular" ) )
              {
                // winner. it's a spherical, so we can use the GPano data

                projection = SPHERICAL ;

                gpano_hfov = 2.0 * M_PI * CroppedAreaImageWidthPixels
                                      / FullPanoWidthPixels ;

                gpano_hofs = 2.0 * M_PI * CroppedAreaLeftPixels
                                      / FullPanoWidthPixels ;

                gpano_vfov = M_PI * CroppedAreaImageHeightPixels
                                      / FullPanoHeightPixels ;

                gpano_vofs = M_PI * CroppedAreaTopPixels
                                      / FullPanoHeightPixels ;


                std::cout << "FullPanoWidthPixels "
                          << FullPanoWidthPixels << std::endl ;

                std::cout << "CroppedAreaLeftPixels "
                          << CroppedAreaLeftPixels << std::endl ;

                std::cout << "CroppedAreaImageWidthPixels "
                          << CroppedAreaImageWidthPixels << std::endl ;

                std::cout << "FullPanoHeightPixels "
                          << FullPanoHeightPixels << std::endl ;

                std::cout << "CroppedAreaTopPixels "
                          << CroppedAreaTopPixels << std::endl ;

                std::cout << "CroppedAreaImageHeightPixels "
                          << CroppedAreaImageHeightPixels << std::endl ;
              }
              else
              {
                // is there ever another projection in GPano data?

                std::cout << "unrecognized GPano projection: " << ProjectionType << std::endl ;
                projection = MOSAIC ;
              }
            }
          }
        }
      }
    }
    catch ( ... )
    {
      std::cout << "an exception occured during metadata retrieval" << std::endl ;
    }
  }
} ;

#endif // PV_METADATA_H
