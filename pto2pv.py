import sys
import math
import parse_pto

# scan the pro file

sc=parse_pto.pto_scan ( sys.argv[1] )

# extract the set of image lines

img=sc.get_lines_like('i')

# helper function: given a pto_member object with a 'back reference',
# produce the member this back reference refers to. So if we have,
# for example, v=0, we get img[0], and img[0].v is the hfov we're
# looking for

def resolve ( member ) :
  if ( member.datatype == 'b' ) :
    index = member.value
    return getattr ( img [ index ] , member.type )
  else :
    return member

# get the average Ev as reference

output=sc.get_lines_like('p')
Eev_out = 0.0

for line in img :
  Eev = resolve ( line.Eev )
  Eev_out += float ( Eev.value )

Eev_out /= len ( img )

# That's about it for coding effort, now we can simply gobble up
# the image lines and print out the ini file. Invoke this script
# like
#
# python pto2pv.py input.pto > output.lux
#
# then you can open the result with
#
# lux output.lux

print ( "# facet map generated from %s" % sys.argv[1] )
print ( "" )
print ( "projection=facet_map" )
print ( "fully_covered=no" )
print ( "input_is_pto=yes" )
print ( "process_linear=yes" )
print ( "" )

for line in img :

  # get the facet image's name

  name = line.n
  print ( "facet=%s" % name.value )

  # it's horizontal field of view

  hfov = resolve ( line.v )
  print ( "facet_hfov=%f" % hfov.value )

  # it's projection

  prj = resolve ( line.f )
  prj = prj.value

  if prj == 0 :
    print ( "facet_projection=rectilinear" )
  if prj == 1 :
    print ( "facet_projection=cylindric" )
  # not quite sure about the two types of fisheye...
  if prj == 2 :
    print ( "facet_projection=fisheye" )
  if prj == 3 :
    print ( "facet_projection=fisheye" )
  if prj == 4 :
    print ( "facet_projection=spherical" )
  if prj == 10 :
    print ( "facet_projection=stereographic" )

  # adapting brightness is now in lux, but the result is not yet
  # always good. I suspect that simply converting the Ev value
  # from a PTO script is not sufficient, because there are more
  # parameters affecting light (camera response curve) which lux
  # does not know about (yet)

  Eev = resolve ( line.Eev )
  dEv = float(Eev.value) - float(Eev_out)
  brightness = math.pow ( 2.0 , dEv )
  print ( "facet_brightness=%f" % brightness )

  # orientation of the facets: yaw, pich, roll

  yaw = resolve ( line.y )
  print ( "facet_yaw=%f" % yaw.value )

  pitch = resolve ( line.p )
  print ( "facet_pitch=%f" % pitch.value )

  roll = resolve ( line.r )
  print ( "facet_roll=%f" % ( roll.value ) )

  # lens correction parameters

  lca = resolve ( line.a )
  lcb = resolve ( line.b )
  lcc = resolve ( line.c )
  lch = resolve ( line.d )
  lcv = resolve ( line.e )

  print ( "facet_lca=%f" % lca.value )
  print ( "facet_lcb=%f" % lcb.value )
  print ( "facet_lcc=%f" % lcc.value )
  print ( "facet_lch=%f" % lch.value )
  print ( "facet_lcv=%f" % lcv.value )

  # vignetting correction parameters

  vca = resolve ( line.Va )
  vcb = resolve ( line.Vb )
  vcc = resolve ( line.Vc )
  vcd = resolve ( line.Vd )
  vcx = resolve ( line.Vx )
  vcy = resolve ( line.Vy )

  print ( "facet_vca=%f" % vca.value )
  print ( "facet_vcb=%f" % vcb.value )
  print ( "facet_vcc=%f" % vcc.value )
  print ( "facet_vcd=%f" % vcd.value )
  print ( "facet_vcx=%f" % vcx.value )
  print ( "facet_vcy=%f" % vcy.value )

  print ( "" )

