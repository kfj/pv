#!/usr/bin/env bash

# lux - a free panorama and image viewer.
# This script creates a folder with a "distributable" binary and copies the  needed 3rd party libraries into the
# subfolder lib. It will set the 'install_name' for each library so that it references
# the internal Libraries "lib" directory. The Script will change every library it can find. 
# Each of these libraries need to have an absolute install path so we can copy them.
# 
# This script should be run from the scripts directory and expects the compiled lux
# in the cmake build directory.

# Version 20210411, HvdW, First version

Version="$(cat ../pv_version_first).$(cat ../pv_version_second).$(cat ../pv_version_third)"

############ Variables ######################
BASE_DIR=$(pwd)
BINARY=lux
BINDIR=MacOS
ORG_LIB_DIR="/opt/local/lib"
APPLICATION_NAME="LUX"
DIST_DIR="LUX"
LIB_DIR="${DIST_DIR}/lib"
RPATH_LIBRARY_PATH="@executable_path/lib"


############ Functions ###########################
get_libraries() {
  local LIBRARIES=$(echo $(otool -L $1 | grep -v ${RPATH_LIBRARY_PATH} - | grep -v \/System\/Library - | grep -v \/usr\/lib - | sed -ne '1!p' | sed -e 's/(.*)//' | sort -u))
  if [ -n "$LIBRARIES" ]; then
    for library in $LIBRARIES
    do
      update_library $library $1
    done
  fi
  install_name_tool -delete_rpath "${ORG_LIB_DIR}/mac/ExternalPrograms/repository/lib" $1 2&>/dev/null
}

resolve_symlink(){
  local lib=$1
  while [ -L "${lib}" ]; do
    lib="$(dirname ${lib})/$(readlink ${lib})"
  done
  echo "${lib}"
}

update_library() {
  local lib="$1"
  local bin="$2"
  
  local real_lib=$(resolve_symlink ${lib})
  local real_lib_file=$(basename ${real_lib})
  if [ ! -f "${BASE_DIR}/${LIB_DIR}/${real_lib_file}" ]; then 
    echo "* Installing Library -->$1<-- into ${LIB_DIR} "
    cp "${real_lib}" "${BASE_DIR}/${LIB_DIR}" || exit 1
    install_name_tool -id "${RPATH_LIBRARY_PATH}/${real_lib_file}" "${BASE_DIR}/${LIB_DIR}/${real_lib_file}"
    chmod 755 "${BASE_DIR}/${LIB_DIR}/${real_lib_file}"
    get_libraries "${BASE_DIR}/${LIB_DIR}/${real_lib_file}"
  fi
  install_name_tool -change "${lib}" "${RPATH_LIBRARY_PATH}/${real_lib_file}" "${bin}"
}


###################### main ##################################################
echo "*-----------------------------------------------------------*"
echo "* This will create a distributable binary inside a folder ${DIST_DIR}"
echo "* It will copy support libraries located in ${ORG_LIB_DIR} into ${LIB_DIR}"
printf "* And it will set the rpath to internal lib path\n\n"
printf "* It expects the compiled lux in the cmake build folder.\n\n"

printf "* Delete and recreate the Distributable folder\n"
rm -rf ${DIST_DIR}
mkdir -p ${DIST_DIR}
mkdir -p "${LIB_DIR}"

printf "* First copy lux and the .ttf into our ${DIST_DIR}\n"
if [ -f ../build/${BINARY} ];
then
  cp ../build/$BINARY "${DIST_DIR}"
else
  printf "\n\n  !!!! The $BINARY binary is not available in the cmake build directory !!!!\n"
  printf "  !!!! I will exit now !!!!\n\n"
  exit 1;
fi

cp ../Sansation_Regular.ttf "${DIST_DIR}"
cp ../Sansation_1.31_ReadMe.txt "${DIST_DIR}"

printf "\n* Now copy the 3rd party libraries and correct the internal rpath in all libraries and in $BINARY\n\n"
get_libraries ${BASE_DIR}/${DIST_DIR}/$BINARY


printf "\n\n* Now we will create the zip to distribute the app\n\n"
rm -rf ${DIST_DIR}-${Version}.zip
cp ../README.rst ${DIST_DIR}/README.txt
cp ../LICENSE ${DIST_DIR}

zip -r ${DIST_DIR}-${Version}-distributable-MacOS.zip ${DIST_DIR}

