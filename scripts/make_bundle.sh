#!/usr/bin/env bash

# lux - a free panorama and image viewer.
# This script creates an Apple App bundle and copies the  needed 3rd party libraries into the
# application bundle. It will set the 'install_name' for each library so that it references
# the internal Libraries directory. The Script will change every library it can find. 
# Each of these libraries need to have an absolute install path so we can copy them.
# 
# This script should be run from the scripts directory and expects the compiled lux
# in the cmake build directory.

# Version 20210411, HvdW, update for name change pv to lux, plus fow other enhancements
# Version 20210312, HvdW, Add version from the pv_version files; use $BINARY more consequent
# Version 20210306, HvdW, Add version to Info.plist and use version for dmg identification

Version="$(cat ../pv_version_first).$(cat ../pv_version_second).$(cat ../pv_version_third)"

############ Variables ######################
BASE_DIR=$(pwd)
BINARY=lux
BINDIR=MacOS
ORG_LIB_DIR="/opt/local/lib"
APPLICATION_NAME="LUX"
APPLICATION_APP_BUNDLE="${APPLICATION_NAME}.app"
APPLICATION_BINDIR="${APPLICATION_APP_BUNDLE}/Contents/MacOS"
APPLICATION_APP_NAME="${APPLICATION_BINDIR}/${APPLICATION_NAME}"
PLUGINS_PATH="${APPLICATION_APP_BUNDLE}/Contents/Libraries"
RESOURCES_PATH="${APPLICATION_APP_BUNDLE}/Contents/Resources"
RPATH_LIBRARY_PATH="@executable_path/../Libraries"


############ Functions ###########################
get_libraries() {
  local LIBRARIES=$(echo $(otool -L $1 | grep -v ${RPATH_LIBRARY_PATH} - | grep -v \/System\/Library - | grep -v \/usr\/lib - | sed -ne '1!p' | sed -e 's/(.*)//' | sort -u))
  if [ -n "$LIBRARIES" ]; then
    for library in $LIBRARIES
    do
      update_library $library $1
    done
  fi
  install_name_tool -delete_rpath "${ORG_LIB_DIR}/mac/ExternalPrograms/repository/lib" $1 2&>/dev/null
}

resolve_symlink(){
  local lib=$1
  while [ -L "${lib}" ]; do
    lib="$(dirname ${lib})/$(readlink ${lib})"
  done
  echo "${lib}" | sed 's.@rpath./usr/local/lib'
}

update_library() {
  local lib="$1"
  local bin="$2"
  
  local real_lib=$(resolve_symlink ${lib})
  local real_lib_file=$(basename ${real_lib})
  if [ ! -f "${BASE_DIR}/${PLUGINS_PATH}/${real_lib_file}" ]; then 
    echo "* Installing Library -->$1<-- into ${APPLICATION_APP_BUNDLE} " 
    cp "${real_lib}" "${BASE_DIR}/${PLUGINS_PATH}" || exit 1
    install_name_tool -id "${RPATH_LIBRARY_PATH}/${real_lib_file}" "${BASE_DIR}/${PLUGINS_PATH}/${real_lib_file}"
    chmod 755 "${BASE_DIR}/${PLUGINS_PATH}/${real_lib_file}"
    get_libraries "${BASE_DIR}/${PLUGINS_PATH}/${real_lib_file}"
  fi
  install_name_tool -change "${lib}" "${RPATH_LIBRARY_PATH}/${real_lib_file}" "${bin}"
}


###################### main ##################################################
echo "*-----------------------------------------------------------*"
echo "* This will create an app bundle ${APPLICATION_APP_BUNDLE}"
echo "* It will copy support libraries located in ${ORG_LIB_DIR} into ${APPLICATION_APP_BUNDLE}"
printf "* And it will set the rpath to internal Libraries path\n\n"
printf "* This script is supposed to be run from the mac-bundle folder.\n"
printf "* It expects the compiled lux in the cmake build folder.\n\n"

printf "* Delete and recreate basic App bundle\n"
rm -rf ${APPLICATION_APP_BUNDLE}
mkdir -p ${APPLICATION_BINDIR}
mkdir -p "${PLUGINS_PATH}"
mkdir -p "${RESOURCES_PATH}"

printf "* First copy lux, the .ttf, the .icns into our ${APPLICATION_APP_BUNDLE}\n"
if [ -f ../build/${BINARY} ];
then
  cp ../build/$BINARY "${APPLICATION_BINDIR}"
else
  printf "\n\n  !!!! The $BINARY binary is not available in the cmake build directory !!!!\n"
  printf "  !!!! I will exit now !!!!\n\n"
  exit 1;
fi
cp ../NotoSans-Regular.ttf "${APPLICATION_BINDIR}"
cp ../noto_font_license.txt "${APPLICATION_BINDIR}"
cp Lux.icns "${RESOURCES_PATH}"

printf "* Update the VersionString to $Version and copy Info.plist into our ${APPLICATION_APP_BUNDLE}\n\n"
sed -e "s+VersionString+$Version+" Info.plist.base > "${APPLICATION_APP_BUNDLE}/Contents"/Info.plist


printf "\n* Now copy the 3rd party libraries and correct the internal rpath in all libraries and in $BINARY\n\n"
get_libraries ${BASE_DIR}/${APPLICATION_BINDIR}/$BINARY


printf "\n\n* Now we will create the dmg to distribute the app\n\n"
rm -rf bundle
rm -rf *.dmg
mkdir -p bundle
cp ../README.rst bundle/README.txt
cp ../noto_font_license.txt bundle/noto_font_license.txt
cp ../LICENSE bundle
cp ../THIRD-PARTY-LICENSES bundle
cp third-party-copyright.zip bundle
cp -a ${APPLICATION_APP_BUNDLE} bundle

#DATE=$(date +%Y-%m-%d)
hdiutil create /tmp/tmp.dmg -ov -volname "${APPLICATION_NAME} Version ${Version}" -fs HFS+ -srcfolder "./bundle"
#hdiutil convert /tmp/tmp.dmg -format UDZO -o PV-${DATE}.dmg
hdiutil convert /tmp/tmp.dmg -format UDZO -o ${APPLICATION_NAME}-${Version}-MacOS.dmg
printf "\n\nDone! You will now find in this folder a \"${APPLICATION_APP_BUNDLE}\" and a \"${APPLICATION_NAME}-${Version}.dmg\"\n\n"
