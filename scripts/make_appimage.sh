#!/bin/bash

# simple script to make an appimage. This script is to be used
# in a separate build folder for building the appimages, and it
# relies on the required software being present right there,
# namely appimagetool-x86_64.AppImage and linuxdeploy-x86_64.AppImage
# with execute permission already set.

# The result will be an appimage like lux-1.1.7-x86_64.AppImage
# in this folder, which is ready for distribution. Run this script
# inside the build folder, so if you're in the root folder, do this:

# mkdir build.appimage
# cd build.appimage
# ../scripts/make_appimage.sh

# First, create AppDir, if it's not already present

mkdir -p AppDir

# we need a custom AppRun script to set the LUX_GUI_FONT
# environment variable so that the GUI font can be found.
# In case there is still an AppRun present in AppDir, we
# delete it first, then copy the AppRun script

rm -f Appdir/AppRun
cp ../scripts/AppRun AppDir

# Next we emplace the metainfo and desktop file

mkdir -p AppDir/usr/share/metainfo
cp ../scripts/metadata.xml AppDir/usr/share/metainfo/org.bitbucket.kfj.lux.appdata.xml

mkdir -p AppDir/usr/share/applications
cp ../scripts/lux.desktop AppDir/usr/share/applications/org.bitbucket.kfj.lux.desktop

# set up the build - for an appimage, we need the /usr install
# prefix. Nothing will actually be installed into /usr - below
# we set DESTDIR to use a different root folder than / for the
# installation: we'll install into AppDir, the folder which
# we'll later on convert into an AppImage

cmake -DCMAKE_INSTALL_PREFIX=/usr ..

# make the lux binary

make -j$(nproc)

# install into AppDir

make install DESTDIR=AppDir

# set the VERSION environment variable

export VERSION=$(cat ../pv_version_first).$(cat ../pv_version_second).$(cat ../pv_version_third)

# now use linuxdeploy to create the appimage

./linuxdeploy-x86_64.AppImage --appdir AppDir --output appimage
