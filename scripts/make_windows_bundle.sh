#! /bin/bash

# shell script to produce a new lux bundle. call like:
# ./bundle.sh lux_for_windows.x.y.z
# optionally, if the exe is not in build/lux.exe,
# pass the path to lux.exe in the second argument.
# this will create a folder 'lux_for_windows.x.y.z' and copy
# all relevant files into this folder. The folder makes lux
# 'stickware', so if you copy the whole folder to a
# USB stick, you can launch lux by doubleclicking on
# lux.exe in this folder on the stick. Another idea is to
# burn it on a CD with your images.
# It's nice to have rst2html, which is in the rst2pdf
# package. I haven't been able to get it to run on my
# msys2 installation for a while, though, so currently
# I'm creating the html documentation on a Linux system.

# The lux bundle is also used by the inno setup route to
# create a windows installer for lux: it conveniently provides
# all the necessary components in one folder, and inno setup
# can simply grab them from there - the .iss script is
# set up to find all components in a single folder. This
# makes it convenient to build new installers for new
# releases without having to edit, e.g., the library
# version numbers: the DLLs are all copied to the bundle
# with their latest version - the one linked into lux -
# and the .iss script simply uses a wildcard to copy all
# DLLs without looking at them in detail. So once the
# bundle is ready, the only modification needed in the
# .iss script is where the source folder is given, which
# has to point to the new bundle (there are 'edit' comments
# in the .iss script to point to this location). After that,
# the new installer can be built. The source folder update
# could be (TODO) automated.

# assume that lux.exe was built in 'build' with cmake, or
# take it from the second argument

if [[ -z "$2" ]]
then
  echo "no command line argument, processing build/lux.exe"
  exe_file=build/lux.exe
else
  exe_file=$2
  echo "processing executable " $exe_file
fi

cp $exe_file .
DLLs=$(ldd lux.exe | grep /mingw64 | sed 's/.dll.*/.dll/')
rst2html README.rst > README.html
rst2html lux_options.rst > lux_options.html
mkdir $1
for f in $DLLs; do cp /mingw64/bin/$f $1; done
cp lux.exe LICENSE THIRD-PARTY-LICENSES README.html lux_options.html NotoSans-Regular.ttf noto_font_license.txt scripts/*.ico $1
mkdir $1/src
cp -r *.h *.cc *.c README.rst LICENSE vspline $1/src
