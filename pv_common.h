/************************************************************************/
/*                                                                      */
/*          pv - a viewer for panoramic images                          */
/*                                                                      */
/*          Copyright 2016 - 2023 by Kay F. Jahnke                      */
/*                                                                      */
/*  The git repository for this software is at                          */
/*                                                                      */
/*  https://bitbucket.org/kfj/pv                                        */
/*                                                                      */
/*  Please direct questions, bug reports, and contributions to          */
/*                                                                      */
/*  kfjahnke+pv@gmail.com                                               */
/*                                                                      */
/*  pv is free software: you can redistribute it and/or modify          */
/*  it under the terms of the GNU General Public License as published   */
/*  by the Free Software Foundation, either version 3 of the License,   */
/*  or (at your option) any later version.                              */
/*                                                                      */
/*  pv is distributed in the hope that it will be useful,               */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       */
/*  GNU General Public License for more details.                        */
/*                                                                      */
/*  You should have received a copy of the GNU General Public License   */
/*  along with pv.  If not, see <http://www.gnu.org/licenses/>.         */
/*                                                                      */
/************************************************************************/

/*! \file pv_common.h

    \brief pv - a panorama viewer

    For documentation, please see the bitbucket repository at

    https://bitbucket.org/kfj/pv

    This file has includes and definitions used throughout pv.
    This file has no architecture-specific declarations.
*/

#ifndef PV_COMMON_H
#define PV_COMMON_H

#include <iostream>
#include <fstream>
#include <iomanip>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <assert.h>
#include <atomic>
#include <ctime>
#include <map>

using pv_clock = std::chrono::high_resolution_clock ;

#include <chrono>

extern pv_clock::time_point program_start ;

#include <vigra/tinyvector.hxx>
#include <vigra/multi_array.hxx>
#include <vigra/quaternion.hxx>

#include <vigra/imageinfo.hxx>
#include <vigra/impex.hxx>
#include <vigra/impexalpha.hxx>

#include "tinyfiledialogs.h"

extern void lux_abort ( const std::string & message ) ;

#define error_file(str) ( str.substr ( 1 + str.find_last_of("/\\") ) )

#define error_context() ( error_file ( std::string(__FILE__) ) \
                          + ", line " + std::to_string(__LINE__) ) 

#define unrecoverable() ( std::string("abort called from: ") + error_context() ) 

#define abort_lux(message) lux_abort( unrecoverable() + "\n" + std::string(message) )

enum projection_type
{
  SPHERICAL ,
  CYLINDRIC ,
  RECTILINEAR ,
  MOSAIC ,
  STEREOGRAPHIC ,
  FISHEYE ,
  CUBEMAP ,
  FACET_MAP ,
  PRJ_NONE ,
  PRJ_UNSUPPORTED
} ;

// enum { LEFT , RIGHT , TOP , BOTTOM , BACK , FRONT } ;
enum { FRONT , RIGHT , BACK , LEFT , TOP , BOTTOM , ANY_FACE } ;

const char * const projection_name[]
{
  "spherical" ,
  "cylindric" ,
  "rectilinear" ,
  "mosaic" ,
  "stereographic" ,
  "fisheye" ,
  "cubemap" ,
  "facet_map" ,
  "prj_none" ,
  "prj_unsupported"
} ;

enum alpha_mode
{
  ALPHA_MODE_NOT_SET ,
  NO_ALPHA ,           // no alpha processing, ignore alpha channel
  NO_ALPHA_IF_OPAQUE , // if alpha is fully opaque, do as abov, else below
  ALPHA_AS_SRC ,       // do alpha processing only if src has alpha
  WITH_ALPHA           // always do alpha processing, add alpha if missing
} ;

enum blending_mode
{
  BLEND_RANKED , // layers are ranked by proximity to the origin
  BLEND_HDR ,    // layers are combined into an HDR image
  BLEND_QUORATE , // layers are combined into a deghosted image
  BLEND_CORRELATE , // for light balancing
  BLEND_NONE // no blending (e.g. for single images)
} ;

const char * const blending_mode_name[]
{
  "RANKED" , // layers are ranked by proximity to the origin
  "HDR" ,    // layers are combined into an HDR image
  "QUORATE" , // layers are combined into a deghosted image
  "CORRELATE" , // for light balancing
  "NONE"
} ;

// pv encodes all orientation data in quaternions, which is convenient
// and numerically very stable:

typedef vigra::Quaternion<double> quaternion_type ;
// typedef vigra::Quaternion < float > qf_t ;

// 3D points are encoded in these types:

typedef vigra::TinyVector<double,2> point_2d_d_type ;
typedef vigra::TinyVector<double,3> point_3d_d_type ;

typedef vigra::TinyVector<float,2> point_2d_f_type ;
typedef vigra::TinyVector<float,3> point_3d_f_type ;

// typedefs for a few types which we use throughout

// float RGB pixel type. On reading, the incoming image is converted to
// pixels in this format, and the spline coefficients also are of this type

typedef vigra::TinyVector < float , 3 > pixel_type ;
typedef vigra::TinyVector < float , 4 > pixel4_type ;

// throughout, we use float coordinates.

typedef vigra::TinyVector < float , 2 > coordinate_type ;

// only for discrete coordinates, as provided by transform:

typedef vigra::TinyVector < int , 2 > int2_type ;

// we hold coordinate arrays in vigra::MultiArrays and access them via
// vigra::MultiArrayViews:

typedef vigra::MultiArray < 2 , coordinate_type > warp_array_type ;

// to feed frames to SFML, they have to contain pixels in 8 bit unsigned sRGBA.
// This pixel type has the same size as a 32 bit integer, so we can cast RGBA pixels
// to int and back if needed.

typedef vigra::TinyVector < unsigned char , 4 > rgba_type ;

// we handle the RGBA frames for SFML as vigra MultiArray(View)s, only passing
// int data when we pass them on to SFML to display

typedef vigra::MultiArray < 2 , rgba_type > rgba_image_type ;
typedef vigra::MultiArrayView < 2 , rgba_type > rgba_image_view_type ;

// some rendering is done to float RGB/float RGBA frames: this happens when
// taking sample frames for setting the white balance, and for snapshots.

typedef vigra::MultiArray<2, pixel_type> frgb_image_type ;
typedef vigra::MultiArray<2, pixel4_type> frgba_image_type ;

typedef vigra::MultiArrayView<2, float> mask_type ;
typedef vigra::MultiArrayView<2, pixel_type> view_type ;
typedef vigra::MultiArrayView<2, pixel4_type> view4_type ;

typedef vigra::MultiArrayView < 2 , int > int_array_view ;

struct job_type ; // forward declaration

// scalar transformation function from target coordinates to
// 3D 'ray' coordinates in model space

typedef std::function
  < point_3d_f_type ( const point_2d_d_type & ) > to_model_f ;

// variables needed for communication between the main thread and the
// rendering thread. The instances are in pv_no_rendering.cc

extern std::mutex job_mutex ;
extern std::queue < job_type* > job_queue ;
extern bool stay_alive ;
extern std::atomic < bool > aborted ;

extern std::mutex rendering_mutex ;
extern std::condition_variable rendering_cv ;

extern std::mutex content_mutex ;
extern std::condition_variable content_cv ;

extern int jobs_pending ;
extern std::atomic < int > snapshots_pending ;
extern void job_begins() ;
extern int jobs_busy() ;
extern void job_ends() ;

extern std::mutex frame_mutex ;
extern std::queue < job_type* > frame_queue ;
extern std::queue < job_type* > single_frame_queue ;

extern std::mutex rgb_mutex ;
extern std::queue < rgba_image_type* > rgb_queue ;

extern std::atomic < bool > status_flag ;
extern std::mutex status_mutex ;
extern bool status_loading ;
extern int status_building ;
extern std::string now_loading ;

extern std::atomic < bool > error_flag ;
extern std::string fatal_error ;

extern void push_frame ( job_type * p_job ) ;

// at times, lux needs to keep alive objects which other objects rely
// on - the typical case is an evaluator depending on a b-spline
// object. keep_alive_type will keep alive a shared_ptr to any type
// until it's destructed.
// We 'abuse' a by-copy lambda capture for the purpose, which makes
// for a simple and elegant implementation.

template < typename active_type >
struct holding_type
: public active_type
{
  std::function < void() > retain ;

  template < typename passive_type >
  holding_type ( std::shared_ptr < passive_type > passive ,
                 const active_type & active )
  : retain ( [passive]() { } ) ,
    active_type ( active )
    { } ;
} ;

// next we have a bit of collateral code to perform rotations of 3D coordinates
// which we'll need for the transformation from target to image coordinates below.

/// get_rotation_q: concatenation of roll, pitch and yaw into a single quaternion

template < typename dtype >
vigra::Quaternion < dtype > get_rotation_q ( dtype yaw , dtype pitch , dtype roll )
{
  // first calculate the component quaternions.
  // for a right-handed system and clockwise rotations, we have this formula:
  // q = cos ( phi / 2 ) + ( ux * i + uy * j + uz * k ) * sin ( phi / 2 )
  // where phi is the angle of rotation  and (ux, uy, uz) is the axis around
  // which we want to rotate some 3D object.

  // we take roll to be a clockwise rotation around the line of sight (-1, 0, 0)
  // to achieve the clockwise rotation of the section taken out of the panorama,
  // we have to rotate the target counterclockwise. So we get -sin ( -roll / 2.0 ),
  // and the two negative signs cancel out.
  vigra::Quaternion < dtype > q_roll ( cos ( roll / 2.0 ) , sin ( roll / 2.0 ) , 0.0 , 0.0 ) ;

  // if we consider a target in the line of sight, we take pitch as moving it upwards,
  // rotating clockwise around the positive y axis (0 , 1, 0)
  vigra::Quaternion < dtype > q_pitch ( cos ( pitch / 2.0 ) , 0.0 , sin ( pitch / 2.0 ) , 0.0 ) ;

  // if we consider a target in the line of sight, we take yaw as moving it to the right,
  // which is counterclockwise around the z axis, or clockwise around the negative
  // z axis (0, 0, -1)
  vigra::Quaternion < dtype > q_yaw ( cos ( yaw / 2.0 ) , 0.0 , 0.0 , - sin ( yaw / 2.0 ) ) ;

  // now produce the concatenated operation by multiplication of the components
  vigra::Quaternion < dtype > total = q_yaw ;
  total *= q_pitch ;
  total *= q_roll ;

  return total ;
}

/// apply the rotation codified in a quaternion to a 3D point

template < typename dtype , typename qtype >
vigra::TinyVector < dtype , 3 >
rotate_q ( const vigra::TinyVector < dtype , 3 > & v ,
           vigra::Quaternion < qtype > q )
{
  vigra::TinyVector < dtype , 3 > result ;
  vigra::Quaternion < dtype > vq ( 0.0 , v[0] , v[1] , v[2] ) ;
  vigra::Quaternion < dtype > qq ( q[0] , q[1] , q[2] , q[3] ) ;
  vigra::Quaternion < dtype > qc = conj ( qq ) ;
  qq *= vq ;
  qq *= qc ;
  result[0] = qq[1] ;
  result[1] = qq[2] ;
  result[2] = qq[3] ;
  return result ;
}

// this function was adapted to vigra quaternions from source code given in this
// Wikipedia article:
// https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
// I first extract the components w,i,k,j as w,x,y,z. The remainder of the code
// proceeds with the isolated components rather than the component access functions
// used in the code in the Wikipedia article.

template < typename dtype >
void toEulerAngle ( const vigra::Quaternion < dtype > & q ,
                    dtype & yaw , dtype & pitch , dtype & roll )
{
  dtype w = q[0] ; // extract the real part
  dtype x = q[1] ; // extract i, j and k from the vector component
  dtype y = q[2] ;
  dtype z = q[3] ;

  // roll (x-axis rotation)
  dtype sinr_cosp = +2.0 * (w * x + y * z);
  dtype cosr_cosp = +1.0 - 2.0 * (x * x + y * y);
  roll = atan2(sinr_cosp, cosr_cosp);

  // pitch (y-axis rotation)
  dtype sinp = +2.0 * (w * y - z * x);
  if (fabs(sinp) >= 1)
    pitch = copysign(M_PI / 2, sinp); // use 90 degrees if out of range
  else
    pitch = asin(sinp);

  // yaw (z-axis rotation)
  dtype siny_cosp = +2.0 * (w * z + x * y);
  dtype cosy_cosp = +1.0 - 2.0 * (y * y + z * z);
  yaw = atan2(siny_cosp, cosy_cosp);
}

double cos3d ( const point_3d_d_type & a , const point_3d_d_type & b ) ;
double angle3d ( const point_3d_d_type & a , const point_3d_d_type & b ) ;

/// conversion from sRGB to linear RGB and back. This is adapted
/// from vigra's sRGBCorrection and inverse_sRGBCorrection routines.

template < class T >
T sRGB2RGB ( T value , T norm = 255 )
{
  value /= norm ;

  if ( value <= T(0.04045) )
    return norm * value / T(12.92) ;
  else
    return norm * pow ( ( value + T(0.055) ) / T(1.055) , T(2.4) ) ;
}

template < class T >
T RGB2sRGB ( T value , T norm = 255 )
{
  value /= norm ;

  if ( value <= T(0.0031308) )
    return norm * T(12.92) * value ;
  else
    return norm * ( T(1.055) * pow ( value , T(0.41666666666666667) ) - T(0.055) ) ;
}

extern void fill_polygon ( const std::vector<float> & px ,
                           const std::vector<float> & py ,
                           int IMAGE_LEFT , int IMAGE_TOP ,
                           int IMAGE_RIGHT , int IMAGE_BOT ,
                           std::function < void ( int , int ) >
                             fillPixel ) ;

// currently unused

// template < class T >
// T gamma ( T value , T norm , T gamma )
// {
//   value /= norm ;
//
//   return std::min ( norm , norm * pow ( value , gamma ) ) ;
// }

/// struct light_settings_type holds all information about per-pixel
/// modifications of intensity, like brightness and white balance. All these
/// settings are applied in the final stage of the pixel pipeline, when the
/// 'raw' intensity values have been produced by interpolation.

struct light_settings_type
{
  vigra::TinyVector < double , 3 > white_balance ;
  double brighten ;
  double black_point ;
  double white_point ;
  bool apply_gradation ;
  bool cap_brightness ;
  float hdr_spread ;

  friend std::ostream& operator<< ( std::ostream& osr ,
                                    const light_settings_type& d )
  {
    osr << "white_balance   " << d.white_balance << std::endl ;
    osr << "brighten        " << d.brighten << std::endl ;
    osr << "black_point     " << d.black_point << std::endl ;
    osr << "white_point     " << d.white_point << std::endl ;
    osr << "apply_gradation " << d.apply_gradation << std::endl ;
    osr << "cap_brightness  " << d.cap_brightness << std::endl ;
    osr << "hdr_spread      " << d.hdr_spread << std::endl ;
    return osr ;
  }

} ;

// in tf_to_source (in pv_rendering.cc), the virtual sensor, defined by three corner
// points origin (upper left), ur (upper right) and ll (lower left) is used to set up
// the geometric transformation from target coordinates back to image coordinates.
// This provides a convenient handle to apply effects to the sensor by modifying it's
// size, position and orientation. These three operations all keep the sensor planar.
// Many of these modifications to the sensor will not be useful, but it's nice to have
// them around just in case, and their application comes at no computational expense.
// This is new functionality; I'm not entirely certain that the maths will work out
// for every single projection - I may have made assumptions in the projection
// calculations about properties of the sensor which are invalidated by the sensor
// manipulations - like that the sensor is always at unit distance from the sphere's
// origin. TODO: have to check before rollout, seems fine so far.
// The only use the sensor manipulation is put to is currently the 'magnifying_glass'
// and sensor tilt, which is handy for 'collapsing lines'.

// struct sensor_settings_type
// {
//   double magnifying_glass = 1.0 ;
//   double magnifying_glass_factor = 10.0 ;
// 
//   point_3d_d_type sensor_size { 1.0 , 1.0 , 1.0 } ;
//   quaternion_type sensor_tilt = get_rotation_q ( 0.0 , 0.0 , 0.0 ) ;
//   point_3d_d_type sensor_shift { 0.0 , 0.0 , 0.0 } ;
// 
//   sensor_settings_type()
//   {
//     magnifying_glass = 1.0 ;
//     magnifying_glass_factor = 10.0 ;
// 
//     sensor_size = { 1.0 , 1.0 , 1.0 } ;
//     sensor_tilt = get_rotation_q ( 0.0 , 0.0 , 0.0 ) ;
//     sensor_shift = { 0.0 , 0.0 , 0.0 } ;
//   }
// 
//   bool operator== ( const sensor_settings_type & other ) const
//   {
//     return    magnifying_glass == other.magnifying_glass
//            && magnifying_glass_factor == other.magnifying_glass_factor
//            && sensor_size == other.sensor_size
//            && sensor_tilt == other.sensor_tilt
//            && sensor_shift == other.sensor_shift ;
//   }
// 
//   /// sensor size is the first sensor modification to be applied. At this point, the
//   /// sensor is still in it's initial position, touching the unit sphere at (-1,0,0)
//   /// with it's center. The size change is applied as a multiplication with 'factor',
//   /// where factor[0] should normally be 1.0 (it signifies the sensor's distance from
//   /// the unit sphere's center, which may be adjusted by sensor shift, further down)
// 
//   void apply_sensor_size ( point_3d_d_type & origin ,
//                            point_3d_d_type & ur ,
//                            point_3d_d_type & ll ,
//                            point_3d_d_type & lr ,
//                            point_3d_d_type factor ) const
//   {
//     origin *= factor ;
//     ur *= factor ;
//     ll *= factor ;
//     lr *= factor ;
//   }
// 
//   /// sensor tilt is the second sensor modification applied. It moves the sensor so
//   /// that it's center is at the unit sphere's center, then applies the quaternion
//   /// encoding the desired rotation, then moves the sensor's center back to where it was.
// 
//   void apply_sensor_tilt ( point_3d_d_type & origin ,
//                            point_3d_d_type & ur ,
//                            point_3d_d_type & ll ,
//                            point_3d_d_type & lr ,
//                            quaternion_type orientation ) const
//   {
//     // move sensor to origin
// 
//     auto center = ( ur + ll ) / 2.0 ;
// 
//     origin[0] -= center[0] ;
//     ur[0] -= center[0] ;
//     ll[0] -= center[0] ;
//     lr[0] -= center[0] ;
// 
//     // apply rotation
// 
//     origin = rotate_q ( origin , orientation ) ;
//     ur = rotate_q ( ur , orientation ) ;
//     ll = rotate_q ( ll , orientation ) ;
//     lr = rotate_q ( lr , orientation ) ;
// 
//     // move back
// 
//     origin[0] += center[0] ;
//     ur[0] += center[0] ;
//     ll[0] += center[0] ;
//     lr[0] += center[0] ;
//   }
// 
//   /// the third sensor manipulation, after size and tilt, modifies the sensor's
//   /// position. All three points defining the sensor are shifted by the same amounts.
// 
//   void apply_sensor_shift ( point_3d_d_type & origin ,
//                             point_3d_d_type & ur ,
//                             point_3d_d_type & ll ,
//                             point_3d_d_type & lr ,
//                             point_3d_d_type factor ) const
//   {
//     origin += factor ;
//     ur += factor ;
//     ll += factor ;
//     lr += factor ;
//   }
// 
//   /// apply() bundles the application of the three sensor manipulations,
//   /// also taking into account the 'magnifying glass' constant by adjusting
//   /// sensor_size accordingly.
// 
//   void apply ( point_3d_d_type & origin ,
//                point_3d_d_type & ur ,
//                point_3d_d_type & ll ,
//                point_3d_d_type & lr ) const
//   {
//     apply_sensor_size ( origin , ur , ll , lr ,
//                         point_3d_d_type ( 1.0 ,
//                                           1.0 / magnifying_glass ,
//                                           1.0 / magnifying_glass ) ) ;
//     apply_sensor_tilt ( origin , ur , ll , lr , sensor_tilt ) ;
//     apply_sensor_shift ( origin , ur , ll , lr , sensor_shift ) ;
//   }
// } ;

struct sensor_settings_type
{
  double magnifying_glass = 1.0 ;
  double magnifying_glass_factor = 10.0 ;

  point_3d_d_type sensor_size { 1.0 , 1.0 , 1.0 } ;
  quaternion_type sensor_tilt = get_rotation_q ( 0.0 , 0.0 , 0.0 ) ;
  point_3d_d_type sensor_shift { 0.0 , 0.0 , 0.0 } ;

  sensor_settings_type() ;
  bool operator== ( const sensor_settings_type & other ) const ;
  
  void apply_sensor_size ( point_3d_d_type & origin ,
                           point_3d_d_type & ur ,
                           point_3d_d_type & ll ,
                           point_3d_d_type & lr ,
                           point_3d_d_type factor ) const ;
                           
  void apply_sensor_tilt ( point_3d_d_type & origin ,
                           point_3d_d_type & ur ,
                           point_3d_d_type & ll ,
                           point_3d_d_type & lr ,
                           quaternion_type orientation ) const ;
                           
  void apply_sensor_shift ( point_3d_d_type & origin ,
                            point_3d_d_type & ur ,
                            point_3d_d_type & ll ,
                            point_3d_d_type & lr ,
                            point_3d_d_type factor ) const ;

  void apply ( point_3d_d_type & origin ,
               point_3d_d_type & ur ,
               point_3d_d_type & ll ,
               point_3d_d_type & lr ) const ;
} ;

// for exposure fusion, faux brackets and image stitching we gather
// the parameters specific to these tasks in this structure:
// TODO: may calculate light balance via this route as well

enum fusion_mode
{
  SINGLE_IMAGE ,
  STITCH_IMAGES ,
  EXPOSURE_FUSION ,
  FAUX_BRACKET ,
  QUORUM ,
  LIGHT_BALANCE ,
  WHITE_BALANCE
} ;

enum job_routing_mode
{
  ROUTE_TO_SCREEN ,
  ROUTE_TO_FILE ,
  ROUTE_TO_FRAME
} ;

enum stack_mode
{
  STACK_FIRST ,
  STACK_HDR ,
  STACK_FUSION
} ;

struct blending_settings_type
{
  // type of the job at hand

  fusion_mode mode ;
  job_routing_mode routing ;
  stack_mode stacking ;

  // number of facets in the facet map (TODO: can we infer that?)

  int nfacets ;

  // number of visible facets (TODO: can we infer that?)

  int npartial ;

  // relative strength of expoure weight and contrast weight

  double exposure_weight ;
  double contrast_weight ;

  // additional parameters for exposure weight's fitness curve

  double exposure_sigma ;
  double exposure_mu ;

  // parameters for the image pyramids used by the B&A algorithm

  double scaling_step ;
  int pyramid_floor ;

  int i_spline_degree ;    // spline degree of image splines
  int i_spline_decimator ; // decimator
  int i_spline_shift_to ;  // spline degree for expanded top level

  int q_spline_degree ;    // spline degree of quality splines
  int q_spline_decimator ; // decimator
  int q_spline_shift_to ;  // spline degree for expanded top level
  
  // for faux brackets, the individual Ev values (may be empty)

  std::vector < double > partial_brightness ;

//   // pointer to a job_type object for the baseline image, and to
//   // derive the partials from
// 
//   job_type * p_baseline_job ;

  // value of ui::process_linear, to determine if RGB->sRGB is needed

  bool process_linear ;

  // filename to store the result of blending

  std::string filename ;
} ;

// we need to calculate the lens correction polynomial and it's inverse.
// To standardize the process, we define a polynomial class which can
// calculate both the function value and the first derivative for a given
// argument x. We add an 'inverse' function; 'inverse' produces the value
// which will evaluate to a desired result when passed to 'function'.
// The inverse functio is not guaranteed to succeed and may be slow.

template < typename value_type , std::size_t DEGREE >
struct pv_polynomial
{
  value_type coefficients [ DEGREE + 1 ] ;
  value_type derivative_coefficients [ DEGREE + 1 ] ;

  pv_polynomial() = default ;

  pv_polynomial ( value_type * p_coefficients )
  {
    std::size_t power = DEGREE ;
    for ( std::size_t i = 0 ; i <= DEGREE ; i++ )
    {
      coefficients [ i ] = p_coefficients [ i ] ;
      derivative_coefficients [ i ] = coefficients [ i ] * power ;
      --power ;
    }
  }

  value_type function ( value_type x ) const
  {
    value_type sum = 0.0 ;
    value_type power = 1.0 ;
    for ( std::size_t i = 0 ; i <= DEGREE ; i++ )
    {
      std::size_t j = DEGREE - i ;
      sum += coefficients [ j ] * power ;
      power *= x ;
    }
    return sum ;
  }

  value_type derivative ( value_type x ) const
  {
    value_type sum = 0.0 ;
    value_type power = 1.0 ;
    for ( std::size_t i = 0 ; i < DEGREE ; i++ )
    {
      std::size_t j = DEGREE - i - 1 ;
      sum += derivative_coefficients [ j ] * power ;
      power *= x ;
    }
    return sum ;
  }

  bool inverse ( value_type desired_output ,
                 value_type & required_input ,
                 value_type tolerance = .000000001 ) const
  {
    value_type current = required_input ;

    for ( int count = 0 ; count < 100 ; count++ )
    {
      auto result = function ( current ) ;
      auto difference = desired_output - result ;

      if ( fabs ( difference ) <= tolerance )
      {
        required_input = current ;
        return true ;
      }

      current = current + difference / derivative ( current ) ;
    }
    return false ;
  }
} ;

// Next we have data structures used to transport information to the rendering
// thread. This is a varied lot and contains the complete information which
// the rendering thread needs. The idea is to have a clearly defined interface
// which avoids use of shared global variables. The
// information in class job_type (further down) might be serialized to transport
// it over a socket, rather than passing in-memory data, which would allow for
// the server to reside on a different machine. The pointers would have to be
// replaced by some sort of handle, with extra code to establish them.

typedef enum { NONE , PAN , SINGLE , IMMEDIATE , TERMINATE } display_mode ;

struct extent_type
{
  double x0 , x1 , y0 , y1 ;
} ;

// TODO allow for source changes during execution. This is tricky; the code
// is written to build up the internal representation assuming this won't
// happen, and the build-up of the IR would need to be factored out to be
// called again at some later stage. In some cases, some assets (notably
// the splines used for interpolation, which are expensive to create) might
// be reusable.

struct source_type
{
  bool active ;

  std::string filename ;

  std::size_t width ;    // original source image width
  std::size_t height ;   // and height in pixels

  int squash ;

  int exif_orientation ; // EXIF orientation, or 0 if EXIF tag is missing

  projection_type projection ; // projection of the source image

  double hofs ;          // horizontal offset angle
  double hfov ;          // horizontal field of view
  double vofs ;          // ditto, vertical
  double vfov ;

  double handicap ;
  std::shared_ptr < vigra::MultiArray < 2 , float > >
    p_priority_map ;

  double brightness ;

  int lens_number ;
  bool lens_correction_active ;
  float s , a , b , c , d , h , v ;

  bool vignetting_correction_active ;
  float vs , va , vb , vc , vd , vx , vy ;

  extent_type extent ;   // 'draped' extent in radians (see set_extent())
  extent_type iextent ;  // raw extent in image coordinates

  // 3D ray coordinates of frustum

  point_3d_d_type ul , ur , lr , ll ;

  // 2D ray corner points in source image plane

  point_2d_d_type ul2 , ur2 , lr2 , ll2 ;

  // maximal distance to a 2D corner point (after applying lcp)

  double r_max ;

  double theta ;         // largest angle between a corner and center

  double x_step ;        // width of center pixel in radians

  bool full_width ;      // true if image is 360 degrees horizontally
  bool full_height ;     // true if vfov is 180 in sphericals, 360 in fisheye

  bool is_linear ;       // true if image data are genuinely linear RGB

  bool cropping_active ;
  bool cropping_elliptic ;

  float crop_fade ;
  float crop_x0, crop_x1, crop_y0, crop_y1;

  // remaining members only set when processing PTO files:

  bool has_masks ;
  struct polygon_type
  {
    int variant ;
    std::vector < float > vx ;
    std::vector < float > vy ;
  } ;
  std::vector < polygon_type > masks ;

  int stack_parent ;
} ;

/// struct frame contains all information needed to produce a target frame from
/// a source image, like the target's intended hfov, yaw, pitch, roll etc.
/// This information may be different with every frame. Here there are also
/// some additional frame-specific data, and there's a pointer to attach the
/// finished frame.

struct interpolator_base ;

// TODO: I'd like to move from using raw b-spline pointers to an 'interpolator'
// object with selectable scale, to hide the image pyramid from the UI-side
// code and make it an implementation detail of the rendering code. This would
// also be a step towards broadening the technical scope to use of openGL-side
// cubemaps instead of vspline b-splines. While I am very attached to the use of
// vspline, because it's my own brainchild, it limits pv to reasonably powerful
// hardware, and having the option to offload interpolation to the GPU might be
// a bonus. Using cubemaps would require transforming incoming image data to
// what can be made into a mipmap: it has to be a set of six square rectilinear
// images. This may be very costly for large images. I haven't investigated into
// GPU-side code capable of processing non-rectilinear data yet; I fear the
// necessary geometric transformations may be hard to code.

enum frame_mode
{
  FRAME_RGBA8 , // produce an 8-bit RGBA frame for on-screen display
  FRAME_FRGB ,  // produce a float RGB frame
  FRAME_FRGBA , // produce a float RGBA frame
  FRAME_MASK    // produce a mask
} ;

struct frame_type
{
  // was: 0: 8-bit RGBA (screen) 1: float RGB 2: float RGBA
  // now an enum.

  frame_mode format ;

  // for multi-image jobs like facet maps, this specifies the blending mode

  blending_mode blending ;

  // if hq is true, the high quality interpolator will be used.

  bool hq ;

  display_mode mode ; // requests PAN vs. IMMEDIATE mode, etc.

  // pointer to interpolator base class; it's up to the rendering code
  // which concrete interpolator object it puts here

  interpolator_base * p_itp ;

  // wait_for_stage sets a minimum 'stage' the interpolator must have reached
  // before frames are rendered with it. The lowest requirement, stage 1,
  // has bilinear interpolation on the raw data. This may be used, but with
  // higher zoom factors, there will be aliasing. Waiting for stage 2 means that
  // the lq interpolator will be accepted as sufficient, while waiting for
  // level 3 requires the interpolator builder to have completed. If the builder
  // has, due to lack of memory, failed to build stuff it was supposed to build,
  // it will still reach stage 3 and the frame may be rendered with the worst
  // interpolator and no antialiasing - but see still_image_supersampling.

  int wait_for_stage ;

  double level_bias ;

  // -1 for 'off', 0..n for 'only this one facet'

  int solo ;
  int mask_for ;
  int mask_stack ;
  bool use_rank ;
  bool yield_argba ;
  int stack_only ;

  // feathering for panorama-like dsiplays (BLEND_RANKED)
  
  double feathering ;

  // this value here is redundant and can be calculated from the
  // source and target metrics plus hfov below. But the calculation
  // takes several steps and it is performed in pv_no_rendering.cc
  // anyway, so we carry the value over from there.

  double zoom_factor ;

  // camera view orientation

  quaternion_type orientation ;

  double x_shift ;  // used for panning

  double rise ; // modifies vofs in single-image interpolators

  bool tonemap ; // apply simple tone-mapping operator

  bool heal ; // reveal filled-in RGB in alpha images

  bool screen_needs_srgb ; // true if GPU context is not srgb-capable

  light_settings_type light_settings ; // brightness, white balance, etc.
  sensor_settings_type sensor_settings ; // magnifying glass, sensor tilt etc.
  blending_settings_type blending_settings ; // used for stitching, fusion

  rgba_image_type * p_frame ; // points to the finished 8-bit RGBA frame
  view_type * p_frgb ;   // ditto, but float RGB
  view4_type * p_frgba ; // ditto, but float RGBA
  mask_type * p_mask ;   // used when masks are produced

  double cost ;       // information on how long frame generation took
} ;

/// struct target_type contains all metrics pertaining to a frame which is
/// to be generated. Width and height give the size of the uncropped image,
/// and if cropping_active is true, crop_x and crop_y give start coordinates
/// of the cropping area, and crop_width and crop_height give it's extent.
/// hfov and vfov pertain to the uncropped target.

struct target_type
{
  projection_type projection ;

  std::size_t width ;    // original source image width
  std::size_t height ;   // and height in pixels

  bool cropping_active ;

  std::size_t crop_x ;
  std::size_t crop_y ;
  std::size_t crop_width ;
  std::size_t crop_height ;

  double hfov ;          // horizontal field of view
  double vfov ;          // vertical field of view

  target_type()
  : cropping_active ( false )
  { }
} ;

/// struct dock has space to attach data which need to be shared between
/// different parts of the program as processing logic dictates. The objects
/// pointed to are managed by the code producing them, that code is responsible
/// for their timely construction and destruction

struct dock_type
{
  warp_array_type * p_warp ; // pointer to a warp array, used for panning
  vigra::MultiArray < 2 , double > * p_correlate ; // for light balance
  frgb_image_type * p_frgb ;   // ditto, but float RGB
  frgba_image_type * p_frgba ; // ditto, but float RGBA
} ;

/// struct job_type holds a set of all structures needed by the generate
/// thread to do it's job. Note that the *source* information is encoded
/// in the interpolator, which (apart from vofs modification through
/// frame.rise) is unaffected by the data in the jobs.

struct job_type
{
  static int _serial ;
  int serial ;
  pv_clock::time_point created ;
  pv_clock::time_point ready ;
  pv_clock::time_point displayed ;
  bool may_pan ;
  bool decimate_area ;

  target_type target ;
  frame_type frame ;
  dock_type dock ;
  int njobs ;       // number of worker threads to use for this job

  // render a frame to a smaller or larger size. This can be affected
  // by passing modified values in target and frame or in one go by
  // using this method.
  // I added code to make the column count a multiple of 16
  // to avoid non-SIMD code. The line count does not affect
  // the use of SIMD, and it seems anisotropic scaling is
  // broken anyway, so y_factor has to be the same as x_factor.
  // I added a flag to switch the rounding-to-16 off, which is
  // now the default - the only call to this function with the
  // round-to-16 flag true is for moving image scaling, where
  // the precise size of the frame is not important, but speed
  // is an issue. Scaling to a multiple of 16 must definitely
  // be avoided for image exports, because some exports rely
  // on a precisely met image width and height - hence the new
  // default.

  void inflate ( double x_factor ,
                 double y_factor = 0.0 ,
                 bool round_to_16 = false )
  {
    if ( y_factor <= 0.0 )
      y_factor = x_factor ;

    if ( x_factor == 1.0 && y_factor == 1.0 )
      return ;

    auto initial_width = target.width ;
    target.width = std::round ( target.width * x_factor ) ;

    if ( round_to_16 && target.width > 16 )
      target.width = ( target.width >> 4 ) << 4 ;

    x_factor =    double ( target.width )
                / double ( initial_width ) ;

    // TODO: is anisotropic scaling broken?

    y_factor = x_factor ;
    target.height = std::round ( target.height * y_factor ) ;

    frame.zoom_factor *= ( x_factor + y_factor ) / 2.0 ;

    if ( target.cropping_active )
    {
      target.crop_width = std::round ( x_factor * target.crop_width ) ;
      target.crop_height = std::round ( y_factor * target.crop_height ) ;
      target.crop_x = target.width - target.crop_width ;
      target.crop_y = target.height - target.crop_height ;
    }
  }
} ;

void * create_spline ( vigra::ImageImportInfo & imageInfo ,
                              const bool & is_periodic ,
                              const bool & is_linear ,
                              const bool & process_linear ,
                              void * & p_alpha ) ;

// struct interpolator_base provides the interface for objects yielding
// image data. The mechanism works like this: the derived class is set up
// to be able to provide functors (usually derived from vspline::unary_functor)
// which can provide float RGB pixel values for real 2D coordinates. When the
// pixel pipeline for a rendering job is built, the fix_... functions are
// used to insert these functors into the processing chain - just as the
// free functions used for other aspects of the pixel pipeline. This way,
// the functors in question can be inserted into the pixel pipeline without
// the need to erase their type (by using vspline::grok), which would be
// necessary if the functor has to be returned as a distinct object. The
// resulting pixel pipeline should be easier to optimize, and it seems to me
// that avoiding the type erasure for the functors improves performance.
// Since classes derived from interpolator_base are free to insert any
// suitable functor, the mechanism becomes open to the insertion of code
// beyond mere image interpolation. One interesting path is code generating
// visual representation of mathematical constructs 'out of thin air'.
// Another option is the use of external interpolation code: The viewer
// only handles a base class pointer.

enum interpolator_type { SINGLE_ITP ,
                         CUBEMAP_ITP ,
                         FACET_MAP_ITP ,
                         OTHER_ITP } ;

struct interpolator_base
{
  int subimage ;

  // this function returns either SINGLE_ITP for single-image interpolators
  // or MULTI_ITP for cubemaps and facet maps

  interpolator_type own_type ;

  // once initialisation of the interpolator object is done, alpha_mode
  // will be either WITH_ALPHA or NO_ALPHA.

  alpha_mode mode ;

  // this member is used to communicate the progress of interpolator
  // building: some interpolators may already become operational even
  // though their full functionality is not yet ready.

  std::atomic < int > stage ;

  // used for interpolators based on vspline::bspline objects. Shifting
  // changes the interpretation of the spline coefficients.

  virtual void shift ( int , int ) = 0 ;

  // This member function is responsible for the insertion of an evaluator
  // into the pixel pipeline and for setting up the final stages of the
  // pipeline and executing it.
  // This function is exposed via the interpolator_base interface to allow
  // the non-rendering code to call it directly (without the usual queueing
  // and processing by the rendering thread): this is done e.g. for snapshots
  // or white balance calculation.

  virtual void process_job ( job_type * p_job ) = 0 ;

  virtual std::vector < int > active_facets ( job_type * p_job )
  {
    return std::vector < int > () ;
  }

  virtual int in_stack_facets ( int stack_parent )
  {
    return 0 ;
  }

  virtual ~interpolator_base() { } ;
} ;

#ifdef USE_MEMLOG

struct mem_node_type
{
  int line ;
  std::string file ;
} ;

extern std::map < void* , mem_node_type > memlog_set ;

struct memlogger_type
{
  int current_line ;
  std::string current_file ;
  int counter ;

  template < typename arg_type >
  arg_type operator<< ( arg_type arg )
  {
    ++counter ;
    memlog_set.insert ( { (void*) arg , { current_line , current_file } } ) ;
    return arg ;
  }

  memlogger_type & operator<< ( int line )
  {
    current_line = line ;
    return *this ;
  }

  memlogger_type & operator<< ( std::string file )
  {
    current_file = file ;
    return *this ;
  }

  template < typename arg_type >
  void operator>> ( arg_type arg )
  {
    memlog_set.erase ( (void*) arg ) ;
    delete arg ;
  }

  memlogger_type()
  {
    counter = 0 ;
  }
} ;

extern memlogger_type memlog ;

#define mem_in() ( memlog << std::string(__FILE__) << __LINE__ ) 

#else

struct memlogger_type
{
  template < typename arg_type >
  arg_type operator<< ( arg_type arg )
  {
    return arg ;
  }

  template < typename arg_type >
  void operator>> ( arg_type arg )
  {
    delete arg ;
  }
} ;

extern memlogger_type memlog ;

#define mem_in() memlog 

#endif // USE_MEMLOG

// forward declaration of class 'dispatch', which is the base class of the
// specific dispatching classes. Here we only need a pointer.

struct dispatch ;

// flavour_type holds all information pertaining to a given flavour

struct flavour_type
{
  std::string name ;
  int priority ;
  std::function < bool() > viability ;
  dispatch * p_dispatch ;
} ;

// declaration of a global variable holding the flavour map

extern std::vector < flavour_type > flavour_set ;

typedef std::function < void ( const point_2d_d_type & ,
                                     point_2d_d_type & ) > crdd_tf_type ;

typedef std::function < void ( const point_3d_d_type & ,
                                     point_3d_d_type & ) > crd3d_tf_type ;

// definition of base class dispatch

struct dispatch
{
  // register_arch adds an entry to the flavour map. The bool return
  // is only needed to 'hook' this function to a static member, in
  // order to trigger it's call during pre-startup initialization

  static bool register_flavour ( const flavour_type & flavour )
  {
    std::cout << "***** register_arch is called for flavour name " << flavour.name << std::endl ;
    flavour_set.push_back ( flavour ) ;
    return true ;
  }

  // this inclusion of interface.h declares the pure virtual members
  // of the base class. definitions occur in flavour-specific code,
  // e.g. in pv_rendering.cc and pv_combine.cc

  #define SET_PURE(dcl) virtual dcl = 0
  #include "interface.h"
  #undef SET_PURE
} ;

// viability functions, defined in pv_no_rendering.cc

bool is_viable() ;
bool has_ssse3() ;
bool has_sse42() ;
bool has_avx() ;
bool has_avx2() ;
bool has_avx512f() ;

/// struct display_type encodes the parameters needed to render a frame.
/// It is used in several ways:
/// - to hold the state of the viewer
/// - to encode changes to this state
/// - to hold an unmoving state

struct display_type
{
  // the orientation of the 'camera' is handled with two quaternions.
  // preadjust is applied right after the 'bias' and is used for yawing:
  // applying yaw has to be relative to the biased image. the second quaternion,
  // 'orientation', holds the (changes in) orientation which are applied after
  // the yaw.

  quaternion_type preadjust ;
  quaternion_type orientation ;
  double dx , dy , dr ; // orientation in mosaic mode

  double rise ;         // rise/drop of camera viewpoint
  double speed ;        // speed of movement
  double zoom_factor ;
  double level_bias ;
  double moving_image_scaling ; // used for downscaled rendering
  double brightness ;   // to adapt brightness of displayed image
  vigra::TinyVector < double , 3 > white_balance ;
  double black_point ;   // to adapt black point
  double white_point ;   // to adapt white point
  double sensor_tilt_h ; // rotation of sensor around vertical
  double sensor_tilt_v ; // rotation of sensor around horizontal
  bool draw ;           // flag to trigger a redraw even if all else is static

  display_type ( quaternion_type _orientation = get_rotation_q ( 0.0 , 0.0 , 0.0 ) ,
                 quaternion_type _preadjust = get_rotation_q ( 0.0 , 0.0 , 0.0 ) ,
                 double v = 0.0 ,
                 double s = 0.0 ,
                 bool d = false )
  : orientation ( _orientation ) ,
    dx ( 0.0 ) ,
    dy ( 0.0 ) ,
    dr ( 0.0 ) ,
    preadjust ( _preadjust ) ,
    rise ( 0 ) ,
    speed ( v ) ,
    zoom_factor ( s ) ,
    level_bias ( 0 ) ,
    moving_image_scaling ( 0.0 ) ,
    brightness ( 0.0 ) ,
    white_balance ( { 1.0 , 1.0 , 1.0 } ) ,
    black_point ( 0 ) ,
    white_point ( 0 ) ,
    sensor_tilt_v ( 0.0 ) ,
    sensor_tilt_h ( 0.0 ) ,
    draw ( d )
    {} ;

  display_type& operator= ( const display_type& other )
  {
    orientation = other.orientation ;
    dx = other.dx ;
    dy = other.dy ;
    dr = other.dr ;
    preadjust = other.preadjust ;
    rise = other.rise ;
    speed = other.speed ;
    zoom_factor = other.zoom_factor ;
    level_bias = other.level_bias ;
    moving_image_scaling = other.moving_image_scaling ;
    draw = other.draw ;
    brightness = other.brightness ;
    black_point = other.black_point ;
    white_point = other.white_point ;
    white_balance = other.white_balance ;
    sensor_tilt_v = other.sensor_tilt_v ;
    sensor_tilt_h = other.sensor_tilt_h ;
    return *this ;
  }

  bool operator== ( const display_type& other )
  {
    return (    orientation  == other.orientation
             && dx == other.dx
             && dy == other.dy
             && dr == other.dr
             && preadjust    == other.preadjust
             && rise  == other.rise
             && speed == other.speed
             && zoom_factor == other.zoom_factor
             && level_bias == other.level_bias
             && moving_image_scaling == other.moving_image_scaling
             && brightness == other.brightness
             && black_point == other.black_point
             && white_point == other.white_point
             && white_balance == other.white_balance
             && sensor_tilt_v == other.sensor_tilt_v
             && sensor_tilt_h == other.sensor_tilt_h
             && draw  == other.draw ) ;
  }

  bool operator!= ( const display_type& other )
  {
    return ! ( *this == other ) ;
  }

  friend std::ostream& operator<< ( std::ostream& osr , const display_type& d )
  {
    osr << "orientation " << d.orientation << std::endl ;
    osr << "dx, dy, dr  " << d.dx << " " << d.dy << " " << d.dr << std::endl ;
    osr << "preadjust.. " << d.preadjust << std::endl ;
    osr << "rise:...... " << d.rise << std::endl ;
    osr << "speed:..... " << d.speed << std::endl ;
    osr << "zoom_factor " << d.zoom_factor << std::endl ;
    osr << "glob. scale " << d.moving_image_scaling << std::endl ;
    osr << "black pt. . " << d.black_point << std::endl ;
    osr << "white pt. . " << d.white_point << std::endl ;
    osr << "white bal.. " << d.white_balance << std::endl ;
    osr << "sensor tilt v" << d.sensor_tilt_v << std::endl ;
    osr << "sensor tilt h" << d.sensor_tilt_h << std::endl ;
    osr << "draw:...... " << d.draw << std::endl ;
    return osr ;
  }

} ;

#endif // PV_COMMON_H
