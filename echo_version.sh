#! /bin/bash

# helper script to produce pv_version.h, containing the current
# version number as a const char * and the version components
# as const integers

echo 'const int pv_first = '$(cat pv_version_first)' ;'
echo 'const int pv_second = '$(cat pv_version_second)' ;'
echo 'const int pv_third = '$(cat pv_version_third)' ;'
echo 'const char * pv_version = "'$(cat pv_version_first).$(cat pv_version_second).$(cat pv_version_third)'" ;'
