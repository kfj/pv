/************************************************************************/
/*                                                                      */
/*          pv - a viewer for panoramic images                          */
/*                                                                      */
/*          Copyright 2016 - 2023 by Kay F. Jahnke                      */
/*                                                                      */
/*  The git repository for this software is at                          */
/*                                                                      */
/*  https://bitbucket.org/kfj/pv                                        */
/*                                                                      */
/*  Please direct questions, bug reports, and contributions to          */
/*                                                                      */
/*  kfjahnke+pv@gmail.com                                               */
/*                                                                      */
/*  pv is free software: you can redistribute it and/or modify          */
/*  it under the terms of the GNU General Public License as published   */
/*  by the Free Software Foundation, either version 3 of the License,   */
/*  or (at your option) any later version.                              */
/*                                                                      */
/*  pv is distributed in the hope that it will be useful,               */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       */
/*  GNU General Public License for more details.                        */
/*                                                                      */
/*  You should have received a copy of the GNU General Public License   */
/*  along with pv.  If not, see <http://www.gnu.org/licenses/>.         */
/*                                                                      */
/************************************************************************/

/*! \file fileio.cc
 * 
 *  \brief code toopen image files with OpenImageIO
 *
 *  I am toying with moving image i/o to OpenImageIO, which has a wider
 *  scope than vigraimpex and yet can pass data to/from strided memory,
 *  so it should blend in with data held in MultiArrayViews.
 *  For now, here's a proof of concept, opening files with 3 channels
 *  and 8 or 16 bit data with OIIO.
*/

#include "pv_common.h"
#include <vector>

#include "fileio.h"

// we start out with the free function 'gyrate' which is generic
// and doesn't use either vigraimpex or OIIO

namespace fileio
{
// gyrate: given an image with an Orientation attribute and the metrics
// of the target memory intended to receive the image data, adapt the
// strides and offset so that they will transport the image data to
// the target memory when passed to the image reading routine, and,
// in the process, rotate/flippe the data so that the target has
// data in Orientation 1. The target memory is expected to be shaped
// so that it can accomodate the image data after any rotation/flip
// was applied.
//
// gyrate is best explained with an example. Suppose you have an image
// yielding a non-zero Orientation attribute, but you want to have it
// in memory rotated/flipped to 'normal' orientation (Orientation 1).
// First you extract the image data's 'native' width and height. If the
// Orientation attribute is five or greater, the camera was rotated by
// 90 or 270 degrees, so you swap width and height to be used for your
// target in memory. You set up memory to receive the image data and
// determine the strides of the memory. Now you're ready to use
// 'gyrate': pass the orientation, width, height, stride along the
// x axis, stride along the y axis and an arbitrary offset (usually 0)
// - the latter three are passed by reference, and gyrate may
// modify them. With the adapted strides and offset, you can now call
// OIIO's read_image, passing the strides, and a base address offsetted
// by offset. Note that gyrate is unaware of the meaning of the strides
// (pixels, fundamentals, bytes), but for OIIO's read_image you will
// need bytes. If you pass in byte strides, you'll get byte strides out,
// same for any other type of strides.
//
// In code, using OpenImageIO (gyrate itself has no external dependencies):
//
//   int o = spec.get_int_attribute ( "Orientation" , 1 ) ;
//   long w = spec.width ;
//   long h = spec.height ;
//   long nch = spec.nchannels ;
//   long nby = nch * sizeof ( dtype ) ;
//   if ( o > 4 )
//     std::swap ( w , h ) ;
//   dtype buffer [ w * h ] ;
//   long xstride = 1 ;
//   long ystride = w ;
//   long offset = 0 ;
//   gyrate ( o , w , h , xstride , ystride , offset ) ;
//   inp->read_image ( ... , & ( buffer[0] ) + offset ,
//                     nby * xstride , nby * ystride ) ;

bool gyrate ( const int & orientation ,
              const long & target_width ,
              const long & target_height ,
              long & xstride ,
              long & ystride ,
              long & _offset )
{
  // before we do anything: is the orientation value valid?

  if ( orientation < 0 || orientation > 8 )
  {
    // TODO might emit an error
    return false ;
  }
    
  // take a copy of the strides passed in

  long xs = xstride ;
  long ys = ystride ;
  long w = target_width ;
  long h = target_height ;

  // additional offset is initially zero

  long offset = 0 ;
  bool flipped = false ;

  switch ( orientation )
  {
    case 2:
    {
      flipped = true ;
      // fallthrough is deliberate
    }
    case 0: // no EXIF orientation found
    case 1: // file and memory order coincide
    {
      // we needn't do anything, leave the strides as they are.
      break ;
    }
    case 4:
    {
      flipped = true ;
      // fallthrough is deliberate
    }
    case 3: // 180 degree rotation
    {
      // both strides go backwards from the corner diagonally
      // opposite the origin
      xstride = -xs ;
      ystride = -ys ;
      offset = ( h - 1 ) * ys + ( w - 1 ) * xs ;
      break ;
    }
    case 7:
    {
      flipped = true ;
      // fallthrough is deliberate
    }
    case 6: // 90 degrees clockwise
    {
      xstride = ys ;
      ystride = -xs ;
      offset = ( w - 1 ) * xs ;
      std::swap ( w , h ) ;
      break ;
    }
    case 5:
    {
      flipped = true ;
      // fallthrough is deliberate
    }
    case 8: // 90 degrees counterclockwise
    {
      xstride = -ys ;
      ystride = xs ;
      offset = ( h - 1 ) * ys ;
      std::swap ( w , h ) ;
      break ;
    }
    default:
    {
      // can't really happen...
      assert ( false ) ;
      break ;
    }
  }

  // if the orientation indicates that the image is flipped, we need
  // to adapt the strides further

  if ( flipped )
  {
    offset += xstride * ( w - 1 ) ;
    xstride = - xstride ;
  }

  // update the offset the user has passed in. Most of the time the
  // user will have passed zero, but doing it like this makes the
  // code more flexible. TODO: debatable

  _offset += offset ;

  // that's us done

  return true ;
}

} ;

#ifdef USE_OIIO

#include <OpenImageIO/imageio.h>
#include <OpenImageIO/typedesc.h>

using namespace OIIO;

namespace ui
{
  extern int subimage ;
  extern std::vector < std::string > oiio_arg ;
  extern std::vector < std::string > oiio_type ;
  extern std::vector < std::string > oiio_val ;
  extern std::size_t oiio_arg_hash ;
}

namespace fileio
{
// using OIIO, we mimick vigraimpex' file import facilities, to the
// extent that we use them in lux. To code calling into fileio, the
// interface is pretty much the same, so the transition is easy.

// starting to handle subimages. this is tricky, because to display
// a new subimage, a new display cycle has to be triggered, so that
// new image data are read and interpolators built. So far, I've
// coded a bit of GUI code to pick specific subimages, which works,
// but the viewer settings seem 'jumpy' and need work.
// It woud be cleaner to pass the subimage parameter down to the code
// issuing import_image calls (TODO)

// The user may have passed arguments to pass on to the ImageSpec.
// The main process preprocesses the lux-specific syntax for these
// arguments into the three vectors oiio_... and here we extract
// the strings and ass them to the ImageSpec.

void add_oiio_args ( ImageSpec & config )
{
  int i = 0 ;
  for ( auto const & arg : ui::oiio_arg )
  {
    if ( ui::oiio_type [ i ] . size() )
    {
      std::cout << "*** typed oiio argument: " << arg
                << " type: " << ui::oiio_type [ i ]
                << " value: " << ui::oiio_val [ i ]
                << std::endl ;

      // typed argument. OIIO recognizes it's own brand of
      // typestring.

      auto typedesc = TypeDesc ( ui::oiio_type [ i ] ) ;

      // with a type descriptor, we can process the value
      // in string form. The user should separate individual
      // values of multi-value rhs with space or tab.

      config.attribute ( arg , typedesc , ui::oiio_val [ i ] ) ;
    }
    else
    {
      std::cout << "*** processing untyped oiio argument: " << arg
                << " value: " << ui::oiio_val [ i ]
                << std::endl ;

      // untyped argument

      config [ arg ] = ui::oiio_val [ i ] ;
    }
    ++i ;
  }
}

// vigra has the handy function is_image, which lux uses to figure
// out if a file really holds image data. We mimick this function
// using OIIO. Typically, is_image is called first, then an
// image_info is built, and finally the data are read. Since all
// these accesses require an ImageInput object, we cache it and
// only create a new one if a different filename is requested.

static std::string current_file ;
// static int ui::subimage ;
static std::unique_ptr < ImageInput > current_inp ( nullptr ) ;

// this function is a bit convoluted, due to the fact that we have
// to deal with subimages. Ideally, we'd obtain the oiio:subimages
// metadatum and take it from there, but that isn't reliable: I tried
// with some heic test files and they did not yield the datum. So we
// take the approach of trying to open a requested subimage and if it
// can't be accessed we simply 'stay put' where we were before.

static bool next_image ( std::string filename )
{
  static std::size_t oiio_arg_hash = 0 ;
  // int proposed_subimage = ui::subimage ;
  // if ( proposed_subimage < 0 )
  // {
  //   proposed_subimage = 0 ;
  // }
  // if ( filename != current_file )
  // {
  //   proposed_subimage = ui::subimage = 0 ;
  // }
  bool need_reload = (    filename != current_file
                     ) ; //  || proposed_subimage != ui::subimage ) ;
  if ( ui::oiio_arg_hash != oiio_arg_hash )
  {
    oiio_arg_hash = ui::oiio_arg_hash ;
    need_reload = true ;
  }
  if ( need_reload )
  {
    ImageSpec config ;
    config [ "oiio:UnassociatedAlpha" ] = 1 ;
    config [ "raw:auto_bright" ] = 1 ;
    add_oiio_args ( config  ) ;
    current_file = filename ;
    current_inp = ImageInput::open ( filename , &config ) ;
    if ( current_inp == nullptr )
    {
      return false ;
      std::cout << "***** next_image returns false" << std::endl ;
    }
    // else
    // {
    //   if ( current_inp->seek_subimage ( proposed_subimage , 0 ) )
    //   {
    //     // seek-subimage succeeded.
    //     ui::subimage = proposed_subimage ;
    //   }
    //   else
    //   {
    //     // no, we can't. we stay where we were before
    //     current_inp->seek_subimage ( ui::subimage , 0 ) ;
    //   }
    // }
    // reflect back ui::subimage to the ui
    // ui::subimage = ui::subimage ;
  }
  return true ;
}

// is_image is called repeatedly in several steps of the gleaning
// process, but we don't want to repeat the tentative ImageInput::open
// call, so we check whether we already know the status of the image
// from a previous call.

bool is_image ( std::string filename )
{
  static std::string current_image ;
  static bool current_result ;

  if ( filename != current_image )
  {
    current_image = filename ;
    // don't call open, only 'create' - that's enough for the purpose.
    auto inp = ImageInput::create ( filename , false ) ;
    current_result = ( inp != nullptr ) ;
  }
  return current_result ;
}

image_info::image_info ( std::string filename )
{
  if ( next_image ( filename ) )
  {
    _fn = filename ;
    const ImageSpec & spec = current_inp->spec() ;
    _w = spec.width ;
    _h = spec.height ;
    _typestr = std::string ( current_inp->format_name() ) ;
    _nch = spec.nchannels ;

    // we simplify here, and only pass 1 for bytes, 2 for shorts
    // and 4 for ints, halfs and floats: the latter three will
    // result in an array of floats (oiio will convert to float
    // on the fly) and only the first two are initially read as
    // integral values to save space. oiio yields the correct
    // value 2 for half floats, but we want these to be promoted
    // to float on reading while we have no CPU half processing.

    _fsz = spec.format.size() ;
    if ( _fsz > 4 )
      _fsz = 4 ;
      
    if ( spec.format == TypeDesc::UINT8 )
    {
      std::cout << "file contains TypeDesc::UINT8" << std::endl ;
    }
    else if ( spec.format == TypeDesc::INT8 )
    {
      std::cout << "file contains TypeDesc::INT8" << std::endl ;
    }
    else if ( spec.format == TypeDesc::UINT16 )
    {
      std::cout << "file contains TypeDesc::UINT16" << std::endl ;
    }
    else if ( spec.format == TypeDesc::INT16 )
    {
      std::cout << "file contains TypeDesc::INT16" << std::endl ;
    }
    else if ( spec.format == TypeDesc::UINT32 )
    {
      std::cout << "file contains TypeDesc::UINT32" << std::endl ;
    }
    else if ( spec.format == TypeDesc::INT32 )
    {
      std::cout << "file contains TypeDesc::INT32" << std::endl ;
    }
    else if ( spec.format == TypeDesc::HALF )
    {
      std::cout << "file contains TypeDesc::HALF" << std::endl ;
      std::cout << "will use float internally" << std::endl ;
      _fsz = 4 ;
    }
    else
    {
      std::cout << "fallback: will use float internally" << std::endl ;
      _fsz = 4 ;
    }
    std::cout << "image info for " << _fn << ":" << std::endl ;
    std::cout << "width " << _w << " height " << _h << std::endl ;
    std::cout << "typestring " << _typestr << " channels " << _nch << std::endl ;
    std::cout << "target format size " << _fsz << std::endl ;
  }
}

std::size_t image_info::width() const { return _w ; }
std::size_t image_info::height() const { return _h ; }
vigra::TinyVector < std::size_t , 2 > image_info::shape() const
{ vigra::TinyVector < std::size_t , 2 > res ;
  res[0] = _w ;
  res[1] = _h ;
  return res ; }
const char * image_info::getFileType() const { return _typestr.c_str() ; }
const char * image_info::getFileName() const { return _fn.c_str() ; }
int image_info::numExtraBands() const { return ( _nch > 3 || _nch == 2 ) ; } 
bool image_info::isGrayscale() const { return ( _nch == 1 || _nch == 2 ) ; } 
int image_info::pixelType() const { return _fsz ; } // TODO!

void import_image ( const image_info & info ,
                    void * data ,
                    std::size_t bytes_per_channel ,
                    std::size_t nchannels ,
                    long x_stride ,
                    long y_stride )
{
  assert ( bytes_per_channel <= 4 ) ;

  // lux always uses RGB buffers - reads from single-channel files will
  // replicate the single channel to all three colour channels.
  // TODO: it should be more efficient to only read the first channel
  // and do the copying-out with an in-memory operation (like, G,B = R)
  // in preference to issuing several read_image calls as it's done now.

  assert ( nchannels == 3 ) ;
  assert ( info.getFileName() == current_file ) ;
  assert ( current_inp != nullptr ) ;

  const auto & inp ( current_inp ) ;
  const ImageSpec &spec = inp->spec() ;

  // this is for safety, but may be unnecessary. It suppresses attempts
  // to load invalid subimages, which otherwise come out plain black.

  // TODO: looks like it's okay to simply use ui::subimage directly.

  // ui::subimage = ui::subimage ;
  // assert ( ui::subimage >= 0 ) ;
  // std::cout << "********** ui::subimage " << ui::subimage << std::endl ;
  // 
  // if ( ui::subimage > 0 )
  // {
  //   if ( ! inp->seek_subimage ( ui::subimage , 0 ) )
  //   {
  //     std::cout << "access to subimage failed" << std::endl ;
  //     ui::subimage = 0 ;
  //     inp->seek_subimage ( ui::subimage , 0 ) ;
  //   }
  // }
  // else
  // {
  //   inp->seek_subimage ( 0 , 0 ) ;
  // }

  // OIIO works with byte strides, we have pixel strides

  x_stride *= bytes_per_channel * nchannels ;
  y_stride *= bytes_per_channel * nchannels ;

  // we'll handle monochrome and RGB files only

  assert ( spec.nchannels == 1 || spec.nchannels == 3 ) ;

  OIIO::TypeDesc typedesc ;
  if ( bytes_per_channel == 1 )
    typedesc = TypeDesc::UINT8 ;
  else if ( bytes_per_channel == 2 )
    typedesc = TypeDesc::UINT16 ;
  else if ( bytes_per_channel == 4 )
    typedesc = TypeDesc::FLOAT ;
  else if ( bytes_per_channel == 8 )
    typedesc = TypeDesc::DOUBLE ;

  // we follow a simple logic. We read 8-bit data to uchar, 16-bit
  // data to ushort, and 32-bit data to float. Reading the original
  // data is only to preserve space, in case the file is very large,
  // and there would be no real gain from reading int32 as-is, instead
  // having float straight away simplifies matters further down the
  // line. This is the plain RGB code, so we can read the entire data
  // set en bloc.
  // monochrome files are read to RGB buffers, filling the R, G and B
  // components with identical data.

  if ( spec.nchannels == 1 )
  {
    auto success = inp->read_image ( ui::subimage , 0 , 0 ,
                                     1, typedesc ,
                                     (unsigned char *) data,
                                     x_stride , y_stride , AutoStride ) ;
    if ( ! success )
    {
      std::cerr  << "error: " << inp->geterror() << "\n";
    }
    else
    {
      inp->read_image ( ui::subimage , 0 , 0 ,
                        1, typedesc ,
                        (unsigned char *) data + bytes_per_channel ,
                        x_stride , y_stride , AutoStride ) ;
      inp->read_image ( ui::subimage , 0 , 0 ,
                        1, typedesc ,
                        (unsigned char *) data + 2 * bytes_per_channel ,
                        x_stride , y_stride , AutoStride ) ;
    }
  }
  else if ( spec.nchannels == 3 )
  {
    auto success = inp->read_image ( ui::subimage , 0 , 0 ,
                      nchannels, typedesc ,
                      data,
                      x_stride , y_stride , AutoStride ) ;
    if ( ! success )
    {
      std::cerr << "error: " << inp->geterror() << "\n";
    }
  }
}

// lux reads RGB and alpha data separately, so that an extant
// alpha channel can be discarded, or a missing one added without
// much ado. The RGB part is handled with a 3-channel-read (note
// the 0, 3 designating first and one-after-last channel) and the
// alpha channel follows as a separate read. With this scheme,
// the existing logic in lux can be reused.
// Note that if the input has two channels only, we assume it's
// grey and alpha, and copy the grey values to R, G and B in the
// target buffer.

void import_image_alpha ( const image_info & info ,
                          void * rgb , void * alpha ,
                          std::size_t bytes_per_channel ,
                          std::size_t nchannels ,
                          long x_stride ,
                          long y_stride ,
                          long xa_stride ,
                          long ya_stride ,
                          std::size_t bytes_per_alpha_channel )
{
  assert ( bytes_per_channel <= 4 ) ;
  if ( bytes_per_alpha_channel == 0 )
    bytes_per_alpha_channel = bytes_per_channel ;

  // we always read to one RGB and one alpha buffer. If there are only
  // two channels in the file, we assume that the first is intensity and
  // the second is alpha (TODO: does this ever happen?)

  assert ( nchannels == 4 ) ; // target holds R, G, B and A
  assert ( info.getFileName() == current_file ) ;

  const auto & inp ( current_inp ) ;

  assert ( inp != nullptr ) ;
  const ImageSpec &spec = inp->spec() ;
  assert ( spec.nchannels == 2 || spec.nchannels == 4 ) ;

  // this is for safety, but may be unnecessary. It suppresses attempts
  // to load invalid subimages, which otherwise come out plain black.

  // assert ( ui::subimage >= 0 ) ;
  // std::cout << "********** ui::subimage " << ui::subimage << std::endl ;
  // 
  // if ( ui::subimage > 0 )
  // {
  //   if ( ! inp->seek_subimage ( ui::subimage , 0 ) )
  //   {
  //     std::cout << "access to subimage failed" << std::endl ;
  //     ui::subimage = 0 ;
  //     inp->seek_subimage ( ui::subimage , 0 ) ;
  //   }
  // }
  // else
  // {
  //   inp->seek_subimage ( 0 , 0 ) ;
  // }

  x_stride *= 3 * bytes_per_channel ;
  y_stride *= 3 * bytes_per_channel ;
  xa_stride *= bytes_per_alpha_channel ;
  ya_stride *= bytes_per_alpha_channel ;

  OIIO::TypeDesc typedesc ;
  if ( bytes_per_channel == 1 )
    typedesc = TypeDesc::UINT8 ;
  else if ( bytes_per_channel == 2 )
    typedesc = TypeDesc::UINT16 ;
  else if ( bytes_per_channel == 4 )
    typedesc = TypeDesc::FLOAT ;
  else if ( bytes_per_channel == 8 )
    typedesc = TypeDesc::DOUBLE ;

  OIIO::TypeDesc alpha_typedesc ;
  if ( bytes_per_alpha_channel == 1 )
    alpha_typedesc = TypeDesc::UINT8 ;
  else if ( bytes_per_alpha_channel == 2 )
    alpha_typedesc = TypeDesc::UINT16 ;
  else if ( bytes_per_alpha_channel == 4 )
    alpha_typedesc = TypeDesc::FLOAT ;
  else if ( bytes_per_alpha_channel == 8 )
    alpha_typedesc = TypeDesc::DOUBLE ;

  // TODO: check return value of read_image
  if ( spec.nchannels == 2 )
  {
    inp->read_image ( ui::subimage, 0, 0, 1, typedesc,
                      (unsigned char *) rgb,
                      x_stride , y_stride , AutoStride ) ;
    inp->read_image ( ui::subimage, 0, 0, 1, typedesc,
                      (unsigned char *) rgb + bytes_per_channel ,
                      x_stride , y_stride , AutoStride ) ;
    inp->read_image ( ui::subimage, 0, 0, 1, typedesc,
                      (unsigned char *) rgb + 2 * bytes_per_channel,
                      x_stride , y_stride , AutoStride ) ;

    inp->read_image ( ui::subimage, 0, 1, 2, alpha_typedesc,
                      (unsigned char *) alpha ,
                      xa_stride , ya_stride , AutoStride ) ;
  }
  else
  {
    inp->read_image ( ui::subimage, 0, 0, 3, typedesc,
                      rgb,
                      x_stride , y_stride , AutoStride ) ;

    inp->read_image ( ui::subimage, 0, 3, 4, alpha_typedesc,
                      alpha ,
                      xa_stride , ya_stride , AutoStride ) ;
  }
}

} ; // namespace fileio

#else // #ifdef USE_OIIO

namespace fileio
{
  // without OIIO, we map the fileio functionality to vigaimpex,
  // which is trivial, because that is the 'native' interface that
  // lux was initially written with.

  bool is_image ( std::string filename )
  {
    return vigra::isImage ( filename.c_str() ) ;
  }

  using vigra::importImage ;
  using vigra::importImageAlpha ;

} ; // namespace fileio

#endif // #else #ifdef USE_OIIO

