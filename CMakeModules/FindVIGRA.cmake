# this file was copied from the hugin source tree, see
# https://sourceforge.net/p/hugin/hugin/ci/default/tree/CMakeModules/FindVIGRA.cmake
# The original file has no licensing information, so I assume that
# copying from it is okay - I removed some hugin-specific stuff

# - Find VIGRA 
# reads cache variables
# defines cache variables
#  VIGRA_INCLUDE_DIR, where to find headers
#  VIGRA_LIBRARIES, list of release link libraries
#  VIGRA_FOUND, If != "YES", error out as VIGRA is required

SET( VIGRA_FOUND "NO" )

IF(WIN32)
  FIND_PATH(VIGRA_INCLUDE_DIR vigra/gaborfilter.hxx 
      PATHS ${SOURCE_BASE_DIR}/vigra/include
  )

  FIND_LIBRARY_WITH_DEBUG(VIGRA_LIBRARIES
    NAMES vigraimpex libvigraimpex 
    PATHS
    ${VIGRA_ROOT_PATH}
    ${VIGRA_ROOT_PATH}/Release
    ${VIGRA_ROOT_PATH}/lib
    ${SOURCE_BASE_DIR}/vigra/lib
    )
ELSE(WIN32)
  FIND_PATH(VIGRA_INCLUDE_DIR vigra/gaborfilter.hxx
    /usr/local/include
    /usr/include
    /opt/local/include
  )

  FIND_LIBRARY(VIGRA_LIBRARIES
    NAMES vigraimpex libvigraimpex 
    PATHS /usr/lib /usr/local/lib /opt/local/lib
 ) 
ENDIF(WIN32)


IF (VIGRA_INCLUDE_DIR AND VIGRA_LIBRARIES)
   SET(VIGRA_FOUND TRUE)
ENDIF (VIGRA_INCLUDE_DIR AND VIGRA_LIBRARIES)

IF (VIGRA_FOUND)
  MESSAGE(STATUS "Found VIGRA: ${VIGRA_LIBRARIES}")
  FIND_FILE(
    VIGRA_CONFIG_VERSION_HXX
    NAMES configVersion.hxx config_version.hxx
    PATHS "${VIGRA_INCLUDE_DIR}/vigra/"
  )
  IF(NOT VIGRA_CONFIG_VERSION_HXX)
    MESSAGE(FATAL_ERROR "Could not find vigra/configVersion.hxx or vigra/config_version.hxx. Your vigra installation seems to be corrupt.")
  ENDIF()

ELSE (VIGRA_FOUND)
	MESSAGE(FATAL_ERROR "Could not find VIGRA")
ENDIF (VIGRA_FOUND)


MARK_AS_ADVANCED(
  VIGRA_LIBRARIES
  VIGRA_INCLUDE_DIR
  )

if (VIGRA_FOUND)
	message(STATUS "FindVIGRA.cmake module: VIGRA_FOUND = true")
else()
	message(STATUS "FindVIGRA.cmake module: VIGRA_FOUND = false")
endif()
