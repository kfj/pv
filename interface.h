/************************************************************************/
/*                                                                      */
/*          lux - panora and image viewer                               */
/*                                                                      */
/*          Copyright 2016 - 2023 by Kay F. Jahnke                      */
/*                                                                      */
/*  The git repository for this software is at                          */
/*                                                                      */
/*  https://bitbucket.org/kfj/pv                                        */
/*                                                                      */
/*  Please direct questions, bug reports, and contributions to          */
/*                                                                      */
/*  kfjahnke+pv@gmail.com                                               */
/*                                                                      */
/*  pv is free software: you can redistribute it and/or modify          */
/*  it under the terms of the GNU General Public License as published   */
/*  by the Free Software Foundation, either version 3 of the License,   */
/*  or (at your option) any later version.                              */
/*                                                                      */
/*  pv is distributed in the hope that it will be useful,               */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       */
/*  GNU General Public License for more details.                        */
/*                                                                      */
/*  You should have received a copy of the GNU General Public License   */
/*  along with pv.  If not, see <http://www.gnu.org/licenses/>.         */
/*                                                                      */
/************************************************************************/

/*! \file interface.h

    \brief code to define ISA-specific routines

    This file is #included twice:

    - once in pv_common.h with SET_PURE #defined to produce pure
      virtual member functions of the dispatch base class

    - once in dispatch.h with SET_PURE #defined to produce overrides
      for each ISA-specific derived dispatch class

    The reason for factoring out these declarations into a separate
    header is to ensure both class definitions share the same signature.
*/

SET_PURE ( interpolator_base * make_interpolator
                               ( const char * filename ,
                                 const source_type & source ,
                                 bool process_linear ,
                                 alpha_mode mode ,
                                 bool grey_edge ,
                                 int _lq_degree ,
                                 int _hq_degree ,
                                 double _scaling_step ,
                                 int _smoothing_level ,
                                 int _cbf_degree ,
                                 int _floor ,
                                 bool build_pyramids ,
                                 bool build_raw_pyramids ,
                                 int subimage ,
                                 interpolator_base * p_recycle
                               ) const
         ) ;

SET_PURE ( interpolator_base * make_cube_itp
            ( const std::vector < source_type > & source_v ,
              bool _process_linear ,
              alpha_mode _mode ,
              int _lq_degree ,
              int _hq_degree ,
              double _scaling_step ,
              int _smoothing_level ,
              int _cbf_degree ,
              int _floor ,
              bool _build_pyramids ,
              bool build_raw_pyramids ,
              interpolator_base * p_recycle ) const ) ;

SET_PURE ( interpolator_base * make_facet_itp
            ( const std::vector < source_type > & source_v ,
              const std::vector < quaternion_type > & _facet_orientation ,
              bool _fully_covered ,
              bool _process_linear ,
              alpha_mode _mode ,
              bool _grey_edge ,
              int _lq_degree ,
              int _hq_degree ,
              double _scaling_step ,
              int _smoothing_level ,
              int _cbf_degree ,
              int _floor ,
              bool _build_pyramids ,
              bool _build_raw_pyramids ,
              interpolator_base * p_recycle ) const ) ;

SET_PURE ( void destroy_itp ( interpolator_base * p_itp ) const ) ;

SET_PURE ( void generate_main() const ) ;

SET_PURE ( void launch ( std::function < void() > payload ) const ) ;

SET_PURE ( void init() const ) ;

SET_PURE ( void exit() const ) ;

SET_PURE ( void process_fusion_job ( job_type * p_baseline_job ) const ) ;

SET_PURE ( void process_stitching_job ( job_type * p_baseline_job ) const ) ;

SET_PURE ( void rgb2srgb ( view_type & image ,
                           int njobs ) const ) ;

SET_PURE ( void rgba2srgba ( view4_type & image ,
                             int njobs ) const ) ;

SET_PURE ( void argba2sargba ( view4_type & image ,
                               int njobs ) const ) ;

SET_PURE ( void srgb2rgb ( view_type & image ,
                           int njobs ) const ) ;

SET_PURE ( void srgba2rgba ( view4_type & image ,
                             int njobs ) const ) ;

SET_PURE ( void sargba2argba ( view4_type & image ,
                               int njobs ) const ) ;

SET_PURE ( void associate_alpha ( view4_type & image ,
                                  int njobs ) const ) ;

SET_PURE ( void deassociate_alpha ( view4_type & image ,
                                    int njobs ) const ) ;

SET_PURE ( crdd_tf_type get_inter_facet_tf ( const source_type & trg ,
                                  const source_type & src ,
                                  quaternion_type rotation ) const ) ;

SET_PURE ( to_model_f get_target_to_model_tf
                       ( const job_type * const p_job ) const ) ;
