/************************************************************************/
/*                                                                      */
/*          pv - a viewer for panoramic images                          */
/*                                                                      */
/*          Copyright 2016 - 2023 by Kay F. Jahnke                      */
/*                                                                      */
/*  The git repository for this software is at                          */
/*                                                                      */
/*  https://bitbucket.org/kfj/pv                                        */
/*                                                                      */
/*  Please direct questions, bug reports, and contributions to          */
/*                                                                      */
/*  kfjahnke+pv@gmail.com                                               */
/*                                                                      */
/*  pv is free software: you can redistribute it and/or modify          */
/*  it under the terms of the GNU General Public License as published   */
/*  by the Free Software Foundation, either version 3 of the License,   */
/*  or (at your option) any later version.                              */
/*                                                                      */
/*  pv is distributed in the hope that it will be useful,               */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       */
/*  GNU General Public License for more details.                        */
/*                                                                      */
/*  You should have received a copy of the GNU General Public License   */
/*  along with pv.  If not, see <http://www.gnu.org/licenses/>.         */
/*                                                                      */
/************************************************************************/

/*! \file options.h
 
    \brief long command line options for pv

    this header has the definitions of command line options. It is included
    several times with different definitions of the function-like macros
    used to set up argument gathering. See the calling code for the
    definition of the macros.

    The name of the macro indicates thy type of the argument, so yes_no,
    for example, introduces an argument which will result in a boolean
    value.

    The first argument to the macro is the name of the option, the second
    one holds the value which the option is set to initially. Further
    arguments depend on the type of the option.

    The initial values do not necessarily constitute usable values; the
    calling code interprets them and some of the initial values merely
    'mean' that the value was not set at all.
*/

yes_no  ( allow_pan_mode , true )
one_of  ( alpha , "auto" ,
          "no" , "as-file" , "auto" , "yes" )
yes_no  ( auto_position , true )
yes_no  ( auto_quality , true )
real    ( autopan , 0.0 )
one_of  ( blending , "auto" , "auto" , "ranked" , "hdr" , "quorate" )
integer ( bls_i_spline_decimator , -2 )
integer ( bls_i_spline_degree , 1 )
integer ( bls_i_spline_shift_to , 2 )
integer ( bls_q_spline_decimator , -2 )
integer ( bls_q_spline_degree , 1 )
integer ( bls_q_spline_shift_to , 2 )
real    ( budget , 0.0 )
yes_no  ( build_pyramids , true )
yes_no  ( build_raw_pyramids , true )
integer ( cbf_degree , 3 )
real    ( click_drag_threshold , 10 )
yes_no  ( compress , false )
real    ( contrast_weight , 0.0 )
yes_no  ( cropping_active , false )
integer ( crop_x0 , -1 )
integer ( crop_x1 , -1 )
integer ( crop_y0 , -1 )
integer ( crop_y1 , -1 )
real    ( lens_crop_factor , 0.0 )
yes_no  ( crossfade , true )
real    ( crossfade_delta , 0.05 )
option  ( cube_left , "" )
option  ( cube_right , "" )
option  ( cube_top , "" )
option  ( cube_bottom , "" )
option  ( cube_front , "" )
option  ( cube_back , "" )
angle   ( cubeface_fov , 0.0 )
yes_no  ( decimate_area , false )
yes_no  ( deghost , false )
real    ( exposure_sigma , 0.2 )
real    ( exposure_mu , 0.5 )
real    ( exposure_weight , 1.0 )
real    ( exposure_scaling_step , 2.0 )
integer ( exposure_pyramid_floor , 16 )
list    ( facet , {} )
rlist   ( facet_brightness , {} )
blist   ( facet_crop_active , {} )
blist   ( facet_crop_elliptic , {} )
rlist   ( facet_crop_fade , {} )
rlist   ( facet_crop_x0 , {} )
rlist   ( facet_crop_x1 , {} )
rlist   ( facet_crop_y0 , {} )
rlist   ( facet_crop_y1 , {} )
rlist   ( facet_handicap , {} )
alist   ( facet_hfov , {} )
blist   ( facet_is_linear , {} )
rlist   ( facet_lca , {} )
rlist   ( facet_lcb , {} )
rlist   ( facet_lcc , {} )
rlist   ( facet_lch , {} )
rlist   ( facet_lcs , {} )
rlist   ( facet_lcv , {} )
alist   ( facet_pitch , {} )
one_of  ( facet_priority , "none" ,
          "none" , "explicit" , "hfov" , "order" )
list    ( facet_projection , {} )
alist   ( facet_roll , {} )
rlist   ( facet_squash , {} )
ilist   ( facet_stack_parent , {} )
rlist   ( facet_vca , {} )
rlist   ( facet_vcb , {} )
rlist   ( facet_vcc , {} )
rlist   ( facet_vcd , {} )
rlist   ( facet_vcx , {} )
rlist   ( facet_vcy , {} )
alist   ( facet_yaw , {} )
integer ( fast_interpolator_degree , 1 )
yes_no  ( faux_bracket , false )
rlist   ( faux_bracket_ev , {} )
real    ( faux_bracket_step , 2.0 )
integer ( faux_bracket_size , 3 )
real    ( feathering , 0.0 )
yes_no  ( focus_stack , false )
integer ( frame_rate_limit , 0 )
integer ( frame_queue_limit , 2 )
yes_no  ( fullscreen , START_IN_FULLSCREEN_MODE )
yes_no  ( fully_covered , false )
yes_no  ( fuse , false )
yes_no  ( grey_edge , true )
option  ( gui_font , "" )
integer ( gui_extent , 0 )
real    ( gui_scale_factor , 0.0 )
yes_no  ( gpu_for_srgb , false )
yes_no  ( hdr_fuse , false )
yes_no  ( hdr_merge , false )
real    ( hdr_spread , 0.0 )
yes_no  ( help , false )
angle   ( hfov , 0.0 )
angle   ( hfov_view , 0.0 )
angle   ( horizontal_offset , -1.0 )
list    ( image , {} )
real    ( imgui_font_size , 24.0 )
angle   ( initial_pitch , 0.0 )
angle   ( initial_roll , 0.0 )
angle   ( initial_yaw , 0.0 )
angle   ( initial_dr , 0.0 )
real    ( initial_dx , 0.0 )
real    ( initial_dy , 0.0 )
yes_no  ( input_is_pto , false )
yes_no  ( is_linear , false )
one_of  ( isa , "auto" ,
          "f" , "a" , "2" , "3" , "4" , "5" , "p" , "auto" ,
          "fallback" , "ssse3" , "sse4.2" , "avx" , "avx2" ,
          "avx512f" , "plain")
integer ( job_queue_limit , 2 )
yes_no  ( legacy_gui , false )
real    ( level_bias , 0.0 )
one_of  ( light_balance , "by_ev" , "by_ev" , "hedged" , "auto" )
real    ( magnifying_glass_factor , 10.0 )
list    ( metadata_query , {} )
option  ( metadata_format , "" )
real    ( moving_image_scaling , 1.0 )
yes_no  ( next_after_snapshot , false )
yes_no  ( next_after_fusion , false )
yes_no  ( next_after_stitch , false )
list    ( oiio_arg , {} )
real    ( output_magnification , 1.0 )
option  ( path , "" )
yes_no  ( process_linear , true )
one_of  ( projection , "auto" ,
          "" , "auto" , "m" , "r" , "s" , "c" , "f" , "g" ,
          "spherical" , "cylindric" , "rectilinear" ,
          "map" , "mosaic" , "stereographic" , "fisheye" , "cubemap" , "facet_map" )
real    ( pyramid_scaling_step , 2.0 )
integer ( pyramid_smoothing_level , -1 )
integer ( quality_interpolator_degree , 3 )
yes_no  ( rectilinear_as_mosaic , false )
yes_no  ( reverse_drag , false )
yes_no  ( reverse_secondary_drag , false )
yes_no  ( scvd_focused_zoom , false )
yes_no  ( scw_focused_zoom , true )
yes_no  ( show_error_dialog , false )
yes_no  ( show_status_line , true )
integer ( single_click_slot , 300 )
real    ( slide_interval , 7.0 )
yes_no  ( slideshow_on , false )
yes_no  ( snapshot_like_source , false )
integer ( snapshot_facet , -1 )
real    ( snapshot_magnification , 1.0 )
option  ( snapshot_prefix , "" )
option  ( snapshot_basename , "" )
integer ( snapshot_threads , 0 )
real    ( snappiness , 0.005 )
integer ( snapshot_compression , 90 )
one_of  ( snapshot_extension , "jpg" ,
         "JPG" , "JPEG" , "PNG" , "TIF" , "TIFF" , "EXR" ,
         "jpg" , "jpeg" , "png" , "tif" , "tiff" , "exr" )
yes_no  ( snapshot_tiff_linear , false )
yes_no  ( snap_to_fusion , true )
yes_no  ( snap_to_hq , true )
yes_no  ( snap_to_stitch , true )
one_of  ( stack , "fusion" , "fusion" , "first" , "hdr" )
yes_no  ( stitch , false )
integer ( solo , -1 )
real    ( spin_threshold , 5 )
integer ( squash , 0 )
real    ( still_image_scaling , 1.0 )
integer ( stop_after , 0 )
yes_no  ( suppress_display , false )
one_of  ( target_projection , "rectilinear" ,
          "rectilinear" , "spherical" , "cylindric" ,
          "stereographic" , "fisheye" , "mosaic" )
yes_no  ( tonemap , false )
integer ( uncropped_width , -1 )
integer ( uncropped_height , -1 )
angle   ( uncropped_hfov , 0.0 )
angle   ( uncropped_vfov , 0.0 )
yes_no  ( use_pto_masks , true )
yes_no  ( use_vsync , true )
yes_no  ( version , false )
angle   ( vertical_offset , -1.0 )
angle   ( vfov , 0.0 )
integer ( window_width , 0 )
integer ( window_height , 0 )
integer ( worker_threads , 0 )

// The macros are undefined after every inclusion of options.h

#undef option
#undef real
#undef angle
#undef integer
#undef yes_no
#undef list
#undef rlist
#undef ilist
#undef alist
#undef blist
#undef one_of
