/************************************************************************/
/*                                                                      */
/*          pv - a viewer for panoramic images                          */
/*                                                                      */
/*          Copyright 2016 - 2023 by Kay F. Jahnke                      */
/*                                                                      */
/*  The git repository for this software is at                          */
/*                                                                      */
/*  https://bitbucket.org/kfj/pv                                        */
/*                                                                      */
/*  Please direct questions, bug reports, and contributions to          */
/*                                                                      */
/*  kfjahnke+pv@gmail.com                                               */
/*                                                                      */
/*  pv is free software: you can redistribute it and/or modify          */
/*  it under the terms of the GNU General Public License as published   */
/*  by the Free Software Foundation, either version 3 of the License,   */
/*  or (at your option) any later version.                              */
/*                                                                      */
/*  pv is distributed in the hope that it will be useful,               */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       */
/*  GNU General Public License for more details.                        */
/*                                                                      */
/*  You should have received a copy of the GNU General Public License   */
/*  along with pv.  If not, see <http://www.gnu.org/licenses/>.         */
/*                                                                      */
/************************************************************************/

/*! \file pv_initialize.h
 * 
 *  \brief code to process command line options
 * 
 *  This header declares struct ini::state_type, which contains all
 *  values that are set during program initialization. a single instance
 *  of this class lives in pyshared.cc. pyshared.cc is compiled into a
 *  shared library which is linked into pv and also used by cppyy in the
 *  python code to share these values with python. With this construction,
 *  the c++ and python side can effectively share ini::state without
 *  having to communicate: pv invokes a python function when it wants
 *  the python side to 'mess with' ini::state, the python function
 *  accesses ini::state by loading pyshared.so and manipulates it,
 *  and when it returns, the c++ side has a modified ini::state to
 *  proceed with.
 *  There are also some declarations of functions in namespace ini;
 *  the actual code for namespace ini is in pv_initialize.cc
 */

#include <string>
#include <vector>

namespace ini
{
// to hold per-facet arguments, we use a class inheriting from std::vector,
// with an additional flag 'touched'. If, on a push_back, this flag is
// found to be false (as it is after construction), the vector is cleared
// before pushing back the item. The function 'listen' clears this flag,
// making the arg_vector object behave as if it had just been constructed.
// If the flag is false and no push_back occurs, the vector's content
// persists, which is needed when override arguments are processed: in this
// situation, the current state must persisit unless it's explicitly
// modified by the override arguments.

template < typename value_type >
struct arg_vector
: public std::vector < value_type >
{
  typedef std::vector < value_type > inner_t ;
  bool touched ;

  // perfect-forward all arguments to std::vector's c'tor

  template < typename ... types >
  arg_vector ( types && ... args )
  : inner_t ( std::forward < types > ( args ) ... )
  {
    touched = false ;
  } ;

  // clear 'touched' flag

  void listen()
  {
    touched = false ;
  }

  void push_back ( const value_type & arg )
  {
    if ( ! touched )
    {
      // push_back with touched == false:
      // set touched, then clear the vector befor pushing back 'arg'

      touched = true ;
      inner_t::clear() ;
    }
    inner_t::push_back ( arg ) ;
  }
} ;

// These definitions will place the target variables into state_type.
// Note how the macros emplace member variables of a type appropriate
// to the emplacement macro:

#define option(NAME,DEFAULT) std::string NAME ;

#define real(NAME,DEFAULT) double NAME ;

#define angle(NAME,DEFAULT) double NAME ;

#define integer(NAME,DEFAULT) int NAME ;

#define yes_no(NAME,DEFAULT) bool NAME ;

#define list(NAME,DEFAULT) arg_vector < std::string > NAME ;

#define rlist(NAME,DEFAULT) arg_vector < double > NAME ;

#define ilist(NAME,DEFAULT) arg_vector < int > NAME ;

#define alist(NAME,DEFAULT) arg_vector < double > NAME ;

#define blist(NAME,DEFAULT) arg_vector < bool > NAME ;

#define one_of(NAME,DEFAULT,...) std::string NAME ; \
  std::vector<std::string> NAME##_set { __VA_ARGS__ } ;

struct mask_type
{
  int image ;
  int variant ;
  std::string vertex_list ;
  std::vector < float > vx ;
  std::vector < float > vy ;

  mask_type()
  : image ( -1 ) ,
    variant ( -1 )
    { }
} ;

struct state_type
{
  // state_type's member variables are emplaced by the macro invocations
  // in options.h, which define the set of options.

  #include "options.h"

  state_type() ;

  void listen() ;

  void clear() ;

  bool have_p_line ;
  projection_type p_projection ;
  double p_hfov ;
  int p_width ;
  int p_height ;
  bool have_crop ;
  int p_crop_x0 ;
  int p_crop_x1 ;
  int p_crop_y0 ;
  int p_crop_y1 ;
  std::vector < mask_type > maskv ;
} ;

// ini::state contains the initailization information. We use a global
// variable here since there can only be one currently valid initialization
// at a time. The structure definition has to be visible to pv_no_rendering.cc
// so that it can glean the initial values. The internal workings are
// local to pv_initialize.cc

extern state_type state ;
extern state_type arguments ;

// alias, we'll use this often during program initialization

extern ini::state_type & args ;

// There are functions in namespace ini which are called by pv_no_rendering.cc
// locate_file prepends args.path to filenames without path.
// read_ini_file reads an ini file and modifies ini::state accordingly, and
// initialize performs the processing of argc and argv on program invocation,
// also modifying ini::state accordingly.

extern std::string script_path ;

std::string locate_file ( std::string filename ) ;

bool read_ini_file ( const std::string & filename , bool fix_path ) ;

bool read_pto_file ( const std::string & filename ) ;

void synonym ( const std::string & source , const std::string & alias ) ;

int initialize ( int argc , const char * argv[] ) ;

void list_arguments ( std::ostream & str ) ;

// structure for session-wide settings
} ;
