/************************************************************************/
/*                                                                      */
/*          lux - panora and image viewer                               */
/*                                                                      */
/*          Copyright 2016 - 2023 by Kay F. Jahnke                      */
/*                                                                      */
/*  The git repository for this software is at                          */
/*                                                                      */
/*  https://bitbucket.org/kfj/pv                                        */
/*                                                                      */
/*  Please direct questions, bug reports, and contributions to          */
/*                                                                      */
/*  kfjahnke+pv@gmail.com                                               */
/*                                                                      */
/*  pv is free software: you can redistribute it and/or modify          */
/*  it under the terms of the GNU General Public License as published   */
/*  by the Free Software Foundation, either version 3 of the License,   */
/*  or (at your option) any later version.                              */
/*                                                                      */
/*  pv is distributed in the hope that it will be useful,               */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       */
/*  GNU General Public License for more details.                        */
/*                                                                      */
/*  You should have received a copy of the GNU General Public License   */
/*  along with pv.  If not, see <http://www.gnu.org/licenses/>.         */
/*                                                                      */
/************************************************************************/

/*! \file pv_rendering_common.h

    \brief pv - a panorama viewer

    For documentation, please see the bitbucket repository at

    https://bitbucket.org/kfj/pv

    This file has definitions common to all rendering code

    vspline is making heavy use of vectorization (if possible), that's
    the reason for the performance gain coming from compilation with
    ISA-specific flags. vspline can, in turn, use Vc to produce it's
    vector code. As far as vspline is concerned, use of Vc is 'nice to
    have', but there is other pixel-pipeline code in pv_rendering.cc
    which benefits a great deal from Vc.

    Since we produce separate ISA-specific object files, we also 'dub'
    vspline: further down you'll see that we #define 'vspline' to become
    PV_ARCH. So we don't use namespace vspline at all, but pack a copy
    of vspline into each ISA-specific namespace. This is possible because
    vspline is header-only. The result is that there are no mangled names
    pertaining to namespace vspline at all (because there is no namespace
    vspline), and all ISA-specific 'incarnations' of vspline are technically
    separate. The linker may still recognize some binary code as redundant,
    but it won't stumble upon several object files providing binary code
    with the same mangled name but different assembler. While this would
    usually not result in a linker error, the linker would nevertheless
    pick the binary code from the first object file which has binary for
    a given mangled name, with unpredictable consequences.
*/

// pull in pv_common.h for definitions common to all pv code

#include "pv_common.h"

// next, pull in all of vspline

// run vspline wielding code with segment size 512
// This size refers to the amount of pixels processed by a 'joblet'.
// The multithreading code breaks nD arrays down into chunks
// of 1D data, each of which is a section of a line in the data.
// Choosing 512 here turned out a good compromise on my system;
// using larger sizes will eventually become problematic due to
// cache overflow, and using smaller sizes produces more overhead.
// TODO: On other systems, other values may perform better.

#define WIELDING_SEGMENT_SIZE 512
#define VSPLINE_EXTERN_THREAD_POOL

#define SHIFT_CEIL 4
#define BUILD_ARGBA

// define 'vspline' to be PV_ARCH, which is an ISA-specific value like
// PV_AVX or PV_AVX2. This produces a separate 'incarnation' of vspline
// with it's own architecture-specifc namespace name instead of 'vspline',
// allowing pv to mix several architectures in one binary.

#define vspline PV_ARCH

// #include all vspline headers from copies given in pv.git, rather
// than expecting vspline to be installed. This makes distribution easier,
// because pv.git now contains all the relevant vspline headers and version
// conflicts become less likely. Note how, above, we have defined vspline
// as PV_ARCH, which is in turn defined by the makefile. This effectively
// makes the includes below set up a separate 'incarnation' of vspline,
// providing all vspline code in a different 'alias' namespace. By
// doing so, we can have code for several ISAs coexist in the final binary.

#include "vspline/vspline.h"

// some additional headers help with vectorized data. vector_promote.h
// defines vigra promote traits for vector data, and xel_of_vector.h
// defines operations with 'xel' of vector, meaning vectorized pixels etc.
// We include these headers first with varying definitions of VECTOR_TYPE.
// With type promotion defined to vigra standards, we'll be able to
// use vigra code with SIMD data - in pv, this is mainly the use of
// simdized quaternions. The code in xel_of_vector.h is merely for
// brevity, to avoid having to hand-code small loops over the elements
// of a 'xel'. Since this is relatively new code, a lot of the pv code
// base still uses such loops, and will only be adopting the new ways
// eventually.

#define XEL_TYPE vigra::TinyVector

#define VECTOR_TYPE vspline::simd_type
#include "vspline/opt/vector_promote.h"
#include "vspline/opt/xel_of_vector.h"
#undef VECTOR_TYPE

#ifdef USE_VC

#define VECTOR_TYPE PV_ARCH::vc_simd_type
#include "vspline/opt/vector_promote.h"
#include "vspline/opt/xel_of_vector.h"
#undef VECTOR_TYPE

#endif

#ifdef USE_HWY

#define VECTOR_TYPE PV_ARCH::hwy_simd_type
#include "vspline/opt/vector_promote.h"
#include "vspline/opt/xel_of_vector.h"
#undef VECTOR_TYPE

#endif

#undef XEL_TYPE

// one more header with infrastructure code adds vspline::min,
// vspline::max and some shuffling code which is not currently used
// by lux.

#include "vspline/opt/simd_type_utils.h"
#include "vspline/opt/alternative_basis.h"

#include "dispatch.h"

namespace PV_ARCH
{
// the definition of PV_ARCH will vary for different target ISAs,
// and the concrete definition used in a specific TU is produced
// by the makefile. This puts the code for different ISAs in separate
// namespaces, cleanly separating one from the other, so they can
// coexist in an executable linking several such ISA-specific TUs.

static const dispatch local_dispatch ;

typedef vspline::bspline < pixel_type , 2 > spline_type ;
typedef vspline::bspline < pixel4_type , 2 > spline4_type ;
typedef vspline::bspline < float , 2 > alpha_spline_type ;

// pv_common.h pulls in a limited amount of vspline entities which we
// share throughout pv code, but without making direct references to
// namespace vspline. This way we keep a check on what's 'generally'
// known and avoid having to #include vspline headers into code which
// doesn't use vspline. So here we have the definitions for these
// entities, which are declared in pv_common.h:

// maximum b-spline degree

static const int max_degree = 7 ; // vspline_constants::max_degree ;

#ifdef VECTORIZE

// if pv can use vectorization, we have the pixel pipeline use vector
// data with this size:

#define VSIZE (vspline::vector_traits<float>::size)

#else

// without it, the pixel pipeline falls back to using scalar code, which is
// done by setting the vectorization width to 1. All vectorized versions of
// functions will be excluded from compilation. The resulting code turns out
// slower than the vector code, even if Vc is not used.

#define VSIZE 1

#endif

// up to lux 1.2.2 I used unsigned int, but there was a compile error
// on freeBSD when using an initializer list of two fc_t to produce a
// 2D index for an array like a [ { fc_a , fc_b } ] - the compiler
// complained about a narrowing conversion, which is technically
// correct. So now I use int instead. facet numbers above 100 should
// be rare, so int has way enough dynamic range.

typedef int fc_t ;

#ifdef VECTORIZE

// if Vc is used, we define these additional types:

typedef typename vspline::vector_traits < float , VSIZE > :: ele_v channel_v ;
typedef typename vspline::vector_traits < int32_t , VSIZE > :: ele_v int_v ;
typedef typename vspline::vector_traits < fc_t , VSIZE > :: ele_v fc_v ;
typedef typename channel_v::mask_type mask_type ;

typedef vigra::TinyVector < channel_v , 3 > pixel_v ;
typedef vigra::TinyVector < channel_v , 4 > pixel4_v ;

typedef typename vspline::vector_traits < coordinate_type , VSIZE > :: type
                 coordinate_v ;
typedef typename vspline::vector_traits < vigra::TinyVector<int,2> , VSIZE > :: type
                 int_coordinate_v ;

#endif

// short names for common transformation types

typedef vspline::unary_functor
  < float ,
    float ,
    VSIZE > tf11_type ;

typedef vspline::unary_functor
  < vigra::TinyVector < float , 2 > ,
    float ,
    VSIZE > tf21_type ;

typedef vspline::unary_functor
  < vigra::TinyVector < float , 2 > ,
    vigra::TinyVector < float , 2 > ,
    VSIZE > tf22_type ;

typedef vspline::unary_functor
  < vigra::TinyVector < float , 2 > ,
    vigra::TinyVector < float , 3 > ,
    VSIZE > tf23_type ;

typedef vspline::unary_functor
  < vigra::TinyVector < float , 2 > ,
    vigra::TinyVector < float , 4 > ,
    VSIZE > tf24_type ;

typedef vspline::unary_functor
  < vigra::TinyVector < float , 3 > ,
    float ,
    VSIZE > tf31_type ;

typedef vspline::unary_functor
  < vigra::TinyVector < float , 3 > ,
    vigra::TinyVector < float , 2 > ,
    VSIZE > tf32_type ;

typedef vspline::unary_functor
  < vigra::TinyVector < float , 3 > ,
    vigra::TinyVector < float , 3 > ,
    VSIZE > tf33_type ;

typedef vspline::unary_functor
  < vigra::TinyVector < float , 3 > ,
    vigra::TinyVector < float , 4 > ,
    VSIZE > tf34_type ;

typedef vspline::unary_functor
  < vigra::TinyVector < float , 4 > ,
    vigra::TinyVector < float , 4 > ,
    VSIZE > tf44_type ;

typedef vspline::unary_functor
  < vigra::TinyVector < float , 4 > ,
    float ,
    VSIZE > tf41_type ;

typedef vspline::grok_type < coordinate_type ,
                             pixel_type ,
                             VSIZE >
        ev_type ;

typedef vspline::grok_type < point_2d_d_type ,
                             point_3d_d_type ,
                             VSIZE >
        evd_type ;

typedef vspline::grok_type < int2_type ,
                             pixel_type ,
                             VSIZE >
        iev_type ;

typedef vspline::grok_type < coordinate_type ,
                             pixel4_type ,
                             VSIZE >
        ev4_type ;

typedef vspline::grok_type < coordinate_type ,
                             float ,
                             VSIZE >
        alpha_ev_type ;

typedef vspline::grok_type < int2_type ,
                             float ,
                             VSIZE >
        iaev_type ;

typedef vspline::grok_type < int2_type ,
                             pixel4_type ,
                             VSIZE >
        iev4_type ;

typedef vspline::grok_type < coordinate_type ,
                             coordinate_type ,
                             VSIZE >
        crd_tf_type ;

typedef vspline::domain_type < coordinate_type , VSIZE > domain_t ;

template < typename in_t , typename out_t >
struct echo_type
: public vspline::unary_functor < in_t , out_t , VSIZE >
{
  std::string caption ;
  vspline::grok_type < in_t , out_t , VSIZE > inner ;

  echo_type ( std::string _caption ,
              vspline::grok_type < in_t , out_t , VSIZE > _inner )
  : caption ( _caption ) ,
    inner ( _inner )
    { }

  template < typename i_t , typename o_t >
  void eval ( const i_t & i , o_t & o ) const
  {
    inner.eval ( i , o ) ;
    std::cout << caption << " " << i << " -> " << o << std::endl ;
  }
} ;

template < typename in_t , typename out_t >
vspline::grok_type < in_t , out_t , VSIZE >
add_echo ( std::string caption , 
           vspline::grok_type < in_t , out_t , VSIZE > inner )
{
  return vspline::grok ( echo_type < in_t , out_t > ( caption , inner ) ) ;
}

// variant of yield_t yielding single floats, used for masks

struct yield1_t
: public vspline::yield_type < int2_type , float , VSIZE >
{
  typedef vspline::yield_type < int2_type , float , VSIZE > base_t ;

  using base_t::base_t ;
} ;

struct omit_alpha_type
: public tf23_type
{
  const ev4_type inner ;

  omit_alpha_type ( const ev4_type & _inner )
  : inner ( _inner )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    auto v4 = inner ( in ) ;
    out[0] = v4[0] ;
    out[1] = v4[1] ;
    out[2] = v4[2] ;
  }
} ;

struct associate_type
: public tf44_type
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out[0] = in[0] * in[3] ;
    out[1] = in[1] * in[3] ;
    out[2] = in[2] * in[3] ;
    out[3] = in[3] ;
  }
} ;

struct deassociate_type
: public tf44_type
{
  void eval ( const in_type & in , out_type & out ) const
  {
    if ( in[3] > 0.00001f )
    {
      out[0] = in[0] / in[3] ;
      out[1] = in[1] / in[3] ;
      out[2] = in[2] / in[3] ;
      out[3] = in[3] ;
    }
    else
    {
      out[0] = 0 ;
      out[1] = 0 ;
      out[2] = 0 ;
      out[3] = 0 ;
    }
  }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out[0] = in[0] / in[3] ;
    out[1] = in[1] / in[3] ;
    out[2] = in[2] / in[3] ;
    out[3] = in[3] ;

    auto mask ( in[3] <= 0.00001f ) ;
    if ( any_of ( mask ) )
    {
      out[0] ( mask ) = 0.0f ;
      out[1] ( mask ) = 0.0f ;
      out[2] ( mask ) = 0.0f ;
      out[3] ( mask ) = 0.0f ;
    }
  }
} ;

// well_exposedness_t calculates a quality measure by applying a gauss
// curve to the difference from medium brightness - normalized to [0,1].
// This measures the 'well-exposedness' of a pixel: pixels close to middle
// grey receive a weight of 1.0, pixels close to zero or maximum
// brightness receive weights near zero.
// This quality functor is similar to the function hdr_fitness used for
// HDR blending, but it does not use a socket value and the formation of
// the exponent is different.
// This functor is meant for measuring sRGB data. Raising mu gives higher
// preference to brighter pixels, and lowering sigma produces weights
// which, for the different exposures, tend to 'focus' on the one exposure
// which is best-exposed in that pixel, rather than forming an average,
// which results from raising sigma.
// The command line parameters 'exposure_mu' and 'exposure_sigma' are
// directly routed to these two c'tor arguments.
// The calculation of the final result with exp() makes sure we get
// only positive values.

struct well_exposedness_t
: public tf31_type
{
  const float sigma ;
  const float mu ;
  const float ceiling ;

  well_exposedness_t ( const float & _sigma = .2f ,
                       const float & _mu = .5f ,
                       const float & _ceiling = 255.0f )
  : sigma ( _sigma ) ,
    mu ( _mu ) ,
    ceiling ( _ceiling )
  { }

  void eval ( const in_type & _in , out_type & out ) const
  {
    // as grey projector, use average of channels:
    // TODO: choosable grey projector?

    auto in = ( _in[0] + _in[1] + _in[2] ) / ( 3.0f * ceiling ) ;

    if ( in == 0.0f )
    {
      out = 0.0f ;
    }
    else
    {
      in -= mu ; // -> [ -.5 : .5 ]

      auto e = std::min ( 70.0f , ( in * in ) / ( 2.0f * sigma * sigma ) ) ;
      out = exp ( - e ) ;
    }
  }

  // vectorized variant. We need a little extra code to deal with a
  // bug in Vc or vspline, and zero-valued pixels are masked out instead
  // of the conditional used in the sclar form.

  template < typename in_type , typename out_type >
  void eval ( const in_type & _in , out_type & out ) const
  {
    auto in = ( _in[0] + _in[1] + _in[2] ) / ( 3.0f * ceiling ) ;
    in -= mu ;

    // bug in vspline or Vc? exponents below -80 or so result in
    // nan when passed to exp(). This avoids the bug, but it sucks:

    auto e = vspline::min ( 70.0f , ( in * in ) / ( 2.0f * sigma * sigma ) ) ;

    // without the bug we might just write:
    // auto e = ( in * in ) / ( 2.0f * sigma * sigma ) ;
      
    out = exp ( - e ) ;
    out ( in == 0.0f ) = 0.0f ;
  }
} ;

// two macros to make vspline::evaluators from the 2D splines over
// pixel data which are used a lot in pv. The 'long' form of calling
// vspline::make_evaluator is quite verbose, because in pv we use
// a fixed VSIZE, rather than what vspline::make_evaluator uses
// as it's default. Hence the macro.

#define make_ev vspline::make_evaluator<spline_type,float,VSIZE>
#define make_iev vspline::make_evaluator<spline_type,int,VSIZE>

// for pixel4_type (RGBA)

#define make_ev4 vspline::make_evaluator<spline4_type,float,VSIZE>
#define make_iev4 vspline::make_evaluator<spline4_type,int,VSIZE>

// For alpha splines - splines over 2D arrays of single floats,
// use these two macros:

#define make_aev vspline::make_evaluator<alpha_spline_type,float,VSIZE>
#define make_iaev vspline::make_evaluator<alpha_spline_type,int,VSIZE>

/// for full spherical images, we have to perform the prefiltering and
/// bracing of the b-splines 'manually', because these images don't fit
/// any of vspline's standard modes. The problem is that while the images
/// are horizontally periodic, the vertical periodicity is not manifest:
/// the vertical periodicity is along great circles passing through the
/// poles, and of these great circles, one half is in the left half and
/// one in the right half of the standard 2:1 spherical format.
/// The prefiltering for the vertical can be done with vspline, but the
/// image halves have to be put into separate views and 'stacked'.
/// This is done first. Next comes the bracing, which also needs to be
/// done manually, since the vertical continuation for one half is always
/// in the other half, going in the opposite direction. This requires
/// more index and view artistry. Coding for full sphericals has become
/// possible only with the introduction of stacked array prefiltering.
/// While the coding presents a certain degree of effort, the reward is
/// mathematically sound treatment of full spherical data, which had been
/// processed with REFLECT BCs after the demise of BC mode
/// vspline::SPHERICAL. So now the specialized code is where it should
/// reside: here in pv_rendering_common.cc, rather than in vspline,
/// where it was misplaced, because it's so specific. And with the new
/// code, we can handle even the smallest possible spherical pamorama,
/// consisting of only two pixels, correctly :D

// TODO: while readymade full sphericals do indeed normally have 2:1
// aspect ratio, the width can be halved and the code functions as
// intended. But odd width can occur, and because it's not meaningless,
// but simply unusual, it should be handled correctly, and it isn't.
// With odd width, we can't split the image into a left and a right
// half-image to stack them on top of each other. It would be necessary
// to generate a stripe on top and a stripe on the bottom which are
// interpolated to yield values just *between* the normal grid columns,
// which would approximate the continuation 'beyond the poles'. This
// could be prefiltered as a stack. To use the fully correct calculation
// which needs periodicity, one would use only one additional stripe
// with full height.

template < typename dtype >
void spherical_prefilter
  ( vspline::bspline < dtype , 2 > & bspl ,
    vigra::MultiArrayView < 2 , dtype > & input ,
    int njobs )
{
  auto output = bspl.core ;
  typedef vigra::MultiArrayView < 2 , dtype > view_type ;

  // the type of filter we want to use is a b-spline prefilter operating
  // in single-precision float, using Vc::SimdArrays with default vector
  // width for aggregation. Note how this code will use vspline's SIMD
  // emulation code if Vc is not used: this code is *not* part of the pixel
  // pipeline, which has to be reduced to VSIZE 1 if Vc is not present.

  typedef vspline::bspl_prefilter
          < vspline::simdized_type , float >
    stripe_handler_type ;

  // we set up the specifications for the filter. it is periodic in all
  // cases (the stacked half-images together are periodic vertically)

  int degree = bspl.spline_degree ; // spline degree
  int npoles = degree / 2 ;         // number of filter poles

  vspline::iir_filter_specs specs
    ( vspline::PERIODIC ,
      degree / 2 ,
      vspline_constants::precomputed_poles [ degree ] ,
      0.0001 ) ;

  // we call separable_filter's overload taking MultiArrayViews and an axis
  // to do the horizontal prefiltering step on the unmodified data

  if ( degree > 1 || input.data() != output.data() )
    vspline::detail::separable_filter
      < view_type , view_type , stripe_handler_type >()
        ( input , output , 0 , specs ) ;

  // now we stack the left half and the verticall flipped right half
  // to obtain a stack which is periodic vertically

  std::vector < view_type > vertical_view ;

  // note that for scaled-down spline there is a possibility that the
  // horizontal extent is not even, as it is for the original image.
  // we silently ignore this for now - it may result in a pixel fault.
  // KFJ 2022-01-20 this became an issue. Here's a quick fix for odd
  // width, assuming shape[0], the width, is 'large'. The correct way
  // would be to interpolate at N/2 distance.

  auto shape = output.shape() ;
  if ( shape[0] & 1 )
  {
    if ( shape[0] > 1 )
    {
      std::cerr << "warning: input is a full spherical with odd width "
                << shape[0] << std::endl ;
      std::cerr
        << "you may notice slight errors near poles and image boundaries"
        << std::endl ;
    }
    shape [ 0 ] = shape [ 0 ] / 2 + 1 ;
  }
  else
  {
    shape [ 0 ] /= 2 ;
  }

  auto stride = output.stride() ;
  stride [ 1 ] = - stride [ 1 ] ;

  view_type upper ( shape , output.stride() , output.data() ) ;

  view_type lower ( shape , stride ,
                    output.data()
                    + ( shape[1] - 1 ) * output.stride(1)
                    + shape[0] * output.stride(0) ) ;

  vertical_view.push_back ( upper ) ;
  vertical_view.push_back ( lower ) ;

  // the stack is fed to the overload taking stacks and an axis,
  // but we only need to actually apply the filter if degree is
  // greater than one, otherwise bracing is all that's required.

  if ( degree > 1 )
  {
    vspline::detail::separable_filter
      < view_type , view_type , stripe_handler_type >()
      ( vertical_view , vertical_view , 1 , specs , njobs ) ;
  }

  // now we apply the special bracing for spherical images
  // first get views to the left and right half-image

  view_type left ( shape , output.stride() , output.data() ) ;
  view_type right ( shape , output.stride() , output.data() + shape[0] ) ;

  // we initialize the indices for the lines we copy from source to target
  // and the limits for these indices to make sure we don't produce memory
  // faults by overshooting. Note how we use views to what amounts to the
  // spline's core, and write to lines outside these views, since we can be
  // sure that this is safe: these writes go to the spline's frame, and we
  // can obtain the frame's size from the bspline object.

  std::ptrdiff_t upper_margin = bspl.left_frame[1] ;
  std::ptrdiff_t y0 = - upper_margin ;
  std::ptrdiff_t lower_margin = bspl.right_frame[1] ;
  std::ptrdiff_t y1 = shape[1] + lower_margin ;

  std::ptrdiff_t upper_source = 0 ;
  std::ptrdiff_t upper_target = -1 ;

  std::ptrdiff_t lower_source = shape[1] - 1 ;
  std::ptrdiff_t lower_target = shape[1] ;

  while ( true )
  {
    // check all indices
    bool check1 = upper_source >= y0 && upper_source < y1 ;
    bool check2 = upper_target >= y0 && upper_target < y1 ;
    bool check3 = lower_source >= y0 && lower_source < y1 ;
    bool check4 = lower_target >= y0 && lower_target < y1 ;

    if ( ( ! check2 ) && ( ! check4 ) )
    {
      // both target indices are out of range, break, we're done
      break ;
    }

    if ( check2 )
    {
      // upper_target is in range
      if ( check1 )
      {
        // so is upper_source, do the copy
        left.bindAt ( 1 , upper_target ) = right.bindAt ( 1 , upper_source ) ;
        right.bindAt ( 1 , upper_target ) = left.bindAt ( 1 , upper_source ) ;
        upper_source ++ ;
      }
      upper_target -- ;
    }

    if ( check4 )
    {
      // lower_target is in range
      if ( check3 )
      {
        // so is lower_source, do the copy
        left.bindAt ( 1 , lower_target ) = right.bindAt ( 1 , lower_source ) ;
        right.bindAt ( 1 , lower_target ) = left.bindAt ( 1 , lower_source ) ;
        lower_source -- ;
      }
      lower_target ++ ;
    }
  }

  // vertical bracing is done, apply horizontal bracing

  bspl.brace ( 0 ) ;
  bspl.prefiltered = true ;
}

// truncated_sinc and hamming_window are helper classes to produce
// a half band filter, see class halfband further down.

template < typename dtype >
struct truncated_sinc
{
  const int N ;

  truncated_sinc ( const int & _N )
  : N ( _N )
  { } ;

  dtype operator() ( const int & n ) const
  {
    if ( n == 0 )
      return dtype ( 1 ) / dtype ( 2 ) ;
    else if ( n >= ( - N / 2 ) && n <= ( N / 2 ) )
      return ( sin ( n * M_PI_2 ) / ( n * M_PI ) ) ;
    else
      return 0 ;
  }
} ;

template < typename dtype >
struct hamming_window
{
  const int N ;

  hamming_window ( const int & _N )
  : N ( _N )
  { } ;

  dtype operator() ( const int & n ) const
  {
    if ( n >= ( - N / 2 ) && n <= ( N / 2 ) )
      return ( 0.54 + 0.46 * cos ( ( 2 * n * M_PI  ) / N ) ) ;
    else
      return 0 ;
  }
} ;

// struct halfband provides the coefficients of a half band filter,
// designed with the method given in this article:
// https://www.dsprelated.com/showarticle/1113.php

template < typename dtype = double >
struct halfband
{
  const truncated_sinc < dtype > s ;
  const hamming_window < dtype > w ;

  halfband ( const int & N )
  : s ( N ) , w ( N )
  { } ;

  dtype operator() ( const int & n ) const
  {
    int nn = n - s.N / 2 ;
    return s ( nn ) * w ( nn ) ;
  }
} ;

// all negative values are raised to zero

template < typename px_t = pixel4_type >
struct no_negative_t
: public vspline::unary_functor < px_t , px_t , VSIZE >
{
  typedef vspline::unary_functor < px_t , px_t , VSIZE > base_t ;

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    for ( int i = 0 ; i < base_t::dim_in ; i++ )
    {
      out[i] = vspline::max ( 0.0f , in[i] ) ;
    }
  }
} ;

typedef no_negative_t < pixel4_type > floor4_t ;

template<>
struct no_negative_t < float >
: public vspline::unary_functor < float , float , VSIZE >
{
  typedef vspline::unary_functor < float , float , VSIZE > base_t ;

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out = vspline::max ( 0.0f , in ) ;
  }
} ;

// make_decimator sets up an evaluator for a b-spline which can serve
// as a decimator in pyramid creation. This used to be a member
// function of class pyramid_type, but I found it was useful beyond
// the class - class raw_image_type uses it to set up the functor
// which it passes to the c'tor of the 'primal pyramid' to populate
// this pyramid's level 1.
// Template arguments are the type of the b-spline and the data type
// the evaluator will yield. The first template argument allows a
// variety of splines (also splines with integral coefficients, like
// the splines over raw image data), and the code is also used for
// single-float splines, used for alpha channels.

template < typename spline_type ,
           typename dtype = pixel_type >
vspline::grok_type < coordinate_type , dtype , VSIZE >
make_decimator ( spline_type * p_bspl ,
                 int smoothing_level ,
                 double scaling_step )
{
  typedef typename spline_type::value_type raw_pixel_type ;

  if ( smoothing_level >= 0 )
  {
    // to evaluate the spline with a reconstruction filter different than
    // the filter which would be appropriate to it's 'native' degree we
    // use 'shifting':

    int shift = smoothing_level - p_bspl->spline_degree ;

    if ( ! p_bspl->shiftable ( shift ) )
      throw vspline::not_supported
      ( "insufficient frame size. shift can not be performed." ) ;

    return vspline::make_evaluator < spline_type ,
                                     float ,
                                     VSIZE ,
                                     float ,
                                     dtype >
        ( *p_bspl , { 0 , 0 } , shift ) ;
  }
  else if ( smoothing_level == -1 )
  {
    // smoothing level -1 indicates 'area decimation', which
    // gives good results for scaling down by up to scaling step 2,
    // which halves the image's size. Area filtering can be done with
    // larger 'kernel sizes' as well, but the implementation used here
    // is limited to a maximum of two and very efficient.

    if ( scaling_step < 1.0 || scaling_step > 2.0 )
      throw vspline::not_supported
      ( "this decimator can only handle decimation factors [1-2]" ) ;

    typedef vspline::area_basis_functor < float > abf_type ;
    vspline::multi_bf_type < abf_type , 2 >
      abf ( 2 , scaling_step ) ;

    typedef vspline::abf_evaluator < coordinate_type ,
                                     dtype ,
                                     decltype ( abf ) ,
                                     VSIZE ,
                                     float ,
                                     raw_pixel_type > aev_type ;

    int shift = 2 - p_bspl->spline_degree ;

    return vspline::grok ( aev_type ( *(p_bspl) , abf , shift ) ) ;
  }
  else
  {
    // the remainder of decimators use 'convolving basis functors'
    // (CBFs in short). These are hybrids of b-spline evaluators
    // and convolution operators, and have the same effect as if
    // the raw data had been filtered with the convolution kernel
    // before the spline was built. The resulting operators have
    // potentially large support (b-spline degree + kernel size,
    // squared) and are therefore potentially slow to compute,
    // but decimators are only used to set up pyramids, so they
    // aren't used repeatedly in rendering and will only make
    // the set-up longer.

    // we have a few 'stock' kernels:

    const std::vector < float > binomial_1
      { .25 ,
        .5 ,
        .25 } ;

    const std::vector < float > binomial_3
      { 1.0 / 16.0 ,
        4.0 / 16.0 ,
        6.0 / 16.0 ,
        4.0 / 16.0 ,
        1.0 / 16.0
      } ;

    // 'optimal' Burt filter, the value of 'a' is taken from vigra,
    // see the function vigra::initBurtFilter, online docu
    // at https://ukoethe.github.io/vigra/doc-release/vigra/classvigra_1_1Kernel1D.html#a1406a301a1cc659b3098bbcc0a827228

    const float a = 0.04785 ;
    const std::vector < float > burt
      { a ,
        .25 ,
        0.5f - 2.0f * a ,
        .25 ,
        a
      } ;

    // optionally we can use a half-band filter.

    std::vector < float > hbk ;
    
    if ( smoothing_level < -4 )
    {
      int ksz = - smoothing_level ;
      assert ( ksz == ( ( ( ksz + 1 ) >> 2 ) << 2 ) - 1 ) ;

      halfband < double > hb ( ksz ) ;

      for ( int i = 0 ; i < ksz ; i++ )
        hbk.push_back ( hb ( i ) ) ;
    }

    std::vector < float > smoothing_kernel ;

    switch ( smoothing_level )
    {
      case -2:
        smoothing_kernel = binomial_1 ;
        break ;
      case -3:
        smoothing_kernel = binomial_3 ;
        break ;
      case -4:
        smoothing_kernel = burt ;
        break ;
      default:
        smoothing_kernel = hbk ;
        break ;
    }

    int ksz = smoothing_kernel.size() ;

    // the basis function we want to use is a 'convolving_basis_functor':

    typedef convolving_basis_functor < float > cbf_type ;

    // for 2D work, we set up a 2D multi_bf_type with this basis functor

    vspline::multi_bf_type < cbf_type , 2 >
      mcbfk ( p_bspl->spline_degree , { 0 , 0 } , smoothing_kernel ) ;

    // given that, we can set up an evaluator using this basis functor;
    // the appropriate type is this one:

    typedef vspline::ev_evaluator < coordinate_type ,
                                    dtype ,
                                    decltype ( mcbfk ) ,
                                    VSIZE ,
                                    float ,
                                    raw_pixel_type > ckev_type ;

    // finally we produce the evaluator and 'grok' it to pass it back.
    // note how we have to pass a shift value to accommodate the larger
    // footprint of the basis functor:
  
    int shift = ksz - 1 ;

    return vspline::grok ( ckev_type ( *(p_bspl) , mcbfk , shift ) ) ;
  }
}

/// struct pyramid_type contains the image pyramids we use for
/// scale variation. Here we have a set of successively smaller images,
/// scaled down with a low-pass filter to avoid aliasing. The default
/// uses an 'area filter' for downscaling, with the image extents
/// halved at each step. The images are coded as vspline::bspline objects.
/// The pyramid's c'tor sets up a 'spline pyramid', where each level is
/// a b-spline, rather than merely an array of pixels. Most of the code
/// using pyramids will require 'true' image pyramids, which will usually
/// be encoded as degree-1 splines. For such use, the splines in the
/// pyramid may need to be 'restored' from higher-degree b-splines to
/// degree-1; use the member function 'restore'.
/// pyramid_type is also used for exposure fusion, so it's factored out
/// into this header.

// TODO: pass in a parameter for additional headroom, or glean the value
// from looking at the bottom-level spline. If the pyramid levels are
// meant to be shifted up later on, the headroom calculated by just
// looking at the spline degree and the decimator is not enough.

template < typename spline_t >
int actual_headroom ( spline_t * p_spline )
{
  auto & degree ( p_spline->spline_degree ) ;
  auto & bcv ( p_spline->bcv ) ;

  auto left_brace = p_spline->get_left_brace_size ( degree , bcv ) ;
  auto right_brace = p_spline->get_right_brace_size ( degree , bcv ) ;

  auto left_headroom = p_spline->left_frame - left_brace ;
  auto right_headroom = p_spline->right_frame - right_brace ;

  auto l_max = std::max ( left_headroom[0] , left_headroom[1] ) ;
  auto r_max = std::max ( right_headroom[0] , right_headroom[1] ) ;

  return std::max ( l_max , r_max ) ;
}

template < typename dtype = pixel_type >
struct pyramid_type
{
  typedef vspline::bspline < dtype , 2 > spline_type ;
  typedef vspline::grok_type < coordinate_type , dtype , VSIZE > ev_type ;

  double scaling_step ;
  double smoothing_level ;
  const bool full_spherical ;
  std::vector < spline_type * > level ;
  std::vector < double > decimator_kernel ;

  // construct an image pyramid with a given level-0 spline. This
  // constructor is a bit convoluted, because it delegates to
  // a second constructor which starts filling pyramid levels
  // starting with level 1, leaving level 0 empty, see below.

  pyramid_type ( spline_type * p_bspl ,
                 double _scaling_step = 2.0 ,
                 int _smoothing_level = 7 ,
                 int floor = 1 ,
                 bool _full_spherical = false ,
                 int njobs = 4 )
  : pyramid_type ( p_bspl->core.shape ( 0 ) ,
                   p_bspl->core.shape ( 1 ) ,
                   p_bspl->spline_degree ,
                   make_decimator < spline_type , dtype >
                     ( p_bspl ,
                       _smoothing_level ,
                       _scaling_step ) ,
                   p_bspl->bcv ,
                   _scaling_step ,
                   _smoothing_level ,
                   floor ,
                   _full_spherical ,
                   njobs ,
                   actual_headroom ( p_bspl )
                 )
  {
    // we now have the image pyramid with levels 1..N filled, and
    // all that's left to do is to put the level-0 spline in:

    level[0] = p_bspl ;
  }
   
  // create a pyramid from an evaluator yielding values *for level 1*
  // of the image pyramid. Level 0 is left empty (it will contain
  // nullptr). This looks like a strange idea - the rationale is this:
  // level 0 may not be representable by a spline of the pyramid's
  // type (this is always 'spline_type'). But it is generally possible to
  // provide a functor which will decimate the level-0 data to level-1.
  // Given that, the levels 1..N can be filled. Now the calling code
  // may or may not decide to fill in level 0 - it might, for example,
  // discard level 0 altogether when 'squashing' is done. If the calling
  // code fills in a regular spline_type object at level 0, we get a
  // 'complete' pyramid. Another use for the 'incomplete' pyramid with
  // level 0 set to nullptr is to avoid creating level 0 in favour
  // of a functor 'standing in' for level 0. This is desirable if
  // the source is very large, and the (float-valued) level 0 would
  // bee too large to handle or take long to set up. The code providing
  // a suitable evaluator for rendering can then detect that level 0
  // is empty and use the stand-in functor for renderings using level 0.
  // So the evaluator 'ev' which we receive here has to be a 'decimator',
  // producing values appropriate for the resolution of the level-1
  // spline in this pyramid.

  pyramid_type ( int l0_width ,
                 int l0_height ,
                 int spline_degree ,
                 ev_type ev ,
                 vspline::bcv_type<2> bcv ,
                 double _scaling_step = 2.0 ,
                 int _smoothing_level = 7 ,
                 int floor = 1 ,
                 bool _full_spherical = false ,
                 int njobs = 4 ,
                 int _headroom = 0
               )
  : scaling_step ( _scaling_step ) ,
    smoothing_level ( _smoothing_level ) ,
    full_spherical ( _full_spherical )
  {
    assert ( floor > 0 ) ;

    int headroom = ( smoothing_level + 1 ) / 2 ;

    if ( smoothing_level == -1 )
      headroom = 1 ;
    else if ( smoothing_level == -2 )
      headroom = 1 ;
    else if ( smoothing_level == -3 )
      headroom = 2 ;
    else if ( smoothing_level == -4 )
      headroom = 2 ;
    else if ( smoothing_level <= -5 )
    {
      int ksz = - smoothing_level ;
      headroom = ( ksz - 1 ) / 2 ;
    }

    headroom = std::max ( _headroom , headroom ) ;

    if ( scaling_step <= 1.0 )
      return ;

    level.push_back ( nullptr ) ;     // we leave level 0 empty
    double scaling_cumulated = 1.0 ;

    auto current_shape = vigra::Shape2 ( l0_width , l0_height ) ;

    while ( true )
    {
      // first we determine the extent of the new level we'll create

      scaling_cumulated /= scaling_step ;

      // with and height are simply rounded to the nearest value.
      // note that slight anisotropy is not an issue, we always have
      // a vspline::domain chained in during evaluation.

      int old_width = current_shape [ 0 ] ;
      int old_height = current_shape [ 1 ] ;
      
      int new_width = std::round ( l0_width * scaling_cumulated ) ;
      int new_height = std::round ( l0_height * scaling_cumulated ) ;

      if ( full_spherical )
      {
        if ( ( new_width & 1 ) && ( new_width > 2 ) )
          ++new_width ;
      }

      // we want to make sure though that the new level is indeed
      // smaller than the previous one, by at least one unit. Only
      // if this would land us with a size of less than 'floor',
      // we stay at 'floor'.

      if ( new_width >= old_width )
        new_width = old_width - 1 ;

      if ( new_height >= old_height )
        new_height = old_height - 1 ;

      if ( new_width < floor )
        new_width = floor ;

      if ( new_height < floor )
        new_height = floor ;

      // we create a bspline object with the new level's metrics.
      // Note the headroom - this will be needed if the decimator needs
      // larger support, like a shifted-up b-spline evaluator. If the
      // decimator is an area filer, the headroom can be smaller.

      spline_type * new_level = mem_in()
         << new spline_type ( vigra::Shape2 ( new_width , new_height ) ,
                              spline_degree ,
                              bcv ,
                              -1.0 ,
                              headroom ) ;

      // now we want to populate the new level with values obtained by
      // evaluating the current decimator (ev) at locations corresponding
      // to cardinal locations in the new level. We'll use a vspline::domain
      // and an index-based transform to obtain the grid positions:
      
      vigra::MultiArray < 1 , float > xv ( new_width ) ;
      vigra::MultiArray < 1 , float > yv ( new_height ) ;

      // the domains map the new level's limits to the previous level's
      // limits. With this range mapping we ensure that the resulting new
      // level behaves exactly as it's parent level, provided that a
      // suitable domain is used during evaluation.
      // We set up two domains, one for each axis

      vspline::domain_type<float,VSIZE>
        d0 ( new_level->lower_limit(0) ,
             new_level->upper_limit(0) ,
             spline_type::lower_limit ( bcv[0] ) ,
             spline_type::upper_limit ( current_shape[0] , bcv[0] ) ) ;

      vspline::domain_type<float,VSIZE>
        d1 ( new_level->lower_limit(1) ,
             new_level->upper_limit(1) ,
             spline_type::lower_limit ( bcv[1] ) ,
             spline_type::upper_limit ( current_shape[1] , bcv[1] ) ) ;

      // now the index-based transform. It will pass discrete coordinates
      // into the domain, and receive corresponding coordinates referring
      // to the previous level

      vspline::transform ( d0 , xv ) ;
      vspline::transform ( d1 , yv ) ;

      // we combine the two index sets into a grid_spec object

      vspline::grid_spec < 2 , float > grid { xv , yv } ;

      // now we can run a grid-based transform to populate the new level
      // with data, using ev to evaluate at every grid position specified
      // by 'grid'

      vspline::transform ( grid , ev , new_level->core , njobs ) ;

      // prefilter. We optionally use spherical_prefilter
      // to apply the special bracing needed for full sphericals

      if ( full_spherical )
        spherical_prefilter ( *new_level , new_level->core , njobs ) ;
      else
        new_level->prefilter ( 1 , njobs ) ;

      level.push_back ( new_level ) ; // save the level

      // if both of the new level's extents have reached 'floor'
      // we're done (new, previously the termination criterion
      // was that either of the two dimensions has reached 'floor'

      if ( ( new_height <= floor ) && ( new_width <= floor ) )
      {
        break ;
      }

      // we haven't reached the floor yet. update current_shape
      // and ev for the next level. We use 'make_decimator' to
      // create the evaluator to populate the next level.

      current_shape = vigra::Shape2 ( new_width , new_height ) ;

      ev = make_decimator < spline_type , dtype >
            ( new_level , smoothing_level , scaling_step ) ;
    }
  }

  // c'tor creating an 'empty' pyramid with all the levels already
  // present, but not populated with data.

  pyramid_type ( int l0_width ,
                 int l0_height ,
                 int spline_degree ,
                 vspline::bcv_type<2> bcv ,
                 double _scaling_step = 2.0 ,
                 int _smoothing_level = 7 ,
                 int floor = 1 ,
                 bool _full_spherical = false ,
                 int njobs = 4 ,
                 int _headroom = 0 )
  : scaling_step ( _scaling_step ) ,
    smoothing_level ( _smoothing_level ) ,
    full_spherical ( _full_spherical )
  {
    assert ( floor > 0 ) ;

    int headroom = ( smoothing_level + 1 ) / 2 ;

    if ( smoothing_level == -1 )
      headroom = 1 ;
    else if ( smoothing_level == -2 )
      headroom = 1 ;
    else if ( smoothing_level == -3 )
      headroom = 2 ;
    else if ( smoothing_level == -4 )
      headroom = 2 ;
    else if ( smoothing_level <= -5 )
    {
      int ksz = - smoothing_level ;
      headroom = ( ksz - 1 ) / 2 ;
    }

    headroom = std::max ( _headroom , headroom ) ;

    if ( scaling_step <= 1.0 )
      return ;

    auto current_shape = vigra::Shape2 ( l0_width , l0_height ) ;

    spline_type * new_level = mem_in()
       << new spline_type ( current_shape ,
                            spline_degree ,
                            bcv ,
                            -1.0 ,
                            headroom ) ;

    level.push_back ( new_level ) ;

    double scaling_cumulated = 1.0 ;

    while ( true )
    {
      // first we determine the extent of the new level we'll create

      scaling_cumulated /= scaling_step ;

      // with and height are simply rounded to the nearest value.
      // note that slight anisotropy is not an issue, we always have
      // a vspline::domain chained in during evaluation.

      int old_width = current_shape [ 0 ] ;
      int old_height = current_shape [ 1 ] ;
      
      int new_width = std::round ( l0_width * scaling_cumulated ) ;
      int new_height = std::round ( l0_height * scaling_cumulated ) ;

      // we want to make sure though that the new level is indeed
      // smaller than the previous one, by at least one unit. Only
      // if this would land us with a size of less than 'floor',
      // we stay at 'floor'.

      if ( new_width >= old_width )
        new_width = old_width - 1 ;

      if ( new_height >= old_height )
        new_height = old_height - 1 ;

      if ( new_width < floor )
        new_width = floor ;

      if ( new_height < floor )
        new_height = floor ;

      // we create a bspline object with the new level's metrics.
      // Note the headroom - this will be needed if the decimator needs
      // larger support, like a shifted-up b-spline evaluator. If the
      // decimator is an area filer, the headroom can be smaller.

      spline_type * new_level = mem_in()
         << new spline_type ( vigra::Shape2 ( new_width , new_height ) ,
                              spline_degree ,
                              bcv ,
                              -1.0 ,
                              headroom ) ;

      level.push_back ( new_level ) ; // save the level

      // if both of the new level's extents have reached 'floor'
      // we're done (new, previously the termination criterion
      // was that either of the two dimensions has reached 'floor'

      if ( ( new_height <= floor ) && ( new_width <= floor ) )
      {
        break ;
      }

      // we haven't reached the floor yet. update current_shape
      // and ev for the next level.

      current_shape = vigra::Shape2 ( new_width , new_height ) ;
    }
  }

  // pop removes the level-0 spline from the pyramid. This reduces the
  // maximal resolution which can be provided by the pyramid, but it also
  // saves memory. level zero may be 'empty' (nullptr).

  void pop()
  {
    auto p_bspl = level[0] ;
    auto it = level.begin() ;
    level.erase ( it ) ;
    if ( p_bspl )
      memlog >> p_bspl ;
  }

  // if the splines in this pyramid have degree greater than one,
  // we use vspline::restore to 'undo' the prefiler and obtain
  // plain image data. The degree of the spline is set to 'to_degree',
  // which is one, by default: the highest degree which needs no
  // prefilter. Finally, the spline is braced.

  void restore ( int to_degree = 1 )
  {
    assert ( to_degree >= 0 && to_degree <= 1 ) ;

    for ( auto & p_bspl : level )
    {
      if ( p_bspl == nullptr )
      {
        // ignore 'empty' pyramid levels

        continue ;
      }
      else if ( p_bspl->spline_degree > 1 )
      {
        // the current level has at least degree 2, so we need to
        // use vspline::restore to 'undo' the prefilter, and we
        // need to call 'brace' to set the coefficients outside
        // the spline's core to correct values

        vspline::restore ( *p_bspl , p_bspl->core ) ;
        p_bspl->brace() ;
      }
      // finally, we take over the new degree

      p_bspl->spline_degree = to_degree ;
    }
  }

  void restore ( int to_degree ,
                 const vspline::grok_type < dtype , dtype , VSIZE > & fn ,
                 int njobs = vspline::default_njobs )
  {
    assert ( to_degree >= 0 && to_degree <= 1 ) ;

    for ( auto & p_bspl : level )
    {
      if ( p_bspl == nullptr )
      {
        // ignore 'empty' pyramid levels

        continue ;
      }
      else
      {
        // the current level has at least degree 2, so we need to
        // use vspline::restore to 'undo' the prefilter, and we
        // need to call 'brace' to set the coefficients outside
        // the spline's core to correct values

        if ( p_bspl->spline_degree > 1 )
        {
          vspline::restore ( *p_bspl , p_bspl->core ) ;
          p_bspl->brace() ;
        }

        // now we apply the 'piggyback' function

        auto sz = p_bspl->container.size() ;
        auto nc = sz / 4096 ;
        if ( nc == 0 )
          nc = 1 ;
        if ( nc > njobs )
          nc = njobs ;

        vspline::apply ( fn , p_bspl->container , nc ) ;
      }

      // finally, we take over the new degree

      p_bspl->spline_degree = to_degree ;
    }
  }

  // diagnostic code, echoing all splines in the pyramid to 'osr'.
  // This merely echos the metadata, not the coefficients.

  friend std::ostream & operator<< ( std::ostream & osr ,
                                     const pyramid_type & py )
  {
    for ( int l = 0 ; l < py.level.size() ; l++ )
    {
      spline_type * p_bspl = py.level[l] ;

      osr << "pyramid level " << l << " p_bspl = " << p_bspl << std::endl ;
      if ( p_bspl != nullptr )
        osr << *p_bspl << std::endl ;
    }
    osr << "*************************" << std::endl ;
    return osr ;
  }

  /// save all images in the pyramid to image files. This will store the
  /// spline coefficients, which may or may not be unmodified image data,
  /// depending on the splines' degree. This routine is used to check the
  /// pyramid, it may be taken out later.

  void dump ( const char * prefix )
  {
    for ( int l = 0 ; l < level.size() ; l++ )
    {
      spline_type * p_bspl = level[l] ;

      char next_name [ strlen ( prefix ) + 8 ] ;
      sprintf ( next_name , "%s%03d.tif" , prefix , l ) ;

      vigra::ImageExportInfo imageInfo ( next_name );
      std::cout << "exporting pyramid level to " << next_name << std::endl ;

      vigra::exportImage ( p_bspl->core ,
                           imageInfo
                           .setPixelType("UINT8")
                           .setCompression("100")
                           .setForcedRangeMapping ( 0 , 255 , 0 , 255 ) ) ;

    }
  }

  ~pyramid_type()
  {
    for ( std::size_t sz = 0 ; sz < level.size() ; sz++ )
    {
      if ( level[sz] != nullptr )
        memlog >> level[sz] ;
    }
  }
} ;

typedef pyramid_type < pixel_type > pixel_pyramid_type ;
typedef pyramid_type < pixel4_type > pixel4_pyramid_type ;
typedef pyramid_type < float > float_pyramid_type ;

// 'attach' creates a holding_type object which holds a support object
// alive (here, typically a raw_image_type object) and, otherwise, works
// just like the second argument, typically an evaluator. The holding_type
// object is subsequently grokked and returned, and the result behaves
// just like any other ev_type, but when it goes out of scope, the
// shared_ptr to the support object goes out of scope and is destructed.
// inner_type must be an object providing suitable eval functions to
// be grokked correctly

template < typename inner_type , typename attached_type >
decltype ( vspline::grok ( inner_type() ) )
attach ( const inner_type & ev ,
         std::shared_ptr < attached_type > support )
{
  return vspline::grok ( holding_type < inner_type > ( support , ev ) ) ;
}
                 
template < typename inner_type , typename attached_type >
decltype ( vspline::grok ( inner_type() ) )
attach ( const inner_type & ev ,
         std::vector < std::shared_ptr < attached_type > > support )
{
  return vspline::grok ( holding_type < inner_type > ( support , ev ) ) ;
}

} ; // namespace PV_ARCH
