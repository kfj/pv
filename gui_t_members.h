/************************************************************************/
/*                                                                      */
/*          pv - a viewer for panoramic images                          */
/*                                                                      */
/*          Copyright 2016 - 2023 by Kay F. Jahnke                      */
/*                                                                      */
/*  The git repository for this software is at                          */
/*                                                                      */
/*  https://bitbucket.org/kfj/pv                                        */
/*                                                                      */
/*  Please direct questions, bug reports, and contributions to          */
/*                                                                      */
/*  kfjahnke+pv@gmail.com                                               */
/*                                                                      */
/*  pv is free software: you can redistribute it and/or modify          */
/*  it under the terms of the GNU General Public License as published   */
/*  by the Free Software Foundation, either version 3 of the License,   */
/*  or (at your option) any later version.                              */
/*                                                                      */
/*  pv is distributed in the hope that it will be useful,               */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       */
/*  GNU General Public License for more details.                        */
/*                                                                      */
/*  You should have received a copy of the GNU General Public License   */
/*  along with pv.  If not, see <http://www.gnu.org/licenses/>.         */
/*                                                                      */
/************************************************************************/

/*! \file gui_base_t_members.h
 * 
 *  \brief common members for gui_type variants.
 * 
 *  rather than finessing with complicated inheritance, we simply
 *  use a common set of members which we 'implant' in the gui_type
 *  variants by #including this header. The variables and member
 *  functions given here are common to all gui_t variants - both
 *  'immediate' ones and ones working via 'override arguments'.
 */

// the 'current' member is the value which is current in the GUI.
// This is initailized from the origin, and it's only changed
// when users 'commit' a gui element with an 'apply' button.

T current ;

// The 'proposed' member is set to the value in 'current' before
// the widget is presented, showing that value. If the user interacts
// with the widget, the modified value in 'proposed' is checked for
// validity and if it's valid, 'current' is set to the new value.

T proposed ;

// the touched flag is set whenever the user modifies the
// current value. It's set in 'accept', after verification.

bool touched ;

// To construct override arguments, the parameter's long name
// has to be known, because override arguments are given in text
// form, just as they would appear on the command line. For
// 'immediate' GUI elements, the name is optional.

const std::string name ;

// The info string is what's displayed as tool tip when the user
// hovers the mouse over the little question mark next to the
// GUI element. If it's passed empty, the question mark is not
// displayed.

const std::string info ;

// The verify function makes sure that a value is acceptable.
// It's called after user interaction with a GUI element. If it
// returns false, the 'current' value remains unchanged.

std::function < bool(const T &) > verify ;

// this is an abstraction of equality which also works for strings
// and similar stuff which may have the same content but reside
// in different objects.

virtual bool is_same ( const T & a , const T & b )
{
  return a == b ; // works for values, not for C strings
}

// an abstraction of assignment, with the same rationale.

virtual void assign ( const T & src , T & trg )
{
  trg = src ; // works for values, not for C strings
}

// 'present' displays the appropriate GUI widget, which will modify
// 'proposed' if the user inputs something. This is a pure virtual
// function, because it is different for every type of argument.

virtual bool present() = 0 ;

virtual void propose()
{
  assign ( current , proposed ) ;
}

virtual void accept()
{
  assign ( proposed , current ) ;
  touched = true ;
}
