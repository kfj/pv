/************************************************************************/
/*                                                                      */
/*          pv - a viewer for panoramic images                          */
/*                                                                      */
/*          Copyright 2016 - 2023 by Kay F. Jahnke                      */
/*                                                                      */
/*  The git repository for this software is at                          */
/*                                                                      */
/*  https://bitbucket.org/kfj/pv                                        */
/*                                                                      */
/*  Please direct questions, bug reports, and contributions to          */
/*                                                                      */
/*  kfjahnke+pv@gmail.com                                               */
/*                                                                      */
/*  pv is free software: you can redistribute it and/or modify          */
/*  it under the terms of the GNU General Public License as published   */
/*  by the Free Software Foundation, either version 3 of the License,   */
/*  or (at your option) any later version.                              */
/*                                                                      */
/*  pv is distributed in the hope that it will be useful,               */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       */
/*  GNU General Public License for more details.                        */
/*                                                                      */
/*  You should have received a copy of the GNU General Public License   */
/*  along with pv.  If not, see <http://www.gnu.org/licenses/>.         */
/*                                                                      */
/************************************************************************/

/*! \file pv_rendering.cc

    \brief pv - a panorama viewer

    For documentation, please see the bitbucket repository at

    https://bitbucket.org/kfj/pv

    This file has the code specific to rendering image frames to be
    displayed by the viewer, and also code for creating the interpolators
    needed to produce these frames. This is the bulk of pv's code, and it
    is crucial that it is as fast as possible. It should be compiled
    with options making it ISA-specific (like -mavx2)
    to make proper use of the target machine's vector units. This part
    of pv also needs most compilation time, and having it in a separate
    TU speeds up the development cycle, because by now it's quite stable
    and doesn't need frequent recompiles, while the GUI code is under
    development.

    Most of the code is in what looks like a namespace PV_ARCH; in fact
    PV_ARCH is #defined at compile-time to be some identifier signifying
    use of a specific ISA - like PV_AVX. pv_rendering.cc is compiled
    several times: once for each supported ISA. The resulting ISA-specific
    object files are linked with pv_no_rendering.o, and this can only
    work if each ISA-specific TU has the code in it's separate namespace,
    because otherwise the linker might find identical mangled identifiers
    and pick the first variant it sees, instead of keeping stuff separate.

    The code in this file makes use of vspline, my b-spline library,
    which is available from

    https://bitbucket.org/kfj/bspline

    But the pv git repo contains the relevant parts of vspline as
    a subtree to avoid version conflicts, and to make it unnecessary
    to clone vspline's repo to build pv.
*/

#include <map>
#include "pv_rendering_common.h"
#include "vspline/general_filter.h"

namespace ui
{
  extern int subimage ;
} ;

#include "fileio.h"

extern void set_extent ( source_type & source ) ;
extern double apply_rise ( source_type & source , double rise ) ;

// the remainder of the code in this file is in an ISA-specific namespace:

namespace PV_ARCH
{
// each rendering code namespace contains a 'flavour' variable
// which the main program processes and uses to, e.g., dispatch
// to specific rendering code. PV_ARCH, PV_ARCHNAME etc. are set
// by the build system (see CMakeLists.txt)
// PV_ARCH::_dispatch is defined in dispatch.h, included by
// pv_rendering_common.h (see above).

flavour_type flavour { PV_ARCHNAME ,
                       PV_PRIORITY ,
                       PV_VIABILITY ,
                       (dispatch*) (&PV_ARCH::_dispatch ) } ;
  
void at_least ( const float & minimum , float & target )
{
  if ( target < minimum )
    target = minimum ;
}

void at_least ( const double & minimum , double & target )
{
  if ( target < minimum )
    target = minimum ;
}

template < typename T >
void at_least ( const float & minimum ,
                T & target )
{
  target = vspline::max ( minimum , target ) ;
}

template < typename T >
void at_least ( const double & minimum ,
                T & target )
{
  target = vspline::max ( minimum , target ) ;
}

typedef vspline::vector_traits < point_3d_f_type , VSIZE > :: type p3v_t ;
typedef vspline::vector_traits < coordinate_type , VSIZE > :: type p2v_t ;
typedef vspline::vector_traits < int , VSIZE > :: type iv_t ;
typedef vspline::vector_traits < float , VSIZE > :: type fv_t ;
typedef vigra::Quaternion < fv_t > qv_t ;

namespace detail
{
// I want to find out if two arcs (on the same sphere) intersect.
// I narrow it to smaller arcs of the great circles.
// The idea is this: I create two planes, p1 through the origin
// and the ends a0, a1 of the first arc, p2 through the origin
// and the ends b0, b1 of the other arc. I assume that the arcs
// intersect, if a0 and a1 lie on opposite sides of p2 and
// b0 and b1 lie on opposite sides of p1.

typedef vigra::TinyVector<double,3> point_3d_d_type ;

point_3d_d_type normal ( const point_3d_d_type & p1 ,
                         const point_3d_d_type & p2 ,
                         const point_3d_d_type & p3 )
{
  return cross ( p2 - p1 , p3 - p1 ) ;
}

// note: the functions 'above' and 'below' rely on the plane passing
// through the origin - as is the case for the planes bounding a
// frustum. For the general case, the second argument 'point' has to
// be passed a 3D vector from some point on the plane to the point,
// so if we have some point P on the plane, we need to pass point - P.

bool above ( const point_3d_d_type & normal ,
             const point_3d_d_type & point )
{
  return ( dot ( normal , point ) >= 0 ) ;
}

bool below ( const point_3d_d_type & normal ,
             const point_3d_d_type & point )
{
  return ( dot ( normal , point ) < 0 ) ;
}

struct frustum_type
{
  point_3d_d_type n0 ;
  point_3d_d_type n1 ;
  point_3d_d_type n2 ;
  point_3d_d_type n3 ;

  frustum_type ( const point_3d_d_type & p0 ,
                 const point_3d_d_type & p1 ,
                 const point_3d_d_type & p2 ,
                 const point_3d_d_type & p3 )
  {
    point_3d_d_type origin ( 0.0 , 0.0 , 0.0 ) ;

    n0 = normal ( origin , p0 , p1 ) ;
    n1 = normal ( origin , p1 , p2 ) ;
    n2 = normal ( origin , p2 , p3 ) ;
    n3 = normal ( origin , p3 , p0 ) ;
  }

  bool inside ( const point_3d_d_type & p )
  {
    bool b1 = above ( n0 , p ) ;
    bool b2 = above ( n1 , p ) ;
    bool b3 = above ( n2 , p ) ;
    bool b4 = above ( n3 , p ) ;

    return ( b1 & b2 & b3 & b4 ) ;
  }
} ;

// plane_bounded_type is a generalization of a frustum where the
// 'inside' is defined by a set of arbitrary planes given by their
// normal and origin

struct plane_bounded_type
{
  const std::vector < point_3d_d_type > & normal ;
  const std::vector < point_3d_d_type > & origin ;
  const int size ;

  plane_bounded_type ( const std::vector < point_3d_d_type > & _normal ,
                       const std::vector < point_3d_d_type > & _origin )
  : normal ( _normal ) ,
    origin ( _origin ) ,
    size ( _normal.size() )
  {
    assert ( size == origin.size() ) ;
  }

  bool inside ( const point_3d_d_type & p )
  {
    for ( int i = 0 ; i < size ; i++ )
    {
      if ( below ( normal[i] , p - origin[i] ) )
      {
        return false ;
      }
    }
    return true ;
  }
} ;

bool arcs_intersect ( point_3d_d_type a0 ,
                      point_3d_d_type a1 ,
                      point_3d_d_type b0 ,
                      point_3d_d_type b1 )
{
  const point_3d_d_type origin ( 0 , 0 , 0 ) ;

  // same outcome: std::swap ( a0 , a1 ) ;

  auto n1 = normal ( origin , a0 , a1 ) ;
  auto n2 = normal ( origin , b0 , b1 ) ;

  // We need two versions, depending on whether a0 is 'above' n2
  // or 'below'. The tests establish several facts:
  // a0/a1 are on opposite sides of n2,
  // b0/b1 are on opposite sides of n1,
  // if a0 is above n2, b0 is below,
  // if a0 is below n2, b0 is above
  // The first two facts are established by the first and second
  // pair of subtests, and the third and fourth by the first and second.
  // a0/a1 may be swapped, b0/b1 also.

  if (    above ( n2 , a0 )
       && below ( n2 , a1 )
       && below ( n1 , b0 )
       && above ( n1 , b1 ) )

    return true ;

  if (    below ( n2 , a0 )
       && above ( n2 , a1 )
       && above ( n1 , b0 )
       && below ( n1 , b1 ) )

    return true;

  return false ;
}

// Next I want to find out if two frusta f1, f2 intersect, narrowing it to
// the infinite pyramid case, so no 'near' and 'far' planes. The idea is this:
// The four edges of each frustum are intersected with a unit sphere,
// yielding twice four points f11, f12, f13, f14 and f21, f22, f23, f24,
// which also define four arcs each: f11..f12, f12..f23 etc.
// I assume there are *only* these four cases :
// A: f1 is wholly within f2
// B: f2 is wholly within f1
// C: f1 and f2 intersect
// D: f2 and f2 do not intersect.
// A and B can be tested by creating four planes coinciding with the frustum
// side planes and testing whether the other four points all lie to the same
// side of the planes.
// To test C, I assume that, if any two arcs, one from f1i, one from f2j
// intersect, the frusta intersect. I assume that if there are no intersections
// and neither A nor B are the case, we have case D.

typedef vigra::TinyVector < point_3d_d_type , 4 > rectangle ;

void normalize ( rectangle & r )
{
  for ( int i = 0 ; i < 4 ; i++ )
    r[i] /= norm ( r[i] ) ;
}

bool frusta_intersect ( rectangle f1 , rectangle f2 )
{
  frustum_type fr1 ( f1[0] , f1[1] , f1[2] , f1[3] ) ;
  frustum_type fr2 ( f2[0] , f2[1] , f2[2] , f2[3] ) ;

//   auto xmin1 = std::min ( std::min ( f1[0][1] , f1[1][1] ) ,
//                           std::min ( f1[2][1] , f1[3][1] ) ) ;
// 
//   auto xmin2 = std::min ( std::min ( f2[0][1] , f2[1][1] ) ,
//                           std::min ( f2[2][1] , f2[3][1] ) ) ;
// 
//   auto xmax1 = std::max ( std::max ( f1[0][1] , f1[1][1] ) ,
//                           std::max ( f1[2][1] , f1[3][1] ) ) ;
// 
//   auto xmax2 = std::max ( std::max ( f2[0][1] , f2[1][1] ) ,
//                           std::max ( f2[2][1] , f2[3][1] ) ) ;
// 
//   auto ymin1 = std::min ( std::min ( f1[0][2] , f1[1][2] ) ,
//                           std::min ( f1[2][2] , f1[3][2] ) ) ;
// 
//   auto ymin2 = std::min ( std::min ( f2[0][2] , f2[1][2] ) ,
//                           std::min ( f2[2][2] , f2[3][2] ) ) ;
// 
//   auto ymax1 = std::max ( std::max ( f1[0][2] , f1[1][2] ) ,
//                           std::max ( f1[2][2] , f1[3][2] ) ) ;
// 
//   auto ymax2 = std::max ( std::max ( f2[0][2] , f2[1][2] ) ,
//                           std::max ( f2[2][2] , f2[3][2] ) ) ;
// 
//   std::cout << "F1: x " << xmin1 << " to " << xmax1
//             << " y " << ymin1 << " to " << ymax1 << std::endl ;
//   std::cout << "F2: x " << xmin2 << " to " << xmax2
//             << " y " << ymin2 << " to " << ymax2 << std::endl ;

  // first test: if any corner point of the rectangles defining the
  // frusta is inside the other frustum, we have an intersection.

  for ( int p = 0 ; p < 4 ; p++ )
  {
    if ( fr1.inside ( f2[p] ) || fr2.inside ( f1[p] ) )
      return true ;
  }

  // if we arrive here, none if the corner points of either rectangle
  // were inside the other one. We are not finished yet: there are cases
  // where the frusta intersect without any of the corner points falling
  // into the other frustum. This can happen, for example, if one rectangle
  // is very narrow and 'cuts off' a corner of the other one.
  // so we test for arc intersections:

  for ( int i = 0 ; i < 4 ; i++ )
  {
    for ( int j = 0 ; j < 4 ; j++ )
    {
      if ( arcs_intersect ( f1[i] , f1[(i+1)%4] , f2[j] , f2[(j+1)%4] ) )
      {
        // intersecting arcs tell us that the frusta intersect.
        // again, we needn't continue - the test is decided

        return true ;
      }
    }
  }

  // there is definitely no intersection

  return false ;
}

} ;

bool frusta_intersect ( const point_3d_d_type & a0 ,
                        const point_3d_d_type & a1 ,
                        const point_3d_d_type & a2 ,
                        const point_3d_d_type & a3 ,
                        const point_3d_d_type & b0 ,
                        const point_3d_d_type & b1 ,
                        const point_3d_d_type & b2 ,
                        const point_3d_d_type & b3 )
{
  detail::rectangle f1 ( a0 , a1 , a2 , a3 ) ;
  detail::rectangle f2 ( b0 , b1 , b2 , b3 ) ;
  return detail::frusta_intersect ( f1 , f2 ) ;
}

// next we have a section of code dealing with conversion from sRGB to linear RGB
// and back. We have the singular and vectorized variants, and also functors derived
// from vspline::unary_functor which use these simple routines and allow for their
// easy application with vspline's apply method, providing a convenient way of
// transforming nD arrays of pixels. Finally we have a fast RGB to sRGB conversion
// routine using linear interpolation over a lookup table, which is used for converting
// internally used linear RGB to sRGB before moving from float to 8bit RGBA. This method
// is less precise than the 'true' conversion due to the linear interpolation between
// the sampled points of the true function, but the difference is small and it is faster.

// overloads for syntactic convenience

bool any_of ( bool arg )
{
  return arg ;
}

bool all_of ( bool arg )
{
  return arg ;
}

bool none_of ( bool arg )
{
  return ! arg ;
}

template < class VT , class NT = float >
VT sRGB2RGB ( VT value , NT norm = NT ( 255 ) )
{
  if ( all_of ( value == NT ( 0 ) ) )
    return VT ( 0 ) ;

  value /= norm ;

  VT result = norm * pow ( ( value + NT(0.055) ) / NT(1.055) , NT(2.4) ) ;

  // using vspline::assign_if to code uniformly for scalar and vectorized VT:

  PV_ARCH::assign_if ( result ,
              value <= NT(0.04045) ,
              norm * value / NT(12.92) ) ;

  return result ;
}

template < class VT , class NT = float >
VT RGB2sRGB ( VT value , NT norm = NT ( 255 ) )
{
  if ( all_of ( value == NT ( 0 ) ) )
    return VT ( 0 ) ;

  value /= norm ;

  VT result = norm * (   NT(1.055) * pow ( value , NT(0.41666666666666667) )
                       - 0.055 ) ;

  // using vspline::assign_if to code uniformly for scalar and vectorized VT:

  PV_ARCH::assign_if ( result ,
              value <= NT(0.0031308) ,
              norm * NT(12.92) * value ) ;

  return result ;
}

/// internally, vspline handles all image data as float with a value range from
/// 0.0 to 255.0. The internal processing may operate with linear RGB values or with
/// sRGB values - internally, both are treated the same way. If sRGB values are
/// used internally, the results aren't strictly correct, but the difference isn't
/// very noticeable and processing is faster, because, at the end of processing, when
/// producing RGBA output for display, sRGB correction doesn't have to be applied.
/// For 'correct' results, internal processing with linear RGB can be used, which
/// takes roughly an extra 15% of processing power, but is mathematically correct
/// and also allows brightness correction by simple multiplication.
/// If input is in 8bit, the values are in range already, otherwise we need an
/// additional bias. The application of a bias and the conversion from sRGB to
/// linear RGB or linear RGB to sRGB are realized as vspline::unary_functors
/// which are processed with vspline::apply, which provides a convenient way
/// to use such a functor with multithreading and vectorization. We have three
/// functors, two handling RGB conversions and a third one which only applies
/// the bias. This is the first place where we define functors derived from
/// vspline's unary_functor. This is a functor containing two basic operations:
/// one unvectorized and one vectorized, both called eval. While functors are often
/// coded as returning their result, I code functors like 'procedures', returning
/// nothing, taking the input as a const reference and storing the result to a reference.
/// This is probably a matter of taste, but it has some advantages:
/// - input and output type are clear from the call parameter signature
/// - there's a possibility for 'shortcuts' which are hard to implement with
///   normal return-a-result semantics
/// - the entire processing chain delegated to 'inner' objects is only visible
///   inside the functor
/// The drawback is that this idiom is harder to get used to. It's easiest to
/// aproach it top-down: You have a value, you want a result, so you start writing
/// down an empty functor with just that and then flesh it out.

/// sRGB2RGB_functor provides the code to convert a float RGB pixel
/// from sRGB to RGB.

struct sRGB2RGB_functor
: public vspline::unary_functor < pixel_type , pixel_type , VSIZE >
{
  // state of the functor, const after construction, so that the functor qualifies
  // as 'pure' in a functional-programming sense
  const float bias ;

  // constructor initializes state
  sRGB2RGB_functor ( float _bias = 1.0f )
  : bias ( _bias )
  { } ;

  template < class PT >
  void eval ( const PT & c , PT & result ) const
  {
    result[0] = sRGB2RGB ( c[0] * bias , 255.0f ) ;
    result[1] = sRGB2RGB ( c[1] * bias , 255.0f ) ;
    result[2] = sRGB2RGB ( c[2] * bias , 255.0f ) ;
  }

} ;

/// RGB2sRGB_functor provides the code to convert a float RGB pixel
/// from RGB to sRGB.

struct RGB2sRGB_functor
: public vspline::unary_functor < pixel_type , pixel_type , VSIZE >
{
  const float bias ;

  // constructor initializes state
  RGB2sRGB_functor ( float _bias = 1.0f )
  : bias ( _bias )
  { } ;

  template < class PT >
  void eval ( const PT & c , PT & result ) const
  {
    result[0] = RGB2sRGB ( c[0] * bias , 255.0f ) ;
    result[1] = RGB2sRGB ( c[1] * bias , 255.0f ) ;
    result[2] = RGB2sRGB ( c[2] * bias , 255.0f ) ;
  }

} ;

struct sRGBA2RGBA_functor
: public vspline::unary_functor < pixel4_type , pixel4_type , VSIZE >
{
  // state of the functor, const after construction, so that the functor qualifies
  // as 'pure' in a functional-programming sense
  const float bias ;

  // constructor initializes state
  sRGBA2RGBA_functor ( float _bias = 1.0f )
  : bias ( _bias )
  { } ;

  template < class PT >
  void eval ( const PT & c , PT & result ) const
  {
    result[0] = sRGB2RGB ( c[0] * bias , 255.0f ) ;
    result[1] = sRGB2RGB ( c[1] * bias , 255.0f ) ;
    result[2] = sRGB2RGB ( c[2] * bias , 255.0f ) ;
    result[3] = c[3] * bias ;
  }

} ;

struct RGBA2sRGBA_functor
: public vspline::unary_functor < pixel4_type , pixel4_type , VSIZE >
{
  const float bias ;

  // constructor initializes state
  RGBA2sRGBA_functor ( float _bias = 1.0f )
  : bias ( _bias )
  { } ;

  template < class PT >
  void eval ( const PT & c , PT & result ) const
  {
    result[0] = RGB2sRGB ( c[0] * bias , 255.0f ) ;
    result[1] = RGB2sRGB ( c[1] * bias , 255.0f ) ;
    result[2] = RGB2sRGB ( c[2] * bias , 255.0f ) ;
    result[3] = c[3] * bias ;
  }

} ;

// convert a float RGB image from linear RGB to sRGB, using the
// 'true' conversion function, rather than the fast method which
// uses linear interpolation in the range of [0:255] only.
// Using vspline::apply rolls the functor out to several threads
// and uses SIMD code, so this is a good del faster than just
// applying it pixel by pixel. Hence the placement here, in the
// rendering code, which has access to vspline.

void dispatch::rgb2srgb ( view_type & image ,
                          int njobs = vspline::default_njobs ) const
{
  vspline::apply ( RGB2sRGB_functor() , image , njobs ) ;
}

void dispatch::rgba2srgba ( view4_type & image ,
                            int njobs = vspline::default_njobs ) const
{
  vspline::apply ( RGBA2sRGBA_functor() , image , njobs ) ;
}

void dispatch::argba2sargba ( view4_type & image ,
                              int njobs = vspline::default_njobs ) const
{
  auto seq =   deassociate_type()
             + RGBA2sRGBA_functor()
             + associate_type() ;

  vspline::apply ( seq , image , njobs ) ;
}

// reverse operation

void dispatch::srgb2rgb ( view_type & image ,
                          int njobs = vspline::default_njobs ) const
{
  vspline::apply ( sRGB2RGB_functor() , image , njobs ) ;
}

void dispatch::srgba2rgba ( view4_type & image ,
                            int njobs = vspline::default_njobs ) const
{
  vspline::apply ( sRGBA2RGBA_functor() , image , njobs ) ;
}

void dispatch::sargba2argba ( view4_type & image ,
                              int njobs = vspline::default_njobs ) const
{
  auto seq =   deassociate_type()
             + sRGBA2RGBA_functor()
             + associate_type() ;

  vspline::apply ( seq , image , njobs ) ;
}

// move from unassociated alpha to associated alpha and back

void dispatch::associate_alpha ( view4_type & image ,
                            int njobs = vspline::default_njobs ) const
{
  vspline::apply ( associate_type() , image , njobs ) ;
}

void dispatch::deassociate_alpha ( view4_type & image ,
                            int njobs = vspline::default_njobs ) const
{
  vspline::apply ( deassociate_type() , image , njobs ) ;
}

struct srgb_lut
{
  /// when we need to convert float results from linear RGB to sRGB quickly,
  /// we avoid using the 'true' conversion function with it's log and exp calls,
  /// and instead use linear interpolation over a lookup table, which is slightly
  /// less precise but faster - and much better than using a plain lookup table,
  /// which is even faster but can result in banding. Here, the discontinuity
  /// of the gamma curve is in the first derivative, which is hardly noticeable
  /// with 256 knot points.
  /// We encapsulate the lookup table in this type:

  struct rgb2srgb_lut_type
  {
    bspline < float , 1 > bspl ;

    rgb2srgb_lut_type()
    : bspl ( 256 , 1 , vspline::NATURAL )
    {
      auto & core = bspl.core ;

      // fill the lookup table
      for ( int i = 0 ; i < 256 ; i++ )
      {
        core[i] = RGB2sRGB ( float ( i ) , 255.0f ) ;
      }

      bspl.prefilter() ;
    }
  } ;

  struct srgb2rgb_lut_type
  {
    bspline < float , 1 > bspl ;

    srgb2rgb_lut_type()
    : bspl ( 256 , 1 , vspline::NATURAL )
    {
      auto & core = bspl.core ;

      // fill the lookup table
      for ( int i = 0 ; i < 256 ; i++ )
      {
        core[i] = sRGB2RGB ( float ( i ) , 255.0f ) ;
      }

      bspl.prefilter() ;
    }
  } ;

  // we need one object of each type:

  rgb2srgb_lut_type rgb2srgb ;
  srgb2rgb_lut_type srgb2rgb ;
} ;

struct srgb_handler
{
  // to produce a functor which can quickly apply a gradation curve to
  // both single floats and float vectors, we inherit from a vspline::evaluator
  // over a degree-1 b-spline. This functor uses fast linear interpolation to
  // yield it's output, which is not as precise as the 'true' calculation but
  // sufficiently close for our purposes.
  // Note how we could use any old b-spline over 256 values as a gradation
  // curve, even though we currently only use a specific one.
  // we set up four subclasses, the first two for single float conversions,
  // and the second two for RGB pixel conversions.

  struct fast_RGB2sRGB_type
  : public vspline::evaluator < float , float , VSIZE , 1 >
  {
    typedef vspline::evaluator < float , float , VSIZE , 1 > ev_t ;

    fast_RGB2sRGB_type ( const vspline::bspline < float , 1 > & bspl )
    : ev_t ( bspl )
    { }
  } ;

  struct fast_sRGB2RGB_type
  : public vspline::evaluator < float , float , VSIZE , 1 >
  {
    typedef vspline::evaluator < float , float , VSIZE , 1 > ev_t ;

    fast_sRGB2RGB_type ( const vspline::bspline < float , 1 > & bspl )
    : ev_t ( bspl )
    { }
  } ;

  struct fast_RGB2sRGB3_type
  : public vspline::unary_functor < pixel_type , pixel_type , VSIZE >
  {
    fast_RGB2sRGB_type fast_RGB2sRGB ;

    fast_RGB2sRGB3_type ( const vspline::bspline < float , 1 > & bspl )
    : fast_RGB2sRGB ( bspl )
    { }

    template < typename IN , typename OUT >
    void eval ( const IN & in , OUT & out ) const
    {
      fast_RGB2sRGB.eval ( in[0] , out[0] ) ;
      fast_RGB2sRGB.eval ( in[1] , out[1] ) ;
      fast_RGB2sRGB.eval ( in[2] , out[2] ) ;
    }

  } ;

  struct fast_sRGB2RGB3_type
  : public vspline::unary_functor < pixel_type , pixel_type , VSIZE >
  {
    fast_sRGB2RGB_type fast_sRGB2RGB ;

    fast_sRGB2RGB3_type ( const vspline::bspline < float , 1 > & bspl )
    : fast_sRGB2RGB ( bspl )
    { }

    template < typename IN , typename OUT >
    void eval ( const IN & in , OUT & out ) const
    {
      fast_sRGB2RGB.eval ( in[0] , out[0] ) ;
      fast_sRGB2RGB.eval ( in[1] , out[1] ) ;
      fast_sRGB2RGB.eval ( in[2] , out[2] ) ;
    }

  } ;

  // we need one instance of each subclass

  fast_RGB2sRGB_type fast_RGB2sRGB ;
  fast_sRGB2RGB_type fast_sRGB2RGB ;

  fast_RGB2sRGB3_type fast_RGB2sRGB3 ;
  fast_sRGB2RGB3_type fast_sRGB2RGB3 ;

  // the c'tor receives a reference to the object holding the lookup
  // tables:

  srgb_handler ( const srgb_lut & lut )
  : fast_RGB2sRGB ( lut.rgb2srgb.bspl ) ,
    fast_sRGB2RGB ( lut.srgb2rgb.bspl ) ,
    fast_RGB2sRGB3 ( lut.rgb2srgb.bspl ) ,
    fast_sRGB2RGB3 ( lut.srgb2rgb.bspl )
  { }

} ;

/* currently unused:

struct precise_srgb_handler
{
  // equivalent class directly using the conversion function, without
  // the lookup table.

  struct precise_RGB2sRGB_type
  : public vspline::unary_functor < float , float , VSIZE >
  {
    template < typename VT >
    void eval ( const VT & in , VT & out ) const
    {
      out = RGB2sRGB ( in ) ;
    }
  } ;

  struct precise_sRGB2RGB_type
  : public vspline::unary_functor < float , float , VSIZE >
  {
    template < typename VT >
    void eval ( const VT & in , VT & out ) const
    {
      out = sRGB2RGB ( in ) ;
    }
  } ;

  struct precise_RGB2sRGB3_type
  : public vspline::unary_functor < pixel_type , pixel_type , VSIZE >
  {
    precise_RGB2sRGB_type precise_RGB2sRGB ;

    template < typename IN , typename OUT >
    void eval ( const IN & in , OUT & out ) const
    {
      precise_RGB2sRGB.eval ( in[0] , out[0] ) ;
      precise_RGB2sRGB.eval ( in[1] , out[1] ) ;
      precise_RGB2sRGB.eval ( in[2] , out[2] ) ;
    }

  } ;

  struct precise_sRGB2RGB3_type
  : public vspline::unary_functor < pixel_type , pixel_type , VSIZE >
  {
    precise_sRGB2RGB_type precise_sRGB2RGB ;

    template < typename IN , typename OUT >
    void eval ( const IN & in , OUT & out ) const
    {
      precise_sRGB2RGB.eval ( in[0] , out[0] ) ;
      precise_sRGB2RGB.eval ( in[1] , out[1] ) ;
      precise_sRGB2RGB.eval ( in[2] , out[2] ) ;
    }

  } ;

  // we need one instance of each subclass

  precise_RGB2sRGB_type precise_RGB2sRGB ;
  precise_sRGB2RGB_type precise_sRGB2RGB ;

  precise_RGB2sRGB3_type precise_RGB2sRGB3 ;
  precise_sRGB2RGB3_type precise_sRGB2RGB3 ;
} ;

*/

// To provide the conversion functors to the remainder of the code, we
// make them available via a pointer - we can't use static objects,
// because the construction of the objects does already use ISA-specific
// code, so we *must not* run the c'tors of objects compiled for an ISA
// which the current CPU can't handle: we'd crash with an illegal
// instruction. Creation and initialization is done only for ISAs which
// are actually about to be used, by dispatch::init(). This will crash
// if the CPU can't handle the ISA used here, but in this case we'd crash
// soon after anyway.

static srgb_lut * p_srgb_lut = nullptr ;
static srgb_handler * p_srgb = nullptr ;

void dispatch::init() const
{
  // if srgb handling has been initialized already, return immediately

  if ( p_srgb != nullptr )
    return ;

  // create first the lookup table, then the RGB/sRGB conversion functors

  p_srgb_lut = mem_in() << new srgb_lut ;
  p_srgb = mem_in() << new srgb_handler ( *p_srgb_lut ) ;
}

void dispatch::exit() const
{
  // if srgb handling has been initialized already, return immediately

  if ( p_srgb != nullptr )
    memlog >> p_srgb ;

  if ( p_srgb_lut != nullptr )
    memlog >> p_srgb_lut ;
}

// helper class to hold a shared pointer to an object of load_type attached
// to an object of base_type. We'll use this type to hold a shared pointer to
// a vspline::bspline object attached to a vspline::evaluator, producing a
// functor which behaves like the evaluator, but is certain that the spline
// will persist while there are any copies of the 'holding_t' object alive.
// vspline::evaluator itself does not provide this service  - it's coded to
// be as compact as possible, and it can be constructed from 'raw' memory
// as well. So we have to add this feature on the user side.
// We won't store objects of this type, but rather 'grok' them and store
// the grokked functor. Grokking captures it's functionality as
// std::function, which works well with our 'holding_t' type, resulting in
// the behaviour we expect: as long as copies of the grokked functor are alive,
// the spline persists, and it is deleted when the last copy goes out of scope.
// Why such artistry? We want to be able to pass the grokked functor to other
// code (like, the rendering thread) without having to worry about how long
// the other code may need the 'holding_t' object. So we're free to destruct
// our initial 'holding_t' object if we come up with 'better' interpolators
// further down the line (which will be created by a dedicated thread) without
// invalidating the spline the rendering thread may still be using.
// Note that this method is only used for the 'primal' evaluators, because
// the intercession of a grok_type has a slight perfomance price. The code
// in class interpolator inserts the relevant functors in their 'native'
// type, so that the evaluators made from the image pyramids (available later
// on in interpolator creation) can be integrated into the pixel pipeline
// without being grokked.

template < typename load_type , typename base_type >
struct holding_t
: public base_type
{
  typedef std::shared_ptr < load_type > sp_load_type ;
  sp_load_type sp_load ;

  holding_t ( sp_load_type _sp_load , const base_type & _base )
  : sp_load ( _sp_load ) , base_type ( _base )
  { } ;
} ;

// factory function to vreate a holding_t

template < typename load_type , typename base_type >
holding_t < load_type , base_type >
hold ( std::shared_ptr < load_type > load ,
       const base_type & base )
{
  return holding_t < load_type , base_type >  ( load , base ) ;
}

/// raw_image_type is a class importing image data from disk and providing functors
/// to access these data in a form which pv expects, given the parameters it was
/// invoked with. The import loads raw data from disk. These data are used as
/// substrate for functors incorporating the necessary transformations, which
/// are built by exploiting vspline's functor chaining capabilities. The result
/// are functors yielding properly scaled and converted float pixels (using
/// interpolation where needed) which can be fed to rendering code, and which
/// can also be used by code creating more sophisticated interpolators without
/// the need to do these preliminary processing steps.
/// Use of interpolation on the raw data is only mathematically correct if the
/// incoming data are linear RGB. If sRGB data are interpolated, the result may
/// be acceptable. lux may be invoked to run with process_linear=false, in which
/// case sRGB raw data will be processed *as if* they were linear, accepting the
/// mathematical errors stemming from the technically incorrect processing.
/// While this way of importing the data seems unnecessarily cirtcuitous, it does
/// have advantages: Most of the time, incoming data will be unsigned char, and
/// accessing them natively uses little memory. If the image is really big, there
/// may not be enough space to hold the image data in float, which is the usual
/// way of keeping them. If import were done to float right in the invocation
/// of vigra's import code, the import would fail due to lack of memory. With
/// the import done in the native format, we have the option to proceed with
/// the native data and on-the-fly translation via the functors, allowing to
/// view very large images with, at least, 'decent' interpolation, which is
/// much better than having to give up. The availability of the preprocessing
/// functors also makes it easier for subsequent code to build the interpolators,
/// because all preprocessing is already encapsulated in the functors. Reading
/// the data from disk may also be faster, because it can proceed without having
/// to touch the data, and the functors, being vspline::unary_functors, are fully
/// vectorized and therefore very fast.
/// The code adds another level of convenience by 'attaching' the raw data to the
/// functors via std::shared_ptr. This way, the functors can be passed around and
/// copied freely, and the raw data are automatically deleted when their last user
/// desctructs. So we can pass the functors to the rendering thread without having
/// to worry about how long the rendering thread will need them, and we don't
/// have to 'manually' destruct the raw data at some point. We also have the option
/// to hold on to the raw data: all we need is to keep one of the functors alive.
/// What's not currently implemented is acceess to the raw data themselves: the
/// functors are type-erased and all we can expect from them is float pixels for
/// given coordinates.

template < typename px_t > using amplify_t =
typename vspline::amplify_type < px_t , px_t , px_t , VSIZE > ;

// unassociated_alpha_type is a functor which combines an RGB
// evaluator and an alpha channel evaluator, yielding an RGBA
// pixel with 'unassociated' or 'unpremultiplied' alpha channel.
// this form is required for output, but it's not suitable for
// the processing pipeline

struct unassociated_alpha_type
: public tf24_type
{
  const ev_type ev ;
  const alpha_ev_type aev ;

  unassociated_alpha_type ( const ev_type & _ev ,
                            const alpha_ev_type & _aev )
  : ev ( _ev ) ,
    aev ( _aev )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    auto v = ev ( in ) ;
    out[0] = v[0] ;
    out[1] = v[1] ;
    out[2] = v[2] ;
    out[3] = aev ( in ) ;
  }
} ;

struct unassociated_ialpha_type
: public iev4_type
{
  const iev_type ev ;
  const iaev_type aev ;

  unassociated_ialpha_type ( const iev_type & _ev ,
                             const iaev_type & _aev )
  : ev ( _ev ) ,
    aev ( _aev )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    auto v = ev ( in ) ;
    out[0] = v[0] ;
    out[1] = v[1] ;
    out[2] = v[2] ;
    out[3] = aev ( in ) ;
  }
} ;

// associated_alpha_type is a functor which combines an RGB
// evaluator and an alpha channel evaluator, yielding an RGBA
// pixel with 'associated' or 'premultiplied' alpha channel.
// This pixel type is used for calculations in the processing
// pipeline and has to be 'unassociated' before it can be fed
// to the display.

struct associated_alpha_type
: public tf24_type
{
  const ev_type ev ;
  const alpha_ev_type aev ;

  associated_alpha_type ( const ev_type & _ev ,
                          const alpha_ev_type & _aev )
  : ev ( _ev ) ,
    aev ( _aev )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    auto v = ev ( in ) ;
    auto alpha = aev ( in ) ;
    out[0] = v[0] * alpha ;
    out[1] = v[1] * alpha ;
    out[2] = v[2] * alpha ;
    out[3] = alpha ;
  }
} ;

struct associated_ialpha_type
: public tf24_type
{
  const iev_type ev ;
  const iaev_type aev ;

  associated_ialpha_type ( const iev_type & _ev ,
                           const iaev_type & _aev )
  : ev ( _ev ) ,
    aev ( _aev )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    auto v = ev ( in ) ;
    auto alpha = aev ( in ) ;
    out[0] = v[0] * alpha ;
    out[1] = v[1] * alpha ;
    out[2] = v[2] * alpha ;
    out[3] = alpha ;
  }
} ;

// simple cast by assignment

template < typename src_t , typename trg_t >
struct cast_type
: public vspline::unary_functor < src_t , trg_t , VSIZE >
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    vspline::assign ( out , in ) ;
  }
} ;

struct min_of_type
: public vspline::unary_functor < int2_type , float , VSIZE >
{
  iaev_type lhs ;
  iaev_type rhs ;

  min_of_type ( const iaev_type & _lhs ,
                const iaev_type & _rhs )
  : lhs ( _lhs ) ,
    rhs ( _rhs )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out = vspline::min ( lhs ( in ) , rhs ( in ) ) ;
  }
} ;

struct max_alpha_type
: public tf21_type
{
  alpha_ev_type lhs ;
  alpha_ev_type rhs ;

  max_alpha_type ( const alpha_ev_type & _lhs ,
                   const alpha_ev_type & _rhs )
  : lhs ( _lhs ) ,
    rhs ( _rhs )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out = vspline::max ( lhs ( in ) , rhs ( in ) ) ;
  }
} ;

struct min_alpha_type
: public tf21_type
{
  alpha_ev_type lhs ;
  alpha_ev_type rhs ;

  min_alpha_type ( const alpha_ev_type & _lhs ,
                   const alpha_ev_type & _rhs )
  : lhs ( _lhs ) ,
    rhs ( _rhs )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out = vspline::min ( lhs ( in ) , rhs ( in ) ) ;
  }
} ;

struct extract_alpha_type
: public tf21_type
{
  const ev4_type inner ;

  extract_alpha_type ( const ev4_type & _inner )
  : inner ( _inner )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    auto v4 = inner ( in ) ;

    // assuming that the alpha data gleaned from calling 'inner'
    // are to be used as unassociated alpha values, we make sure
    // that they are greater than zero.
  
    // TODO: alpha == 0 should be no problem, but I get strange
    // artifacts when running the mB&A with that.

    out = vspline::max ( v4[3] , 0.00001f ) ;
  }
} ;

// shift_t adds a fixed offset to a coordinate

struct shift_t
: public tf22_type
{
  const coordinate_type shift_by ;

  shift_t ( const coordinate_type & _shift_by )
  : shift_by ( _shift_by )
  { }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out[0] = in[0] + shift_by[0] ;
    out[1] = in[1] + shift_by[1] ;
  }
} ;

struct yield_opaque
: public tf23_type
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out = 255.0f ;
  }
} ;

struct raw_image_type
{
  typedef vigra::TinyVector < float , 2 > coordinate_type ;
  typedef vigra::TinyVector < int , 2 > discrete_coordinate_type ;
  typedef vigra::TinyVector < float , 3 > pixel_type ;

  typedef vspline::grok_type < discrete_coordinate_type , pixel_type , VSIZE >
    d_ev_type ;

  typedef vspline::grok_type < discrete_coordinate_type , float , VSIZE >
    d_alpha_ev_type ;

  float bias ;

  const source_type source ;
  bool process_linear ;
  bool grey_edge ;
  alpha_mode mode ;
  double scaling_step ;
  int smoothing_level ;
  int headroom ;
  vigra::Shape2 core_shape ;

  template < typename dtype > using
     raw_pixel_type = vigra::TinyVector < dtype , 3 > ;

  template < typename dtype > using
    raw_spline_type = vspline::bspline < raw_pixel_type < dtype > , 2 > ;

  template < typename dtype > using
    raw_alpha_spline_type = vspline::bspline < dtype , 2 > ;

  // this does no longer store the pixel's type, but just 1,2, or 4,
  // meaning the number of bytes which should be used for the data
  // ele type of the pixels in the array we set up to store the data.
  // 1 and 2 result in uchar and ushort values, 4 results in float,
  // because there is no advantage to be had from storing int4
  // values vs. float values, since later on arithmetic is done in
  // float anyway. oiio will convert whatever is in the image files
  // to the type we choose for storage. half data result in a '4'
  // value, so that we receive half data as floats.

  int pixel_dtype ;

  void * p_raw_spline ;
  void * p_raw_alpha_spline ;

  typedef typename vigra::TinyVector < vspline::xlf_type , 2 > limit_type ;

  limit_type lower_limit ;
  limit_type upper_limit ;

  // here we have the interpolators which are built over the raw image data.
  // The _ev0 functors provide direct access to the raw data at *discrete*
  // coordinates (with automatic application of the necessary bias etc.)
  // and may be used to initialize interpolators holding float data later on.
  // The other interpolators are 'proper' interpolators which provide spline
  // interpolation over the raw data, also with appropriate bias etc., but
  // with spline coefficients which have not been prefiltered - with degrees
  // greater than one, there will be blur, but for low degrees this is not
  // very noticeable, and does, on the other hand, reduce noise as well.

  d_ev_type primal_dev0 ;
  d_alpha_ev_type primal_alpha_dev0 ;

  alpha_ev_type primal_alpha_ev1 ;

  // next we have a set of functors yielding RGBA data. The first two are
  // taking discrete coordinates, and their results can be used to populate
  // an image or the base level of an image pyramid. The next two take
  // float coordinates and are based on degree-1 spline evaluators over the
  // raw data. They don't produce technically correct results, because they
  // merely interpolatoe the RGB and the alpha component separately and then
  // combine the results, but they are acceptable for preliminary use, until
  // 'proper' evaluators are set up.

  iev4_type primal_iargba ;
  iev4_type primal_iurgba ;
  ev4_type primal_argba ;
  ev4_type primal_urgba ;

  // primal_ev has several derived functors taking float coordinates. These
  // functors yield RGB data, working with the raw RGB data, which is correct
  // if there is no alpha channel.

  std::vector < ev_type > primal_ev ;
  std::shared_ptr < pixel_pyramid_type > p_primal_pyramid ;

  // any masks specified, via source image cropping or PTO masks, are applied
  // directly to the raw alpha data. This is done straight after loading the
  // data from disk.
  // TODO: I apply blur to the alpha channel, but this has to be applied only
  // to the alpha data which result from masking/cropping, whereas alpha
  // values from the image itself must remain unblurred. The code to achieve
  // this goal is currently a bit convoluted. 

  template < typename alpha_spline_type >
  void mask_alpha_channel ( alpha_spline_type * p_alpha )
  {
    typedef typename alpha_spline_type::value_type px_t ;
    float vmax = std::numeric_limits<px_t>::max() ;
    if ( std::is_same < px_t , float > :: value )
      vmax = 1.0f ;

    if ( source.has_masks == false && source.cropping_active == false )
    {
      // The image has no masks or cropping. So we can save ourselves
      // the setting up of a masked alpha channel and simply manipulate
      // the original alpha channel to affect the margin fade out.

      int w = p_alpha->core.shape(0) ;
      int h = p_alpha->core.shape(1) ;
      if ( source.full_height == false )
      {
        p_alpha->core.bindOuter ( 0 ) *= 1.0f / 16 ;
        p_alpha->core.bindOuter ( 1 ) *= 4.0f / 16 ;
        p_alpha->core.bindOuter ( 2 ) *= 11.0f / 16 ;
        p_alpha->core.bindOuter ( 3 ) *= 15.0f / 16 ;
        p_alpha->core.bindOuter ( h-4 ) *= 15.0f / 16 ;
        p_alpha->core.bindOuter ( h-3 ) *= 11.0f / 16 ;
        p_alpha->core.bindOuter ( h-2 ) *= 4.0f / 16 ;
        p_alpha->core.bindOuter ( h-1 ) *= 1.0f / 16 ;
      }
      if ( source.full_width == false )
      {
        p_alpha->core.bindInner ( 0 ) *= 1.0f / 16 ;
        p_alpha->core.bindInner ( 1 ) *= 4.0f / 16 ;
        p_alpha->core.bindInner ( 2 ) *= 11.0f / 16 ;
        p_alpha->core.bindInner ( 3 ) *= 15.0f / 16 ;
        p_alpha->core.bindInner ( w-4 ) *= 15.0f / 16 ;
        p_alpha->core.bindInner ( w-3 ) *= 11.0f / 16 ;
        p_alpha->core.bindInner ( w-2 ) *= 4.0f / 16 ;
        p_alpha->core.bindInner ( w-1 ) *= 1.0f / 16 ;
      }
      return ;
    }

    // first, we set up an array to hold the masking information,
    // starting out with the alpha value for 'fully opaque', which is
    // 255 in lux.

    vigra::MultiArray < 2 , float > masking ( p_alpha->core.shape() ) ;
    masking = vmax ;

    // first, we process PTO masks. These are simple polygonal masks,
    // of which we only process 'exclude masks', masks which mask out
    // unwanted image content.

    if ( source.has_masks )
    {
      auto clear = [&] ( int x , int y )
      {
        masking [ { x , y } ] = 0 ;
      } ;

      int w = masking.shape(0) ;
      int h = masking.shape(1) ;

      for ( const auto & mask : source.masks )
      {
        if ( mask.variant == 0 )
          fill_polygon ( mask.vx , mask.vy , 0 , 0 , w , h , clear ) ;
      }

    }

    if ( source.cropping_active )
    {
      float a = fabs ( source.crop_x1 - source.crop_x0 ) / 2.0 ;
      float b = fabs ( source.crop_y1 - source.crop_y0 ) / 2.0 ;

      if ( source.cropping_elliptic )
      {
        std::cout << "elliptic crop" << std::endl ;
        float mx = ( source.crop_x0 + source.crop_x1 ) / 2.0 ;
        float my = ( source.crop_y0 + source.crop_y1 ) / 2.0 ;

        // with the four limits given, we can create an elliptic
        // crop with the vertical extent y1-y0 and the horizontal
        // extent x1-x0.
        // TODO: we might fade the ellipse out by assigning semi
        // transparent alpha to points near the margin.

        if ( source.crop_fade > 0.0 )
        {
          auto ts = 1.0 / std::min ( a , b ) ;
          auto ri = 1.0 - source.crop_fade * ts ;
          auto ro = 1.0 ;

          for ( int y = 0 ; y < masking.shape(1) ; y++ )
          {
            auto dy = fabs ( y - my ) ;

            for ( int x = 0 ; x < masking.shape(0) ; x++ )  
            {
              auto dx = fabs ( x - mx ) ;

              auto r = ( dx * dx ) / ( a * a ) + ( dy * dy ) / ( b * b ) ;
              if ( r > ro )
                masking [ { x , y } ] = 0 ;
              else if ( r > ri )
              {
                auto xs = 1.0 - ( r - ri ) / ( source.crop_fade * ts ) ;
                masking [ { x , y } ] *= xs ;
              }
            }
          }
        }
        else
        {
          for ( int y = 0 ; y < masking.shape(1) ; y++ )
          {
            auto dy = fabs ( y - my ) ;
            // if the y coordinate is outside the ellipse, mask out
            // the entire line
            if ( dy > b )
            {
              for ( int x = 0 ; x < masking.shape(0) ; x++ )  
              {
                masking [ { x , y } ] = 0 ;
              }
              continue ;
            }
            // else, find the half width of the ellipse at given y and
            // mask out points with x outside
            // for the ellipse, we have x*x / a*a + y*y / b*b = 1, hence

            float xmargin = sqrt ( ( a * a ) * ( 1.0 - ( dy * dy ) / ( b * b ) ) ) ;
            for ( int x = 0 ; x < masking.shape(0) ; x++ )
      
            {
              auto dx = fabs ( x - mx ) ;
              if ( dx > xmargin )
                masking [ { x , y } ] = 0 ;
            }
          }
        }
      }
      else
      {
        std::cout << "rectangular crop" << std::endl ;
        if ( source.crop_fade > 0.0 )
        {
          for ( int y = 0 ; y < masking.shape(1) ; y++ )
          {
            for ( int x = 0 ; x < masking.shape(0) ; x++ )  
            {
              if (   ( x < source.crop_x0 )
                  || ( x >= source.crop_x1 )
                  || ( y < source.crop_y0 )
                  || ( y >= source.crop_y1 ) )
                masking [ { x , y } ] = 0 ;
              else if (    ( x >= source.crop_x0 + source.crop_fade )
                        && ( x <= source.crop_x1 - source.crop_fade )
                        && ( y >= source.crop_y0 + source.crop_fade )
                        && ( y <= source.crop_y1 - source.crop_fade ) )
                continue ;
              float alpha = 1.0 ;
              auto dx = x - source.crop_x0 ;
              if ( dx < source.crop_fade )
                alpha = std::min ( alpha , dx / source.crop_fade ) ;
              dx = source.crop_x1 - x ;
              if ( dx < source.crop_fade )
                alpha = std::min ( alpha , dx / source.crop_fade ) ;
              auto dy = y - source.crop_y0 ;
              if ( dy < source.crop_fade )
                alpha = std::min ( alpha , dy / source.crop_fade ) ;
              dy = source.crop_y1 - y ;
              if ( dy < source.crop_fade )
                alpha = std::min ( alpha , dy / source.crop_fade ) ;
              masking [ { x , y } ] *= alpha ;
            }
          }
        }
        else
        {
          for ( int y = 0 ; y < masking.shape(1) ; y++ )
          {
            for ( int x = 0 ; x < masking.shape(0) ; x++ )
            {
              if (    ( x < source.crop_x0 )
                  || ( x >= source.crop_x1 )
                  || ( y < source.crop_y0 )
                  || ( y >= source.crop_y1 ) )
                masking [ { x , y } ] = 0 ;
            }
          }
        }
      }
    }

    // now we delete two lines around the margin, and then
    // alpha blur will take care of the fade-out. This produces
    // a fade-out of the image around the margin.

    int w = masking.shape(0) ;
    int h = masking.shape(1) ;
    if ( source.full_height == false )
    {
      masking.bindOuter ( 0 ) = 0.0f ;
      masking.bindOuter ( 1 ) = 0.0f ;
      masking.bindOuter ( h-2 ) = 0.0f ;
      masking.bindOuter ( h-1 ) = 0.0f ;
    }
    if ( source.full_width == false )
    {
      masking.bindInner ( 0 ) = 0.0f ;
      masking.bindInner ( 1 ) = 0.0f ;
      masking.bindInner ( w-2 ) = 0.0f ;
      masking.bindInner ( w-1 ) = 0.0f ;
    }

    // finally, apply a low pass filter to the masking alpha channel
    // this mitigates the issue of the hard mask boundaries, but it
    // will 'pull in' a bit of masked-out content, so if the mask is
    // cut very close to unwanted features, they may bleed in. The
    // large-ish binomial is tentative, but seems to work quite well.

    vspline::convolution_filter
    ( masking ,
      masking ,
      { vspline::REFLECT , vspline::REFLECT } ,
      { 1.0 / 16.0 ,
        4.0 / 16.0 ,
        6.0 / 16.0 ,
        4.0 / 16.0 ,
        1.0 / 16.0
      } ,
      2 ) ;

// larger kernel:
//         { 1.0 / 64.0 ,
//           6.0 / 64.0 ,
//           15.0 / 64.0 ,
//           20.0 / 64.0 ,
//           15.0 / 64.0 ,
//           6.0 / 64.0 ,
//           1.0 / 64.0 } ,
//         3 ) ;

    // now we superimpose the masking alpha channel and the alpha
    // channel the image came with. We use the minimum of both.

    typedef vspline::yield_type < int2_type , px_t , VSIZE > yield_px_t ;

    min_of_type min_of (   yield_px_t ( p_alpha->core )
                         + cast_type < px_t , float > () ,
                         yield1_t ( masking ) ) ;

    vspline::transform ( min_of + cast_type < float , px_t > () ,
                         p_alpha->core ) ;

//     for ( int p = 0 ; p < masking.size() ; p++ )
//     {
//       if ( masking[p] < 255.0f )
//         p_alpha->core[p] = std::min ( float(p_alpha->core[p]) ,
//                                       float(masking[p]) ) ;
//     }

    // if any masks were applied, we finally need to 'brace' the alpha
    // channel

    p_alpha->brace() ;
  }

  // the hierarchy of set_XXX methods builds up the evaluation code
  // by stringing together the 'actual' evaluator with optional additional
  // processing steps. Once the complete functor is built, it is 'grokked'
  // and passed back through the call hierarchy to the initial caller, see
  // set_raw_ev below, where the entry point is.

  // set_rgb is the outermost setter function. Here we check if we need to
  // apply a conversion from sRGB to linear RGB or linear RGB to sRGB to
  // the incoming data.

  template < typename ET >
  ev_type set_rgb ( const ET & ev )
  {
    // we may add a tailing vspline::amplify_type object to modify the
    // brightness. this object has to be last in line.

    bool add_brightness = ( fabs ( source.brightness - 1.0 ) > .0001 ) ;
    amplify_t < pixel_type > brightness ( source.brightness ) ;

    if ( source.is_linear == process_linear )
    {
      // no need for RGB/sRGB conversion. If incoming raw data are sRGB,
      // they will nevertheless be processed with linear mathematics and the
      // result will not be subjected to linear RGB -> sRGB conversion. This
      // 'short circuit' is technically wrong, but produces acceptable results
      // and is faster, so lux won't insist on doing the mathematically correct
      // processing and allow it to happen.

      if ( add_brightness )
        return vspline::grok ( ev + brightness ) ;
      else
        return vspline::grok ( ev ) ;
    }
    else
    {
      // if the two flags differ, we either have sRGB raw data which need to
      // be converted to internal linear RGB - or we have linear RGB raw data
      // which have to be converted to sRGB to be submitted to 'short circuit'
      // processing without a final conversion to sRGB for display. This latter
      // processing path may seem strange, but it saves the final conversion
      // to sRGB, which is time-consuming, and if rendering time is the main
      // concern, accepting a result which is not totally correct but 'looks
      // okay' may be a good option.

      // we need to clamp the output of 'ev' to make sure fast_RGB2sRGB3
      // or fast_sRGB2RGB3 do not receive out-of-bounds input:

      auto gc = vspline::clamp < float , VSIZE > ( 0.0f , 255.0f , 0.0f , 255.0f ) ;
      auto mp = vspline::mapper < pixel_type , VSIZE > ( gc , gc , gc ) ;

      // With JPEG and unsigned-integer TIFFs, out-of-bounds input will
      // not occur, since the whole bandwidth of the data type constitutes
      // valid intensity values - e.g. 0-255 or 0-65535. But with images
      // holding float data, we may get, e.g., negative values which don't
      // constitute meaningfule intensity values. This isn't merely academic:
      // I've encountered this with float-valued images created with nona,
      // and also with float-valued output from enblend. Without the clamping,
      // the evaluation of the fast_... functors outside their range can result
      // in strange artifacts or might even crash the program.

      // on the output side, clamping is always done, because the output is
      // presented in sRGB. The absence of RGB/sRGB conversion for cases where
      // it's unnecessary (above, is_linear == process_linear) implies that
      // for these cases out-of-bounds values will be passed through without
      // interference. This is deliberate: openEXR input, for example, may have
      // very high intensity values, and clamping these data would remove all
      // the 'interesting' high-intensity content. Only when 'bending' openEXR
      // data to be internally processed as sRGB, clamping will happen. And
      // float TIFFs may contain negative intensity values. While I'm not sure
      // if this is intentional, if the data are interpreted as linear and
      // internal processing is also linear (pv -L xx.tif) - or the data are
      // interpreted as sRGB and internal processing is also done in sRGB
      // (pv -l xx.tif), the negative intensity values will be passed through.
      // You can even specifically see them, then, by adapting black and white
      // point apropriately, e.g. black point -3 and white point 0. You may
      // have to zoom in a fair amount to see the affected pixels, oftentimes
      // the nagative values occur only in small areas measuring a few pixels,
      // likely as a result of omitting saturation arithmetics when doing
      // image interpolation.

      if ( source.is_linear )
      {
        // we have incoming linear data, but internal processing is in sRGB

        if ( add_brightness )
          return vspline::grok (   ev
                                 + mp
                                 + p_srgb->fast_RGB2sRGB3
                                 + brightness ) ;
        else
          return vspline::grok (   ev
                                 + mp
                                 + p_srgb->fast_RGB2sRGB3 ) ;
      }
      else
      {
        // just the other way round

        if ( add_brightness )
          return vspline::grok (   ev
                                 + mp
                                 + p_srgb->fast_sRGB2RGB3
                                 + brightness ) ;
        else
          return vspline::grok (   ev
                                 + mp
                                 + p_srgb->fast_sRGB2RGB3 ) ;
      }
    }
  }

  // if 'bias' is not 1.0, the evaluator 'ev' is chained with an amplify_type
  // object, which multiplies the evaluator's output with 'bias'. The result
  // is passed to set_rgb to add sRGB/RGB conversion, if needed. set_rgb
  // returns a 'grokked' functor, which hides the composition of the object(s)
  // which do the processing internally. The type erasure used in grokking
  // also enables us to return a fixed type.

  template < typename ET >
  ev_type set_bias ( const ET & ev )
  {
    if ( bias == 1.0f )
      return set_rgb ( ev ) ;
    else
      return set_rgb ( ev + amplify_t < pixel_type > ( bias ) ) ;
  }

  template < typename ET >
  alpha_ev_type set_alpha_bias ( const ET & ev )
  {
    if ( bias == 1.0f )
      return vspline::grok ( ev ) ;
    else
      return vspline::grok ( ev + amplify_t < float > ( bias ) ) ;
  }

  // set_opaque is used to create a fully opaque alpha channel if mode
  // is 'WITH_ALPHA' and the image file does not have an alpha channel

  template < typename dtype >
  void set_opaque ( vigra::MultiArrayView < 2 , dtype > & trg )
  {
    trg = std::numeric_limits<dtype>::max() ;
  }

  // TODO: is this right? normally we're using 255 for full saturation

  void set_opaque ( vigra::MultiArrayView < 2 , float > & trg )
  {
    trg = 1.0f ;
  }

  void set_opaque ( vigra::MultiArrayView < 2 , double > & trg )
  {
    trg = 1.0 ;
  }

  template < typename dtype >
  void set_raw_ev ( fileio::image_info imageInfo )
  {
    const int & exif_orientation ( source.exif_orientation ) ;
    const bool is_periodic ( source.full_width ) ;
    const bool & is_linear ( source.is_linear ) ;

    int extra_bands = imageInfo.numExtraBands() ;

    // we'll start out be creating degree-1 splines over the image data in
    // their native format. Most of the time this will be UINT8 pixels, and
    // these initial splines will therefore need much less memory than the
    // float splines used later on for 'normal' processing. vspline is
    // flexible when it comes to evaluation, and can provide evaluators
    // yielding float from splines holding uint8 or short data. We'll
    // exploit this flexibility and create such evaluators, then we'll
    // chain these evaluators with a few further processing steps
    // addressing the range of the data, and the need to convert between
    // RGB and sRGB data. We'll end up with functors yielding float
    // data, preprocessed as needed, and calling code can just use these
    // functors to access the image data, relying on the preprocessing
    // being encapsulated in the functors. One use for these functors is
    // immediate access to the image data for rendering viewer frames,
    // in 'decent' (bilinear interpolation) quality and without use of
    // an image pyramid: in the beginning it's much better to have
    // something at all to show, and the 'better' interpolators can be
    // built in the background and will be used once they are ready.
    // With small images, this will be done so quickly that the user may
    // not even notice the start-up with lessened image quality. The
    // 'work in progress' on the interpolators might be communicated via
    // some sort of progress bar. The functors with the 'built-in'
    // preprocessing of the data can then be used to set up the
    // float-based interpolators, which provides a uniform source of
    // initial data, since the additional processing stages (widening
    // to float, bias, etc) are all encapsulated in the 'primal'
    // functors and the code setting up the 'better' interpolators
    // can access the data as appropriately preprocessed floats, no
    // matter what the source data were initally.
    // The additional preprocessing steps are implemented via the
    // method templates set_XXX further up.
    // As the code stands, the 'primal' interplator produces decent
    // results for view if no major downscaling is happening - if it does,
    // we have aliasing, and this is clearly visible, the more so the
    // larger the degree of downscaling is. As of this writing, this is
    // visible e.g. when displaying screen-filling views of large
    // images: they look grainy and crisp at first, then seem to blur,
    // when the antialiasing kicks in.

    typedef vigra::TinyVector < dtype , 3 > raw_pixel_type ;
    typedef vspline::bspline < raw_pixel_type , 2 > raw_spline_type ;
    typedef vspline::bspline < dtype , 2 > raw_alpha_spline_type ;

    // There is one more issue to consider: image orientation. Images are
    // often stored in a specific memory order which does not necessarily
    // coincide with display order: to display the image correctly, either
    // the displaying code has to take into account such a rotation, or
    // the layout of the data in memory has to be modified. I chose to
    // take the latter approach, because it makes handling 'further down
    // the line' easier and makes memory access more efficient, because
    // horizontal lines will coincide with dimension 0 of the nD arrays
    // holding the data. Here vigraimpex is extremely helpful: it can
    // perform reading from disk to arbitrarily strided data, as long as
    // the shape is right. So we can create a view to the array we use as a
    // target for the read operation which has just the shape that vigraimpex
    // expects, but still deposits the data in the memory order we desire.
    // Such a transposition will cost a bit of performance just once,
    // while the data are transported from vigraimpex' line buffer to
    // the target array (which may even go without effect because the disk
    // access is the limiting factor), and later on we can proceed as if the
    // data had been 'in the right order' in the first place.
    // The drawback is that we need some finessing with PTO data, which use
    // sensor-relative coordinates. Tha latter approach is better when
    // communicating lens properties, which are always sensor-relative,
    // because the lens can only be mounted in one position.

    // get the image's shape, taking into account the EXIF orientation tag:
    // all orientations greater than four are portrait, hence we swap width
    // and height, since internally we're handling the data so that the
    // top left pixel is first in memory, and we perform the import of the
    // image data in a way which deposits them in memory just so.

    core_shape = imageInfo.shape() ;
    if ( exif_orientation > 4 )
    {
      std::swap ( core_shape[0] , core_shape[1] ) ;
    }

    // we always specify REFLECT BCs for the vertical axis.
    // for full spherical panoramas touching the poles, this will display
    // correctly, if the prefiltering and bracing is done with specialized
    // code, see the calls to prefilter. The horizontal axis will also be
    // done with REFLECT BCs, unless the image is 360 degrees, which is
    // signalled by 'is_periodic'.

    vspline::bcv_type<2> bcv { vspline::REFLECT } ;

    if ( is_periodic )
    {
      bcv[0] = vspline::PERIODIC ;
    }

    // create a degree-1 b-spline over the raw data. Note that this spline
    // already has the desired final shape; if there was a change of aspect,
    // core_shape has been flipped.

    auto p_bspl = mem_in()
         << new raw_spline_type ( core_shape , 1 , bcv ,
                                  -1.0 , headroom ) ;

    p_raw_spline = (void*) p_bspl ;

    lower_limit = p_bspl->lower_limit() ;
    upper_limit = p_bspl->upper_limit() ;

    // this is where we deal with EXIF orientation. We have already adapted
    // core_shape to contain a flipped aspect for EXIF orientation
    // values greater than 4. The image, on disk, holds the data with the aspect
    // given by the vigra::imageInfo - normally this is a landscape value,
    // but in any case refers to the data as stored. So to use vigra's
    // importImage function, we have to pass it a view with that same geometry.
    // On the other hand, we create splines which hold the data so that the top
    // left pixel is always the first in memory. So what we need to do is to
    // create a view to the spline's data so that reading the raw data to this
    // view will place them into the spline's core in the desired layout.

    // strides and offset of the core view in the spline

    long xs = p_bspl->core.stride ( 0 ) ; // always 1
    long ys = p_bspl->core.stride ( 1 ) ;
    long offset = 0 ;

    // ditto, for alpha channel

    long axs = 0 ;
    long ays = 0 ;
    long aoffset = 0 ;

    // we assume any extra band holds an alpha channel and set up a
    // degree-1 b-spline to contain it. This will use bilinear
    // interpolation on the alpha channel. Again we use a shared object.
    // If we find, further down, that the alpha channel is fully opaque,
    // the spline we may set up for the raw alpha data will perish once
    // p_alpha goes out of scope.

    raw_alpha_spline_type * p_alpha = nullptr ;

    if ( extra_bands || mode == WITH_ALPHA )
    {
      // create an array for alpha data - either because the image
      // holds alpha data, which may or may not be used but are read
      // from the file initially - or because mode is set to WITH_ALPHA,
      // in which case an alpha channel will be created if the image
      // does not provide one.

      p_alpha = mem_in()
        << new raw_alpha_spline_type ( core_shape , 1 , bcv ) ;

      p_raw_alpha_spline = (void*) p_alpha ;

      axs = p_alpha->core.stride ( 0 ) ;
      ays = p_alpha->core.stride ( 1 ) ;
      aoffset = 0 ;
    }

    // strides for the MultiArrayView used to import the alpha channel.
    // we calculate these seperately to allow for the image data spline to
    // be created with additional headroom, so that it can be shifted.
    // currently this is not done, but I'd like to keep the option.
    // the stride adaptation code is now factored out to fileio::gyrate
    // and may move to OIIO

    // complete (all EXIF orientations) set of JPEG test images:
    // https://github.com/recurser/exif-orientation-examples

    fileio::gyrate ( exif_orientation ,
                     core_shape[0] , core_shape[1] ,
                     xs , ys , offset ) ;
  
    vigra::MultiArrayView < 2 , raw_pixel_type >
      import_view ( imageInfo.shape() ,
                    vigra::Shape2 ( xs , ys ) ,
                    p_bspl->core.data() + offset ) ;

    pv_clock::time_point start = pv_clock::now();

    if ( extra_bands )
    {
      fileio::gyrate ( exif_orientation ,
                       core_shape[0] , core_shape[1] ,
                       axs , ays , aoffset ) ;

      vigra::MultiArrayView < 2 , dtype >
        import_alpha_view ( imageInfo.shape() ,
                            vigra::Shape2 ( axs , ays ) ,
                            p_alpha->core.data() + aoffset ) ;

#ifdef USE_OIIO
      fileio::import_image_alpha ( imageInfo ,
                                   (void*) ( import_view.data() ) ,
                                   (void*) ( import_alpha_view.data() ) ,
                                   sizeof ( dtype ) ,
                                   4 ,
                                   import_view.stride(0) ,
                                   import_view.stride(1) ,
                                   import_alpha_view.stride(0) ,
                                   import_alpha_view.stride(1)
                                  ) ;
#else
      vigra::importImageAlpha ( imageInfo , import_view ,
                                import_alpha_view ) ;
#endif

      if ( mode == NO_ALPHA_IF_OPAQUE )
      {
        // to avoid rendering with alpha channel if the alpha
        // channel is fully opaque, we test for this special case.
        // first get a sample: the very first alpha value

        auto sample = p_alpha->core [ vigra::Shape2 ( 0 , 0 ) ] ;
        double a00 = sample * bias ;

        if ( a00 > 254.99 && a00 < 255.01 )
        {
          bool is_fully_opaque = true ;

          // sample is (nearly) fully opaque, let's see if all other
          // alpha values are the same as this first one

          // TODO: test may be coded more efficiently

          for ( auto const & a : p_alpha->core )
          {
            if ( a != sample )
            {
              // found a differing value; let's say the test failed

              is_fully_opaque = false ;
              break ;
            }
          }

          if ( is_fully_opaque )
          {
            std::cout << "ignoring fully opaque alpha channel"
                      << std::endl ;
            memlog >> p_alpha ;
            p_alpha = nullptr ;
            p_raw_alpha_spline = nullptr ;
          }
        }
      }
      else if ( mode == NO_ALPHA )
      {
        // this mode ignores the alpha channel unconditionally. It's as
        // if the image had not had an alpha channel in the first place.

        std::cout << "unconditionally ignoring alpha channel"
                  << std::endl ;

        memlog >> p_alpha ;
        p_alpha = nullptr ;
        p_raw_alpha_spline = nullptr ;
      }
      else
      {
        // for the other two modes, an alpha channel is required. So we
        // use the alpha channel from the image - even if it is fully
        // opaque.

        p_alpha->brace() ;
      }
    }
    else
    {
#ifdef USE_OIIO
      fileio::import_image ( imageInfo ,
                             (void*) ( import_view.data() ) ,
                             sizeof ( dtype ) ,
                             3 ,
                             import_view.stride(0) ,
                             import_view.stride(1) ) ;
#else
      vigra::importImage ( imageInfo , import_view ) ;
#endif

      if ( mode == WITH_ALPHA )
      {
        // the source image has no alpha channel, but 'mode' tells us
        // that we should provide an alpha channel. We have the array
        // for the alpha data already, now we fill in the value for
        // 'fully opaque', which depends on the data type (see set_opaque)

        set_opaque ( p_alpha->container ) ;
      }
    }

    p_bspl->brace() ;

    // now we have the raw data inside a bspline's core, in the 'right'
    // memory order, ready to use, and, optionally, the alpha channel
    // in another spline.

    // next we create a set of evaluators from the spline, with different
    // properties. We'll make 'ordinary' degree-0 to degree-7 evaluators
    // (shifted to this degree, so there is no prefilter), and also a
    // decimator to be used by the pyramid-building code to populate
    // pyramid level 1.
    // The 'discrete' evaluator isn't used for interpolation, but to
    // directly read the preprocessed image data. This evaluator is
    // set up to accept discrete coordinates. vspline has specialized
    // code for evaluation at discrete coordinates, where weight generation
    // is easier for degree 2 and up, and unnecessary for degree 0 and 1.
    // Here we have a discrete evaluator for degree 0, which simply loads
    // the spline coefficients and uses no weights on them, which is fast.
    // If the raw data are sRGB and process_linear is true, interpolation
    // on the data is not mathematically correct, so in this case, only
    // the discrete degree-0 evaluator below can be used to provide correct
    // image data. The calling code (class interpolator) will set up an image
    // pyramid from scratch, with the base level populated with results from
    // primal_dev0, which has the necessary sRGB to linear RGB conversion
    // and biasing built in.
    // While this image pyramid is not yet available, the technically
    // incorrect but serviceable interpolators built further down the line
    // can be employed to produce output - for on-screen display, this is
    // okay as a temporary measure until the 'proper' rendering objects
    // become available, which are set up by class interpolator.

    enum { vsize = vspline::vector_traits < float > :: vsize } ;

    auto dev0 =  vspline::evaluator < discrete_coordinate_type , pixel_type ,
                                     VSIZE , 0 , float ,
                                     raw_pixel_type > ( *p_bspl , { 0 , 0 } , -1 ) ;
    primal_dev0 = set_bias ( dev0 ) ;

    // The degree-1 and up evaluators can be used as interpolators and take real
    // coordinates. The degree-0 and degree-1 evaluators will yield mathematically
    // correct results if they contain linear RGB data, and acceptable but
    // slightly wrong results otherwise. Since there is no b-spline prefiltering
    // here, the higher-degree evaluators (2-7) will yield smoothed results,
    // because the b-spline basis is applied to un-prefiltered data, and the
    // same slight mathematical incorrectness occurs if the raw data are sRGB.
    // The higher-degree evaluators have two uses: they can be used as
    // decimators to populate an image pyramid's second level (typically using
    // the degree-7 evaluator), or they can serve for still image output,
    // accepting the slightly smoothed results - this is used mainly with
    // degree-2 which produces minimal smoothing but avoids the star-shaped
    // artifacts occuring with degree-1.

    // the first two 'primal' evaluators are built separately because they take
    // distinct template arguments (the 'specialize' template argument):
  
    primal_ev[0] = set_bias ( vspline::evaluator < coordinate_type , pixel_type ,
                                                   VSIZE , 0 , float ,
                                                   raw_pixel_type >
                                                   ( *p_bspl , { 0 , 0 } , -1 ) ) ;

    primal_ev[1] = set_bias ( vspline::evaluator < coordinate_type , pixel_type ,
                                                   VSIZE , 1 , float ,
                                                   raw_pixel_type > ( *p_bspl ) ) ;

    // for the remainder of the shifted evaluators, we use 'specialize' == -1,
    // indicating 'true' b-spline evaluation:

    for ( int d = 2 ; d < 8 ; d++ )
    {
      primal_ev[d] = set_bias ( vspline::evaluator < coordinate_type , pixel_type ,
                                                     VSIZE , -1 , float ,
                                                     raw_pixel_type >
                                                     ( *p_bspl , { 0 , 0 } , d - 1 ) ) ;
    }

    // after the shifted splines, we also add a decimator made with the
    // parameters which are also used for the image pyramid. This decimator
    // differs from the ones in the pyramid because it processes the raw
    // pixel data (e.g. UINT8 for JPEGs), as they are held in *p_bspl.
    // The decimator, like the 'primal evaluators', is 'wrapped' in the
    // additional processing stages needed to yield properly biased and
    // RGB/sRGB converted pixels. The resulting wrapped evaluator can be
    // fed as level-1 pixel generator to the c'tor of the primal pyramid.

    // if the data in the b-spline are sRGB, the results are not
    // technically correct: the transformation to linear RGB needs to
    // be done *before* the decimator is set up. Applying a filter to the
    // sRGB data and converting the result to linear RGB produces different
    // results than applying the filter to linear RGB data. So, again, if
    // process_linear is true and the input data are sRGB, we can create
    // this evaluator, but it won't be used to populate a pyramid's second
    // level. If the input is sRGB and process_linear is false, the decimator
    // will be used and the slight errors will be accepted: the sRGB data will
    // be processed as they are.
    // If the decimator requires a b-spline substrate, which is the case for
    // all decimators using CBFs (smoothing_level < -1), the resulting
    // decimator can't produce technically correct results, because the raw
    // data usually can't be prefiltered correctly. The results will be
    // usable, but the b-spline basis applied in the decimator will result
    // in stronger low-pass filtering than intended. Again, the error is
    // not overly large - the error is mainly in those frequencies in the
    // signal which are discarded anyway.
    
    primal_ev[8] = set_bias ( make_decimator ( p_bspl ,
                                               smoothing_level ,
                                               scaling_step ) ) ;
                                               
    if ( p_alpha )
    {
      if ( grey_edge )
      {
        mask_alpha_channel ( p_alpha ) ;
      }

      // this branch is taken if there is an alpha channel - either
      // it has been provided by the image or set up anew to enable
      // alpha processing even if the image did not provide alpha.

      // again we set up the evaluators using holding_t.

      auto adev0 = vspline::evaluator < discrete_coordinate_type , float ,
                                        VSIZE , 0 , float ,
                                        dtype > ( *p_alpha , { 0 , 0 } , -1 ) ;

      auto aev1 = vspline::evaluator < coordinate_type , float ,
                                       VSIZE , 1 , float ,
                                       dtype > ( *p_alpha ) ;

      // if there is an alpha channel, it may need bias as well:

      primal_alpha_dev0 = set_alpha_bias ( adev0 ) ;

      // primal_iargba is a special discrete evaluator, yielding RGBA pixels
      // with associated alpha channel. This will be used to yield initial
      // values for alpha pyramids, which operate only with 'premultiplied'
      // alpha. primal_iurgba is the equivalent functor yielding unassociated
      // RGBA values for discrete coordinates.

      primal_iargba = associated_ialpha_type ( primal_dev0 , primal_alpha_dev0 ) ;
      primal_iurgba = unassociated_ialpha_type ( primal_dev0 , primal_alpha_dev0 ) ;

      // using bilinear interpolation separately on the RGB component and the
      // alpha channel, and then applying the resulting alpha value to the RGB
      // data, is not technically correct, but acceptable for this 'preliminary'
      // source of pixel data. For now, I add these two functors for completeness'
      // sake - they may not be used and abolished later on.

      primal_alpha_ev1 = set_alpha_bias ( aev1 ) ;

      primal_argba = associated_alpha_type ( primal_ev[1] , primal_alpha_ev1 ) ;
      primal_urgba = unassociated_alpha_type ( primal_ev[1] , primal_alpha_ev1 ) ;

      mode = WITH_ALPHA ;
    }
    else
    {
      mode = NO_ALPHA ;
    }

    // mode is now either WITH_ALPHA or NO_ALPHA.

    // now, we have evaluators which can be used by the calling code already.
    // This isn't quite optimal speed-wise, but much better than nothing.
    // We also have functors yielding usable alpha values in primal_alpha_ev0
    // and primal_alpha_ev1.
    // The XXX_dev0 functors use discrete coordinates and are meant as
    // the base for constructing more elaborate interpolators, whereas the
    // other functors are meant to be used for rendering while the more
    // elaborate interpolators are not yet available, and/or for setting up
    // a 'primal pyramid' - an image pyramid from the second level up which
    // can later on be taken over by the calling code. The 'primal pyramid'
    // will only be produced under specific conditions:
    // - the calling code has passed build_primal_pyramid=true
    // - and the data are suitable.
  }

  // get_evaluator sets up an evaluator, packaged with a domain if necessary.
  // the resulting RGB pixels are made only from the RGB component of the
  // source data - if the source has RGBA data, the result is not technically
  // correct, but usable for a 'preliminary' data source.
  
  template < int nchannels = 3 ,
             typename ev_t =
             typename std::conditional < nchannels == 3 ,
                                         ev_type ,
                                         ev4_type > :: type >
  ev_t get_evaluator ( int level ,
                       int degree ,
                       limit_type ev_lower_limit ,
                       limit_type ev_upper_limit ) const
  {
    if ( degree > 7 )
      degree = 7 ;

    if ( degree < 0 )
      degree = 0 ;

    if ( level == 0 || p_primal_pyramid == nullptr )
    {
      // level-0 evaluators are made directly from primal_ev[X]; there is
      // no level-0 spline in the primal pyramid. This is to save space:
      // the level-0 spline normally holds four times as many pixels as
      // the level-1 spline, and we're avoiding the corresponding float
      // spline for level 0, because it takes up a lot of space: if the
      // raw data are unsigned char, it would take up four times as much
      // space as the raw data.
      // If there is no 'primal pyramid', we are also forced to make do
      // with one of the primeal evaluators, regardless of the desired
      // level: there simply isn't another level to be had, then.

      domain_t domain ( ev_lower_limit ,
                        ev_upper_limit ,
                        lower_limit ,
                        upper_limit ) ;

      return vspline::grok ( domain + primal_ev [ degree ] ) ;
    }
    else
    {
      // Here, we are certain that we have a request for level > 0 *and*
      // we have a 'primal pyramid' to produce it from.

      const auto & pv = p_primal_pyramid->level ;

      // if the requested level is higher than the primal pyramid's highest
      // level, we silently pick the pyramid's highest level instead.

      if ( level >= pv.size() )
        level = pv.size() - 1 ;

      // now we set up the 'domain' which handles the shifting and scaling
      // of incoming coordinates to the extent of the spline we have picked

      domain_t domain ( ev_lower_limit ,
                        ev_upper_limit ,
                        pv[level]->lower_limit() ,
                        pv[level]->upper_limit() ) ;

      // degree-0 and degree-1 use specialized evaluator objects, so we
      // use a switch statement. Alternatively, we might use make_evaluator,
      // which would produce a grok_type - to be 'grokked' again after chaning
      // with the domain. This might produce a slight performance penalty,
      // depending on the compiler and it's capabilities to 'optimze into'
      // the functional constructs.
      // In any case, get_evaluator will produce a viable evaluator as a
      // vspline::grok_type. How well this functor matches the parameters
      // passed to this function will depend on circumstances, but we can
      // be sure it's usable and adapted to yield data for coordinates
      // in the range of (ev_lower_limit, ev_upper_limit). Note that it's
      // *not* a 'safe evaluator': access with out-of-range coordinates
      // will likely result in a crash.

      switch ( degree )
      {
        case 0:
        {
          auto ev = vspline::evaluator
                        < coordinate_type , pixel_type ,
                          VSIZE , 0 , float , pixel_type >
                            ( * ( pv [ level ] ) , { 0 , 0 } , - 1 ) ;
          return vspline::grok ( domain + ev ) ;
        }
        case 1:
        {
          auto ev = vspline::evaluator
                        < coordinate_type , pixel_type ,
                          VSIZE , 1 , float , pixel_type >
                            ( * ( pv [ level ] ) ) ;

          return vspline::grok ( domain + ev ) ;
        }
        default:
        {
          auto ev = vspline::evaluator
                        < coordinate_type , pixel_type ,
                          VSIZE , -1 , float , pixel_type >
                            ( * ( pv [ level ] ) , { 0 , 0 } , degree - 1 ) ;

          return vspline::grok ( domain + ev ) ;
        }
      }
    }
  }

  template < >
  ev4_type get_evaluator < 4 , ev4_type > ( int level ,
                                            int degree ,
                                            limit_type ev_lower_limit ,
                                            limit_type ev_upper_limit ) const
  {
    domain_t domain ( ev_lower_limit ,
                      ev_upper_limit ,
                      lower_limit ,
                      upper_limit ) ;

    return vspline::grok ( domain + primal_argba ) ;
  }

  // finally, raw_image_type's constructor. This will load the image from
  // disk, produce a discrete evaluator yielding correct float data at
  // discrete coordinates, additional level-0 evaluators providing
  // interpolation over the raw data (which may not be mathematically
  // 100% correct) - and, optionally, a 'primal pyramid' containing splines
  // over scaled-down images, to render scaled-down views.
  // Even if the calling code requests the building of the 'primal pyramid',
  // this will only be done if the circumstances are right, so the calling
  // code must not rely on the primal pyramid.

  raw_image_type ( fileio::image_info imageInfo ,
                   const source_type & _source ,
                   bool _process_linear ,
                   bool _grey_edge ,
                   alpha_mode _mode = NO_ALPHA_IF_OPAQUE ,
                   bool build_primal_pyramid = true ,
                   double _scaling_step = 2.0 ,
                   int _smoothing_level = 7 ,
                   int floor = 64 )
  : source ( _source ) ,
    process_linear ( _process_linear ) ,
    grey_edge ( _grey_edge ) ,
    mode ( _mode ) ,
    scaling_step ( _scaling_step ) ,
    smoothing_level ( _smoothing_level ) ,
    p_raw_spline ( nullptr ) ,
    p_raw_alpha_spline ( nullptr ) ,
    primal_ev ( 9 )
  {
    // first we detect a few cases where we'll suppress building the primal
    // pyramid, even if the build_primal_pyramid parameter above is passed
    // true. We only need to test for these cases if build_primal_pyramid
    // is initially true.

    if ( build_primal_pyramid == true )
    {
      if ( source.is_linear == false && process_linear == true )
      {
        // The source data are sRGB, but internal processing uses linear RGB.
        // If that is the case, we can't build a primal pyramid, because we
        // use the raw data as level 0, which we don't want to convert to
        // linear RGB because they are usually 8bit and we'd get quantization
        // errors. Hence we omit the primal pyramid and rely on the final
        // pyramid instead.

        // How about the 'opposite' case, where incoming data are linear RGB
        // and process_linear is false? In that case, the incoming data are
        // chained with RGB->sRGB conversion, and this conversion happens
        // *after* interpolation and biasing, so the result is correct sRGB
        // and as intended.

        // TODO: if incoming data have wider range (e.g. float) we might opt
        // not to disable the primal pyramid

        build_primal_pyramid = false ;
      }
      else if ( smoothing_level < -1 )
      {
        // if the chosen decimator is -2 or smaller (indicating use of a CBF,
        // which needs a b-spline substrate), we dont'create a primal pyramid
        // either: the b-spline substrate would need prefiltering, which we
        // can only do on float data without loosing too much to quantization
        // errors.

        build_primal_pyramid = false ;
      }
      else if ( mode == WITH_ALPHA )
      {
        // if we have RGBA incoming, we can't build the primal pyramid, because
        // decimation needs to operate on associated alpha values, which aren't
        // manifest: we only have a functor yielding associated alpha for
        // discrete coordinates (primal_dev40), which can be used to fill the
        // base level of a pyramid holding aRGBA data. So, for this situation,
        // we also suppress building of the 'primal pyramid'.

        build_primal_pyramid = false ;
      }
    }
        

    int extra_bands = imageInfo.numExtraBands() ;
    pixel_dtype = imageInfo.pixelType() ;

    if ( extra_bands > 1 )
    {
      // are there really images with more than one extra channel?
      throw vspline::not_supported
        ( "lux can only process at most one extra channel" ) ;
    }

    // depending on the requested 'smoothing level' we calculate
    // the 'headroom' which is technically necessary. If building of
    // the 'primal pyramid' is disabled, there is no extra headroom
    // required technically, but we use SHIFT_CEIL as a minimum, to
    // allow for shifting later on.

    headroom = SHIFT_CEIL ;

    if ( smoothing_level >= 0 )
      headroom += ( smoothing_level + 1 ) / 2 ;
    else if ( smoothing_level == -1 )
      headroom += 1 ;
    else if ( smoothing_level == -2 )
      headroom += 1 ;
    else if ( smoothing_level == -3 )
      headroom += 2 ;
    else if ( smoothing_level == -4 )
      headroom += 2 ;
    else if ( smoothing_level <= -5 )
    {
      int ksz = - smoothing_level ;
      headroom += ( ( ksz - 1 ) / 2 ) ;
    }
    
    // depending on the pixel type and the RGB/sRGB situation, we will get
    // a specific pixel pipeline yielding preprocessed float values. This
    // pipeline is built up by a set of templated methods calling each other,
    // and the last one called 'groks' the whole chain of pixel processing
    // stages and returns the final functor as a vspline::grok_type

    // possible returns: 1,2 and 4. the first two are mapped to i8, i16
    // and 4 is mapped to float.

    // std::cout << "**** pixel_dtype " << pixel_dtype << std::endl ;

    switch ( pixel_dtype )
    {
      // case fileio::image_info::UINT8:
      case 1 :
      {
        // set_raw_ev will in turn call set_bias, which will in turn
        // call set_rgb, which finally produces the results. The final
        // functors will yield values ranging from 0 to 255 in either
        // sRGB or RGB, depending on the value of 'process_linear':
        // if process_linear is true, the functor will yield linear RGB,
        // otherwise sRGB.

        bias = 1.0f ;
        set_raw_ev < unsigned char > ( imageInfo ) ;
        break ;
      }
      // case fileio::image_info::UINT16:
      case 2 :
      {
        bias = 255.0f / 65535.0f ;
        set_raw_ev < unsigned short > ( imageInfo ) ;
        break ;
      }
      // case fileio::image_info::FLOAT:
      case 4 :
      {
        bias = 255.0f ;
        set_raw_ev < float > ( imageInfo ) ;
        break ;
      }
      default:
      {
        // bit harsh, but for now:
        abort_lux ( std::string ( "unexpected source image pixel data type\n" )
                + "in image file " + source.filename ) ;
      }
    }

    // Now we have a set of evaluators in primal_ev which can provide
    // image data. Next we optionally build a 'primal pyramid' if this
    // is not disabled and if it's technically possible.

    if ( build_primal_pyramid )
    {
      vspline::bcv_type<2> bcv { vspline::REFLECT } ;

      if ( source.full_width )
      {
        bcv[0] = vspline::PERIODIC ;
      }

      bool full_spherical = (    ( source.projection == SPHERICAL )
                              && source.full_height
                              && source.full_width ) ;

      // which 'primal evaluator' do we need? small positive smoothing levels
      // indicate shifted b-splines, as they are kept in primal_ev slots 0-7.
      // other smoothing levels indicate use of a specific decimator, which
      // will be found in slot 8. We might pass the ev in slot 8 unconditionally,
      // but the ev's in slot 0 and 1 are slightly more efficient than the
      // corresponding decimator in slot 8 due to better specialization.

      int primal_ev_nr = smoothing_level ;

      if ( primal_ev_nr > 7 || primal_ev_nr < 0 )
        primal_ev_nr = 8 ;

      // smoothing levels < -1 imply use of a CBF, which needs a b-spline
      // as it's substrate. But we can't usually prefilter the raw data:
      // mostly the data will be UINT8 or UINT16, and we don't have dynamic
      // range left for overshoots. That's why we only produce a degree-1
      // pyramid here, which works with the non-CBF decimators. decimators
      // using CBFs switch off build_raw_pyramids (see above) so we needn't
      // be concerned about the use of CBFs, because the decimator won't
      // be used in the first place.

      // create the pyramid

      p_primal_pyramid = std::make_shared < pixel_pyramid_type >
                            ( source.width ,
                              source.height ,
                              1 , // spline_degree
                              primal_ev[primal_ev_nr] ,
                              bcv ,
                              scaling_step ,
                              smoothing_level ,
                              floor ,
                              full_spherical ,
                              vspline::default_njobs
                            ) ;
    }
  }

  ~raw_image_type()
  {
    switch ( pixel_dtype )
    {
      // case fileio::image_info::UINT8:
      case 1:
      {
        memlog >> ( ( raw_spline_type < unsigned char > * ) p_raw_spline ) ;
        if ( p_raw_alpha_spline )
          memlog >> ( ( raw_alpha_spline_type < unsigned char > * ) p_raw_alpha_spline ) ;
        break ;
      }
      // case fileio::image_info::UINT16:
      case 2:
      {
        memlog >> ( ( raw_spline_type < unsigned short > * ) p_raw_spline ) ;
        if ( p_raw_alpha_spline )
          memlog >> ( ( raw_alpha_spline_type < unsigned short > * ) p_raw_alpha_spline ) ;
        break ;
      }
      // case fileio::image_info::FLOAT:
      case 4:
      {
        memlog >> ( ( raw_spline_type < float > * ) p_raw_spline ) ;
        if ( p_raw_alpha_spline )
          memlog >> ( ( raw_alpha_spline_type < float > * ) p_raw_alpha_spline ) ;
        break ;
      }
      default:
        break ;
    }
  }

} ;

// to_ray_type transforms incoming (target) coordinates to corresponding
// 3D 'rays'.

// KFJ 2019-03-28 changed the type for incoming coordinates from int2_type to
// coordinate_type. This change is possible because vspline's index_wield function
// (which provides discrete indices as input to a functor) produces these coordinates
// in the type the functor requires, rather than as an integral type, which the
// previous implementation did.
// Added 'mosaic_flag' which changes the interpretation of yaw to horizontal shift
// and pitch to vertical shift. This way we don't have to code separate logic to
// handle the orientation. Never mind the computations with quaternions are more
// expensive than mere shifts - this code is only executed once per construction
// of a target_to_ray object.

// KFJ 2019-07-10 to_ray_type now calculates coordinates pertaining to the
// viewer's coordinates system, which are independent of the evaluator. This
// simplifies matters here, because we don't have to have access to information
// about the evaluator (or the spline it's based on) and moves the need to
// transform the viewer coordinates into coordinates suitable for the evaluator
// into the interpolator used for the purpose. This has one disadvantage: when
// in pan mode, we still need to use an evaluator chained to a domain, rather
// than having a warp array with coordinates directly suitable for b-spline
// evaluation. The extra processing costs time: ca. .2 ms per frame. But, on
// the other hand, pan mode is not the most time-critical of all modes, and
// for direct calculation there should be no speed penalty, because the
// coordinate scaling and shifting has to be done at some point anyway.
// We do gain an advantage for pan mode as well: While panning, the interpolator
// can insert arbitrary evaluation code, which makes it possible for the
// interpolator to switch from using it's primal evaluator to using image
// pyramids when they are ready without the warp array becoming invalid. This
// invalidation of the warp array would be hard to detect, as it's up solely
// to the interpolator to make the switch. With the new code, this is not a
// problem. The old code would have required to disallow pan mode until the
// interpolator is fully set up.

// to_ray_type is the template for projections from 2D target
// coordinates to 3D ray coordinates.

template < projection_type projection >
struct to_ray_type
{ } ;

template<>
struct to_ray_type < RECTILINEAR >
: public tf23_type
{
  point_3d_d_type s0 ; // position of sample(0,0) of target
  point_3d_d_type dh ; // step vector in target's horizontal
  point_3d_d_type dv ; // step vector in target's vertical

  point_3d_d_type origin ;
  point_3d_d_type ur ;
  point_3d_d_type ll ;

  // not needed here, but used in other code:

  point_3d_d_type lr ;

  vigra::Quaternion<double> orientation ;
  // quaternion_type orientation ;
  bool is_rotated ;

  int w_out ;
  int h_out ;

  // rotation of a rectilinear target can be achieved by simply rotating
  // the corner points of the sensor's representation in model space.
  // This abbreviates the pipeline: instead of using a 2D stepper,
  // transforming the 2D coordinates to a 3D ray and rotating the ray,
  // we can produce a 3D stepper based on the pre-rotated corner points.
  // This makes calculation of rectilinear views fast.

  void rotate ( const quaternion_type & q )
  {
    orientation = q * orientation ;
    is_rotated = ( orientation != quaternion_type() ) ;

    origin = rotate_q ( origin , q ) ;
    ur = rotate_q ( ur , q ) ;
    ll = rotate_q ( ll , q ) ;
    lr = rotate_q ( lr , q ) ;

    // recalculate s0, dh and dv

    dh = ( ur - origin ) / w_out ;
    dv = ( ll - origin ) / h_out ;
    s0 = origin + 0.5 * dh + 0.5 * dv ;
  }

  // The c'tor sets up the parameters of the 'stepper': Each target
  // coordinate corresponds with a sample point on the target's
  // representation in model space. There are two ways to express
  // the sample points' location: as a 2D coordinate relative to
  // the target's surface, or as a 3D ray coordinate. The stepper
  // would normally traverse a grid on the 2D surface, then transform
  // the 2D coordinate to the corresponding 3D coordinate and finally
  // apply the rotation, but for a rectilinear target we can actually
  // *use a 3D stepper directly in model space* to the same effect.
  // So here we don't inherit from stepper_2d_type.

  to_ray_type ( const job_type * const p_job )
  {
    const frame_type & frame ( p_job->frame ) ;    // short identifier
    const target_type & target ( p_job->target ) ; // short identifier

    orientation = frame.orientation ;
    is_rotated = ( orientation != quaternion_type() ) ;

    double hfov = target.hfov ;
    double vfov = target.vfov ;

    w_out = target.width ;
    h_out = target.height ;

    // half width/height (cartesian distance of center to edge)

    double wh = tan ( hfov / 2.0 ) ;
    double hh = tan ( vfov / 2.0 ) ;

    // now calculate the position of the top left corner of
    // the 'sensor' (origin), and of the upper right and lower
    // left corner. The three points defining the 'sensor'
    // are now three corners of a rectangle centered at a
    // point on the unit sphere (1,0,0). Note how these three
    // points define the area of the sensor and do not coincide
    // with the points where this area is sampled: the origin
    // for the sampling of the sensor area (s0) and the sampling
    // steps (dh, dv) are calculated further down.

    origin = { 1.0 , - wh , - hh } ;
    ur     = { 1.0 ,   wh , - hh } ;
    ll     = { 1.0 , - wh ,   hh } ;

    // not needed here, but used in other code:

    lr     = { 1.0 ,   wh ,   hh } ;

    // now we apply modifications to the sensor like tilt,
    // shift and size. These operations will keep the sensor
    // rectangular, but that's the only invariant.

    frame.sensor_settings.apply ( origin , ur , ll , lr ) ;

    // perform the rotation of the target corner points
    // by applying the orientation quaternion.

    origin = rotate_q ( origin , frame.orientation ) ;
    ur = rotate_q ( ur , frame.orientation ) ;
    ll = rotate_q ( ll , frame.orientation ) ;
    lr = rotate_q ( lr , frame.orientation ) ;

    // calculate the difference from a target pixel to it's
    // horizontal/vertical neighbour. Note how ul/lr are derived
    // only from hfov/vfov, whereas the step width also depends
    // on w_out and h_out. hfov/vfov are metrics of the view,
    // while w_out and h_out are metrics of the target frame,
    // which may have been subjected to (possibly anisotropic)
    // scaling.

    dh = ( ur - origin ) / w_out ;
    dv = ( ll - origin ) / h_out ;

    // the center of the target's (0,0) pixel is half a step
    // from the origin. We get:
    // s0 + ( w_out - 1 ) * dh = ur - 0.5 
    // s0 + ( h_out - 1 ) * dv = ll - 0.5 
    // and our sampling of the sensor area is at the points
    // s0 + x * dh + y * dv, x < w_out, y < h_out

    s0 = origin + 0.5 * dh + 0.5 * dv ;
  } ;

  // eval() of the transformation receives an incoming 2D coordinate
  // into the target image and produces a 3D ray.
  
  template < class in_type , class out_type >
  void eval ( const in_type & in ,
                    out_type & out ) const
  {
    out[0] = s0[0] + in[0] * dh[0] + in[1] * dv[0] ;
    out[1] = s0[1] + in[0] * dh[1] + in[1] * dv[1] ;
    out[2] = s0[2] + in[0] * dh[2] + in[1] * dv[2] ;
  }
} ;

// other target projections use a helper class: stepper_2d_type.
// This class calculates sampling origin, step vectors and corner
// points in 2D, which the projection uses to convert incoming
// discrete target coordinates to 2D coordinates on the target's
// representation in model space.

struct stepper_2d_type
{
  point_2d_d_type s0 ; // position of sample(0,0) of target
  point_2d_d_type dh ; // step vector in target's horizontal
  point_2d_d_type dv ; // step vector in target's vertical

  point_2d_d_type origin ;
  point_2d_d_type ur ;
  point_2d_d_type ll ;
  point_2d_d_type lr ;

  // quaternion_type orientation ;
  vigra::Quaternion<double> orientation ;
  bool is_rotated ;

  int w_out ;
  int h_out ;

  // we can't use shortcuts - any rotations applied to this object
  // are chained to the orientation that's already present.

  void rotate ( const quaternion_type & q )
  {
    orientation = q * orientation ;
    is_rotated = ( orientation != quaternion_type() ) ;
  }

  // default c'tor, used when the to_ray_type object sets up the
  // stepper's parameters itself (like for stereographic targets)

  stepper_2d_type()
  : orientation ( get_rotation_q ( 0.0 , 0.0 , 0.0 ) )
  { }

  // set up a 2D stepper over a grid on the 2D surface of the target,
  // in units of model space. Transformation to 3D cordinates and
  // rotation is left to the evaluation code, there are no shortcuts.

  stepper_2d_type ( const job_type * const p_job )
  {
    const frame_type & frame ( p_job->frame ) ;    // short identifier
    const target_type & target ( p_job->target ) ; // short identifier

    double hfov = target.hfov ;
    double vfov = target.vfov ;

    w_out = target.width ;
    h_out = target.height ;

    // half width/height

    double wh ;
    double hh ;

    switch ( target.projection )
    {
      case RECTILINEAR:
        wh = tan ( hfov / 2.0 ) ;
        hh = tan ( vfov / 2.0 ) ;
        break ;
      case CYLINDRIC:
        wh = hfov / 2.0 ;
        hh = tan ( vfov / 2.0 ) ;
        break ;
      case SPHERICAL:
      case FISHEYE:
      case MOSAIC:
        wh = hfov / 2.0 ;
        hh = vfov / 2.0 ;
        break ;
      case STEREOGRAPHIC:
        wh = 2.0 * tan ( hfov / 4.0 ) ;
        hh = 2.0 * tan ( vfov / 4.0 ) ;
        break ;
      default:
        // this should not happen, but having a default is good style
        abort_lux ( "unhandled target projection" ) ;
        break ;
    }

    // now calculate the position of the top left corner of
    // the 'sensor' (origin), and of the upper right and lower
    // left corner. The three points defining the 'sensor'
    // are now three corners of a rectangle centered at a
    // point on the unit sphere (1,0,0). Note how these three
    // points define the area of the sensor and do not coincide
    // with the points where this area is sampled: the origin
    // for the sampling of the sensor area (s0) and the sampling
    // steps (dh, dv) are calculated further down.

    orientation = frame.orientation ;
    is_rotated = ( orientation != quaternion_type() ) ;

    origin = point_2d_d_type ( - wh , - hh ) ;
    ur     = point_2d_d_type (   wh , - hh ) ;
    ll     = point_2d_d_type ( - wh ,   hh ) ;
    lr     = point_2d_d_type (   wh ,   hh ) ;

    // 'manually' apply magnifying glass factor, if present.
    // sensor_settings_type's methods expect 3D points, but
    // stepper_2d_type operates in 2D.

    auto factor = 1.0 / frame.sensor_settings.magnifying_glass ;
    if ( factor != 1.0 )
    {
      origin *= factor ;
      ur *= factor ;
      ll *= factor ;
      lr *= factor ;
    }

    // calculate the difference from a target pixel to it's
    // horizontal/vertical neighbour. Note how ul/lr are derived
    // only from hfov/vfov, whereas the step width also depends
    // on w_out and h_out. hfov/vfov are metrics of the view,
    // while w_out and h_out are metrics of the target frame,
    // which may have been subjected to (possibly anisotropic)
    // scaling.

    dh = ( ur - origin ) / w_out ;
    dv = ( ll - origin ) / h_out ;

    s0 = origin + 0.5 * dh + 0.5 * dv ;
  }
} ;

// now we handle non-rectilinear target projections. For these cases,
// there are no handy shortcuts as in the rectilinear case, we have to
// use a 2D stepper producing 2D target coordinates, transform them
// to 3D ray coordinates, and then rotate them according to the given
// orientation value.

template<>
struct to_ray_type < SPHERICAL >
: public tf23_type ,
  public stepper_2d_type
{
  // this class, like the other non-rectilinear target-to-ray classes
  // sets up a 2D stepper over a grid on the 2D surface of the target,
  // in units of model space. Transformation to 3D cordinates and rotation
  // is left to the evaluation code, there are no shortcuts.

  to_ray_type ( const job_type * const p_job )
  : stepper_2d_type ( p_job )
  { } ;

  // eval() of the transformation receives an incoming 2D coordinate
  // into the target image and produces a 3D ray. This is expensive,
  // due to the transcendental functions and the quaternion rotation.
  // TODO: any speedup here would be very welcome.
  
  template < class in_type , class out_type >
  void eval ( const in_type & in ,
                    out_type & out ) const
  {
    // the 'stepper' gives us the 2D coordinate on the target's representation
    // in model space, which coincide with the two angles of the polar coordinate
    // in 3D space (where r==1)

    auto x1 = s0[0] + in[0] * dh[0] + in[1] * dv[0] ;
    auto y1 = s0[1] + in[0] * dh[1] + in[1] * dv[1] ;

    auto & z2 ( out[0] ) ;
    auto & x2 ( out[1] ) ;
    auto & y2 ( out[2] ) ;

    // the transformation to 3D coordinates looks slightly different to the
    // transformation found in literature, due to the coordinate system used
    // by pv. Note that the cosine is just a shifted version of a sine.

    z2 = cos ( x1 ) * cos ( y1 ) ;
    x2 = sin ( x1 ) * cos ( y1 ) ;
    y2 = sin ( y1 ) ;

    // and, optionally, apply the rotation into the facet's CS

    if ( is_rotated )
    {
      typedef typename in_type::value_type dtype ;
      typedef vigra::Quaternion < dtype > q_t ;

      const auto & fo ( orientation ) ;
      q_t q ( fo[0] , fo[1] , fo[2] , fo[3] ) ;

      out = rotate_q ( out , q ) ;
    }
  }
} ;

template<>
struct to_ray_type < FISHEYE >
: public tf23_type ,
  public stepper_2d_type
{
  to_ray_type ( const job_type * const p_job )
  : stepper_2d_type ( p_job )
  { }

  template < class in_type , class out_type >
  void eval ( const in_type & in ,
                    out_type & out ) const
  {
    // the 'stepper' gives us x and y coordinate of the fisheye target

    auto u = s0[0] + in[0] * dh[0] + in[1] * dv[0] ;
    auto v = s0[1] + in[0] * dh[1] + in[1] * dv[1] ;

    // from which we derive the radius theta and the angle phi

    auto r = sqrt ( u * u + v * v ) ;
    auto phi = atan2 ( u , - v ) ;

    auto & z2 ( out[0] ) ;
    auto & x2 ( out[1] ) ;
    auto & y2 ( out[2] ) ;

    z2 = cos ( r ) ;
    y2 = - sin ( r ) * cos ( phi ) ;
    x2 = sin ( r ) * sin ( phi ) ;

    if ( is_rotated )
    {
      typedef typename in_type::value_type dtype ;
      typedef vigra::Quaternion < dtype > q_t ;

      const auto & fo ( orientation ) ;
      q_t q ( fo[0] , fo[1] , fo[2] , fo[3] ) ;

      out = rotate_q ( out , q ) ;
    }
  }
} ;

template<>
struct to_ray_type < CYLINDRIC >
: public tf23_type ,
  public stepper_2d_type
{
  to_ray_type ( const job_type * const p_job )
  : stepper_2d_type ( p_job )
  { }

  template < class in_type , class out_type >
  void eval ( const in_type & in ,
                    out_type & out ) const
  {
    auto x1 = s0[0] + in[0] * dh[0] + in[1] * dv[0] ;
    auto y1 = s0[1] + in[0] * dh[1] + in[1] * dv[1] ;

    auto & z2 ( out[0] ) ;
    auto & x2 ( out[1] ) ;
    auto & y2 ( out[2] ) ;

    z2 = cos ( x1 ) ;
    x2 = sin ( x1 ) ;
    y2 = y1 ;

    if ( is_rotated )
    {
      typedef typename in_type::value_type dtype ;
      typedef vigra::Quaternion < dtype > q_t ;

      const auto & fo ( orientation ) ;
      q_t q ( fo[0] , fo[1] , fo[2] , fo[3] ) ;

      out = rotate_q ( out , q ) ;
    }
  }
} ;

template<>
struct to_ray_type < STEREOGRAPHIC >
: public tf23_type ,
  public stepper_2d_type
{
  to_ray_type ( const job_type * const p_job )
  : stepper_2d_type ( p_job )
  { }

  template < class in_type , class out_type >
  void eval ( const in_type & in ,
                    out_type & out ) const
  {
    // the 'stepper' gives us x and y coordinate of the fisheye target

    auto u = s0[0] + in[0] * dh[0] + in[1] * dv[0] ;
    auto v = s0[1] + in[0] * dh[1] + in[1] * dv[1] ;

    // from which we derive the radius theta and the angle phi

    auto r = sqrt ( u * u + v * v ) ;
    auto theta = atan ( r / 2.0 ) * 2.0f ;
    auto phi = atan2 ( u , - v ) ;

    auto & z2 ( out[0] ) ;
    auto & x2 ( out[1] ) ;
    auto & y2 ( out[2] ) ;

    z2 = cos ( theta ) ;
    y2 = - sin ( theta ) * cos ( phi ) ;
    x2 = sin ( theta ) * sin ( phi ) ;

    if ( is_rotated )
    {
      typedef typename in_type::value_type dtype ;
      typedef vigra::Quaternion < dtype > q_t ;

      const auto & fo ( orientation ) ;
      q_t q ( fo[0] , fo[1] , fo[2] , fo[3] ) ;

      out = rotate_q ( out , q ) ;
    }
  }
} ;

// to_ray_type for mosaic projection is slightly different to the other
// to_ray_type specializations. Here, we 'bend' the rotational quaternion
// to a different interpretation: we take roll as roll, but interpret yaw
// and pitch as horizontal and vertical shift. We use a stepper_2d_type
// object, but it's setup is done mainly here to implement the differnt
// interpretation of the orientation

template<>
class to_ray_type < MOSAIC >
: public tf23_type ,
  public stepper_2d_type
{
  // rotate p around the origin

  void rotate2d ( const double & theta ,
                  point_2d_d_type & p )
  {
    double xx =   p[0] * cos ( theta ) + p[1] * sin ( theta ) ;
    double yy = - p[0] * sin ( theta ) + p[1] * cos ( theta ) ;
    p[0] = xx ;
    p[1] = yy ;
  }

  // rotate p around c

  void rotate2d ( const double & theta ,
                  const point_2d_d_type & c ,
                  point_2d_d_type & p )
  {
    p -= c ;
    rotate2d ( theta , p ) ;
    p += c ;
  }

  // rotate all corner points

  void mrotate2d ( const double & theta )
  {
    rotate2d ( theta , origin ) ;
    rotate2d ( theta , ur ) ;
    rotate2d ( theta , lr ) ;
    rotate2d ( theta , ll ) ;
  }

  // rotate all corner points around c

  void mrotate2d ( const double & theta ,
                   const point_2d_d_type & c )
  {
    rotate2d ( theta , c , origin ) ;
    rotate2d ( theta , c , ur ) ;
    rotate2d ( theta , c , lr ) ;
    rotate2d ( theta , c , ll ) ;
  }

  // shift all corner points by delta

  void mshift ( const point_2d_d_type & delta )
  {
    origin += delta ;
    ur += delta ;
    lr += delta ;
    ll += delta ;
  }

  // recalculate s0 and step vectors

  void recalc()
  {
    dh = ( ur - origin ) / w_out ;
    dv = ( ll - origin ) / h_out ;
    s0 = origin + 0.5 * dh + 0.5 * dv ;
  }

public:

  // 'rotate' is called during the setup of facet_helper_type objects
  // to provide a modified 'stepper' which produces sample coordinates
  // in a facet's coordinate system. So we need to transform the
  // stepper's points to the facet's coordinate system, applying
  // shift and roll. Note how we use the *opposite* sequence to
  // what's used in the c'tor: here we shift first, then roll.

  void rotate ( const quaternion_type & q )
  {
    // we have a quaternion coming in, but in this context (mosaic
    // projection) it is only used to 'ferry' the three values dx,
    // dy and roll, which we extract here:

    auto dx   = q[0] ;
    auto dy   = q[1] ;
    auto roll = q[2] ;

    // combine dx and dy into a 2D value 'delta'

    point_2d_d_type delta { dx , dy } ;

    // shift by delta,

    mshift ( delta ) ;

    // rotate by 'roll'

    mrotate2d ( roll ) ;

    // recalculate s0 and step vectors

    recalc() ;
  }

  // the c'tor also does a coordinate transformation, from the stepper
  // 'at rest' to the stepper which has been moved about by the user
  // in the non-rendering code. The parameters of this transformation
  // have been stored by stepper_2d_type's c'tor in it's member variable
  // 'orientation', a quaternion, which is used to 'ferry in' the
  // parameters dx, dy and roll.

  to_ray_type ( const job_type * const p_job )
  : stepper_2d_type ( p_job )
  {
    auto dx = -orientation[0] ;
    auto dy = -orientation[1] ;
    auto roll = -orientation[2] ;

    // combine dx and dy into a 2D value 'delta'

    point_2d_d_type delta { dx , dy } ;

    // get the current center point

    point_2d_d_type center = ( ur + ll ) / 2.0 ;

    // rotate by 'roll'

    mrotate2d ( roll ) ;

    // rotate center by roll as well

    auto cr ( center ) ;

    // this shifts 'center', find out by how much

    rotate2d ( roll , cr ) ;
    auto diff = cr - center ;

    // now translate by delta, compensating the shift which occured, which
    // leaves us with the rotation, but keeps the view's center fixed.

    mshift ( delta - diff ) ;

    // recalculate s0 and step vectors

    recalc() ;
  } ;

  // eval() of the transformation receives an incoming 2D coordinate
  // into the target image and produces a 3D ray.
  
  template < class in_type , class out_type >
  void eval ( const in_type & in ,
                    out_type & out ) const
  {
    // note: z won't be used, to_source has to be mosaic as well.
    // but to be safe, we assign 1.0
    out[0] = 1.0f ;
    out[1] = s0[0] + in[0] * dh[0] + in[1] * dv[0] ;
    out[2] = s0[1] + in[0] * dh[1] + in[1] * dv[1] ;
  }
} ;

// Now, for each source image projection, we code a specific second
// transformation step, which transforms 3D ray coordinates to 2D source
// image coordinates. These 2D coordinates are relative to the source
// image's representation in model space. How these coordinates are
// scaled and shifted later on to access an interpolator is not an
// issue here, the functors provide the 'pure' geometry.

// to_source_type is the template for projections from 3D ray
// coordinates to 2D source coordinates

template < projection_type projection , typename dtype = float >
struct to_source_type
: public vspline::unary_functor < vigra::TinyVector < dtype , 3 > ,
                                  vigra::TinyVector < dtype , 2 > ,
                                  VSIZE >
{ } ;

// we also provide the reverse operation, converting 2D source
// coordinates to 3D ray coordinates. This will be used for
// facet-to-facet transformations - conversion from target
// coordinates to 3D ray coordinates is handled by to_ray_type.

template < projection_type projection , typename dtype = float >
struct from_source_type
: public vspline::unary_functor < vigra::TinyVector < dtype , 2 > ,
                                  vigra::TinyVector < dtype , 3 > ,
                                  VSIZE >
{ } ;

template < typename dtype >
struct to_source_type < SPHERICAL , dtype >
: public vspline::unary_functor < vigra::TinyVector < dtype , 3 > ,
                                  vigra::TinyVector < dtype , 2 > ,
                                  VSIZE >
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in ,
              out_type & out ) const
  {
    auto s = sqrt ( in[0] * in[0] + in[1] * in[1] ) ;
    auto theta = atan2 ( in[2] , s ) ;
    auto phi = atan2 ( in[1] , in[0] ) ;

    out[0] = phi ;
    out[1] = theta ;
  }
} ;

template < typename dtype >
struct from_source_type < SPHERICAL , dtype >
: public vspline::unary_functor < vigra::TinyVector < dtype , 2 > ,
                                  vigra::TinyVector < dtype , 3 > ,
                                  VSIZE >
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in ,
              out_type & out ) const
  {
    auto const & x1 ( in[0] ) ;
    auto const & y1 ( in[1] ) ;

    auto & z2 ( out[0] ) ;
    auto & x2 ( out[1] ) ;
    auto & y2 ( out[2] ) ;

    z2 = cos ( x1 ) * cos ( y1 ) ;
    x2 = sin ( x1 ) * cos ( y1 ) ;
    y2 = sin ( y1 ) ;
  }
} ;

template < typename dtype >
struct to_source_type < CYLINDRIC , dtype >
: public vspline::unary_functor < vigra::TinyVector < dtype , 3 > ,
                                  vigra::TinyVector < dtype , 2 > ,
                                  VSIZE >
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in ,
              out_type & out ) const
  {
    auto norm = sqrt ( in[0] * in[0] + in[1] * in[1] ) ;
    at_least ( dtype ( .00001f ) , norm ) ;

    out[0] = atan2 ( in[1] , in[0] ) ;
    out[1] = in[2] / norm ;
  }
} ;

template < typename dtype >
struct from_source_type < CYLINDRIC , dtype >
: public vspline::unary_functor < vigra::TinyVector < dtype , 2 > ,
                                  vigra::TinyVector < dtype , 3 > ,
                                  VSIZE >
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in ,
              out_type & out ) const
  {
    auto const & x1 ( in[0] ) ;
    auto const & y1 ( in[1] ) ;

    auto & z2 ( out[0] ) ;
    auto & x2 ( out[1] ) ;
    auto & y2 ( out[2] ) ;

    z2 = cos ( x1 ) ;
    x2 = sin ( x1 ) ;
    y2 = y1 ;
  }
} ;
  
template < typename dtype >
struct to_source_type < RECTILINEAR , dtype >
: public vspline::unary_functor < vigra::TinyVector < dtype , 3 > ,
                                  vigra::TinyVector < dtype , 2 > ,
                                  VSIZE >
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in ,
              out_type & out ) const
  {
    auto x = in[0] ;
    at_least ( dtype ( .00001f ) , x ) ;

    out[0] = in[1] / x ;
    out[1] = in[2] / x ;
  }
} ;

template < typename dtype >
struct from_source_type < RECTILINEAR , dtype >
: public vspline::unary_functor < vigra::TinyVector < dtype , 2 > ,
                                  vigra::TinyVector < dtype , 3 > ,
                                  VSIZE >
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in ,
              out_type & out ) const
  {
    auto const & x1 ( in[0] ) ;
    auto const & y1 ( in[1] ) ;

    auto & z2 ( out[0] ) ;
    auto & x2 ( out[1] ) ;
    auto & y2 ( out[2] ) ;

    z2 = 1 ;
    x2 = x1 ;
    y2 = y1 ;
  }
} ;
  
template < typename dtype >
struct to_source_type < FISHEYE , dtype >
: public vspline::unary_functor < vigra::TinyVector < dtype , 3 > ,
                                  vigra::TinyVector < dtype , 2 > ,
                                  VSIZE >
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in ,
              out_type & out ) const
  {
    // the maths are quite involved: first we obtain the planar polar coordinates
    // of the source image point, then we convert the 2D polar coordinates to
    // 2D cartesian.

    // KFJ 2018-02-03 same change as for spherical, avoiding asin and using
    // atan2 instead on s and x

    auto s = sqrt ( in[1] * in[1] + in[2] * in[2] ) ;

    auto r = dtype ( M_PI_2 ) - atan2 ( in[0] , s ) ;

    auto phi = atan2 ( in[2] , in[1] ) ;

    out[0] = r * cos ( phi ) ;
    out[1] = r * sin ( phi ) ;
  }
} ;

template < typename dtype >
struct from_source_type < FISHEYE , dtype >
: public vspline::unary_functor < vigra::TinyVector < dtype , 2 > ,
                                  vigra::TinyVector < dtype , 3 > ,
                                  VSIZE >
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in ,
              out_type & out ) const
  {
    auto const & u ( in[0] ) ;
    auto const & v ( in[1] ) ;

    auto r = sqrt ( u * u + v * v ) ;
    auto phi = atan2 ( u , - v ) ;

    auto & z2 ( out[0] ) ;
    auto & x2 ( out[1] ) ;
    auto & y2 ( out[2] ) ;

    z2 = cos ( r ) ;
    y2 = - sin ( r ) * cos ( phi ) ;
    x2 = sin ( r ) * sin ( phi ) ;
  }
} ;
  
template < typename dtype >
struct to_source_type < STEREOGRAPHIC , dtype >
: public vspline::unary_functor < vigra::TinyVector < dtype , 3 > ,
                                  vigra::TinyVector < dtype , 2 > ,
                                  VSIZE >
{
  typedef vspline::unary_functor < vigra::TinyVector < dtype , 3 > ,
                                  vigra::TinyVector < dtype , 2 > ,
                                  VSIZE > base_type ;
  using typename base_type::in_type ;
  using typename base_type::out_type ;

  void eval ( const in_type & in ,
              out_type & out ) const
  {
    auto reciprocal_norm = dtype ( 1.0f ) / sqrt (   in[0] * in[0]
                                                   + in[1] * in[1]
                                                   + in[2] * in[2] ) ;

    auto x = in[0] * reciprocal_norm ;
    auto y = in[1] * reciprocal_norm ;
    auto z = in[2] * reciprocal_norm ;

    // 'factor' projects x and y to stereographic: x+1 puts us to the point
    // on the sphere opposite the center of the view, and the 2.0 accounts for
    // the fact that the plane is now 2u distant instead of just 1 when seen
    // from the origin. If x gets very close to -1, we produce FLT_MAX as the
    // result, which shoud be outside the valid range

    auto factor = dtype ( 2.0f ) / ( x + dtype ( 1.0f ) ) ;

    if ( x <= dtype ( -1.0f ) + FLT_EPSILON )
    {
      out[0] = FLT_MAX ;
    }
    else
    {
      out[0] = y * factor ;
    }
    
    if ( x <= dtype ( -1.0f ) + FLT_EPSILON )
    {
      out[1] = FLT_MAX ;
    }
    else
    {
      out[1] = z * factor ;
    }
  }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in ,
              out_type & out ) const
  {
    auto reciprocal_norm = dtype ( 1.0f ) / sqrt (   in[0] * in[0]
                                                   + in[1] * in[1]
                                                   + in[2] * in[2] ) ;

    // project 3D view ray to unit sphere surface by applying the norm

    auto x = in[0] * reciprocal_norm ;
    auto y = in[1] * reciprocal_norm ;
    auto z = in[2] * reciprocal_norm ;

    // 'factor' projects x and y to stereographic: x+1 puts us to the point
    // on the sphere opposite the center of the view, and the 2.0 accounts for
    // the fact that the plane is now 2u distant instead of just 1 when seen
    // from the origin. If x gets very close to -1, we produce FLT_MAX as the
    // result, which shoud be outside the valid range

    auto factor = dtype ( 2.0f ) / ( x + dtype ( 1.0f ) ) ;

    out[0] = y * factor ;
    out[0] ( x <= dtype ( -1.0f ) + FLT_EPSILON ) = FLT_MAX ;
    out[1] = z * factor ;
    out[1] ( x <= dtype ( -1.0f ) + FLT_EPSILON ) = FLT_MAX ;
  }
} ;

template < typename dtype >
struct from_source_type < STEREOGRAPHIC , dtype >
: public vspline::unary_functor < vigra::TinyVector < dtype , 2 > ,
                                  vigra::TinyVector < dtype , 3 > ,
                                  VSIZE >
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in ,
              out_type & out ) const
  {
    auto const & u ( in[0] ) ;
    auto const & v ( in[1] ) ;

    auto & z2 ( out[0] ) ;
    auto & x2 ( out[1] ) ;
    auto & y2 ( out[2] ) ;

    auto r = sqrt ( u * u + v * v ) ;
    auto theta = atan ( r / dtype ( 2.0f ) ) * dtype ( 2.0f ) ;
    auto phi = atan2 ( u , - v ) ;

    z2 = cos ( theta ) ;
    y2 = - sin ( theta ) * cos ( phi ) ;
    x2 = sin ( theta ) * sin ( phi ) ;
  }
} ;
  
/// this class provides the projection for an unmodified head-on view of
/// an image. This view is what an 'ordinary' image viewer would show, and
/// it's typically used for viewing maps or - well - mosaics ;)
/// note that this projection goes together with to_ray_type < MOSAIC >,
/// where what is normally used to encode 'yaw' and 'pitch' is 'misused' to
/// mean horizontal and vertical shift. This trick allows to use the pano
/// viewing user interface without modifications: the UI changes the
/// 'orientation' quaternion as if there was a change of orientation, but
/// the rotational angles are interpreted differently in 'mosaic' mode.

template < typename dtype >
struct to_source_type < MOSAIC , dtype >
: public vspline::unary_functor < vigra::TinyVector < dtype , 3 > ,
                                  vigra::TinyVector < dtype , 2 > ,
                                  VSIZE >
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in ,
              out_type & out ) const
  {
    // the 'bent' ray coordinate already carries the desired coordinate
    // in it's second and third component, so all we need is this:

    out[0] = in[1] ;
    out[1] = in[2] ;
  }
} ;

template < typename dtype >
struct from_source_type < MOSAIC , dtype >
: public vspline::unary_functor < vigra::TinyVector < dtype , 2 > ,
                                  vigra::TinyVector < dtype , 3 > ,
                                  VSIZE >
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in ,
              out_type & out ) const
  {
    // the 'bent' ray coordinate already carries the desired coordinate
    // in it's second and third component, so all we need is this:

    out[0] = 1.0 ;
    out[1] = in[0] ;
    out[2] = in[1] ;
  }
} ;

// lens correction by coordinate manipulation. This code should behave
// precisely like panotools lens correction, as documented in
// https://www.panotools.org/dersch/barrel/barrel.html

struct lens_correction
: public tf22_type
{
  const float s ;          // reference radius
  const float a, b, c, d ; // coefficients of polynomial
  const float h, v ;       // horizontal and vertical offset
  const float cap_radius ;
  const bool shift_only ;

  // the 'lcp' member is not used to calculate the polynomial during
  // rendering, but to provide the inverse function, which is needed
  // to transform coordinates from one facet to another.
  // The build_polynomial functions are used to set the polynomial
  // up during construction, the polynomial itself is const.

  const pv_polynomial < double , 4 > lcp ;

  static const pv_polynomial < double , 4 > build_polynomial
    ( const source_type & source )
  {
    double cf[] { double ( source.a ) ,
                  double ( source.b ) ,
                  double ( source.c ) ,
                  double ( source.d ) ,
                  0.0 } ;
    return pv_polynomial < double , 4 > ( cf ) ;
  }

  lens_correction()
  : s(1), a(1), b(1), c(1), d(1), h(0), v(0),
    cap_radius(1), shift_only(true), lcp()
    { }

  // the incoming coordinate is simply the 3D ray coordinate projected
  // to the 2D source surface. This datum may be far beyond the limits
  // of image data populating this surface. The lens correction polynomial
  // is not guaranteed to behave in any particular way, so the coordinate
  // we get might be transformed from some 'far-out' position *back into*
  // the area covered by image data. To avoid this effect, we set an upper
  // limit for the radius we're prepared to accept as input, choosing the
  // value so generously that the capping won't affect correct operation,
  // but hoping that this limit will be small enough to avoid the unwanted
  // effect, which I dub 'warp-back'. We choose the maximal center-to-corner
  // distance which source_type provides - it's calculated there in the
  // course of figuring out the facet's frustum and takes lens correction
  // into account, so it's just what we need.

  lens_correction ( const source_type & source )
  : s ( source.s ) ,
    a ( source.a ) ,
    b ( source.b ) ,
    c ( source.c ) ,
    d ( source.d ) ,
    h ( source.h ) ,
    v ( source.v ) ,
    cap_radius ( source.r_max ) ,
    shift_only ( (    source.a == 0.0
                   && source.b == 0.0
                   && source.c == 0.0 ) ) ,
    lcp ( build_polynomial ( source ) )
  { }

  // scalar version of the evaluation. We use a template to allow
  // code which isn't time-critical or requires higher precision
  // to work in double precision.

  template < typename dtype >
  void eval ( const vigra::TinyVector < dtype , 2 > & in ,
                    vigra::TinyVector < dtype , 2 > & out ) const
  {
    dtype x = in[0] ;
    dtype y = in[1] ;

    if ( shift_only )
    {
      // add h and v to yield the output

      out[0] = x + h ;
      out[1] = y + v ;
    }
    else
    {
      // r is the distance to the center defined by (h,v)

      auto r = sqrt ( x * x + y * y ) ;

      // we cap the radius to avoid 'warp-back'

      if ( r > cap_radius )
        r = cap_radius ;

      // the radius is scaled to multiples of the reference radius

      r /= s ;

      // now we can calculate the scaling factor

      auto f = a * r * r * r + b * r * r + c * r + d ;

      // apply f to x and y, and add h and v to yield the output

      out[0] = x * f + h ;
      out[1] = y * f + v ;
    }
  }

  // inverse function, only scalar version, and working in double
  // precision - the polynomial is also in double and the code is
  // not time-critical.

  template < typename dtype >
  void reval ( const vigra::TinyVector < dtype , 2 > & in ,
                     vigra::TinyVector < dtype , 2 > & out ) const
  {
    double x = in[0] ;
    double y = in[1] ;

    if ( shift_only )
    {
      // subtract h and v to yield the output

      out[0] = x - h ;
      out[1] = y - v ;
    }
    else
    {
      x -= h ;
      y -= v ;

      // calculate distance from center, scale to reference radius
      // units

      double r = sqrt ( x * x + y * y ) ;
      r /= s ;

      // defensive: if r is very small, we simply ignore the
      // polynomial and avoid risking large errors by dividing by
      // a small value

      if ( r > 0.000000001 )
      {
        // find rr as the value which yields r when fed to the
        // polynomial - this is done with the polynomial's 'inverse'
        // method, which works correctly as long as the polynomial
        // is monotonously rising.

        double rr = 1 ;
        bool success = lcp.inverse ( r , rr ) ;
        assert ( success ) ;

        // now we can calculate the scaling factor and apply it to
        // x and y, yielding the output

        double factor = rr / r ;
        x *= factor ;
        y *= factor ;
      }

      out[0] = x ;
      out[1] = y ;
    }
  }

#ifdef VECTORIZE

  void eval ( const coordinate_v & in ,
                    coordinate_v & out ) const
  {
    auto x = in[0] ;
    auto y = in[1] ;

    if ( shift_only )
    {
      // add h and v to yield the output

      out[0] = x + h ;
      out[1] = y + v ;
    }
    else
    {
      // r is the distance to the center defined by (h,v)

      auto r = sqrt ( x * x + y * y ) ;

      // we cap the radius to avoid 'warp-back'

      r ( r > cap_radius ) = cap_radius ;

      // this is scaled to multiples of the reference radius

      r /= s ;

      // now we can calculate the scaling factor

      auto f = a * r * r * r + b * r * r + c * r + d ;

      // apply f to x and y, and add h and v to yield the output

      out[0] = x * f + h ;
      out[1] = y * f + v ;
    }
  }

#endif

} ;

// produce a coordinate transformation function which can transform
// a given 2D coordinate in facet 'trg' to a 2D coordinate in facet
// 'src'.

vspline::grok_type < point_2d_d_type ,
                     point_3d_d_type ,
                     VSIZE >
get_to_ray_type ( const source_type & src )
{
  switch ( src.projection )
  {
    case RECTILINEAR:
      return from_source_type < RECTILINEAR , double > () ;
      break ;
    case SPHERICAL:
      return from_source_type < SPHERICAL , double > () ;
      break ;
    case STEREOGRAPHIC:
      return from_source_type < STEREOGRAPHIC , double > () ;
      break ;
    case CYLINDRIC:
      return from_source_type < CYLINDRIC , double > () ;
      break ;
    case FISHEYE:
      return from_source_type < FISHEYE , double > () ;
      break ;
    case MOSAIC:
      return from_source_type < MOSAIC , double > () ;
      break ;
    default:
      // this should not happen, but having a default is good style
      std::string error
        ( "multi_facet_helper_type: unhandled projection" ) ;
      error += std::to_string ( src.projection ) ;
      abort_lux ( error ) ;
      break ;
  }
  return from_source_type < RECTILINEAR , double > () ;
}

vspline::grok_type < point_3d_d_type ,
                     point_2d_d_type ,
                     VSIZE >
get_to_source_type ( const source_type & src )
{
  switch ( src.projection )
  {
    case RECTILINEAR:
      return to_source_type < RECTILINEAR , double > () ;
      break ;
    case SPHERICAL:
      return to_source_type < SPHERICAL , double > () ;
      break ;
    case STEREOGRAPHIC:
      return to_source_type < STEREOGRAPHIC , double > () ;
      break ;
    case CYLINDRIC:
      return to_source_type < CYLINDRIC , double > () ;
      break ;
    case FISHEYE:
      return to_source_type < FISHEYE , double > () ;
      break ;
    case MOSAIC:
      return to_source_type < MOSAIC , double > () ;
      break ;
    default:
      // this should not happen, but having a default is good style
      std::string error
        ( "multi_facet_helper_type: unhandled projection" ) ;
      error += std::to_string ( src.projection ) ;
      abort_lux ( error ) ;
      break ;
  }
  return to_source_type < RECTILINEAR , double > () ;
}

// function to find corresponding coordinates in another facet.
// we use reverse transform semantics: the target facet is the
// facet where we have a coordinate, and the source facet is
// the facet where we want corresponding coordinates.
// This code is meant to be usable from the non-rendering code
// (I wrote it for mask conversion), hence the dispatch.
// The transformation is composed of several stages, and some
// of the stages may be omitted - like the application of
// the lens correction polynomial if there is no lens correction
// for a given facet.
// Notice how throughout we use [=] for the lambdas, making sure
// that the functor can be 'floated' and passed out to the caller

crdd_tf_type dispatch::get_inter_facet_tf
  ( const source_type & trg ,
    const source_type & src ,
    quaternion_type rotation ) const
{
  crdd_tf_type pass = [] ( const point_2d_d_type & t ,
                                 point_2d_d_type & s )
                         {
                           s = t ;
                         } ;

  crd3d_tf_type pass3 = [] ( const point_3d_d_type & t ,
                                   point_3d_d_type & s )
                           {
                             s = t ;
                           } ;

  if ( &src == &trg )
    return pass ;

  crdd_tf_type ex_trg = pass ;
  crd3d_tf_type rotate = pass3 ;
  crdd_tf_type to_src = pass ;

  // optional application of lens correction

  if ( src.lens_correction_active )
  {
    lens_correction lc ( src ) ;

    to_src = [=] ( const point_2d_d_type & t ,
                         point_2d_d_type & s )
                         {
                          lc.eval ( t , s ) ;
                         } ;
  }

  if ( trg.lens_correction_active )
  {
    lens_correction lc ( trg ) ;

    ex_trg = [=] ( const point_2d_d_type & t ,
                         point_2d_d_type & s )
                         {
                           lc.reval ( t , s ) ;
                         } ;
  }

  // 'pure' geometrical transformation, according to the facets'
  // projection

  auto trg_to_ray = get_to_ray_type ( trg ) ;
  auto ray_to_source = get_to_source_type ( src ) ;


  if ( rotation != quaternion_type() )
  {
    rotate = [=] ( const point_3d_d_type & t ,
                         point_3d_d_type & s )
                         {
                           s = rotate_q ( t , rotation ) ;
                         } ;
  }

  // range transformation from image coordinates to model space
  // and back

  typedef vspline::domain_type < point_2d_d_type , VSIZE > domain_t ;

  domain_t target_domain ( { trg.iextent.x0 , trg.iextent.y0 } ,
                           { trg.iextent.x1 , trg.iextent.y1 } ,
                           { trg.extent.x0 , trg.extent.y0 } ,
                           { trg.extent.x1 , trg.extent.y1 } ) ;

  domain_t source_domain ( { src.extent.x0 , src.extent.y0 } ,
                           { src.extent.x1 , src.extent.y1 } ,
                           { src.iextent.x0 , src.iextent.y0 } ,
                           { src.iextent.x1 , src.iextent.y1 } ) ;

  // putting it all together:

  return [=] ( const point_2d_d_type & t ,
                     point_2d_d_type & s )
                     {
                       point_2d_d_type p2 ;
                       point_3d_d_type p3 ;
                       target_domain.eval ( t , p2 ) ;
                       ex_trg ( p2 , p2 ) ;
                       trg_to_ray.eval ( p2 , p3 ) ;
                       rotate ( p3 , p3 ) ;
                       ray_to_source.eval ( p3 , p2 ) ;
                       to_src ( p2 , s ) ;
                       source_domain.eval ( s , s ) ;
                     } ;
}

// helper class to provide an evaluator yielding black/transparent
// for every coordinate. This is used for partially covered facet maps

template < typename result_type , int value = 0 >
struct ev_constant
: public vspline::unary_functor
         < coordinate_type , result_type , VSIZE >
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out = out_type ( value ) ;
  }
} ;

// fade_to_grey works on a bspline object's edge, inverting the sign of
// all values in the frame if the boundary condition is REFLECT. This results
// in the spline evaluating to precisely zero at it's limits, and fading
// towards this value. We can use this trick for both pixel splines and
// alpha splines with the same visual result: the image fades to black
// at the edges of the source image.
// Obviously we don't want this to happen for periodic splines.
// I also dim the outermost image line and the innermost frame line
// to one half - the flipping alone only has an effect which is 1/2
// pixel wide, so that some (minor) visible artifacts remain, but with
// the extra dimming, the fading-out is so good that the sawtooth
// artifacts are pretty much gone.
// When 'revert' is passed true, the effect is undone. This is simple:
// just flip again, and where dimming was applied, use the reciprocal
// factor. Being able to revert the effect helps us reuse more
// interpolators - see source_compatible()

template < typename spline_type >
void fade_to_grey ( spline_type * p_bspl ,
                    bool grey_horizontal ,
                    bool grey_north ,
                    bool grey_south ,
                    bool revert = false
                  )
{
  typedef typename spline_type::value_type vt ;
  vt flip ( -1.0f ) ;
  vt dim ( revert ? 2.0f : .5f ) ;

  if ( grey_horizontal )
  {
    int x ;
    int lf = p_bspl->left_frame[0] ;
    for ( x = 0 ; x < lf - 1 ; x++ )
    {
      p_bspl->container.bindAt ( 0 , x ) *= flip ;
    }
    p_bspl->container.bindAt ( 0 , x ) *= ( flip * dim ) ;
    p_bspl->container.bindAt ( 0 , x + 1 ) *= ( dim ) ;
    
    x = p_bspl->left_frame[0] + p_bspl->core.shape(0) - 1 ;
    p_bspl->container.bindAt ( 0 , x ) *= ( dim ) ;
    ++x ;
    p_bspl->container.bindAt ( 0 , x ) *= ( flip * dim ) ;
    ++x ;
    for ( ; x < p_bspl->container.shape(0) ; x++ )
    {
      p_bspl->container.bindAt ( 0 , x ) *= flip ;
    }
  }

  if ( grey_north )
  {
    int y ;
    int lf = p_bspl->left_frame[1] ;
    for ( y = 0 ; y < lf - 1 ; y++ )
    {
      p_bspl->container.bindAt ( 1 , y ) *= flip ;
    }
    p_bspl->container.bindAt ( 1 , y ) *= ( flip * dim ) ;
    p_bspl->container.bindAt ( 1 , y + 1 ) *= ( dim ) ;
  }

  if ( grey_south )
  {
    int y = p_bspl->left_frame[1] + p_bspl->core.shape(1) - 1 ;
    p_bspl->container.bindAt ( 1 , y ) *= ( dim ) ;
    ++y ;
    p_bspl->container.bindAt ( 1 , y ) *= ( flip * dim ) ;
    ++y ;
    for ( ; y < p_bspl->container.shape(1) ; y++ )
    {
      p_bspl->container.bindAt ( 1 , y ) *= flip ;
    }
  }
}

// To generate pixels for the target image, we take a functional approach:
// We create a processing pipeline from stages. Each stage represents an action
// which is parametrized at it's creation, making it pure in the sense that
// it has no mutable state. These actions are implemented as functors. I use a
// style of functor here which does not return anything, instead the data come
// in as const reference and go out to a reference to some target location.
// In a sense, doing so makes the function 'disappear', because the only thing
// specified is how an input relates to an output, and if there is a sequence of
// such relations, we end up having written a sequence of actions without
// any conventional function calls - it's as if we had written the sequence
// of actions expressis verbis. While using function calls may or may not be
// translated to similar constructs by the compiler, here we don't rely on
// the compiler to possibly generate them but explicitly ask for this construct.
// Processing pipelines are built by 'chaining' the actions (using vspline::chain
// and the factory function vspline::chain), which simply means that the first
// action's output becomes the second action's input.
//
// If there are many stages, this method becomes unwieldy and the complexity
// takes compile times up. So instead of having a separate functor for each stage,
// at times I cumulate several stages into one functor and use conditionals on const
// bools which are initialized at the functor's creation to switch stages on or off.
// Now the conditionals are inside the inner processing loop, but the compiler
// can optimize away the parts which are switched off, since they are known at
// the time the functor is created, which seems to be just about as fast as having
// separate functors for each stage.
//
// At times I also use a coding style where one functor takes another functor
// as argument and internally uses it to do it's job - rather than using chaining.
// I do so for example when 'shortcircuiting' things, where some prediacte steers
// whether the inner functor is executed or not, or when I have to access both
// the input and the output, for example in capture_type, which uses the incoming
// discrete 2D coordinate of a coordinate transformation as an index into an
// array where a copy of the transformation's result is stored.

/// struct preprocess bundles a few 'final touches' to incoming coordinates
/// and feeds them to it's interpolater, which is held as 'inner'.
///
/// struct preprocess receives transformed coordinates, so here we're already
/// operating in source image coordinates. Depending on circumstances, these
/// coordinates need further processing before they are used to call the inner
/// type's evaluation routine - here, inner type will usually by a bspline object,
/// and the evaluation finally yields the target pixel(s).
///
/// Note how we pull inner_type into this object rather than using vspline::chain.
/// This is necessary because in the vectorized evaluation routine, we may
/// have code to run after the call to inner::eval(), and the evaluation itself
/// may be omitted if all coordinates are outside the defined range.

template < class inner_type >
struct preprocess
: public vspline::unary_functor < coordinate_type , pixel_type , VSIZE >
{
  // immutable state of the functor:

  const bool shift_horizontally ;
  const bool limit_horizontally ;
  const bool limit_vertically ;
  const bool do_mask ;

  const float dx ;
  const float wrap ;
  const float lower_horizontal_limt ;
  const float upper_horizontal_limt ;
  const float lower_vertical_limit ;
  const float upper_vertical_limit ;

  const inner_type inner ;

  // constructor fixes the immutable state:

  preprocess ( bool _shift_horizontally ,
               bool _limit_horizontally ,
               bool _limit_vertically ,

               float _dx ,
               float _wrap ,
               float _lower_horizontal_limit ,
               float _upper_horizontal_limit ,
               float _lower_vertical_limit ,
               float _upper_vertical_limit ,

               const inner_type & _inner
             )

  : shift_horizontally ( _shift_horizontally ) ,
    limit_horizontally ( _limit_horizontally ) ,
    limit_vertically ( _limit_vertically ) ,
    do_mask ( _limit_horizontally | _limit_vertically ) ,

    dx ( _dx ) ,
    wrap ( _wrap ) ,
    lower_horizontal_limt ( _lower_horizontal_limit ) ,
    upper_horizontal_limt ( _upper_horizontal_limit ) ,
    lower_vertical_limit ( _lower_vertical_limit ) ,
    upper_vertical_limit ( _upper_vertical_limit ) ,

    inner ( _inner )
    { } ;

  // here is an example of a processing stage combining several steps into
  // one functor. const bool flags set at the functor's creation determine
  // whether a step is executed or not.

  // unvectorized operation:

  void eval ( const coordinate_type & c ,
                    pixel_type & result ) const
  {
    // to perform operations on the incoming coordinate, we need a non-const
    // helper value, which should get optimized away by the compiler.

    coordinate_type cc ( c ) ;

    // now we test the flags one by one, and if they are set we perform
    // the required action

    if ( shift_horizontally )
    {
      // this stage is used in PAN mode. if the source image is spherical
      // or cylindrical, panning can be achieved by simply adding a fixed
      // value to the x coordinates. If this takes the x coordinate too high
      // up, we wrap it back. Note how we blindly rely on dx being positive
      // and small enough to make a single subtraction sufficient to get it
      // back in range.
      cc [ 0 ] += dx ;
      if ( cc [ 0 ] >= upper_horizontal_limt )
        cc [ 0 ] -= wrap ;
    }
    if ( limit_horizontally )
    {
      // if the source image isn't 'full width' (360 degrees), we have to check
      // for the x coordinate to be inside the allowed range, so we can avoid
      // access to the spline outside it's defined range. For out-of-range
      // coordinates, we take a shortcut here and produce a black pixel.
      if (    cc [ 0 ] <= lower_horizontal_limt
           || cc [ 0 ] >= upper_horizontal_limt )
      {
        result = 0.0f ;
        return ;
      }
    }
    if ( limit_vertically )
    {
      // ditto, vertically
      if (    cc [ 1 ] <= lower_vertical_limit
           || cc [ 1 ] >= upper_vertical_limit )
      {
        result = 0.0f ;
        return ;
      }
    }
    // now all preprocessing (if any) is done. We might carry on here and split
    // the coordinate into integral and real part, then proceed with a call to
    // the evaluator taking the split coordinate, but we have the 'evenness'
    // template argument in the evaluator to the same effect, and there is no
    // advantage in doing the split here. So now it's evaluation time:
    inner.eval ( cc , result ) ;
  }

#ifdef VECTORIZE

  // the vectorized code does precisely the same as the unvectorized code, but
  // since we have conditionals, we need to use masking.

  typedef typename coordinate_v::value_type in_ele_v ;

  void eval ( const coordinate_v & c ,    // simdized nD coordinate
                    pixel_v & result ) const  // simdized value_type
  {
    result = 0.0f ;

    coordinate_v cc ( c ) ;

    in_ele_v::MaskType mask ( false ) ;

    if ( shift_horizontally )
    {
      cc [ 0 ] += dx ;
      cc [ 0 ] ( cc [ 0 ] >= upper_horizontal_limt ) -= wrap ;
    }

    if ( do_mask )
    {
      // do_mask is  set, so first we prepare the mask. While we're doing so,
      // we also set coordinates which were out of bounds to zero to avoid
      // evaluation outside the defined range.
      // KFJ 2019-07-12 setting invalid coordinates to zero was okay when
      // using raw spline coordinates, but when using arbitrary ranges, zero
      // may be out of bounds, hence we set invalid coordinates to the
      // lower limit, which should be safe.
      if ( limit_horizontally )
      {
        mask = (   ( cc [ 0 ] <= lower_horizontal_limt )
                 | ( cc [ 0 ] >= upper_horizontal_limt ) ) ;
        cc [ 0 ] ( mask ) = lower_horizontal_limt ;
      }
      if ( limit_vertically )
      {
        mask |= (   ( cc [ 1 ] <= lower_vertical_limit )
                  | ( cc [ 1 ] >= upper_vertical_limit ) ) ;
        cc [ 1 ] ( mask ) = lower_vertical_limit ;
      }
      if ( all_of ( mask ) )
      {
        // if the mask is fully set we can avoid the evaluation altogether.
        // testing the mask here is cheap, since we have just accessed it anyway.
        result = 0.0f ;
      }
      else
      {
        // otherwise we evaluate and apply the mask to the result
        inner.eval ( cc , result ) ;
        result [ 0 ] ( mask ) = 0.0f ;
        result [ 1 ] ( mask ) = 0.0f ;
        result [ 2 ] ( mask ) = 0.0f ;
      }
    }
    else
    {
      // if do_mask isn't set, we evaluate unconditionally
      inner.eval ( cc , result ) ;
    }
  }

#endif
} ;

/// preprocess4 is a variant of preprocess producing float RGBA pixels.

template < class inner_type >
struct preprocess4
: public vspline::unary_functor < coordinate_type , pixel4_type , VSIZE >
{
  // immutable state of the functor:

  const bool shift_horizontally ;
  const bool limit_horizontally ;
  const bool limit_vertically ;
  const bool do_mask ;

  const float dx ;
  const float wrap ;
  const float lower_horizontal_limt ;
  const float upper_horizontal_limt ;
  const float lower_vertical_limit ;
  const float upper_vertical_limit ;

  const inner_type inner ;

  const vspline::grok_type < coordinate_type , float , VSIZE > transparency ;

  // constructor fixes the immutable state:

  preprocess4 ( bool _shift_horizontally ,
                bool _limit_horizontally ,
                bool _limit_vertically ,

                float _dx ,
                float _wrap ,
                float _lower_horizontal_limit ,
                float _upper_horizontal_limit ,
                float _lower_vertical_limit ,
                float _upper_vertical_limit ,

                const inner_type & _inner ,
                const vspline::grok_type < coordinate_type , float , VSIZE >
                      & _transparency
              )
  : shift_horizontally ( _shift_horizontally ) ,
    limit_horizontally ( _limit_horizontally ) ,
    limit_vertically ( _limit_vertically ) ,
    do_mask ( _limit_horizontally | _limit_vertically ) ,

    dx ( _dx ) ,
    wrap ( _wrap ) ,
    lower_horizontal_limt ( _lower_horizontal_limit ) ,
    upper_horizontal_limt ( _upper_horizontal_limit ) ,
    lower_vertical_limit ( _lower_vertical_limit ) ,
    upper_vertical_limit ( _upper_vertical_limit ) ,

    inner ( _inner ) ,
    transparency ( _transparency )
    { } ;

  // unvectorized operation:

  void eval ( const coordinate_type & c ,
                    pixel4_type & result ) const
  {
    // to perform operations on the incoming coordinate, we need a non-const
    // helper value, which should get optimized away by the compiler.

    coordinate_type cc ( c ) ;

    // now we test the flags one by one, and if they are set we perform
    // the required action

    if ( shift_horizontally )
    {
      // this stage is used in PAN mode. if the source image is spherical
      // or cylindrical, panning can be achieved by simply adding a fixed
      // value to the x coordinates. If this takes the x coordinate too high
      // up, we wrap it back. Note how we blindly rely on dx being positive
      // and small enough to make a single subtraction sufficient to get it
      // back in range.
      cc [ 0 ] += dx ;
      if ( cc [ 0 ] >= upper_horizontal_limt )
        cc [ 0 ] -= wrap ;
    }
    if ( limit_horizontally )
    {
      // if the source image isn't 'full width' (360 degrees), we have to check
      // for the x coordinate to be inside the allowed range, so we can avoid
      // access to the spline outside it's defined range. For out-of-range
      // coordinates, we take a shortcut here and produce a black transparent pixel.
      if (    cc [ 0 ] <= lower_horizontal_limt
           || cc [ 0 ] >= upper_horizontal_limt )
      {
        result = 0.0f ; // note: also sets alpha channel to transparent
        return ;
      }
    }
    if ( limit_vertically )
    {
      // ditto, vertically
      if (    cc [ 1 ] <= lower_vertical_limit
           || cc [ 1 ] >= upper_vertical_limit )
      {
        result = 0.0f ; // note: also sets alpha channel to transparent
        return ;
      }
    }

    transparency.eval ( cc , result[3] ) ;

    // honour alpha channel inside the image and set the fourth float in
    // 'result' to the value which 'transparency' yields at the given coordinate.
    // If this turns out to be 0 (fully transparent), skip inner.eval

    if ( result[3] != 0.0f )
    {
      // now all preprocessing (if any) is done. We might carry on here and split
      // the coordinate into integral and real part, then proceed with a call to
      // the evaluator taking the split coordinate, but we have the 'evenness'
      // template argument in the evaluator to the same effect, and there is no
      // advantage in doing the split here. So now it's evaluation time. Here
      // we pass a reference to 'pixel_type' which encompasses only the RGB
      // part of the result.

      pixel_type & result3 = reinterpret_cast < pixel_type & > ( result ) ;
      inner.eval ( cc , result3 ) ;
    }
  }

#ifdef VECTORIZE

  // the vectorized code does precisely the same as the unvectorized code, but
  // since we have conditionals, we need to use masking.

  typedef typename coordinate_v::value_type in_ele_v ;

  void eval ( const coordinate_v & c ,    // simdized nD coordinate
                    pixel4_v & result ) const  // simdized value_type
  {
    pixel_v & result3 = reinterpret_cast < pixel_v & > ( result ) ;

    result = 0.0f ;

    coordinate_v cc ( c ) ;

    if ( shift_horizontally )
    {
      cc [ 0 ] += dx ;
      cc [ 0 ] ( cc [ 0 ] >= upper_horizontal_limt ) -= wrap ;
    }

    if ( do_mask )
    {
      // this mask will be true where there are no image data. We start
      // out all-false:

      in_ele_v::MaskType mask ( false ) ;

      // do_mask is  set, so first we prepare the mask. While we're doing so,
      // we also set coordinates which were out of bounds to zero to avoid
      // evaluation outside the defined range.

      if ( limit_horizontally )
      {
        mask = (   ( cc [ 0 ] <= lower_horizontal_limt )
                 | ( cc [ 0 ] >= upper_horizontal_limt ) ) ;
        cc [ 0 ] ( mask ) = lower_horizontal_limt ;
      }
      if ( limit_vertically )
      {
        mask |= (   ( cc [ 1 ] <= lower_vertical_limit )
                  | ( cc [ 1 ] >= upper_vertical_limit ) ) ;
        cc [ 1 ] ( mask ) = lower_vertical_limit ;
      }
      // if the mask is fully set we can avoid the evaluation altogether.
      if ( ! all_of ( mask ) )
      {
        // if there is unmasked content, honour the alpha
        // channel inside the image, and if there are
        // any non-transparent parts, do inner.eval

        transparency.eval ( cc , result [ 3 ] ) ;

        if ( ! all_of ( result [ 3 ] == 0.0f ) )
        {
          // evaluate and apply the mask to the result

          inner.eval ( cc , result3 ) ;

          // and set all masked values to transparent

          result [ 0 ] ( mask ) = 0.0f ;
          result [ 1 ] ( mask ) = 0.0f ;
          result [ 2 ] ( mask ) = 0.0f ;
          result [ 3 ] ( mask ) = 0.0f ;
        }
      }
    }
    else
    {
      // honour the alpha channel inside the image

      transparency.eval ( cc , result [ 3 ] ) ;

      // if do_mask isn't set, we evaluate unconditionally unless
      // all pixels are transparent

      if ( ! all_of ( result [ 3 ] == 0.0f ) )
        inner.eval ( cc , result3 ) ;
    }
  }

#endif
} ;

/// helper function to determine whether a specific light setting has
/// an effect. This is used to set a boolean flag in the constructor
/// of pixel stage objects taking light settings, see yield_rgb etc.

static bool has_effect ( light_settings_type light_settings )
{
  vigra::TinyVector < double , 3 > factor = light_settings.white_balance ;
  factor *= light_settings.brighten ;
  auto b0 = abs ( light_settings.black_point / factor ) ;
  auto w0 = abs ( light_settings.white_point / factor - 255.0 ) ;
  return ( sum ( b0 ) > .00001 || sum ( w0 ) > .00001 ) ;
}

/// helper function to produce a vspline::domain from a light setting.
/// Such a domain is created by all pixel stage objects taking light settings,
/// even if it's not used (when it has no effect, it's bypassed) - creating
/// it is cheap, so this is not an issue.
/// This domain object combines the overall brightness, white balance and
/// black and white point into a single functor.

static vspline::domain_type < pixel_type , VSIZE >
  make_domain ( light_settings_type light_settings )
{
  typedef vspline::domain_type < pixel_type , VSIZE > domain_t ;

  vigra::TinyVector < double , 3 > factor = light_settings.white_balance ;
  factor *= light_settings.brighten ;

  auto b0 = light_settings.black_point / factor ;
  auto w0 = light_settings.white_point / factor ;

  pixel_type in_low = b0 ;
  pixel_type in_high = w0 ;
  pixel_type out_low ( 0.0f ) ;
  pixel_type out_high ( 255.0f ) ;

  return domain_t ( in_low , in_high , out_low , out_high ) ;
}

/// this object converts 'raw' float RGB pixels to 8bit RGBA which can be fed
/// to SFML's display engine. This saves us an intermediate float RGB image,
/// reducing memory load and traffic. We inherit from class unary_functor
/// to satisfy the interface expected by the transform routines. To further
/// speed up this process, if internal processing is in linear RGB, we
/// convert back to sRGB using fast_RGB2sRGB(), which is slightly less
/// precise but faster, yet more accurate than using a LUT, which would
/// introduce banding due to quantization in some situations.

struct yield_rgba
: public vspline::unary_functor < pixel_type , int , VSIZE >
{
  // we use const bool flags to switch on/off linear processing/brightening.
  // the conditionals on these flags should be optimized away, but I suspect
  // there may be a minimal run-time impact nevertheless. These flags could
  // be made into template arguments. I'm leaving the code as is to keep
  // compile times lower for now - two extra specializations produce a fair
  // amount of machine code bloat.

  const bool apply_gradation ;
  const bool apply_domain ;
  const light_settings_type light_settings ;

  typedef vspline::domain_type < pixel_type , VSIZE > domain_t ;
  domain_t light_domain ;

  yield_rgba ( light_settings_type _light_settings )
  : light_settings ( _light_settings ) ,
    apply_domain ( has_effect ( _light_settings ) ) ,
    apply_gradation ( _light_settings.apply_gradation ) ,
    light_domain ( make_domain ( _light_settings ) )
  { } ;

  /// conversion function yielding 8bit RGBA from float RGB.
  /// the conversion produces RGBA values with no transparency
  /// and uses saturation arithmetics.
  /// If apply_gradation is true, input is taken to be linear RGB.
  /// Conversion is done by means of linear interpolation over a
  /// lookup table, using the globally provided functor
  /// fast_RGB2sRGB. This is faster than applying the 'true'
  /// conversion function with it's log and exp calls and
  /// the difference isn't really noticeable.

  // TODO: sometimes the range-limited (fast) conversion
  // from linear RGB to sRGB is unsuitable and we'd rather
  // use the true conversion, which is no limited to [0:255].

  /// If the user has passed -l on the command line, the incoming
  /// data are in fact sRGB, and applying the linear transformations
  /// given in light_settings is not really mathematically correct,
  /// but it's fast and usually looks 'okay'. Without -l, the
  /// calculation is actually done on linear RGB data and therefore
  /// mathematically correct.
  /// We keep the part of the code which does not involve the
  /// alpha channel in _eval, so we can use it for both alpha-aware
  /// and opaque pixel pipeline code. eval(), below, sets the alpha
  /// channel to 255 and then calls _eval.

  // third stage; limit range and optionally apply RGB to sRGB conversion
  // and convert to 8-bit intensities, pack into 32bit int

  void __eval ( const pixel_type & in ,
                             int & out ) const
  {
    float channel ;
    for ( int ch = 2 ; ch >= 0 ; ch-- )
    {
      channel = in [ ch ] ;
      out <<= 8 ;

      if ( channel > 0.0f )
      {
        // for screen display, we unconditionally cap at 255
        if ( channel > 255.0 )
          out |= 255 ;
        else
        {
          if ( apply_gradation )
          {
            p_srgb->fast_RGB2sRGB.eval ( channel , channel ) ;
          }

          out |= int ( channel ) ;
        }
      }
    }
  } ;

  // second stage: optionally modify light in linear RGB

  void _eval ( const pixel_type & in ,
                            int & out ) const
  {
    if ( apply_domain )
    {
      pixel_type help = in ;
      light_domain.eval ( in , help ) ;
      __eval ( help , out ) ;
    }
    else
    {
      __eval ( in , out ) ;
    }
  }

  // first stage: set alpha channel to opaque

  void eval ( const pixel_type & in ,
                           int & out ) const
  {
    out = 255 ; // alpha channel
    _eval ( in , out ) ;
  }

#ifdef VECTORIZE

  typedef typename vspline::vector_traits < int , VSIZE > :: ele_v value_v ;
  typedef typename vspline::vector_traits < int , VSIZE > :: type out_v ;

  // now the vectorized variant of eval. To satisfy the unary_functor
  // interface, we use out_v as out type, even though it is
  // 'technically' a single-channel value (RGBA is packed into one int),
  // so our out type comes out as a vigra::TinyVector of one simdized int
  // and we have to select it's first and only component by indexing with
  // zero, which the optimizer (hopefully) optimizes away.
  // The stages are as above, but since we're dealing with vectors, we
  // use masking instead of conditionals.

  void __eval ( const pixel_v & in ,
                      value_v & out ) const
  {
    channel_v channel ;
    for ( int ch = 2 ; ch >= 0 ; ch-- )
    {
      channel = in [ ch ] ;

      // if we have incoming NaN values, the 'inverted' test will
      // set them to zero, whereas testing for channel < 0 will
      // carry the NaN through. This is defensive - NaNs will crash
      // the fast_RGB2sRGB invocation below - but the NaNs should
      // not be there in the first place, I just haven't yet figured
      // out where they happen.

      channel ( ! ( channel >= 0.0f ) ) = 0.0f ;

      // was:

//       channel ( channel < 0.0f ) = 0.0f ;

      // for screen display, we unconditionally cap at 255

      channel ( channel > 255.0f ) = 255.0f ;

      if ( apply_gradation )
      {
        p_srgb->fast_RGB2sRGB.eval ( channel , channel ) ;
      }

      out <<= 8 ;

      out |= value_v ( channel ) ;
    }
  } ;

  void _eval ( const pixel_v & in ,
                     value_v & out ) const
  {
    if ( apply_domain )
    {
      pixel_v help ;
      light_domain.eval ( in , help ) ;
      __eval ( help , out ) ;
    }
    else
    {
      __eval ( in , out ) ;
    }
  }

  void eval ( const pixel_v & in ,
                    value_v & out ) const
  {
    out = 255 ; // set alpha channel to fully opaque
    _eval ( in , out ) ;
  }

#endif
} ;

/// yield_rgba4 is a variation on yield_rgba, where we have incoming
/// float RGBA pixels. Here, instead of setting the alpha channel to
/// 255 (no transparency), we use the alpha value provided by the
/// incoming pixel. Note that clear_before_draw should be set to
/// 'true' when handling data with alpha channel.
/// This mode of postprocessing produces data for on-screen display,
/// like the one above. The processing of the RGB part is delegated
/// to yield_rgb.

struct yield_rgba4
: public vspline::unary_functor < pixel4_type , int , VSIZE >
{
  yield_rgba handle_rgb ; // functor handling the RGB part

  yield_rgba4 ( light_settings_type light_settings )
  : handle_rgb ( light_settings )
  { } ;

  void eval ( const pixel4_type & i_result ,
                            int & result ) const
  {
    const pixel_type & rgb_part =
      reinterpret_cast < const pixel_type & > ( i_result ) ;

    result = i_result[3] ; // transfer alpha channel to result
    handle_rgb._eval ( rgb_part ,  result ) ; // delegate RGB processing
  } ;

#ifdef VECTORIZE

  typedef typename vspline::vector_traits < int , VSIZE > :: ele_v value_v ;
  typedef typename vspline::vector_traits < int , VSIZE > :: type out_v ;

  void eval ( const pixel4_v & i_result ,
                    value_v & result ) const
  {
    const pixel_v & rgb_part =
      reinterpret_cast < const pixel_v & > ( i_result ) ;

    result = i_result[3] ;
    handle_rgb._eval ( rgb_part ,  result ) ; // delegate RGB processing
  } ;

#endif
} ;

/// variant of the last stage of the pixel pipeline yielding float RGB
/// output. The code is roughly the same as above, only the conversion
/// to unsigned char is omitted, and the output has only three channels

struct yield_float_rgb
: public vspline::unary_functor < pixel_type , pixel_type , VSIZE >
{
  // we use const bool flags to switch on/off linear processing/brightening.
  // the conditionals on these flags should be optimized away, but I suspect
  // there may be a minimal run-time impact nevertheless. These flags could
  // be made into template arguments. I'm leaving the code as is to keep
  // compile times lower for now - two extra specializations produce a fair
  // amount of machine code bloat.

  const bool apply_gradation ;
  const bool apply_domain ;
  const bool cap_brightness ;
  const light_settings_type light_settings ;

  typedef vspline::domain_type < pixel_type , VSIZE > domain_t ;
  domain_t light_domain ;

  yield_float_rgb ( light_settings_type _light_settings )
  : light_settings ( _light_settings ) ,
    apply_domain ( has_effect ( _light_settings ) ) ,
    apply_gradation ( _light_settings.apply_gradation ) ,
    cap_brightness ( _light_settings.cap_brightness ) ,
    light_domain ( make_domain ( _light_settings ) )
  { } ;

  /// conversion function yielding float RGB from float RGB,
  /// applying optional brightness correction, conversion from
  /// linear RGB to sRGB and range clipping to [0,255]
  /// the conversion produces RGB values with no transparency
  /// and uses saturation arithmetics.
  /// If apply_gradation is true, input is taken to be linear RGB.
  /// Conversion is done by means of linear interpolation over a
  /// lookup table, using the globally provided functor
  /// fast_RGB2sRGB. This is faster than applying the 'true'
  /// conversion function with it's log and exp calls and
  /// the difference isn't really noticeable.
  /// If the user has passed -l on the command line, the incoming
  /// data are in fact sRGB, and applying the linear transformations
  /// given in light_settings is not really mathematically correct,
  /// but it's fast and usually looks 'okay'. Without -l, the
  /// calculation is actually done on linear RGB data and therefore
  /// mathematically correct.

  void _eval ( const pixel_type & in ,
               pixel_type & out ) const
  {
    float channel ;

    for ( int ch = 0 ; ch < 3 ; ch++ )
    {
      channel = in [ ch ] ;

      if ( channel < 0.0f )
      {
        channel = 0.0f ;
      }
      else
      {
        if ( cap_brightness && ( channel > 255.0f ) )
        {
          channel = 255.0f ;
        }
        else
        {
          if ( apply_gradation )
          {
//             p_srgb->fast_RGB2sRGB.eval ( channel , channel ) ;
            channel /= 255.0f ;
            if ( channel <= 0.0031308f ) 
              channel *= 255.0f * 12.92f ;
            else
              channel = 255.0f * (   1.055f
                                   * std::pow ( channel , 0.41666666666666667f )
                                   - 0.055f ) ;
          }
        }
      }
      out [ ch ] = channel ;
    }
  } ;

  void eval ( const pixel_type & in ,
                    pixel_type & out ) const
  {
    if ( apply_domain )
    {
      pixel_type help = in ;
      light_domain.eval ( in , help ) ;
      _eval ( help , out ) ;
    }
    else
    {
      _eval ( in , out ) ;
    }
  }

#ifdef VECTORIZE

  typedef typename vspline::vector_traits < float , VSIZE > :: ele_v channel_v ;

  void _eval ( const pixel_v & in ,
               pixel_v & out ) const
  {
    channel_v channel ;

    for ( int ch = 0 ; ch < 3 ; ch++ )
    {
      channel = in [ ch ] ;

      channel ( channel < 0.0f ) = 0.0f ;

      if ( cap_brightness )
        channel ( channel > 255.0f ) = 255.0f ;

      if ( apply_gradation )
      {
//         p_srgb->fast_RGB2sRGB.eval ( channel , channel ) ;
        channel = RGB2sRGB ( channel , 255.0f ) ;
      }

      out [ ch ] = channel ;
    }
  } ;

  void eval ( const pixel_v & in ,
                    pixel_v & out ) const
  {
    if ( apply_domain )
    {
      pixel_v help ;
      light_domain.eval ( in , help ) ;
      _eval ( help , out ) ;
    }
    else
    {
      _eval ( in , out ) ;
    }
  }

#endif
} ;

/// variant on yield_float_rgb taking pixels with alpha channel. The alpha
/// channel is merely passed through unchanged, the RGB part is processed
/// by an internal yield_float_rgb object

struct yield_float_rgba
: public vspline::unary_functor < pixel4_type , pixel4_type , VSIZE >
{
  yield_float_rgb handle_rgb ;

  yield_float_rgba ( light_settings_type light_settings )
  : handle_rgb ( light_settings )
  { } ;

  void eval ( const pixel4_type & i_result ,
                    pixel4_type & result ) const
  {
    const pixel_type & rgb_part_in =
      reinterpret_cast < const pixel_type & > ( i_result ) ;

    pixel_type & rgb_part_out =
      reinterpret_cast < pixel_type & > ( result ) ;

    result[3] = i_result[3] ;
    handle_rgb.eval ( rgb_part_in , rgb_part_out ) ;
  } ;

#ifdef VECTORIZE

  typedef typename vspline::vector_traits < int , VSIZE > :: ele_v value_v ;
  typedef typename vspline::vector_traits < int , VSIZE > :: type out_v ;

  void eval ( const pixel4_v & i_result ,
                    pixel4_v & result ) const
  {
    const pixel_v & rgb_part_in =
      reinterpret_cast < const pixel_v & > ( i_result ) ;

    pixel_v & rgb_part_out =
      reinterpret_cast < pixel_v & > ( result ) ;

    result[3] = i_result[3] ;
    handle_rgb.eval ( rgb_part_in , rgb_part_out ) ;
  } ;

#endif
} ;

// helper function to convert any angle to the range from 0-360

template < class real_type >
void cage_angle ( real_type & angle )
{
  angle = fmod ( angle , 2.0 * M_PI ) ;
  if ( angle < 0.0 )
    angle += 2.0 * M_PI ;
}

// struct capture_type is used to capture the result of a coordinate
// transformation to reuse it if technically possible. The process
// is quite straightforward: The functor affecting the coordinate
// transformation is embedded into the capture_type object as 'inner'
// functor. the resulting object is used in the processing chain
// instead of the 'naked' tansformation, resulting in the data being
// captured to the capture_type's designated storage location, which
// is fixed at it's creation.

// capture_type expects incoming 2D discrete coordinates, as generated
// by a transform. It's output are 2D float coordinates, the same as
// inner-type's output.

// Note hat the vectorized eval variant uses 'fluff' which requires
// receiving memory large enough to accomodate an entire vectorized
// datum. When using vspline as the transform backend, this is always
// the case ('leftover' is processed with scalar code after peeling),
// but when using zimt, the coordinate storage has to have a line size
// (dimension zero) which is a multiple of VSIZE. Since this latter
// requirement does little harm (it may waste a bit of memory) the
// code using capture_type will provide correctly padded memory, so
// the pipeline can be run with vspline or zimt.

template < class inner_type >
struct capture_type
: public vspline::unary_functor < int2_type , coordinate_type , VSIZE >
{
  // type of storage for transformed coordinates

  typedef vigra::MultiArrayView < 2 , coordinate_type > store_type ;

  // stride of the storage

  int2_type stride ;

  // the storage itself, held per reference, so we can assign

  store_type & store ;

  // 'inner' holds the coordinate transformation functor

  inner_type inner ;

  capture_type ( store_type & _store ,
                 const inner_type & _inner )
  : store ( _store ) ,
    stride ( _store.stride() ) ,
    inner ( _inner )
  { } ;

  // single-value evaluation is straightforward:

  void eval ( const int2_type & in ,
              coordinate_type & out ) const
  {
    // move from discrete coordinate to float coordinate

    coordinate_type c ( in ) ;

    // perform the coordinate transformation

    inner.eval ( c , out ) ;

    // store the transformed coordinate to the storage area.

    store [ in ] = out ;
  }

#ifdef VECTORIZE

  typedef vspline::unary_functor < int2_type , coordinate_type , VSIZE >
    base_type ;

  // for vectorized evaluation, we first need a few extra types:

  using base_type::in_v ;     // vectorized incoming discrete 2D coordinate
  using base_type::in_ele_v ; // type for 'condensed' coordinates
  using base_type::out_v ;    // vectorized 2D float coordinate

  void eval ( const in_v & in ,
              out_v & out ) const
  {
    // move from 2D coordinates to float coordinates

    out_v c ( in ) ;

    // perform the coordinate transformation

    inner.eval ( c , out ) ;

    // 'condense' the 2D coordinates into offsets in memory

    int offset = in[0][0] * stride[0] + in[1][0] * stride[1] ;

    // and use these offsets to deposit the transformed
    // coordinates to the storage area.

    wielding::fluff ( out , store.data() + offset , 1 ) ;
  }

#endif
} ;

// very simple global tonemapping operator. The parameters are chosen
// to force brightness of ca. 1023 down to 255. This is a resonable
// guess: the incoming range 0-1023 is what pv's HDR blending produces
// for +/-2Ev brackets. This operator should be applied in linear light.
// Lower brightness values get subdued slightly, but I find the overall
// effect acceptable.

struct tonemap_type
: public tf33_type
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out[0] = 318.0f * ( in[0] / ( in[0] + 255.0f ) ) ;
    out[1] = 318.0f * ( in[1] / ( in[1] + 255.0f ) ) ;
    out[2] = 318.0f * ( in[2] / ( in[2] + 255.0f ) ) ;
  }
} ;

struct tonemap4_type
: public tf44_type
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out[0] = 318.0f * ( in[0] / ( in[0] + 255.0f ) ) ;
    out[1] = 318.0f * ( in[1] / ( in[1] + 255.0f ) ) ;
    out[2] = 318.0f * ( in[2] / ( in[2] + 255.0f ) ) ;
    out[3] = in[3] ;
  }
} ;

// apply shader. This shading function is for jobs without alpha channel.
// We have two possible target types: a float RGB frame used for white
// balance calculations, or an unsigned char RGBA frame to be used as a
// texture for on-screen display.

void shade ( const ev_type & shader ,
             const job_type * const p_job )
{
  const frame_type & frame ( p_job->frame ) ; // short identifier
  const target_type & target ( p_job->target ) ; // short identifier

  auto njobs = p_job->njobs ;
  if ( njobs <= 0 )
    njobs = vspline::default_njobs ;

  assert ( frame.format != FRAME_FRGBA ) ;

  if ( frame.format == FRAME_FRGB )
  {
    yield_float_rgb to_float_rgb ( frame.light_settings ) ;

    assert ( p_job->frame.p_frgb != nullptr ) ;
    auto sz = p_job->frame.p_frgb->shape() ;

    if ( target.cropping_active )
    {
      assert ( sz[0] == target.crop_width ) ;
      assert ( sz[1] == target.crop_height ) ;
    }
    else
    {
      assert ( sz[0] == target.width ) ;
      assert ( sz[1] == target.height ) ;
    }

    if ( target.cropping_active )
    {
      shift_t shift ( coordinate_type ( target.crop_x , target.crop_y ) ) ;
      if ( p_job->frame.tonemap )
      {
        vspline::transform ( shift + shader + tonemap_type() + to_float_rgb ,
                            *(p_job->frame.p_frgb) ,
                            njobs ) ;
      }
      else
      {
        vspline::transform ( shift + shader + to_float_rgb ,
                            *(p_job->frame.p_frgb) ,
                            njobs ) ;
      }
    }
    else
    {
      if ( p_job->frame.tonemap )
      {
        vspline::transform ( shader + tonemap_type() + to_float_rgb ,
                            *(p_job->frame.p_frgb) ,
                            njobs ) ;
      }
      else
      {
        vspline::transform ( shader + to_float_rgb ,
                            *(p_job->frame.p_frgb) ,
                            njobs ) ;
      }
    }
  }
  else
  {
    assert ( frame.p_itp->mode == NO_ALPHA ) ;

    assert ( p_job->frame.p_frame != nullptr ) ;

    int_array_view int_view ( frame.p_frame->shape() ,
                              (int*) ( frame.p_frame->data() ) ) ;

    // the last stage of the pixel pipeline converts the raw pixel data to
    // 8bit RGBA texture data to be passed to openGL:

    yield_rgba to_rgba ( frame.light_settings ) ;

    // finally, call vspline::transform to roll out the pixel pipeline

    if ( p_job->frame.tonemap )
    {
      vspline::transform ( shader + tonemap_type() + to_rgba ,
                          int_view ,
                          njobs ) ;
    }
    else
    {
      vspline::transform ( shader + to_rgba ,
                          int_view ,
                          njobs ) ;
    }
  }
}

// special case for mask generation: the evaluator is an alpha_ev_type
// yielding single float values.

void shade1 ( const alpha_ev_type & shader ,
              const job_type * const p_job )
{
  const frame_type & frame ( p_job->frame ) ; // short identifier
  const target_type & target ( p_job->target ) ; // short identifier

  auto njobs = p_job->njobs ;
  if ( njobs <= 0 )
    njobs = vspline::default_njobs ;

  assert ( frame.format == FRAME_MASK ) ;
  assert ( p_job->frame.p_mask != nullptr ) ;

  auto sz = p_job->frame.p_mask->shape() ;

  if ( target.cropping_active )
  {
    assert ( sz[0] == target.crop_width ) ;
    assert ( sz[1] == target.crop_height ) ;
  }
  else
  {
    assert ( sz[0] == target.width ) ;
    assert ( sz[1] == target.height ) ;
  }

  if ( target.cropping_active )
  {
    shift_t shift ( coordinate_type ( target.crop_x , target.crop_y ) ) ;
      vspline::transform ( shift + shader ,
                           *(p_job->frame.p_mask) ,
                           njobs ) ;
  }
  else
  {
    vspline::transform ( shader ,
                         *(p_job->frame.p_mask) ,
                         njobs ) ;
  }
}

// for jobs with alpha channel, we use shade4 for the final step:
// the shader is passed to vspline::transform to populate the target
// array. Depending on the precise natue of the job, the target array
// can hold either float RGBA or unsigned char RGBA

void shade4 ( const ev4_type & shader ,
              const job_type * const p_job )
{
  const frame_type & frame ( p_job->frame ) ; // short identifier
  const target_type & target ( p_job->target ) ; // short identifier

  auto njobs = p_job->njobs ;
  if ( njobs <= 0 )
    njobs = vspline::default_njobs ;

  assert ( frame.format != FRAME_FRGB ) ;

  if ( frame.format == FRAME_FRGBA )
  {
    assert ( p_job->frame.p_frgba != nullptr ) ;
    auto sz = p_job->frame.p_frgba->shape() ;

    if ( target.cropping_active )
    {
      assert ( sz[0] == target.crop_width ) ;
      assert ( sz[1] == target.crop_height ) ;
    }
    else
    {
      assert ( sz[0] == target.width ) ;
      assert ( sz[1] == target.height ) ;
    }

    yield_float_rgba to_float_rgba ( frame.light_settings ) ;

    if ( target.cropping_active )
    {
      shift_t shift ( coordinate_type ( target.crop_x , target.crop_y ) ) ;
      if ( p_job->frame.tonemap )
        vspline::transform ( shift + shader + tonemap4_type() + to_float_rgba ,
                            *(p_job->frame.p_frgba) ,
                            njobs ) ;
      else
        vspline::transform ( shift + shader + to_float_rgba ,
                            *(p_job->frame.p_frgba) ,
                            njobs ) ;
    }
    else
    {
      if ( p_job->frame.tonemap )
        vspline::transform ( shader + tonemap4_type() + to_float_rgba ,
                            *(p_job->frame.p_frgba) ,
                            njobs ) ;
      else
        vspline::transform ( shader + to_float_rgba ,
                            *(p_job->frame.p_frgba) ,
                            njobs ) ;
    }
  }
  else
  {
    assert ( p_job->frame.p_frame != nullptr ) ;

    int_array_view int_view ( frame.p_frame->shape() ,
                              (int*) ( frame.p_frame->data() ) ) ;

     // the last stage of the pixel pipeline converts the raw pixel data to
     // 8bit RGBA texture data to be passed to openGL:

     yield_rgba4 to_rgba ( frame.light_settings ) ;

     // finally, call vspline::transform to roll out the pixel pipeline

    if ( p_job->frame.tonemap )
      vspline::transform ( shader + tonemap4_type() + to_rgba ,
                            int_view ,
                            njobs ) ;
    else
      vspline::transform ( shader + to_rgba ,
                            int_view ,
                            njobs ) ;
  }
}

// when a job is received by an interpolator object, it creates a
// run_type object for the specific invocation. This object builds
// up state for the 'run' and offers facilities to make use of this
// state. The run_type object hides the specifics of building up the
// state for specific interpolators and provides a common interface.
// This is the base class of all runner objects.

struct run_type_base
{
  // some runners don't contain any shaders, e.g. those jobs which
  // only provide statistical information (BLEND_CORRELATE). For
  // such jobs, 'render' will be false.

  bool render ;

  // if set, runner is for alpha channel pipeline (ev4), otherwise
  // without alpha (ev)

  bool alpha ;

  // shaders for single-pixel/vectorized pixel operation

  ev_type ev ;
  ev4_type ev4 ;

} ;

// version of shade which accepts a reference to a run_type_base object,
// dispatching to shade/shade4

void shade ( const run_type_base & runner , job_type * p_job )
{
  if ( runner.render == false )
  {
//     std::cout << "****** non-rendering shader" << std::endl ;
    return ;
  }

  if ( runner.alpha )
  {
//     std::cout << "****** rendering shader, alpha = true" << std::endl ;
    shade4 ( runner.ev4 , p_job ) ;
  }
  else
  {
//     std::cout << "****** rendering shader, alpha = false" << std::endl ;
    shade ( runner.ev , p_job ) ;
  }
}

// fade_out_type wraps an alpha functor, producing a new functor which
// lowers alpha for marginal pixels. The fade-out is over a width of
// 'threshold' in model space units.

template < typename dry_alpha_type >
struct fade_out_type
: public tf21_type
{
  const dry_alpha_type dry_alpha ;
  const float threshold ;
  const extent_type extent ;

  fade_out_type ( const dry_alpha_type & _dry_alpha ,
                  const float & _threshold ,
                  const extent_type & _extent )
  : dry_alpha ( _dry_alpha ) ,
    threshold ( _threshold ) ,
    extent ( _extent )
  { }

  // by coding the marginality tests with std::min/std::max, we can
  // use a single implementation for both single-pixel and vectorized
  // operation:

  template < typename in_type , typename out_type >
  void eval ( const in_type & crd , out_type & alpha ) const
  {
    out_type factor ( threshold ) ;
    typedef decltype ( crd[0] ) c_type ;

    factor = vspline::min ( factor , c_type ( crd[0] - extent.x0 ) ) ;
    factor = vspline::min ( factor , c_type ( extent.x1 - crd[0] ) ) ;
    factor = vspline::min ( factor , c_type ( crd[1] - extent.y0 ) ) ;
    factor = vspline::min ( factor , c_type ( extent.y1 - crd[1] ) ) ;
    factor = vspline::max ( factor , c_type ( 0.0f ) ) ;
    factor /= threshold ;

    // 'factor' is now in the range of [0,1], so we can directly multiply
    // the alpha channel with it. For coordinates outside the extent,
    // 'factor' will be zero, so they result will be totally transparent,
    // which is fine, and for non-marginal coordinates, 'factor' is 1.0,
    // which will preserve the alpha channel unmodified.

    // obtain the 'dry' alpha channel

    dry_alpha.eval ( crd , alpha ) ;

    // apply the factor

    alpha *= factor ;
  }
} ;

template < typename dry_alpha_type >
fade_out_type < dry_alpha_type >
make_fader ( const dry_alpha_type & a ,
             const float & t ,
             const extent_type & e )
{
  return fade_out_type < dry_alpha_type > ( a , t , e ) ;
}

std::atomic<int> counter ( 0 ) ;

// class interpolator provides an elaborate way of accessing image data:
// initially, a raw_image_type object is created. This object does already
// provide access to the image data in preprocessed form; it offers functors
// yielding the image data in float, with the necessary bias and sRGB/RGB
// conversion built in. The degree-1 functors offered by raw_image_type
// are suitable for use by lux' display engine, but their performance is
// not optimal, due to the processing taking place in the functors. But
// it's a good way to give the user 'something to look at' immediately,
// and this is what class interpolator does: as soon as the raw_image_type
// object is fully constructed, the interpolator object is operational,
// at this stage by merely inserting a suitable functor from the
// raw_image_type object into the pixel pipeline. The more elaborate
// interpolators (one or two image pyramids containing b-splines) are
// then built in a separate thread and are inserted into the pixel
// pipeline once they become available. Once the 'sophisticated' functors
// are fully on-line, the 'primal' functors are disabled and any resources
// they have bound (like the raw image data) are freed.
// Concerning the mechanism of integrating the functors into the pixel
// pipeline, see the documentation of class interpolator_base.

struct interpolator
: public interpolator_base
{
  // we use the same type system as raw_image_type

  typedef vigra::TinyVector < float , 2 > coordinate_type ;
  typedef vigra::TinyVector < int , 2 > discrete_coordinate_type ;
  typedef vigra::TinyVector < float , 3 > pixel_type ;
  typedef vspline::grok_type < discrete_coordinate_type , pixel_type , VSIZE > d_ev_type ;
  typedef vspline::grok_type < discrete_coordinate_type , float , VSIZE > d_alpha_ev_type ;

  fileio::image_info imageInfo ;

  int exif_orientation ;

  int lq_degree ;
  int hq_degree ;

  double scaling_step ;
  int pyramid_bias ;
  int smoothing_level ;
  int cbf_degree ;
  int headroom ;
  int floor ;
  using interpolator_base::subimage ;

  ev_type primal_ev ;
  ev_type primal_ev2 ;
  alpha_ev_type primal_alpha_ev ;
  std::shared_ptr < raw_image_type > ps_raw_image ;

  typedef typename vigra::TinyVector < vspline::xlf_type , 2 > limit_type ;

  // KFJ 2023-12-20 changing from domain_type to cdomain_type here,
  // to avoid out-of-range errors: these are fatal for coordinates
  // used for b-spline access, and since the factors involved to
  // convert model space units to spline coordinates can become
  // rather large, this looks like a sensible precaution: I've had
  // reports of crashes which seem to have originated from just such
  // range excessions.
  // TODO: is this a quick shot? Maybe it's better to avoid the
  // cdomain_type and instead use an evaluator with a forced clamp
  // gate.

  typedef typename vspline::cdomain_type < coordinate_type ,
                                           VSIZE > domain_t ;

  limit_type lower_limit ;
  limit_type upper_limit ;

  typedef vspline::bspline < float , 2 > alpha_spline_type ;

  std::atomic < alpha_spline_type * > p_alpha ;
  std::shared_ptr < pixel_pyramid_type > p_pyramid ;
  std::shared_ptr < pixel4_pyramid_type > p_pyramid4 ;

  spline_type * p_lq_spline = nullptr ;
  spline_type * p_hq_spline = nullptr ;
  spline4_type * p_lq_spline4 = nullptr ;
  spline4_type * p_hq_spline4 = nullptr ;

  int lq_shift = 0 ;
  int hq_shift = 0 ;

  std::thread * p_builder = 0 ;

  bool is_periodic ;
  bool process_linear ;
  alpha_mode initial_mode ;
  using interpolator_base::mode ;
  bool grey_edge ;
  bool build_pyramids ;
  bool build_raw_pyramids ;

  source_type source ;

  // source_compatible is a helper routine which determines whether
  // two source_type objects a and b coincide sufficiently to use one
  // instead of the other - this is factored out from 'rebuild', below

  bool source_compatible ( const source_type & a , const source_type & b )
  {
    if ( a.width != b.width )
      return false ;
    if ( a.height != b.height )
      return false ;
    if ( a.exif_orientation != b.exif_orientation )
      return false ;
    if ( a.full_width != b.full_width )
      return false ;
    if ( a.full_height != b.full_height )
      return false ;
    if ( a.is_linear != b.is_linear )
      return false ;
    // full sphericals use a special prefilter, so if a is a full
    // spherical and b is not spherical, a and b are incompatible.
    if ( ( a.projection == SPHERICAL ) && a.full_height )
    {
      if ( b.projection != SPHERICAL )
        return false ;
    }
    if ( ( b.projection == SPHERICAL ) && b.full_height )
    {
      if ( a.projection != SPHERICAL )
        return false ;
    }
    return true ;
  }

  // rebuild takes the same set of parameters which class interpolator's
  // c'tor requires. It tests, whether the interpolator object on which
  // it is invoked (the 'current' interpolator) can be recycled to
  // realize the given parameter set. This is possible if several
  // conditions are met, like, it's for the same image file etc...
  // if the current interpolator can be recycled, rebuild performs the
  // necessary actions and returns true, if not, it returns false.

  bool rebuild ( fileio::image_info _imageInfo ,
                 const source_type & _source ,
                 bool _process_linear ,
                 alpha_mode _mode ,
                 bool _grey_edge ,
                 int _lq_degree ,
                 int _hq_degree ,
                 double _scaling_step ,
                 int _smoothing_level ,
                 int _cbf_degree ,
                 int _floor ,
                 bool _build_pyramids ,
                 bool _build_raw_pyramids ,
                 int _subimage 
               )
  {
    bool compatible = true ;
    // bool do_lq_shift = false ;
    // bool do_hq_shift = false ;
    bool apply_ftg = false ;

    // this test is a bit more involved and has been factored out,
    // since it may return true, we put it first.

    compatible = source_compatible ( _source , source ) ;
    compatible &= ( _subimage == subimage ) ;

    // make sure we're dealing with the same file

    if ( std::strcmp ( _imageInfo.getFileName() , imageInfo.getFileName() ) )
      compatible = false ;

    if ( _process_linear != process_linear )
      compatible = false ;

    if ( _mode != initial_mode ) // TODO bit harsh, might still be compatible
      compatible = false ;

    // degree 0 and 1 are compatible via shifting:
    // TODO: this doesn't work currently, so I switch off this
    // optimization for the time being.

    if ( _hq_degree != hq_degree )
    {
      // if ( hq_degree < 2 && _hq_degree < 2 )
      //   do_hq_shift = true ;
      // else
        compatible = false ;
    }

    if ( _lq_degree != lq_degree )
    {
      // if ( lq_degree < 2 && _lq_degree < 2 )
      //   do_lq_shift = true ;
      // else
        compatible = false ;
    }

    if ( _scaling_step != scaling_step )
      compatible = false ;

    if ( _smoothing_level != smoothing_level )
      compatible = false ;

    if ( _cbf_degree != cbf_degree )
      compatible = false ;

    if ( _floor != floor )
      compatible = false ;

    if ( _build_pyramids != build_pyramids )
      compatible = false ;

    if ( _build_raw_pyramids != build_raw_pyramids )
      compatible = false ;

    // so far, no obstacles to recycling have been detected. But there
    // may be some actions we have to perform to put the interpolator into
    // the state which is required:

    if ( compatible && ( _grey_edge != grey_edge ) )
    {
      if ( _grey_edge )
        apply_grey_edge() ;
      else
        apply_grey_edge ( true ) ; // true is for 'revert' parameter
      grey_edge = _grey_edge ; // now save the new mode
    }

    // test for difference in brightness, modify if necessary

    if ( compatible && ( _source.brightness != source.brightness ) )
    {
      if ( ! ( build_pyramids && _build_pyramids ) )
      {
        // we can only adapt to a new brightness value if we have
        // image pyramids - otherwise the brightness is encoded as
        // a part of the functor 'presenting' the raw image data,
        // and we can't get 'inside' it

        compatible = false ;
      }
      else if ( _source.brightness != source.brightness )
      {
        float factor = float ( _source.brightness / source.brightness ) ;
        pixel4_type factor4 { factor , factor , factor , 1.0f } ;

        // apply the factor to the image pyramids

        std::cout << "facet brightness; old: " << source.brightness
                  << " new: " << _source.brightness << std::endl ;

        std::cout << "multiplying image data with "
                  << factor << std::endl ;

        if ( p_pyramid )
        {
          auto psz = p_pyramid->level.size() ;
          auto p_top = p_pyramid->level[psz-1] ;
          auto & px ( p_top->core [ {  0, 0 } ] ) ;
          for ( auto p_bspl : p_pyramid->level )
            p_bspl->container *= pixel_type(factor) ;
        }

        if ( p_pyramid4 )
        {
          auto psz = p_pyramid4->level.size() ;
          auto p_top = p_pyramid4->level[psz-1] ;
          auto & px ( p_top->core [ {  0, 0 } ] ) ;
          for ( auto p_bspl : p_pyramid4->level )
            p_bspl->container *= factor4 ;
        }

        // now do the same for the interpolators

        if ( p_lq_spline )
          p_lq_spline->container *= pixel_type(factor) ;

        if ( p_hq_spline && ( p_hq_spline != p_lq_spline ) )
          p_hq_spline->container *= pixel_type(factor) ;

        if ( p_lq_spline4 )
          p_lq_spline4->container *= factor4 ;

        if ( p_hq_spline4 && ( p_hq_spline4 != p_lq_spline4 ) )
          p_hq_spline4->container *= factor4 ;
      }
    }

    // degree 0 and 1 are compatible via a simple shift:
    // currently disabled, code has issues

    // if ( compatible && do_lq_shift )
    // {
    //   this->shift ( 0 , _lq_degree - lq_degree ) ;
    //   // this->lq_degree = _lq_degree ;
    // }
    // 
    // if ( compatible && do_hq_shift )
    // {
    //   this->shift ( 1 , _hq_degree - hq_degree ) ;
    //   // this->hq_degree = _hq_degree ;
    // }

    // finally, if compatible is true, we take over the new source object
    // and return true.

    if ( compatible )
    {
      this->source = _source ;
      return true ;
    }

    // otherwise we fail:

    return false ;
  }

  // class interpolator's c'tor takes the necessary information to create
  // a raw_image_type object, plus the source image's metrics.
  // spline_degree is the caller's intended spline degree, but if there is
  // not enough memory to create float-based interpolators, this is not
  // honoured: we work on a best-effort base. The least we'll provide is
  // a single bilinear interpolator directly on the raw data which is
  // returned for every scale. If circumstances allow, we'll provide better.
  // TODO: establish memory budget, avoid swapping

  interpolator ( fileio::image_info _imageInfo ,
                 const source_type & _source ,
                 bool _process_linear ,
                 alpha_mode _mode ,
                 bool _grey_edge ,
                 int _lq_degree ,
                 int _hq_degree ,
                 double _scaling_step ,
                 int _smoothing_level ,
                 int _cbf_degree ,
                 int _floor ,
                 bool _build_pyramids ,
                 bool _build_raw_pyramids ,
                 int _subimage
               )
  : imageInfo ( _imageInfo ) ,
    source ( _source ) ,
    process_linear ( _process_linear ) ,
    grey_edge ( _grey_edge ) ,
    initial_mode ( _mode ) ,
    lq_degree ( _lq_degree ) ,
    hq_degree ( _hq_degree ) ,
    scaling_step ( _scaling_step ) ,
    smoothing_level ( _smoothing_level ) ,
    cbf_degree ( _cbf_degree ) ,
    floor ( _floor ) ,
    build_pyramids ( _build_pyramids ) ,
    build_raw_pyramids ( _build_raw_pyramids )
  {
    subimage = _subimage ;
    own_type = SINGLE_ITP ;
    exif_orientation = source.exif_orientation ,
    is_periodic = source.full_width ;
    stage = 0 ; // not yet operational
    pyramid_bias = 0 ;

    headroom = ( smoothing_level + 1 ) / 2 ;

    if ( smoothing_level == -1 )
      headroom = 1 ;
    else if ( smoothing_level == -2 )
      headroom = 1 ;
    else if ( smoothing_level == -3 )
      headroom = 2 ;
    else if ( smoothing_level == -4 )
      headroom = 2 ;
    else if ( smoothing_level <= -5 )
    {
      int ksz = - smoothing_level ;
      headroom = ( ksz - 1 ) / 2 ;
    }

    headroom += SHIFT_CEIL ;

    // first we create a raw_image_type object. This object loads the image
    // data from disk and provides 'primal' interpolators, which are already
    // good enough for most purposes.

    ps_raw_image = std::make_shared < raw_image_type >
                     ( imageInfo ,
                       source ,
                       process_linear ,
                       grey_edge ,
                       _mode ,
                       build_raw_pyramids ,
                       _scaling_step ,
                       _smoothing_level ,
                       _floor ) ;

    raw_image_type & raw_image ( *ps_raw_image ) ;

    // raw_image boils the mode down to WITH_ALPHA or NO_ALPHA:

    mode = raw_image.mode ;

    primal_ev = raw_image.primal_ev[1] ;
    primal_ev2 = raw_image.primal_ev[2] ;
    primal_alpha_ev = raw_image.primal_alpha_ev1 ;

    lower_limit = raw_image.lower_limit ;
    upper_limit = raw_image.upper_limit ;

    p_alpha = 0 ;
    p_pyramid = 0 ;

    stage = 1 ; // primal evaluators operational

    if ( build_pyramids )
    {
      auto payload = std::bind ( &interpolator::build ,
                                 this ,
                                 source.squash ) ;

      p_builder = mem_in() << new std::thread ( payload ) ;
    }
    else
    {
      stage = 3 ; // already deemed fully operational
    }
  }

  // class interpolator's d'tor joins and deletes the builder thread

  ~interpolator()
  {
    if ( p_builder )
    {
      p_builder->join() ;
      memlog >> p_builder ;
    }

    if ( p_alpha )
      memlog >> p_alpha.load() ;

    if ( p_hq_spline && ( p_hq_spline != p_lq_spline ) )
      memlog >> p_hq_spline ;

    if ( p_lq_spline )
      memlog >> p_lq_spline ;

    if ( p_hq_spline4 && ( p_hq_spline4 != p_lq_spline4 ) )
      memlog >> p_hq_spline4 ;

    if ( p_lq_spline4 )
      memlog >> p_lq_spline4 ;
  }

  void shift ( int quality , int by )
  {
    if ( quality == 1 )
    {
      int proposed = hq_degree + hq_shift + by ;
      if ( proposed >= 0 && proposed <= 7 )
        hq_shift += by ;
    }

    else if ( quality == 0 )
    {
      int proposed = lq_degree + lq_shift + by ;
      if ( proposed >= 0 && proposed <= 7 )
        lq_shift += by ;
    }
  }

  void apply_grey_edge ( bool revert = false )
  {
    // we modify the image or alpha data to optionally fade at the
    // image edges. With revert == true, the effect is undone.

    bool grey_horizontal = ! source.full_width ;
    
    // special treatment for pole caps

    bool grey_north = true ;
    bool grey_south = true ;

    if ( source.projection != MOSAIC )
    {
      if ( source.vofs <= M_PI_2 )
        grey_north = false ;

      if ( source.vofs + source.vfov >= M_PI + M_PI_2 )
        grey_south = false ;
    }

    // if there is no alpha channel, we apply fade_to_grey to the
    // splines and the image pyramids. We use the same trick of
    // flipping the frame, but now this results in a gradient to
    // pure black at the spline's limits.

    if ( p_pyramid )
    {
      for ( auto p_bspl : p_pyramid->level )
        fade_to_grey ( p_bspl , grey_horizontal ,
                        grey_north , grey_south , revert ) ;
    }

    if ( p_lq_spline )
      fade_to_grey ( p_lq_spline , grey_horizontal ,
                      grey_north , grey_south , revert ) ;

    if ( p_hq_spline && ( p_hq_spline != p_lq_spline ) )
      fade_to_grey ( p_hq_spline , grey_horizontal ,
                      grey_north , grey_south , revert ) ;

    if ( p_pyramid4 )
    {
      for ( auto p_bspl : p_pyramid4->level )
      {
        auto chv = p_bspl->get_channel_view ( 3 ) ;
        fade_to_grey ( &chv , grey_horizontal ,
                        grey_north , grey_south , revert ) ;
      }
    }

    if ( p_lq_spline4 )
    {
      auto chv = p_lq_spline4->get_channel_view ( 3 ) ;
      fade_to_grey ( &chv ,
                      grey_horizontal , grey_north ,
                      grey_south , revert ) ;
    }

    if ( p_hq_spline4 && ( p_hq_spline4 != p_lq_spline4 ) )
    {
      auto chv = p_hq_spline4->get_channel_view ( 3 ) ;
      fade_to_grey ( &chv ,
                      grey_horizontal , grey_north ,
                      grey_south , revert ) ;
    }
  }

  // the 'build' routine creates more sophisticated interpolators beyond
  // the raw image object's simple interpolators - if possible.
  // We run this with as many jobs as there are physical cores; for now
  // this seems a good compromise. It produces the image pyramids speedily,
  // and when frame generation needs to hapen while the build is still in
  // progress, default_njobs is sufficiently larger to crowd out the build
  // process and provide frames reasonably quickly, avoiding too much stutter.

  const int cores_for_build = vspline::ncores ;

  void build ( int squash = 0 )
  {
    {
      std::lock_guard < std::mutex > lk ( status_mutex ) ;
      status_building++ ;
      status_flag = true ;
    }

    // short alias

    raw_image_type & raw_image ( *ps_raw_image ) ;

    // first, if there is enough memory, we create a float-based level-0
    // interpolator, replacing the raw-data-based primal interpolator

    // get the image's shape from raw_image:

    auto core_shape = raw_image.core_shape ;

    // we always specify REFLECT BCs for the vertical axis.
    // for full spherical panoramas touching the poles, this will display
    // correctly, if the prefiltering and bracing is done with specialized
    // code, see the calls to prefilter. The horizontal axis will also be
    // done with REFLECT BCs, unless the image is 360 degrees, which is
    // signalled by 'is_periodic'.

    vspline::bcv_type<2> bcv { vspline::REFLECT } ;

    if ( is_periodic )
      bcv[0] = vspline::PERIODIC ;

    bool full_spherical = (    ( source.projection == SPHERICAL )
                            && source.full_height
                            && source.full_width ) ;

    if ( raw_image.mode == WITH_ALPHA )
    {
      auto p_help = mem_in() << new alpha_spline_type ( core_shape , 1 , bcv ) ;

      if ( p_help )
      {
        // use the 'primal' evaluator from the raw_image_type object to
        // populate the initial float-valued spline. Note how we don't
        // use njobs, but ncores, to leave some room for other tf's
        // happening on behalf of the viewer

        vspline::transform ( raw_image.primal_alpha_dev0 ,
                             p_help->core ,
                             cores_for_build ) ;
        p_help->brace() ;
      }

      p_alpha = p_help ;
    }

    // we now copy the primal pyramid regardless of lq degree, because
    // we'll use it for all decimating views.
    
    bool copy_raw_pyramid = true ;

    if ( ps_raw_image->p_primal_pyramid == nullptr )
      copy_raw_pyramid = false ;

    // TODO: I think this is obsolete, checked in raw_image_type already.

    if ( smoothing_level < -1 )
      copy_raw_pyramid = false ;

    // we may or may not produce a level-0 spline. If p_bspl is NULL,
    // the spline is not created and deemed unnecessary.

    spline_type * p_bspl = nullptr ;
    spline4_type * p_bspl4 = nullptr ;

    int initial_spline_degree = 1 ;

    if ( smoothing_level < -1 )
    {
      initial_spline_degree = cbf_degree ;
    }

    // now we set up the 'final' image pyramid. If we have a 'primal
    // pyramid', this pyramid lacks a level-0 spline, so we put the new
    // spline we just made into it's level 0 and we're done. If we don't
    // have a primal pyramid, we build the 'final' pyramid from scratch,
    // passing the newly built spline to be used as it's base level.

    std::shared_ptr < pixel_pyramid_type > p_help ;
    std::shared_ptr < pixel4_pyramid_type > p_help4 ;

    if ( raw_image.mode != WITH_ALPHA )
    {
      if ( squash == 0 || copy_raw_pyramid == false )
      {
        // squash == 0 signals that pyramid level 0 will be required, so we
        // need the level-0 spline. If we can't copy the 'raw' pyramid (because
        // it hasn't been built in the first place) we'll also need the level-0
        // spline to serve as the pyramid's base level.

        // create a suitable bspline object, with some headroom for shifting
        // This spline will be attached to the lq image pyramid and deleted with
        // the pyramid.

        p_bspl = mem_in() << new
          spline_type ( core_shape , initial_spline_degree ,
                        bcv , -1.0 , headroom ) ;

        // use the 'primal' evaluator from the raw_image_type object to
        // populate the initial float-valued spline.

        vspline::transform ( raw_image.primal_dev0 ,
                            p_bspl->core ,
                            cores_for_build ) ;

        // prefilter. TODO consider stashing the original data rather than
        // restoring them later if the spline degree is > 1

        if ( full_spherical )
          spherical_prefilter ( *p_bspl , p_bspl->core , cores_for_build ) ;
        else
          p_bspl->prefilter ( 1 , cores_for_build ) ;

        // now we have p_bspl pointing to a level-0 spline. This may be a
        // degree-1 spline if the decimation does not require a b-spline
        // (levels -1 and up) - or a degree-3 spline, for decimators using
        // CBFs
      }

      if ( copy_raw_pyramid )
      {
        // copy_raw_pyramid implies that the 'primal pyramid' is degree-1:

        assert ( ps_raw_image->p_primal_pyramid->level[1]->spline_degree == 1 ) ;

        // p_bspl may be == nullptr at this point if squashing was specified

        if ( p_bspl != nullptr )
        {
          // we have a level-0 spline, so we take it over as level 0 of the
          // primal pyramid - the primal pyramid lacks level 0 because it's
          // relying on the raw data to provide level-0 information

          ps_raw_image->p_primal_pyramid->level [ 0 ] = p_bspl ;
        }

        p_help = ps_raw_image->p_primal_pyramid ;
      }
      else
      {
        assert ( p_bspl != nullptr ) ;

        // we can't take over the raw image's primal pyramid, so we
        // build a new pyramid.

        p_help = std::make_shared < pixel_pyramid_type >
                                      ( p_bspl ,
                                        scaling_step ,
                                        smoothing_level ,
                                        floor ,
                                        full_spherical ,
                                        cores_for_build ) ;

        // if this new pyramid has any splines in it which have degree > 1,
        // we 'restore' them (undo the prefilter) and set them to degree 1.
        // After that, the pyramid will contain image data at all levels,
        // which we require for the use in the viewer, where we either pick
        // the pyramid level nearest in resolution to the desired view and
        // use bilinear interpolation from that level - or we use area
        // decimation, which also needs image data as it's substrate, rather
        // than a b-spline with degree > 1.

        p_help->restore() ;
      }

      // if 'pyramid squashing' needs to be done, we do it now, on the
      // lq pyramid. If an hq pyramid is built, it starts with level 0
      // of the squashed pyramid, creating the hq pyramid with the same
      // number of levels as the lq pyramid.

      for ( int npop = 0 ; npop < squash ; npop++ )
      {
        if ( p_help->level.size() > 1 )
        {
          p_help->pop() ;
          pyramid_bias -- ;
        }
      }
    }

// new code for processing of associated-alpha RGBA data. The raw_image_type
// object can provide such data for discrete coordinates, so we populate a
// spline with them and build a corresponding pyramid.

    if ( raw_image.mode == WITH_ALPHA )
    {
      // if the image has an alpha channel, we set up the level-0 spline
      // filled with associated alpha RGBA.

      // TODO: will prefiltering work out okay here if the initial spline
      // degree is greater than one? This should only be the case if the
      // decimation in the pyramid is done with a CBF, so the smoothing
      // should make sure that we don't run into negative aRGBA values.
      // The fill-in code further down will also fill in such negative
      // aRGBA values, so in the end, after filling in, the pyramid
      // should be fine.

      p_bspl4 = mem_in() << new
        spline4_type ( core_shape , initial_spline_degree ,
                      bcv , -1.0 , headroom ) ;

      vspline::transform ( raw_image.primal_iargba ,
                           p_bspl4->core ,
                           cores_for_build ) ;

      if ( full_spherical )
        spherical_prefilter ( *p_bspl4 , p_bspl4->core , cores_for_build ) ;
      else
        p_bspl4->prefilter ( 1 , cores_for_build ) ;

      // set up an image pyramid with alpha-associated RGBA values, squash
      // if necessary.

      p_help4 = std::make_shared < pixel4_pyramid_type >
                                    ( p_bspl4 ,
                                      scaling_step ,
                                      smoothing_level ,
                                      floor ,
                                      full_spherical ,
                                      cores_for_build ) ;

      for ( int npop = 0 ; npop < squash ; npop++ )
      {
        if ( p_help4->level.size() > 1 )
        {
          p_help4->pop() ;
          pyramid_bias-- ;
        }
      }

      // if the pyramid contains prefiltered coefficients, restore to
      // unmodified image data.

      p_help4->restore() ;
    }

    // now the lq pyramid is ready for use, and all aRGBA values should
    // be valid, with at least minimal alpha and positive colour channel.
    // We assign to the appropriate member variables, so that rendering
    // code can pick up the pyramids.

    p_pyramid = p_help ;
    p_pyramid4 = p_help4 ;

    stage = 2 ; // signal that the lq pyramid is now usable

    // the next stage is to provide splines for interpolation: so far,
    // we only have image pyramids, which we can use for scaled-down
    // views. We can scale up from the pyramid's base level, but the
    // pyramid holds image data, so the best scale-up we could produce
    // would be bilinear - for better upscaling, we need prefilered
    // data in a b-spline. If that is wanted (lq_degree>1, hq_degree>1),
    // we produce such splines now.

    spline_type * p_lq_help = nullptr ;
    spline_type * p_hq_help = nullptr ;
    spline4_type * p_lq_help4 = nullptr ;
    spline4_type * p_hq_help4 = nullptr ;

    if ( raw_image.mode != WITH_ALPHA )
    {
      if ( lq_degree > 1 )
      {
        auto l0_core = p_help->level[0]->core ;
        auto l0_shape = l0_core.shape() ;
        
        // build a separate interpolator spline for this degree

        p_lq_help = mem_in()
          << new spline_type ( l0_shape , lq_degree , bcv , -1.0 , headroom ) ;

        if ( full_spherical )
          spherical_prefilter ( *p_lq_help , l0_core , cores_for_build ) ;
        else
          p_lq_help->prefilter ( l0_core , 1 , cores_for_build ) ;
      }

      if ( hq_degree > 1 )
      {
        if ( hq_degree == lq_degree )
          p_hq_help = p_lq_help ;
        else
        {
          auto l0_core = p_help->level[0]->core ;
          auto l0_shape = l0_core.shape() ;
          
          // build a separate interpolator spline for this degree

          p_hq_help = mem_in()
            << new spline_type ( l0_shape , hq_degree , bcv , -1.0 , headroom ) ;

          if ( full_spherical )
            spherical_prefilter ( *p_hq_help , l0_core , cores_for_build ) ;
          else
            p_hq_help->prefilter ( l0_core , 1 , cores_for_build ) ;
        }
      }
    }

    if ( raw_image.mode == WITH_ALPHA )
    {
      if ( lq_degree > 1 )
      {
        auto l0_core = p_pyramid4->level[0]->core ;
        auto l0_shape = l0_core.shape() ;
        
        // build a separate interpolator spline for this degree

        p_lq_help4 = mem_in()
          << new spline4_type ( l0_shape , lq_degree , bcv , -1.0 , headroom ) ;

        // the spline is problematic. Where alpha values are low and have
        // larger neighbours, prefiltering can move the coefficients to
        // negative values, spoiling the evaluation. This produces visible
        // artifacts near areas which were initially 4*0, because they
        // were filled in with low alpha content. Omitting the prefilter
        // blurs the image, and we don't want that either. Hence:

        // new code: before prefiltering, we deassocoiate the aRGBA data.
        // The content in p_hq_help4 will have usable RGB. Prefiltering
        // should leave the alpha channel untouched, so we prefiler R, G
        // and B individually, using 'channel views'. After that, we
        // reassociate. Why the acrobatics? We know that the alpha values
        // in the aRGBA data have positive alpha (because of the fill-in
        // run), and if we associate the prefiltered RGB values with them,
        // evaluating the spline can't produce negative alpha in the
        // result due to the spline being limited to the convex hull of
        // the coefficients, and if they are all-positive, so is the
        // spline. If any of the R, G or B values should have come out
        // negative, that's not a problem, because the saturation
        // arithmetics will clamp such vlues to zero later on.
        // Not having prefiltered the alpha channel may produce mild
        // blur of the alpha channel in the evaluate, but that isn't
        // a problem.

        vspline::transform ( deassociate_type() ,
                              l0_core ,
                              p_lq_help4->core ) ;

        for ( int channel = 0 ; channel < 3 ; channel++ )
        {
          auto channel_help = p_lq_help4->get_channel_view ( channel ) ;

          if ( full_spherical )
            spherical_prefilter ( channel_help ,
                                  channel_help.core ,
                                  cores_for_build ) ;
          else
            channel_help.prefilter ( 1 , cores_for_build ) ;
        }

        vspline::apply ( associate_type(), p_lq_help4->core ) ;
        p_lq_help4->brace() ;
      }

      if ( hq_degree > 1 )
      {
        if ( hq_degree == lq_degree )
          p_hq_help4 = p_lq_help4 ;
        else
        {
          auto l0_core = p_pyramid4->level[0]->core ;
          auto l0_shape = l0_core.shape() ;
          
          // build a separate interpolator spline for this degree

          p_hq_help4 = mem_in()
            << new spline4_type ( l0_shape , hq_degree , bcv , -1.0 , headroom ) ;

          // we use the same prefiltering strategy as above

          vspline::transform ( deassociate_type() ,
                               l0_core ,
                               p_hq_help4->core ) ;

          for ( int channel = 0 ; channel < 3 ; channel++ )
          {
            auto channel_help = p_hq_help4->get_channel_view ( channel ) ;

            if ( full_spherical )
              spherical_prefilter ( channel_help ,
                                    channel_help.core ,
                                    cores_for_build ) ;
            else
              channel_help.prefilter ( 1 , cores_for_build ) ;
          }

          vspline::apply ( associate_type(), p_hq_help4->core ) ;
          p_hq_help4->brace() ;
        }
      }
    }

    p_lq_spline = p_lq_help ;
    p_hq_spline = p_hq_help ;
    p_lq_spline4 = p_lq_help4 ;
    p_hq_spline4 = p_hq_help4 ;

    if ( grey_edge )
      apply_grey_edge() ;

    stage = 3 ; // interpolator is fully built

    // just to doublecheck... when in alpha mode, all the ...4 pointers
    // should be set, otherwise those without the '4'. All the ...4
    // pointers point to objects holding associated alpha data (aRGBA)

    if ( lq_degree > 1 )
      assert ( ( p_lq_spline4 == nullptr ) ^ ( p_lq_spline == nullptr ) ) ;
    if ( hq_degree > 1 )
      assert ( ( p_hq_spline4 == nullptr ) ^ ( p_hq_spline == nullptr ) ) ;
    assert ( ( p_pyramid4 == nullptr ) ^ ( p_pyramid == nullptr ) ) ;

    // relinquish hold on the raw image object; we now have the 'proper'
    // image pyramid(s) and we'll only use them from now on. Note how
    // other code may still carry on using the raw image object; it will
    // only be destroyed with the last copy of this shared pointer:

    ps_raw_image.reset() ;

    {
      std::lock_guard < std::mutex > lk ( status_mutex ) ;
      status_building-- ;
      status_flag = true ;
    }
  }

  /// perform_transform is responsible for filling a frame with data by means of
  /// applying the required transformation of source image data as it goes along,
  /// calculating the source image coordinate for every pixel, then invoking
  /// a suitable interpolator to obtain the pixel's 'raw' value, then applying
  /// 'light' modifications - if any - and finally producing the desired data
  /// type of output pixel, which is stored as the result.
  ///
  /// the routine takes a pointer to a job_type object, as passed to the calling
  /// routine. This is where the metrics for the job at hand are found.
  /// The next argument is the transformation from 2D target image coordinates
  /// to 2D source image coordinates - the latter expressed in scale-invariant
  /// form in radians.
  ///
  /// To obtain (interpolated) image data, an evaluator is obtained by calling
  /// provide_ev3 or provide_ev4, respectively. The resulting evaluator will
  /// accept coordinates in 'model space units' - the magnitude of coordinates
  /// which occurs after the target coordinates have been transformed into
  /// source coordinates in model space (aka coordinates of the 'draped image').
  /// The evaluator internally translates these coordinates into coordinates
  /// suitable to access the b-spline which represents the data at the given
  /// magnification. This mode of operation makes it possible to ignore the
  /// inner workings of the evaluator.
  ///
  /// There is one special situation where this routine is used: if a new warp array
  /// has to be populated with transformed coordinates. In this case, the warp array
  /// is populated 'en passant' by storing the transformed coordinates while the
  /// frame is rendered.

  template < typename projection_type >
  void perform_transform ( const job_type * const p_job ,
                           const projection_type projection ) const
  {
    const frame_type & frame ( p_job->frame ) ;    // short identifier
    const target_type & target ( p_job->target ) ; // short identifier
    const dock_type & dock ( p_job->dock ) ; // short identifier

    // The caller can request associated alpha data or unassociated alpha,
    // or plain RGB data without alpha.
    // The three evaluators here are set up accordingly - if the interpolator
    // is in alpha mode, the alpha channel is taken from the image data,
    // otherwise it's set to 'fully opaque' (255.0f).
    // With this setup, jobs which ask for RGBA or RGB data can be handled,
    // which is a change to my previous implementation which would only
    // provide the type of data which were 'native' to the interpolator.

    ev_type ev ;
    ev4_type ev4 ;
    alpha_ev_type transparency ;

    if ( mode == WITH_ALPHA )
    {
      ev4 = provide_ev4 ( p_job ) ;
      transparency = extract_alpha_type ( ev4 ) ;
      if ( frame.yield_argba )
      {
        ev = omit_alpha_type ( ev4 ) ;
      }
      else
      {
        ev = omit_alpha_type ( ev4 + deassociate_type() ) ;
      }
    }
    else
    {
      ev_type ev3 = provide_ev3 ( p_job ) ;
      transparency = yield_opaque() ;

      if ( frame.yield_argba )
      {
        ev = ev3 + amplify_t < pixel_type > ( 255.0f ) ;
      }
      else
      {
        ev = ev3 ;
      }
    }

    // create a MultiArrayView to the target frame's data, interpreting the data
    // as integers (instead of the 'real' data type, which is 8bit RGBA,
    // a TinyVector of four unsigned char)

    int_array_view int_view ( frame.p_frame->shape() ,
                              (int*) ( frame.p_frame->data() ) ) ;

    if ( frame.format == FRAME_FRGBA )
    {
      // frame.format == FRAME_FRGBA means we are to produce a float RGBA
      // frame for a snapshot. Since we're supposed to produce alpha data,
      // we pick a preprocess4 object.

      typedef preprocess4 < ev_type > preprocess_type ;

      // now we can set up the preprocess4 object:

      preprocess_type prep ( false ,
                             ! source.full_width ,
                             ! source.full_height ,
                             0 ,
                             0 ,
                             source.extent.x0 ,
                             source.extent.x1 ,
                             source.extent.y0 ,
                             source.extent.y1 ,
                             ev ,
                             transparency ) ;

      shade4 ( projection + prep , p_job ) ;
    }
    else // not a format FRAME_FRGBA job
    {
      typedef preprocess < ev_type > preprocess_type ;

      preprocess_type prep ( false ,
                             ! source.full_width ,
                             ! source.full_height ,
                             0 ,
                             0 ,
                             source.extent.x0 ,
                             source.extent.x1 ,
                             source.extent.y0 ,
                             source.extent.y1 ,
                             ev ) ;

      if ( frame.format == FRAME_FRGB )
      {
        shade ( projection + prep , p_job ) ;
      }
      else if ( mode == NO_ALPHA )
      {
        if ( frame.mode == PAN )
        {
          // we'll only ever land in perform_transform with mode == PAN if this is
          // the first frame to be rendered in PAN mode. This signals that we need
          // to populate a warp array with transformed coordinates for later use.
          // If this occurs, we wrap 'projection' in a 'capture_type' object, which
          // stores the transformed coordinates to a warp array as it goes along.
          // Subsequent frames in PAN mode can use the warp array.

          typedef preprocess < ev_type > preprocess_type ;

          preprocess_type prep ( false ,
                                 ! source.full_width ,
                                 ! source.full_height ,
                                 0 ,
                                 0 ,
                                 source.extent.x0 ,
                                 source.extent.x1 ,
                                 source.extent.y0 ,
                                 source.extent.y1 ,
                                 ev ) ;

          // create the 'siphoning-off' object, wrapping the projection and taking
          // a reference to the warp array to deposit the transformed coordinates.

          capture_type < projection_type >
            capture ( *(dock.p_warp) , projection ) ;

          shade ( capture + prep , p_job ) ;
        }
        else // frame.mode is not PAN. this is simple:
        {
          shade ( projection + prep , p_job ) ;
        }
      }
      else
      {
        // there is an alpha channel. The code is just about the same as above,
        // only that pre- and postprocessing is done with objects handling
        // data with alpha information.

        if ( frame.mode == PAN )
        {
          // again we have the pan mode special case, which we treat just as
          // above (where we weren't using alpha data).

          // We use preprocess4 which honours the alpha channel, instead of the
          // plain preprocess object above. Note how we instantiate it with ev_type
          // as template argument: we're pulling in a straight vspline::evaluator;
          // the domain is attached to the projection functor

          typedef preprocess4 < ev_type > preprocess_type ;

          preprocess_type prep ( false ,
                                 ! source.full_width ,
                                 ! source.full_height ,
                                 0 ,
                                 0 ,
                                 source.extent.x0 ,
                                 source.extent.x1 ,
                                 source.extent.y0 ,
                                 source.extent.y1 ,
                                 ev ,
                                 transparency ) ;

          capture_type < projection_type >
            capture ( *(dock.p_warp) , projection ) ;

          shade4 ( capture + prep , p_job ) ;
        }
        else // not PAN mode
        {
          // We use preprocess4 which honours the alpha channel, instead of the
          // plain preprocess object above.

          typedef preprocess4 < ev_type > preprocess_type ;

          preprocess_type prep ( false ,
                                 ! source.full_width ,
                                 ! source.full_height ,
                                 0 ,
                                 0 ,
                                 source.extent.x0 ,
                                 source.extent.x1 ,
                                 source.extent.y0 ,
                                 source.extent.y1 ,
                                 ev ,
                                 transparency ) ;

          shade4 ( projection + prep , p_job ) ;
        }
      }
    }
  }

  /// perform_remap performs the warp-array based transform used for pan mode.
  /// this operation is less involved than the transformation-based routines, since
  /// the source image coordinates are already at hand, stored in the warp array.
  /// What's left to do is set up the 'preprocess' object which, in this case,
  /// is mainly used to perform the pan by setting the incoming x coordinates off
  /// by 'x_offset'.

  void perform_remap ( const job_type * const p_job ) const
  {
    const frame_type & frame ( p_job->frame ) ;   // short identifier
    const dock_type & dock ( p_job->dock ) ;

    ev_type ev ;
    ev4_type ev4 ;
    alpha_ev_type transparency ;

    if ( mode == WITH_ALPHA )
    {
      ev4 = provide_ev4 ( p_job ) ;
      transparency = extract_alpha_type ( ev4 ) ;
      if ( frame.yield_argba )
      {
        ev = omit_alpha_type ( ev4 ) ;
      }
      else
      {
        ev = omit_alpha_type ( ev4 + deassociate_type() ) ;
      }
    }
    else
    {
      ev = provide_ev3 ( p_job ) ;
    }

    if ( frame.tonemap )
      ev = vspline::grok ( ev + tonemap_type() ) ;

    // first we calculate the amount of shift we need for the pan

    double shift = frame.x_shift ;

    // we map this (arbitrary) angle to 0-2pi, so it's always positive,
    // as the preprocess object we use expects this.

    cage_angle ( shift ) ;

    // first we'll fix the destination of the processing chain:
    // we have an rgba image in p_job->p_frame. We create a view
    // to it as an int array, since the interpolator we intend to use
    // directly yields 8bit RGBA packed into ints, so it needs a view
    // to an array of int as it's target:

    int_array_view int_view ( frame.p_frame->shape() ,
                              (int*) ( frame.p_frame->data() ) ) ;

    // now we build up the processing chain and call vspline:transform
    // to execute it for all pixels in this target array. We need to
    // distinguish two cases: either we're processing alpha data or not.

    if ( mode == WITH_ALPHA )
    {
      // there is an alpha channel.
      // we need a preprocess4 object, which handles RGBA data; plain
      // preprocess objects only handle RGB.

      typedef preprocess4 < ev_type > preprocess_type ;

      // We use a preprocess4 type object (which includes transparency)
      // and attach a yield_rgba4 type object to postprocess the
      // float RGBA data coming from the preprocess4 object. The yield_rgba4
      // object produces 8-bit unsigned RGBA pixels just like a yield_rgba
      // object, but it takes float RGBA input and honours the input's
      // alpha channel, whereas a yield_rgba object takes float RGB and
      // always sets the alpha channel to 255 (fully opaque).

      auto yield = preprocess_type ( true ,
                                     ! source.full_width ,
                                     ! source.full_height ,
                                     shift ,
                                     2.0 * M_PI ,
                                     source.extent.x0 ,
                                     source.extent.x1 ,
                                     source.extent.y0 ,
                                     source.extent.y1 ,
                                     ev ,
                                     transparency )
                  + yield_rgba4 ( frame.light_settings ) ;

      // finally we execute the transform, using the warp array provided by
      // the 'dock' and depositing the result in int_view, which is a view
      // to the RGBA data going to SFML. Why don't we do the transform
      // jointly for bothe the alpha and non-alpha case? Because 'yield'
      // is of a different type in both cases. We could 'grok' the whole
      // processing chain in both cases, but since we're very much inner loop
      // here, we're trying to keep the code as lean as possible.
      // note that we can't use 'shade4()' here, because the vspline transform
      // takes the 'warp array' as it's second parameter, whereas in shade4()
      // we always use an index-based transform.

      auto njobs = p_job->njobs ;
      if ( njobs <= 0 )
        njobs = vspline::default_njobs ;

      auto cutout = dock.p_warp->subarray
          ( { 0 , 0 } , frame.p_frame->shape() ) ;

      vspline::transform ( yield ,
                           cutout , // *(dock.p_warp) ,
                           int_view ,
                           njobs ) ;
    }
    else
    {
      // without an alpha channel things are simpler. We use a plain
      // preprocess object (which does not handle alpha data):

      typedef preprocess < ev_type > preprocess_type ;

      // we chain the preprocess object and a yield_rgba object,
      // to convert the preprocess object's float RGB output into 8bit RGBA.
      // Since we aren't performing the coordinate transformation here - the
      // transformed coordinates are taken from the warp array - this processing
      // chain is a bit shorter than the one in perform_transform() above.

      auto yield
      =   preprocess_type ( true ,
                            ! source.full_width ,
                            ! source.full_height ,
                            shift ,
                            2.0 * M_PI ,
                            source.extent.x0 ,
                            source.extent.x1 ,
                            source.extent.y0 ,
                            source.extent.y1 ,
                            ev )
        + yield_rgba ( frame.light_settings ) ;

      // finally we execute the transform, using the warp array provided by
      // the 'dock' and depositing the result in int_view, which is a view
      // to the RGBA data going to SFML.
      // note that we can't use 'shade()' here, because the vspline transform
      // takes the 'warp array' as it's second parameter, whereas in shade()
      // we always use an index-based transform.

      auto njobs = p_job->njobs ;
      if ( njobs <= 0 )
        njobs = vspline::default_njobs ;

      auto cutout = dock.p_warp->subarray
          ( { 0 , 0 } , frame.p_frame->shape() ) ;

      vspline::transform ( yield ,
                           cutout , // *(dock.p_warp) ,
                           int_view ,
                           njobs) ;
    }
  }

  // the next two functions serve to set up a combination of to_ray
  // and to_source projection for a given job. Having made the projection
  // a template argument to to_ray_type and to_source_type, we can code
  // this very cleanly using a case switch and macros:

  template < typename src_projection_type >
  crd_tf_type fix_target_projection_type ( const job_type * const p_job ,
                                           const src_projection_type tf2 ) const
  {
    crd_tf_type tf ;

  #define make_case(TPRJ) \
      case TPRJ : \
      { \
        auto tf1 = to_ray_type < TPRJ > ( p_job ) ; \
        tf = vspline::grok ( tf1 + tf2 ) ; \
        break ; \
      }

    switch ( p_job->target.projection )
    {
      make_case ( RECTILINEAR )
      make_case ( SPHERICAL )
      make_case ( CYLINDRIC )
      make_case ( FISHEYE )
      make_case ( STEREOGRAPHIC )
      make_case ( MOSAIC )

      default:
      {
        // this should not happen, but having a default is good style
        abort_lux ( "unhandled projection in fix_target_projection_type()" ) ;
      }
    }

  #undef make_case

    return tf ;
  }

  // helper function which returns the transformation from a 2D target
  // coordinate to a 3D model space 'ray' coordinate

  to_model_f to_model ( const job_type * const p_job )
  {
    evd_type tf ;

  #define make_case(TPRJ) \
      case TPRJ : \
      { \
        tf = to_ray_type < TPRJ > ( p_job ) ; \
        break ; \
      }

    switch ( p_job->target.projection )
    {
      make_case ( RECTILINEAR )
      make_case ( SPHERICAL )
      make_case ( CYLINDRIC )
      make_case ( FISHEYE )
      make_case ( STEREOGRAPHIC )
      make_case ( MOSAIC )

      default:
      {
        // this should not happen, but having a default is good style
        abort_lux ( "unhandled projection in to_model()" ) ;
      }
    }

  #undef make_case

    return [=] ( const point_2d_d_type & c )
    {
      point_3d_d_type r ;
      tf.eval ( c , r ) ;

      return r ;

    //   if ( p_job->target.projection == MOSAIC )
    //     return r ;
    //
    //   auto & x ( r[0] ) ;
    //   auto & y ( r[1] ) ;
    //   auto & z ( r[2] ) ;
    //   auto norm = sqrt ( x * x + y * y + z * z ) ;
    //
    //   return point_3d_d_type { x / norm , y / norm , z / norm } ;
    } ;
  }

  crd_tf_type fix_source_projection_type ( const job_type * const p_job ) const
  {
    crd_tf_type tf ;

  #define make_case(TPRJ) \
      case TPRJ: \
      { \
        to_source_type < TPRJ > tf2 ; \
        tf = fix_target_projection_type ( p_job , tf2 ) ; \
        break ; \
      }

    switch ( source.projection )
    {
      make_case ( SPHERICAL )
      make_case ( CYLINDRIC )
      make_case ( FISHEYE )
      make_case ( STEREOGRAPHIC )
      make_case ( RECTILINEAR )
      make_case ( MOSAIC )

      default:
      {
        // this should not happen, but having a default is good style
        abort_lux ( "unset projection in fix_source_projection_type()" ) ;
      }
    }

  #undef make_case

    return tf ;
  }

  /// fix_projection_type fixes the transformation from incoming
  /// discrete target coordinates to real source image coordinates.
  /// The transformation is done in two steps, via an intermediate
  /// 3D coordinate (a 'ray' originating at the center of the viewer's
  /// model of space). In sum, we receive a projection from 2D to 2D,
  /// which is passed on to 'perform_transform', where the remainder
  /// of the pixel pipeline is set up and passed to vspline::transform
  /// to fill the target array with pixels.

  void fix_projection_type ( const job_type * const p_job ) const
  {
    // calculate the projection from target coordinates to source
    // coordinates. This is a two-step process: fix_source_projection_type
    // will in turn call fix_target_projection_type, which finally
    // produces the combined transformation functor and 'groks' it.
    // My initial implementation used separate types for each projection,
    // which was manageable as long as only the source projection was
    // variable, but with varying target projections this became too
    // unwieldy, exploding binary size and compilation time, hence
    // the type erasure.

    auto tf = fix_source_projection_type ( p_job ) ;

    // given the coordinate transformation, move on to the next stage

    perform_transform ( p_job , tf ) ;
  }

  // next we have code to provide a specific interpolator, given
  // the desired scale and whether the interpolator is hq or lq.
  // The resulting functor will be built from a spline in one of
  // the image pyramids if possible, and fall back to accessing
  // the raw data via the 'primal' evaluator otherwise.

  // The evaluators we provide are quite involved because they are
  // independent of scale, which is achieved by using a domain. Using the
  // domain makes the access to a 'naked' evaluator problematic, because
  // the scaling and shifting may produce slightly out-of-bounds coordinates.
  // To avoid this, I use 'safe' evaluators, adding yet more code to be
  // executed. Both the 'safe' aspect and the domain cost processing time,
  // but the gain in simplicity should be worth it.

  // in any case, this function produces an evaluator which takes
  // 2D coordinates in model space units, referring to the 'draped'
  // image. So, evaluating this evaluator with coordinate (0,0) will
  // produce a pixel value from the image's center. Accessing the
  // evaluator will be safe in the bounds given by the source image's
  // 'extent', a 2D range of coordinates describing the image's extent
  // in it's own plane, in model space units.

  // When data with alpha channel are processed (nchannels==4), the
  // evaluator will produce associated alpha data (aRGBA). This is
  // unconditional - if the calling code requires unassociated alpha
  // data, the evaluator has to be suffixed with a deassociate_type
  // object.

  template < int nchannels = 3 ,
             typename pixel_t =
             typename std::conditional < nchannels == 3 ,
                                         pixel_type ,
                                         pixel4_type > :: type ,
             typename pyramid_t =
             typename std::conditional < nchannels == 3 ,
                                         pixel_pyramid_type  ,
                                         pixel_pyramid_type  > :: type ,
             typename ev_t =
             typename std::conditional < nchannels == 3 ,
                                         ev_type ,
                                         ev4_type > :: type ,
             typename spline_t =
             typename std::conditional < nchannels == 3 ,
                                         spline_type ,
                                         spline4_type > :: type >
  ev_t provide_ev ( const job_type * const p_job ,
                    std::shared_ptr < pyramid_t > p_pyramid ,
                    const spline_t * p_lq_spline ,
                    const spline_t * p_hq_spline ) const
  {
    auto ps_raw_image_copy = ps_raw_image ;

    // stall until the interpolator is set up to at least the stage
    // the job requires

    while ( stage < p_job->frame.wait_for_stage )
    {
      std::this_thread::sleep_for(std::chrono::microseconds(10000) ) ;
    }

    // extract the images's extent - the extent values in source_type
    // are in a single structure with four values x0, x1, y0 and y1,
    // but for the 'domain' we'll use for range mapping, we need
    // two 2D coordinates for the lower and upper bound.

    coordinate_type in_low ( source.extent.x0 , source.extent.y0 ) ;
    coordinate_type in_high ( source.extent.x1 , source.extent.y1 ) ;

    // short identifier to access p_job->frame

    const frame_type & frame ( p_job->frame ) ;

    // ld = logarithm of zoom factor to the base of scaling step
    // This value is needed to determine the appropriate pyramid level

    double ld =   log ( 1.0 / frame.zoom_factor )
                / log ( scaling_step ) ;

    ld += frame.level_bias ;

    // ld < 0 implies a magnified view, which requires interpolation
    // rather than decimation

    if ( ld < 0.0 )
    {
      // we try and use the dedicated interpolation splines, which may
      // or may not be available. If they are available, we use them to
      // produce the desired evaluator and return straight away. We have
      // two cases to deal with: if an HQ frame is desired, we'll try and
      // use the HQ spline, and if an LQ frame is desired, we'll try and
      // use the LQ spline. The strategy is an opt-in: if use of the
      // dedicated splines is an option, we take this route and return.

      if ( ( frame.hq == true ) && ( p_hq_spline != nullptr ) )
      {
        domain_t domain ( in_low , in_high , *p_hq_spline ) ;

        auto ev = vspline::make_evaluator < spline_t , float , VSIZE >
                  ( *p_hq_spline , { 0 , 0 } , hq_shift ) ;

        return vspline::grok ( domain + ev ) ;
      }
      if ( ( frame.hq == false ) && ( p_lq_spline != nullptr ) )
      {
        domain_t domain ( in_low , in_high , *p_lq_spline ) ;
        auto ev = vspline::make_evaluator < spline_t , float , VSIZE >
                  ( *p_lq_spline , { 0 , 0 } , lq_shift ) ;

        return vspline::grok ( domain + ev ) ;
      }
    }

    // if we arrive here, we haven't made use of the dedicated interpolation
    // splines. The next thing we try is accessing the image pyramids: for
    // interpolation, the base level of a pyramid provides a viable source,
    // just the quality may be less than what a dedicated interpolation spline
    // would have provided.
    // Again, ld < 0 implies a magnified view, which requires interpolation
    // rather than decimation. Again, we use an opt-in strategy: If we have
    // the image pyramid available, we'll use it's base level and return.

    if ( ld < 0.0 && p_pyramid != nullptr )
    {
      auto p_l0_spline = p_pyramid->level[0] ;
      domain_t domain ( in_low , in_high , *p_l0_spline ) ;
      int degree ;

      if ( frame.hq )
        degree = hq_degree + hq_shift ;
      else
        degree = lq_degree + lq_shift ;

      int shift = degree - 1 ;
      auto ev = vspline::make_evaluator < spline_t , float , VSIZE >
                ( *p_l0_spline , { 0 , 0 } , shift ) ;

      return vspline::grok ( domain + ev ) ;
    }

    // no luck so far. If the pyramid is available, arriving here means
    // that we certainly need to decimate: all magnifying cases have
    // been dealt with.

    if ( p_pyramid )
    {
      // we need to determine whether to use area decimation or not:

      int degree ;
      if ( p_job->decimate_area )
      {
        degree = 2 ;
      }
      else if ( frame.hq )
      {
        degree = hq_degree + hq_shift ;
        if ( degree > 1 )
          degree = 2 ;
      }
      else
      {
        degree = lq_degree + lq_shift ;
        if ( degree > 1 )
          degree = 2 ;
      }

      if ( degree == 2 )
      {
        // calculate values for area decimator:

        int level_int = int ( ld ) ;
        double delta = ld - level_int ;

        if ( delta >= 1.0 )
        {
          delta = 0.0 ;
          level_int++ ;
        }

        int pyramid_level = level_int ;
        double area_width = pow ( p_pyramid->scaling_step , delta ) ;
      
        pyramid_level += pyramid_bias ;

        if ( pyramid_level < 0 )
          pyramid_level = 0 ;

        if ( pyramid_level >= p_pyramid->level.size() )
          pyramid_level = p_pyramid->level.size() - 1 ;

        assert (    pyramid_level >= 0
                 && pyramid_level < p_pyramid->level.size() ) ;

        spline_t * p_level
          = (spline_t*) p_pyramid->level [ pyramid_level ] ;

        domain_t domain ( in_low , in_high , *p_level ) ;

        typedef area_basis_functor < float > lbf_type ;
        lbf_type lbf ( 2 , area_width ) ;
        vspline::homogeneous_mbf_type < lbf_type > mbf ( lbf ) ;

        typedef vspline::abf_evaluator < coordinate_type ,
                                         pixel_t ,
                                         decltype ( mbf ) ,
                                         VSIZE > ckev_type ;

        auto shift = 1 ; // p_level is degree 1, we need degree 2

        auto ev = ckev_type ( *p_level , mbf , shift ) ;

        return vspline::grok ( domain + ev ) ;
      }
      else
      {
        // don't use area decimation, simply round to the nearest level

        int pyramid_level = std::round ( ld ) ;
        
        pyramid_level += pyramid_bias ;

        if ( pyramid_level < 0 )
          pyramid_level = 0 ;

        if ( pyramid_level >= p_pyramid->level.size() )
          pyramid_level = p_pyramid->level.size() - 1 ;

        assert (    pyramid_level >= 0
                 && pyramid_level < p_pyramid->level.size() ) ;

        spline_t * p_level
          = (spline_t*) p_pyramid->level [ pyramid_level ] ;

        domain_t domain ( in_low , in_high , *p_level ) ;

        // degree can be 0 or 1, we may get a shift of 0 or -1

        int shift = degree - 1 ;

        auto ev = vspline::make_evaluator < spline_t , float , VSIZE >
                     ( *p_level , { 0 , 0 } , shift ) ;

        return vspline::grok ( domain + ev ) ;
      }
    }

    // there are no image pyramids available (yet), use the primal
    // evaluators. We have taken the copy of ps_raw_image before trying
    // to access the pyramids, so it must be valid, just doublecheck:

    assert ( ps_raw_image_copy != nullptr ) ;

    int itp_degree = lq_degree + lq_shift ;

    if ( frame.hq )
      itp_degree = hq_degree + hq_shift ;

    if ( itp_degree < 0 )
      itp_degree = 0 ;

    // raw image has un-prefiltered data, so we won't go beyond degree 2,
    // to avoid excessive blur

    if ( itp_degree > 2 )
      itp_degree = 2 ;

    // the raw image can't provide area decimation, so we 'manually'
    // calculate the pyramid level by rounding substrate.ld.

    int pyramid_level = std::round ( ld ) ;

    if ( pyramid_level < 0 )
      pyramid_level = 0 ;

    auto ev = ps_raw_image_copy->get_evaluator < nchannels >
            ( pyramid_level , itp_degree , in_low , in_high ) ;


    // we have obtained the evaluator from the raw image object, so we
    // need to make sure that the raw image isn't destructed while this
    // evaluator is still 'alive'. so we use 'attach' to form an object
    // which holds a copy of the shared pointer to the raw image together
    // with the evaluator and behaves just like the evaluator otherwise.
    // This object is 'grokked', resulting in an ev_type which has the
    // 'retaining' property built-in without showing it externally.

    return ( attach ( ev , ps_raw_image_copy ) ) ;
  }

  ev_type provide_ev3 ( const job_type * const p_job ) const
  {
    return provide_ev < 3 > ( p_job ,
                              p_pyramid ,
                              p_lq_spline ,
                              p_hq_spline ) ;
  }

  ev4_type provide_ev4 ( const job_type * const p_job ) const
  {
    assert ( mode == WITH_ALPHA ) ;
  
    return provide_ev < 4 > ( p_job ,
                              p_pyramid4 ,
                              p_lq_spline4 ,
                              p_hq_spline4 ) ;
  }

  struct geometry_type
  {
    quaternion_type orientation ;
    double zoom ;
    vigra::Shape2 shape ;
    sensor_settings_type sensor ;
    double vofs ;
  } ;

  bool pan_mode_possible ( const geometry_type & initial_geometry ,
                           const geometry_type & current_geometry )
  {
    auto diff =   initial_geometry.orientation
                / current_geometry.orientation ;

    // now we convert this quaternion to Euler angles:

    double y , p , r ;
    toEulerAngle ( diff , y , p , r ) ;

    // Now we check whether combined pitch and roll are below
    // a small threshold. If that is so, the previous and current
    // orientation can differ only in yaw - or not at all.
    // If, on top of that, all other geometry-relevant factors
    // coincide, the geometries are suitable for PAN mode

    return (    sqrt ( p * p + r * r ) < 1e-12
             && initial_geometry.zoom == current_geometry.zoom
             && initial_geometry.shape == current_geometry.shape
             && initial_geometry.sensor == current_geometry.sensor
             && initial_geometry.vofs == current_geometry.vofs ) ;
  }

  virtual void process_job ( job_type * p_job )
  {
    static warp_array_type warp_array ; // initially contains no data
    static extent_type warp_extent ;
    static geometry_type initial_geometry ;
    static int pan_stage = 0 ;

    frame_type & frame ( p_job->frame ) ;          // short identifier
    dock_type & dock ( p_job->dock ) ;             // short identifier
    const target_type & target ( p_job->target ) ; // short identifier

    if ( frame.rise != 0.0 )
      apply_rise ( source , frame.rise ) ;

    // we attach a pointer to 'warp_array' to the frame's dock. This may be
    // used by code 'further down the line'.

    dock.p_warp = & warp_array ;
    bool force_immediate = false ;

    if ( frame.mode != SINGLE )
    {
      // we gather all geometry-relevant information in a variable, which saves
      // us having to handle all the individual data:

      geometry_type current_geometry { frame.orientation ,
                                      frame.zoom_factor ,
                                      vigra::Shape2 ( target.width ,
                                                      target.height ) ,
                                      frame.sensor_settings ,
                                      source.vofs } ;

      // non-SINGLE-mode jobs can use PAN mode processing if the non-rendering
      // thread gives it's okay. Initially, the non-rendering thread had the
      // logic to figure out when PAN mode could be used, but I've now moved
      // most of the logic here. The next condition after the permission from
      // above is the source image's projection. Using PAN mode for MOSAIC
      // images would also work, but is not beneficial: the arithmetic is
      // trivial and accessing a warp array in memory costs more time.

        if (    p_job->may_pan
             && (    source.projection == SPHERICAL
                  || source.projection == CYLINDRIC ) )
      {
        // if entering PAN mode is at all allowed, we check the 'pan stage'

        if ( pan_stage == 0 )
        {
          // stage zero means we're starting afresh with detecting whether
          // we can use PAN mode or not. We record the current frame's
          // 'geometry' as a reference. The frame will be rendered in
          // 'immediate' mode.

          initial_geometry = current_geometry ;
          pan_stage = 1 ;
          frame.mode = IMMEDIATE ;
        }
        else if ( pan_stage == 1 )
        {
          // stage one means we have a previous frame as reference, so now
          // we can see how the current frame differs from the previous
          // one. If we find that the previous and the current frame
          // would have been suitable to render the current frame in PAN mode,
          // mode, we switch to PAN mode. Even if the current frame *could have*
          // been rendered in PAN mode, we don't have a warp array yet to do
          // so. Switching to PAN mode now will produce the warp array,
          // as the coordinates will be 'siphoned off' during this frame,
          // which, otherwise, is still rendered in IMMEDIATE mode.

          if ( pan_mode_possible ( initial_geometry , current_geometry ) )
          {
            // We set frame.mode to PAN and move to stage 2.
            // We also set force_immediate. This signals to the code below
            // that, even though frame.mode is PAN, we need to calculate
            // it in IMMEDIATE mode, because we don't have prerecorded
            // coordinates (the 'warp' array) yet: they will be produced
            // during the rendition of this frame.
            // We reset initial_geometry, because this will be the
            // reference for PAN mode frames.

            pan_stage = 2 ;
            frame.mode = PAN ;
            force_immediate = true ;
            initial_geometry = current_geometry ;

            // If we don't have a warp array yet, or if it's the wrong
            // shape, we 'swap in' a new warp array - the old one, which
            // is 'swpped out', will perish when new_warp_array goes out
            // of scope. Initially I used a warp array to precisely fit
            // the frame, but when using zimt for the transform, the
            // ensuing fluff operation would overshoot. The slightly
            // larger size with width a multiple of VSIZE works with
            // vspline and zimt alike, and this is the only place in
            // the rendering code where cpping would make a difference.

            auto frame_shape = frame.p_frame->shape() ;
            auto warp_shape = warp_array.shape() ;
            bool fits =    ( warp_shape[0] >= frame_shape[0] )
                        && ( warp_shape[1] >= frame_shape[1] ) ;
            if ( ! fits )
            {
              auto width = frame_shape[0] ;
              if ( width % VSIZE )
                width = ( ( width / VSIZE ) + 1 ) * VSIZE ;
              auto height = frame_shape[1] ;
              warp_shape = vigra::Shape2 ( width , height ) ;
              warp_array_type new_warp_array ( warp_shape ) ;
              warp_array.swap ( new_warp_array ) ;
            }
          }
          else
          {
            // there was yaw or pitch.
            // remain in stage 1, but adapt initial_geometry

            initial_geometry = current_geometry ;
          }
        }
        else if ( pan_stage == 2 )
        {
          // stage two means we're in 'established' PAN mode: we now
          // have a valid 'warp' array with precalculated coordinates.
          // again we check if pitch and roll are zero by looking at
          // the differential quaternion:

          auto diff =   initial_geometry.orientation
                      / current_geometry.orientation ;
          double y , p , r ;
          toEulerAngle ( diff , y , p , r ) ;

          // PAN mode is only possible if there is (virtually) no
          // pich or roll, and all other geometry-relevant factors
          // are equal.

          if ( pan_mode_possible ( initial_geometry , current_geometry ) )
          {
            // All conditions are met. This frame will be rendered using
            // the precalculated coordinates in the warp array, using
            // 'x_shift' equal to the yaw we have detected.

            frame.mode = PAN ;
            force_immediate = false ;
            frame.x_shift = - y ;

            // this should always be the case, but let's be safe.
            // note: the width of the warp array may be larger, it's
            // allocated with a multiple of VSIZE to allow the pixel
            // pipeline to proceed without capping (if the size weren't
            // a multiple of VSIZE, the 'fluff' operation would have to
            // be capped, but we want a cap-free pipeline)

            assert ( frame.p_frame->shape(0) <= warp_array.shape(0) ) ;
            assert ( frame.p_frame->shape(1) <= warp_array.shape(1) ) ;
          }
          else
          {
            // There was pitch and/or roll.
            // return to stage 1, but adapt initial_geometry

            pan_stage = 1 ;
            initial_geometry = current_geometry ;
          }
        }
      }
      else
      {
        // if the non-rendering thread signals that PAN mode should
        // not be used, we reset pan_stage to zero, to start the cycle
        // afresh next time round.

        pan_stage = 0 ;
      }
    }

    // depending on the display mode of this job, we can generate a frame
    // in different ways. Here we get to the point of triggering both the
    // specialization of the pipeline type and the creation of the object
    // of this type with a single simple call, which results in the stage-by-stage
    // specialization in the code above, culminating in the call to a transform
    // routine. The call to a routine named 'fix_projection_type' is deceptive,
    // since it triggers all the above and does much more than only
    // 'fixing the projection type' - that is merely the first specialization.
    // Note how the 'force_immediate' flag forces the frame to be rendered
    // with the single-mode processing chain, even though the frame's mode
    // is actually PAN. Why so? rather than using a separate transform run to create
    // the warp array and then proceed to feed the warp array to transform, we create
    // the warp array 'en passant': If the single-mode rendering code receives
    // a job with mode == PAN, it uses a slightly modified processing pipeline,
    // where the coordinate transformation's result is 'captured'. This only
    // adds a small amount of extra load, consisting in the time needed to
    // store the data, while the frame is actually rendered as a regular single-mode
    // frame and ready to display. So we catch two birds with one stone: the
    // single transform yields us a usable frame and a warp array, which we can
    // use for the next PAN mode job.

    if (    frame.mode == IMMEDIATE
         || frame.mode == SINGLE
         || force_immediate )
    {
      // now proceed with building up the processing pipeline. The first step
      // of this buildup fixes the evaluator, this routine calls a routine
      // to fix the projection type, etc. finally, once the processing chain
      // is fully set up, the transform using it is executed.

      fix_projection_type ( p_job ) ;
    }

    else if ( frame.mode == PAN )
    {
      // if we arrive here, we are in 'established' PAN mode, meaning we have
      // a valid warp array of the right size containing valid coordinates to
      // perform a transform. The cases where we had to create the warp array
      // or populate it with data have been dealt with above, and have resulted
      // in rendering the frame with a single-mode processing chain.
      // All that's left to do here is to actually call the transform.
      // Here, the transform is a 'remap': it uses precalculated coordinates.
      // The processing pipeline is much shorter for a PAN mode frame. PAN mode
      // is specific to single-image interpolators and spherical or cylindric
      // projection.

      perform_remap ( p_job ) ;
    }

  }

} ;

/// Now we have code which will be called from the non-rendering thread
/// via it's dispatcher object. make_interpolator is the portal which
/// accepts source image metrics and interpolator specs, and builds an
/// interpolator object using a specific ISA. The precise nature of the
/// object remains opaque to the calling code, which merely receives
/// a pointer to interpolator_base, which establishes a small interface
/// of capabilities all of pv's interpolator-like objects provide via
/// the pure virtual base class.

interpolator_base * dispatch::make_interpolator
                              ( const char * filename ,
                                const source_type & source ,
                                bool process_linear ,
                                alpha_mode mode ,
                                bool grey_edge ,
                                int _lq_degree ,
                                int _hq_degree ,
                                double _scaling_step ,
                                int _smoothing_level ,
                                int _cbf_degree ,
                                int _floor ,
                                bool build_pyramids ,
                                bool build_raw_pyramids ,
                                int subimage ,
                                interpolator_base * p_recycle
                              ) const
{
  if ( aborted )
    return nullptr ;

  {
    std::lock_guard < std::mutex > lk ( status_mutex ) ;
    now_loading = filename ;
    status_flag = true ;
  }

  std::cout << "processing image file " << filename << std::endl ;

  fileio::image_info imageInfo ( filename ) ;

  std::cout << "have image info for " << filename << std::endl ;
  if ( p_recycle )
  {
    // is our candidate for recycling the same object type?

    bool type_compatible = ( p_recycle->own_type == SINGLE_ITP ) ;

    if ( type_compatible )
    {
      // yes; try to reuse the previous interpolator

      interpolator * p_old = (interpolator*) p_recycle ;

      bool recycled = p_old->rebuild ( imageInfo ,
                                       source ,
                                       process_linear ,
                                       mode ,
                                       grey_edge ,
                                       _lq_degree ,
                                       _hq_degree ,
                                       _scaling_step ,
                                       _smoothing_level ,
                                       _cbf_degree ,
                                       _floor ,
                                       build_pyramids ,
                                       build_raw_pyramids ,
                                       subimage
                                     ) ;

      if ( recycled )
      {
        // the old interpolator could be recycled, return the recycled
        // interpolator straight away

        std::cout << "reusing interpolator for " << filename << std::endl ;

        {
          std::lock_guard < std::mutex > lk ( status_mutex ) ;
          now_loading = std::string() ;
          status_flag = true ;
        }

        return p_recycle ;
      }
    }

    // fell through with no luck, delete the old interpolator and fall
    // through to creating a new one (below)

    // std::cout << "------ discarding old interpolator" << std::endl ;
    memlog >> p_recycle ;
  }
  
  // build and return a new interpolator

  // std::cout << "------ building new interpolator" << std::endl ;
  
  auto itp = mem_in() << new interpolator ( imageInfo ,
                            source ,
                            process_linear ,
                            mode ,
                            grey_edge ,
                            _lq_degree ,
                            _hq_degree ,
                            _scaling_step ,
                            _smoothing_level ,
                            _cbf_degree ,
                            _floor ,
                            build_pyramids ,
                            build_raw_pyramids ,
                            subimage
                          ) ;

  {
    std::lock_guard < std::mutex > lk ( status_mutex ) ;
    now_loading = std::string() ;
    status_flag = true ;
  }

  return itp ;
}

to_model_f dispatch::get_target_to_model_tf
  ( const job_type * const p_job ) const
{
  // TODO: is this cast safe?

  interpolator * p_itp = (interpolator*) ( p_job->frame.p_itp ) ;
  return p_itp->to_model ( p_job ) ;
}

/*

// sample code for synthetic images. gs_func_type is s std::function
// which returns a shader given a pointer to job_type.

typedef std::function < ev_type ( job_type * ) > gs_func_type ;

// struct synthetic_image reveives a gs_func_type object. when it's
// member function process_job is called, it obtains a shader by
// calling the gs_func_type object with the current job.
// The resulting shader is used to call 'shade' which produces
// the target image.

struct synthetic_image
: public interpolator_base
{
  // function which can provide a shader given a job

  gs_func_type gf ;

  // install the shader generator function and set a few internals

  synthetic_image ( const gs_func_type _gf )
  : gf ( _gf )
  {
    own_type = OTHER_ITP ;
    mode = NO_ALPHA ;
    stage = 3 ;
  }

  void shift ( int quality , int by )
  { }

  // create a shader for the job at hand and use 'shade' to apply it

  void process_job ( job_type * p_job )
  {
    auto image_ev = gf ( p_job ) ;
    shade ( image_ev , p_job ) ;
  }

} ;

// just to demonstrate displaying synthetic images, here we calculate
// a pixel RGB value directly from the incoming coordinates + zoom
// and horizontal/vertical shift. The results depend on the chosen
// source image, which isn't really intuitive, but this is really
// just to show how an arbitrary evaluator can be used as image source.

struct sample_synthetic_image
: public vspline::unary_functor < coordinate_type , pixel_type , VSIZE >
{
  const float zoom ;
  const float xshift ;
  const float yshift ;

  sample_synthetic_image ( const job_type * p_job )
  : zoom ( 2.0 * M_PI / p_job->frame.zoom_factor ) ,
    xshift ( p_job->frame.orientation[0] ) ,
    yshift ( p_job->frame.orientation[1] )
  {
    std::cout << "******* sample_synthetic_image zoom = "
              << zoom << std::endl ;
    std::cout << "******* sample_synthetic_image xshift = "
              << xshift << std::endl ;
    std::cout << "******* sample_synthetic_image yshift = "
              << yshift << std::endl ;
  }

  template < typename I , typename O >
  void eval ( const I & in , O & out ) const
  {
    out[0] = 127.5f + 127.5f * sin ( ( in[0] ) * zoom + xshift ) ;
    out[1] = 127.5f - 127.5f * sin ( ( in[0] ) * zoom + xshift ) ;
    out[2] = 127.5f + 127.5f * cos ( ( in[1] ) * zoom + yshift ) ;
  }
} ;

// produce a synthetic image using above functor

interpolator_base * dispatch::make_sample_synthetic_image() const
{
  auto f = [] ( job_type * p_job )
  {
    return sample_synthetic_image ( p_job ) ;
  } ;

  return new synthetic_image ( f ) ;
} ;

*/

/***************************************************************************/

// next we have code for 'cubemaps'. We start out with some collateral
// code and finally code class cubemap_interpolator.

// get_cube_face determines the cube face, and 2D coordinate on that face.
// it's a specific facet detector and modelled on a vspline::unary_functor,
// but with two result parameters. Incoming we have a 2D target coordinate,
// outgoing a 2D planar in-face coordinate in the range of [ -1 , 1 ] and
// the cube face as an integer index.

template < projection_type TPRJ >
struct get_cube_face
{
  static const int nfacets = 6 ;

  typedef vspline::vector_traits < point_3d_f_type , VSIZE > :: type p3v_t ;
  typedef vspline::vector_traits < coordinate_type , VSIZE > :: type p2v_t ;
  typedef vspline::vector_traits < int , VSIZE > :: type iv_t ;

  // the target_to_ray object will convert incoming 2D target coordinates
  // into 3D ray coordinates specific to this job

  const to_ray_type < TPRJ > ttr ;

  get_cube_face ( const job_type * p_job )
  : ttr ( p_job )
  { }

  // there is no handicap or ranking for cube faces

  float get_handicap ( fc_t f ) const
  {
    return 0.0f ;
  }

  template < typename dtype >
  dtype get_rank ( const vigra::TinyVector < dtype , 2 > & crd ,
                   const fc_t & f ) const
  {
    // for cubemaps, the squared distance is good enough for ranking

    return crd[0] * crd[0] + crd[1] * crd[1] ;
  }

  // single-coordinate overload of cube face detection and coordinate
  // projection.

  void eval ( const coordinate_type & c1 ,
              coordinate_type & c2 ,
              fc_t & face ) const
  {
    // convert incoming target coordinates to 3D ray coordinates

    point_3d_f_type c3 ;
    ttr.eval ( c1 , c3 ) ;

    // pick up x, y, and z in pv's order: z is forward, x is right
    // and y is down

    const auto & z ( c3[0] ) ;
    const auto & x ( c3[1] ) ;
    const auto & y ( c3[2] ) ;

    // for the result, the cubeface-specific 2D coordinates, we also
    // use symbolic references

    auto & u ( c2[0] ) ;
    auto & v ( c2[1] ) ;

    // To find out which coordinate component is 'major', we form the
    // absolute value

    float xx = fabs ( x ) ;
    float zz = fabs ( z ) ;
    float yy = fabs ( y ) ;

    if ( ( xx > zz ) && ( xx > yy ) )
    {
      // xx is greatest
      if ( x >= 0 )
      {
        face = RIGHT ;
        u = - z / xx ;
        v =   y / xx ;
      }
      else
      {
        face = LEFT ;
        u =   z / xx ;
        v =   y / xx ;
      }
    }
    else if ( zz > yy )
    {
      // zz is greatest
      if ( z >= 0 )
      {
        face = FRONT ;
        u =   x / zz ;
        v =   y / zz ;
      }
      else
      {
        face = BACK ;
        u = - x / zz ;
        v =   y / zz ;
      }
    }
    else
    {
      // y is greatest
      if ( y >= 0 )
      {
        face = BOTTOM ;
        u =   x / yy ;
        v = - z / yy ;
      }
      else
      {
        face = TOP ;
        u =   x / yy ;
        v =   z / yy ;
      }
    }
  }

  // given a specific face, produce the 2D 'in-face' coordinate, which
  // ranges from -1 to 1 for both the x and y component. If the incoming
  // coordinate can't be projected to the given cube face, this function
  // returns false and produces the outgoing coordinate [0,0], so that
  // the result can be used for an evaluation without risking an
  // out-of-bounds error.

  bool get_coordinate ( const coordinate_type & c1 ,
                        coordinate_type & c2 ,
                        const fc_t & face ) const
  {
    // convert incoming target coordinates to 3D ray coordinates

    point_3d_f_type c3 ;
    ttr.eval ( c1 , c3 ) ;

    // pick up x, y, and z in pv's order: z is forward, x is right
    // and y is down

    const auto & z ( c3[0] ) ;
    const auto & x ( c3[1] ) ;
    const auto & y ( c3[2] ) ;

    // for the result, the cubeface-specific 2D coordinates, we also
    // use symbolic references

    auto & u ( c2[0] ) ;
    auto & v ( c2[1] ) ;

    u = v = 0 ;

    switch ( face )
    {
      case FRONT:
      {
        if ( z <= 0.0 )
          return false ;
        u =   x / z ;
        v =   y / z ;
        break ;
      }
      case BACK:
      {
        if ( z >= 0.0 )
          return false ;
        u =   x / z ;
        v = - y / z ;
        break ;
      }
      case RIGHT:
      {
        if ( x <= 0.0 )
          return false ;
        u = - z / x ;
        v =   y / x ;
        break ;
      }
      case LEFT:
      {
        if ( x >= 0.0 )
          return false ;
        u = - z / x ;
        v = - y / x ;
        break ;
      }
      case BOTTOM:
      {
        if ( y <= 0.0 )
          return false ;
        u =   x / y ;
        v = - z / y ;
        break ;
      }
      case TOP:
      {
        if ( y >= 0.0 )
          return false ;
        u = - x / y ;
        v = - z / y ;
        break ;
      }
      default:
        return false ;
        break ;
    }

    if ( u < -1.0f || u > 1.0f || v < -1.0f || v > 1.0f )
      return false ;

    return true ;
  }

  // the vectorized version of get_coordinate behaves precisely as
  // the single-pixel variant, returning a mask with the validity
  // values for all coordinates in the vector.

#ifdef VECTORIZE

  mask_type get_coordinate ( const coordinate_v & c1 ,
                             coordinate_v & c2 ,
                             const fc_t & face ) const
  {
    mask_type valid ;

    // convert incoming target coordinates to 3D ray coordinates

    p3v_t c3 ;
    ttr.eval ( c1 , c3 ) ;

    // pick up x, y, and z in pv's order: z is forward, x is right
    // and y is down

    const auto & z ( c3[0] ) ;
    const auto & x ( c3[1] ) ;
    const auto & y ( c3[2] ) ;

    // for the result, the cubeface-specific 2D coordinates, we also
    // use symbolic references

    auto & u ( c2[0] ) ;
    auto & v ( c2[1] ) ;

    u = v = 0 ;

    switch ( face )
    {
      case FRONT:
      {
        valid = mask_type ( z > 0.0 ) ;
        u ( valid ) =   x / z ;
        v ( valid ) =   y / z ;
        break ;
      }
      case BACK:
      {
        valid = mask_type ( z < 0.0 ) ;
        u ( valid ) =   x / z ;
        v ( valid ) = - y / z ;
        break ;
      }
      case RIGHT:
      {
        valid = mask_type ( x > 0.0 ) ;
        u ( valid ) = - z / x ;
        v ( valid ) =   y / x ;
        break ;
      }
      case LEFT:
      {
        valid = mask_type ( x < 0.0 ) ;
        u ( valid ) = - z / x ;
        v ( valid ) = - y / x ;
        break ;
      }
      case BOTTOM:
      {
        valid = mask_type ( y > 0.0 ) ;
        u ( valid ) =   x / y ;
        v ( valid ) = - z / y ;
        break ;
      }
      case TOP:
      {
        valid = mask_type ( y < 0.0 ) ;
        u ( valid ) = - x / y ;
        v ( valid ) = - z / y ;
        break ;
      }
      default:
      {
        return mask_type ( false ) ;
        break ;
      }
    }

    valid &= (    ( u >= -1.0f )
               & ( u <= 1.0f )
               & ( v >= -1.0f )
               & ( v <= 1.0f ) ) ;

    u ( ! valid ) = 0.0f ;
    v ( ! valid ) = 0.0f ;

    return valid ;
  }
 
  // vectorized cubeface detection and coordinate reprojection. Here we use
  // masking instead of conditionals, as ever.

  void eval ( const p2v_t & c1 , p2v_t & c2 , fc_v & fv ) const
  {
    p3v_t c3 ;
    ttr.eval ( c1 , c3 ) ;

    const auto & z ( c3[0] ) ;
    const auto & x ( c3[1] ) ;
    const auto & y ( c3[2] ) ;

    auto & u ( c2[0] ) ;
    auto & v ( c2[1] ) ;

    // initialize fv. If some detected faces are not TOP or BOTTOM,
    // fv will be overwritten there later on

    fv = TOP ;
    fv ( y >= 0 ) = BOTTOM ;

    // obtain absolute values of the components

    auto xx = abs ( x ) ;
    auto zz = abs ( z ) ;
    auto yy = abs ( y ) ;

    // set xmajor true where x is the largest component. such faces will
    // either be LEFT or RIGHT, depending on the sign of x; we take note
    // of those which are RIGHT, keeping the 'right' mask for further down.

    auto xmajor = ( ( xx > zz ) & ( xx > yy ) ) ;
    auto right = ( xmajor & ( x >= 0 ) ) ;

    fv ( xmajor ) = LEFT ;
    fv ( right )  = RIGHT ;

    // if x is not the largest component, either z or y must be. So if
    // z is larger than y, z must be the largest.  Such faces will
    // either be BACK or FRONT, depending on the sign of z. This ends
    // the test, because the remaining entries in fv already have the
    // correct value (TOP/BOTTOM) due to the initialization of fv.
    // Here we keep the 'back' mask for further down.

    auto zmajor = ( ( ! xmajor ) & ( zz > yy ) ) ;
    auto back = ( zmajor & ( z <= 0 ) ) ;

    fv ( zmajor ) = FRONT ;
    fv ( back )   = BACK ;

    // to obtain the 2D coordinates into the individual cube faces,
    // we have to project the 3D coordinates to the cube face's plane
    // by dividing by the absolute value of the 'major' component.
    // finally we have to flip one axis for one of the two faces
    // sharing the same major component. We use the
    // same strategy as for setting the face vector: start out as if
    // there were only top/bottom faces, then overwrite where we find
    // other face values.

    u  =             x / yy ;
    v  =             z / yy ;
    v ( y >= 0 ) = - v ;

    u ( xmajor ) =   z / xx ;
    v ( xmajor ) =   y / xx ;
    u ( right )  = - u ;

    u ( zmajor ) =   x / zz ;
    v ( zmajor ) =   y / zz ;
    u ( back )   = - u ;
  }

#endif

  // calculates the smallest distance-to-margin for coordinate(s)
  // in a facet. currently unused, and for completeness' sake.

  float get_min_d ( const coordinate_type & c_in ,
                    const fc_t & facet ) const
  {
    // to access the facet helper type for the given facet:

    auto min_d = c_in[0] - -1.0f ;
    min_d = std::min ( min_d , 1.0f - c_in[0] ) ;
    min_d = std::min ( min_d , c_in[1] - -1.0f ) ;
    min_d = std::min ( min_d , 1.0f - c_in[1] ) ;

    return min_d ;
  }

#ifdef VECTORIZE

  channel_v get_min_d ( const coordinate_v & c_in ,
                        const fc_t & facet ) const
  {
    // to access the facet helper type for the given facet:

    auto min_d = c_in[0] - -1.0f ;
    min_d = vspline::min ( min_d , 1.0f - c_in[0] ) ;
    min_d = vspline::min ( min_d , c_in[1] - -1.0f ) ;
    min_d = vspline::min ( min_d , 1.0f - c_in[1] ) ;

    return min_d ;
  }

#endif

} ;

// get_in_face_coordinates provides cube face coordinates if the face is
// already known - and, in the vectorized form, if it's the same for all
// incoming ray coordinates.
// The face is introduced as a template argument for maximum performance.

template < fc_t face , projection_type TPRJ >
struct get_in_face_coordinates
{
  static const int nfacets = 6 ;

  typedef vspline::vector_traits < point_3d_f_type , VSIZE > :: type p3v_t ;
  typedef vspline::vector_traits < coordinate_type , VSIZE > :: type p2v_t ;
  typedef vspline::vector_traits < int , VSIZE > :: type iv_t ;

  const to_ray_type < TPRJ > ttr ;

  get_in_face_coordinates ( const job_type * p_job )
  : ttr ( p_job )
  { }

  template < typename T >
  using intermediate_type = typename 
  std::conditional
    < std::is_same < T , coordinate_type > :: value ,
      point_3d_f_type ,
      p3v_t > :: type ;

  // if the cube face is known beforehand, we can use one of
  // these six shortcuts. The idea is to look at the limits
  // in the ray generator and test if all corners land in one face.
  // If so, we can use the shortcut for the whole operation.
  // We might, additionally, split the target array into tiles
  // and process them recursively, to speed up operation on most
  // of the array and only use the multi-facet code on some of it.
  // A recursion limit should keep recursion from becoming too
  // much of an administrative effort.

  template < typename in_type , typename out_type >
  void eval ( const in_type & c1 ,
              out_type & c2 ,
              std::integral_constant < int , RIGHT >
            ) const
  {
    intermediate_type < in_type > c3 ;
    ttr.eval ( c1 , c3 ) ;
    c2[0] = - c3[0] / c3[1] ;
    c2[1] =   c3[2] / c3[1] ;
  }

  template < typename in_type , typename out_type >
  void eval ( const in_type & c1 ,
              out_type & c2 ,
              std::integral_constant < int , LEFT >
            ) const
  {
    intermediate_type < in_type > c3 ;
    ttr.eval ( c1 , c3 ) ;
    c2[0] = - c3[0] / c3[1] ;
    c2[1] = - c3[2] / c3[1] ;
  }

  template < typename in_type , typename out_type >
  void eval ( const in_type & c1 ,
              out_type & c2 ,
              std::integral_constant < int , FRONT >
            ) const
  {
    intermediate_type < in_type > c3 ;
    ttr.eval ( c1 , c3 ) ;
    c2[0] = c3[1] / c3[0] ;
    c2[1] = c3[2] / c3[0] ;
  }

  template < typename in_type , typename out_type >
  void eval ( const in_type & c1 ,
              out_type & c2 ,
              std::integral_constant < int , BACK >
            ) const
  {
    intermediate_type < in_type > c3 ;
    ttr.eval ( c1 , c3 ) ;
    c2[0] =   c3[1] / c3[0] ;
    c2[1] = - c3[2] / c3[0] ;
  }

  template < typename in_type , typename out_type >
  void eval ( const in_type & c1 ,
              out_type & c2 ,
              std::integral_constant < int , BOTTOM >
            ) const
  {
    intermediate_type < in_type > c3 ;
    ttr.eval ( c1 , c3 ) ;
    c2[0] =   c3[1] / c3[2] ;
    c2[1] = - c3[0] / c3[2] ;
  }

  template < typename in_type , typename out_type >
  void eval ( const in_type & c1 ,
              out_type & c2 ,
              std::integral_constant < int , TOP >
            ) const
  {
    intermediate_type < in_type > c3 ;
    ttr.eval ( c1 , c3 ) ;
    c2[0] = - c3[1] / c3[2] ;
    c2[1] = - c3[0] / c3[2] ;
  }

  // dispatch routine, picking the overload appropriate to the
  // template argument 'face'.

  template < typename in_type , typename out_type >
  void eval ( const in_type & c3 ,
              out_type & c2 ) const
  {
    eval ( c3 , c2 , std::integral_constant < int , face > () ) ;
  }
} ;

// yield_priority is a functor which produces priority values for
// given coordinates which are added to a facet's 'rank'. There are
// two types of priority: simple per-facet priority or per-pixel
// priority based on a 'priority map' which occurs when 'include
// masks' are assigned to facets, prioritizing content inside the
// mask so that it's displayed 'on top' of other facets.

struct yield_priority : public tf21_type
{
  const float fixed_value ;
  const std::shared_ptr < vigra::MultiArray < 2 , float > >
    p_priority_map ;
  alpha_ev_type priority_from_map ;
  const domain_t to_image ;

  // setting up the domain depends on the projection, so we need
  // additional logic. We use a static function to set everything up:

private :

  // we have coordinates in model space units coming in, ranging
  // in the source's 'extent', whereas the priority map is pixel-based
  // using image coordinates. 

  static domain_t make_domain ( const source_type & source )
  {
    float x0 = source.full_width ? 0.0f : -0.5f ;
    float y0 = -0.5f ;
    float x1 = source.full_width ? source.width : source.width - 0.5f ;
    float y1 = source.height - 0.5f ;

    return domain_t ( { float(source.extent.x0) ,
                        float(source.extent.y0) } ,
                      { float(source.extent.x1) ,
                        float(source.extent.y1) } ,
                      { x0 , y0 } ,
                      { x1 , y1 } ) ;
  }

public:

  yield_priority ( const source_type & source )
  : fixed_value ( source.handicap ) ,
    to_image ( make_domain ( source ) ) ,
    p_priority_map ( source.p_priority_map )
  {
    // to make sure that access to the priority map does stay
    // within the array's bounds, we clamp incoming image coordinates
    // so that rounding them will certainly produce in-bounds values.
    // Since we are rounding to the nearest discrete coordinate, we
    // use the lowest and highest allowed discrete coordinate as the
    // limits of the clamp gate. The domain used to convert model
    // space to image coordinates uses the precise values, here, for
    // the purpose of clamping, only marginal values will be affected,
    // while the transformation inside the 'allowed' area is precise.

    if ( p_priority_map )
    {
      float x0 = 0.0f ;
      float y0 = 0.0f ;
      float x1 = source.full_width ? source.width
                                   : source.width - 1.0f ;
      float y1 = source.height - 1.0f ;

      auto horizontal_clamp = vspline::clamp < float , VSIZE > ( x0 , x1 ) ;
      auto vertical_clamp = vspline::clamp < float , VSIZE > ( y0 , y1 ) ;
      auto clamp = vspline::mapper < coordinate_type , VSIZE >
                     ( horizontal_clamp , vertical_clamp ) ;
      auto yield = vspline::yield_type < coordinate_type , float , VSIZE >
                     ( *p_priority_map ) ;

      priority_from_map = vspline::grok ( to_image + clamp + yield ) ;
    }
  }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    // a conditional in the eval function looks like a spanner in the
    // works, but since p_priority_map is const, the conditional should
    // be optimized out.

    out = fixed_value ;

    if ( p_priority_map )
    {
      out += priority_from_map ( in ) ;
    }
  }
} ;

// facet_helper inherits from target_to_ray, holding s0, dh etc., but
// rotated to be 'in a specific facet's coordinate system'. What do
// I mean by that? The CS is transformed so that the facet's normal
// becomes (1,0,0).
// On top of the target_to_ray object's values, facet_helper has the
// cosine of the facet images's central axis to the sensor's central
// axis, which is an indicator for it's 'fitness': If this value
// is 1, the facet and the sensor are coplanar and the fitness is
// maximal. If it is -1, the facet and sensor are on opposite sides
// of the origin and fitness is minimal.

template < projection_type projection >
struct facet_helper_type
: public to_ray_type < projection >
{
  // this object handles transformation of incoming target coordinates
  // (2D coordinates pertaining to the target array) to 3D 'ray'
  // coordinates. The ray coordinates produced by this object are
  // in the facet's coordinate system, so, e.g. for rectilinear facets,
  // they are all (1,u,v) where u and v are the in-plane coordinates
  // for the facet, (1,0,0) being the tangent point of the facet
  // with the unit sphere. u grows from left to right, v grows from
  // top to bottom - just like 'normal' image coordinates.

  typedef to_ray_type < projection > tr_t ;

  vspline::grok_type  < vigra::TinyVector < float , 3 > ,
                        vigra::TinyVector < float , 2 > ,
                        VSIZE > ray_to_source ;

  // extent of the facet. We have x0 < u < x1 and y0 < v < y1.

  float x0 , x1 , y0 , y1 ;

  float xm , ym ; // intersection with optical axis

  float socket ;  // hfov-dependent value to raise the 'shallow cone' part
  float rise ;    // height difference of shallow cone part bottom to top
  float drop ;    // height difference of 'steep pyramid' part bottom to top
  float fx , fy ; // factors to map x and y to [-1:1]
  float fr ;      // factor to map radius to [0:1]

  // brightness value, as applied in the interpolator. So the
  // interpolator already provides brightness-corrected values.
  // This datum is for reference

  float brightness ;

  // handicap. This is a value which is added to a facet's 'rank' to move
  // it 'further out'. facets with small handicap will appear 'in front of'
  // facets with larger handicap if the handicap is sufficiently large.
  // Initially I used a fixed value only, but to implement 'include' masks,
  // I changed to using a functor which normally yields a constant (the
  // former fixed 'handicap' value) but can be changed to a functor
  // deriving the handicap value from the given coordinate - e.g. by
  // lookup in a priority map which provides high priority (==low values)
  // for coordinates inside 'include' maks.

  alpha_ev_type priority_ev ;

  template < typename dtype >
  dtype handicap ( const vigra::TinyVector < dtype , 2 > & crd ) const
  {
    return priority_ev ( crd ) ;
  }

  // cosine of facet center to sensor center angle. Since the
  // facet helper is created for a specific frame, the position of
  // the sensor is already known when it is created.

  double fs_cos_theta ;

  // we'll have a std::vector of facet_helper_type, so we need this:

  facet_helper_type ( const tr_t & ttr )
  : tr_t ( ttr )
  { }
} ;

// multi_facet_helper_type provides a std::vector of facet_helper_type
// objects, one for each facet.
// On top of a pointer to the current job, it's c'tor is provided with
// the metrics of the facets (extent, orientation, brightness, see
// facet_helper_type) and two sets of evaluators yielding RGB values
// and alpha values, respectively.
// Setting up a multi_facet_helper_type object depends on the target
// projection (template argument TPRJ). In the general case, we can't
// make any assumptions about the nature of the to_ray_type object,
// so we need to code without 'looking into' it. Further down we have
// a specialization for RECTILINEAR target projection, which allows
// certain optimizations regarding rotation, see there.

template < projection_type TPRJ >
struct multi_facet_helper_type
{
  static constexpr float margin = .00001f ;

  typedef facet_helper_type < TPRJ > fh_t ;

  std::vector < fh_t > facet_helper ;
  int nfacets ;

  // use_rank determines how facet priority is calculated. If use_rank
  // is true, get_rank() will be called and it's result is added with
  // the per-facet handicaps, if any. If use_rank is false, get_rank is
  // not called and priority is strictly by handicap.

  const bool use_rank ;

  // cos_theta_min is a threshold value: the bounding cone of all facets
  // is established, and the largest bounding cone's radial angle 'wins'.
  // This largest angle's cosine is stored here; if, during evaluation,
  // any ray falls outside the cone encoded by this value it can safely
  // be ignored.

  float cos_theta_min ;

  // evaluators for RGB and alpha component

  std::vector < int > facet_number ;
  std::vector < ev_type > evv ;
  std::vector < alpha_ev_type > aevv ;

  multi_facet_helper_type ( const job_type * const p_job ,
                            const std::vector < source_type >
                              & source_v ,
                            const std::vector < quaternion_type >
                              & facet_orientation ,
                            const std::vector < ev_type >
                              & _evv ,
                            const std::vector < alpha_ev_type >
                              & _aevv ,
                            bool may_reorder = true ,
                            bool _use_rank = true
                          )
  : evv ( 0 ) ,
    aevv ( 0 ) ,
    use_rank ( _use_rank )
  {
    const target_type & target ( p_job->target ) ;
    const frame_type & frame ( p_job->frame ) ;

    // We start out with 'facet culling' code: considering the geometry
    // of both the view and the set of facets, we can find facets which
    // can't be 'seen' by the current view. If we form a subset of facets
    // which are visible, we can save a lot of time, because we needn't
    // go through the procedure of obtaining, validating and evaluating
    // coordinates for invisible facets. Culling facets is optional - the
    // code will function even if no invisible facets are removed - so
    // the culling stage is for performance only.
    // For every facet, we have an angle 'theta' which is the (3D) angle
    // between the facet's center and it's furthest corner. So this
    // angle defines a cone around the 3D representation of the facet.
    // We can form a similar cone around the current view. If the angle
    // between the facet's central ray and the sensor's central ray is
    // larger than the sum of the two 'theta' values, the bounding
    // cones of sensor and facet can't intersect, and we can safely
    // forget about the facet.
    // We do the 'bounding cone' test first, because it is quick. If
    // a facet isn't weeded out by the bounding cone test, we use a
    // more involved test based on facet and sensor frustum to possibly
    // weed out more facets: every facet we needn't consider during the
    // evaluation phase reduces computational load significantly, so
    // our effort is well spent.

    double theta_max = 0.0 ;
    int candidate = 0 ;
    int fittest = -1 ;
    double max_fitness = -2.0 ;
    bool facets_with_lc = false ;

    to_ray_type < TPRJ > ttr ( p_job ) ;

    for ( int i = 0 ; i < source_v.size() ; i++ )
    {
      if ( ( frame.solo >= 0 ) && ( i != frame.solo ) )
        continue ;

      // stack processing. facets which aren't 'stack parents' have
      // active == false and are normally excluded. But if stack_only
      // is set, all facets belonging to the designaled stack are
      // 'allowed in', and because the stack parent has active == true,
      // it's also allowed in, so in sum we get the whole stack.

      if ( frame.stack_only > 0 )
      {
        // stack_only is set. This happens only when stacks are fused.
        // only facets from the stack given by frame.stack_only - 1
        // will be processed.

        auto stack_parent = frame.stack_only - 1 ;

        if ( source_v[i].stack_parent != stack_parent )
        {
          // stack_only is set, but this facet doesn't belong to the
          // designated stack coded in stack_only - 1.

          continue ;
        }

        // if the facet does belong to the stack, it's accepted even
        // if it's not set 'active'. This is for fusing stacks; normally
        // non-parent facets are ignored to speed up processing 
      }
      else if ( frame.solo < 0 )
      {
        // stack_only is not set, and we're not in solo mode. But if
        // this is a masking job, we ignore 'active' because that would
        // make us skip all facet members except the stack parent.

        if ( source_v[i].active == false && frame.mask_for == -1 )
        {
          // if we're not processing stacks, we ignore facets which
          // aren't set 'active' to speed up processing.

          continue ;
        }

        // if the facet is active, accept it.
      }

      facets_with_lc |= source_v[i].lens_correction_active ;

      // create a facet_helper_type object from ttr and rotate it
      // to the facet's coordinate system

      fh_t fh ( ttr ) ;
      fh.rotate ( facet_orientation [ i ] ) ;

      // get the radial angle of the facet's bounding cone

      double facet_theta = source_v[i].theta ;

      // If this angle is larger than any previously encountered one,
      // update theta_max

      if ( facet_theta > theta_max )
        theta_max = facet_theta ;

      // find the 'theta' value for the sensor. This is done 'indirectly':
      // We take the center and one corner in *target* coordinates and use
      // the facet's 'to_ray' capability to yield 3D ray coordinates for
      // these two points. Then we can easily get the angle.

      point_2d_d_type corner2 ( -0.5 , -0.5 ) ;
      point_2d_d_type center2 ( ( target.width - 1.0 ) / 2.0 ,
                                ( target.height - 1.0 ) / 2.0 ) ; 

      point_3d_d_type corner ;
      point_3d_d_type center ;

      fh.eval ( corner2 , corner ) ;
      fh.eval ( center2 , center ) ;

      // for fov > pi, hfov/2 exceeds the angle between ul and center.

      double fov_half_max
        = std::max ( target.hfov , target.vfov ) / 2.0 ;

      double sensor_theta
        = std::max ( fov_half_max , angle3d ( corner , center ) ) ;

      point_3d_d_type facet_center ( 1.0 , 0.0 , 0.0 ) ;

      double facet_center_to_sensor_center
              = angle3d ( facet_center , center ) ;

      // we save this datum as this facet's overall 'fitness' value

      fh.fs_cos_theta = cos ( facet_center_to_sensor_center ) ;

      if ( facet_center_to_sensor_center > ( facet_theta + sensor_theta ) )
      {
        // no bounding cone intersection: the angle between the sensor's
        // center and the facet's center is larger than the sum of the
        // radial angles of the bounding cones of the sensor and the
        // facet. We can ignore this facet.

//         std::cout << "bounding cones don't intersect, excluding facet "
//                   << i << std::endl ;
        continue ;
      }

      // The bounding cone test already weeds out many invisible facets.
      // But - especially with elongated facets - some invisible facets
      // are not detected. So the next stage does a frustum intersection
      // test. The test is skipped for 'very large' facets.
      // If the target's hfov is above 2 pi, the frustum is the whole
      // sphere, so the test is futile. vfov greater than pi gets too
      // involved, so I omit that case as well.
      // TODO: this might be elaborated. One idea is to optionally use
      // several frusta to describe the covered area better:
      // non-rectilinear facets can be quite a bit smaller than the
      // single frustum formed from their corner points.

      if (    ( source_v[i].theta < ( M_PI / 2.0 - .00001 ) )
           && ( target.hfov < 2.0 * M_PI && target.vfov < M_PI ) )
      {
        // We'll base the shape of the sensor's frustum on the horizontal
        // and vertical field of view:

        double theta = target.hfov ;
        double phi = target.vfov ;

        if ( theta < M_PI )
        {
          // clamp phi if it's greater than pi

          if ( phi >= M_PI )
            phi = M_PI ;
        }
        else
        {
          // for hfov > pi, we'll use the 'opposite' frustum: the volume
          // left over outside the 'real' frustum.

          theta = ( 2.0 * M_PI ) - theta ;
          if ( phi >= M_PI )
            phi = ( 2.0 * M_PI ) - phi ;
          else
            phi = M_PI ;

          // we mustn't let phi get too close to pi. If the frustum is
          // a little smaller vertically, that's no problem, we'll just
          // not exclude the facet which costs a bit of performance only,
          // whereas a false exclusion mustn't ever happen.

          if ( ( phi + .00001 ) > M_PI )
            phi = M_PI - .00001 ;
        }

        // we'll position the frustum so that is't horizontal size
        // is 2, for convenience of calculation. size 2 gives us
        // dx = 1, hence the distance of the frustum plane from
        // the origin becomes:

        double dx = 1.0 ;
        double dz = dx / tan ( theta / 2.0 ) ;

        if ( dz > .00001 )
        {
          // sanity check. very near pi the maths get imprecise, there
          // we don't do the frustum test.
          // the margin of the frustum is r from the origin:

          double r = sqrt ( dx * dx + dz * dz ) ;

          // and the ratio between this distance and dz gives us
          // a scaling factor

          double scale = r / dz ;

          // in the center of the frustum, we'll mark the height
          // where the ray at vfov/2 passes

          double dy = dz * tan ( phi / 2.0 ) ;

          // using the scaling factor, we can calculate the height
          // of the frustum: the perspective transformation to the
          // frustum plane gives a different height at the margin

          double dym = dy * scale ;

          // For the hfov > pi case, z's sign is opposite:

          if ( target.hfov >= M_PI )
            dz = - dz ;

          // now we have the four corner points of the frustum. The frustum
          // completely contains all rays to the sensor, and towards it's
          // center it also contains more and more rays which pass above and
          // below the sensor. This makes the test pass some facets as visible
          // even if they are in fact not, and the number of false-positives
          // increases with hfov. But the test is nice and straightforward,
          // and doesn't need to much involved 3D geometry.

          point_3d_d_type a ( dz , -dx , - dym ) ;
          point_3d_d_type b ( dz ,  dx , - dym ) ;
          point_3d_d_type c ( dz ,  dx ,   dym ) ;
          point_3d_d_type d ( dz , -dx ,   dym ) ;

          // next we apply the rotation due to the viewer's orientation

          a = rotate_q ( a , frame.orientation ) ;
          b = rotate_q ( b , frame.orientation ) ;
          c = rotate_q ( c , frame.orientation ) ;
          d = rotate_q ( d , frame.orientation ) ;

          // and now we rotate into the facet's CS

          a = rotate_q ( a , facet_orientation[i] ) ;
          b = rotate_q ( b , facet_orientation[i] ) ;
          c = rotate_q ( c , facet_orientation[i] ) ;
          d = rotate_q ( d , facet_orientation[i] ) ;

          a /= norm ( a ) ;
          b /= norm ( b ) ;
          c /= norm ( c ) ;
          d /= norm ( d ) ;

          // the frustum is set up clockwise for hfov < pi and anticlockwise
          // for hfov > pi, and the tests for the two cases are also different:

          if ( target.hfov < M_PI )
          {
            // now check the sensor's frustum and the facet's frustum for
            // intersections (source_v[i] provides the facet's frustum):

            bool intersect =
              frusta_intersect ( a , b , c , d ,
                                source_v[i].ul ,
                                source_v[i].ur ,
                                source_v[i].lr ,
                                source_v[i].ll ) ;

            if ( ! intersect )
            {
//               std::cout << "no frustum intersections, excluding facet "
//                         << i << std::endl ;
              continue ;
            }
          }
          else
          {
            // set up the 'opposite' frustum

            detail::frustum_type fr ( b , a , d , c ) ;

            // if all of the facet's corner points are inside this 'opposite'
            // frustum, the facet can't be visible. Since this frustum is
            // is a convex polyhedron, no connection between any of the points
            // within it's boundaries can intersect any of the space outside
            // it's boundaries.

            if (   ( fr.inside ( source_v[i].ul ) )
                && ( fr.inside ( source_v[i].ur ) )
                && ( fr.inside ( source_v[i].lr ) )
                && ( fr.inside ( source_v[i].ll ) ) )
            {
//               std::cout << "all corners 'below', excluding facet "
//                         << i << std::endl ;
              continue ;
            }
          }
        }
      }

      // If we arrive here, none of the culling tests have removed this
      // facet by issuing a 'continue' statement, so we store the facet
      // helper object.

//       std::cout << "******* passed all tests: using facet "
//                 << i << std::endl ;

      // set the extent of the facet helper object to the facet's extent

      fh.x0 = float ( source_v[i].extent.x0 ) ;
      fh.x1 = float ( source_v[i].extent.x1 ) ;
      fh.y0 = float ( source_v[i].extent.y0 ) ;
      fh.y1 = float ( source_v[i].extent.y1 ) ;

      fh.rise = .07f ;
      fh.socket = ( 1.0f - fh.rise - .05f ) * source_v[i].hfov / ( 2.0 * M_PI ) ;
      fh.drop = 10.0f * ( 1.0f - fh.socket ) ;
      fh.fx = 2.0f / fabs ( fh.x1 - fh.x0 ) ;
      fh.fy = 2.0f / fabs ( fh.y1 - fh.y0 ) ;

      // xm, ym capture the lens shift. to calculate the rank, we need
      // to center the 'shallow cone' on the intersection of the image
      // with the optical axis. The 'steep pyramid' coincides with the
      // image's margins instead.

      if ( source_v[i].lens_correction_active )
      {
        fh.xm = source_v[i].h ;
        fh.ym = source_v[i].v ;
      }
      else
      {
        fh.xm = fh.ym = 0.0 ;
      }

      // the maximal x and y coordinates which can occur also depend
      // on the lens shift.

      auto xmax = std::max ( fabs ( fh.x0 - fh.xm ) ,
                             fabs ( fh.x1 - fh.xm ) ) ;

      auto ymax = std::max ( fabs ( fh.y0 - fh.ym ) ,
                             fabs ( fh.y1 - fh.ym ) ) ;

      auto rmax = sqrt ( xmax * xmax + ymax * ymax ) ;
      fh.fr = 1.0f / rmax ;

      // to realize several source projections, I use a 'grokked' functor
      // to provide the conversion from 3D 'ray' coordinates to 2D source
      // coordinates. This avoids yet another template argument.

      switch ( source_v[i].projection )
      {
        case RECTILINEAR:
          fh.ray_to_source = to_source_type < RECTILINEAR > () ;
          break ;
        case SPHERICAL:
          fh.ray_to_source = to_source_type < SPHERICAL > () ;
          break ;
        case STEREOGRAPHIC:
          fh.ray_to_source = to_source_type < STEREOGRAPHIC > () ;
          break ;
        case CYLINDRIC:
          fh.ray_to_source = to_source_type < CYLINDRIC > () ;
          break ;
        case FISHEYE:
          fh.ray_to_source = to_source_type < FISHEYE > () ;
          break ;
        case MOSAIC:
          fh.ray_to_source = to_source_type < MOSAIC > () ;
          break ;
        default:
          // this should not happen, but having a default is good style
          std::string error
            ( "multi_facet_helper_type: unhandled projection" ) ;
          error += std::to_string ( source_v[i].projection ) ;
          abort_lux ( error ) ;
          break ;
      }

      // we check for optional lens correction parameters

      if ( source_v[i].lens_correction_active )
      {
        lens_correction lc ( source_v[i] ) ;
        fh.ray_to_source = vspline::grok ( fh.ray_to_source + lc ) ;
      }

      // take note of the brightness value

      fh.brightness = source_v[i].brightness ;

      // copy the handicap from source_v, handle 'elevation'

      fh.priority_ev = yield_priority ( source_v[i] ) ;

      // store the facet helper

      facet_helper.push_back ( fh ) ;

      // take note of the facet's number in the complete set
      
      facet_number.push_back ( i ) ;

      // store the corresponding evaluators

      evv.push_back ( _evv [ i ] ) ;

      // some modes require an opaque alpha channel

      bool set_alpha_opaque = false ;

      if ( ( frame.solo != -1 ) && ( i == frame.solo ) && frame.heal )
        set_alpha_opaque = true ;

//       if ( ( frame.elevate != -1 ) && ( i == frame.elevate ) )
//         set_alpha_opaque = true ;

      if ( set_alpha_opaque )
        aevv.push_back ( yield_opaque() ) ;
      else
        aevv.push_back ( _aevv [ i ] ) ;

      // keep track of the facet's overall fitness

      if ( fh.fs_cos_theta > max_fitness )
      {
        // if the current facet is the fittest so far, save it's
        // index (in facet_helper) in 'fittest'.

        max_fitness = fh.fs_cos_theta ;
        fittest = candidate ;
      }

      ++candidate ;
    }

    // The larger theta, the smaller cos_theta:

    cos_theta_min = float ( cos ( theta_max ) ) ;

    nfacets = facet_helper.size() ;

//     std::cout << "culling reduced facets from " << source_v.size()
//               << " to " << nfacets << std::endl ;

    // if we have more than one participating facet and the 'fittest'
    // facet is not in the 'pole position', we put it there. Why so?
    // The 'fittest' facet is most likely to come out as the 'winner'
    // when the eval code looks for the best-matched facet to draw
    // a pixel value from. When it's tested first, it is likely to
    // produce the smallest angle to the sensor's center for a given
    // ray, and subsequently tested facets are likely to come out
    // 'worse', which immediately disqualifies them, without having
    // to perform further tests on them like 'withinness'.
    // If all facets which are 'hit' by the ray participate in the
    // result, sequence does not matter - but if we establish
    // a fixed order of facets like a layer scheme, we may need to
    // switch off this bit of code. For now, our sole ranking criterion
    // is distance of the facet's intersection with the view ray from
    // viewer origin.

    if ( may_reorder && ( nfacets > 1 ) && ( fittest != 0 ) )
    {
      std::swap ( facet_helper[0] , facet_helper[fittest] ) ;
      std::swap ( evv[0] , evv[fittest] ) ;
      std::swap ( aevv[0] , aevv[fittest] ) ;
      std::swap ( facet_number[0] , facet_number[fittest] ) ;
    }

    // as a finishing touch, we add an extra evaluator to evv and aevv
    // which yields zero (black/transparent). This is for outliers: if
    // evaluation is requested for someplace where there is no facet,
    // the facet detection code is set up to return 'nfacets' for the
    // facet number, which will evaluate these extra evaluators, and we
    // needn't do any extra bounds checking.

    evv.push_back ( vspline::grok ( ev_constant < pixel_type > () ) ) ;
    aevv.push_back ( vspline::grok ( ev_constant < float > () ) ) ;
  }
} ;

// specialization of multi_facet_helper_type for mosaic target projection
// with this projection, we can't use the 3D facet culling code.

template<>
struct multi_facet_helper_type < MOSAIC >
{
  static constexpr float margin = .00001f ;

  typedef facet_helper_type < MOSAIC > fh_t ;

  std::vector < fh_t > facet_helper ;
  int nfacets ;

  // use_rank determines how facet priority is calculated. If use_rank
  // is true, get_rank() will be called and it's result is added with
  // the per-facet handicaps, if any. If use_rank is false, get_rank is
  // not called and priority is strictly by handicap.

  const bool use_rank ;

//   // cos_theta_min is a threshold value: the bounding cone of all facets
//   // is established, and the largest bounding cone's radial angle 'wins'.
//   // This largest angle's cosine is stored here; if, during evaluation,
//   // any ray falls outside the cone encoded by this value it can safely
//   // be ignored.
// 
//   float cos_theta_min ;

  // evaluators for RGB and alpha component

  std::vector < int > facet_number ;
  std::vector < ev_type > evv ;
  std::vector < alpha_ev_type > aevv ;

  multi_facet_helper_type ( const job_type * const p_job ,
                            const std::vector < source_type >
                              & source_v ,
                            const std::vector < quaternion_type >
                              & facet_orientation ,
                            const std::vector < ev_type >
                              & _evv ,
                            const std::vector < alpha_ev_type >
                              & _aevv ,
                            bool may_reorder = true ,
                            bool _use_rank = true
                          )
  : evv ( 0 ) ,
    aevv ( 0 ) ,
    use_rank ( _use_rank )
  {
//     std::cout << "mfh: use_rank = " << use_rank << std::endl ;

    const target_type & target ( p_job->target ) ;
    const frame_type & frame ( p_job->frame ) ;

    // We start out with 'facet culling' code: considering the geometry
    // of both the view and the set of facets, we can find facets which
    // can't be 'seen' by the current view. If we form a subset of facets
    // which are visible, we can save a lot of time, because we needn't
    // go through the procedure of obtaining, validating and evaluating
    // coordinates for invisible facets. Culling facets is optional - the
    // code will function even if no invisible facets are removed - so
    // the culling stage is for performance only.
    // For this (mosaic) case, we measure facet-center to sensor-center
    // distance as overall fitness criterion (for reordering).

    double closest = FLT_MAX ;
    int candidate = 0 ;
    int fittest = -1 ;

    to_ray_type < MOSAIC > ttr ( p_job ) ;

    for ( int i = 0 ; i < source_v.size() ; i++ )
    {
      if ( ( frame.solo >= 0 ) && ( i != frame.solo ) )
        continue ;

      // create a facet_helper_type object from ttr and rotate it
      // to the facet's coordinate system

      fh_t fh ( ttr ) ;
      fh.rotate ( facet_orientation [ i ] ) ;

      // determine the transformation from 3D 'ray' coordinates to
      // 2D source coordinates, including lens correction if present

      switch ( source_v[i].projection )
      {
        case MOSAIC: // implicit, target projection mosaic implies it
          if ( source_v[i].lens_correction_active )
          {
            lens_correction lc ( source_v[i] ) ;
            fh.ray_to_source
              = vspline::grok ( to_source_type < MOSAIC > () + lc ) ;
          }
          else
          {
            fh.ray_to_source
              = vspline::grok ( to_source_type < MOSAIC > () ) ;
          }
          break ;
        default:
          // this should not happen, but having a default is good style
          std::string error
            ( "multi_facet_helper_type<MOSAIC>: unhandled projection " ) ;
          error += std::to_string ( source_v[i].projection ) ;
          abort_lux ( error ) ;
          break ;
      }
  
      // the four corners and center of the sensor in 2D

      point_2d_f_type ul2 ( -0.5 , -0.5 ) ;
      point_2d_f_type ur2 ( target.width - 0.5 , -0.5 ) ;
      point_2d_f_type lr2 ( target.width - 0.5 , target.height - 0.5 ) ;
      point_2d_f_type ll2 ( -0.5 , target.height - 0.5 ) ;
      point_2d_f_type center2 ( ( ul2 + lr2 ) / 2.0 ) ; 

      // their 3D equivalent in  model space

      point_3d_f_type ul3 ;
      point_3d_f_type ur3 ;
      point_3d_f_type lr3 ;
      point_3d_f_type ll3 ;
      point_3d_f_type center3 ;

      // is obtained by using the facet_helper object's eval function

      fh.eval ( ul2 , ul3 ) ;
      fh.eval ( ur2 , ur3 ) ;
      fh.eval ( lr2 , lr3 ) ;
      fh.eval ( ll2 , ll3 ) ;
      fh.eval ( center2 , center3 ) ;

      // now we use the ray_to_source function to obtain 2D source
      // coordinates (in model space units)

      point_2d_f_type uls , urs , lrs , lls , cs ;
      fh.ray_to_source.eval ( ul3 , uls ) ;
      fh.ray_to_source.eval ( ur3 , urs ) ;
      fh.ray_to_source.eval ( lr3 , lrs ) ;
      fh.ray_to_source.eval ( ll3 , lls ) ;
      fh.ray_to_source.eval ( center3 , cs ) ;

      // we take note of the distance of the sensor's center to the
      // facets's center (which is at (0,0)

      float dsf = sqrt ( cs[0] * cs[0] + cs[1] * cs[1] ) ;

      // the facet's corners are:

      point_2d_f_type ulf { float ( source_v[i].extent.x0 ) ,
                            float ( source_v[i].extent.y0 ) } ;
      point_2d_f_type urf { float ( source_v[i].extent.x1 ) ,
                            float ( source_v[i].extent.y0 ) } ;
      point_2d_f_type lrf { float ( source_v[i].extent.x1 ) ,
                            float ( source_v[i].extent.y1 ) } ;
      point_2d_f_type llf { float ( source_v[i].extent.x0 ) ,
                            float ( source_v[i].extent.y1 ) } ;

      // and it's extremal coordinates (simplified because we're operating
      // in facet-relative coordinates, so the facet's edges are collinear
      // with the axes:

      float xf_min = std::min ( source_v[i].extent.x0 ,
                                 source_v[i].extent.x1 ) ;

      float xf_max = std::max ( source_v[i].extent.x0 ,
                                 source_v[i].extent.x1 ) ;

      float yf_min = std::min ( source_v[i].extent.y0 ,
                                 source_v[i].extent.y1 ) ;

      float yf_max = std::max ( source_v[i].extent.y0 ,
                                 source_v[i].extent.y1 ) ;

      // for the bounding box of the sensor, we need a bit more calculation,
      // because due to rotation and lens correction, the sensor's rectangle
      // in the facet's CS may be oriented arbitrarily

      #define MIN4(A,B,C,D,I) \
      std::min ( std::min ( A[I] , B[I] ) , std::min ( C[I] , D[I] ) )

      #define MAX4(A,B,C,D,I) \
      std::max ( std::max ( A[I] , B[I] ) , std::max ( C[I] , D[I] ) )

      float xs_min = MIN4 ( uls , urs , lrs , lls , 0 ) ;
      float xs_max = MAX4 ( uls , urs , lrs , lls , 0 ) ;
      float ys_min = MIN4 ( uls , urs , lrs , lls , 1 ) ;
      float ys_max = MAX4 ( uls , urs , lrs , lls , 1 ) ;
      
      // now we can perform the bounding box test:

      bool intersect = (    ( xf_min < xs_max && xf_max > xs_min )
                         && ( yf_min < ys_max && yf_max > ys_min ) ) ;

      if ( ! intersect )
      {
        // no bounding box intersection: We can ignore this facet.

        continue ;
      }

      // The bounding box test already weeds out many invisible facets,
      // and if the facets are not rotated or deformed (lcs!), the test
      // is already sufficient. For other cases we'd need to do a rectangle
      // intersection test, which is omitted for now.

      // TODO: implement the test

      // If we arrive here, none of the culling tests have removed this
      // facet by issuing a 'continue' statement, so we store the facet
      // helper object.

      // set the extent of the facet helper object to the facet's extent

      fh.x0 = float ( source_v[i].extent.x0 ) ;
      fh.x1 = float ( source_v[i].extent.x1 ) ;
      fh.y0 = float ( source_v[i].extent.y0 ) ;
      fh.y1 = float ( source_v[i].extent.y1 ) ;

      fh.rise = .07f ;
      fh.socket = ( 1.0f - fh.rise - .05f ) * source_v[i].hfov / ( 2.0 * M_PI ) ;
      fh.drop = 10.0f * ( 1.0f - fh.socket ) ;
      fh.fx = 2.0f / fabs ( fh.x1 - fh.x0 ) ;
      fh.fy = 2.0f / fabs ( fh.y1 - fh.y0 ) ;

      auto xmax = std::max ( fabs ( fh.x0 ) , fabs ( fh.x1 ) ) ;
      auto ymax = std::max ( fabs ( fh.y0 ) , fabs ( fh.y1 ) ) ;
      auto rmax = sqrt ( xmax * xmax + ymax * ymax ) ;
      fh.fr = 1.0f / rmax ;

      // take note of the brightness value

      fh.brightness = source_v[i].brightness ;

      // copy the handicap from source_v, handle 'elevation'

      fh.priority_ev = yield_priority ( source_v[i] ) ;

      // store the facet helper

      facet_helper.push_back ( fh ) ;

      // take note of the facet's number in the complete set
      
      facet_number.push_back ( i ) ;

      // store the corresponding evaluators

      evv.push_back ( _evv [ i ] ) ;

      // some modes require an opaque alpha channel

      bool set_alpha_opaque = false ;

      if ( ( frame.solo != -1 ) && ( i == frame.solo ) && frame.heal )
        set_alpha_opaque = true ;

//       if ( ( frame.elevate != -1 ) && ( i == frame.elevate ) )
//         set_alpha_opaque = true ;

      if ( set_alpha_opaque )
        aevv.push_back ( yield_opaque() ) ;
      else
        aevv.push_back ( _aevv [ i ] ) ;

      // keep track of the facet's overall fitness

      if ( dsf < closest )
      {
        // if the current facet is the fittest so far, save it's
        // index (in facet_helper) in 'fittest'.

        closest = dsf ;
        fittest = candidate ;
      }

      ++candidate ;
    }

//     std::cout << "culling reduced facets from " << facet_extent.size()
//               << " to " << nfacets << std::endl ;

    nfacets = facet_helper.size() ;

    // if we have more than one participating facet and the 'fittest'
    // facet is not in the 'pole position', we put it there. Why so?
    // The 'fittest' facet is most likely to come out as the 'winner'
    // when the eval code looks for the best-matched facet to draw
    // a pixel value from. When it's tested first, it is likely to
    // produce the smallest angle to the sensor's center for a given
    // ray, and subsequently tested facets are likely to come out
    // 'worse', which immediately disqualifies them, without having
    // to perform further tests on them like 'withinness'.
    // If all facets which are 'hit' by the ray participate in the
    // result, sequence does not matter - but if we establish
    // a fixed order of facets like a layer scheme, we may need to
    // switch off this bit of code. For now, our sole ranking criterion
    // is distance of the facet's intersection with the view ray from
    // viewer origin.

    if ( may_reorder && ( nfacets > 1 ) && ( fittest != 0 ) )
    {
      std::swap ( facet_helper[0] , facet_helper[fittest] ) ;
      std::swap ( evv[0] , evv[fittest] ) ;
      std::swap ( aevv[0] , aevv[fittest] ) ;
    }

    // as a finishing touch, we add an extra evaluator to evv and aevv
    // which yields zero (black/transparent). This is for outliers: if
    // evaluation is requested for someplace where there is no facet,
    // the facet detection code is set up to return 'nfacets' for the
    // facet number, which will evaluate these extra evaluators, and we
    // needn't do any extra bounds checking.

    evv.push_back ( vspline::grok ( ev_constant < pixel_type > () ) ) ;
    aevv.push_back ( vspline::grok ( ev_constant < float > () ) ) ;
  }
} ;

// obtain facet number(s) and in-facet coordinates for 2D target coordinates.
// This variant is for the fully-covered case, meaning that every 3D ray
// is guaranteed to hit a facet image. With this proviso, we can omit
// testing for 'withinness' and save a few cycles. This code is currently
// limited to rectilinear facets.

template < projection_type TPRJ >
struct get_facet_covered
{
  static const projection_type target_projection = TPRJ ;

  fc_t facet ;
  const int nfacets ;
  const multi_facet_helper_type < TPRJ > mfh ;
  const bool alpha_flag ;

  get_facet_covered ( const multi_facet_helper_type < TPRJ > & h ,
                      const int & _facet ,
                      const job_type * p_job )
  : mfh ( h ) ,
    nfacets ( h.nfacets ) ,
    facet ( _facet ) ,
    alpha_flag ( p_job->frame.p_itp->mode == WITH_ALPHA )
  { }

  // calculates the smallest distance-to-margin for coordinate(s)
  // in a facet. This is intended for margin feathering. currently
  // unused.

  float get_min_d ( const coordinate_type & c_in ,
                    const fc_t & facet ) const
  {
    // to access the facet helper type for the given facet:

    const auto & ttr ( mfh.facet_helper [ facet ] ) ;

    auto min_d = c_in[0] - ttr.x0 ;
    min_d = std::min ( min_d , ttr.x1 - c_in[0] ) ;
    min_d = std::min ( min_d , c_in[1] - ttr.y0 ) ;
    min_d = std::min ( min_d , ttr.y1 - c_in[1] ) ;

    return min_d ;
  }

#ifdef VECTORIZE

  channel_v get_min_d ( const coordinate_v & c_in ,
                        const fc_t & facet ) const
  {
    // to access the facet helper type for the given facet:

    const auto & ttr ( mfh.facet_helper [ facet ] ) ;

    auto min_d = c_in[0] - ttr.x0 ;
    min_d = vspline::min ( min_d , ttr.x1 - c_in[0] ) ;
    min_d = vspline::min ( min_d , c_in[1] - ttr.y0 ) ;
    min_d = vspline::min ( min_d , ttr.y1 - c_in[1] ) ;

    return min_d ;
  }

#endif

  template < typename dtype >
  dtype get_handicap ( const vigra::TinyVector < dtype , 2 > & crd ,
                       const fc_t & f ) const
  {
    return mfh.facet_helper[f].handicap ( crd ) ;
  }

  template < typename dtype >
  dtype get_rank ( const vigra::TinyVector < dtype , 2 > & crd ,
                   const fc_t & f ) const
  {
    if ( ! mfh.use_rank )
      return get_handicap ( crd , f ) ;

    const auto & fh ( mfh.facet_helper[f] ) ;

    // r is the radius, scaled so it's 1 at most.

    auto cx = crd[0] - fh.xm ;
    auto cy = crd[1] - fh.ym ;

    // quick shot, may make r > 1 which may cause trouble...

    dtype r = fh.fr * sqrt ( cx * cx + cy * cy ) ;

    // the 'shallow cone' is at least 'socket' high and rises very
    // gently with the radius, so that it's never more than
    // fh.socket + fh.rise.

    dtype shallow = fh.socket + r * fh.rise ;

    // xn, yn are scaled coordinates so that they are 1 at most

    dtype xn = abs ( crd[0] ) * fh.fx ;
    dtype yn = abs ( crd[1] ) * fh.fy ;

    // dn is the distance of the higher one of the two to the margin
    // so that dn is 0 for marginal coordinates

    dtype dn = 1.0f - vspline::max ( xn , yn ) ;

    // 'steep' falls of rapidly from 1.0 at the margin

    dtype steep = 1.0f - dn * fh.drop ;

    // the result is the maximum of steep and shallow, so that the
    // result drops rapidly from 1.0 at the margin to where the
    // 'steep pyramid' intersects with the 'shallow' cone, and drops
    // slowly from there towards the center, which has the lowest
    // value: socket.

    dtype result = vspline::max ( steep , shallow ) ;
    
    // an additional handicap modifies the rank value. This may be
    // due to per-facet prioritization or per-pixel due to 'include
    // masks'. 'exclude masks' are handled by alpha channel manipulation,
    // the have no effect on priority values.

    result += get_handicap ( crd , f ) ;

    return result ;
  }

  // single-coordinate variant of face detection and coordinate
  // projection. This is the 'best facet wins' approach, which will
  // result in a hard discontinuity in the facetted image: each pixel
  // is from a specific facet, which may change from one pixel to the
  // next. With synthetic facets, which are made specifically to fit
  // 'perfectly', such hard discontinuities are not a (grave) problem,
  // but for registered image sets, the fit is rarely perfect and the
  // discontinuities are visible. For these cases, we have further
  // member functions get_coordinate, which produce in-facet coordinates
  // for a specific facet. The calling code can then do it's own ranking,
  // blending, etc - and produce a result without discontinuities by, eg.
  // feathering, which may require evaluation of several facets to
  // combine into a result.

  // fixed facet unvectorized code
  // here, the facet stored in the member variable 'facet' is used, and
  // we have no 'facet' output parameter.

  void eval ( const coordinate_type & c_in ,
              coordinate_type & c_out ) const
  {
    point_3d_f_type c3 ;

    const auto & ttr ( mfh.facet_helper [ facet ] ) ;
    ttr.eval ( c_in , c3 ) ;

    c_out[0] = c3 [ 1 ] / fabs ( c3 [ 0 ] ) ;
    c_out[1] = c3 [ 2 ] / fabs ( c3 [ 0 ] ) ;
  }


  // fixed facet code

#ifdef VECTORIZE

  void eval ( const coordinate_v & c_in ,
              p2v_t & c_out ) const
  {
    const auto & ttr ( mfh.facet_helper [ facet ] ) ;
    p3v_t c3 ;
    ttr.eval ( c_in , c3 ) ;

    c_out[0] = c3 [ 1 ] / abs ( c3 [ 0 ] ) ;
    c_out[1] = c3 [ 2 ] / abs ( c3 [ 0 ] ) ;
  }

#endif

  // given a specific facet, produce the 2D coordinate in the plane of
  // this facet if this in-plane coordinate is inside the facet's boundaries,
  // and return true. If the ray does not hit the facet, return false, and
  // set the result coordinate to (0,0)

  bool get_coordinate ( const coordinate_type & c_in ,
                        coordinate_type & c_out ,
                        const fc_t & facet ) const
  {
    // intermediate 3D ray coordinate

    point_3d_f_type c3 ;

    // for the result, the face-specific 2D coordinates,
    // we use symbolic references

    auto & u ( c_out[0] ) ;
    auto & v ( c_out[1] ) ;

    u = v = 0 ;

    // to access the facet helper type for the given facet:

    const auto & ttr ( mfh.facet_helper [ facet ] ) ;

    // evaluating it with c_in yields c3

    ttr.eval ( c_in , c3 ) ;

    // symbolic names for the 3D coordinate's components
    // pick up x, y, and z in pv's order: z is forward, x is right
    // and y is down

    const auto & z1 ( c3[0] ) ;
    const auto & x1 ( c3[1] ) ;
    const auto & y1 ( c3[2] ) ;

    // z1 <= 0 means c3 is 'behind' the origin, hence invisible

    if ( z1 <= 0.0 )
      return false ;

    // project x1 and x1 to the facet's plane which is z==1

    u = x1 / z1 ;
    v = y1 / z1 ;

    // test for 'withinness': are the prospective coordinates within
    // the 'extent' of the facet?

    if (    u > ttr.x0
         && u < ttr.x1
         && v > ttr.y0
         && v < ttr.y1 )
      return true ;

    return false ;
  }

  // vectorized version, using masks instead of booleans

#ifdef VECTORIZE

  mask_type get_coordinate ( const coordinate_v & c_in ,
                             coordinate_v & c_out ,
                             const fc_t & facet ) const
  {
    p3v_t c3 ;
    const auto & ttr ( mfh.facet_helper [ facet ] ) ;
    ttr.eval ( c_in , c3 ) ;

    const auto & z ( c3[0] ) ;
    const auto & x ( c3[1] ) ;
    const auto & y ( c3[2] ) ;

    auto & u ( c_out[0] ) ;
    auto & v ( c_out[1] ) ;

    u = v = 0 ;

    // conservative code here to avoid division by zero. If that is
    // not a problem, one might just go ahead and do the division, then
    // mask out later

    mask_type valid ( z > 0.0f ) ;
    auto zz ( z ) ;
    zz ( ! valid ) = 1.0f ;

    u = x / zz ;
    v = y / zz ;
    u ( ! valid ) = 0.0f ;
    v ( ! valid ) = 0.0f ;

    valid &= (   ( u > ttr.x0 )
               & ( u < ttr.x1 )
               & ( v > ttr.y0 )
               & ( v < ttr.y1 ) ) ;

    u ( ! valid ) = 0.0f ;
    v ( ! valid ) = 0.0f ;

    return valid ;
  }

#endif

} ;

// obtain facet number(s) and in-facet coordinates for 2D coordinates.
// This variant is for the partially-covered case, meaning that 3D rays
// may or may not hit a facet image. The c'tor receives an additional
// argument 'cos_theta_threshold' which helps to weed out 'unrealistic'
// results: if the cosine of the angle between a facet's axis and the
// incoming ray is below the threshold, the facet is not considered a
// possible match.
// Note how this object does not provide fixed-facet code: if the ray
// does not touch any facet, the calling code should deal with the
// situation. Fixed-facet code where a facet is touched would be the same
// as in get_facet_covered, so the caller can use that object instead.

// this object now handles several projections.

template < projection_type TPRJ >
struct get_facet_from_crd
{
  static const projection_type target_projection = TPRJ ;

  const int nfacets ;
  const multi_facet_helper_type < TPRJ > mfh ;
  const bool alpha_flag ;

  get_facet_from_crd ( const multi_facet_helper_type < TPRJ > & h ,
                       const job_type * p_job )
  : mfh ( h ) ,
    nfacets ( h.nfacets ) ,
    alpha_flag ( p_job->frame.p_itp->mode == WITH_ALPHA )
  { }

  // calculates the smallest distance-to-margin for coordinate(s)
  // in a facet. This is intended for margin feathering. currently
  // unused.

  float get_min_d ( const coordinate_type & c_in ,
                    const fc_t & facet ) const
  {
    // to access the facet helper type for the given facet:

    const auto & ttr ( mfh.facet_helper [ facet ] ) ;

    auto min_d = c_in[0] - ttr.x0 ;
    min_d = std::min ( min_d , ttr.x1 - c_in[0] ) ;
    min_d = std::min ( min_d , c_in[1] - ttr.y0 ) ;
    min_d = std::min ( min_d , ttr.y1 - c_in[1] ) ;

    return min_d ;
  }

#ifdef VECTORIZE

  channel_v get_min_d ( const coordinate_v & c_in ,
                        const fc_t & facet ) const
  {
    // to access the facet helper type for the given facet:

    const auto & ttr ( mfh.facet_helper [ facet ] ) ;

    auto min_d = c_in[0] - ttr.x0 ;
    min_d = vspline::min ( min_d , ttr.x1 - c_in[0] ) ;
    min_d = vspline::min ( min_d , c_in[1] - ttr.y0 ) ;
    min_d = vspline::min ( min_d , ttr.y1 - c_in[1] ) ;

    return min_d ;
  }

#endif

  template < typename dtype >
  dtype get_handicap ( const vigra::TinyVector < dtype , 2 > & crd ,
                       const fc_t & f ) const
  {
    return mfh.facet_helper[f].handicap ( crd ) ;
  }

  template < typename dtype >
  dtype get_rank ( const vigra::TinyVector < dtype , 2 > & crd ,
                   const fc_t & f ,
                   std::false_type ) const
  {
    if ( ! mfh.use_rank )
      return get_handicap ( crd , f ) ;

    const auto & fh ( mfh.facet_helper[f] ) ;

    // r is the radius, scaled so it's 1 at most.

    auto cx = crd[0] - fh.xm ;
    auto cy = crd[1] - fh.ym ;

    // quick shot, may make r > 1 which may cause trouble...

    dtype r = fh.fr * sqrt ( cx * cx + cy * cy ) ;
    dtype shallow = fh.socket + r * fh.rise ;
    dtype xn = abs ( crd[0] ) * fh.fx ;
    dtype yn = abs ( crd[1] ) * fh.fy ;
    dtype dn = 1.0f - vspline::max ( xn , yn ) ; // distance to margin
    dtype steep = 1.0f - dn * fh.drop ;

    dtype result = vspline::max ( steep , shallow ) ;
    
    // an additional handicap increases the rank value

    result += get_handicap ( crd , f ) ;

    return result ;
  }

  template < typename dtype >
  dtype get_rank ( const vigra::TinyVector < dtype , 2 > & crd ,
                   const fc_t & f ,
                   std::true_type // for mosaic projection
                 ) const
  {
    if ( ! mfh.use_rank )
      return get_handicap ( crd , f ) ;

    dtype result = sqrt ( crd[0] * crd[0] + crd[1] * crd[1] ) ;
    
    // an additional handicap increases the rank value

    result += get_handicap ( crd , f ) ;

    return result ;
  }
  
  template < typename dtype >
  dtype get_rank ( const vigra::TinyVector < dtype , 2 > & crd ,
                   const fc_t & f ) const
  {
    return get_rank ( crd , f ,
                      typename std::conditional < TPRJ == MOSAIC ,
                                                  std::true_type ,
                                                  std::false_type > :: type() ) ;
  }

  // single-coordinate variant of face detection and coordinate
  // projection. This is the 'best facet wins' approach, which will
  // result in a hard discontinuity in the facetted image: each pixel
  // is from a specific facet, which may change from one pixel to the
  // next. With synthetic facets, which are made specifically to fit
  // 'perfectly', such hard discontinuities are not a (grave) problem,
  // but for registered image sets, the fit is rerely perfect and the
  // discontinuities are visible. For these cases, we have further
  // member functions get_coordinate, which produce in-facet coordinates
  // for a specific facet. The calling code can then do it's own ranking,
  // blending, etc - and produce a result without discontinuities by, eg.
  // feathering, which may require evaluation of several facets to
  // combine into a result.

  // given a specific facet, produce the 2D coordinate in the plane of
  // this facet, also making sure it's inside the facet's borders. If
  // the incoming coordinate can't be mapped to a coordinate inside the
  // facet's borders, return false, else return true.
  // The resulting coordinate will be set to (0,0) if the mapping is
  // not possible.

  bool get_coordinate ( const coordinate_type & c1 ,
                        coordinate_type & c2 ,
                        const fc_t & facet ) const
  {
    // convert 2D target to 3D ray coordinates

    point_3d_f_type c3 ;

    // for the result, the face-specific 2D coordinates, we also
    // use symbolic references

    auto & u ( c2[0] ) ;
    auto & v ( c2[1] ) ;

    u = v = 0 ;
    const auto & ttr ( mfh.facet_helper [ facet ] ) ;
    ttr.eval ( c1 , c3 ) ;

    // symbolic names

    const auto & z1 ( c3[0] ) ;
    const auto & x1 ( c3[1] ) ;
    const auto & y1 ( c3[2] ) ;

    // get the source coordinate

    ttr.ray_to_source.eval ( c3 , c2 ) ;

    // test for 'withinness'

    if (     u >= ttr.x0
          && u <= ttr.x1
          && v >= ttr.y0
          && v <= ttr.y1 )
    {
      return true ;
    }

    u = v = 0 ;
    return false ;
  }

  // vectorized version

#ifdef VECTORIZE

  mask_type get_coordinate ( const coordinate_v & c1 ,
                             coordinate_v & c2 ,
                             const fc_t & facet ) const
  {
    // intermediate 3D ray coordinate and facet helper

    p3v_t c3 ;
    const auto & ttr ( mfh.facet_helper [ facet ] ) ;

    // evaluating the facet helper produces the 'ray' coordinate

    ttr.eval ( c1 , c3 ) ;

    // pick up x, y, and z in pv's order: z is forward, x is right
    // and y is down

    const auto & z ( c3[0] ) ;
    const auto & x ( c3[1] ) ;
    const auto & y ( c3[2] ) ;

    // for the result, the facet-specific 2D coordinates, we also
    // use symbolic references

    auto & u ( c2[0] ) ;
    auto & v ( c2[1] ) ;

    ttr.ray_to_source.eval ( c3 , c2 ) ;

    auto valid  = (   ( u >= ttr.x0 )
                    & ( u <= ttr.x1 )
                    & ( v >= ttr.y0 )
                    & ( v <= ttr.y1 ) ) ;

    u ( ! valid ) = 0.0f ;
    v ( ! valid ) = 0.0f ;

    return valid ;
  }

#endif

} ;

// single_facet_type is used for cases where all evaluations use the same
// facet of a multi-facet image. Here, we only have a single evaluator
// for the specific facet, and a functor projecting 3D ray coordinates
// to facet-specific 2D coordinates.
// This is an optimization: if the calling code determines that all
// target pixels will 'draw' from just a single face(t), it's futile to
// do execute any ranking code. Since the detection of the special case
// can be done during 'setup' time rather than during the evaluation of
// the pixel pipeline, detection can be expensive without undue runtime
// effects. The code is simple, but requires a specialized object yieling
// the source coordinate only for the one facet. This object has to be
// instantiated 'further up'. This object's type is the first template
// argument, and the second is the type of the evaluator used to produce
// a pixel from a coordinate - some type of interpolator.

template < class face_project_type >
struct single_facet_type
: public tf23_type
{
  typedef tf23_type base_type ;

  const int nfacets ;

  const face_project_type face_project ;
  const ev_type ev ;

  single_facet_type ( const face_project_type & _face_project ,
                      const ev_type & _ev )
  : face_project ( _face_project ) ,
    ev ( _ev ) ,
    nfacets ( _face_project.nfacets )
  { }

  void eval ( const coordinate_type & in ,
              pixel_type & out ) const
  {
    coordinate_type crd ;
    face_project.eval ( in , crd ) ;
    ev.eval ( crd , out ) ;
  }

#ifdef VECTORIZE

  using typename base_type::in_v ;

  void eval ( const coordinate_v & in ,
              pixel_v & out ) const
  {
    coordinate_v crd ;
    face_project.eval ( in , crd ) ;
    ev.eval ( crd , out ) ;
  }

#endif

} ;

// multi_facet_type uses a facet detector to obtain facet index and
// 2D coordinate for each incoming 3D coordinate, then proceeds to
// evaluate the facet-specific evaluator to obtain the result pixel.
// If all evaluations will happen in only one fixed facet, we'll pick
// 'single_facet_type' instead (see above)
// multi_facet_type can use several facet detectors: get_cube_face
// for cubemaps and get_facet_covered and get_facet_from_crd for
// facet maps. For cubemaps, there is a specialized version below,
// which exploits the fact that cubemaps can determine the required
// cube face very efficiently without calling get_coordinate once for
// each face and comparing the results.
// So here we have the application of the 'fittest facet wins' strategy:
// The code selects the fittest facet and finds the in-facet coordinate
// pertaining to this facet. This coordinate is then evaluated with the
// evaluator for the specific facet - picked from the vector of evaluators
// for all in-play facets.

template < class facet_detect_type >
struct multi_facet_type
: public tf23_type
{
  typedef tf23_type base_type ;

  const facet_detect_type facet_detect ;
  const std::vector < ev_type > evv ;
  const int nfacets ;
  const bool equiface ;
  
  multi_facet_type ( const facet_detect_type & _facet_detect ,
                     const std::vector < ev_type > & _evv ,
                     bool _equiface = false )
  : facet_detect ( _facet_detect ) ,
    evv ( _evv ) ,
    nfacets ( _facet_detect.nfacets ) ,
    equiface ( _equiface )
  { }

  // single-pixel operation is simple: obtain the right facet, then
  // evaluate with the evaluator for it. Here, for the general case,
  // we call get_coordinate for every in-play facet, compare the
  // results, and evaluate in the 'winner' facet.

  void eval ( const coordinate_type & in ,
              pixel_type & out ) const
  {
    fc_t facet = nfacets ;
    coordinate_type crd ;
    float min_rank = FLT_MAX ;

    for ( fc_t f = 0 ; f < nfacets ; f++ )
    {
      coordinate_type c ;
      bool valid = facet_detect.get_coordinate ( in , c , f ) ;

      if ( ! valid )
        continue ;

      auto rank = facet_detect.get_rank ( c , f ) ;

      if ( rank < min_rank )
      {
        facet = f ;
        min_rank = rank ;
        crd = c ;
      }
    }

    // relying of facet still being == nfacets if no facets show

    evv [ facet ] . eval ( crd , out ) ;
  }

  // vectorized operation is more complex, but due to the nature of
  // facetted source images, it's most likely that all coordinates will
  // fall into a single facet, so we test for that special case first.
  // If the test succeeds, we gain a lot because we can use a fast path.
  // If the coordinates span several facets, we test for each facet whether
  // any cordinates lie in it: it's most likely only a small number of
  // facets is involved, so this approach performs better than running
  // a single-pixel evaluation for all coordinates in the vector, which
  // is attractive because there are less conditionals.

  // TODO: test these claims

#ifdef VECTORIZE

  using typename base_type::in_v ;

  void eval ( const in_v & in ,
              pixel_v & out ) const
  {
    fc_v facet ( nfacets ) ;
    coordinate_v place { 0.0f , 0.0f } ;
    channel_v min_rank ( FLT_MAX ) ;

    for ( fc_t f = 0 ; f < nfacets ; f++ )
    {
      coordinate_v crd ;
      mask_type valid = facet_detect.get_coordinate ( in , crd , f ) ;
      auto rank = facet_detect.get_rank ( crd , f ) ;

      mask_type better ( rank < min_rank ) ;
      better &= valid ;

      if ( any_of ( better ) )
      {
        facet ( better ) = f ;
        min_rank ( better ) = rank ;
        place[0] ( better ) = crd[0] ;
        place[1] ( better ) = crd[1] ;
      }
    }

    fc_t facet0 = facet[0] ;
    if ( all_of ( facet == facet0 ) )
    {
      // all coordinates are on the same facet! Winner. evaluate directly
      // from 'place' to 'out' and we're done. Note that the 'facet' value
      // may be == nfacets, in which case the 'extra' evaluator evaluating
      // to zero is picked.

      evv [ facet0 ] . eval ( place , out ) ;
    }
    else
    {
      // multi-facet eval

      pixel_v help ;

      for ( int f = 0 ; f < nfacets ; f++ )
      {
        auto mask = ( facet == f ) ;
        if ( any_of ( mask ) )
        {
          // if the facets aren't equally sized, additional code is needed
          // to avoid evaluation where the mask is not set:

          if ( equiface )
          {
            // equifacet means that all coordinates (after projection into
            // the facets) share the same range, so every coordinate can
            // be evaluated for every facet. If this is the case, we can
            // simply do vectorized evaluations with 'place' on all facets,
            // then combine the results by a masked copy to 'out'.
            // When we're dealing with cubemaps, equifacet is true.

            evv [ f ] . eval ( place , help ) ;
          }
          else
          {
            // if the facets accept different coordinate ranges, we start
            // out with a zeroed-out coordinate vector and only transfer
            // coordinates to it where the mask is set. Evaluating the
            // resulting coordinate vector is safe, and again the result is
            // transferred to 'out' with a masked copy.
            // Note: zero coordinate is safe because that's precisely the
            // tangent point, which coincides with the facet image's center.

            coordinate_v safe_place { 0.0f , 0.0f } ;
            safe_place[0] ( mask ) = place[0] ;
            safe_place[1] ( mask ) = place[1] ;
            evv [ f ] . eval ( safe_place , help ) ;
          }
          // a masked copy to 'result' accumulates interpolated pixel
          // values from all involved facets in 'out'

          out[0] ( mask ) = help[0] ;
          out[1] ( mask ) = help[1] ;
          out[2] ( mask ) = help[2] ;
        }
      }
    }
  }

#endif

} ;

// partial specialization for cubemaps. the face detector type used
// here has very efficient code to determine the right cube face(s),
// which we exploit by coding the eval functions differently. This
// code was originally used for all face(t) detectors, but now I've
// removed the 'eval' functions in code for facet maps and use only
// get_coordinate instead, which makes the code more compact.

template < projection_type TPRJ >
struct multi_facet_type < get_cube_face < TPRJ > >
: public tf23_type
{
  typedef tf23_type base_type ;

  const get_cube_face < TPRJ > face_detect ;
  const std::vector < ev_type > evv ;
  const int nfacets ;
  const bool equiface ;
  
  multi_facet_type ( const get_cube_face < TPRJ > & _face_detect ,
                     const std::vector < ev_type > & _evv ,
                     bool _equiface = true )
  : face_detect ( _face_detect ) ,
    evv ( _evv ) ,
    nfacets ( _face_detect.nfacets ) ,
    equiface ( _equiface )
  { }

  // single-pixel operation is simple: obtain the right facet, then
  // evaluate with the evaluator for it.

  void eval ( const coordinate_type & in ,
              pixel_type & out ) const
  {
    fc_t face ;
    coordinate_type crd ;

    face_detect.eval ( in , crd , face ) ;
    evv [ face ] . eval ( crd , out ) ;
  }

  // vectorized operation is more complex, but due to the nature of
  // facetted source images, it's most likely that all coordinates will
  // fall into a single facet, so we test for that special case first.
  // If the test succeeds, we gain a lot because we can use a fast path.
  // If the coordinates span several facets, we test for each facet whether
  // any cordinates lie in it: it's most likely only a small number of
  // facets is involved, so this approach performs better than running
  // a single-pixel evaluation for all coordinates in the vector, which
  // is attractive because there are less conditionals.

  // TODO: test these claims

#ifdef VECTORIZE

  using typename base_type::in_v ;

  void eval ( const in_v & in ,
              pixel_v & out ) const
  {
    fc_v face ;
    coordinate_v place ;
    face_detect.eval ( in , place , face ) ;

    fc_t face0 = face[0] ;
    if ( all_of ( face == face0 ) )
    {
      // all coordinates are on the same face! Winner. evaluate directly
      // from 'place' to 'out' and we're done.

      evv [ face0 ] . eval ( place , out ) ;
    }
    else
    {
      // multi-face eval

      pixel_v help ;

      for ( int f = 0 ; f < nfacets ; f++ )
      {
        auto mask = ( face == f ) ;
        if ( any_of ( mask ) )
        {
          // if the faces aren't equally sized, additional code is needed
          // to avoid evaluation where the mask is not set:

          if ( equiface )
          {
            // equiface means that all coordinates (after projection into
            // the facets) share the same range, so every coordinate can
            // be evaluated for every facet. If this is the case, we can
            // simply do vectorized evaluations with 'place' on all facets,
            // then combine the results by a masked copy to 'out'.
            // When we're dealing with cubemaps, equiface is true.

            evv [ f ] . eval ( place , help ) ;
          }
          else
          {
            // if the facets accept different coordinate ranges, we start
            // out with a zeroed-out coordinate vector and only transfer
            // coordinates to it where the mask is set. Evaluating the
            // resulting coordinate vector is safe, and again the result is
            // transferred to 'out' with a masked copy.
            // Note: zero coordinate is safe because that's precisely the
            // tangent point, which coincides with the facet image's center.

            coordinate_v safe_place { 0.0f , 0.0f } ;
            safe_place[0] ( mask ) = place[0] ;
            safe_place[1] ( mask ) = place[1] ;
            evv [ f ] . eval ( safe_place , help ) ;
          }
          // a masked copy to 'result' accumulates interpolated pixel
          // values from all involved facets in 'out'

          out[0] ( mask ) = help[0] ;
          out[1] ( mask ) = help[1] ;
          out[2] ( mask ) = help[2] ;
        }
      }
    }
  }

#endif

} ;

// next we extend the code to deal with data carrying an alpha channel.
// This is a considerable complication, because it's no longer enough
// to merely have a 'winner' facet: if the 'winner' pixel is not fully
// opaque, we need to do alpha blending with any content which may
// 'shine through' from facets 'further out'.

// for the vectorized version of 'eval' we need a bit of helper code.
// we'll have to deal with situations where we evaluate several facets,
// yielding sets of vectors of pixels, one pixel vector per facet.
// The helper code is to gather pixels from several of these vectors
// according to some index vector holding the facet numbers to use.

// masked swap of two vectorized values. sawps only where mask is true

template < typename v_t , typename m_t >
void swap ( v_t & lhs , v_t & rhs , m_t mask )
{
  v_t help ( lhs ) ;
  lhs ( mask ) = rhs ;
  rhs ( mask ) = help ;
}

#ifdef VECTORIZE

// given a float* 'src' to an array holding float vectors, pick those
// values into target which are taken from the vector given by 'where',
// which holds vector indices. So this is a gather operation.
// This float vector code will be used for alpha values.

void pick ( float * src , fc_v where , channel_v & target )
{
  static const fc_t vsz = (unsigned short) VSIZE ;
// #ifdef USE_VC
//   static const auto ix0 = fc_v::IndexesFromZero() ;
// #else
  static const auto ix0 = fc_v::iota() ;
// #endif
  fc_v indexes ;
  vspline::assign ( indexes , ix0 ) ;

  target = channel_v ( src , indexes + ( vsz * where ) ) ;
}

// given a float* src to an array holding pixel vectors, pick those
// pixel values into target which are taken from the vector given
// by 'where', which holds vector indices. Also a gather operation.
// This code is for the RGB part of the operation.

void pick ( float * src , fc_v where , pixel_v & target )
{
  static const fc_t vsz = (unsigned short) VSIZE ;
// #ifdef USE_VC
//   static const auto ix0 = fc_v::IndexesFromZero() ;
// #else
  static const auto ix0 = fc_v::iota() ;
// #endif
  fc_v indexes ;
  vspline::assign ( indexes , ix0 ) ;

  target [ 0 ] = channel_v ( src , indexes + vsz * 3 * where ) ;
  target [ 1 ] = channel_v ( src + vsz , indexes + vsz * 3 * where ) ;
  target [ 2 ] = channel_v ( src + vsz + vsz , indexes + vsz * 3 * where ) ;
}

#endif

/// this variant of multi_facet_type is for processing data with
/// alpha channel. As usual, we have the alpha pipeline separate
/// from the RGB pipeline. We expect a set of alpha functors in
/// 'aevv', on top of the RGB functors in evv.
/// Incoming we have 3D 'ray' coordinates, outgoing the assembled
/// RGBA pixels with alpha channel in the fourth component.
/// Handling transparency is quite expensive, but the code tries to
/// reduce the use of alpha blending as much as possible by weeding
/// out cases which can be dealt with more easily. So the run-time
/// penalty is corresponding rather with the area where transparency
/// effects take place, whereas areas with opaque 'winner' facet do
/// not cost much more processing than is used in the non-alpha code.
/// I don't provide a specialization for cubemaps here, bcause cubemaps
/// are really intended as a display format, so the cube faces will
/// typically not have transparency. The code will process cubemaps
/// with aplha channel, but cubeface detection will not use specific
/// code, so cubemaps with alpha channel will render significantly
/// slower than cubemaps without alpha channel, about as fast as
/// if they had been presented as facet maps.

template < class facet_detect_type >
struct multi_facet_type_4
: public tf24_type
{
  typedef tf24_type base_type ;

  using base_type::in_v ;
  using base_type::out_ele_v ;

  const facet_detect_type facet_detect ;
  const std::vector < ev_type > evv ;
  const std::vector < alpha_ev_type > aevv ;
  const int nfacets ;
  const bool equiface ;
  
  // node which is stored as value in the multimap used for z-buffering.

  struct node_type
  {
    float alpha ;
    fc_t facet ;
    coordinate_type crd ;
  } ;

  multi_facet_type_4 ( const facet_detect_type & _facet_detect ,
                       const std::vector < ev_type > & _evv ,
                       const std::vector < alpha_ev_type > & _aevv ,
                       bool _equiface = false )
  : facet_detect ( _facet_detect ) ,
    evv ( _evv ) ,
    aevv ( _aevv ) ,
    nfacets ( _facet_detect.nfacets ) ,
    equiface ( _equiface )
  { }

  // evaluation with full alpha processing. This is quite involved,
  // because if non-opaque pixels are involved, we need to do z-buffering
  // and alpha blending to get a correct result.

  void eval ( const coordinate_type & in ,
              pixel4_type & out ) const
  {
    // we start out with code to obtain the best-matched facet.

    coordinate_type crd ;
    pixel_type & rgb_out ( reinterpret_cast < pixel_type & > ( out ) ) ;
    float alpha ;

    // we need to go through the facets to establish the content by
    // z-buffering the per-facet results, which is expensive.

    std::multimap < float , node_type > list ;

    for ( fc_t f = 0 ; f < nfacets ; f++ )
    {
      bool valid = facet_detect.get_coordinate ( in , crd , f ) ;

      // if the facet is not visible, we can ignore it

      if ( ! valid )
        continue ;

      // get the alpha value

      aevv [ f ] . eval ( crd , alpha ) ;

      // we can ignore fully transparent facets altogether.

      if ( alpha == 0.0f )
        continue ;

      // so the facet is not fully transparent. We need to get
      // it's 'rank'. The smaller this value, the better the fitness.

      auto rank = facet_detect.get_rank ( crd , f ) ;

      node_type n { alpha / 255.0f , f , crd } ;
      auto pos = list.insert ( { rank , n } ) ;

      if ( alpha >= 254.9f )
      {
        // if this facet is fully opaque at crd, we have a new
        // nearest opaque pixel, which occludes any pixels behind it.

        ++pos ;

        if ( pos != list.end() )
        {
          list.erase ( pos , list.end() ) ;
        }
      }
    }

    // now we have the alpha information in the list; the list's last
    // node has the most distant pixel which can contribute to the
    // combined outcome - it may or may not be fully opaque. We process
    // the list members back-to-front to produce the final result.

    if ( list.size() == 0 )
    {
      out = pixel4_type ( 0.0f ) ;
      return ;
    }

    auto it = list.end() ;
    --it ;

    const auto & node ( it->second ) ;
    alpha = node.alpha ;
    evv [ node.facet ] . eval ( node.crd , rgb_out ) ;

    while ( it != list.begin() )
    {
      --it ;
      const auto & node ( it->second ) ;
      float current_alpha = node.alpha ;

      // RGB pixel value

      pixel_type current_rgb ;

      // get the RGB

      evv [ node.facet ] . eval ( node.crd , current_rgb ) ;

      // premultiply the RGB values with their corresponding alpha values

      current_rgb *= current_alpha ;
      rgb_out *= alpha ;

      // calculate the new combined alpha value

      alpha = current_alpha + ( 1.0f - current_alpha ) * alpha ;

      if ( alpha == 0.0f )
      {
        // blacken pixel if alpha is zero

        rgb_out = 0.0f ;
      }
      else
      {
        // perform the blending

        rgb_out *= ( 1.0f - current_alpha ) ;
        rgb_out = ( current_rgb + rgb_out ) / alpha ;
      }
    }

    // move back to 'normal' alpha range [0..255]

    out[3] = alpha * 255.0f ;
  }

  // vectorized version of 'eval'. This is even more involved, because
  // it can't just use a simple sequence for ranking, but has to do
  // the ranking of several pixels in a vector synchronously. I use
  // what I call a 'trickle-up sort': For each in-play facet, we obtain
  // data and put the resulting vector to the end of a set of vectors.
  // Then we compare with the second-last vector and swap where the order
  // is wrong. Next we compare the second-last vector to it's predecessor
  // and swap where the order is wrong, etc. etc. - stopping only once
  // there are no further vectors to swap with, or once the order is
  // correct at the given stage. So if we order numerically and a facet
  // produces small values, these small values 'trickle up' until they
  // find their correct 'slot', hence the name 'trickle-up sort'. This
  // alogrithm is reasonably efficient for small numbers of facets, but
  // for large numbers, it is slow (N*N/2). We rely on the facets having
  // been 'culled' properly so that there is a reasonably small set of
  // in-play facets we'll have to deal with, so the assumption of a small
  // number of facets is reasonable. While culling reduces the set of
  // in-play facets, we'll also look at the set of facets which are
  // involved due to the vector of loci we're currently processing, which,
  // in most cases, is yet again a subset of the in-play facets, and if
  // we're lucky even just a single one.

#ifdef VECTORIZE

  void eval ( const in_v & in ,
              pixel4_v & out ) const
  {
    // get aliases for the RGB and alpha component of 'out'

    pixel_v & rgb_out ( reinterpret_cast < pixel_v & > ( out ) ) ;
    channel_v & alpha_out ( out [ 3 ] ) ;

    // in the first step, we obtain in-facet coordinates for all loci in
    // 'in', and store them to 'crdv', taking note of their validity in
    // 'valid'.

    coordinate_v crdv [ nfacets ] ;
    mask_type valid [ nfacets ] ;

    // as input to the 'ranking' stage, we want vectors containing keys
    // (the 'distance', which in this case is the squared distance, because
    // for mere ranking the square root is not required), and values, which
    // are the facet numbers these distances belong to.

    // the arrays 'skey' (sorted key) and 'value' will hold the result of
    // the ranking operation: value[0] will hold the numbers of the facets
    // with lowest 'distance', and skey[0] will hold the corresponding
    // 'distances'. value[1] holds numbers of second-ranking facets etc.
    // The method is a 'trickle-up' sort where low values move towards lower
    // indices until they aren't lower than the currently looked-at value.
    // For alpha blending, the 'distance' value won't be needed anymore, all
    // that's required is the 'ranking' information in 'value'.

    channel_v skey [ nfacets ] ;
    fc_v value [ nfacets ] ;

    // we'll keep track of loci producing valid coordinates. If we don't
    // detect any valid coordinates at all, we can exit early, which saves
    // a great deal of time.

    mask_type anything ( false ) ;

    // 'nactive' counts the number of facets which produce a valid contribution.
    // If we find that only one facet contributes valid coordinates, we have
    // yet another time-saver. With every facet contributing valid coordinates,
    // we set 'only' to it's number. Then, if only one facet contributes valid
    // coordinates, we know that 'only' holds it's number.

    fc_t nactive = 0 ;
    fc_t only ;

    for ( fc_t f = 0 ; f < nfacets ; f++ )
    {
      // obtain in-facet coordinates for the current facet and save to crdv,
      // store validity in 'valid'

      coordinate_v & crd ( crdv [ f ] ) ; // short alias

      // obtain coordinates for facet f and preset 'value' and 'skey'

      valid [ f ] = facet_detect.get_coordinate ( in , crd , f ) ;
      value [ f ] = f ;
      skey [ f ] = FLT_MAX ;

      // obviously, if the current facet has no valid coordinates to contribute,
      // we can omit the sorting step and leave the whole rank occupied by
      // FLT_MAX as 'distance' value (the preset)

      if ( any_of ( valid [ f ] ) )
      {
        // the facet has valid coordinates, take note

        anything |= valid [ f ] ;
        only = f ;
        ++nactive ;

        auto rank = facet_detect.get_rank ( crd , f ) ;
        skey [ f ] ( valid [ f ] ) = rank ;

        // skey will provide keys (namely, the distance values) for sorting,
        // and the 'value' of the sort is facet numbers, in 'value':

        // perform the 'trickle-up' sort

        for ( int g = f ; g > 0 ; --g )
        {
          // anything 'moving up'?

          auto mask = ( skey [ g ] < skey [ g - 1 ] ) ;

          // trickle up, or break if there are no 'movers'

          if ( any_of ( mask ) )
          {
            swap ( skey [ g ] , skey [ g - 1 ] , mask ) ;
            swap ( value [ g ] , value [ g - 1 ] , mask ) ;
          }
          else
          {
            break ;
          }
        }
      }
    }

    // now we look for time-savers.

    if ( nactive == 0 )
    {
      // no valid coordinates at all

      out = pixel4_v ( 0.0f ) ;

      return ;
    }
    else if ( nactive == 1 )
    {
      // just one facet yielding valid coordinates. In this case,
      // 'only' holds the number of the single valid facet

      aevv [ only ] . eval ( crdv [ only ] , alpha_out ) ;
      alpha_out ( ! valid [ only ] ) = 0.0f ;

      evv [ only ] . eval ( crdv [ only ] , rgb_out ) ;

      return ;
    }

    // we have all in-facet coordinates in crdv, and corresponding masks
    // in 'valid' telling us which coordinates are valid.
    // skey and value contain the *sorted* distances and facet numbers.

    // first optimization: if all top-ranking pixels come from the same
    // facet and are opaque, we can just pick them and return. This is a
    // very common scenario, so the effort to detect it is well spent.

    fc_t fc0 = value [ 0 ] [ 0 ] ;

    if ( all_of ( value [ 0 ] == fc0 ) )
    {
      // produce a vector holding fully opaque alpha values where the
      // in-facet coordinates of fc0 are valid. This serves as reference;
      // if, what's obtained from evaluating the alpha functor, is the
      // same, we have the special case for the shortcut.

      channel_v help ( 0.0f ) ;
      help ( valid [ fc0 ] ) = 255.0f ;

      // load actual alpha values for fc0

      aevv [ fc0 ] . eval ( crdv [ fc0 ] , alpha_out ) ;

      alpha_out ( ! valid [ fc0 ] ) = 0.0f ;

      // if all alpha values are the same, we have the shortcut

      if ( all_of ( help == alpha_out ) )
      {
        // get the RGB values

        evv [ fc0 ] . eval ( crdv [ fc0 ] , rgb_out ) ;

        return ;
      }
    }

    // no luck so far. We have to set up arrays with RGB and alpha
    // vectors for the next stage, because now we have to gather alpha
    // and RGB values from more than one facet, so we have to have all
    // values in place to pick from.

    pixel_v px [ nfacets ] ;
    channel_v al [ nfacets ] ;

    for ( fc_t f = 0 ; f < nfacets ; f++ )
    {
      evv [ f ] . eval ( crdv [ f ] , px [ f ] ) ;

      aevv [ f ] . eval ( crdv [ f ] , al [ f ] ) ;
      al [ f ] ( ! ( valid [ f ] ) ) = 0.0f ;
    }

    // second optimization: oftentimes, the front-ranking pixels will all
    // be opaque. If so, we needn't do alpha blending at all. We obtain
    // the set of top-ranking alpha values with a gather operation from
    // al, and, if the shortcut can be taken, we gather the RGB values
    // from px.

    float * src = (float*) &(px[0]) ;
    float * asrc = (float*) &(al[0]) ;

    // these assertions should hold, they can go later. We take this
    // equality for granted in the upcoming gather operations.

//     assert ( (float*) &(px[1]) == src + 3 * VSIZE ) ;
//     assert ( (float*) &(al[1]) == asrc + VSIZE ) ;

    // gather alpha values of the front-ranking facets into alpha_out

    pick ( asrc , value [ 0 ] , alpha_out ) ;

    // if all front-ranking pixels are fully opaque, we can simply gather
    // the RGB data and be done.

    if ( all_of ( alpha_out == 255.0f ) )
    {
      pick ( src , value [ 0 ] , rgb_out ) ;
      return ;
    }

    // neither shortcut was viable, so next we perform the alpha blending.
    // We have two pointers: src points to an array of pixel_v, holding
    // the RGB values, one pixel_v per facet. asrc points to an array of
    // channel_v holding the corresponding alpha values. We start out with
    // the last *rank* and do the alpha blending 'A over B', moving towards
    // the front rank until A is the front rank.

    pick ( src , value [ nfacets - 1 ] , rgb_out ) ;
    pick ( asrc , value [ nfacets - 1 ] , alpha_out ) ;

    // for the blending calculation, we need alpha_out in [0..1]

    alpha_out /= 255.0f ;

    // now we perform the alpha blending back-to-front, layering the
    // current rank onto what we have so far.

    for ( int v = nfacets - 2 ; v >= 0 ; --v )
    {
      // obtain alpha and RGB data for the current rank

      channel_v current_alpha ;
      pixel_v current_rgb ;

      pick ( src , value[v] , current_rgb ) ;
      pick ( asrc , value[v] , current_alpha ) ;

      // change alpha range to [0..1]

      current_alpha /= 255.0f ;

      // premultiply the RGB values with their corresponding alpha values

      current_rgb *= current_alpha ;
      rgb_out *= alpha_out ;

      // calculate the new combined alpha value

      alpha_out = current_alpha + ( 1.0f - current_alpha ) * alpha_out ;

      // perform the RGB blending

      rgb_out *= pixel_v ( 1.0f - current_alpha ) ;
      rgb_out = ( current_rgb + rgb_out ) / alpha_out ;

      // blacken RGB where alpha is zero. This looks like it could be
      // omitted, but if we had alpha_out == 0 someplace, the result of
      // the division above has to be cleared.

      rgb_out[0] ( alpha_out == 0.0f ) = 0.0f ;
      rgb_out[1] ( alpha_out == 0.0f ) = 0.0f ;
      rgb_out[2] ( alpha_out == 0.0f ) = 0.0f ;
    }

    // finally, move alpha_out back to [0..255] and we're done.

    alpha_out *= 255.0f ;
  }

#endif

} ;

// fixed-facet variant. This is trivial:

template < class facet_project_type >
struct single_facet_type_4
: public vspline::unary_functor < coordinate_type , pixel4_type , VSIZE >
{
  const int nfacets ;

  const facet_project_type facet_project ;
  const ev_type ev ;
  const alpha_ev_type aev ;
  
  single_facet_type_4 ( const facet_project_type & _facet_project ,
                        const ev_type & _ev ,
                        const alpha_ev_type & _aev )
  : facet_project ( _facet_project ) ,
    ev ( _ev ) ,
    aev ( _aev ) ,
    nfacets ( _facet_project.nfacets )
  { }

  void eval ( const coordinate_type & in ,
              pixel4_type & out ) const
  {
    coordinate_type crd ;
    pixel_type & help ( reinterpret_cast < pixel_type & > ( out ) ) ;

    facet_project.eval ( in , crd ) ;
    ev.eval ( crd , help ) ;
    aev.eval ( crd , out [ 3 ] ) ;
  }

#ifdef VECTORIZE

  void eval ( const coordinate_v & in ,
              pixel4_v & out ) const
  {
    coordinate_v place ;
    pixel_v & help ( reinterpret_cast < pixel_v & > ( out ) ) ;
    facet_project.eval ( in , place ) ;

    ev.eval ( place , help ) ;
    aev.eval ( place , out [ 3 ] ) ;
  }

#endif

} ;

// feathered_facet_type blends facets where they meet, making the
// seam less visible. When showing a synoptic view from a set of
// synthetic facets, feathering should not be necessary - it's more
// for displaying sets of registered source images which do not fit
// perfectly. The strategy is to detect pixles which are close to
// the seam and to form a weighted sum of such pixels, choosing the
// weights so that at the seam the weight for the contributors
// becomes equal. Close to the seam, this does require quite a lot
// of effort, because it requires z-buffering. Most of the view will
// normally not be close to a seam, though, and we have several
// 'shortcuts' avoiding the full z-buffering code.
// This object has the non-alpha code, which doesn't have to consider
// transparency, which is yet another complication, and handled by
// feathered_facet_type_4, further down.

template < projection_type TPRJ >
struct feathered_facet_type
: public vspline::unary_functor
          < coordinate_type ,
            pixel_type ,
            VSIZE >
{
  typedef get_facet_from_crd < TPRJ > facet_detect_type ;

  typedef vspline::unary_functor
           < coordinate_type ,
             pixel_type ,
             VSIZE >
    base_type ;

  const facet_detect_type facet_detect ;
  const std::vector < ev_type > evv ;
  const int nfacets ;
  const bool equiface ;

  const float threshold ;

  struct node_type
  {
    fc_t facet ;
    coordinate_type crd ;
  } ;

  feathered_facet_type ( const facet_detect_type & _facet_detect ,
                         const std::vector < ev_type > & _evv ,
                         float _threshold )
  : facet_detect ( _facet_detect ) ,
    evv ( _evv ) ,
    nfacets ( _facet_detect.nfacets ) ,
    equiface ( false ) ,
    threshold ( _threshold )
  { }

  // single-pixel evaluation

  void eval ( const coordinate_type & in ,
              pixel_type & out ) const
  {
    std::multimap < float , node_type > list ;
    float alpha ;
    coordinate_type crd ;

    // we look at all facets and determine how far the current pixel's
    // location on the facet is away from the facet's center - out usual
    // quality criterion.

    for ( fc_t f = 0 ; f < nfacets ; f++ )
    {
      // get the in-facet coordinate for 'in'

      bool valid = facet_detect.get_coordinate ( in , crd , f ) ;

      // if the facet is not visible, we can ignore it

      if ( ! valid )
        continue ;

      // We need to find the facet's rank, which we derive from
      // the distance to facet center. The smaller this value,
      // the higher the rank.

      auto rank = facet_detect.get_rank ( crd , f ) ;

      // we save facet number and in-facet coordinate in a node_type
      // object and put that into 'list', with 'distance' as key.

      node_type node { f , crd } ;
      list.insert ( { rank , node } ) ;
    }

    auto it = list.begin() ;

    // now we have the node information in the list. All coordinates
    // with RGB content are listed. If the list is empty, we can stop
    // here and return a black pixel, and if there is only one candidate,
    // we'll return it's RGB value.

    if ( list.size() == 0 )
    {
      out = pixel_type ( 0.0f ) ;
      return ;
    }
    else if ( list.size() == 1 )
    {
      evv [ it->second.facet ] . eval ( it->second.crd , out ) ;
      return ;
    }

    // we have several 'candidates' to consider. The first one in the list
    // is our standard: it has the lowest distance to facet center, and we
    // assign a weight of 1. The total weight also starts out as 1, but
    // may increase if we find other candidates which are 'nearly as good'
    // as the first one, the criterion being 'threshold'.

    const float & rank_front ( it->first ) ;
    float total = 1.0f ;

    // we'll definitely use the first candidate's RGB content

    evv [ it->second.facet ] . eval ( it->second.crd , out ) ;

    ++it ;

    while ( it != list.end() )
    {
      // we need the 'rank' of the current candidate

      const auto & rank_current ( it->first ) ;

      // due to the ordered insertion into 'list' we can be sure
      // that rank_current >= rank_front. dr is the difference in
      // rank.

      auto dr = rank_current - rank_front ;
      if ( dr > threshold )
      {
        // this candidate is not good enough to participate in the
        // final result, and we know already that the ones further back
        // in the list will be even worse, so we leave the loop.

        break ;
      }

      // The fitness will range from 0 (dr==threshold) to 1 (dr==0)

      auto fitness = 1.0f - dr / threshold ;

      // we need to update 'total' so we can normalize later on

      total += fitness ;

      // now we get the current candidate's RGB value, apply the fitness
      // value and sum it up to 'out'.

      pixel_type rgb ;
      evv [ it->second.facet ] . eval ( it->second.crd , rgb ) ;
      rgb *= fitness ;
      out += rgb ;

      // finally we adapt the iterator for the next cycle

      ++it ;
    }

    // normalize if necessary and we're done.

    if ( total > 1.0f )
      out /= total ;
  }

  // vectorized evaluation. Here, instead of the ranking with a
  // std::multimap, I use my own vectorized 'trickle-up-sort'.

#ifdef VECTORIZE

  using typename base_type::in_v ;

  void clear ( channel_v & v , mask_type m ) const
  {
    v ( m ) = 0.0f ;
  }

  template < int N >
  void clear ( vigra::TinyVector < channel_v , N > & v ,
               mask_type m ) const
  {
    for ( int ch = 0 ; ch < N ; ch++ )
      v [ ch ] ( m ) = 0.0f ;
  }

  void eval ( const in_v & in ,
              pixel_v & rgb_out ) const
  {
    // in the first step, we obtain in-facet coordinates for all loci in
    // 'in', and store them to 'crdv', taking note of their validity in
    // 'valid'.

    coordinate_v crdv [ nfacets ] ;
    mask_type valid [ nfacets ] ;

    // as input to the 'ranking' stage, we want vectors containing keys
    // (the 'distance', which in this case is the squared distance, because
    // for mere ranking the square root is not required), and values, which
    // are the facet numbers these distances belong to.

    // the arrays 'skey' (sorted key) and 'value' will hold the result of
    // the ranking operation: value[0] will hold the numbers of the facets
    // with lowest 'distance', and skey[0] will hold the corresponding
    // 'distances'. value[1] holds numbers of second-ranking facets etc.
    // The method is a 'trickle-up' sort where low values move towards lower
    // indices until they aren't lower than the currently looked-at value.
    // For alpha blending, the 'distance' value won't be needed anymore, all
    // that's required is the 'ranking' information in 'value'.

    channel_v skey [ nfacets ] ;
    fc_v value [ nfacets ] ;

    // we'll keep track of loci producing valid coordinates. If we don't
    // detect any valid coordinates at all, we can exit early, which saves
    // a great deal of time.

    mask_type anything ( false ) ;

    // 'nactive' counts the number of facets which produce a valid contribution.
    // If we find that only one facet contributes valid coordinates, we have
    // yet another time-saver. With every facet contributing valid coordinates,
    // we set 'only' to it's number. Then, if only one facet contributes valid
    // coordinates, we know that 'only' holds it's number.

    fc_t nactive = 0 ;
    fc_t only ;

    for ( fc_t f = 0 ; f < nfacets ; f++ )
    {
      // obtain in-facet coordinates for the current facet and save to crdv,
      // store validity in 'valid'

      coordinate_v & crd ( crdv [ f ] ) ; // short alias

      // obtain coordinates for facet f and preset 'value' and 'skey'

      valid [ f ] = facet_detect.get_coordinate ( in , crd , f ) ;
      value [ f ] = f ;
      skey [ f ] = FLT_MAX ;

      // obviously, if the current facet has no valid coordinates to contribute,
      // we can omit the sorting step and leave the whole rank occupied by
      // FLT_MAX as 'rank' value (the preset)

      if ( any_of ( valid [ f ] ) )
      {
        // the facet has valid coordinates, take note

        anything |= valid [ f ] ;
        only = f ;
        ++nactive ;

        auto rank = facet_detect.get_rank ( crd , f ) ;
        skey [ f ] ( valid [ f ] ) = rank ;

        // skey will provide keys (namely, the rank values) for sorting,
        // and the 'value' of the sort is facet numbers, in 'value':

        // perform the 'trickle-up' sort

        for ( int g = f ; g > 0 ; --g )
        {
          // anything 'moving up'?

          auto mask = ( skey [ g ] < skey [ g - 1 ] ) ;

          // trickle up, or break if there are no 'movers'

          if ( any_of ( mask ) )
          {
            swap ( skey [ g ] , skey [ g - 1 ] , mask ) ;
            swap ( value [ g ] , value [ g - 1 ] , mask ) ;
          }
          else
          {
            break ;
          }
        }
      }
    }

    // now we look for time-savers.

    if ( nactive == 0 )
    {
      // no valid coordinates at all

      rgb_out = pixel_v ( 0.0f ) ;
      return ;
    }
    else if ( nactive == 1 )
    {
      // just one facet yielding valid coordinates. In this case,
      // 'only' holds the number of the single valid facet

      evv [ only ] . eval ( crdv [ only ] , rgb_out ) ;
      clear ( rgb_out , ! valid [ only ] ) ;

      return ;
    }

    // we have all in-facet coordinates in crdv, and corresponding masks
    // in 'valid' telling us which coordinates are valid.
    // skey and value contain the *sorted* distances and facet numbers.

    // first optimization: if all top-ranking pixels come from the same
    // facet and have no close neighbours, we can just load the RGB values
    // and we're done

    fc_t fc0 = value [ 0 ] [ 0 ] ;

    if ( all_of ( value [ 0 ] == fc0 ) )
    {
      // if all alpha values are the same and the front facet has no
      // close neighbours, we have the shortcut

      if ( all_of ( ( skey[1] - skey[0] ) >= threshold ) )
      {
        // get the RGB values

        evv [ fc0 ] . eval ( crdv [ fc0 ] , rgb_out ) ;
        return ;
      }
    }

    // Next we set up arrays with RGB vectors for the next stage,
    // because now we have to gather RGB values from more than one
    // facet, so we have to have all values in place to pick from.

    pixel_v px [ nfacets ] ;

    for ( fc_t f = 0 ; f < nfacets ; f++ )
    {
      evv [ f ] . eval ( crdv [ f ] , px [ f ] ) ;

      clear ( px [ f ] , ! ( valid [ f ] ) ) ;
    }

    float * src = (float*) &(px[0]) ;

    // this assertion should hold, it can go later. We take this
    // equality for granted in the upcoming gather operations.

    assert ( (float*) &(px[1]) == src + 3 * VSIZE ) ;

    // if the rank difference from the front to next-best pixels is
    // above 'threshold' for all pixels, we can simply gather the RGB
    // data and be done. We'll need the top rank of RGB data anyway:

    pick ( src , value [ 0 ] , rgb_out ) ;

    if ( all_of ( ( skey[1] - skey[0] ) >= threshold ) )
    {
      // if the top rank has no close neighbours, we're done.

      return ;
    }

    // we'll have to do the feathering now.

    channel_v wsum ( 1.0f ) ;

    for ( int r = 1 ; r < nfacets ; r++ )
    {
      auto difference = skey[r] - skey[0] ;
      auto close = ( difference < threshold ) ;

      if ( none_of ( close ) )
        break ;

      pixel_v rgb ;
      pick ( src , value [ r ] , rgb ) ;

      auto weight = 1.0f - difference / threshold ;
      weight ( ! close ) = 0.0f ;

      wsum += weight ;

      rgb_out += weight * rgb ;
//       add_weighted ( rgb_out , weight , rgb ) ;
    }

    // normalize and we're done

    rgb_out /= wsum ;
  }

#endif

} ;

/// feathered_facet_type_4 is probably the most complex evaluator so far.
/// We have three factors to consider: the 'rank' of the in-play facets,
/// The rank differences, which may result in feathering if the rank
/// difference is small, and finally transparency, with facets with
/// lower rank possibly 'shining through' semi-transparent facets with
/// higher rank. To do the job properly, we'll have to deal adequaltely
/// with feathered seams shining through semi-transparent content of
/// higher rank, and higher-ranking content which has been subject to
/// feathering. It's also important to keep in mind that feathering may
/// combine more than two facets.

template < projection_type TPRJ >
struct feathered_facet_type_4
: public tf24_type
{
  typedef tf24_type base_type ;

  typedef get_facet_from_crd < TPRJ > facet_detect_type ;
  
  const facet_detect_type facet_detect ;
  const std::vector < ev_type > evv ;
  const std::vector < alpha_ev_type > aevv ;
  const int nfacets ;
  const bool equiface ;

  const float threshold ;

  struct node_type
  {
    float alpha ;
    pixel_type rgb ;
    fc_t facet ;
    coordinate_type crd ;
  } ;

  feathered_facet_type_4 ( const facet_detect_type & _facet_detect ,
                           const std::vector < ev_type > & _evv ,
                           const std::vector < alpha_ev_type > & _aevv ,
                           float _threshold ,
                           bool _equiface = false  )
  : facet_detect ( _facet_detect ) ,
    evv ( _evv ) ,
    aevv ( _aevv ) ,
    nfacets ( _facet_detect.nfacets ) ,
    equiface ( _equiface ) ,
    threshold ( _threshold )
  { }

  // single-pixel evaluation.
  // I'll use this strategy: after handling a few corner cases which
  // can be dealt with more quickly (zero or one nodes in the list,
  // fully opaque 'front' node) I'll use modified RGB values for alpha
  // blending wherever a node has other nodes in 'feathering distance':
  // the modification consists in forming a weighted sum of all the
  // nodes which are close together. So feathering is applied first,
  // and then the result of feathering is alpha-blended. It's important
  // to note that we can't simply replace all RGB values by their
  // 'feathered' equivalent.

  void eval ( const coordinate_type & in ,
              pixel4_type & out ) const
  {
    typedef std::multimap < float , node_type > list_type ;
    typedef typename list_type::iterator it_type ;

    list_type list ;

    float alpha ;
    coordinate_type crd ;
    pixel_type & rgb_out ( reinterpret_cast < pixel_type & > ( out ) ) ;

    // we look at all facets and determine how far the current pixel's
    // location on the facet is away from the facet's center - out usual
    // quality criterion. We use the squared distance for computational
    // efficiency: no point in taking the square root, the order is all
    // we want to know. We also check the pixel's alpha value and ignore
    // totally transparent ones

    for ( fc_t f = 0 ; f < nfacets ; f++ )
    {
      // get the in-facet coordinate for 'in'

      bool valid = facet_detect.get_coordinate ( in , crd , f ) ;

      // if the facet is not visible, we can ignore it

      if ( ! valid )
        continue ;

      // and if it is transparent, we can also ignore it.

      aevv [ f ] . eval ( crd , alpha ) ;

      if ( alpha <= 0.0f )
        continue ;

      // next we obtain the current RGB value

      pixel_type rgb ;
      evv [ f ] . eval ( crd , rgb ) ;

      // We need to find the facet's rank, which we derive from
      // the squared distance to facet center. The smaller this
      // value, the higher the rank.

      auto rank = facet_detect.get_rank ( crd , f ) ;

      // we save facet number and in-facet coordinate in a node_type
      // object and put that into 'list', with 'distance' as key.
      // We store alpha as a value in 0..1 for the computations.

      node_type node { alpha / 255.0f , rgb , f , crd } ;

      auto pos = list.insert ( { rank , node } ) ;
    }

    // difference-of-distance function. currently merely the absolute
    // value of the difference

    auto fd = [] ( float lhs , float rhs ) -> float
              { return fabs ( rhs - lhs ) ; } ;

    // now we have the node information in the list. All coordinates
    // with RGBA content are listed. If the list is empty, we can stop
    // here and return a black pixel, and if there is only one candidate,
    // we'll return it's RGBA value.

    if ( list.size() == 0 )
    {
      out = pixel4_type ( 0.0f ) ;
      return ;
    }
    else if ( list.size() == 1 )
    {
      auto it = list.begin() ;
      rgb_out = it->second.rgb ;
      out[3] = it->second.alpha * 255.0f ;
      return ;
    }
    else
    {
      // with more than one candidate, we still have an opportunity
      // for a quicker calculation: if the front node is fully opaque,
      // we can simply perform the feathering and return, we needn't
      // take into account any alpha blending.

      auto front = list.begin() ;

      if ( front->second.alpha >= 1.0f )
      {
        rgb_out = front->second.rgb ;
        out[3] = 255.0f ;
        float wsum = 1.0f ;

        auto next = front ;
        ++next ;

        while ( next != list.end() )
        {
          auto difference = fd ( next->first , front->first ) ;
          if ( difference >= threshold )
            break ;

          auto weight = 1.0f - ( difference / threshold ) ;
          weight *= next->second.alpha ;
          wsum += weight ;
          rgb_out += weight * next->second.rgb ;
          ++next ;
        }
        rgb_out /= wsum ;
        return ;
      }
    }

    // we have more than one node, and the first node is not fully
    // opaque. Now we'll go through the list back-to-front. We start
    // at the last node which can at all contribute: the last
    // feathering contributor to the first fully opaque node.
    // first we find the first fully opaque node; this can't be
    // the very first node - we've just checked that above.

    auto tail = list.end() ;
    auto first_opaque = list.begin() ;
    ++first_opaque ;

    while (    ( first_opaque != list.end() )
            && ( first_opaque->second.alpha < 1.0f ) )
    {
      ++first_opaque ;
    }

    // if we've found an opaque node, we next check for any close
    // successors which would contribute to feathering

    if ( first_opaque != list.end() )
    {
      tail = first_opaque ;
      ++tail ;
      while (    ( tail != list.end() )
              && ( fd ( tail->first , first_opaque->first ) < threshold ) ) 
      {
        ++tail ;
      }
    }

    // now we work backwards from the just-established last node

    auto it = tail ;
    --it ;

    // we're starting out with a totally transparent pixel

    alpha = 0.0f ;

    // now we move backwards in the list, layering on the contributing
    // pixels according to their alpha value. If any of the nodes we
    // encounter is within 'feathering distance' of other nodes, we
    // don't feed it's original RGB value into the alpha blending
    // calculation, but instead we use a modified RGB value reflecting
    // the feathering: we use a weighted sum of all 'nearby' nodes.
    // This is more effort than just modifying the nearest node with
    // RGB values from 'below' it, but with the possibility of stuff
    // 'shining through' transparent pixels, we have to do it in order
    // to have consistent values to blend.

    while ( true )
    {
      // find two iterators head and tail delimiting the set of
      // nodes which are within 'feathering distance'

      it_type head = it ;
      it_type tail = it ;

      while ( head != list.begin() )
      {
        --head ;
        auto distance = fd ( it->first , head->first ) ;
        if ( distance >= threshold )
        {
          ++head ;
          break ;
        }
      }

      it_type last = list.end() ;
      --last ;

      while ( tail != last )
      {
        ++tail ;
        auto distance = fd ( tail->first , it->first ) ;
        if ( distance >= threshold )
        {
          --tail ;
          break ;
        }
      }

      pixel_type current_rgb ;
      float current_alpha = it->second.alpha ;

      if ( head == tail )
      {
        // head == tail means that there weren't any other nodes
        // within 'feathering distance'. We can use the unmodified
        // RGB value of the current pixel

        current_rgb = it->second.rgb ;
      }
      else
      {
        // head != tail indicates that feathering is needed for the
        // current node. We'll replace 'current_rgb' with a weighted
        // sum of all contributing nodes (including the current one)

        float weight_sum = 0.0f ;
        current_rgb = pixel_type ( 0.0f ) ;
        it_type contributor = head ;

        while ( true )
        {
          // form the weight from the difference in distance to
          // facet center and the alpha value

          float difference = fd ( contributor->first , it->first ) ;
          float weight = 1.0f - difference / threshold ;
          weight *= contributor->second.alpha ;

          // sum up the weight for the normailzation step and apply
          // add the weighted RGB value to the sum

          weight_sum += weight ;
          current_rgb += weight * contributor->second.rgb ;

          if ( contributor == tail )
            break ;

          ++contributor ;
        }

        // normalize if weight_sum isn't zero (if it is, current_rgb
        // is zero as well)

        if ( weight_sum != 0.0f )
          current_rgb /= weight_sum ;
      }

      // what's left is the normal code for alpha blending, only that
      // current_rgb may have been modified by feathering.

      // premultiply the RGB values with their corresponding alpha values

      current_rgb *= current_alpha ;
      rgb_out *= alpha ;

      // calculate the new combined alpha value

      alpha = current_alpha + ( 1.0f - current_alpha ) * alpha ;

      if ( alpha == 0.0f )
      {
        rgb_out = 0.0f ;
      }
      else
      {
        // perform the blending

        rgb_out *= ( 1.0f - current_alpha ) ;
        rgb_out = ( current_rgb + rgb_out ) / alpha ;
      }

      // if the current node was the very first one, we're ready

      if ( it == list.begin() )
        break ;

      // otherwise, move to the current node's predecessor

      --it ;
    }

    // move back to 'normal' alpha range [0..255] - for the calculations
    // we've used alpha in [0..1]

    out[3] = alpha * 255.0f ;
  }

  // vectorized version of 'eval'. This does roughly the same as the
  // single-pixel code. The differences are mainly technical: instead of
  // using a std::multimap for ranking, I use my own code for a vectorized
  // 'trickle-up' sort, and the usual conditionals-to-masks change happens.

#ifdef VECTORIZE

  void eval ( const in_v & in ,
              pixel4_v & out ) const
  {
    // the first bit, establishing the rank of the facets, is just the same
    // as in multi_facet_type_4

    out = pixel4_v ( 0.0f ) ;

    // get aliases for the RGB and alpha component of 'out'

    pixel_v & rgb_out ( reinterpret_cast < pixel_v & > ( out ) ) ;
    channel_v & alpha_out ( out [ 3 ] ) ;

    // in the first step, we obtain in-facet coordinates for all loci in
    // 'in', and store them to 'crdv', taking note of their validity in
    // 'valid'.

    coordinate_v crdv [ nfacets ] ;
    mask_type valid [ nfacets ] ;

    // as input to the 'ranking' stage, we want vectors containing keys
    // (the 'distance', which in this case is the squared distance, because
    // for mere ranking the square root is not required), and values, which
    // are the facet numbers these distances belong to.

    // the arrays 'skey' (sorted key) and 'value' will hold the result of
    // the ranking operation: value[0] will hold the numbers of the facets
    // with lowest 'distance', and skey[0] will hold the corresponding
    // 'distances'. value[1] holds numbers of second-ranking facets etc.
    // The method is a 'trickle-up' sort where low values move towards lower
    // indices until they aren't lower than the currently looked-at value.
    // For alpha blending, the 'distance' value won't be needed anymore, all
    // that's required is the 'ranking' information in 'value'.

    channel_v skey [ nfacets ] ;
    fc_v value [ nfacets ] ;

    // we'll keep track of loci producing valid coordinates. If we don't
    // detect any valid coordinates at all, we can exit early, which saves
    // a great deal of time.

    mask_type anything ( false ) ;

    // 'nactive' counts the number of facets which produce a valid contribution.
    // If we find that only one facet contributes valid coordinates, we have
    // yet another time-saver. With every facet contributing valid coordinates,
    // we set 'only' to it's number. Then, if only one facet contributes valid
    // coordinates, we know that 'only' holds it's number.

    fc_t nactive = 0 ;
    fc_t only ;

    for ( fc_t f = 0 ; f < nfacets ; f++ )
    {
      // obtain in-facet coordinates for the current facet and save to crdv,
      // store validity in 'valid'

      coordinate_v & crd ( crdv [ f ] ) ; // short alias

      // obtain coordinates for facet f and preset 'value' and 'skey'

      valid [ f ] = facet_detect.get_coordinate ( in , crd , f ) ;
      value [ f ] = f ;
      skey [ f ] = FLT_MAX ;

      // obviously, if the current facet has no valid coordinates to contribute,
      // we can omit the sorting step and leave the whole rank occupied by
      // FLT_MAX as 'distance' value (the preset)

      if ( any_of ( valid [ f ] ) )
      {
        // the facet has valid coordinates, take note

        anything |= valid [ f ] ;
        only = f ;
        ++nactive ;

        // set 'rank' to the squared distance to facet center where the
        // the coordinates are valid (other positions are preset to FLT_MAX).

        auto rank = facet_detect.get_rank ( crd , f ) ;
        skey [ f ] ( valid [ f ] ) = rank ;

        // skey will provide keys (namely, the distance values) for sorting,
        // and the 'value' of the sort is facet numbers, in 'value':

        // perform the 'trickle-up' sort

        for ( int g = f ; g > 0 ; --g )
        {
          // anything 'moving up'?

          auto mask = ( skey [ g ] < skey [ g - 1 ] ) ;

          // trickle up, or break if there are no 'movers'

          if ( any_of ( mask ) )
          {
            swap ( skey [ g ] , skey [ g - 1 ] , mask ) ;
            swap ( value [ g ] , value [ g - 1 ] , mask ) ;
          }
          else
          {
            break ;
          }
        }
      }
    }

    // now we look for time-savers.

    if ( nactive == 0 )
    {
      // no valid coordinates at all

      return ;
    }
    else if ( nactive == 1 )
    {
      // just one facet yielding valid coordinates. In this case,
      // 'only' holds the number of the single valid facet

      aevv [ only ] . eval ( crdv [ only ] , alpha_out ) ;

      alpha_out ( ! valid [ only ] ) = 0.0f ;

      evv [ only ] . eval ( crdv [ only ] , rgb_out ) ;

      return ;
    }

    // we have all in-facet coordinates in crdv, and corresponding masks
    // in 'valid' telling us which coordinates are valid.
    // skey and value contain the *sorted* distances and facet numbers.

    // first optimization: if all top-ranking pixels come from the same
    // facet and are opaque, we can just pick them and return. This is a
    // very common scenario, so the effort to detect it is well spent.
    // Here we additionally have to make sure that there are no close
    // neighbours, which is the only difference to the non-feathering
    // code in multi_facet_type_4

    fc_t fc0 = value [ 0 ] [ 0 ] ;

    if ( all_of ( value [ 0 ] == fc0 ) )
    {
      // produce a vector holding fully opaque alpha values where the
      // in-facet coordinates of fc0 are valid. This serves as reference;
      // if, what's obtained from evaluating the alpha functor, is the
      // same, we have the special case for the shortcut.

      channel_v help ( 0.0f ) ;
      help ( valid [ fc0 ] ) = 255.0f ;

      // load actual alpha values for fc0

      aevv [ fc0 ] . eval ( crdv [ fc0 ] , alpha_out ) ;

      alpha_out ( ! valid [ fc0 ] ) = 0.0f ;

      // if all alpha values are the same and the front facet has no
      // close neighbours, we have the shortcut

      if (    all_of ( help == alpha_out )
           && all_of ( abs ( skey[1] - skey[0] ) >= threshold ) )
      {
        // get the RGB values

        evv [ fc0 ] . eval ( crdv [ fc0 ] , rgb_out ) ;
        return ;
      }
    }

    // no luck so far. We have to set up arrays with RGB and alpha
    // vectors for the next stage, because now we have to gather alpha
    // and RGB values from more than one facet, so we have to have all
    // values in place to pick from. Again the code is the same as in
    // the base class

    pixel_v px [ nfacets ] ;
    channel_v al [ nfacets ] ;

    for ( fc_t f = 0 ; f < nfacets ; f++ )
    {
      evv [ f ] . eval ( crdv [ f ] , px [ f ] ) ;
      aevv [ f ] . eval ( crdv [ f ] , al [ f ] ) ;
      al [ f ] ( ! ( valid [ f ] ) ) = 0.0f ;
    }

    // second optimization: oftentimes, the front-ranking pixels will all
    // be opaque. If so, we needn't do alpha blending at all. We obtain
    // the set of top-ranking alpha values with a gather operation from
    // al, and, if the shortcut can be taken, we gather the RGB values
    // from px.

    float * src = (float*) &(px[0]) ;
    float * asrc = (float*) &(al[0]) ;

    // these assertions should hold, they can go later. We take this
    // euality for granted in the upcoming gather operations.

    assert ( (float*) &(px[1]) == src + 3 * VSIZE ) ;
    assert ( (float*) &(al[1]) == asrc + VSIZE ) ;

    // gather alpha values of the front-ranking facets into alpha_out

    pick ( asrc , value [ 0 ] , alpha_out ) ;

    // if all front-ranking pixels are fully opaque and the rank
    // difference from the front to next-best pixel is always above
    // 'threshold' we can simply gather the RGB data and be done.

    if (    all_of ( alpha_out == 255.0f )
         && all_of ( abs ( skey[1] - skey[0] ) >= threshold ) )
    {
      pick ( src , value [ 0 ] , rgb_out ) ;
      return ;
    }

    // neither shortcut was viable, so next we perform the alpha blending.
    // We have two pointers: src points to an array of pixel_v, holding
    // the RGB values, one pixel_v per facet. asrc points to an array of
    // channel_v holding the corresponding alpha values. We start out with
    // the last *rank* and do the alpha blending 'A over B', moving towards
    // the front rank until A is the front rank.

    // we set up a second set of RGB values and a set of weight sums to
    // perfrom the feathering calculations. We'll use weighted sums of
    // RGB values of pixels which are 'close' instead of the unmodified
    // RGB values which we have stored in 'px'

    pixel_v rgb_sum [ nfacets ] ;
    channel_v wsum [ nfacets ] ;

    for ( int f = 0 ; f < nfacets ; f++ )
    {
      // we'll add to these values, so we need to start out with zero.

      rgb_sum [ f ] = 0.0f ;
      wsum [ f ] = 0.0f ;
    }

    // for the feathering calculation, we go back-to-front, which is arbitrary.

    for ( int f = 0 ; f < nfacets ; f++ )
    {
      // obtain the RGB and alpha values for the current rank into rgb_f and a_f.
      // For 'the rank itself' weight is == 1 * alpha, which we use as the weight
      // to apply to rgb_f add to wsum. We move to alpha in [0..1] in this step.

      pixel_v rgb_f ;
      channel_v a_f ;

      // gather the data for the current rank

      pick ( src , value [ f ] , rgb_f ) ;
      pick ( asrc , value [ f ] , a_f ) ;
      a_f /= 255.0f ;

      // multiply RGB values with their weight and update rgb_sum and wsum

      rgb_sum [ f ] += rgb_f * a_f ;
      wsum [ f ] += a_f ;

      // look at all ranks below it

      for ( int g = f + 1 ; g < nfacets ; g++ )
      {
        // again we have storage handy for a pixel_v of RGB and a channel_v
        // of alpha.

        pixel_v rgb_g ;
        channel_v a_g ;

        // here we additionally zake into account the difference in 'distance'
        // between rank f and rank g, and we note where this difference is
        // below the threshold: there, feathering happens.

        auto difference = skey [ g ] - skey [ f ] ;
        auto in_range = ( difference < threshold ) ;

        if ( none_of ( in_range ) )
        {
          // all pixels in rank g are further away than threshold

          break ;
        }

        // again we gather RGB and alpha data from px and al (via src and asrc)
        // and move to alpha in [0..1]

        pick ( src , value [ g ] , rgb_g ) ;
        pick ( asrc , value [ g ] , a_g ) ;
        a_g /= 255.0f ;

        // this gives us the component of the weight depending of the difference
        // of distance:

        auto weight = ( 1.0f - difference / threshold ) ;

        // which, multiplied with the 'partner's' alpha, gives the weight we
        // apply and sum up to *both* of the participants f and g.

        rgb_sum [ f ] [ 0 ] ( in_range ) += a_g * weight * rgb_g [ 0 ] ;
        rgb_sum [ f ] [ 1 ] ( in_range ) += a_g * weight * rgb_g [ 1 ] ;
        rgb_sum [ f ] [ 2 ] ( in_range ) += a_g * weight * rgb_g [ 2 ] ;

        wsum [ f ] ( in_range ) += a_g * weight ;

        rgb_sum [ g ] [ 0 ] ( in_range ) += a_f * weight * rgb_f [ 0 ] ;
        rgb_sum [ g ] [ 1 ] ( in_range ) += a_f * weight * rgb_f [ 1 ] ;
        rgb_sum [ g ] [ 2 ] ( in_range ) += a_f * weight * rgb_f [ 2 ] ;

        wsum [ g ] ( in_range ) += a_f * weight ;
      }
    }

    channel_v alpha ;

    auto mask = ( wsum [ nfacets - 1 ] != 0.0f ) ;
    rgb_out = 0.0f ;
    rgb_out [ 0 ] ( mask ) = rgb_sum [ nfacets - 1 ] [ 0 ] / wsum [ nfacets - 1 ] ;
    rgb_out [ 1 ] ( mask ) = rgb_sum [ nfacets - 1 ] [ 1 ] / wsum [ nfacets - 1 ] ;
    rgb_out [ 2 ] ( mask ) = rgb_sum [ nfacets - 1 ] [ 2 ] / wsum [ nfacets - 1 ] ;

    pick ( asrc , value [ nfacets - 1 ] , alpha_out ) ;

    // for the blending calculation, we need alpha_out in [0..1]

    alpha_out /= 255.0f ;

    // now we perform the alpha blending back-to-front, layering what we
    // have so far onto the values of the current rank.

    for ( int v = nfacets - 2 ; v >= 0 ; --v )
    {
      // obtain alpha and RGB data for the current rank

      channel_v current_alpha ;
      pixel_v current_rgb ;

      mask = ( wsum [ v ] != 0.0f ) ;
      current_rgb = 0.0f ;

      current_rgb [ 0 ] ( mask ) = rgb_sum [ v ] [ 0 ] / wsum [ v ] ;
      current_rgb [ 1 ] ( mask ) = rgb_sum [ v ] [ 1 ] / wsum [ v ] ;
      current_rgb [ 2 ] ( mask ) = rgb_sum [ v ] [ 2 ] / wsum [ v ] ;

      pick ( asrc , value[v] , current_alpha ) ;

      // change alpha range to [0..1]

      current_alpha /= 255.0f ;

      // premultiply the RGB values with their corresponding alpha values

      current_rgb *= current_alpha ;
      rgb_out *= alpha_out ;

      // calculate the new combined alpha value

      alpha_out = current_alpha + ( 1.0f - current_alpha ) * alpha_out ;

      // perform the RGB blending

      rgb_out *= pixel_v ( 1.0f - current_alpha ) ;
      rgb_out = ( current_rgb + rgb_out ) / alpha_out ;

      // blacken RGB where alpha is zero. This looks like it could be
      // omitted, but if we had alpha_out == 0 someplace, the result of
      // the division above has to be cleared.

      rgb_out[0] ( alpha_out == 0.0f ) = 0.0f ;
      rgb_out[1] ( alpha_out == 0.0f ) = 0.0f ;
      rgb_out[2] ( alpha_out == 0.0f ) = 0.0f ;
    }

    // finally, move alpha_out back to [0..255] and we're done.

    alpha_out *= 255.0f ;
  }

#endif

} ;

// multi_layer_type provides a way of combining several facets by using
// a blending function. While multi_facet_type etc. select one 'best'
// facet or infer a ranking by proximity to the origin, multi_layer_type
// combines all pixels on a given ray.

// hdr_fitness provides a fitness value for each contributing pixel.
// The first argument is the RGB value - this is the value *after*
// facet brightness has been applied, so it may well be larger than 255.
// The second argument gives the grey value for which the fitness will
// be maximal, which is specific to the facet.
// This variant uses a small 'socket' value, which is passed zero
// for *all but the darkest* exposure. At the same time, the gaussian
// weighting curves are lowered so that at zero and maximum intensity
// they become zero as well. This makes the socket necessary, to make
// sure that we don't get zero total fitness ever, when all contributing
// pixels come out with zero weight. With the socket value for the pixel
// from the darkest exposure, we're sure to have valid - if maybe noisy -
// data at the location, because the darkest exposure (after brightening)
// covers the largest range of incoming light.
// TODO: getting the fitness function right is difficult, and the
// solution given here is merely one of many possibilites. One might
// consider more parametrization or even plugins here.

template < typename dtype >
dtype _hdr_fitness ( const vigra::TinyVector < dtype , 3 > & rgb ,
                     const float & peak ,
                     const float & socket )
{
  // first step: grey projection. we use a simple grey projector
  // yielding the maximum of the three channels to avoid overexposed
  // pixels, even if only one channel is overexposed

  // dtype grey = std::max ( std::max ( rgb[0] , rgb[1] ) , rgb[2] ) ;

  // currently using the average of the three channels as the driving
  // value, which seems to give better results for the sky

  dtype grey = ( rgb[0] + rgb[1] + rgb[2] ) / 3.0f ;

  // second step: calculate the difference from the grey value which
  // will yield the highest fitness. We use the difference to the
  // 'peak' value, normalized so that if 'peak' is middle grey we
  // get values in the range of [0:1] if grey is in [0:2*peak]

  dtype difference = abs ( ( grey - peak ) / peak ) ;

  // third step: calculate the fitness from the distance.
  // multiplying 'difference' with 5 gives most weight to pixels
  // near the partial image's middle grey values and makes for
  // rapid decrease of the weight near the extrema of the
  // histogram, 'making room' for the other images rather
  // than aiming at an average. This is similar to the
  // 'exposure_sigma' value in enfuse.
  // using the plain exponential would produce non-zero weights
  // even for zero-valued or maximal-valued pixels, but due to
  // the large exponent these weights are quite small.
  // Nevertheless I lower the weight curve by exp(5), the value
  // the exponential takes at +/-1. This produces a weighting curve
  // which is precisely zero at 0 and 2*peak.
  // 'socket' is passed in as a small positive value if we're
  // processing the 'darkest' facet, and FLT_MIN otherwise. The effect
  // is to make sure that the sum of weights in the blending function
  // will always be nonzero, while partial weights for 'brighter' facets
  // may become almost zero. With the larger socket value for the
  // 'darkest' facet we make sure it's picked for highlights.

  dtype weight = exp ( -5.0 * difference ) ;
  weight -= exp ( -5.0f ) ; // might subtract a tad more

  weight = vspline::max ( dtype ( socket ) , weight ) ;

  return weight ;
}

// this variant uses the same fitness function as the exposure fusion code.
// the difference seems marginal. I made a few attempts moving between the
// variants, because I saw artifacts in the HDR output which I thought should
// not be there. I think now the artifacts (small banding near high-contrast
// elements) were triggered by the sharpening my camera applied to the images;
// using TIFFs made from the RAW data did not show them.

template < typename dtype >
dtype hdr_fitness ( const vigra::TinyVector < dtype , 3 > & rgb ,
                    const float & peak ,
                    const float & socket )
{
  const float mu = .5f ;
  const float sigma = .2f ;
  
  // first step: grey projection. we use a simple grey projector
  // using the average of the three channels as the driving value.
  // Note how the 'optimal' value is not fixed at 128.0 for this
  // routine, it depends on how bright the brightness-adapted
  // pixels come out.

  auto max = peak * 2.0f ;

  dtype grey = ( rgb[0] + rgb[1] + rgb[2] ) / ( 3.0 * max ) ; // -> [ 0 : 1 ]

  // second step: calculate the difference from middle grey

  dtype d = grey - mu ;

  // third step: calculate the fitness from the distance.

  auto e = vspline::min ( 70.0f , ( d * d ) / ( 2.0f * sigma * sigma ) ) ;
  return exp ( - e ) ;
}

// alternative fitness function, producing per-channel fitness values.
// In theory, this should use more of the incoming information, processing
// data from partially blown pixels. But due to the non-linear camera
// response and the possibly imprecise production of linear RGB data
// from the input (which may well not be standard sRGB) it can produce
// colour casts in the highlights. For *true* linear RGB data this function
// should be fine, though.

template < typename dtype >
void hdr_fitness ( const vigra::TinyVector < dtype , 3 > & rgb ,
                   vigra::TinyVector < dtype , 3 > & fitness ,
                   const float & peak ,
                   const float & socket )
{
  for ( int channel = 0 ; channel < 3 ; channel++ )
  {
    auto difference = abs ( ( rgb [ channel ] - peak ) / peak ) ;

    dtype weight = exp ( -5.0 * difference ) ;
    weight -= exp ( -5.0f ) ; // might subtract a tad more

    fitness [ channel ] = std::max ( dtype ( socket ) , weight ) ;
  }
}

/// multi_layer_type is a 'blending' evaluator, which forms a composite
/// of all pixels found along the ray pertaining to a given target
/// coordinate - 'multi_layer', because this is as if the partial
/// images were semi-transparent layers draped in space.
/// To produce this effect, it's necessary to gather all the pixels
/// the ray passes through and feed them to a blending function which
/// forms the composite. The functions in this class are separated so
/// the the processing steps are reflected: the eval functions take
/// coordinate_type, which is a discrete coordinate into the target
/// array, albeit in floating point format. This coordinate is translated
/// to per-facet coordinates (using a multi_facet_helper_type object)
/// which are checked for validity. If valid in-facet coordinates are
/// found, they are stored in a vector of 'nodes' encoding the facet
/// number and coordinate together. This vector is passed on to the
/// blending function, which evaluates the per-facet coordinates and
/// produces the blended pixel. So we have two stages: the first stage,
/// in eval, is purely spatial, the second stage, blend, is invoked if
/// the spatial prerequisites are satisfied.

template < projection_type TPRJ >
struct multi_layer_type
: public tf23_type
{
  typedef tf23_type base_type ;

  typedef get_facet_from_crd < TPRJ > facet_detect_type ;

  using base_type::in_v ;
  using base_type::out_ele_v ;

  const facet_detect_type facet_detect ;
  const multi_facet_helper_type < TPRJ > mfh ;
  const std::vector < ev_type > evv ;
  const int nfacets ;
  int darkest_facet ;
  int brightest_facet ;

  // node which is stored as value in the multimap used for z-buffering.

  struct node_type
  {
    fc_t facet ;
    coordinate_type crd ;
  } ;

#ifdef VECTORIZE

  struct node_v
  {
    fc_t facet ;
    mask_type valid ;
    coordinate_v crd ;
  } ;

#endif

  multi_layer_type ( const facet_detect_type & _facet_detect ,
                     const std::vector < ev_type > & _evv )
  : facet_detect ( _facet_detect ) ,
    mfh ( _facet_detect.mfh ) ,
    evv ( _evv ) ,
    nfacets ( _facet_detect.nfacets )
  {
    float ceiling = 0.0f ;
    float floor = FLT_MAX ;
    for ( int i = 0 ; i < nfacets ; i++ )
    {
      const float & fc_brightness
                    ( mfh.facet_helper [ i ] . brightness ) ;
      if ( fc_brightness > ceiling )
      {
        ceiling = fc_brightness ;
        darkest_facet = i ;
      }
      if ( fc_brightness < floor )
      {
        floor = fc_brightness ;
        brightest_facet = i ;
      }
    }
  }

#define USE_GREY_PROJECTOR

#ifdef USE_GREY_PROJECTOR

  // simple HDR fusion by brightness only. The fitness is calculated so
  // that brightness values near the facet's middle grey are considered
  // fittest - hence the use of 'peak', which 'pulls in' the facet's
  // brightness value.

  void blend ( node_type * share , int nc , pixel_type & out ) const
  {
    pixel_type help ;

    float ttl = 0.0f ;

    for ( int i = 0 ; i < nc ; i++ )
    {
      const fc_t & facet ( share[i].facet ) ;
      bool darkest ( facet == darkest_facet ) ;
      const float & fc_brightness ( mfh.facet_helper [ facet ] . brightness ) ;

      // note that facet brightness has been applied to the image data
      // during 'digestion' of the source images. By multiplying with
      // fc_brightness here 'peak' corresponds to what was middle grey
      // in the image data.

//       float peak = 127.5f * fc_brightness ;
      float peak = ( darkest ? 128.0f : 127.5f ) * fc_brightness ;

      // 'socket' is the minimal fitness value assigned to a contributing
      // pixel. This value is near zero for all facets except the 'darkest one',
      // the facet which was taken with the shortest exposure time. Pixels
      // from this facet will therefore always contribute to the end result,
      // but if the other facets have fit(ter) contributions, this will not
      // have much effect on the total outcome. But if the other facets are
      // blown, we get the information from the darkest facet only, with
      // whatever highlight behaviour the image was produced with.
      // Rather than using a socket value of zero for the brighter facets,
      // we use a very small value, to deal with the corner case where the
      // darkest facet is invisble, which might result in a zero weight
      // total if the other facets are, e.g., blown.

      float socket = darkest ? .000001f : FLT_MIN ;

      // evaluate facet's evaluator, which has the image data, multiplied
      // by the brightness factor. So the result is i the range of
      // [ 0 .. 255 * brightness ]

      evv [ facet ] . eval ( share[i].crd , help ) ;

      auto fitness = hdr_fitness ( help , peak , socket ) ;

      ttl += fitness ;        // sum up all fitness values
      out += fitness * help ; // sum up weighted pixel value
    }

    // divide by sum of all fitness values. Note that the division is safe
    // because every partial weight, and hence their sum, is guaranteed
    // to be nonzero.

    out /= ttl ;
  }

  // vectorized version of above

#ifdef VECTORIZE

  void blend ( node_v * share , int nc , pixel_v & out ) const
  {
    pixel_v help ;
    channel_v ttl ( 0.0f ) ;
    mask_type vcum ( false ) ;

    for ( int i = 0 ; i < nc ; i++ )
    {
      const fc_t & facet ( share[i].facet ) ;
      bool darkest ( facet == darkest_facet ) ;
      const float & fc_brightness
                    ( mfh.facet_helper [ facet ] . brightness ) ;
//       float peak = 127.5f * fc_brightness ;
      float peak = ( darkest ? 128.0f : 127.5f ) * fc_brightness ;
      float socket = darkest ? .000001f : FLT_MIN ;

      evv [ facet ] . eval ( share[i].crd , help ) ;

      // cumulate validity: we need to know if there were any valid
      // data at all for every given coordinate

      vcum |= share[i].valid ;

      help[0] ( ! share[i].valid ) = 0.0f ;
      help[1] ( ! share[i].valid ) = 0.0f ;
      help[2] ( ! share[i].valid ) = 0.0f ;

      auto fitness = hdr_fitness ( help , peak , socket ) ;
      fitness ( ! share[i].valid ) = 0.0f ;
      ttl += fitness ;

      out[0] += fitness * help[0] ;
      out[1] += fitness * help[1] ;
      out[2] += fitness * help[2] ;
    }

    out[0] /= ttl ;
    out[1] /= ttl ;
    out[2] /= ttl ;

    // if there were no valid data at all at a given coordinate,
    // blacken the pixel

    out[0] ( ! vcum ) = 0.0f ;
    out[1] ( ! vcum ) = 0.0f ;
    out[2] ( ! vcum ) = 0.0f ;
  }

#endif

#else

  // experimental alternative: this version does not use a grey projector
  // and calculates individual weights for the three channels. There seem
  // to be some drawbacks with the corona of the sun, but otherwise the
  // results are quite pleasing.

  // TODO: equivalent version for alpha processing code

  void blend ( node_type * share , int nc , pixel_type & out ) const
  {
    pixel_type help ;

    pixel_type ttl ( 0.0f ) ;
    pixel_type fitness ;

    for ( int i = 0 ; i < nc ; i++ )
    {
      const fc_t & facet ( share[i].facet ) ;
      const float & fc_brightness ( mfh.facet_helper [ facet ] . brightness ) ;
      float peak = 127.5f * fc_brightness ;

      float socket = ( facet == darkest_facet ) ? .000001f : FLT_MIN ;

      evv [ facet ] . eval ( share[i].crd , help ) ;

      hdr_fitness ( help , fitness , peak , socket ) ;

      ttl += fitness ;        // sum up all fitness values
      out += fitness * help ; // sum up weighted pixel value
    }

    out /= ttl ;
  }

  // vectorized version of above

#ifdef VECTORIZE

  void blend ( node_v * share , int nc , pixel_v & out ) const
  {
    pixel_v help ;
    pixel_v ttl ( 0.0f ) ;
    pixel_v fitness ( 0.0f ) ;
    mask_type vcum ( false ) ;

    for ( int i = 0 ; i < nc ; i++ )
    {
      const fc_t & facet ( share[i].facet ) ;
      const float & fc_brightness
                    ( mfh.facet_helper [ facet ] . brightness ) ;
      float peak = 127.5f * fc_brightness ;
      float socket = ( facet == darkest_facet ) ? .000001f : FLT_MIN ;

      evv [ facet ] . eval ( share[i].crd , help ) ;

      vcum |= share[i].valid ;

      help[0] ( ! share[i].valid ) = 0.0f ;
      help[1] ( ! share[i].valid ) = 0.0f ;
      help[2] ( ! share[i].valid ) = 0.0f ;

      hdr_fitness ( help , fitness , peak , socket ) ;

      fitness[0] ( ! share[i].valid ) = 0.0f ;
      fitness[1] ( ! share[i].valid ) = 0.0f ;
      fitness[2] ( ! share[i].valid ) = 0.0f ;

      ttl += fitness ;

      out[0] += fitness[0] * help[0] ;
      out[1] += fitness[1] * help[1] ;
      out[2] += fitness[2] * help[2] ;
    }

    out[0] /= ttl[0] ;
    out[1] /= ttl[1] ;
    out[2] /= ttl[2] ;

    out[0] ( ! vcum ) = 0.0f ;
    out[1] ( ! vcum ) = 0.0f ;
    out[2] ( ! vcum ) = 0.0f ;
  }

#endif

#endif

  // evaluation without alpha processing. We gather all information
  // we need into a set of node_type objects, which is passed to the
  // blending function to produce the final result.

  void eval ( const coordinate_type & in ,
              pixel_type & out ) const
  {
    node_type share [ nfacets ] ;
    out = pixel_type ( 0.0f ) ;
    int nc = 0 ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      bool valid = facet_detect.get_coordinate
                       ( in , share[nc].crd , i ) ;

      // if the facet is not visible, we can ignore it

      if ( valid )
      {
        share[nc].facet = i ;
        ++nc ;
      }
    }

    // now we have the node information in 'share'

    if ( nc == 1 )
    {
      evv [ share[0].facet ] . eval ( share[0].crd , out ) ;
    }
    else if ( nc > 0 )
    {
      blend ( share , nc , out ) ;
    }
  }

  // vectorized evaluation

#ifdef VECTORIZE

  void eval ( const in_v & in ,
              pixel_v & out ) const
  {
    node_v share [ nfacets ] ;
    out = pixel_v ( 0.0f ) ;
    int nc = 0 ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      share[nc].valid = facet_detect.get_coordinate
                            ( in , share[nc].crd , i ) ;

      // if the facet is not visible, we can ignore it

      if ( any_of ( share[nc].valid ) )
      {
        share[nc].facet = i ;
        ++nc ;
      }
    }

    if ( nc == 1 )
    {
      evv [ share[0].facet ] . eval ( share[0].crd , out ) ;
      out[0] ( ! share[0].valid ) = 0.0f ;
      out[1] ( ! share[0].valid ) = 0.0f ;
      out[2] ( ! share[0].valid ) = 0.0f ;
    }
    else if ( nc > 0 )
    {
      blend ( share , nc , out ) ;
    }
  }

#endif

} ;

// multi_exposure_type provides an experimental blending mode for
// 'quorate' blending (use --blending=quorate). What does that mean?
// When processing a ray, all pairs of pixels are investigated,
// and if they are 'similar' to each other, this increases both
// their weights. Outliers, which aren't ever close to other pixels,
// are 'punished' because they never receive the 'proximity reward'.
// Being a pixel-based method, the images must match very well to
// avoid artifacts where the 'ghost' shimmers through, and at least
// three images are needed to form the 'quorate' decision. If there
// are less than two pixels on the ray, multi_exposure_type falls
// back to HDR blending, by delegating to it's base class,
// multi_layer_type.
// A good candidate for use with quorate blending are image sets done
// with a camera's serial shot mode, best done from a tripod.
// The current implementation only provides non-alpha code and no
// SIMD variant, so it's quite slow, but enough to experiment with.

template < projection_type TPRJ >
struct multi_exposure_type
: public multi_layer_type < TPRJ >
{
  typedef multi_layer_type < TPRJ > base_type ;

  using typename base_type::in_v ;
  using typename base_type::out_ele_v ;
  using typename base_type::node_type ;
  using typename base_type::facet_detect_type ;

  using base_type::evv ;
  using base_type::nfacets ;
  using base_type::facet_detect ;
  using base_type::blend ;

  multi_exposure_type ( const facet_detect_type & _facet_detect ,
                        const std::vector < ev_type > & _evv )
  : base_type ( _facet_detect , _evv )
  { }

  void qblend ( node_type * share , int nc , pixel_type & out ) const
  {
    pixel_type rgb [ nc ] ;
    float bonus [ nc ] ;
    float norm = 0.0f ;

    if ( nc < 3 )
    {
      // can't form a quorum with just one or two pixels, so we delegate
      // such cases to the base class: HDR blending.

      blend ( share , nc , out ) ;
      return ;
    }

    for ( int i = 0 ; i < nc ; i++ )
    {
      const fc_t & facet ( share[i].facet ) ;
      evv [ facet ] . eval ( share[i].crd , rgb[i] ) ;
      bonus [ i ] = 0.0f ;
    }

    for ( int i = 0 ; i < ( nc - 1 ) ; i++ )
    {
      for ( int j = i + 1 ; j < nc ; j++ )
      {
        // the closer the intensity, the better 'quality' q*, E [0:1]

        auto qr = exp ( -.25f * fabs ( rgb[i][0] - rgb[j][0] ) ) ;
        auto qg = exp ( -.25f * fabs ( rgb[i][1] - rgb[j][1] ) ) ;
        auto qb = exp ( -.25f * fabs ( rgb[i][2] - rgb[j][2] ) ) ;

        // the least similar channel provides the weight; if all
        // channels in both pixels are equal the weight is 1.

        auto weight = std::min ( qr , qg ) ;
        weight = std::min ( weight , qb ) ;

        // both participating pixels receive the weight equally

        bonus [ i ] += weight ;
        bonus [ j ] += weight ;
        norm += 2.0f * weight ;
      }
    }

    // the output pixel is weighted by the 'bonus' values, and then
    // normalized.

    out = rgb [ 0 ] * bonus [ 0 ] ;

    for ( int i = 1 ; i < nc ; i++ )
    {
      out += ( rgb [ i ] * bonus [ i ] ) ;
    }

    out /= norm ;
  }

#ifdef VECTORIZE

  using typename base_type::node_v ;

  void qblend ( node_v * share , int nc , pixel_v & out ) const
  {
    pixel_v rgb [ nc ] ;
    channel_v bonus [ nc ] ;
    channel_v norm = 0.0f ;
    int_v nvalid = 0 ;

    // how many valid coordinates do we have for the facets?

    for ( int i = 0 ; i < nc ; i++ )
    {
      nvalid ( share[i].valid ) += 1 ;
    }

    if ( all_of ( nvalid < 3 ) )
    {
      // no quorate blending at all for this vector

      blend ( share , nc , out ) ;
      return ;
    }

    // we do have at least some candidates for quoreate blending.

    for ( int i = 0 ; i < nc ; i++ )
    {
      const fc_t & facet ( share[i].facet ) ;
      evv [ facet ] . eval ( share[i].crd , rgb[i] ) ;
      // do we need that:
//       rgb[i][0] ( ! share[i].valid ) = 0.0f ;
//       rgb[i][1] ( ! share[i].valid ) = 0.0f ;
//       rgb[i][2] ( ! share[i].valid ) = 0.0f ;
      bonus [ i ] = 0.0f ;
    }

    for ( int i = 0 ; i < ( nc - 1 ) ; i++ )
    {
      for ( int j = i + 1 ; j < nc ; j++ )
      {
        // the closer the intensity, the better 'quality' q*, E [0:1]
        // bug in vspline or Vc? exponents below -80 or so result in
        // nan when passed to exp(). This avoids the bug, but it sucks:

        auto er = vspline::min ( 70.0f , .25f * abs ( rgb[i][0] - rgb[j][0] ) ) ;
        auto eg = vspline::min ( 70.0f , .25f * abs ( rgb[i][1] - rgb[j][1] ) ) ;
        auto eb = vspline::min ( 70.0f , .25f * abs ( rgb[i][2] - rgb[j][2] ) ) ;

        auto qr = exp ( - er ) ;
        auto qg = exp ( - eg ) ;
        auto qb = exp ( - eb ) ;

        // the least similar channel provides the weight; if all
        // channels in both pixels are equal the weight is 1.

        auto weight = vspline::min ( qr , qg ) ;
        weight = vspline::min ( weight , qb ) ;

        // if the coordinate for either facet i or facet j is invalid,
        // we assign zero weight
      
        weight ( ! share [ i ] . valid ) = 0.0f ;
        weight ( ! share [ j ] . valid ) = 0.0f ;

        // both participating pixels receive the weight equally

        bonus [ i ] += weight ;
        bonus [ j ] += weight ;
        norm += 2.0f * weight ;
      }
    }

    // the output pixel is weighted by the 'bonus' values, and then
    // normalized.

    out = rgb [ 0 ] * bonus [ 0 ] ;

    for ( int i = 1 ; i < nc ; i++ )
    {
      out += ( rgb [ i ] * bonus [ i ] ) ;
    }

    out /= norm ;

    out[0] ( norm == 0.0f ) = 0.0f ;
    out[1] ( norm == 0.0f ) = 0.0f ;
    out[2] ( norm == 0.0f ) = 0.0f ;

    // if some coordinates could not be processed with quorate blending,
    // we call the HDR blending code and take it's result for these
    // coordinates.

    if ( ! all_of ( nvalid > 2 ) )
    {
      pixel_v help ;
      blend ( share , nc , help ) ;
      out[0] ( nvalid < 3 ) = help[0] ;
      out[1] ( nvalid < 3 ) = help[1] ;
      out[2] ( nvalid < 3 ) = help[2] ;
    }
  }
  
#endif

  // evaluation without alpha processing. We gather all information
  // we need into a set of node_type objects, which is passed to the
  // blending function to produce the final result.

  void eval ( const coordinate_type & in ,
              pixel_type & out ) const
  {
    node_type share [ nfacets ] ;
    out = pixel_type ( 0.0f ) ;
    int nc = 0 ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      bool valid = facet_detect.get_coordinate
                       ( in , share[nc].crd , i ) ;

      // if the facet is not visible, we can ignore it

      if ( valid )
      {
        share[nc].facet = i ;
        ++nc ;
      }
    }

    // now we have the node information in 'share'

    if ( nc == 1 )
    {
      evv [ share[0].facet ] . eval ( share[0].crd , out ) ;
    }
    else if ( nc > 0 )
    {
      qblend ( share , nc , out ) ;
    }
  }

  // vectorized evaluation

#ifdef VECTORIZE

  void eval ( const in_v & in ,
              pixel_v & out ) const
  {
    node_v share [ nfacets ] ;
    out = pixel_v ( 0.0f ) ;
    int nc = 0 ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      share[nc].valid = facet_detect.get_coordinate
                            ( in , share[nc].crd , i ) ;

      // if the facet is not visible, we can ignore it

      if ( any_of ( share[nc].valid ) )
      {
        share[nc].facet = i ;
        ++nc ;
      }
    }

    if ( nc == 1 )
    {
      evv [ share[0].facet ] . eval ( share[0].crd , out ) ;
      out[0] ( ! share[0].valid ) = 0.0f ;
      out[1] ( ! share[0].valid ) = 0.0f ;
      out[2] ( ! share[0].valid ) = 0.0f ;
    }
    else if ( nc > 0 )
    {
      qblend ( share , nc , out ) ;
    }
  }

#endif

} ;

// next we have equivalent code with alpha processing. The strategy
// is the same, but the blending is slightly different.

template < projection_type TPRJ >
struct multi_layer_type_4
: public tf24_type
{
  typedef tf24_type base_type ;

  typedef get_facet_from_crd < TPRJ > facet_detect_type ;

  using base_type::in_v ;
  using base_type::out_ele_v ;

  const facet_detect_type facet_detect ;
  const multi_facet_helper_type < TPRJ > mfh ;
  const std::vector < ev_type > evv ;
  const std::vector < alpha_ev_type > aevv ;
  const int nfacets ;
  int darkest_facet ;

  // node which is stored as value in the multimap used for z-buffering.

  struct node_type
  {
    fc_t facet ;
    float alpha ;
    coordinate_type crd ;
  } ;

#ifdef VECTORIZE

  struct node_v
  {
    fc_t facet ;
    mask_type valid ;
    channel_v alpha ;
    coordinate_v crd ;
  } ;

#endif

  // simple HDR fusion by brightness only. The fitness is calculated so
  // that brightness values near the facet's middle grey are considered
  // fittest - hence the use of 'peak', which 'pulls in' the facet's
  // brightness value.

  void blend ( node_type * share , int nc , pixel4_type & out ) const
  {
    pixel_type help ;
    pixel_type & rgb = reinterpret_cast < pixel_type & > ( out ) ;
    float ttl = 0.0f ;
    float max_alpha = 0.0f ;

    for ( int i = 0 ; i < nc ; i++ )
    {
      const fc_t & facet ( share[i].facet ) ;
      bool darkest ( facet == darkest_facet ) ;
      const float & fc_brightness ( mfh.facet_helper [ facet ] . brightness ) ;
//       float peak = 127.5f * fc_brightness ;
      float peak = ( darkest ? 128.0f : 127.5f ) * fc_brightness ;
      float socket = darkest ? .000001f : FLT_MIN ;

      // evaluate facet's evaluator, which has the image data, multiplied
      // by the brightness factor. So the result is i the range of
      // [ 0 .. 255 * brightness ]

      evv [ facet ] . eval ( share[i].crd , help ) ;

      auto fitness = hdr_fitness ( help , peak , socket ) ;
      fitness *= share[i].alpha ;
      if ( share[i].alpha > max_alpha )
        max_alpha = share[i].alpha ;

      ttl += fitness ;        // sum up all fitness values
      rgb += fitness * help ; // sum up weighted pixel value
    }

    // divide by sum of all fitness values

    if ( ttl > 0.0f )
      rgb /= ttl ;

    out[3] = max_alpha ;
  }

  // vectorized version of above

#ifdef VECTORIZE

  void blend ( node_v * share , int nc , pixel4_v & out ) const
  {
    pixel_v help ;
    pixel_v & rgb = reinterpret_cast < pixel_v & > ( out ) ;
    channel_v ttl ( 0.0f ) ;
    channel_v alpha_max ( 0.0f ) ;
    mask_type vcum ( false ) ;

    for ( int i = 0 ; i < nc ; i++ )
    {
      const fc_t & facet ( share[i].facet ) ;
      bool darkest ( facet == darkest_facet ) ;
      const float & fc_brightness ( mfh.facet_helper [ facet ] . brightness ) ;
//       float peak = 127.5f * fc_brightness ;
      float peak = ( darkest ? 128.0f : 127.5f ) * fc_brightness ;
      float socket = darkest ? .000001f : FLT_MIN ;

      // evaluate facet's evaluator, which has the image data, multiplied
      // by the brightness factor. So the result is i the range of
      // [ 0 .. 255 * brightness ]

      evv [ facet ] . eval ( share[i].crd , help ) ;

      vcum |= share[i].valid ;

      help[0] ( ! share[i].valid ) = 0.0f ;
      help[1] ( ! share[i].valid ) = 0.0f ;
      help[2] ( ! share[i].valid ) = 0.0f ;

      auto fitness = hdr_fitness ( help , peak , socket ) ;
      fitness ( ! share[i].valid ) = 0.0f ;
      fitness *= share[i].alpha ;
      alpha_max ( share[i].alpha > alpha_max ) = share[i].alpha ;

      ttl += fitness ;

      rgb[0] += fitness * help[0] ;
      rgb[1] += fitness * help[1] ;
      rgb[2] += fitness * help[2] ;
    }
    rgb[0] ( ttl > 0.0f ) /= ttl ;
    rgb[1] ( ttl > 0.0f ) /= ttl ;
    rgb[2] ( ttl > 0.0f ) /= ttl ;

    out[3] = alpha_max ;

    // if there were no valid data at all at a given coordinate,
    // make the pixel totally transparent

    out[3] ( ! vcum ) = 0.0f ;
  }

#endif

  multi_layer_type_4 ( const facet_detect_type & _facet_detect ,
                       const std::vector < ev_type > & _evv ,
                       const std::vector < alpha_ev_type > & _aevv )
  : facet_detect ( _facet_detect ) ,
    mfh ( _facet_detect.mfh ) ,
    evv ( _evv ) ,
    aevv ( _aevv ) ,
    nfacets ( _facet_detect.nfacets )
  {
    float ceiling = 0.0f ;
    for ( int i = 0 ; i < nfacets ; i++ )
    {
      const float & fc_brightness
                    ( mfh.facet_helper [ i ] . brightness ) ;
      if ( fc_brightness > ceiling )
      {
        ceiling = fc_brightness ;
        darkest_facet = i ;
      }
    }
  }

  // evaluation with full alpha processing. This is quite involved,
  // because if non-opaque pixels are involved, we need to do z-buffering
  // and alpha blending to get a correct result.

  void eval ( const coordinate_type & in ,
              pixel4_type & out ) const
  {
    pixel_type & help ( reinterpret_cast < pixel_type & > ( out ) ) ;
    node_type share [ nfacets ] ;

    int nc = 0 ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      bool valid = facet_detect.get_coordinate ( in , share[nc].crd , i ) ;
  
      // if the facet is not visible, we can ignore it

      if ( ! valid )
        continue ;

      // get the alpha value

      aevv [ i ] . eval ( share[nc].crd , share[nc].alpha ) ;

      // we can ignore fully transparent facets altogether.

      if ( share[nc].alpha > 0.0f )
      {
        share[nc].facet = i ;
        ++nc ;
      }
    }

    // now we have the node information in 'share'

    if ( nc == 0 )
    {
      out = pixel4_type ( 0.0f ) ;
    }
    else if ( nc == 1 )
    {
      evv [ share[0].facet ] . eval ( share[0].crd , help ) ;
      out[3] = share[0].alpha ;
    }
    else
    {
      // we have several contributing facets.
      out = pixel4_type ( 0.0f ) ;
      blend ( share , nc , out ) ;
    }
  }

#ifdef VECTORIZE

  void eval ( const in_v & in ,
              pixel4_v & out ) const
  {
    pixel_v & help ( reinterpret_cast < pixel_v & > ( out ) ) ;

    if ( nfacets == 0 )
    {
      out = pixel4_v ( 0.0f ) ;
    }
    else
    {
      node_v share [ nfacets ] ;
      int nc = 0 ;

      for ( int i = 0 ; i < nfacets ; i++ )
      {
        share[nc].valid = facet_detect.get_coordinate ( in , share[nc].crd , i ) ;

        // if the facet is not visible, we can ignore it

        if ( any_of ( share[nc].valid ) )
        {
          aevv [ i ] . eval ( share[nc].crd , share[nc].alpha ) ;
          share[nc].alpha ( ! share[nc].valid ) = 0.0f ;
          if ( any_of ( share[nc].alpha != 0.0f ) )
          {
            share[nc].facet = i ;
            ++nc ;
          }
        }
      }

      if ( nc > 0 )
      {
        out = pixel4_v ( 0.0f ) ;
        blend ( share , nc , out ) ;
      }
    }
  }

#endif

} ;

// quorate blending with an alpha channel. The code is very similar to
// the non-alpha code. The final outcome will have an alpha channel with
// the maximum alpha value over all partials. non-opaque alpha values
// result in lessened weight of partials.

template < projection_type TPRJ >
struct multi_exposure_type_4
: public multi_layer_type_4 < TPRJ >
{
  typedef multi_layer_type_4 < TPRJ > base_type ;

  using typename base_type::in_v ;
  using typename base_type::out_ele_v ;
  using typename base_type::node_type ;
  using typename base_type::facet_detect_type ;

  using base_type::evv ;
  using base_type::aevv ;
  using base_type::nfacets ;
  using base_type::facet_detect ;
  using base_type::blend ;

  multi_exposure_type_4 ( const facet_detect_type & _facet_detect ,
                          const std::vector < ev_type > & _evv ,
                          const std::vector < alpha_ev_type > & _aevv )
  : base_type ( _facet_detect , _evv , _aevv )
  { }

  void qblend ( node_type * share , int nc , pixel4_type & out ) const
  {
    pixel_type rgb [ nc ] ;
    float bonus [ nc ] ;
    float norm = 0.0f ;
    float max_alpha = 0.0f ;

    if ( nc < 3 )
    {
      // can't form a quorum with just one or two pixels, so we delegate
      // such cases to the base class: HDR blending.

      blend ( share , nc , out ) ;
      return ;
    }

    for ( int i = 0 ; i < nc ; i++ )
    {
      const fc_t & facet ( share[i].facet ) ;
      evv [ facet ] . eval ( share[i].crd , rgb[i] ) ;
      bonus [ i ] = 0.0f ;
      if ( share[i].alpha > max_alpha )
        max_alpha = share[i].alpha ;
    }

    for ( int i = 0 ; i < ( nc - 1 ) ; i++ )
    {
      for ( int j = i + 1 ; j < nc ; j++ )
      {
        // the closer the intensity, the better 'quality' q*, E [0:1]

        auto qr = exp ( -.25f * fabs ( rgb[i][0] - rgb[j][0] ) ) ;
        auto qg = exp ( -.25f * fabs ( rgb[i][1] - rgb[j][1] ) ) ;
        auto qb = exp ( -.25f * fabs ( rgb[i][2] - rgb[j][2] ) ) ;

        // the least similar channel provides the weight; if all
        // channels in both pixels are equal the weight is 1.

        auto weight = std::min ( qr , qg ) ;
        weight = std::min ( weight , qb ) ;

        // both participating pixels receive the weight multiplied
        // with the alpha value

        auto bi = weight * share[i].alpha ;
        norm += bi ;
        bonus [ i ] += bi ;

        auto bj = weight * share[j].alpha ;
        norm += bj ;
        bonus [ j ] += bj ;
      }
    }

    // the output pixel is weighted by the 'bonus' values, and then
    // normalized.

    out[0] = rgb[0][0] * bonus[0] ;
    out[1] = rgb[0][1] * bonus[0] ;
    out[2] = rgb[0][2] * bonus[0] ;

    for ( int i = 1 ; i < nc ; i++ )
    {
      out[0] += rgb[i][0] * bonus[i] ;
      out[1] += rgb[i][1] * bonus[i] ;
      out[2] += rgb[i][2] * bonus[i] ;
    }

    out[0] /= norm ;
    out[1] /= norm ;
    out[2] /= norm ;
    out[3] = max_alpha ;
  }

#ifdef VECTORIZE

  using typename base_type::node_v ;

  void qblend ( node_v * share , int nc , pixel4_v & out ) const
  {
    pixel_v rgb [ nc ] ;
    channel_v bonus [ nc ] ;
    channel_v norm = 0.0f ;
    channel_v alpha_max ( 0.0f ) ;
    int_v nvalid = 0 ;

    // how many valid coordinates do we have for the facets?

    for ( int i = 0 ; i < nc ; i++ )
    {
      nvalid ( share[i].valid ) += 1 ;
    }

    if ( all_of ( nvalid < 3 ) )
    {
      // no quorate blending at all for this vector

      blend ( share , nc , out ) ;
      return ;
    }

    // we do have at least some candidates for quorate blending.

    for ( int i = 0 ; i < nc ; i++ )
    {
      const fc_t & facet ( share[i].facet ) ;
      evv [ facet ] . eval ( share[i].crd , rgb[i] ) ;
      alpha_max ( share[i].alpha > alpha_max ) = share[i].alpha ;
      bonus [ i ] = 0.0f ;
    }

    for ( int i = 0 ; i < ( nc - 1 ) ; i++ )
    {
      for ( int j = i + 1 ; j < nc ; j++ )
      {
        // the closer the intensity, the better 'quality' q*, E [0:1]
        // bug in vspline or Vc? exponents below -80 or so result in
        // nan when passed to exp(). This avoids the bug, but it sucks:

        auto er = vspline::min ( 70.0f , .25f * abs ( rgb[i][0] - rgb[j][0] ) ) ;
        auto eg = vspline::min ( 70.0f , .25f * abs ( rgb[i][1] - rgb[j][1] ) ) ;
        auto eb = vspline::min ( 70.0f , .25f * abs ( rgb[i][2] - rgb[j][2] ) ) ;

        auto qr = exp ( - er ) ;
        auto qg = exp ( - eg ) ;
        auto qb = exp ( - eb ) ;

        // the least similar channel provides the weight; if all
        // channels in both pixels are equal the weight is 1.

        auto weight = vspline::min ( qr , qg ) ;
        weight = vspline::min ( weight , qb ) ;

        // if the coordinate for either facet i or facet j is invalid,
        // we assign zero weight
      
        weight ( ! share [ i ] . valid ) = 0.0f ;
        weight ( ! share [ j ] . valid ) = 0.0f ;

        // both participating pixels receive the weight multiplied with
        // the alpha value

        auto bi = weight * share[i].alpha ;
        norm += bi ;
        bonus [ i ] += bi ;

        auto bj = weight * share[j].alpha ;
        norm += bj ;
        bonus [ j ] += bj ;
      }
    }

    // the output pixel is weighted by the 'bonus' values, and then
    // normalized.

    out[0] = rgb[0][0] * bonus[0] ;
    out[1] = rgb[0][1] * bonus[0] ;
    out[2] = rgb[0][2] * bonus[0] ;

    for ( int i = 1 ; i < nc ; i++ )
    {
      out[0] += rgb[i][0] * bonus[i] ;
      out[1] += rgb[i][1] * bonus[i] ;
      out[2] += rgb[i][2] * bonus[i] ;
    }

    out[0] /= norm ;
    out[1] /= norm ;
    out[2] /= norm ;

    out[0] ( norm == 0.0f ) = 0.0f ;
    out[1] ( norm == 0.0f ) = 0.0f ;
    out[2] ( norm == 0.0f ) = 0.0f ;

    out[3] = alpha_max ;

    // if some coordinates could not be processed with quorate blending,
    // we call the HDR blending code and take it's result for these
    // coordinates.

    if ( ! all_of ( nvalid > 2 ) )
    {
      pixel4_v help ;
      blend ( share , nc , help ) ;
      out[0] ( nvalid < 3 ) = help[0] ;
      out[1] ( nvalid < 3 ) = help[1] ;
      out[2] ( nvalid < 3 ) = help[2] ;
      out[3] ( nvalid < 3 ) = help[3] ;
    }
  }
  
#endif

  // evaluation with full alpha processing. This is quite involved,
  // because if non-opaque pixels are involved, we need to do z-buffering
  // and alpha blending to get a correct result.

  void eval ( const coordinate_type & in ,
              pixel4_type & out ) const
  {
    pixel_type & help ( reinterpret_cast < pixel_type & > ( out ) ) ;
    node_type share [ nfacets ] ;

    int nc = 0 ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      bool valid = facet_detect.get_coordinate ( in , share[nc].crd , i ) ;
  
      // if the facet is not visible, we can ignore it

      if ( ! valid )
        continue ;

      // get the alpha value

      aevv [ i ] . eval ( share[nc].crd , share[nc].alpha ) ;

      // we can ignore fully transparent facets altogether.

      if ( share[nc].alpha > 0.0f )
      {
        share[nc].facet = i ;
        ++nc ;
      }
    }

    // now we have the node information in 'share'

    if ( nc == 0 )
    {
      out = pixel4_type ( 0.0f ) ;
    }
    else if ( nc == 1 )
    {
      evv [ share[0].facet ] . eval ( share[0].crd , help ) ;
      out[3] = share[0].alpha ;
    }
    else
    {
      // we have several contributing facets.
      out = pixel4_type ( 0.0f ) ;
      qblend ( share , nc , out ) ;
    }
  }

#ifdef VECTORIZE

  void eval ( const in_v & in ,
              pixel4_v & out ) const
  {
    pixel_v & help ( reinterpret_cast < pixel_v & > ( out ) ) ;

    if ( nfacets == 0 )
    {
      out = pixel4_v ( 0.0f ) ;
    }
    else
    {
      node_v share [ nfacets ] ;
      int nc = 0 ;

      for ( int i = 0 ; i < nfacets ; i++ )
      {
        share[nc].valid = facet_detect.get_coordinate ( in , share[nc].crd , i ) ;

        // if the facet is not visible, we can ignore it

        if ( any_of ( share[nc].valid ) )
        {
          aevv [ i ] . eval ( share[nc].crd , share[nc].alpha ) ;
          share[nc].alpha ( ! share[nc].valid ) = 0.0f ;
          if ( any_of ( share[nc].alpha != 0.0f ) )
          {
            share[nc].facet = i ;
            ++nc ;
          }
        }
      }

      if ( nc > 0 )
      {
        out = pixel4_v ( 0.0f ) ;
        qblend ( share , nc , out ) ;
      }
    }
  }

#endif

} ;

// class correlate_type serves as a platform to calculate inter-facet
// relationship data. The concrete measurement is done by 'reduce_type',
// which receives the information at the pixel level and does the
// cumulation of results.

template < projection_type TPRJ , class reduce_type >
struct correlate_type
: public vspline::sink_functor < coordinate_type , VSIZE >
{
  typedef vspline::sink_functor < coordinate_type , VSIZE >
    base_type ;

  typedef get_facet_from_crd < TPRJ > facet_detect_type ;

  const facet_detect_type facet_detect ;
  reduce_type reduce ;
  const multi_facet_helper_type < TPRJ > mfh ;
  const int nfacets ;

  typedef typename reduce_type::node_type node_type ;

  int translate_facet_number ( int internal )
  {
    return mfh.facet_number [ internal ] ;
  }

  correlate_type ( const facet_detect_type & _facet_detect ,
                   const reduce_type & _reduce )
  : facet_detect ( _facet_detect ) ,
    mfh ( _facet_detect.mfh ) ,
    reduce ( _reduce ) ,
    nfacets ( _facet_detect.nfacets )
  { }

  // evaluation without alpha processing. We gather all information
  // we need into a set of node_type objects, which is passed to the
  // blending function to produce the final result.

  void operator() ( const coordinate_type & in )
  {
    node_type share [ nfacets ] ;
    int nc = 0 ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      bool valid = facet_detect.get_coordinate
                       ( in , share[nc].crd , i ) ;

      // if the facet is not visible, we can ignore it

      if ( valid )
      {
        share[nc].facet = i ;
        ++nc ;
      }
    }

    // now we have the node information in 'share'

    if ( nc > 1 )
    {
      reduce ( share , nc ) ;
    }
  }

  // vectorized evaluation

#ifdef VECTORIZE

  typedef typename reduce_type::node_v node_v ;
  using base_type::in_v ;

  void operator() ( const in_v & in )
  {
    node_v share [ nfacets ] ;
    int nc = 0 ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      share[nc].valid = facet_detect.get_coordinate
                            ( in , share[nc].crd , i ) ;

      // if the facet is not visible, we can ignore it

      if ( any_of ( share[nc].valid ) )
      {
        share[nc].facet = i ;
        ++nc ;
      }
    }

    if ( nc > 1 )
    {
      reduce ( share , nc ) ;
    }
  }

#endif

} ;

template < projection_type TPRJ , class reduce_type >
struct correlate4_type
: public correlate_type < TPRJ , reduce_type >
{
  typedef correlate_type < TPRJ , reduce_type > base_type ;
  const std::vector < alpha_ev_type > & aevv ;
  using base_type::facet_detect ;
  typedef get_facet_from_crd < TPRJ > facet_detect_type ;

  using typename base_type::node_type ;
  using base_type::nfacets ;
  using base_type::reduce ;

  correlate4_type ( const facet_detect_type & _facet_detect ,
                    const reduce_type & _reduce ,
                    const std::vector < alpha_ev_type > & _aevv )
  : base_type ( _facet_detect , _reduce ) ,
    aevv ( _aevv )
  { }

  // evaluation without alpha processing. We gather all information
  // we need into a set of node_type objects, which is passed to the
  // blending function to produce the final result.

  void operator() ( const coordinate_type & in )
  {
    node_type share [ nfacets ] ;
    int nc = 0 ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      bool valid = facet_detect.get_coordinate
                       ( in , share [ nc ] . crd , i ) ;

      // if the facet is not visible, we can ignore it

      if ( valid )
      {
        float alpha ;
        aevv [ i ] . eval ( share [ nc ] . crd , alpha ) ;

        // the RGB content is valid where alpha is fully opaque.
        // in other places it may only be present because of fill-in.
        // but fill-in only produces very low alpha, so with a higher
        // threshold here these extrapolated values should be
        // excluded from the comparison.

        if ( alpha >= 0.15f )
        {
          share [ nc ] . facet = i ;
          ++nc ;
        }
      }
    }

    // now we have the node information in 'share'

    if ( nc > 1 )
    {
      reduce ( share , nc ) ;
    }
  }

  // vectorized evaluation

#ifdef VECTORIZE

  using typename base_type::in_v ;
  using typename base_type::node_v ;

  void operator() ( const in_v & in )
  {
    node_v share [ nfacets ] ;
    int nc = 0 ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      share[nc].valid = facet_detect.get_coordinate
                            ( in , share[nc].crd , i ) ;

      // if the facet is not visible, we can ignore it

      if ( any_of ( share[nc].valid ) )
      {
        channel_v alpha ;
        aevv [ i ] . eval ( share [ nc ] . crd , alpha ) ;

        // the RGB content is valid where alpha is fully opaque.
        // in other places it may only be present because of fill-in.
        // but fill-in only produces very low alpha, so with a higher
        // threshold here these extrapolated values should be
        // excluded from the comparison.

        share [ nc ] . valid &= ( alpha >= 0.15f ) ;
        if ( any_of ( share [ nc ] . valid ) )
        {
          share [ nc ] . facet = i ;
          ++nc ;
        }
      }
    }

    if ( nc > 1 )
    {
      reduce ( share , nc ) ;
    }
  }

#endif

} ;

// brightness_reductor is a concrete reductor handling inter-facet
// brightness correlation. It's employed by a correlate_type object
// to cumulate the pixel-level data into the result.

std::mutex reduce_mutex ;

struct brightness_reductor
: public vspline::sink_functor < coordinate_type , VSIZE >
{
  typedef vspline::sink_functor < coordinate_type , VSIZE > base_type ;
  using typename base_type::in_type ;
  using typename base_type::in_v ;
  using base_type::vsize ;
  const int nfacets ;
  typedef vspline::vector_traits < double , VSIZE > :: type d_v ;
  typedef vigra::MultiArray < 2 , double > cref_type ;
  cref_type & cref ;
  cref_type partial_cref ;
  const std::vector < ev_type > & evv ;

  brightness_reductor ( cref_type & _cref ,
                        const std::vector < ev_type > & _evv )
  : cref ( _cref ) ,
    evv ( _evv ) ,
    nfacets ( _evv.size() - 1 ) ,
    partial_cref ( vigra::Shape2 ( _evv.size() - 1 , _evv.size() - 1 ) )
  { }

  brightness_reductor ( const brightness_reductor & other )
  : brightness_reductor ( other.cref , other.evv )
  { }

  struct node_type
  {
    fc_t facet ;
    coordinate_type crd ;
  } ;

  void operator() ( node_type * share , int nc )
  {
    pixel_type px [ nfacets ] ;

    // evaluate the per-facet coordinates in 'share' to receive pixel
    // values (in float RGB). We already know that all coordinates
    // in 'share' are valid, and that there are at least two nodes
    // in 'share', because the calling code skips over all other
    // cases.

    for ( int i = 0 ; i < nc ; i++ )
      evv [ share [ i ] . facet ] . eval ( share [ i ] . crd , px [ i ] ) ;

    // for each channel, we check all pairings of facets for out-of-range
    // values, which are ignored. In-range vaues are cumulated, discarding
    // the channel information. The 'total light' correlation is encoded
    // in partial_cref, which is used 'further up' to calculate ratios
    // of brightness between the facets.

    for ( int channel = 0 ; channel < 3 ; channel++ )
    {
      for ( int i = 0 ; i < nc - 1 ; i++ )
      {
        float vi = ( px [ i ] [ channel ] ) ;

        if ( vi > 0.0f && vi < 220.0f )
        {
          for ( int j = i + 1 ; j < nc ; j++ )
          {
            float vj = ( px [ j ] [ channel ] ) ;

            if ( vj > 0.0f && vj < 220.0f )
            {
              // for the pairing (i,j), this channel is valid in
              // both facets. We store the value coming from facet i
              // on the 'side' of the crosscorrelation matrix where
              // i > j, and the value coming from facet j on the side
              // where i < j. Later, when we form a ratio from the
              // cumulated values in cref, all we need to do is to
              // divide one by the other.

              auto fca = share [ i ] . facet ;
              auto fcb = share [ j ] . facet ;

              partial_cref [ { fca , fcb } ] += vi ;
              partial_cref [ { fcb , fca } ] += vj ;
            }
          }
        }
      }
    }
  }

#ifdef VECTORIZE

  struct node_v
  {
    fc_t facet ;
    mask_type valid ;
    coordinate_v crd ;
  } ;

  void operator() ( node_v * share , int nc )
  {
    pixel_v px [ nfacets ] ;

    for ( int i = 0 ; i < nc ; i++ )
      evv [ share [ i ] . facet ] . eval ( share [ i ] . crd , px [ i ] ) ;

    for ( int channel = 0 ; channel < 3 ; channel++ )
    {
      for ( int i = 0 ; i < nc - 1 ; i++ )
      {
        channel_v vi = ( px [ i ] [ channel ] ) ;

        mask_type i_valid = share [ i ] . valid ;
        i_valid &= ( ( vi > 0.0f ) & ( vi < 220.0f ) ) ;

        if ( any_of ( i_valid ) )
        {
          for ( int j = i + 1 ; j < nc ; j++ )
          {
            channel_v vj = ( px [ j ] [ channel ] ) ;

            mask_type j_valid = i_valid ;
            j_valid &= share [ j ] . valid ;
            j_valid &= ( ( vj > 0.0f ) & ( vj < 220.0f ) ) ;

            if ( any_of ( j_valid ) )
            {
              channel_v cum_i ( 0.0f ) ;
              cum_i ( j_valid ) = vi ;

              channel_v cum_j ( 0.0f ) ;
              cum_j ( j_valid ) = vj ;

              auto fca = share [ i ] . facet ;
              auto fcb = share [ j ] . facet ;

              partial_cref [ { fca , fcb } ] += cum_i.sum() ;
              partial_cref [ { fcb , fca } ] += cum_j.sum() ;
            }
          }
        }
      }
    }
  }

#endif // VECTORIZE

  ~brightness_reductor()
  {
    // update cross reference under protection of reduce_mutex,
    // making sure that each thread has exclusive access to cref
    // while it is updated.
    {
      std::lock_guard < std::mutex > lk ( reduce_mutex ) ;
      cref += partial_cref ;
    }
  }
} ;

/***************************************************************************/

/// struct multi_interpolator serves as a base class for interpolators
/// combining several source images - currently, cubemap_interpolator
/// and facet_map_interpolator.

struct multi_interpolator
: public interpolator_base
{
  using interpolator_base::mode ;
  const bool fully_covered ;
  const fc_t nfacets ;
  std::atomic < int > stage ;
  std::vector < interpolator * > sub_itp ;
  const std::vector < source_type > & source_v ;
  const std::vector < quaternion_type > & orientation_v ;

  multi_interpolator (
      const std::vector < source_type > & _source_v ,
      const std::vector < quaternion_type > & _orientation_v ,
      bool _fully_covered ,
      bool _process_linear ,
      alpha_mode _mode ,
      bool _grey_edge ,
      int _lq_degree ,
      int _hq_degree ,
      double _scaling_step ,
      int _smoothing_level ,
      int _cbf_degree ,
      int _floor ,
      bool _build_pyramids ,
      bool _build_raw_pyramids ,
      interpolator_base * _p_recycle ,
      interpolator_type _own_type )
  : nfacets ( _source_v.size() ) ,
    sub_itp ( _source_v.size() ) ,
    source_v ( _source_v ) ,
    orientation_v ( _orientation_v ) ,
    fully_covered ( _fully_covered )
  {
    own_type = _own_type ;

    // if _p_recycle refers to a multi_interpolator of the same type,
    // we'll try to recycle the interpolator.

    multi_interpolator * p_recycle = nullptr ;

    if ( _p_recycle )
    {
      if ( own_type == _p_recycle->own_type )
      {
        p_recycle = (multi_interpolator*) _p_recycle ;

        // if there are more sub-interpolators in the multi_interpolator
        // p_recycle points to than this one has facets, we delete them now.

        for ( int i = nfacets ; i < p_recycle->sub_itp.size() ; i++ )
        {
          memlog >> p_recycle->sub_itp[i] ;
          p_recycle->sub_itp[i] = nullptr ;
        }
      }
      else
      {
        // _p_recycle refers to some other type of interpolator. Delete it
        // completely

        memlog >> _p_recycle ;
      }
    }

    // we'll start out with the requested alpha mode. After the first
    // interpolator is built, we'll check it's mode and request this
    // mode for subsequent subimages, to have uniform handling.
    // mode will end up either WITH_ALPHA or NO_ALPHA.

    mode = _mode ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      // to get the interpolator for this cube face, we'll use
      // make_interpolator, which can recycle an existing interpolator
      // if this is technically feasible. With this little trick, we'll
      // even be able to recycle the full set of facet interpolators
      // if that's possible, so 'replaying' a multi_interpolator is fast.
      // TODO: equal images might use a common sub-interpolator

      interpolator * pi = nullptr ;

      if ( p_recycle )
      {
        if ( p_recycle->sub_itp.size() > i )
        {
          pi = p_recycle->sub_itp[i] ;
          p_recycle->sub_itp[i] = nullptr ;
        }
      }

      // multi-facetted views should not use edge greying based on
      // darkening of the RGB values. If alpha processing is active,
      // the grey edges are realized by alpha channel alterations
      // which are okay to use for multi-facetted views.

      if ( mode == NO_ALPHA )
        _grey_edge = false ;

      auto itp = local_dispatch.make_interpolator (
                      source_v[i].filename.c_str() ,
                      source_v[i] ,
                      _process_linear ,
                      mode ,
                      _grey_edge ,
                      _lq_degree ,
                      _hq_degree ,
                      _scaling_step ,
                      _smoothing_level ,
                      _cbf_degree ,
                      _floor ,
                      _build_pyramids ,
                      _build_raw_pyramids ,
                      0 ,
                      pi ) ;

      // the first facet sets the alpha processing mode. We want a common
      // mode for all facets.

      if ( i == 0 )
      {
        mode = itp->mode ;
      }

      sub_itp[i] = (interpolator*) itp ;
    }

    if ( p_recycle )
    {
      // now we can destroy the object p_recycle points to

      memlog >> p_recycle ;
    }

    // TODO: stage depends on sub-itps, really

    stage = 3 ;
  }

  // the d'tor deletes the individual sub-interpolators

  ~multi_interpolator()
  {
    for ( int i = 0 ; i < nfacets ; i++ )
    {
      if ( sub_itp[i] != nullptr )
        memlog >> sub_itp[i] ;
    }
  }

  // roll out shift to all sub-interpolators

  void shift ( int quality , int by )
  {
    for ( int i = 0 ; i < nfacets ; i++ )
    {
      sub_itp[i] -> shift ( quality , by ) ;
    }
  }

  struct run_type
  : public run_type_base
  {
    run_type()
    {
      render = false ;
    } ;

    template < typename face_detect_type >
    run_type ( const job_type * const p_job ,
               const std::vector < ev_type > & evv ,
               const std::vector < alpha_ev_type > & aevv ,
               const face_detect_type & cf )
    {
      const frame_type & frame ( p_job->frame ) ;
      render = true ;

      if ( frame.format == FRAME_FRGB )
      {
        multi_facet_type < face_detect_type >
          mfev ( cf , evv , false ) ;

        ev = vspline::grok ( mfev ) ;
        alpha = false ;
      }
      else
      {
        multi_facet_type_4 < face_detect_type >
          mfev4 ( cf , evv , aevv , false ) ;

        ev4 = vspline::grok ( mfev4 ) ;
        alpha = true ;
      }
    }

    template < typename face_project_type >
    run_type ( const job_type * const p_job ,
               const ev_type & _ev ,
               const alpha_ev_type & aev ,
               face_project_type cf )
    {
      const frame_type & frame ( p_job->frame ) ;
      render = true ;

      if ( frame.format == FRAME_FRGB )
      {
        single_facet_type < face_project_type >
          sfev ( cf , _ev ) ;

        ev = vspline::grok ( sfev ) ;
        alpha = false ;
      }
      else
      {
        single_facet_type_4 < face_project_type >
          sfev4 ( cf , _ev , aev ) ;

        ev4 = vspline::grok ( sfev4 ) ;
        alpha = true ;
      }
    }
  } ;

  // in classes derived from multi_interpolator, we'll have overrides
  // for 'build_runner' - the base class doesn't have a definition.

  virtual run_type_base build_runner ( job_type * p_job ) = 0 ;

} ;

/***************************************************************************/

// this helper function tests with a given facet detector whether
// all target corner points will be projected into the same facet.
// If yes, this common facet is returned - otherwise nfacets.

template < projection_type TPRJ >
bool get_common_facet ( const target_type & target ,
                        get_cube_face < TPRJ > facet_detector ,
                        fc_t & common_facet )
{
  bool fixed_facet = true ;
  coordinate_type crd ;
  fc_t facet ;

  coordinate_type ul ( 0.0f , 0.0f ) ;
  coordinate_type ur ( target.width - 1 , 0.0f ) ;
  coordinate_type lr ( target.width - 1 , target.height - 1 ) ;
  coordinate_type ll ( 0.0f , target.height - 1 ) ;

  facet_detector.eval ( ul , crd , common_facet ) ;
  facet_detector.eval ( ur , crd , facet ) ;
  if ( facet == common_facet )
  {
    facet_detector.eval ( lr , crd , facet ) ;
    if ( facet == common_facet )
    {
      facet_detector.eval ( ll , crd , facet ) ;
      fixed_facet = ( facet == common_facet ) ;
    }
    else
    {
      fixed_facet = false ;
    }
  }
  else
  {
    fixed_facet = false ;
  }

  if ( common_facet == facet_detector.nfacets )
    fixed_facet = false ;

  return fixed_facet ;
}

/// cubemap_interpolator is an interpolator providing a 360X180 degree
/// view from six separate square images. The current implementation
/// can take the full range of useful arguments, but there is a catch:
/// Interpolation is not done 'over the edges', so there are sometimes
/// visible artifacts along the cube's edges and, specifically, vertices.
/// This is due to the fact that at these locations we have discontinuities
/// which would need additional processing to provide a better result:
/// here, interpolation should be done taking into account data from several
/// cube faces, which don't share a common sampling grid. There are ways how
/// we might mend the problem, e.g. : we might have splines with modified
/// 'frames' reflecting the continuation, which would require to 'manually'
/// fill in the frames - either from, e.g., spherical data, or by setting
/// up an interpolation scheme along the seams like RBF interpolation which
/// can process ungridded data.
/// For now, we use REFLECT BCs to push the discontinuity as far out as
/// possible, and use class interpolator for the faces, which gives us
/// the whole functionality of this class including image pyramids etc.
/// The cube edge artifacts are roughly on pixel level, so to notice them
/// it's usually necessary to zoom well in.
/// The best way to avoid the artifacts (or get very close) is to use
/// slightly larger (greater than 90 degrees hfov) cube face images,
/// which is easy to do when using synthetically produced cube faces.

struct cubemap_interpolator
: public multi_interpolator
{
  using multi_interpolator::mode ;
  double cubeface_fov ;
  using typename multi_interpolator::run_type ;

  /// cubemap_interpolator's c'tor takes just about all arguments that
  /// class interpolator's c'tor takes, with the few exceptions which make
  /// no sense for cubemaps, like grey edges.

  cubemap_interpolator ( const std::vector < source_type > & source_v ,
                         bool _process_linear ,
                         alpha_mode _mode ,
                         int _lq_degree ,
                         int _hq_degree ,
                         double _scaling_step ,
                         int _smoothing_level ,
                         int _cbf_degree ,
                         int _floor ,
                         bool _build_pyramids ,
                         bool _build_raw_pyramids ,
                         interpolator_base * _p_recycle )
  : multi_interpolator (
      source_v ,
      std::vector < quaternion_type > ( 0 ) , // implicit for cubemaps
      true , // always fully_covered ,
      _process_linear ,
      _mode ,
      false , // never a grey edge ,
      _lq_degree ,
      _hq_degree ,
      _scaling_step ,
      _smoothing_level ,
      _cbf_degree ,
      _floor ,
      _build_pyramids ,
      _build_raw_pyramids ,
      _p_recycle ,
      CUBEMAP_ITP )
  { }

  // the penultimate stage before perform_transform fixes face detection.
  // We use an optimization: if the complete rendering job will only access
  // data from a single face, we'll use 'fixed face' code, because it's
  // obviously futile to calculate the face for every pixel if it's known
  // beforehand. While this saves time for this (common) case, it is not
  // very granular - one off-face pixel will stop the show and result in
  // the 'free face' variant.
  // First we have the template for unspecified target projection, which
  // does not consider a separate single-facet path and always uses a
  // get_cube_face object of the given target projection.

  template < projection_type TPRJ >
  run_type
  fix_face_detection ( const job_type * const p_job ,
                       const std::vector < ev_type > & evv ,
                       const std::vector < alpha_ev_type > & aevv ) const
  {
    // produce a cube face detector

    get_cube_face < TPRJ > cf ( p_job ) ;

    // create and return a runner for the job, using this detector

    return run_type ( p_job , evv , aevv , cf ) ;
  }

  // specialization for rectlinear target projection. Here we handle
  // the single-facet path separately.

  template<>
  run_type
  fix_face_detection < RECTILINEAR >
        ( const job_type * const p_job ,
          const std::vector < ev_type > & evv ,
          const std::vector < alpha_ev_type > & aevv ) const
  {
    const frame_type & frame ( p_job->frame ) ; // short identifier
    const target_type & target ( p_job->target ) ; // short identifier

    // first in line we have target_to_ray, which converts incoming
    // 2D target coordinates to 3D radian 'rays'

    to_ray_type < RECTILINEAR > ttr ( p_job ) ;

    // get the sensor corners from ttr, call cf's 'eval' to get
    // the faces for them. If all faces are equal, use fixed_face path

    bool fixed_face = true ;
    coordinate_type crd ;
    fc_t face ;
    fc_t common_face ;

    // produce a cube face detector

    get_cube_face < RECTILINEAR > cf ( p_job ) ;

    // use it to evaluate all four corner points of the sensor's
    // representation in 3D space, and test if all these evaluations
    // 'land' in the same cube face

    fixed_face = get_common_facet ( target , cf , common_face ) ;

    // now we can route to fixed-face or variable-face code:

    if ( fixed_face )
    {
      switch ( common_face )
      {
        // fixed_face code for cubemaps uses six discrete types for
        // the six cube faces, which is the optimal way of coding the
        // problem, because it optimizes well.

        #define fixed_face_tf(FACE) \
        { \
        case FACE: \
          get_in_face_coordinates < FACE , RECTILINEAR > cc ( p_job ) ; \
          return run_type \
            ( p_job , evv [ common_face ] , aevv [ common_face ] , cc ) ; \
        break ; \
        }

        fixed_face_tf ( LEFT )
        fixed_face_tf ( RIGHT )
        fixed_face_tf ( FRONT )
        fixed_face_tf ( BACK )
        fixed_face_tf ( TOP )
        fixed_face_tf ( BOTTOM )

        #undef fixed_face_tf

      }
    }
    return run_type ( p_job , evv , aevv , cf ) ;
  }

  // build_runner creates a run_type object from the specs in the
  // job_type object. The 'runner' holds either an RGB or RGBA shader,
  // and this is the object passed to 'shade' to fill the target frame
  // with the result.
  // The shaders in the runner object can also be used to write code
  // which uses a multi_interpolator like a single source image: the
  // shaders simply provide pixel values for target coordinates and
  // hide all the complexities 'underneath' which produce such pixel
  // values from a set of sub-interpolators belonging to the
  // multi_interpolator.

  virtual run_type_base build_runner ( job_type * p_job )
  {
    const frame_type & frame ( p_job->frame ) ;
    const target_type & target ( p_job->target ) ;

    // we'll need a set of six individual evaluators, rather than just
    // a single one. The evaluation code will pick one or several of
    // these evaluators during the execution of the pixel pipeline.

    std::vector < ev_type > evv ( 6 ) ;

    // and we may use six alpha channel functors as well

    std::vector < alpha_ev_type > aevv ( 6 ) ;

    for ( int i = 0 ; i < 6 ; i++ )
    {
      // get a short name for the current face's interpolator

      const auto & p_itp ( sub_itp[i] ) ;

      int pyramid_bias = p_itp->pyramid_bias ;

      coordinate_type in_low ( source_v[i].extent.x0 , source_v[i].extent.y0 ) ;
      coordinate_type in_high ( source_v[i].extent.x1 , source_v[i].extent.y1 ) ;

      // 'mode' is the flag which tells us the type of data which the
      // interpolator's holds: either we have plain RGB data, or aRGBA.
      // This makes for different routes of setting up the set of
      // evaluators and alpha evaluators. The second criterion we test
      // is whether the job asks or associated or unassociated alpha.

      if ( mode == WITH_ALPHA )
      {
        ev4_type ev4 = p_itp->provide_ev4 ( p_job ) ;
        aevv[i] = extract_alpha_type ( ev4 ) ;

        if ( frame.yield_argba )
        {
          evv[i] = omit_alpha_type ( ev4 ) ;
        }
        else
        {
          evv[i] = omit_alpha_type ( ev4 + deassociate_type() ) ;
        }
      }
      else
      {
        ev_type ev3 = p_itp->provide_ev3 ( p_job ) ;
        aevv[i] = yield_opaque() ;

        if ( frame.yield_argba )
        {
          evv[i] = ev3 + amplify_t < pixel_type > ( 255.0f ) ;
        }
        else
        {
          evv[i] = ev3 ;
        }
      }
    }

    // now we route to different code variants depending on the target
    // projection.

    run_type_base runner ;

    switch ( target.projection )
    {
      case RECTILINEAR:
      {
        runner = fix_face_detection < RECTILINEAR > ( p_job , evv , aevv ) ;
        break ;
      }
      case SPHERICAL:
      {
        runner = fix_face_detection < SPHERICAL > ( p_job , evv , aevv ) ;
        break ;
      }
      case CYLINDRIC:
      {
        runner = fix_face_detection < CYLINDRIC > ( p_job , evv , aevv ) ;
        break ;
      }
      case FISHEYE:
      {
        runner = fix_face_detection < FISHEYE > ( p_job , evv , aevv ) ;
        break ;
      }
      case STEREOGRAPHIC:
      {
        runner = fix_face_detection < STEREOGRAPHIC > ( p_job , evv , aevv ) ;
        break ;
      }
      default:
      {
        // this should not happen, but having a default is good style
        abort_lux ( "unhandled target projection" ) ;
      }
    }
    return runner ;
  }

  // process_job obtains a runner object for the job and immediately
  // calls 'shade' to use the shader in the runner object to populate
  // the target frame with pixel data.
  // The code in 'build_runner' used to be in 'process_job', but I've
  // decided to factor it out and have a separate object (the 'runner')
  // to contain the shader and stuff it depends on. The runner object
  // can be used for other purposes as well.

  virtual void process_job ( job_type * p_job )
  {
    auto runner = build_runner ( p_job ) ;
    shade ( runner , p_job ) ;
  }

} ;

// again we have the dispatcher routines to be called from the
// non-rendering thread:

interpolator_base * dispatch::make_cube_itp
       ( const std::vector < source_type > & source_v ,
         bool _process_linear ,
         alpha_mode _mode ,
         int _lq_degree ,
         int _hq_degree ,
         double _scaling_step ,
         int _smoothing_level ,
         int _cbf_degree ,
         int _floor ,
         bool _build_pyramids ,
         bool _build_raw_pyramids ,
         interpolator_base * p_recycle ) const
{
  auto itp = mem_in() << new cubemap_interpolator ( source_v ,
                                        _process_linear ,
                                        _mode ,
                                        _lq_degree ,
                                        _hq_degree ,
                                        _scaling_step ,
                                        _smoothing_level ,
                                        _cbf_degree ,
                                        _floor ,
                                        _build_pyramids ,
                                        _build_raw_pyramids ,
                                        p_recycle ) ;
  return (interpolator_base*) itp ;
}

/***************************************************************************/

// facet_map_interpolator provides the 'standard' interpolator interface
// to a facet map - a set of oriented facets tangent to the unit sphere.
// Pixels on the facets are either 'ranked' by proximity to the origin
// (blending mode 'BLEND_RANKED') or HDR-merged (blending mode 'BLEND_HDR')
// with 'ranked' blending, if processing is done with alpha channel,
// correct alpha blending is done, so (semi-)transparent pixels will let
// other pixels 'behind' them shine through. With 'hdr' blending alpha
// processing can also be done, even though this is rarely desirable:
// for HDR blending, you'll usually have a registered image stack and
// no transparency - unless you're doing deghosting as well.
// facet maps are like a set of source images in a PTO file, which are
// also processed as if they were 'mounted' touching the panosphere.
// facet_map_interpolator can be used to do some processing which is
// similar to a stitcher, but it lacks sophisticated blending with
// feathering and seem optimization - it's strictly 'closest facet wins'.
// The appeal of using this class is the near-instant visibility of the
// result without having to wait for a stitching job to produce it's
// output image. Modifications to the ini file can be made visible
// quickly by 'refreshing' (press F1) which re-reads the ini file.
// This is fast because pv will reuse interpolators if it can, and
// if you merely change stuff like facet orientation, brightnes, or FOV
// or replace one facet image with another, refreshing should be almost
// immediate.

struct correct_vignetting
: public tf23_type
{
  typedef vspline::grok_type < coordinate_type , pixel_type , VSIZE >
            inner_type ;

  const inner_type inner ;
  const float vs, va, vb, vc, vd, vx, vy ;

  correct_vignetting ( const source_type & src ,
                       const inner_type & _inner )
  : inner ( _inner ) ,
    vs ( src.vs ) ,
    va ( src.va ) ,
    vb ( src.vb ) ,
    vc ( src.vc ) ,
    vd ( src.vd ) ,
    vx ( src.vx ) ,
    vy ( src.vy )
  { }

  void echo ( float r2 , float f ) const
  {
    if ( r2 > .99f )
      std::cout << "r2: " << r2 << " f: " << f << std::endl ;
  }

#ifdef VECTORIZE

  void echo ( channel_v r2 , channel_v f ) const
  {
    if ( any_of ( r2 > .99f ) )
      std::cout << "r2: " << r2 << " f: " << f << std::endl ;
  }

#endif

  template < typename in_type , typename out_type >
  void eval ( const in_type & crd , out_type & out ) const
  {
    // apply vignetting center shift vx, vy

    in_type help ( crd ) ;
    help[0] -= vx ;
    help[1] -= vy ;

    // get r2, the squared radius in model space units

    auto r2 = help[0] * help[0] + help[1] * help[1] ;

    // scale r2 to multiples of half diagonal 'vs' - divide by the square
    // of the reference radius 'vs', because r2 is the squared radius

    r2 /= ( vs * vs ) ;

    // calculate vignetting factor

    auto factor = va + r2 * vb + r2 * r2 * vc + r2 * r2 * r2 * vd ;

    // evaluate inner functor, yielding RGB value

    inner.eval ( crd , out ) ;

    // apply vignetting correction: divide by the factor

    out[0] /= factor ;
    out[1] /= factor ;
    out[2] /= factor ;
  }
} ;

struct yield_black : public tf23_type
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out = 0.0f ;
  }
} ;

struct yield_white : public tf23_type
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out = 1.0f ;
  }
} ;

struct yield_white_if : public tf23_type
{
  const alpha_ev_type aev ;

  yield_white_if ( const alpha_ev_type & _aev )
  : aev ( _aev )
  { } ;

  void eval ( const in_type & in , out_type & out ) const
  {
    auto alpha = aev ( in ) ;
    if ( alpha > 0.1f )
      out = 1.0f ;
    else
      out = 0.0f ;
  }

  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    auto alpha = aev ( in ) ;
    auto mask = ( alpha > 0.1f ) ;
    out = 0.0f ;
    out[0] ( mask ) = 1.0f ;
    out[1] ( mask ) = 1.0f ;
    out[2] ( mask ) = 1.0f ;
  }
} ;

struct force_positive : public tf33_type
{
  template < typename in_type , typename out_type >
  void eval ( const in_type & in , out_type & out ) const
  {
    out[0] = max ( 0.0f , in[0] ) ;
    out[1] = max ( 0.0f , in[1] ) ;
    out[2] = max ( 0.0f , in[2] ) ;
  }
} ;

struct facet_map_interpolator
: public multi_interpolator
{
  using multi_interpolator::mode ;

  /// facet_map_interpolator's c'tor takes just about all arguments that
  /// class interpolator's c'tor takes, with the few exceptions which make
  /// no sense for facet_maps, like grey edges.

  facet_map_interpolator
    ( const std::vector < source_type > & source_v ,
      const std::vector < quaternion_type > & _orientation_v ,
      bool _fully_covered ,
      bool _process_linear ,
      alpha_mode _mode ,
      bool _grey_edge ,
      int _lq_degree ,
      int _hq_degree ,
      double _scaling_step ,
      int _smoothing_level ,
      int _cbf_degree ,
      int _floor ,
      bool _build_pyramids ,
      bool _build_raw_pyramids ,
      interpolator_base * _p_recycle )
  : multi_interpolator (
      source_v ,
      _orientation_v ,
      _fully_covered ,
      _process_linear ,
      _mode ,
      _grey_edge ,
      _lq_degree ,
      _hq_degree ,
      _scaling_step ,
      _smoothing_level ,
      _cbf_degree ,
      _floor ,
      _build_pyramids ,
      _build_raw_pyramids ,
      _p_recycle ,
      FACET_MAP_ITP )
  { }

  // we inherit from multi_interpolator's run_type, adding more
  // constructors with arguments specific to facet maps.

  struct run_type
  : public multi_interpolator::run_type
  {
    // we want to use the base class' c'tors as well

    using multi_interpolator::run_type::run_type ;

    template < projection_type TPRJ >
    run_type ( const job_type * const p_job ,
               const std::vector < ev_type > & evv ,
               const std::vector < alpha_ev_type > & aevv ,
               const get_facet_from_crd < TPRJ > & cf )
    {
      const frame_type & frame ( p_job->frame ) ; // short identifier
      const target_type & target ( p_job->target ) ; // short identifier
      render = true ;
      
      if ( frame.format == FRAME_FRGB )
      {
        alpha = false ;
        if ( frame.blending == BLEND_CORRELATE )
        {
          render = false ;
          int nfacets = cf.nfacets ; // evv.size() - 1 ;

          // correlation with less tha two facets is futile

          if ( nfacets < 2 )
            return ;

          vigra::MultiArray < 2 , double >
            cref ( vigra::Shape2 ( nfacets , nfacets ) ) ;
          brightness_reductor bred ( cref , evv ) ;

          correlate_type < TPRJ , brightness_reductor >
            corr ( cf , bred ) ;

          vspline::reduce ( corr ,
                            vigra::Shape2 ( target.width ,
                                            target.height ) ) ;

          assert ( p_job->dock.p_correlate != nullptr ) ;

          vigra::MultiArray < 2 , double > & cref_target
            ( * ( p_job->dock.p_correlate ) ) ;

          for ( int i = 0 ; i < nfacets ; i++ )
          {
            auto a = corr.translate_facet_number ( i ) ;
            for ( int j = 0 ; j < nfacets ; j++ )
            {
              auto b = corr.translate_facet_number ( j ) ;
              // layer the results of this run onto what's in cref_target;
              // the caller may call several times to cover all facets.
              cref_target [ { a , b } ] += cref [ { i , j } ] ;
            }
          }
        }
        else if ( frame.blending == BLEND_HDR )
        {
          multi_layer_type < TPRJ >
            mfev ( cf , evv ) ;
          ev = vspline::grok ( mfev ) ;
        }
        else if ( frame.blending == BLEND_QUORATE )
        {
          multi_exposure_type < TPRJ >
            mfev ( cf , evv ) ;
          ev = vspline::grok ( mfev ) ;
        }
        else if ( frame.feathering > 0 )
        {
          auto threshold = frame.feathering ;

          feathered_facet_type < TPRJ >
            mfev ( cf , evv , threshold ) ;
          ev = vspline::grok ( mfev ) ;
        }
        else
        {
          multi_interpolator::run_type runner ( p_job , evv , aevv , cf ) ;
          multi_interpolator::run_type & self ( *this ) ;
          self = runner ;
        }
      }
      else
      {
        alpha = true ;
        if ( frame.blending == BLEND_CORRELATE )
        {
          render = false ;
          int nfacets = cf.nfacets ;

          // correlation with less than two facets is futile

          if ( nfacets < 2 )
            return ;

          vigra::MultiArray < 2 , double >
            cref ( vigra::Shape2 ( nfacets , nfacets ) ) ;
          brightness_reductor bred ( cref , evv ) ;

          correlate4_type < TPRJ , brightness_reductor >
            corr ( cf , bred , aevv ) ;

          vspline::reduce ( corr ,
                            vigra::Shape2 ( target.width ,
                                            target.height ) ) ;

          assert ( p_job->dock.p_correlate != nullptr ) ;

          vigra::MultiArray < 2 , double > & cref_target
            ( * ( p_job->dock.p_correlate ) ) ;

          for ( int i = 0 ; i < nfacets ; i++ )
          {
            auto a = corr.translate_facet_number ( i ) ;
            for ( int j = 0 ; j < nfacets ; j++ )
            {
              auto b = corr.translate_facet_number ( j ) ;
              // layer the results of this run onto what's in cref_target;
              // the caller may call several times to cover all facets.
              cref_target [ { a , b } ] += cref [ { i , j } ] ;
            }
          }
        }
        else if ( frame.blending == BLEND_HDR )
        {
          multi_layer_type_4 < TPRJ >
            mfev4 ( cf , evv , aevv ) ;
          ev4 = vspline::grok ( mfev4 ) ;
        }
        else if ( frame.blending == BLEND_QUORATE )
        {
          multi_exposure_type_4 < TPRJ >
            mfev4 ( cf , evv , aevv ) ;
          ev4 = vspline::grok ( mfev4 ) ;
        }
        else if ( frame.feathering > 0 )
        {
          auto threshold = frame.feathering ;

          feathered_facet_type_4 < TPRJ >
            mfev4 ( cf , evv , aevv , threshold ) ;
          ev4 = vspline::grok ( mfev4 ) ;
        }
        else
        {
          multi_interpolator::run_type runner ( p_job , evv , aevv , cf ) ;
          multi_interpolator::run_type & self ( *this ) ;
          self = runner ;
        }
      }
    }

  } ;

  // for arbitrarily facetted but fully covered cases,
  // we will use this function.

  template < projection_type TPRJ >
  run_type fix_facet_detection
       ( const job_type * const p_job ,
         const multi_facet_helper_type < TPRJ > & mfh ) const
  {
    // set up a facet detector

    get_facet_covered < TPRJ > cf ( mfh , mfh.nfacets , p_job ) ;

    // If there is only one in-play facet, we can use fixed-facet
    // code, which is faster.

    fc_t common_facet ;
    bool fixed_facet = ( mfh.nfacets == 1 ) ;

    // now we can route to fixed-facet or variable-facet code:

    if ( fixed_facet && ( ! cf.alpha_flag ) )
    {
      // we got lucky. set up a facet detector for fixed-facet
      // operation, using the same evaluator for all evaluations

      get_facet_covered < TPRJ > cf ( mfh , 0 , p_job ) ;

      return run_type
        ( p_job , mfh.evv [ 0 ] , mfh.aevv [ 0 ] , cf ) ;
    }
    else
    {
      // no luck - the view spans several facets

      return run_type ( p_job , mfh.evv , mfh.aevv , cf ) ;
    }
  }

  // for arbitrarily facetted but possibly only partially covered cases,
  // we will use this function. (_pt is for 'partial')

  template < projection_type TPRJ >
  run_type fix_facet_detection_pt
       ( const job_type * const p_job ,
         const multi_facet_helper_type < TPRJ > & mfh ) const
  {
    get_facet_from_crd < TPRJ > cf ( mfh , p_job ) ;
    return run_type ( p_job , mfh.evv , mfh.aevv , cf ) ;
  }

  // process_job establishes interpolation for the job.
  // This code is quite similar to the code in class interpolator, which
  // suggests that there may be components which might be factored out.
  // Again we use a 'fat' object to insert a complex part of the pixel
  // pipeline by having pipeline building 'pass through it'. This makes
  // for nicely structured code, preferable to passing around lots of
  // arguments to free functions.

  unsigned char saturated_convert ( const float & v )
  {
    if ( v <= 0.0f )
      return 0u ;
    else if ( v >= 255.0f )
      return 255u ;
    else
      return (unsigned char) ( v ) ;
  }

  alpha_ev_type max_alpha_of ( std::vector < alpha_ev_type > & stack_aev )
  {
    assert ( stack_aev.size() > 0 ) ;
    alpha_ev_type head = stack_aev[0] ;
    for ( int tail = 1 ; tail < stack_aev.size() ; tail++ )
      head = max_alpha_type ( head , stack_aev[tail] ) ;
    return head ;
  }

  alpha_ev_type min_alpha_of ( std::vector < alpha_ev_type > & stack_aev )
  {
    assert ( stack_aev.size() > 0 ) ;
    alpha_ev_type head = stack_aev[0] ;
    for ( int tail = 1 ; tail < stack_aev.size() ; tail++ )
      head = min_alpha_type ( head , stack_aev[tail] ) ;
    return head ;
  }

  // for now, just a stub, because the base class' method is pure virtual

  virtual run_type_base build_runner ( job_type * p_job )
  {
    frame_type & frame ( p_job->frame ) ;
    const target_type & target ( p_job->target ) ;

    blending_settings_type & bls ( frame.blending_settings ) ;
    
    // we'll need a set of nfacets+1 individual interpolators

    std::vector < ev_type > evv ( nfacets + 1 ) ;

    // and we may use nfacets+1 alpha channel functors as well

    std::vector < alpha_ev_type > aevv ( nfacets + 1 ) ;

    // if a mask for a stack is needed, a facet number of a facet
    // in this stack is passed in frame.mask_stack. Here we figure
    // out this facet's 'stack parent', which may or may not be
    // the same.

    int mask_stack_parent = -1 ;

    if ( frame.mask_stack >= 0 )
    {
      const auto & p_itp ( sub_itp [ frame.mask_stack ] ) ;
      mask_stack_parent = p_itp->source.stack_parent ;
    }

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      // get a short name for the current face's interpolator

      const auto & p_itp ( sub_itp[i] ) ;

      int pyramid_bias = p_itp->pyramid_bias ;

      // we'll need the sub-interpolator's source extent to for the domain
      // to range-map radian coordinates to spline coordinates

      coordinate_type in_low { float ( p_itp->source.extent.x0 ) ,
                               float ( p_itp->source.extent.y0 ) } ;

      coordinate_type in_high { float ( p_itp->source.extent.x1 ) ,
                                float ( p_itp->source.extent.y1 ) } ;

      // 'mode' is the flag which tells us the type of data which the
      // interpolator's holds: either we have plain RGB data, or aRGBA.
      // This makes for different routes of setting up the set of
      // evaluators and alpha evaluators. The second criterion we test
      // is whether the job asks or associated or unassociated alpha.
      // In any case, we provide both an RGB evaluator *and* an alpha
      // evaluator: if the interpolator itself only holds RGB data, the
      // alpha evaluator simply yields 'fully opaque' (== 255.0f).
      // Note the amplification of the RGB value if the interpolator
      // provides RGB only, but the job requires associated alpha.

      if ( frame.mask_for == -1 )
      {
        // this is not a masking job. The evaluators will yield
        // image data, whereas a masking job (see below) yields
        // values in [0:1]

        if ( mode == WITH_ALPHA )
        {
          ev4_type ev4 = p_itp->provide_ev4 ( p_job ) ;
          aevv[i] = extract_alpha_type ( ev4 ) ;

          if ( frame.yield_argba )
          {
            evv[i] = omit_alpha_type ( ev4 ) ;
          }
          else
          {
            evv[i] = omit_alpha_type ( ev4 + deassociate_type() ) ;
          }
        }
        else
        {
          ev_type ev3 = p_itp->provide_ev3 ( p_job ) ;
          aevv[i] = yield_opaque() ;

          if ( frame.yield_argba )
          {
            evv[i] = ev3 + amplify_t < pixel_type > ( 255.0f ) ;
          }
          else
          {
            evv[i] = ev3 ;
          }
        }

        if ( p_itp->source.vignetting_correction_active )
        {
          auto cv = correct_vignetting ( p_itp->source , evv[i] ) ;
          evv[i] = vspline::grok ( cv ) ;
        }

      }
      else
      {
        // this is a masking job. If mask_for specifies that this
        // facet should be masked give it a fully set (all 1.0) ev.

        bool add_this_one = (    frame.mask_for >= 0
                              && i == frame.mask_for ) ;

        // and if mask_stack is set, we also use an all 1.0 ev
        // for all other facets in the stack, which yields a mask
        // for the entire stack - the union of the facet members'
        // coverage.

        if ( frame.mask_stack > -1 )
        {
          int parent = p_itp->source.stack_parent ;
          add_this_one |= ( parent == mask_stack_parent ) ;
        }

        evv[i] = add_this_one
                ? vspline::grok ( yield_white() )
                : vspline::grok ( yield_black() ) ;

        if ( mode == WITH_ALPHA )
        {
          auto ev4 = p_itp->provide_ev4 ( p_job ) ;
          aevv[i] = extract_alpha_type ( ev4 ) ;
        }
        else
        {
          aevv[i] = yield_opaque() ;
        }
      }
    }

    // now we route to different code variants depending on the target
    // projection.

    run_type_base runner ;

    switch ( target.projection )
    {
      case RECTILINEAR:
      {
        multi_facet_helper_type < RECTILINEAR > mfh ( p_job ,
                                      source_v ,
                                      orientation_v ,
                                      evv ,
                                      aevv ,
                                      true ,
                                      frame.use_rank ) ;

        if ( fully_covered )
          runner = fix_facet_detection ( p_job , mfh ) ;
        else
          runner = fix_facet_detection_pt ( p_job , mfh ) ;

        break ;
      }
      case SPHERICAL:
      {
        multi_facet_helper_type < SPHERICAL > mfh ( p_job ,
                                      source_v ,
                                      orientation_v ,
                                      evv ,
                                      aevv ,
                                      true ,
                                      frame.use_rank ) ;

        if ( fully_covered )
          runner = fix_facet_detection ( p_job , mfh ) ;
        else
          runner = fix_facet_detection_pt ( p_job , mfh ) ;

        break ;
      }
      case CYLINDRIC:
      {
        multi_facet_helper_type < CYLINDRIC > mfh ( p_job ,
                                      source_v ,
                                      orientation_v ,
                                      evv ,
                                      aevv ,
                                      true ,
                                      frame.use_rank ) ;

        if ( fully_covered )
          runner = fix_facet_detection ( p_job , mfh ) ;
        else
          runner = fix_facet_detection_pt ( p_job , mfh ) ;

        break ;
      }
      case FISHEYE:
      {
        multi_facet_helper_type < FISHEYE > mfh ( p_job ,
                                      source_v ,
                                      orientation_v ,
                                      evv ,
                                      aevv ,
                                      true ,
                                      frame.use_rank ) ;

        if ( fully_covered )
          runner = fix_facet_detection ( p_job , mfh ) ;
        else
          runner = fix_facet_detection_pt ( p_job , mfh ) ;

        break ;
      }
      case STEREOGRAPHIC:
      {
        multi_facet_helper_type < STEREOGRAPHIC > mfh ( p_job ,
                                      source_v ,
                                      orientation_v ,
                                      evv ,
                                      aevv ,
                                      true ,
                                      frame.use_rank ) ;

        if ( fully_covered )
          runner = fix_facet_detection ( p_job , mfh ) ;
        else
          runner = fix_facet_detection_pt ( p_job , mfh ) ;

        break ;
      }
      case MOSAIC:
      {
        multi_facet_helper_type < MOSAIC > mfh ( p_job ,
                                      source_v ,
                                      orientation_v ,
                                      evv ,
                                      aevv ,
                                      true ,
                                      frame.use_rank ) ;

        if ( fully_covered )
          runner = fix_facet_detection ( p_job , mfh ) ;
        else
          runner = fix_facet_detection_pt ( p_job , mfh ) ;

        break ;
      }
      default:
      {
        // this should not happen, but having a default is good style
        std::string error ( "unknown target projection: " ) ;
        error += std::to_string ( target.projection ) ;
        abort_lux ( error ) ;
        break ;
      }
    }

    return runner ;
  }

  virtual void process_job ( job_type * p_job )
  {
    auto runner = build_runner ( p_job ) ;
    shade ( runner , p_job ) ;
  }

  virtual std::vector < int > active_facets ( job_type * p_job )
  {

    // we'll need a set of nfacets+1 individual interpolators to pass
    // to the multi_facet_helper_type's c'tor, but the content is
    // irrelevant, we needn't initialize them

    std::vector < ev_type > evv ( nfacets + 1 ) ;
    std::vector < alpha_ev_type > aevv ( nfacets + 1 ) ;

    switch ( p_job->target.projection )
    {
      case RECTILINEAR:
      {
        multi_facet_helper_type < RECTILINEAR > mfh ( p_job ,
                                      source_v ,
                                      orientation_v ,
                                      evv ,
                                      aevv ) ;
        return mfh.facet_number ;
        break ;
      }
      case SPHERICAL:
      {
        multi_facet_helper_type < SPHERICAL > mfh ( p_job ,
                                      source_v ,
                                      orientation_v ,
                                      evv ,
                                      aevv ) ;
        return mfh.facet_number ;
        break ;
      }
      case CYLINDRIC:
      {
        multi_facet_helper_type < CYLINDRIC > mfh ( p_job ,
                                      source_v ,
                                      orientation_v ,
                                      evv ,
                                      aevv ) ;
        return mfh.facet_number ;
        break ;
      }
      case FISHEYE:
      {
        multi_facet_helper_type < FISHEYE > mfh ( p_job ,
                                      source_v ,
                                      orientation_v ,
                                      evv ,
                                      aevv ) ;
        return mfh.facet_number ;
        break ;
      }
      case STEREOGRAPHIC:
      {
        multi_facet_helper_type < STEREOGRAPHIC > mfh ( p_job ,
                                      source_v ,
                                      orientation_v ,
                                      evv ,
                                      aevv ) ;
        return mfh.facet_number ;
        break ;
      }
      case MOSAIC:
      {
        multi_facet_helper_type < MOSAIC > mfh ( p_job ,
                                      source_v ,
                                      orientation_v ,
                                      evv ,
                                      aevv ) ;
        return mfh.facet_number ;
        break ;
      }
      default:
      {
        // this should not happen, but having a default is good style
        std::string error ( "unknown target projection: " ) ;
        error += std::to_string ( p_job->target.projection ) ;
        abort_lux ( error ) ;
        return std::vector<int>() ; // just to avoid warnings
        break ;
      }
    }
  }

  virtual int in_stack_facets ( int facet )
  {
    assert ( facet >= 0 ) ;
    assert ( facet < nfacets ) ;

    int stack_parent = source_v [ facet ] . stack_parent ;
    int n = 0 ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      if ( source_v[i].stack_parent == stack_parent )
        ++n ;
    }

    return n ;
  }

} ;

// the next two functions will be called from the non-rendering code
// via the dispatch mechanism

interpolator_base * dispatch::make_facet_itp
       ( const std::vector < source_type > & source_v ,
         const std::vector < quaternion_type > & _facet_orientation ,
         bool _fully_covered ,
         bool _process_linear ,
         alpha_mode _mode ,
         bool _grey_edge ,
         int _lq_degree ,
         int _hq_degree ,
         double _scaling_step ,
         int _smoothing_level ,
         int _cbf_degree ,
         int _floor ,
         bool _build_pyramids ,
         bool _build_raw_pyramids ,
         interpolator_base * p_recycle ) const
{
  auto itp = mem_in() << new facet_map_interpolator ( source_v ,
                                          _facet_orientation ,
                                          _fully_covered ,
                                          _process_linear ,
                                          _mode ,
                                          _grey_edge ,
                                          _lq_degree ,
                                          _hq_degree ,
                                          _scaling_step ,
                                          _smoothing_level ,
                                          _cbf_degree ,
                                          _floor ,
                                          _build_pyramids ,
                                          _build_raw_pyramids ,
                                          p_recycle ) ;
  return (interpolator_base*) itp ;
}

// void dispatch::destroy_facet_itp ( interpolator_base * _p_itp ) const
// {
//   facet_map_interpolator * p_itp = (facet_map_interpolator*) _p_itp ;
//   memlog >> p_itp ;
// }

void dispatch::destroy_itp ( interpolator_base * _p_itp ) const
{
  auto type = _p_itp->own_type ;

  switch ( type )
  {
    case SINGLE_ITP:
    {
      interpolator * p_itp = (interpolator*) _p_itp ;
      memlog >> p_itp ;
      break ;
    }
    case CUBEMAP_ITP:
    {
      cubemap_interpolator * p_itp = (cubemap_interpolator*) _p_itp ;
      memlog >> p_itp ;
      break ;
    }
    case FACET_MAP_ITP:
    {
      facet_map_interpolator * p_itp = (facet_map_interpolator*) _p_itp ;
      memlog >> p_itp ;
      break ;
    }
    default:
    {
      delete _p_itp ;
      break ;
    }
  }
}

// launch runs a payload function in a thread from the thread pool,
// unless VSPLINE_SINGLETHREAD is defined, when it's run in the
// current thread's context. The payload is wrapped in a try/catch
// construct to catch std::runtime_error and translate it into
// a set error_flag plus a message in fatal_error, which is detected
// in the main thread and results in program termination with the
// text passed in the exception.

void dispatch::launch ( std::function < void() > payload ) const
{
  auto secure_payload = [=]()
  {
    try
    {
      payload() ;
    }
    catch ( const std::runtime_error & e )
    {
      fatal_error = e.what() ;
      error_flag = true ;
    }
  } ;

#ifdef VSPLINE_SINGLETHREAD
  secure_payload() ;
#else
  vspline_threadpool::common_thread_pool.launch ( secure_payload ) ;
#endif
}

/// process_job is the rendering thread's subroutine which actually
/// calculates a frame to display. The desired orientation, hfov etc.
/// are passed by the job_type p_job points to. The frame is made and attached
/// to the job, which is then queued to be eventually fetched by the main
/// thread and sent to SFML to display. Decoupling rendering and display into
/// separate threads is necessary: the frames take (at times wildly) varying
/// time to render, and only by carrying on rendering and producing some frames
/// in advance, we end up with enough 'room to manoevre' to display at a steady
/// rate. This way we have some latency, but that can't be helped: the only way
/// to avoid having to drop frames would be to make sure that every single frame
/// is generated in less time than the difference between the display of two
/// successive frames.
/// A single outlier taking longer could result in a dropped frame, even though
/// openGL's buffering may be enough to mask the lapse. By introducing another
/// level of buffering, we can make the engine run more smoothly, and we can
/// even influence the queue size to accept latency for reliable in-time frame
/// availability if we need to, rather than having to depend on openGL's fixed
/// buffering. This way, and especially with the added management of frame
/// creation time via 'global scaling' and 'configurable budgeting' which are
/// employed with auto_quality and auto_budget mode we get a very stable
/// rendering engine with only few dropped frames, which occur mostly when
/// load (either in-process or by other processes on the system) suddenly
/// rises and produce a bit of stutter until the system adapts to the new
/// situation.

void process_job ( job_type * p_job )
{
  auto start = pv_clock::now();

  frame_type & frame ( p_job->frame ) ;          // short identifier
  dock_type & dock ( p_job->dock ) ;             // short identifier
  const target_type & target ( p_job->target ) ; // short identifier

//   auto dt = std::chrono::duration_cast < std::chrono::microseconds >
//      ( p_job->created - program_start ) . count() ;
//   
//   std::cout << "process_job: hq: " << frame.hq
//             << " created: " << dt << std::endl ;

  // try and fetch an rgba frame from the recycling queue to reuse it
  // unless p_frame is already set - if so, use that frame

  while ( true )
  {
    {
      // under ::rgb_mutex, test if there are frames in the queue
      std::lock_guard<std::mutex> lk ( ::rgb_mutex ) ;
      if ( ::rgb_queue.size() )
      {
        // there is at least one frame in the queue, acquire it
        frame.p_frame = ::rgb_queue.front() ;
        ::rgb_queue.pop() ;
        // leave the loop
        break ;
      }
    }
    // there was no rgb frame in the queue - snooze and try again
    // this happens if we provide frames faster than real-time and they
    // pile up in the frame queue, while the main thread only displays
    // them one per display cycle.

    std::this_thread::sleep_for(std::chrono::microseconds(1000));
  }

  // we take note of this point in time, because later on we want to calculate
  // the rendering time

  auto have_frame = pv_clock::now();

  // BLEND_CORRELATE signals an analytic job; it's passed to the
  // interpolator straight away and not passed back via the frame queue.

  if ( frame.blending == BLEND_CORRELATE )
  {
    frame.p_itp->process_job ( p_job ) ;
    return ;
  }

  // we check here if the RGBA frame's size coincides with what the job requires.
  // if the frame isn't the right size, we discard it and allocate a new one
  // with the job's target size in it's stead. This is a safeguard against resizing
  // of the display or the application of global scaling.
  // TODO it might be more efficient to keep frames which are larger than needed
  // and render to a subarray instead of discarding and reallocating always

  assert ( frame.p_frame != 0 ) ;

  if (    frame.p_frame->shape(0) != target.width
       || frame.p_frame->shape(1) != target.height )
  {
    memlog >> frame.p_frame ;
    frame.p_frame = mem_in() << new rgba_image_type
      ( vigra::Shape2 ( target.width , target.height ) ) ;
  }

  blending_settings_type & bls ( frame.blending_settings ) ;

  // some blending modes are handled with a background job. note that
  // such modes are only set for 'single mode' operation, other jobs
  // will never have these blending modes set, even if they form synoptic
  // views, because they are not blended with the special code in
  // pv_combine.cc using the adapted Burt and Adelson algorithm.

  if (    (   bls.mode == STITCH_IMAGES
           || bls.mode == EXPOSURE_FUSION
           || bls.mode == FAUX_BRACKET )
       && ( frame.solo < 0 ) )
  {
    if ( frame.p_itp->mode == NO_ALPHA )
      frame.format = FRAME_FRGB ;
    else
      frame.format = FRAME_FRGBA ;

    // the rendering will be done in linear RGB and converted after,
    // for most accurate results and wider dynamic range

    frame.light_settings.apply_gradation = false ;
    frame.light_settings.cap_brightness = false ;

    if ( bls.mode == STITCH_IMAGES )
    {
      auto payload = [=]()
      {
        local_dispatch.process_stitching_job ( p_job ) ;
        job_ends() ;
      } ;
      local_dispatch.launch ( payload ) ;
    }
    else if (    bls.mode == EXPOSURE_FUSION
              || bls.mode == FAUX_BRACKET )
    {
      auto payload = [=]()
      {
        local_dispatch.process_fusion_job ( p_job ) ;
        job_ends() ;
      } ;
      local_dispatch.launch ( payload ) ;
    }
    // the job has been dispatched to the stitching/fusion code, and
    // the result will be routed back to the main thread once it's
    // ready
  }
  else
  {
    // 'normal' frames are instead directly passed to their interpolator,
    // which processes them in the rendering frame's context, and not as
    // background jobs. The expectation is that the rendering of 'normal'
    // jobs will be reasonably fast, approaching frame rates sufficient
    // for smooth animation if the rendering job at hand is not too
    // demanding.

    frame.p_itp->process_job ( p_job ) ;

    // at this point, the fame has been rendered, and we return after
    // a bit of bookkeeping. The job holds the rendered frame via a
    // pointer (p_frame), but we put the whole job into the queue,
    // so that the main thread can look at it for statistics, and also
    // to duplicate it if necessary.

    auto end = pv_clock::now() ;
    p_job->ready = end ;

    frame.cost
      = float ( std::chrono::duration_cast<std::chrono::microseconds>
                    ( end - have_frame ) . count() ) ;

    push_frame ( p_job ) ;
    job_ends() ;
  }
}

/// generate_main is the top-level function of the frame-generating thread.
/// It handles the interaction with the job queue, fetching jobs as they come
/// in and processing them by calls to process_job().

void dispatch::generate_main() const
{
  while ( true )
  {
    ::job_mutex.lock() ;
    if ( ::stay_alive == false )
    {
      int busy_jobs = jobs_pending ;
      ::job_mutex.unlock() ;

      while ( busy_jobs )
      {
        // if there are running background jobs, we wait on
        // rendering_cv - background jobs notify on this cv
        // when they terminate (see 'job-ends()'). Spurious
        // wakes aren't a problem, the wait-on-cv is just
        // to avoid idling with short sleeps (as done
        // previously) - this mode should use less CPU.

        std::unique_lock < std::mutex > lk ( rendering_mutex ) ;
        rendering_cv.wait ( lk  ) ;
        busy_jobs = jobs_busy() ;
      }

      // when all background jobs are done, we can break,
      // and the rendering thread will terminate and be ready
      // to be joined.

      break ;
    }

    if ( ::job_queue.empty() )
    {
      ::job_mutex.unlock() ;

      // when the job queue is empty, we also wait on
      // rendering_cv: the main thread notifies on this cv
      // when it pushes a job, and also when the rendering
      // thread is to be joined. This wait also replaces a
      // scheme using short sleeps, and here the gain should
      // be much larger, because the viewer is probably idle
      // most of the time.

      std::unique_lock < std::mutex > lk ( rendering_mutex ) ;
      rendering_cv.wait ( lk  ) ;
    }
    else
    {
      ++jobs_pending ;
      job_type * p_job = ::job_queue.front() ;
      ::job_queue.pop() ;
      ::job_mutex.unlock() ;

      if ( p_job->njobs == 0 )
        p_job->njobs = vspline::default_njobs ;

      try
      {
        process_job ( p_job ) ;
      }
      catch ( const std::runtime_error & e )
      {
        // the code on the rendering side will throw a runtime error
        // if things go awry. To communicate this to the main thread,
        // we put the message into fatal_error and set error_flag.

        fatal_error = e.what() ;
        error_flag = true ;
        break ;
      }
      catch ( ... )
      {
        // this should not really happen:

        fatal_error = "oops.. caught an unexpected exception" ;
        error_flag = true ;
        break ;
      }
    }
  }
}

} ; // namespace PV_ARCH

// // the definition of the static member 'archname' in the flavour-specific
// // dispatch class can only occur once in the program - it must be omitted
// // in other TUs of the same flavour.
// // By initializing this static member with the result of calling
// // register_arch, we get the self-registration of the flavour as a side
// // effect, occuring at program startup, so that the isa map is filled
// // when the program proper receives control.
// 
// bool PV_ARCH::dispatch::_hook
//   ( dispatch::register_flavour ( { PV_ARCHNAME ,
//                                    PV_PRIORITY ,
//                                    PV_VIABILITY ,
//                                    (dispatch*) ( &PV_ARCH::_dispatch ) }
//                                )
//   ) ;
