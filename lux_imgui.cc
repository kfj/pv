/************************************************************************/
/*                                                                      */
/*          pv - a viewer for panoramic images                          */
/*                                                                      */
/*          Copyright 2016 - 2023 by Kay F. Jahnke                      */
/*                                                                      */
/*  The git repository for this software is at                          */
/*                                                                      */
/*  https://bitbucket.org/kfj/pv                                        */
/*                                                                      */
/*  Please direct questions, bug reports, and contributions to          */
/*                                                                      */
/*  kfjahnke+pv@gmail.com                                               */
/*                                                                      */
/*  pv is free software: you can redistribute it and/or modify          */
/*  it under the terms of the GNU General Public License as published   */
/*  by the Free Software Foundation, either version 3 of the License,   */
/*  or (at your option) any later version.                              */
/*                                                                      */
/*  pv is distributed in the hope that it will be useful,               */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       */
/*  GNU General Public License for more details.                        */
/*                                                                      */
/*  You should have received a copy of the GNU General Public License   */
/*  along with pv.  If not, see <http://www.gnu.org/licenses/>.         */
/*                                                                      */
/************************************************************************/

/*! \file lux_imgui.cc
 * 
 *  \brief code for the new lux GUI made with Dear ImGui
 * 
 * This code started out as a stripped-down copy of imgui_demo.cc,
 * some of the code is taken straight from that file.
 * 
 * Parameterization in lux is a big issue. The first stages of
 * parameterization occur during program startup: lux reads the
 * user's .lux.ini file containing user-specific arguments (if
 * present), and then processes command line arguments. What's
 * gleaned from these two steps constitutes the 'initial arguments'
 * which are kept in ini::state, a.k.a. ini::args. This is an object
 * holding the arguments in processed form - i.e. integer arguments
 * are held as integers, not as a string representing the number.
 * These initial values persist throughout the session, but they
 * may be modified subsequently by the user, so that the session
 * will carry on with the modified parameter set. I'll refer to
 * these values as 'session-wide' values. lux' 'legacy GUI' did
 * not provide a way to modify session-wide settings; this is
 * a new feature which is being introduced with the ImGui-based
 * GUI.
 * While users interact with an image (or a synoptic view),
 * they may want to modify specific settings just for the current
 * image/synopsis. This can be affected by 'override arguments',
 * which modify ini::args. The session-wide values are kept safe
 * from such modifications, and on proceeding to the next
 * image/synopsis, they are used to reset ini::args to the
 * session-wide state. Override arguments could be issued in the
 * legacy GUI in 'written form' just as they might have been passed
 * on the command line, and their scope was limited to the current
 * image/synopsis. The new GUI will offer ways of affecting such
 * overrides alongside ways to affect session-wide changes; how
 * this will be done precisely is not yet clear, but I tend towards
 * a scheme which offers access to sets of arguments and sports two
 * different 'apply' buttons, one for overrides and one for session-
 * wide settings.
 * Similar to override arguments is parameterization found in .lux
 * files or gleaned from PTO files. Arguments from these sources are
 * equivalent to override arguments, because they affect only the
 * current image/synopsis; on proceeding to the next one, the
 * session-wide arguments will be reloaded.
 * Once parameterization is complete, lux starts into a display
 * cycle. During this cycle, the set of arguments remains unchanged.
 * At the beginning of the cycle, most of the arguments are read
 * and processed into variables in the ui namespace. The processing
 * is quite involved and makes sure that the variables in ui hold
 * values which are valid and usable. variables in namespace ui
 * are open to modification by the user interface. There isn't
 * a 1:1 mapping from arguments to ui variables, but often the
 * ui value is just a copy of an argument. There are other ui
 * variables which reflect the state of the 'viewer engine', like
 * the orientation of the virtual camera. Such ui variables only
 * take their initial state from the arguments, often in an
 * indirect fashion. ui variables are meant to be modified by the
 * user, and modifying them results in an immediate effect. while
 * the user handles a specific image/synopsis, ui variables will
 * be conserved as best as possible: changing parameterization is
 * taken into account, but current settings like the orientation
 * of the virtual camera are conserved if feasible - some changes
 * of parameterization necessarily invalidate ui variables.
 * So interaction with ui variables has immediate effects, overrides
 * start a new display cycle with the same image/synopsis and preserve
 * other ui variables which aren't affected by the override, and
 * session-wide changes act like overrides, but also persist for the
 * remainder of the session (unless chaged again). There is currently
 * no way to modify user-specific arguments with the GUI - this needs
 * to be done by editing .lux.ini.
 * When users use the GUI to modify parameterization, they manipulate
 * 'panels'. These panels offer application of the parameters they
 * show as overrides (so, for the current image/synopsis) or as
 * session-wide settings. In both cases, the application results in
 * a 'show-again-sequence': the current display cycle is ended, and
 * a new one is started with the changed parameters, but the same
 * image/synopsis. Other GUI elements only modify the state
 * of the viewer engine - so they manipulate ui variables. Such
 * manipulations do not persist to a new image/synopsis: when a
 * new image/synopsis is loaded, it starts out with the current
 * set of session-wide parameters and additional parameterization
 * gleaned from the source, like image metadata or parameterization
 * given in a .lux file.
 */

// three lines taken from imgui_demo.cc:

#if defined(_MSC_VER) && !defined(_CRT_SECURE_NO_WARNINGS)
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "imgui/imgui.h"
#include "imgui/imgui_internal.h" // to access GImGui
// #define HAVE_MD_ICONS
#ifdef HAVE_MD_ICONS
// available from: https://github.com/juliettef/IconFontCppHeaders
#include "IconsMaterialDesign.h"
#endif

#include "pv_common.h"
#include "pv_initialize.h"

extern ini::state_type arguments ;

// function taken from imgui_demo.cc

static void HelpMarker(const char* desc)
{
    ImGui::TextDisabled("(?)");
    if (ImGui::BeginItemTooltip())
    {
        ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
        ImGui::TextUnformatted(desc);
        ImGui::PopTextWrapPos();
        ImGui::EndTooltip();
    }
}

// helper type to 'grok' a GUI element. This is using type erasure
// to produce an object with init, run and commit functions, but no
// accessible reference to the GUI element's type - the specific
// capabilities are lambda-captured. Since the result is uniform in
// type, we can put it into containers, like in an object of class
// panel_t, above. We lambda-capter by refernce, so the GUI elements
// must persist while the panel is used.

struct grok_ui_ele_t
{
  std::function < void() > init ;
  std::function < void() > run ;
  std::function < bool(bool) > commit ;

  // c'tor taking a client object. The functionality is extracted
  // with lambda capture

  template < typename client_t >
  grok_ui_ele_t ( client_t & client )
  : init ( [&]() { client.init() ; } ) ,
    run ( [&]() { client.run() ; } ) ,
    commit ( [&] ( bool session_wide )
      { return client.commit ( session_wide ) ; } )
  { }

  // c'tor taking three discrete std::functions. This is more
  // flexible but requires more coding caller-side.

  template < typename client_t >
  grok_ui_ele_t ( std::function < void() > _init ,
                  std::function < void() > _run ,
                  std::function < bool(bool) > _commit )
  : init ( _init ) ,
    run ( _run ) ,
    commit ( _commit )
  { }
} ;

namespace ui
{
  extern float fgui ;
  void on_show_again() ;
  extern bool slideshow_on ;
  extern long grace_period ;
  extern projection_type projection ;
} ;

// we derive panel_t from a std::vector because we intend to
// add GUI elements one after the other during the creation of
// the GUI elements - being a std::vector, panel_t has push_back

struct panel_t
: public std::vector < grok_ui_ele_t >
{
  typedef std::vector < grok_ui_ele_t > base_t ;

  bool & reload_flag ;
  bool & show_flag ;
  std::string name ;
  float fsz ;

  void add ( grok_ui_ele_t e )
  {
    push_back ( e ) ;
  }

  panel_t ( bool & _reload_flag ,
            bool & _show_flag ,
            std::string _name )
  : reload_flag ( _reload_flag ) ,
    show_flag ( _show_flag ) ,
    name ( _name )
  { }
    
  // panel_t offers init, run, and commit, just like an individual
  // GUI element. The functions are implemented as traversal of the
  // contained GUI elements, invoking the corresponding member
  // functions of the GUI elements.

  void init()
  {
    for ( grok_ui_ele_t & e : *this )
      e.init() ;
  }

  bool commit ( bool session_wide )
  {
    bool changed = false ;

    for ( grok_ui_ele_t & e : *this )
    {
      changed |= e.commit ( session_wide ) ;
    }

    return changed ;
  }

  bool header()
  {
    if ( ! show_flag )
    {
      return false ;
    }

    if ( reload_flag )
    {
      init() ;
      reload_flag = false ;
    }

    fsz = ImGui::GetFontSize() ;

    const ImGuiViewport* main_viewport = ImGui::GetMainViewport();
    ImGui::SetNextWindowPos ( ImVec2 ( main_viewport->WorkPos.x + 50 ,
                                       main_viewport->WorkPos.y + 100 ) , ImGuiCond_FirstUseEver ) ;
    ImGui::SetNextWindowSize ( ImVec2 ( 20 * fsz , 18 * fsz ) ,
                                ImGuiCond_FirstUseEver ) ;
    
    const ImGuiWindowFlags window_flags = 0 ;
    ImGui::Begin ( name.c_str() , &show_flag , window_flags ) ;

    ImGui::PushItemWidth(ImGui::GetFontSize() * -12 ) ;

    return true ;
  }

  void footer()
  {
    int middle = 300 * ui::fgui ;

    // The user may wish to apply the options in this panel to only
    // the current view, or permanently, until further notice. So we
    // offer the 'Apply' and 'Okay' buttons in two flavours; not the
    // ##X suffices, which are necessary to disambiguate the button
    // labels but which ImGui does not display.

    ImGui::Separator() ;
    ImGui::Text ( "Use the options given above..." ) ;
    ImGui::SameLine() ;
    HelpMarker ( "'Apply' will apply the changes you've made, and 'Okay' will apply them and also close this panel. Note that you can also set permanent options by creating/editing the .lux.ini file in your home folder. Options you set there will override the defaults. lux looks up options first in the built-in defaults, then in the .lux.ini file, then in the command line arguments, then in the session-wide arguments, and finally in lux files which are passed as images: such files can also contain options." ) ;

    if ( ui::projection == FACET_MAP || ui::projection == CUBEMAP )
      ImGui::Text ( "... for this Set of Images:  " ) ;
    else
      ImGui::Text ( "... for this Image:  " ) ;
    ImGui::SameLine ( middle ) ;
    bool apply1 = ImGui::Button ( "Apply##1" ) ;
    ImGui::SameLine() ;
    bool okay1 = ImGui::Button ( "Okay##1" ) ;

    ImGui::Text ( "... for the current lux session: " ) ;
    ImGui::SameLine ( middle ) ;
    bool applyn = ImGui::Button ( "Apply##2" ) ;
    ImGui::SameLine() ;
    bool okayn = ImGui::Button ( "Okay##2" ) ;

    apply1 |= okay1 ;
    applyn |= okayn ;

    bool cancel = ImGui::Button ( "Cancel" ) ;
    
    if ( apply1 )
    {
      if ( commit ( false ) )
      {
        ui::on_show_again() ;
      }
    }
    else if ( applyn )
    {
      if ( commit ( true ) )
      {
        ui::on_show_again() ;
      }
    }

    if ( okay1 || okayn || cancel )
    {
      show_flag = false ;
      if ( cancel )
        reload_flag = true ;
    }
  }

  void run ( bool call_end = true , bool show_footer = true )
  {
    for ( grok_ui_ele_t & e : *this )
      e.run() ;
    if ( show_footer )
      footer() ;
    if ( call_end )
      ImGui::End() ;
  }

} ;

// to add GUI elements to a panel, we use a convenient macro, which
// 'does it all', so that a single statement creates the GUI element
// and then adds it to the intended target panel. A macro is the
// sensible choice here, because we can use token pasting and the
// stringize operator. We assume there is a panel_t object named
// 'panel' in the calling scope which will accomodate the GUI
// element - usually a static panel_t object in the panel function.
// We also assume that the initial value can be found in a variable
// in namespace ui with the same name.

#define GUI_ELEMENT(TYPE,NAME,INFO) \
  static TYPE##_cli_gui_t NAME ( panel , ui::NAME , #NAME , INFO )

// we settle the ImGui code in namespace ui, because that's the
// namespace holding most of the parameterization which we'll access
// with the ImGui code, and it also holds 'meta-variables' which
// affect the ImGui code as a whole - i.e. which panels are visible.

extern int cl_tokenize ( const std::string & cl ,
                         std::vector < std::string > & result ) ;

namespace ui
{
extern bool show_modal_dialog ;
extern display_mode mode ;
extern bool relaunch ;
extern void on_terminate() ;
extern void on_load_images() ;
extern void on_toggle_decimator() ;
extern bool decimate_area ;
extern std::vector < std::string > override_args ;
extern std::vector < std::string > sessionwide_args ;
extern bool & change_draw ;
extern source_type source ;
extern target_type target ;
extern job_type job ;
extern bool legacy_gui ;
extern bool show_status_line ;
extern display_type state ;

extern bool show_imgui_demo ;
extern bool show_main_menu ;
extern bool show_geometry_panel ;
extern bool show_conditioning_panel ;
extern bool show_export_panel ;
extern bool show_behaviour_panel ;
extern bool show_view_control_panel ;
// extern bool show_transport_panel ;

// any_panel returns true if any of the new GUI's panels are visible.
// This is used by pv_no_rendering.cc to inhibit the effect of the
// Escape key while panels are on.

bool any_panel()
{
  return (    show_imgui_demo
           || show_geometry_panel
           || show_conditioning_panel
           || show_export_panel
           || show_behaviour_panel
           || show_view_control_panel
           // || show_transport_panel
         ) ;
}

void close_all_panels()
{
  show_imgui_demo = false ;
  show_geometry_panel = false ;
  show_conditioning_panel = false ;
  show_export_panel = false ;
  show_behaviour_panel = false ;
  show_view_control_panel = false ;
  // show_transport_panel = false ;
}

// After some initial attempts with hand-coding the GUI, I found that
// this is tiresome and repetitive, and using an OO layer on top of the
// ImGui code promises to make things simpler and more consistent.
// Some panel code is hand-coded, though, because it follows specific
// logic which isn't captured by the 'standard' gui_t variants.

// template class cli_gui_t is a platform to modify CL arguments
// at run-time. This requires a cycle of edit - commit - apply,
// because the manifestation of the changed parameter has to occur
// outside of the display cycle, using lux' mechanism of 'overrides'.
// In a way, this is the least 'immediate mode'-like aspect of the
// GUI, but it's also the aspect which was most awkward in the
// legacy GUI, because it required users to actually present the
// desired parameterization in 'written form' - a process which is
// error-prone and slow. Giving GUI access to this process makes
// the functionality much more accessible, and reduces the risk of
// errors. Since there is already a formalized description of the
// command line arguments (in form of the macro invocations in
// options.h), it's even feasible to generate panel elements for
// CL argument overrides automatically by redefining the macros
// appropriately - this hasn't been attempted yet.

template < typename U , typename T = U >
struct cli_gui_t
{
  // rather than using inheritance, we 'implant' the set of
  // members which are common to all gui_t variants.

  #include "gui_t_members.h"

  // the 'origin' member is the value which is current in the user
  // interface - it's a const reference, as it won't be written to.
  // changes of the GUI representation have to be commited to take
  // effect via a show-again-sequence, and optionally by modifying
  // the session-wide argument value.

  const U & origin ;

  // init takes the value from 'origin' - the ui variable which
  // is affected by this gui element - and produces a 'current'
  // value which resides in the gui element. This value may differ
  // from the ui variable - e.g. degrees vs. radians.

  virtual void init()
  {
    assign ( origin , current ) ;
  }

  // cli_gui_t's c'tor first takes the 'origin' of the datum.
  // next is it's name, which is mandatory for cli_gui_t and has
  // to coincide with the CLI parameter's long name. '_info' is
  // optional: this is the tool tip. Finally, _verify is a function
  // to validate a value which the user proposes.

  cli_gui_t ( const U & _origin ,
              const std::string & _name ,
              const std::string & _info = std::string() ,
              std::function < bool(const T &) > _verify
                = [](const T&) { return true ; }
              )
  : origin ( _origin ) ,
    name ( _name ) ,
    info ( _info ) ,
    verify ( _verify )
  {
    init() ;
  }

  // run displays 'current' as 'proposed'. If the user interacts,
  // the modified value in 'proposed' is validated, and if it checks
  // out as acceptable, it's 'accepted' as the new 'current' value.
  // Note that there are no immediate effects to the rest of the
  // program - this is only triggered by a call to 'commit', which
  // is issued when the user clicks the apply button. The mechanism
  // use here makes sure that 'current' only ever holds valid values.

  void run()
  {
    propose() ;

    if ( present() && verify ( proposed ) )
      accept() ;
    
    if ( info != std::string() )
    {
      ImGui::SameLine() ;
      HelpMarker ( info.c_str() ) ;
    }
  }

  // commit will issue an argument override which will affect
  // the current image only, using an 'override argument' and
  // a show-again-sequence. The override argument is pushed to
  // ui::override_args, the show-again sequence is triggered
  // in code 'further up'. If session-wide is passed 'true',
  // the new value will instead be taken over for the remainder
  // of the session (or until it's changed again).

  bool commit ( bool session_wide = false )
  {
    if ( ! touched )
      return false ;

    if ( name != std::string() )
    {
      std::string option ( "--" ) ;
      option += name ;
      option += "=" ;
      option += std::to_string ( current ) ;
      if ( session_wide )
        ui::sessionwide_args.push_back ( option ) ;
      else
        ui::override_args.push_back ( option ) ;
    }
    return true ;
  }
} ;

// rather than using straight template instatiations, we use explicit
// classes inheriting from template instatiations, which makes overrides
// of some specific functionality easier.

struct float_cli_gui_t
: public cli_gui_t < float >
{
  template < typename ... types >
  float_cli_gui_t ( panel_t & panel , types && ... args )
  : cli_gui_t<float> ( std::forward < types > ( args ) ... )
  { 
    panel.add ( *this ) ;
  }
  bool present()
  {
    return ImGui::InputFloat ( name.c_str() , &proposed ,
                               0.03f * current , current, "%.3f");
  }
} ;

struct double_cli_gui_t
: public cli_gui_t < double >
{
  template < typename ... types >
  double_cli_gui_t ( panel_t & panel , types && ... args )
  : cli_gui_t<double> ( std::forward < types > ( args ) ... )
  { 
    panel.add ( *this ) ;
  }
  bool present()
  {
    return ImGui::InputDouble ( name.c_str() , &proposed ,
                               0.03f * current , current, "%.3f");
  }
} ;

struct int_cli_gui_t
: public cli_gui_t < int >
{
  template < typename ... types >
  int_cli_gui_t ( panel_t & panel , types && ... args )
  : cli_gui_t<int> ( std::forward < types > ( args ) ... )
  { 
    panel.add ( *this ) ;
  }
  bool present()
  {
    return ImGui::InputInt ( name.c_str() , &proposed ) ;
  }
} ;

struct bool_cli_gui_t
: public cli_gui_t < bool >
{
  template < typename ... types >
  bool_cli_gui_t ( panel_t & panel , types && ... args )
  : cli_gui_t<bool> ( std::forward < types > ( args ) ... )
  { 
    panel.add ( *this ) ;
  }

  bool present()
  {
    return ImGui::Checkbox ( name.c_str() , &proposed ) ;
  }

  bool commit ( bool session_wide = false )
  {
    if ( ! touched )
      return false ;

    std::string option ( "--" ) ;
    option += name ;
    option += "=" ;
    option += ( proposed ? "yes" : "no" ) ;

    if ( session_wide )
      ui::sessionwide_args.push_back ( option ) ;
    else
      ui::override_args.push_back ( option ) ;

    return true ;
  }
} ;

struct combo_cli_gui_t
: public cli_gui_t < std::string >
{
  typedef cli_gui_t < std::string > base_t ;

  const std::vector < std::string > & values ;
  const char ** values_cstr ;
  int index ;

  combo_cli_gui_t ( panel_t & panel ,
                    const std::vector < std::string > & _values ,
                    const std::string & _origin ,
                    const std::string & _name ,
                    const std::string & _info = std::string() )
  : base_t ( _origin , _name , _info ) ,
    values ( _values )
  {
    index = -1 ;
    int i = 0 ;
    for ( auto v : values )
    {
      // std::cout << "*** " << v << " vs. " << origin << std::endl ;
      if ( v == origin )
      {
        index = i ;
        break ;
      }
      ++i ;
    }
    assert ( index >= 0 ) ;
    values_cstr = new const char * [ values.size() ] ;
    for ( i = 0 ; i < values.size() ; i++ )
      values_cstr[i] = values[i].c_str() ;
    panel.add ( *this ) ;
  }

  bool verify ( const std::string & c )
  {
    for ( auto v : values )
    {
      if ( v == c )
        return true ;
    }
    return false ;
  }

  bool present()
  {
    return ImGui::Combo ( name.c_str() ,
                          &index ,
                          values_cstr ,
                          values.size() ) ;
  }

  bool commit ( bool session_wide = false )
  {
    if ( ! touched )
      return false ;

    std::string option ( "--" ) ;
    option += name ;
    option += "=" ;
    option += values [ index ] ;

    if ( session_wide )
      ui::sessionwide_args.push_back ( option ) ;
    else
      ui::override_args.push_back ( option ) ;

    return true ;
  }

} ;

// text input field for extra arguments. We derive this from
// cli_gui_t < bool > and bind it to a dummy bool; the functionality
// is implemented via the commit function.

struct text_cli_gui_t
: public cli_gui_t < bool >
{
  char text[1024 * 16] ;
  ImGuiInputTextFlags flags = ImGuiInputTextFlags_AllowTabInput ;

  template < typename ... types >
  text_cli_gui_t ( panel_t & panel , types && ... args )
  : cli_gui_t<bool> ( std::forward < types > ( args ) ... )
  { 
    panel.add ( *this ) ;
  }

  // 'text' is 16K, but we only display a four-line window. If
  // user input exceeds the four lines, ImGui provides a scroll
  // bar.

  bool present()
  {
    return ImGui::InputTextMultiline
      ( name.c_str() ,
        text ,
        IM_ARRAYSIZE ( text ) ,
        ImVec2 ( -FLT_MIN , ImGui::GetTextLineHeight() * 4 ) ,
        flags
      ) ;
  }

  void run()
  {
    propose() ;

    if ( info != std::string() )
    {
      ImGui::Text ( "%s" , name.c_str() ) ;
      ImGui::SameLine() ;
      HelpMarker ( info.c_str() ) ;
    }

    if ( present() && verify ( proposed ) )
      accept() ;
    
  }

  bool commit ( bool session_wide = false )
  {
    if ( ! touched )
      return false ;

    if ( strlen ( text ) == 0 )
      return false ;

    if ( session_wide )
      cl_tokenize ( text , ui::sessionwide_args ) ;
    else
      cl_tokenize ( text , ui::override_args ) ;

    text[0] = 0 ;

    return true ;
  }
} ;

struct string_cli_gui_t
: public cli_gui_t < std::string >
{
  char text[1024] ;

  template < typename ... types >
  string_cli_gui_t ( panel_t & panel , types && ... args )
  : cli_gui_t<std::string> ( std::forward < types > ( args ) ... )
  { 
    panel.add ( *this ) ;
  }

  bool present()
  {
    return ImGui::InputText
      ( name.c_str() ,
        text ,
        IM_ARRAYSIZE ( text )
      ) ;
  }

  bool commit ( bool session_wide = false )
  {
    if ( ! touched )
      return false ;
    if ( strlen ( text ) <= 0 )
      return false ;

    std::string option ( "--" ) ;
    option += name ;
    option += "=" ;
    option += ( text ) ;

    if ( session_wide )
      ui::sessionwide_args.push_back ( option ) ;
    else
      ui::override_args.push_back ( option ) ;

    return true ;
  }
} ;

extern bool run_fullscreen ;
bool file_select_pending = false ;
extern void on_toggle_legacy_gui() ;

static void ShowLuxMenuFile()
{
    if (ImGui::MenuItem("Open", "Ctrl+O"))
    {
      if ( ui::run_fullscreen )
      {
        // in full-screen mode, we can't use on_load_images, because it
        // closes and reopens the window. So we postpone the file dialog:

        file_select_pending = true ;
        mode = TERMINATE ;
        relaunch = false ;
      }
      else
      {
        ui::on_load_images() ;
      }
    }
    if (ImGui::MenuItem("ImGui Demo"))
    {
      // having this option is handy to see the whole range of ImGui
      // widgets anc capabilities, and to check if they all behave
      // as intended on a given platform.

      show_imgui_demo = true ;
    }
    if (ImGui::MenuItem("lux legacy GUI"))
    {
      // switch to the old GUI. first deactivate all panels:

      ui::show_main_menu = false ;
      ui::show_export_panel = false ;
      ui::show_conditioning_panel = false ;
      ui::show_geometry_panel = false ;
      ui::show_behaviour_panel = false ;

      // no call the ui function to toggle the GUI

      ui::on_toggle_legacy_gui() ;
    }
    if (ImGui::MenuItem("Quit"))
    {
      ui::on_terminate() ;
    }
}

static void ShowLuxMenuViewer()
{
  bool clicked = false ;
  if (ImGui::MenuItem("View Control"))
  {
    show_view_control_panel = true ;
  }
  ImGui::SameLine();
  HelpMarker ( "This panel offers controls on how the viewer operates - it has controls for the virtual camera, image loading and transport" ) ;
  if (ImGui::MenuItem("General Settings"))
  {
    show_behaviour_panel = true ;
  }
  ImGui::SameLine();
  HelpMarker ( "This panel offers access to some common lux options, which you can also access via the command line. You can change these settings for the current view or dor the entire session." ) ;
  if (ImGui::MenuItem("Conditioning Settings"))
  {
    show_conditioning_panel = true ;
  }
  ImGui::SameLine();
  HelpMarker ( "With this panel you can determine how lux will process image data - settings like interpolation and decimation methods and image pyramid building options." ) ;
  if (ImGui::MenuItem("Geometry Settings"))
  {
    show_geometry_panel = true ;
  }
  ImGui::SameLine();
  HelpMarker ( "With this panel you can tell lux to use a specific projection for the source image at hand and the displayed view - normally this is done automatically. For 'proper' projections you can also set the source image's horizontal field of view." ) ;
  if (ImGui::MenuItem("Export"))
  {
    show_export_panel = true ;
  }
  ImGui::SameLine();
  HelpMarker ( "With this panel you can control settings for and creation of snapshots." ) ;
}

// uses 'add' suffix to produce unique label strings: this function
// is called both for the source and the target projection, and if
// both use the same labels, the UI does not work.

static std::string ShowPrjSelection ( int & prj ,
                                      int & _prj ,
                                      std::string add ,
                                      bool offer_mosaic )
{
  std::string rect ( "rectilinear" ) ;
  rect += add ;
  std::string sph ( "spherical" ) ;
  sph += add ;
  std::string cyl ( "cylindric" ) ;
  cyl += add ;
  std::string fish ( "fisheye" ) ;
  fish += add ;
  std::string ster ( "stereographic" ) ;
  ster += add ;
  ImGui::RadioButton(rect.c_str(), &prj, RECTILINEAR);
  ImGui::SameLine();
  ImGui::RadioButton(sph.c_str(), &prj, SPHERICAL);
  ImGui::SameLine();
  ImGui::RadioButton(cyl.c_str(), &prj, CYLINDRIC);

  ImGui::RadioButton(fish.c_str(), &prj, FISHEYE);
  ImGui::SameLine();
  ImGui::RadioButton(ster.c_str(), &prj, STEREOGRAPHIC);
  if ( offer_mosaic )
  {
    std::string mos ( "mosaic" ) ;
    mos += add ;
    ImGui::SameLine();
    ImGui::RadioButton(mos.c_str(), &prj, MOSAIC);
  }

  if ( prj != _prj )
  {
    switch ( prj )
    {
      case RECTILINEAR :
        return ( "rectilinear" ) ;
        break ;
      case SPHERICAL :
        return ( "spherical" ) ;
        break ;
      case CYLINDRIC :
        return ( "cylindric" ) ;
        break ;
      case FISHEYE :
        return ( "fisheye" ) ;
        break ;
      case STEREOGRAPHIC :
        return ( "stereographic" ) ;
        break ;
      default :
        if ( offer_mosaic )
          return ( "mosaic" ) ;
        else
          return ( "rectilinear" ) ;
        break ;
    }
  }
  else
    return std::string() ;
}

// if this flag is set, a new cycle has started and the variables
// holding state need to be updated.

bool imgui_reload_geometry = true ;
bool imgui_reload_conditioning = true ;
bool imgui_reload_export = true ;
bool imgui_reload_behaviour = true ;
bool imgui_reload_view_control = true ;
// bool imgui_reload_transport = true ;

extern void do_zoom_in() ;
extern void do_zoom_out() ;

void ShowGeometryPanel()
{
  if ( ! show_geometry_panel )
    return ;

  auto fsz = ImGui::GetFontSize() ;

  const ImGuiViewport* main_viewport = ImGui::GetMainViewport();
  ImGui::SetNextWindowPos ( ImVec2 ( main_viewport->WorkPos.x + 50 ,
                            main_viewport->WorkPos.y + 50 ) , ImGuiCond_FirstUseEver ) ;
  ImGui::SetNextWindowSize ( ImVec2 ( 20 * fsz , 18 * fsz ) ,
                             ImGuiCond_FirstUseEver ) ;
  
  ImGuiWindowFlags window_flags = 0 ;
  ImGui::Begin("Geometry Panel", &show_geometry_panel, window_flags) ;
  ImGui::PushItemWidth(ImGui::GetFontSize() * -2);

  ImGui::Text ( "Set Projection and FoV for this View" ) ;
  ImGui::SameLine() ;
  HelpMarker ( "The settings in this panel override the geometry settings which are active for the current view. Changes you make here will not have a permanent effect - only the current view is affected." ) ;

  static int _prj , prj ;
  static float _fov , fov ;
  static int _tprj , tprj ;
  static float _tfov , tfov ;
  static float _vofs , vofs , vfov ;
  static float vofs_lower_limit , vofs_upper_limit ;
  std::string src_prj ;
  std::string trg_prj ;

  if ( imgui_reload_geometry )
  {
    _prj = prj = int ( ui::projection ) ;
    _tprj = tprj = int ( ui::target.projection ) ;
    _fov = fov = ui::source.hfov * 180.0 / M_PI ;

    if (    source.projection != MOSAIC
         && source.projection != CUBEMAP
         && source.projection != FACET_MAP )
    {
      double lower_limit , upper_limit ;

      if (    source.projection == FISHEYE
           || source.projection == STEREOGRAPHIC )
      {
        lower_limit = 0.0 ;
        upper_limit = 2.0 * M_PI ;
      }
      else if (    source.projection == RECTILINEAR
                || source.projection == SPHERICAL
                || source.projection == CYLINDRIC )
      {
        lower_limit = M_PI_2 ;
        upper_limit = M_PI + M_PI_2 ;
      }

      vfov = ui::source.vfov * 180.0 / M_PI ;
      _vofs = vofs = ui::source.vofs * 180.0 / M_PI ;
      lower_limit = lower_limit * 180.0 / M_PI ;
      upper_limit = upper_limit * 180.0 / M_PI ;
      vofs_lower_limit = lower_limit ;
      vofs_upper_limit = ( upper_limit - vfov ) ;
    }

    imgui_reload_geometry = false ;
  }

  if ( prj == CUBEMAP )
  {
    ImGui::Text ( "source image projection: CUBEMAP" ) ;
  }
  else if ( prj == FACET_MAP )
  {
    ImGui::Text ( "source image projection: FACET MAP" ) ;
  }
  else
  {
    ImGui::Text ( "source image projection:" ) ;
    ImGui::SameLine() ;
    HelpMarker ( "Set the source image's projection. lux will normally figure this out from the image's metadata, but if metadata are missing or you want to override them, you can do it here. Note that picking 'mosaic' here will also set the target projection to 'mosaic'.\nOnce you're done setting projection and field of view, click 'Apply'." ) ;

    src_prj = ShowPrjSelection ( _prj , prj , "##1" , true ) ;

    if ( _prj != MOSAIC && _tprj != MOSAIC )
    {
      ImGui::Text ( "source image horizontal field of view:" ) ;
      ImGui::SameLine() ;
      HelpMarker ( "Set the source image's horizontal field of view. lux will normally figure this out from the image's metadata, but if metadata are missing or you want to override them, you can do it here. Use the upper slider for quickly setting a starting point, then fine-tune with the lower slider.\nOnce you're done setting projection and field of view, click 'Apply'." ) ;

      float max_fov = 360.0f ;
      if ( _prj == RECTILINEAR )
        max_fov = 179.0f ;
          
      ImGui::SliderFloat("hfov##1", &fov, 0.0f, max_fov);
      ImGui::InputFloat("hfov##2", &fov, 1.0);

      ImGui::Text ( "source image vertical offset:" ) ;
      ImGui::SameLine() ;
      HelpMarker ( "Set the source image's vertical offset. This affects the horizontal line in the image where lux assumes the horizon. Modify this value if the horizon is wrong. Some images - like full sphericals - have no room to manoevre.\nIt's more intuitive to modify the horizon with the 'H' and Shift + 'H' keys." ) ;

      ImGui::SliderFloat("vofs", &vofs, vofs_lower_limit, vofs_upper_limit);
    }
  }

  if ( _prj == MOSAIC )
  {
    ImGui::Text ( "target image projection: mosaic" ) ;
  }
  else
  {
    if ( _tprj == MOSAIC )
      // _tprj = _prj ;
      _tprj = RECTILINEAR ;

    ImGui::Text ( "target image projection:" ) ;
    ImGui::SameLine() ;
    HelpMarker ( "Set the viewer's target projection. lux will normally display a rectilinear image - this is like what you'd get taking a photograph with an 'ordinary' lens. A different target projection will result in views looking like images which were taken with other types of lenses, like fisheyes. Once you're done setting projection and field of view, click 'Apply' or 'Okay' - the latter will also close the panel." ) ;

    trg_prj = ShowPrjSelection ( _tprj , tprj , "##2" ,
                                 false ) ;
  }

  bool apply_pressed = ImGui::Button ( "Apply" ) ;
  ImGui::SameLine() ;
  bool okay_pressed = ImGui::Button ( "Okay" ) ;
  apply_pressed |= okay_pressed ;

  if ( apply_pressed )
  {
    bool change = false ;
    if ( src_prj != std::string() )
    {
      if ( trg_prj == "mosaic" )
        src_prj = "mosaic" ;
      change = true ;
      std::string option ( "--projection=" ) ;
      option += src_prj ;
      ui::override_args.push_back ( option ) ;
    }
    if ( trg_prj != std::string() )
    {
      if ( src_prj == "mosaic" )
        trg_prj = "mosaic" ;
      change = true ;
      std::string option ( "--target_projection=" ) ;
      option += trg_prj ;
      ui::override_args.push_back ( option ) ;
    }
    if ( fov != _fov )
    {
      change = true ;
      std::string arg = "--hfov=" + std::to_string ( fov ) ;
      ui::override_args.push_back ( arg ) ;
    }
    if ( source.projection != MOSAIC && vofs != _vofs )
    {
      change = true ;
      std::string arg =   "--vertical_offset="
                + std::to_string ( vofs ) ;
      ui::override_args.push_back ( arg ) ;
    }
    if ( change )
    {
      ui::on_show_again() ;
    }
  }
  if ( okay_pressed )
    show_geometry_panel = false ;

  ImGui::End() ;
}

extern bool process_linear ;
extern int fast_interpolator_degree ;
extern int quality_interpolator_degree ;
extern double pyramid_scaling_step ;
extern int pyramid_smoothing_level ;
extern bool grey_edge ;
extern bool auto_quality ;
extern bool auto_position ;
extern bool allow_pan_mode ;
extern source_type source ;
extern double hfov ;
extern void on_shift_up() ;
extern void on_shift_down() ;

void ShowConditioningPanel()
{
  static panel_t panel ( imgui_reload_conditioning ,
                         show_conditioning_panel ,
                         "Conditioning Panel" ) ;

  if ( ! panel.header())
      return ;

  ImGui::Text ( "Set Image Processing Options" ) ;
  ImGui::SameLine() ;
  HelpMarker ( "The settings in this panel will override the image processing settings which are active for the current view, or for the remainder of the session, until further notice. There are two rows of Apply/Okay buttons near the bottom of the panel: the first row pertains to the current view only, the second row to the entire session. As long as you don't click any of the Buttons, changes in the panel won't have an effect, and clicking 'Cancel' will discard all changes. Clicking the 'Apply' buttons leaves the panel visible, clicking 'Okay' applies the overrides and closes the panel." ) ;

  static bool_cli_gui_t is_lin
  (
    panel ,
    ui::source.is_linear ,
    "is_linear" ,
    "Some image formats - like TIFF - can contain linear RGB or sRGB data. For such images, you can use this checkbox to tell lux how to interpret the source image's data. For other formats (like JPG) this option makes no difference, and you can't use it to force a different interpretation."
  ) ;

  GUI_ELEMENT(bool,process_linear,
    "lux does all internal image processing in the linear RGB colour space. This is the default and mathematically correct, but it takes additional processing when the view is displayed, because the linear RGB data have to be converted to sRGB for visualization. If you clear this flag, lux will process incoming data *as if* they were linear RGB, even if they are not, and, after processing, use the result as *if it* were sRGB. This is not entirely correct mathematically, but oftentimes it is 'good enough' and it saves processing time. So it's more of an emergency measure if you don't have enough CPU power for the view at hand."
  ) ;

  GUI_ELEMENT(int,fast_interpolator_degree,
    "This option sets the degree of the b-spline used for animated sequences. lux uses b-splines for interpolation. The simplest b-spline is the one with 'degree' zero - this is also known as 'nearest neighbour interpolation' and picks the RGB value of the nearest source image pixel. One step up is degree one, a.k.a. 'bilinear interpolation', which is the default here - it's quite fast to compute and looks okay as long as the image isn't magnified much. Above one are what you might call 'b-splines proper' which show no artifacts even with strong magnification, but such splines take more processing time, and often result in stutter in animated sequences. If you get smooth animations with degree two here, it's preferable to the default, but going beyond two has little merit here."
  ) ;

  GUI_ELEMENT(int,quality_interpolator_degree,
    "This option sets the degree of the b-spline used for the still image - so, when the viewer is at rest. lux uses b-splines for interpolation. The default for this value is three - a 'cubic b-spline'. This is a good compromise between CPU load and image quality. Since this interpolator is only used for still images, it's no problem if the calculation takes a little longer. Degree 2 is faster and looks very similar, with degree one you get the typical 'star-shaped artifacts' in high magnification, and degree zero gives you the pixels-as-squares look you get with most image viewers."
  ) ;

  // TODO: values above 2.0?

  static bool_cli_gui_t brp
  (
    panel ,
    ini::args.build_raw_pyramids ,
    "build_raw_pyramids" ,
    "To display scaled-down views appropriately, lux uses image pyramids. the down-side of image pyramids is their rather large memory footprint and the time they take to build. Switching this option off will suppress the creation of image pyramids altogether. The viewer will produce frames directly from the full-size image, which will result in aliasing for small zoom levels, and magnified views may look slightly blurred." ) ;

  static bool_cli_gui_t bp
  (
    panel ,
    ini::args.build_pyramids ,
    "build_pyramids" ,
    "Switching this option off will not switch off pyramid building altogether, only the image in original resolution will not use single-precision float data internally, but whatever data type the image contains natively. Most of the time this is a smaller data type like UINT8, so this saves a good deal of memory. The disadvantage of switching this option off is that magnified views need to work from the raw image data, so it's no longer possible to use b-splines with a degree larger than one for interpolation in a mathematically correct way, so that magnified views may show a slight blur." ) ;

  // TODO: enforce range

  static double_cli_gui_t py_scaling
  (
    panel ,
    ui::pyramid_scaling_step ,
    "pyramid_scaling_step" ,
    "This value determines by what factor a new pyramid level is smaller than the one below it. Thanks to the image pyramid math use by lux, this does not have to be precisely 2.0 as in other image pyramid code, but you can use - within reason - arbitrary float values. 'Sensible' values range between 1.25 and 2.0. If you get too close to 1.0, the pyramid will become very steep without any real benefit, but likely image degradation due to the repeated data re-processing. Going above 2.0, you may save a bit of memory by getting a 'shallower' pyramid, but switching from one level to the next during zoom sequences may become quite noticeable. This option works together with the next one, the 'decimator'." ,
    [] ( const double & candidate)
       { return candidate > 1.1 && candidate <= 2.0 ; }
   ) ;

  GUI_ELEMENT ( int , pyramid_smoothing_level ,
    "Here you can pass an integer value to determine the 'decimator' which is used to create a new pyramid level from the one below it. The parametrization is a bit obscure (TODO: submenu) - please refer to the documentation of the 'pyramid_smoothing_level' command line parameter, which is set here. Please note that some values are not permitted and will crash your lux session. The default of -1 uses 'area decimation', -2 uses the simple (1/4 1/2 1/4) binomial filter for low-pass filtering. -3 uses the binomial two degrees up, -4 uses an 'optimal Burt filter'. Positive values use the corresponding b-spline reconstruction kernel - seven is a good choice."
  ) ;

  GUI_ELEMENT ( bool , decimate_area ,
    "This flag activates the use of 'area decimation' for animated displays of scaled-down views. This method of down-scaling uses an adaptive filter, whereas the default method uses interpolation from the image pyramid level nearest in resolution to the required output resolution. Using 'decimate_area' scales down from the next-better pyramid level, and level switching becomes less noticeable. The disadvantage is slightly more CPU load. Note that 'area decimation' can also be used during the creation of the image pyramid to create a pyramid level from the level below it. For that use of area decimation, set 'pyramid smoothing level' to -1 (the default)" ) ;

  GUI_ELEMENT ( bool , grey_edge ,
    "If this flag is set, lux will produce a soft edge around the image. This avoids annoying staircase artifacts along the image's edges. Currently, switching this off will have no effect for images with an alpha channel."
  ) ;

  static bool_cli_gui_t xfade
  (
    panel ,
    ini::args.crossfade ,
    "crossfade" ,
    "This option activates cross-fading from one view to the next. I feel that this is more visually appealing than simply switching to the next view immediately, but it's a matter of taste..." ) ;

  static double_cli_gui_t xfade_d
  (
    panel ,
    ini::args.crossfade_delta ,
    "crossfade_delta" ,
    "When cross-fading from one view to the next, this option controls the speed of the cross-fade." ) ;

  GUI_ELEMENT ( bool , auto_position ,
    "If your image is a panorama, this option will set the initial view so that it coincides with the left margin of the image and has the horizon in the view's vertical center. This is so that you can immediately hit Space for a horizontal pan." ) ;

  static bool_cli_gui_t r_as_m
  ( panel ,
    ini::args.rectilinear_as_mosaic ,
    "rectilinear_as_mosaic" ,
    "Display rectilinear images as 'flat' images, without perspective correction. This cancels rectilinear projection gleaned from image metadata and is only effective from the next image onwards." ) ;
  
  GUI_ELEMENT ( bool , allow_pan_mode ,
    "Activate faster rendering of 'straight' pan for some projections" ) ;

  GUI_ELEMENT ( bool , auto_quality ,
    "Automatically adapt rendering quality for animated sequences to avoid stutter due to dropped frames" ) ;

  panel.run ( true , true ) ;
}

extern std::string snapshot_extension ;
extern float snapshot_magnification ;
extern float output_magnification ;
extern int compression ;
extern void on_snapshot() ;
extern void on_facet_echo() ;
extern blending_mode blending ;
extern bool use_nonstandard_target ;
extern bool faux_bracket ;
extern bool exposure_fusion ;
extern void _exposure_fusion ( bool force = false ) ;
extern void _stitch_images ( bool force = false ) ;
extern void _snapshot ( bool force = false ) ;
extern void on_source_like_snapshot() ;
extern void on_view_snapshot() ;

void ShowExportPanel()
{
  static panel_t panel ( imgui_reload_export ,
                         show_export_panel ,
                         "Export Panel" ) ;

  if ( ! panel.header() )
    return ;

  ImGui::Text ( "Set Options for Exporting Image Files" ) ;
  ImGui::SameLine() ;
  HelpMarker ( "The settings in this panel set the export settings which are active for the current view, or for the remainder of the session, until further notice. There are two rows of Apply/Okay buttons near the bottom of the panel: the first row pertains to the current view only, the second row to the entire session. As long as you don't click any of the Buttons, changes in the top half of the panel won't have an effect, and clicking 'Cancel' will discard all changes. Clicking the 'Apply' buttons leaves the panel visible, clicking 'Okay' applies the overrides and closes the panel." ) ;

  static std::vector < std::string >
  extensions { "jpg" , "png" , "tif" , "exr" } ;

  static combo_cli_gui_t snap_ext ( panel , extensions ,
                           ini::args.snapshot_extension ,
                           "snapshot_extension" ,
                           "determines the image format for snapshots" ) ;

  if ( ini::args.snapshot_extension == "tif" )
  {
    static bool_cli_gui_t snapshot_tiff_linear
    (
      panel ,
      ini::args.snapshot_tiff_linear ,
      "snapshot_tiff_linear" ,
      "when producing snapshots in TIFF format, save linear RGB data, rather than sRGB."
    ) ;
  }

  // TODO: might slot in a modal if target file exists
  // TODO: offer a format string to infix stuff like serial number

  static string_cli_gui_t snap_base ( panel ,
                                      ini::args.snapshot_basename ,
                                      "snapshot_basename" ,
    "Override the Base Name for snapshots. Normally this is put together from the input filename and some suffixes indicating the type of processing, if any, and a serial number. Leave this field empty for the default behaviour. If you enter anything here, the given basename will only be suffixed with the snapshot extension. If you set the to a particular value and leave it at that, new snapshots will override old ones, so this is more of a one-shot option, and you have to pass a new base name for every new snapshot. Note that file names generated with this option may match existing files, in which case the existing file will be overwritten (!). The snapshot will go to the folder where lux was invoked unless you use a full path name." ) ;

  static float_cli_gui_t
    snap_mag ( panel , ui::snapshot_magnification ,
                "snapshot_magnification" ,
                "Magnification of the snaphot vs. on-screen size:"
                " If your window is 800X600 and the magnification is 2.0,"
                " the snapshot will be rendered as 1600X1200. This value"
                " refers to 'ordinary' snapshots - 'source-like snapshots'"
                " won't be affected - use 'output magnification' if you"
                " want all output to be magnified." ,
                [] ( const float & candidate)
                  { return candidate > 0.0f ; }
              ) ;

  static float_cli_gui_t
    out_mag ( panel , ui::output_magnification ,
                "output_magnification" ,
                "Magnification of a source-like snapshot. The metrics of"
                " such a snapshot are determined by the source image - or"
                " the p-line of a PTO file when a PTO file is displayed,"
                " and they are not affected by 'snapshot magnification'."
                " If you need a larger (or smaller) version of a source-like"
                " snapshot, use this parameter." ,
                [] ( const float & candidate)
                  { return candidate > 0.0f ; }
              ) ;

  static int_cli_gui_t
    snap_cmpr ( panel ,
                ui::compression ,
                "snapshot_compression" ,
                "Some image formats - like JPG - can apply a variable"
                " degree of image compression, which you can set here."
                " Lower the qualtiy setting to compress the image more,"
                " e.g. for web export. The value is given in percent." ,
                [] ( const int & candidate)
                  { return candidate > 0 && candidate <= 100 ; } 
              ) ;

  static std::vector < std::string >
  blending_modes { "auto" , "ranked" , "hdr" , "quorate" } ;

  if ( ui::projection == FACET_MAP )
  {
    static combo_cli_gui_t blending (
      panel , blending_modes , ini::args.blending ,
      "blending" ,
      "determines the blending mode for synoptic views. Normally this is set automatically, but here you can override the automatics. 'ranked' is for panoramas, 'hdr' for hdr merging and exposure fusions, and 'quorate' for deghosting." ) ;
    
    static double_cli_gui_t hdr_spread
    (
      panel ,
      ini::args.hdr_spread ,
      "hdr_spread" ,
      "This value controls the dynamic range of exposure fusions (this applies to image stacks in panoramas with stacks as well). An hdr spread of zero will produce an 'normal' exposure fusion - the resulting image is limited to the sRGB range, with all images compressed into this range with the exposure fusion algorithm. An hdr spread of one produces an 'hdr fusion' of the images: the resulting image will contain the entire range of intensity values captured by the contributing images, but the merge is also done with the exposure fusion algorithm. Values in between will compress the dynamic range to some extent in between. The one-click 'HDR-Fuse', below, uses a fixed hdr_spread of 1.0 and overrides this setting." ) ;

  }

  static int_cli_gui_t fb_size
  (
    panel ,
    ini::args.faux_bracket_size ,
    "faux_bracket_size" ,
    "Number of artificial exposures used for a faux bracket. The default is three exposures. Note that you can also pass a set of explicit Ev values on the command line (refer to the decumentation of faux_bracket_ev), in which case the settings here will be ignored."
  ) ;

  static double_cli_gui_t fb_step
  (
    panel ,
    ini::args.faux_bracket_step ,
    "faux_bracket_step" ,
    "Ev difference of artificial exposures used for a faux bracket. The default is two Ev. Note that you can also pass a set of explicit Ev values on the command line (refer to the decumentation of faux_bracket_ev), in which case the settings here will be ignored."
  ) ;

  panel.run ( false ) ;

  ImGui::TextWrapped (
    "The remainder of GUI elements in this panel has immediate effect, you don't have to use 'Apply' or 'Okay'. The buttons offer several 'single-click' renditions (most of them are only available for facet maps - e.g. from PTO files) which process the input in different ways, like stitch or fuse them." ) ;

  static bool like_source = false ;
  ImVec2 button_sz ( 220 * ui::fgui , 30 * ui::fgui ) ;
  bool snap = ImGui::Button ( "Snapshot" , button_sz ) ;

  ImGui::SameLine ( 250 * ui::fgui ) ;
  ImGui::Checkbox ( "like source##1" , &like_source ) ;
  if ( snap )
  {
    if ( like_source )
      on_source_like_snapshot() ;
    else
      ui::on_view_snapshot() ;
  }
  ImGui::SameLine() ;
  HelpMarker (
    "Take a snapshot. The checkbox to the right can be used to create a 'source-like' snapshot. Normal snapshots will look like the view seen on-screen, but source-like snapshots will take geometry and size from the source image (or the p-line of a PTO file).\nIf the input is a synoptic view - like a PTO file - lux will use a heuristic method to figure out how to combine the images (the 'blending mode') - as a panorama, exposure fusion etc. - and create the snapshot accordingly. If you want to override the automatics, you can use one of the set of buttons below. Some options affect how the snapshot is made, e.g. the options in the top half of this panel. If you don't touch these options, everything will be set automatically. You can press 'E' to the same effect - use Shift+E for a source-like snapshot" ) ;
  
  static bool fuse_like_source = false ;
  static bool stitch_like_source = false ;
  static bool stack_like_source = false ;
  static bool deghost_like_source = false ;
  static bool hdr_merge_like_source = false ;
  static bool hdr_fuse_like_source = false ;
  static bool compress_like_source = false ;

  if ( projection == FACET_MAP )
  {
    bool fuse = ImGui::Button ( "Exposure Fusion" , button_sz ) ;
    ImGui::SameLine ( 250 * ui::fgui ) ;
    ImGui::Checkbox ( "like source##2" , &fuse_like_source ) ;
    ImGui::SameLine() ;
    HelpMarker ( "unconditionally produce an exposure fusion from the image set, either from the portion visible on-screen, or, if 'like source' is set, like the source image or according to the settings in the p-line." ) ;

    if ( fuse )
    {
      ui::blending = BLEND_HDR ;
      ini::args.exposure_weight = 1.0 ;
      ini::args.contrast_weight = 0.0 ;
      ui::use_nonstandard_target = fuse_like_source ;
      ui::_exposure_fusion() ;
    }

    bool stack = ImGui::Button ( "Focus stack" , button_sz ) ;
    ImGui::SameLine ( 250 * ui::fgui ) ;
    ImGui::Checkbox ( "like source##3" , &stack_like_source ) ;
    ImGui::SameLine() ;
    HelpMarker ( "unconditionally produce a focus stack from the image set, either from the portion visible on-screen, or, if 'like source' is set, like the source image or according to the settings in the p-line." ) ;

    if ( stack )
    {
      ui::blending = BLEND_HDR ;
      ini::args.exposure_weight = 0.0 ;
      ini::args.contrast_weight = 1.0 ;
      ui::use_nonstandard_target = stack_like_source ;
      ui::_exposure_fusion() ;
    }

    bool stitch = ImGui::Button ( "Stitch Images" , button_sz ) ;
    ImGui::SameLine ( 250 * ui::fgui ) ;
    ImGui::Checkbox ( "like source##4" , &stitch_like_source ) ;
    ImGui::SameLine() ;
    HelpMarker ( "unconditionally stitch the image set together, either from the portion visible on-screen, or, if 'like source' is set, like the source image or according to the settings in the p-line." ) ;

    if ( stitch )
    {
      ui::blending = BLEND_RANKED ;
      ui::use_nonstandard_target = stitch_like_source ;
      ui::_stitch_images() ;
    }

    bool deghost = ImGui::Button ( "Deghost Image Set" , button_sz ) ;
    ImGui::SameLine ( 250 * ui::fgui ) ;
    ImGui::Checkbox ( "like source##5" , &deghost_like_source ) ;
    ImGui::SameLine() ;
    HelpMarker ( "unconditionally blend the image set with deghosting ('quorate' blending mode), either from the portion visible on-screen, or, if 'like source' is set, like the source image or according to the settings in the p-line. Note that this only makes sense for a series of images with equal Ev." ) ;

    if ( deghost )
    {
      ui::blending = BLEND_QUORATE ;
      ui::use_nonstandard_target = deghost_like_source ;
      ui::_snapshot() ;
    }

    bool hdr_merge = ImGui::Button ( "HDR-merge Image Set" , button_sz ) ;
    ImGui::SameLine ( 250 * ui::fgui ) ;
    ImGui::Checkbox ( "like source##6" , &hdr_merge_like_source ) ;
    ImGui::SameLine() ;
    HelpMarker ( "unconditionally HDR-merge the image set into an openEXR HDR image, either from the portion visible on-screen, or, if 'like source' is set, like the source image or according to the settings in the p-line." ) ;

    if ( hdr_merge )
    {
      ui::blending = BLEND_HDR ;
      ui::use_nonstandard_target = hdr_merge_like_source ;
      ini::args.snapshot_extension = "exr" ;
      ui::snapshot_extension = ".exr" ;
      ui::_snapshot() ;
    }

    bool hdr_fuse = ImGui::Button ( "HDR-fuse Image Set" , button_sz ) ;
    ImGui::SameLine ( 250 * ui::fgui ) ;
    ImGui::Checkbox ( "like source##7" , &hdr_fuse_like_source ) ;
    ImGui::SameLine() ;
    HelpMarker ( "unconditionally HDR-fuse the image set into an openEXR HDR image, either from the portion visible on-screen, or, if 'like source' is set, like the source image or according to the settings in the p-line. This uses exposure fusion and hdr_spread=1, which creates a subtly different result to HDR-merge (above)." ) ;

    if ( hdr_fuse )
    {
      ui::blending = BLEND_HDR ;
      ui::use_nonstandard_target = hdr_fuse_like_source ;
      ini::args.snapshot_extension = "exr" ;
      ui::snapshot_extension = ".exr" ;
      // ui::exposure_fusion = true ;
      ini::args.exposure_weight = 1.0 ;
      ini::args.contrast_weight = 0.0 ;
      ini::args.hdr_spread = 1 ;
      ui::_exposure_fusion() ;
    }
  }

  bool compress = ImGui::Button ( "Compress Dynamic Range" , button_sz ) ;
  ImGui::SameLine ( 250 * ui::fgui ) ;
  ImGui::Checkbox ( "like source##8" , &compress_like_source ) ;
  ImGui::SameLine() ;
  HelpMarker ( "compress the dynamic range of the current image by creating a 'faux bracket', either from the portion visible on-screen, or, if 'like source' is set, like the source image or according to the settings in the p-line. This will create a faux bracket with the settings given in above (faux bracket size and step) and exposure-fuse it. This is most useful for HDR input, but it will also accept LDR images. Faux bracketing is a good alternative to tone-mapping, and often produces more natural-looking results. At the same time, content can be kept in HDR (e.g. in an openEXR file) and a dynamic-compressed rendition is just a click away." ) ;

  if ( compress )
  {
    if ( projection != FACET_MAP )
    {
      ui::blending = BLEND_HDR ;
    }
    ui::use_nonstandard_target = compress_like_source ;
    ui::faux_bracket = true ;
    // ui::exposure_fusion = true ;
    ini::args.faux_bracket_ev = std::vector<double> ( 3 ) ;
    ini::args.faux_bracket_ev[0] = -2.0 ;
    ini::args.faux_bracket_ev[1] = 0.0 ;
    ini::args.faux_bracket_ev[2] = 2.0 ;
    ui::_exposure_fusion() ;
  }

  ui::use_nonstandard_target = false ;

  ImGui::End() ;
}

extern bool reverse_drag ;
extern bool reverse_secondary_drag ;
extern bool show_error_dialog ;
extern bool snap_to_hq ;
extern bool snap_to_fusion ;
extern bool snap_to_stitch ;

void ShowBehaviourPanel()
{
  static panel_t panel ( imgui_reload_behaviour ,
                         show_behaviour_panel ,
                         "Behaviour Panel" ) ;

  if ( ! panel.header() )
    return ;

  ImGui::Text ( "This panel has settings for lux' overall behaviour." ) ;

  // ImGui::PushItemWidth(ImGui::GetFontSize() * -10);

  GUI_ELEMENT ( bool , auto_quality ,
    "switch automatic rendering quality on/off. Automatic rendering quality affects animated sequences (moving images) only. When it's on and detects that rendering isn't fast enough to provide a smooth animation, it lowers the resolution of the view. Once the viewer is at rest again, the full resolution 'kicks in' again." ) ;

  GUI_ELEMENT ( bool , reverse_drag ,
    "reverse the effect of primary mouse button click-drag gestures" ) ;

  GUI_ELEMENT ( bool , reverse_secondary_drag ,
    "reverse the effect of secondary mouse button click-drag gestures" ) ;

  GUI_ELEMENT ( bool , show_error_dialog ,
    "When a recoverable error occurs, show a dialog box and allow the user to continue or abort lux. The default behaviour is to ignore such errors." ) ;

  GUI_ELEMENT ( bool , show_status_line ,
    "Display a status line at the bottom of the viewer window which displays progress messages and, optionally, image metadata" ) ;

  GUI_ELEMENT ( bool , snap_to_hq ,
    "When the viewer is at rest, produce an image with the best available quality, e.g. using the 'quality' interpolator for 'ordinary' images, calculating exposure fusions for exposure series, and stitching panoramas into a seamless view. The latter two modes also depend on the next two options - see below. If this option is off, the viewer will 'stick to' the last image it displayed using the currently active 'fast' interpolation method." ) ;

  GUI_ELEMENT ( bool , snap_to_fusion ,
    "When displaying exposure stacks and the viewer is at rest, calculate an exposure fusion and display it. Switching this off will always show the 'stack parent' - the image with midde exposure - which is also displayed for animated sequences. This option will only have an effect if snap_to_hq is on as well. With this option set, an exposure fusion is calculated with lux' modified version of the Burt&Adelson image splining algorithm" ) ;

  GUI_ELEMENT ( bool , snap_to_stitch ,
    "When displaying unstitched panoramas (e.g. from a PTO file) and the viewer is at rest, calculate a panoramic image and display it. Switching this off will show a synoptic view with hard borders between the partial images in the place where lux will put the seams. With this option set, the images will be blended with lux' modified version of the Burt&Adelson image splining algorithm." ) ;

  static bool dummy ;

  static text_cli_gui_t extra_args
    ( panel , dummy , "extra arguments" ,
      "Here, you can pass arguments with the same syntax as the command line would accept them, so both short and long arguments are accepted. The are committed together with any other changes you made to the flags above. This is handy to apply arguments which are not accessible via GUI elements. Note, though, that some arguments won't work as expected - this is work in progress." ) ;

  panel.run() ;
}

// The next section offers 'immediate' controls, which need no 'Apply'.
// They more or less offer what can be done with the mouse and the
// keyboard, and as the implementation progresses, the functionality
// of the 'old' GUI will be available here as well. Since we have
// several panels and a powerful GUI library, control with the new
// GUI will be much more complete and accessible.
// The functionality itself is mainly affected by calling on_...
// and do_... functions in namespace ui, which affect acute and chronic
// effects. TODO: currently, all these functions and additionally some
// ui variables are explicitly declared here, it may be better to use
// a header.

extern sensor_settings_type sensor_settings ;
extern void on_magnifying_glass() ;
extern void on_blow_up() ;
extern void on_double_zoom() ;
extern void on_halve_zoom() ;
extern void on_vertical_fit() ;
extern void on_horizontal_fit() ;
extern void on_100_percent() ;
extern void do_zoom_in() ;
extern void do_zoom_out() ;
extern bool lock_vertical ;
extern void on_level_out() ;
extern void on_level_on_horizon() ;
extern void do_camera_left() ;
extern void do_camera_right() ;
extern double snappiness ;
extern void do_decrease_snappiness() ;
extern void do_increase_snappiness() ;
extern void do_rotate_clockwise() ;
extern void do_rotate_counter_clockwise() ;
extern void do_camera_up() ;
extern void do_camera_down() ;
extern void on_page_up() ;
extern void on_page_down() ;
extern void on_end() ;
extern void on_home() ;
extern void on_step_back() ;
extern void on_step_forward() ;
extern void on_one_quadrant() ;

// simple two-arrow-button construct to affect a decrease/increase
// using two 'chronic' ui functions - ui::do_... which perform
// actions corresponding to *held* keys. This is important for
// smooth operations of e.g. zoom or camera movement, which are
// realized by modulating the effect's first derivative.
// The implementation needs a bit of trickery, because the
// key repeat delay and repeat have to be modified for smooth
// operation and reset to the previous value afterwards.

void two_arrow ( std::function < void() > decrease ,
                 std::function < void() > increase ,
                 const char * label1 = "-##less#0" ,
                 const char * label2 = "+##more#0" )
{
  ImGuiContext& g = *GImGui;
  const float backup_repeat_delay = g.IO.KeyRepeatDelay;
  const float backup_repeat_rate = g.IO.KeyRepeatRate;

  g.IO.KeyRepeatDelay = 0.0f;
  g.IO.KeyRepeatRate = 1.0f / 200.0f ;

  ImVec2 button_sz ( 40 * ui::fgui , 30 * ui::fgui ) ;
  float spacing = ImGui::GetStyle().ItemInnerSpacing.x;
  ImGui::PushButtonRepeat(true);
  if (ImGui::Button(label1,button_sz))
    decrease() ;
  ImGui::SameLine(0.0f, spacing);
  if (ImGui::Button(label2,button_sz))
    increase() ;
  ImGui::PopButtonRepeat();
  g.IO.KeyRepeatRate = backup_repeat_rate;
  g.IO.KeyRepeatDelay = backup_repeat_delay;
}

// two_arrow_plus additionally displays the current value between
// the decrease and increase buttons. This value is editable, but
// it has to be committed with return to show an immediate effect.
// Even without the commit, the edit is effective, but it will only
// show when a new frame is rendered due to some other trigger.
// When the value is modified by the de/increase buttons, it depends
// on the decrease and increase function what the immediate effect
// is - typically they are do_... functions in namespace ui, which
// have immediately visible effects as well.

void two_arrow_plus ( std::function < void() > decrease ,
                      std::function < void() > increase ,
                      double & value ,
                      const char * label1 = "-##less#1" ,
                      const char * label2 = "+##more#1" )
{
  char label[24] ;
  sprintf ( label , "##%16p" , (void*) ( &value ) ) ;
  ImGuiContext& g = *GImGui;

  // we want smooth operation for these controls - some are used
  // for visible modifications, like zoom, and the normal key
  // repeat rate is to low, producing stutter. Also, the delay
  // is annoying, we want instant response here.

  float backup_repeat_delay = g.IO.KeyRepeatDelay;
  float backup_repeat_rate = g.IO.KeyRepeatRate;
  g.IO.KeyRepeatDelay = 0.0f;
  g.IO.KeyRepeatRate = 1.0f / 200.0f ;

  ImVec2 button_sz ( 40 * ui::fgui , 30 * ui::fgui ) ;
  ImVec2 wide_button_sz ( 60 * ui::fgui , 30 * ui::fgui ) ;
  float spacing = ImGui::GetStyle().ItemInnerSpacing.x;
  ImGui::PushButtonRepeat(true) ;
  
  if (ImGui::Button(label1,button_sz))
    decrease() ;

  ImGui::SameLine(0.0f, spacing);
  ImGui::SetNextItemWidth ( 80 * ui::fgui ) ;

  // for the value input, we switch back to the normal key repeat
  // and delay, so that edits succeed without surprises.

  g.IO.KeyRepeatRate = backup_repeat_rate;
  g.IO.KeyRepeatDelay = backup_repeat_delay;
  ImGui::InputDouble ( label , &value , 0.0 , 0.0 , "%6.2lf" ) ;

  // merely editing the input field has no visible effect, we
  // set change_draw if the user presses return while the input
  // is focused to force a redraw of the view.

  if ( ImGui::IsItemFocused() )
  {
    if ( ImGui::IsKeyPressed ( ImGuiKey_Enter ) )
    {
      change_draw = true ;
    }
  }

  // if, after editing the field, the user leaves it, we also emit
  // a change_draw.

  if ( ImGui::IsItemDeactivatedAfterEdit() )
  {
    change_draw = true ;
  }

  // for the increase button, we switch to the fast respeat and no
  // delay again

  ImGui::SameLine(0.0f, spacing);
  g.IO.KeyRepeatDelay = 0.0f;
  g.IO.KeyRepeatRate = 1.0f / 200.0f ;
  if (ImGui::Button(label2,button_sz))
    increase() ;

  // return to previous button repeat state, repeat rate and delay

  ImGui::PopButtonRepeat();
  g.IO.KeyRepeatRate = backup_repeat_rate;
  g.IO.KeyRepeatDelay = backup_repeat_delay;
}

extern bool show_again ;

void i_two_arrow_plus ( std::function < void() > act ,
                        int & value ,
                        const char * label1 = "-##less#2" ,
                        const char * label2 = "+##more#2" )
{
  char label[24] ;
  sprintf ( label , "##%16p" , (void*) ( &value ) ) ;
  ImGuiContext& g = *GImGui;
  int delta = ui::show_again ? 0 : 1 ;

  // we want smooth operation for these controls - some are used
  // for visible modifications, like zoom, and the normal key
  // repeat rate is to low, producing stutter. Also, the delay
  // is annoying, we want instant response here.

  // float backup_repeat_delay = g.IO.KeyRepeatDelay;
  // float backup_repeat_rate = g.IO.KeyRepeatRate;
  // g.IO.KeyRepeatDelay = 0.0f;
  // g.IO.KeyRepeatRate = 1.0f / 10.0f ;

  ImVec2 button_sz ( 40 * ui::fgui , 30 * ui::fgui ) ;
  ImVec2 wide_button_sz ( 60 * ui::fgui , 30 * ui::fgui ) ;
  float spacing = ImGui::GetStyle().ItemInnerSpacing.x;

  ImGui::Text ( "Subimage: " ) ;
  ImGui::SameLine(0.0f, spacing);

  ImGui::PushButtonRepeat(true) ;
  
  if (ImGui::Button(label1,button_sz))
  {
    if ( value > 0 )
      value -= delta ;
  }
  if ( ImGui::IsItemDeactivated() )
    act() ;
  
  ImGui::SameLine(0.0f, spacing);
  ImGui::SetNextItemWidth ( 60 * ui::fgui ) ;

  // for the value input, we switch back to the normal key repeat
  // and delay, so that edits succeed without surprises.

  // g.IO.KeyRepeatRate = backup_repeat_rate;
  // g.IO.KeyRepeatDelay = backup_repeat_delay;
  ImGui::DragInt ( label , &value , 1 ) ;

  // merely editing the input field has no visible effect, we
  // set change_draw if the user presses return while the input
  // is focused to force a redraw of the view.

  if ( ImGui::IsItemFocused() )
  {
    if ( ImGui::IsKeyPressed ( ImGuiKey_Enter ) )
    {
      if ( value < 0 )
        value = 0 ;
      act() ;
    }
  }

  // if, after editing the field, the user leaves it, we also emit
  // a change_draw.

  if ( ImGui::IsItemDeactivatedAfterEdit() )
  {
    act() ;
  }

  // for the increase button, we switch to the fast respeat and no
  // delay again

  ImGui::SameLine(0.0f, spacing);
  // g.IO.KeyRepeatDelay = 0.0f;
  // g.IO.KeyRepeatRate = 1.0f / 10.0f ;
  if (ImGui::Button(label2,button_sz))
  {
    value += delta ;
    // act() ;
  }
  if ( ImGui::IsItemDeactivated() )
    act() ;

  // return to previous button repeat state, repeat rate and delay

  ImGui::PopButtonRepeat();
  // g.IO.KeyRepeatRate = backup_repeat_rate;
  // g.IO.KeyRepeatDelay = backup_repeat_delay;
}

extern void do_brightness_down() ;
extern void do_brightness_up() ;
extern void do_black_down() ;
extern void do_black_up() ;
extern void do_white_down() ;
extern void do_white_up() ;
extern void on_reset_light() ;
extern void on_capture_wb() ;
extern bool capture_light ;
extern void on_reset_wb() ;
extern bool autopan ;
extern void on_toggle_autopan() ;
extern void on_reverse_autopan() ;
extern void do_decrease_autopan() ;
extern void do_increase_autopan() ;
extern void on_next_image() ;
extern void on_previous_image() ;
extern long grace_period ;
extern double slide_interval ;
extern bool magnify_on ;
extern double & blow_up ;
extern int subimages ;
extern int subimage ;
extern void do_aq_down() ;
extern void do_aq_up() ;
extern bool auto_quality ;

void ShowViewControlPanel()
{
  static panel_t panel ( imgui_reload_view_control ,
                         show_view_control_panel ,
                         "View Control Panel" ) ;

  if ( ! panel.header() )
    return ;

  int col2 = 200 * fgui , col3 = 400 * fgui , middle = 300 * fgui ;

  ImGui::TextWrapped ( "This panel has settings for the current view." ) ;
  ImGui::SameLine() ;
  HelpMarker (
    " Any changes you make here affect the viewer immediately, there is no need to commit the changes. Note that lux uses the notion of controlling the virtual camera, so 'right' in this context means 'move the virtual camera right', not 'move the content seen in the window to the right'. Some users find that confusing." ) ;

  ImVec2 button_sz ( 160 * ui::fgui , 30 * ui::fgui ) ;

  if ( ui::lock_vertical )
    ImGui::Text ( " pan " ) ;
  else
    ImGui::Text ( " yaw " ) ;
  ImGui::SameLine() ;
  two_arrow ( ui::do_camera_left ,
              ui::do_camera_right ,
#ifdef HAVE_MD_ICONS
              ICON_MD_ARROW_LEFT , ICON_MD_ARROW_RIGHT ) ;
#else
              "<" , ">" ) ;
#endif

  ImGui::SameLine ( col2 ) ;
  ImGui::Text ( "pitch" ) ;
  ImGui::SameLine() ;
  two_arrow ( ui::do_camera_up ,
              ui::do_camera_down ,
#ifdef HAVE_MD_ICONS
              ICON_MD_ARROW_UPWARD , ICON_MD_ARROW_DOWNWARD ) ;
#else
              "^" , "v" ) ;
#endif
  
  ImGui::SameLine ( col3 ) ;
  ImGui::Text ( "roll" ) ;
  ImGui::SameLine() ;
  two_arrow ( ui::do_rotate_clockwise ,
              ui::do_rotate_counter_clockwise ,
#ifdef HAVE_MD_ICONS
              ICON_MD_ROTATE_RIGHT , ICON_MD_ROTATE_LEFT ) ;
#else
              "cw" , "acw" ) ;
#endif
              
  ImGui::SameLine() ;
  HelpMarker ( 
    "smooth 3DOF camera orientation controls. These buttons work just like the cursor keys and R/Shift+R (Note: you can press and hold these buttons for the effect to last, don't just click them). The first pair affects a pan (rotation around the 'world' vertical axis) unless the vertical is 'unlocked' (using F2); then it affects a yaw relative to the camera's vertical axis. The next two pairs affect a yaw and roll, respectively. Note that the symbols follow the notion of controlling the virtual camera, which some users find confusing: they'd expect the right arrow to move the image to the right." ) ; // TODO: add a toggle

  const char * ap_on = "auto-pan on" ;
  const char * ap_off = "auto-pan off" ;

  if ( ImGui::Button ( autopan ? ap_off : ap_on , button_sz ) )
  {
    on_toggle_autopan() ;
    // if ( autopan )
    // {
    //   close_all_panels() ;
    //   ui::show_main_menu = false ;
    // }
  }
  ImGui::SameLine ( col2 ) ;
  ImGui::Text ( "speed:" ) ;
  ImGui::SameLine() ;
  two_arrow ( ui::do_decrease_autopan ,
              ui::do_increase_autopan ) ;
  
  ImGui::SameLine ( col3 ) ;

  if ( ImGui::Button ( "reverse" , button_sz ) )
    on_reverse_autopan() ;
  ImGui::SameLine() ;
  HelpMarker ( "automatic pan control. The button to the left switches automatic pan on/off (same as pressing space, or using the horizontal slap gesture), and the control in the middle controls the speed of the automatic pan (same as Shift+A/A). The button to the right reverses the direction (same as pressing 'O', the letter). Starting autopan will hide all GUI elements." ) ;
  
  if (    source.projection == SPHERICAL
       || source.projection == FISHEYE 
       || source.projection == STEREOGRAPHIC )
  {
    if ( ImGui::Button ( "Zenith" , button_sz ) )
      ui::on_page_up() ;
    ImGui::SameLine ( col2 ) ;
    if ( ImGui::Button ( "Nadir" , button_sz ) )
      ui::on_page_down() ;
    ImGui::SameLine() ;
    HelpMarker ( 
      "Point the virtual camera towards the Zenith, or Nadir, respectively. You can also use the PageUp/PageDown keys to the same effect." ) ;
  }

  else if ( source.projection == MOSAIC )
  {
    if ( ImGui::Button ( "Page Top" , button_sz ) )
      ui::on_home() ;
    ImGui::SameLine ( col2 ) ;
    if ( ImGui::Button ( "Page Middle" , button_sz ) )
    {
      ui::on_level_on_horizon() ;
      ui::on_horizontal_fit() ;
    }
    ImGui::SameLine ( col3 ) ;
    if ( ImGui::Button ( "Page Bottom" , button_sz ) )
      ui::on_end() ;
    ImGui::SameLine() ;
    HelpMarker ( 
      "In MOSAIC projection, these buttons are used for the 'Comic Book Mode'. 'Page Top' sets the view to the image's width and aligns on the images top margin, 'Page Bottom' does the same towards the image's bottom margin. This is like pressing 'Home' or 'End', respectively. 'Page Middle' lands you in the vertical center. All there buttons widen the view to fill the window horizontally." ) ;

    if ( ImGui::Button ( "Step Down" , button_sz ) )
      ui::on_page_down() ;
    ImGui::SameLine ( col2 ) ;
    if ( ImGui::Button ( "Step Up" , button_sz ) )
      ui::on_page_up() ;
    ImGui::SameLine() ;
    HelpMarker ( 
      "In MOSAIC projection, these buttons are used for the 'Comic Book Mode'. 'Step Down'/'Step Up' moves the view down/up by a third of the page height, which is the same as pressing PgDown/PgUp. The 'Step Down' and 'Step Up' buttons carry you on to the previous/next page as well. These two buttons do not change the vertical size of the view, so you can zoom out a bit to get more of the page into the view." ) ;
  }

  if ( ImGui::Button ( "step left" , button_sz ) )
    ui::on_step_back() ;
  ImGui::SameLine ( col2 ) ;
  if ( ImGui::Button ( "step right" , button_sz ) )
    ui::on_step_forward() ;
  ImGui::SameLine ( col3 ) ;
  if ( ImGui::Button ( "+1 Quadrant" , button_sz ) )
    ui::on_one_quadrant() ;
  ImGui::SameLine() ;
  HelpMarker ( 
    "pan the virtual camera in large steps. The first two buttons move the camera by roughly half the view's extent - same as pressing 'Shift+T''/T', the third button moves it by an entire quadrant (90 degrees) - same as pressing '4'." ) ;

  if ( ImGui::Button ( "level out" , button_sz ) )
    ui::on_level_out() ;
  ImGui::SameLine ( col2 ) ;
  if ( ImGui::Button ( "level on horizon" , button_sz ) )
    ui::on_level_on_horizon() ;
  ImGui::SameLine ( col3 ) ;
  ImGui::Checkbox ( "lock vertical" , &ui::lock_vertical ) ;
  ImGui::SameLine() ;
  HelpMarker ( 
    "The two buttons on the left set the camera roll to zero; the second one additionaly sets the pitch to zero as well, landing you with the assumed horizon in the view's vertical center. This checkbox on the right un/sets the 'vertical lock' for panning/yawing, just like pressing 'F2' toggles it. Normally you want the vertical locked, so that the view remains upright (or stays at the same tilt, as set by camera roll). Sometimes, like when navigating near the poles of a full spherical, this is inconvenient." ) ;

  ImGui::Separator() ;

  // TODO: rewrite shift logic. There should be a display of the
  // amount of shift, and the controls should be separate for the
  // fast and quality itp.

  if ( ImGui::Button ( "Shift Down" , button_sz ) )
    ui::on_shift_down() ;
  ImGui::SameLine ( col2 ) ;
  if ( ImGui::Button ( "Shift Up" , button_sz ) )
    ui::on_shift_up() ;
  ImGui::SameLine() ;
  HelpMarker ( 
    "lux normally uses b-spline interpolation. 'shifting' is a technique to 'pretend' that  the spline at hand is of a lower or higher degree than it was set up to be (e.g. the defaults of degree one for animated views and degree three for still images). If you use the Shift buttons when the viewer is at rest, the still image interpolator will be affected, and if you use them while there is an animation (e.g. an automatic pan) - or snap-to-hq is off, the moving image interpolator is affected. You can best observe the effect in high magnification. You can press 'S' or 'Shift+S' to the same effect. Shifting Up will increase the apparent spline degree, shifting down will lower it - down to degree zero, which is the same as nearest-neighbour interpolation. But keep in mind that, for example, shifting down a cubic spline to degree zero will show the cubic spline's coefficients - not the original image data. If you want that, choose spline degree zero in the 'Conditioning' Panel" ) ;

  ImGui::Separator() ;

  if ( ui::projection == FACET_MAP )
  {
    if ( ImGui::Button ( "Light Balance" , button_sz ) )
    {
      capture_light = true ;
      change_draw = true ;
    }
    ImGui::SameLine() ;
    HelpMarker ( "This button triggers a 'light balance': overlapping images are compared to see if they differ in brightness. The detected differences are used to modify the per-facet brightness values to levels which make the images equally bright in overlapping areas. You can also press Shift+L for the same effect. Depending on the input, this does not always work out perfectly - it works best with images in linear RGB, like linear TIFFs or openEXR images. If the input is a PTO file, usually, the per-facet brightness values will already be set to reasonable values (provided they were done with 'Photometric Optimization'), but you can still try this option to see if lux can improve on what's in the PTO file - it uses a different method. This procedure also works for image stacks - here, the overlap is more or less complete, and the calculation is especially precise. For panoramas, sufficient overlap is needed to get a good light balance. Note that the modified per-facet brightness values will not be stored back to the input, but they will be used for stitching and blending until you proceed to the next image." ) ;
  }

  if ( ImGui::Button ( "Reset Light" , button_sz ) )
  {
    on_reset_light() ;
  }
  ImGui::SameLine ( col2 ) ;
  if ( ImGui::Button ( "White Balance" , button_sz ) )
  {
    on_capture_wb() ;
  }
  ImGui::SameLine ( col3 ) ;
  if ( ImGui::Button ( "Reset W. Balance" , button_sz ) )
  {
    on_reset_wb() ;
  }
  ImGui::SameLine() ;
  HelpMarker ( "The button to the left resets the light values (same as F7). The button in the middle recalculates the white balance based on the entire view (like pressing 'W'). You may want to  zoom to an area which you want to come out grey - avoid white areas (like clouds), they may be white due to overexposure. The right button resets the white balance (like Shift+W)." ) ;

  ImGui::Text ( "brightness:" ) ;

  ImGui::SameLine ( col2 ) ;
  two_arrow_plus ( ui::do_brightness_down ,
                   ui::do_brightness_up ,
                   state.brightness ) ;

  ImGui::SameLine() ;
  HelpMarker ( "This control modifies brightness, which you can also affect with a horizontal secondary-button click-drag gesture, or with F5/F6." ) ;

  ImGui::Text ( "black point:" ) ;
  ImGui::SameLine ( col2 ) ;

  two_arrow_plus ( ui::do_black_down ,
                   ui::do_black_up ,
                   state.black_point ) ;

  ImGui::SameLine() ;
  HelpMarker ( "black point control. The black point is the brightness value in the source image which is translated to pure black in the view. Source image values below this threshold will also show black.You can also use F3 to the same effect, optionally with Shift to reverse the effect." ) ;

  ImGui::Text ( "white point" ) ;
  ImGui::SameLine ( col2 ) ;

  two_arrow_plus ( ui::do_white_down ,
                   ui::do_white_up ,
                   state.white_point ) ;

  ImGui::SameLine() ;
  HelpMarker ( "white point control. The white point is the brightness value in the source image which is translated to pure white in the view. Source image values above this value will also show white. You can also use F4 to the same effect, optionally with Shift to reverse the effect." ) ;
  ImGui::Separator() ;

  if (    source.projection == RECTILINEAR
       || source.projection == MOSAIC )
  {
    if ( ImGui::Button ( "fit vertically" , button_sz ) )
      ui::on_vertical_fit() ;
    ImGui::SameLine ( col2 ) ;
    if ( ImGui::Button ( "fit horizontally" , button_sz ) )
      ui::on_horizontal_fit() ;
    ImGui::SameLine ( col3 ) ;
    if ( ImGui::Button ( "1:1" , button_sz ) )
      ui::on_100_percent() ;
    ImGui::SameLine() ;
    HelpMarker ( 
      "To the left: Fit the image into the viewing window horizontally, or vertically. Same as pressing Y/Shift+Y. To the right: magnify image so that one source image pixel corresponds with one on-screen pixel, same as pressing 1" ) ;
  }

  static bool is_blown_up = false , is_magnified = false ;
  is_blown_up = ( blow_up != 1.0 ) ;
  is_magnified = magnify_on ;
  if ( ImGui::Checkbox ( "Magnify" , &is_magnified ) )
    ui::on_magnifying_glass() ;
  ImGui::SameLine ( col2 ) ;
  ImGui::SetNextItemWidth ( 160 * ui::fgui ) ;
  if ( ImGui::Checkbox ( "Blow Up" , &is_blown_up ) )
    ui::on_blow_up() ;
  ImGui::SameLine ( col3 ) ;
  ImGui::Text ( "Factor:" ) ;
  ImGui::SameLine() ;
  ImGui::SetNextItemWidth ( 140 * ui::fgui ) ;
  ImGui::InputDouble ( "##" ,
                       &sensor_settings.magnifying_glass_factor ,
                       0.1f , 1.0f , "%.1f" ) ;

  ImGui::SameLine() ;
  HelpMarker ( 
    "Toggle use of the 'Magnifying Glass'. the first button simply toggles a fixed magnification on/off (Same as pression 'I'), the second does almost the same thing but keeps the current zoom level's associated pyramid level (same as pressing 'Shift+I'). The input field to the right lets you change the magnification factor used for both magnifying glass modes." ) ;

  if ( ImGui::Button ( "zoom X 1/2" , button_sz ) )
    ui::on_halve_zoom() ;
  ImGui::SameLine ( col2 ) ;
  if ( ImGui::Button ( "zoom X 2" , button_sz ) )
    ui::on_double_zoom() ;
  ImGui::SameLine ( col3 ) ;
  two_arrow_plus ( ui::do_zoom_out ,
                   ui::do_zoom_in ,
                   state.zoom_factor ) ;
  ImGui::SameLine() ;
  HelpMarker ( 
    "Set the zoom factor - the two buttons on the left are the same as pressing '2' or '3'. this is a convenient way to quickly zoom in/out, and often used after first right-clicking on the point of interest to move it to the center, because this zoom operation is center-focused. The decrease/increase widget to the right mediates a smooth zoom, like pressing +/- on the Numpad or other zoom keys; the value display in it's middle shows the current value." ) ;

  two_arrow_plus ( ui::do_decrease_snappiness ,
                   ui::do_increase_snappiness ,
                   ui::snappiness ) ;
  ImGui::SameLine() ;
  ImGui::Text ( "snappiness" ) ;
  ImGui::SameLine() ;
  HelpMarker ( 
    "de/increase 'snappiness'. This is an overall setting affecting how strongly lux reacts to 'chronic' input, like the camera orientation controls below." ) ;

  two_arrow_plus ( ui::do_aq_down ,
                   ui::do_aq_up ,
                   ui::state.moving_image_scaling ) ;
  ImGui::SameLine() ;
  ImGui::Text ( "animation quality" ) ;
  ImGui::SameLine ( col3 ) ;
  ImGui::Checkbox ( "automatic quality" , & auto_quality ) ;
  ImGui::SameLine() ;
  HelpMarker ( 
    "de/increase 'animation quality', or set animation quality to automatic. This is an overall setting affecting decrease of resolution for animated sequences. Note that modifying this setting won't have a lasting effect when automatic rendering quality is on. Note also that to set automatic rendering quality sessionwide, please use the general settings panel - the setting here only affects the current view. The numerical value on the left is a scaling factor; lux renders frames for animated sequences scaled with this factor. To toggle the state of automatic rendering quality, you can also press 'G', and to affect the factor directly, use 'M' or 'Shift+M', respectively" ) ;

  ImGui::Separator() ;

  // ImVec2 button_sz ( 130 * ui::fgui , 30 * ui::fgui ) ;

  if ( ImGui::Button ( "previous" , button_sz ) )
    ui::on_previous_image() ;
  ImGui::SameLine ( col2 ) ;
  if ( ImGui::Button ( "next" , button_sz ) )
    ui::on_next_image() ;
  ImGui::SameLine() ;
  HelpMarker ( "Proceed to the previous/next image in the image queue. Note that this will not pick the next image in the folder of the current image, but the next image which was selected during the last file-select - or the next image passed on the command line. You can also use the Tab/Shift+Tab keys for the same purpose." ) ;

  ImGui::SameLine ( col3 ) ;
  if ( ImGui::Button ( "load images" , button_sz ) )
    ui::on_load_images() ;
  ImGui::SameLine() ;
  HelpMarker ( "open a file-select dialog to load more images into the image queue. These images will be enqueued after the current image - images already in the queue will remain there and appear after the newly enqueued ones." ) ;

  // for the slide show, we have a three-way control - on top of being
  // on or off, it can be 'suspended' - when the user interacted with
  // the view or went back to the previous image.

  const char * sl_txt ;
  if ( ui::slideshow_on )
  {
    if ( ui::grace_period < 0 )
    {
      sl_txt = "resume slide show" ;
    }
    else
    {
      sl_txt = "stop slide show" ;
    }
  }
  else
  {
    sl_txt = "start slide show" ;
  }

  if ( ImGui::Button ( sl_txt , button_sz ) )
  {
    if ( ui::slideshow_on )
    {
      if ( ui::grace_period < 0 )
      {
        ui::on_next_image() ;
      }
      else
      {
        ui::slideshow_on = false ;
      }
    }
    else
    {
      ui::slideshow_on = true ;
      ui::on_next_image() ;
    }
  }

  ImGui::SameLine ( col2 ) ;
  // ImGui::SetNextItemWidth ( 140 * ui::fgui ) ;
  // ImGui::InputDouble ( "slide interval" , 
  //                       &ui::slide_interval ,
  //                       0.1f , 1.0f , "%.1f" ) ;
  ImGui::Text ( "Interval:" ) ;
  ImGui::SameLine ( col3 ) ;
  two_arrow_plus ( []() { ui::slide_interval
                   = std::max ( 0.1 , ui::slide_interval -= .01 ) ; } ,
                   []() { ui::slide_interval += .01 ; } ,
                   ui::slide_interval ) ;
  ImGui::SameLine() ;
  HelpMarker ( "slide show controls. The button to the left controls the slide show mode. the button changes it's function depending on the mode the slide show is in. If the slide show is off, it offers you to start it. Once the slide show is on, and you interact with the view or go back to the previous image, the slide show is suspended, and you are offered the option to resume it - you can simply Tab to the next image to the same effect. If the slide show is on and not suspended, the button offers you to switch it off.\nThe control to the right sets the duration from one slide to the next." ) ;

  if ( ui::subimages > 1 )
  {
    ImGui::Separator() ;
    i_two_arrow_plus ( & ui::on_show_again , ui::subimage ) ;
    if ( ui::subimage >= ui::subimages )
      ui::subimage = ui::subimages - 1 ;
    ImGui::SameLine() ;
    HelpMarker ( "Pick a specific subimage of the file. This option is only present for input with more than one subimage, e.g. videos. This is a new feature and still experimental." ) ;
  }

  panel.run ( true , false ) ;
}

void ShowLuxMainMenuBar()
{
  if ( show_main_menu && ImGui::BeginMainMenuBar() )
  {
    if (ImGui::BeginMenu("File"))
    {
      ShowLuxMenuFile();
      ImGui::EndMenu();
    }
    if (ImGui::BeginMenu("Viewer"))
    {
      ShowLuxMenuViewer();
      ImGui::EndMenu();
    }
    ImGui::EndMainMenuBar();
  }
}

const char * popup_title ;
char * popup_message = NULL ;
const char * popup_left_button_text ;
const char * popup_right_button_text ;
std::function < void() > on_popup_left ;
std::function < void() > on_popup_right ;

// show_popup displays a modal dialog on top of the current view.
// This function can't be used if there is no display window, e.g.
// to show dialogs before the main window was opened. The function
// results in an ImGui modal popup with two buttons which trigger
// two possible functions. Contrary to the modal dialog via tfd,
// this one needs the main event loop to function, and does not
// return user input - the functions passed in have to trigger
// whatever action is needed as response to the user's click on
// one of the buttons. They should execute quickly, otherwise the
// main thread is blocked.

void show_popup ( const char * title ,
                  const char * message ,
                  const char * left_text ,
                  const char * right_text ,
                  std::function < void() > on_left = []() { } ,
                  std::function < void() > on_right = []() { } )
{
  assert ( show_modal_dialog == false ) ;
  assert ( popup_message == NULL ) ;

  show_modal_dialog = true ;
  popup_title = title ;
  std::size_t len = std::strlen ( message ) ;
  popup_message = new char [ len + 1 ] ;
  std::strcpy ( popup_message , message ) ;
  popup_left_button_text = left_text ;
  popup_right_button_text = right_text ;
  on_popup_left = on_left ;
  on_popup_right = on_right ;
  change_draw = true ;
}

// just to test the functionaltiy - press Shift+V

void on_test_popup()
{
  show_popup ( "TITLE" ,
               "this is a very long message with lots of blah blah blah blah blah blah blah blah blah blah blah blah" ,
               "EXIT" , "IGNORE" ,
               on_terminate , []() { } ) ;
}

// implementation of modal dialog display. This is adapted from
// the code im the ImGui demo.

void ShowPopup()
{
  if ( ! show_modal_dialog )
    return ;

  ImGui::OpenPopup(popup_title);

  // Always center this window when appearing
  ImVec2 center = ImGui::GetMainViewport()->GetCenter();
  ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));

  if (ImGui::BeginPopupModal(popup_title, NULL, ImGuiWindowFlags_AlwaysAutoResize))
  {
    ImGui::TextWrapped ( "%s" , popup_message ) ;
    ImGui::Separator();

    if (ImGui::Button(popup_left_button_text, ImVec2(120, 0)))
    {
      ImGui::CloseCurrentPopup();
      show_modal_dialog = false ;
      delete[] popup_message ;
      popup_message = NULL ;
      change_draw = true ;
      on_popup_left() ;
    }
    ImGui::SetItemDefaultFocus();
    ImGui::SameLine();
    
    if (ImGui::Button(popup_right_button_text, ImVec2(120, 0)))
    {
      ImGui::CloseCurrentPopup();
      show_modal_dialog = false ;
      delete[] popup_message ;
      popup_message = NULL ;
      change_draw = true ;
      on_popup_right() ;
    }
    ImGui::EndPopup();
  }
}

void ShowImGui()
{
  ShowLuxMainMenuBar() ;
  ShowViewControlPanel() ;
  ShowGeometryPanel() ;
  ShowConditioningPanel() ;
  ShowExportPanel() ;
  ShowBehaviourPanel() ;
  if ( ui::show_imgui_demo )
    ImGui::ShowDemoWindow ( &ui::show_imgui_demo ) ;

  ShowPopup() ;
}


} ; // namespace ui


