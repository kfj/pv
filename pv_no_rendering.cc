/************************************************************************/
/*                                                                      */
/*          pv - a viewer for panoramic images                          */
/*                                                                      */
/*          Copyright 2016 - 2023 by Kay F. Jahnke                      */
/*                                                                      */
/*  The git repository for this software is at                          */
/*                                                                      */
/*  https://bitbucket.org/kfj/pv                                        */
/*                                                                      */
/*  Please direct questions, bug reports, and contributions to          */
/*                                                                      */
/*  kfjahnke+pv@gmail.com                                               */
/*                                                                      */
/*  pv is free software: you can redistribute it and/or modify          */
/*  it under the terms of the GNU General Public License as published   */
/*  by the Free Software Foundation, either version 3 of the License,   */
/*  or (at your option) any later version.                              */
/*                                                                      */
/*  pv is distributed in the hope that it will be useful,               */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       */
/*  GNU General Public License for more details.                        */
/*                                                                      */
/*  You should have received a copy of the GNU General Public License   */
/*  along with pv.  If not, see <http://www.gnu.org/licenses/>.         */
/*                                                                      */
/************************************************************************/

/*! \file pv_no_rendering.cc

    \brief pv - a panorama viewer

    For documentation, please see the bitbucket repository at

    https://bitbucket.org/kfj/pv

    This file holds all code which is not directly involved
    with rendering frames, and can do without architecture-specific
    compiler flags. Here, the bulk of the code is concerned with
    the UI. This file makes no references to Vc or vspline which
    the rendering code in pv_rendering.cc relies on, so it can be
    compiled with a simple compiler invocation like:

    clang++ -c -Ofast -std=c++11 -pthread \
      pv_no_rendering.cc -o pv_no_rendering.o
*/

#include <stack>
#include <random>
#include <algorithm>
#ifdef USE_OIIO
#include "pv_metadata_oiio.h"
#else
#include "pv_metadata.h"
#endif
#include "pv_version.h"
#include "pv_common.h"
#include "pv_initialize.h"

// variable to hold the sessionwide parameterization while
// overrides are active.

ini::state_type argument_backup ;

// use tinyfiledialogs to implement file select dialogs

#define USE_TINYFILEDIALOGS

// employ Dear ImGui for the GUI. This activates Imgui-related
// code. To actually see ImGui, the CL option legacy_gui has to
// be off.

#define USE_IMGUI

// when running on Gnome, every now and then, when switching to
// Fullscreen mode, the window shows a white bar on top like an
// empty title bar. The system sends a resize event with the
// actual window size (fullscreen minus the white bar), so my
// workaround is to catch this event and react to it by
// re-creating the window with Fullscreen style. This produces
// a bit of flicker, especially if it happens repeatedly, but
// otherwise it solves the problem.

#define WHITE_BAR_BUG_WORKAROUND

// flavour_set is a vector of flavour_type objects.
// these objects have the details of the flavour-specific code,
// e.g. metadata like the ISA name, and a pointer to a dispatcher.
// It is unique for the entire program, hence it lives here
// in the TU holding 'main'

std::vector < flavour_type > flavour_set ;

// #include vspline headers from the vspline subtree of pv.git, rather
// than expecting vspline to be installed. This makes distribution easier,
// because pv.git now contains all the relevant vspline headers and version
// conflicts become less likely - provided the subtree is updated correctly.
// The exception here is vspline/thread_pool.h which has no ISA-specific
// code. We use a common thread pool for all of pv, and it lives here:

#define VSPLINE_EXTERN_THREAD_POOL

#include "vspline/thread_pool.h"

// we compile with -DVSPLINE_EXTERN_THREAD_POOL, to use only one thread
// pool for the whole program. Here's where it lives:

namespace vspline_threadpool
{
  thread_pool common_thread_pool ;
} ;

// This will be set during start-up. some functions behave differently
// depending on whether they are executed by the main thread or
// another one, e.g. program abortion due to an unrecoverable error:
// in the main thread, estd::exit is called, in other threads an
// exception is raised.

std::thread::id main_thread_id ;

// Frame generation is done in a separate thread. We use a pointer
// because the rendering thread is only launched after some initial
// state - like, which ISA to use - has been established, so the
// thread can't be created right at program start. The thread is
// stopped and joined at program termination via an atexit handler,
// so we needn't shut it down manually and can call std::exit
// anywhere in the main thread without the rendering thread staying
// alive and 'dangling'. see shut_down_rendering for the handler

std::thread * p_rendering_thread = nullptr ;

// for communication between the main thread and the frame generating
// thread we use a bunch of queues, a flag, and mutexes protecting them:

bool stay_alive = true ;

std::mutex job_mutex ;
std::queue < job_type* > job_queue ;
std::atomic < bool > aborted ;

std::mutex rendering_mutex ;
std::condition_variable rendering_cv ;

std::mutex content_mutex ;
std::condition_variable content_cv ;

std::mutex frame_mutex ;
std::queue < job_type* > frame_queue ;

std::mutex rgb_mutex ;
std::queue < rgba_image_type* > rgb_queue ;

std::atomic < bool > status_flag ;
std::mutex status_mutex ;

std::atomic < bool > error_flag ( false ) ;
std::string fatal_error ;

int status_building = 0 ;
bool status_light_balancing = false ;
std::string now_loading ;

// jobs_pending holds the number of currently active rendering jobs.
// During animated sequences, this will be one, otherwise zero, plus
// any stitches/exposure fusions/snapshots being processed in the
// background. If jobs_busy() returns zero, the viewer is idle.

int jobs_pending = 0 ;
std::atomic < int > snapshots_pending ( 0 ) ;

void job_begins()
{
  std::lock_guard < std::mutex > lk ( job_mutex ) ;
  ++jobs_pending ;
}

int jobs_busy()
{
  std::lock_guard < std::mutex > lk ( job_mutex ) ;
  return jobs_pending ;
}

void job_ends()
{
  {
    std::lock_guard < std::mutex > lk ( job_mutex ) ;
    if ( jobs_pending > 0 )
      --jobs_pending ;
  }
  {
    std::lock_guard < std::mutex > lk ( rendering_mutex ) ;
  }
  rendering_cv.notify_one() ;
}

void push_frame ( job_type * p_job )
{
  frame_mutex.lock() ;
  frame_queue.push ( p_job ) ;
  frame_mutex.unlock() ;
  std::lock_guard < std::mutex > lk ( content_mutex ) ;
  content_cv.notify_one() ;
}

#ifdef USE_MEMLOG

std::map < void* , mem_node_type > memlog_set ;

#endif

memlogger_type memlog ;

// helper function using tinyfiledialogs to select input files

extern std::vector < std::string > select_files ( std::string ) ;

// for each (vectorization) architecture we want to support, we #define
// a corresponding PV_ARCH. This is used as a separate namespace holding
// an architecture-specific version of the rendering code.
// access to the architecture-specific code is via 'dispatch.h', which
// provides an architecture-specific class 'dispatch' in each of the
// architecture-specific namespaces. These dispatch classes inherit
// from a common base class, where the architecture-specific functions
// are coded as pure virtual member functions, whereas the derived
// dispatch objects override them with architecture-specific code.

#define PV_ARCH PV_AVX2
#include "dispatch.h"
#undef PV_ARCH

#define PV_ARCH PV_AVX
#include "dispatch.h"
#undef PV_ARCH

#define PV_ARCH PV_PLAIN
#include "dispatch.h"
#undef PV_ARCH

// TODO: I'm unsure about which AVX512 flavour to use; for now, I only compile
// AVX512 code with -mavx512f, which I think covers the 'common' functionality

#define PV_ARCH PV_AVX512f
#include "dispatch.h"
#undef PV_ARCH

// next we define an architecture-specific dispatch object for each of
// the architectures we support:

// static const PV_AVX2::dispatch avx2_dispatch ;
// static const PV_AVX::dispatch avx_dispatch ;
// static const PV_PLAIN::dispatch plain_dispatch ;
// static const PV_AVX512f::dispatch avx512f_dispatch ;

// Vc's cpuid.h will detect the CPU's vector unit for us. This is used
// for automatic dispatching to the ISA used by the CPU running this
// program; 'manual' dispatching can be done without recourse to Vc

#ifdef USE_VC
#include <Vc/cpuid.h>
#endif

// When using highway, we need highway.h for the equivalent.

#ifdef USE_HWY
#include <hwy/highway.h>
#endif

bool is_viable()
{
  return true ;
}

bool has_avx()
{
#ifdef USE_VC
  return Vc::CpuId::hasAvx() ;
#else
  return false ;
#endif
}

bool has_ssse3()
{
#if defined USE_VC
  return Vc::CpuId::hasSsse3() ;
#elif defined USE_HWY
  auto trg = hwy::SupportedTargets() ;
  return ( trg & HWY_SSSE3 ) ;
#else
  return false ;
#endif
}

bool has_sse42()
{
#if defined USE_VC
  return Vc::CpuId::hasSse42() ;
#elif defined USE_HWY
  auto trg = hwy::SupportedTargets() ;
  return ( trg & HWY_SSE4 ) ;
#else
  return false ;
#endif
}

bool has_avx2()
{
#if defined USE_VC
  return Vc::CpuId::hasAvx2() ;
#elif defined USE_HWY
  auto trg = hwy::SupportedTargets() ;
  return ( trg & HWY_AVX2 ) ;
#else
  return false ;
#endif
}

bool has_avx512f()
{
#if defined USE_VC
  return Vc::CpuId::hasAvx512f() ;
#elif defined USE_HWY
  auto trg = hwy::SupportedTargets() ;
  return ( trg & HWY_AVX3 ) ;
#else
  return false ;
#endif
}

const dispatch * auto_dispatcher = nullptr ;

#include "fileio.h"

fileio::image_info open_image_file ( const std::string & filename )
{
  // let's try and open the file to see if it's there at all
  // and we have permission to open it.
  {
    std::ifstream ifs ( filename ) ;

    if ( ! ifs.good() )
    {
      // no good file

      std::string error ( "failed to access image file: " ) ;
      error += filename ;
      error += "\nmissing file or insufficient permissions." ;
      abort_lux ( error ) ;
    }
  }

  // okay, it's there, let's check whether it's an image file
  // vigraimpex can handle

  // if ( ! vigra::isImage ( filename.c_str() ) )
  // {
  //   abort_lux
  //     (   std::string ( "unrecoverable error processing: " )
  //       + filename
  //       + ":\nlibvigraimpex says that this file is not an image."
  //       + "\nis this a valid image file?"
  //     ) ;
  // }

  if ( ! fileio::is_image ( filename ) )
  {
    abort_lux
      (   std::string ( "unrecoverable error processing: " )
        + filename
        + ":\nOpenImageIO says that this file is not an image."
        + "\nis this a valid image file?"
      ) ;
  }

  // vigraimpex likes the file, let's access it

  try
  {
    fileio::image_info imageInfo ( filename.c_str() ) ;
    return imageInfo ;
  }
  catch ( ... )
  {
    abort_lux
      (   std::string ( "unrecoverable error processing: " )
        + filename
        + ":\nan exception occured when accessing this file"
        + "\nis this a valid image file?"
      ) ;
  }

  // just for syntactic correctness, can't be reached

  return fileio::image_info ( "" ) ;
}

const dispatch * dispatcher = nullptr ;

#include <csignal>

#include <SFML/Graphics.hpp>

#ifdef USE_IMGUI

// necessary for ImGui::*, imgui-SFML.h doesn't include imgui.h:
#include "imgui/imgui.h"
// for ImGui::SFML::* functions and SFML-specific overloads
#include "imgui-SFML/imgui-SFML.h"

// #define HAVE_MD_ICONS
#ifdef HAVE_MD_ICONS
// available from: https://github.com/juliettef/IconFontCppHeaders
#include "IconsMaterialDesign.h"
#endif

#endif // #ifdef USE_IMGUI

// for consistency, a global variable holding the first available
// full-screen video mode (set in main())

sf::VideoMode desktop ;

pv_clock::time_point program_start = pv_clock::now() ;

int job_type::_serial = 0 ;

double cos3d ( const point_3d_d_type & a , const point_3d_d_type & b )
{
  return ( dot ( a , b ) / ( norm ( a ) * norm ( b ) ) ) ;
}

double angle3d ( const point_3d_d_type & a , const point_3d_d_type & b )
{
  return acos ( cos3d ( a , b ) ) ;
}

// convert a polar coordinate given as azimuth and elevation (assuming
// r == 1) into pv-specific 3D coordinates. Azimuth is interpreted as
// a clockwise angle from the forward axis (1,0,0), increasing to
// the right, and elevation increases upwards from the forward axis.

point_3d_d_type ae_to_cartesian ( double azimuth , double elevation )
{
  point_3d_d_type result ;
  result[0] = cos ( azimuth ) * cos ( elevation ) ; // pv's z axis (front)
  result[1] = sin ( azimuth ) * cos ( elevation ) ; // pv's x axis (right)
  result[2] = - sin ( elevation ) ;                 // pv's y axis (down)
  return result ;
}

point_3d_d_type polar_to_cartesian ( double radius , double angle )
{
  point_3d_d_type result ;
  result[0] = cos ( radius ) ;                   // pv's z axis (front)
  result[1] = sin ( radius ) * sin ( angle ) ;   // pv's x axis (right)
  result[2] = - sin ( radius ) * cos ( angle ) ; // pv's y axis (down)
  return result ;
}

/// The 'extent' of a source image is a metric in radians, giving
/// the distance of the four margins to image center. Usually the
/// image is centered, and then x0 == -x1 and y0 == -y1.
/// But if the image is off-center (e.g. when pv was invoked with
/// -x or -y parameters) the values will differ.
/// set_extent calculates and sets the 'extent' member of class
/// source_type. class source_type does not have a c'tor, so this
/// function must be used after a source_type object is set up.
/// or whenever hfov, vfov, hofs or vofs have changed.

void set_extent ( source_type & source )
{
  double ax ,  // part of image width (dx) left from center
         ay ,  // part of image height (dy) above center
         bx ,  // part of image width (dx) right of center
         by ,  // part of image height (dy) below center
         alpha_x , // corresponding angles,
         beta_x ,  // alpha_x <-> a_x, beta_x <-> b_x etc.
         alpha_y ,
         beta_y ;

  // alpha_x and beta_x are the angles from left margin to center and center
  // to right margin, respectively. alpha_x + hofs == pi

  alpha_x = M_PI - source.hofs ;
  beta_x = source.hfov - alpha_x ;

  // XXX_y is the angle from top margin to center and center to bottom
  // margin, respectively. When the image is vertically centered, alpha_y
  // and beta_y come out equal.

  alpha_y = M_PI - source.vofs ;
  beta_y = source.vfov - alpha_y ;

  // we also calculate the minimal and maximal 2D coordinates which
  // the interpolator will be able to handle - these are also 'draped'
  // coordinates, in unit sphere radii.

  switch ( source.projection )
  {
    case SPHERICAL:
    case FISHEYE:
    case MOSAIC:
    {
      ax = alpha_x ;
      bx = beta_x ;

      ay = alpha_y ;
      by = beta_y ;
      break ;
    }
    case CYLINDRIC:
    {
      ax = alpha_x ;
      bx = beta_x ;

      ay = tan ( alpha_y ) ;
      by = tan ( beta_y ) ;
      break ;
    }
    case RECTILINEAR:
    {
      ax = tan ( alpha_x ) ;
      bx = tan ( beta_x ) ;

      ay = tan ( alpha_y ) ;
      by = tan ( beta_y ) ;
      break ;
    }
    case STEREOGRAPHIC:
    {
      ax = 2.0 * tan ( alpha_x / 2.0 ) ;
      bx = 2.0 * tan ( beta_x / 2.0 ) ;

      ay = 2.0 * tan ( alpha_y / 2.0 ) ;
      by = 2.0 * tan ( beta_y / 2.0 ) ;
      break ;
    }
    default:
    {
      abort_lux ( "unknown source image projection" ) ;
      break ;
    }
  }

  source.extent = { - ax , bx , -ay , by } ;

  switch ( source.projection )
  {
    case SPHERICAL:
    case FISHEYE:
    case CYLINDRIC:
      source.x_step = source.hfov / source.width ;
      break ;
    case RECTILINEAR:
      source.x_step = 2.0 * tan ( source.hfov / 2.0 ) / source.width ;
      break ;
    case MOSAIC: // TODO doublecheck if this is correct for MOSAIC
      source.x_step = source.hfov / source.width ;
      break ;
    case STEREOGRAPHIC:
      source.x_step = 4.0 * tan ( source.hfov / 4.0 ) / source.width ;
      break ;
    default:
    {
      abort_lux ( "unknown source image projection" ) ;
      break ;
    }
  }

  // get the extent in image coordinates. This is the range
  // of coordinates inside which the image interpolator will
  // yield correct values. Since we're using reflect boundary
  // conditions, and we've also settled on using the same
  // interval for periodic cases, we can now calculate the
  // range for all images alike. This is the range which
  // corresponds precisely to the field of view. Note that
  // there was a change in vspline to calculate the lower
  // and upper limits of a periodic spline's range to use
  // these modified values, which is used to form domains
  // with splines as argument, which accesses their limits.

  // we now use the same horizontal range for 360 degree images
  // in order to center the head-on view on the image's center.
  // Previously, I used this extent:

  // if ( full_width )
  // {
  //   iextent.x0 = 0.0 ;
  //   iextent.x1 = width ;
  // }

  source.iextent.x0 = -0.5 ;
  source.iextent.x1 = source.width - 0.5 ;

  source.iextent.y0 = -0.5 ;
  source.iextent.y1 = source.height - 0.5 ;

}

double apply_rise ( source_type & source , double rise )
{
  double lower_limit , upper_limit ;

  if (    source.projection == FISHEYE
       || source.projection == STEREOGRAPHIC )
  {
    lower_limit = 0.0 ;
    upper_limit = 2.0 * M_PI ;
  }
  else if (    source.projection == RECTILINEAR
            || source.projection == SPHERICAL
            || source.projection == CYLINDRIC )
  {
    lower_limit = M_PI_2 ;
    upper_limit = M_PI + M_PI_2 ;
  }
  else
  {
    // for other projections, no effect
    return 0.0 ;
  }

  auto proposed_vofs = source.vofs + rise ;

  if ( proposed_vofs < lower_limit )
    rise = lower_limit - source.vofs ;

  else if ( proposed_vofs + source.vfov > upper_limit )
    rise = upper_limit - ( source.vfov + source.vofs ) ;

  if ( rise )
  {
    source.vofs += rise ;
    set_extent ( source ) ;
  }

  return rise ;
}

// The next section of code provides the user interface. This section starts with
// objects holding the state of the user interface. The UI interacts with these
// objects until all user interaction is collected for one cycle of operation,
// then it moves the information which is needed by the generate thread into a
// job_type object and enqueues that to be processed. So the objects used by the
// UI and the generate thread are separated; the only way from one to the other
// is via the core of the event loop which fills the job_type object.

typedef vigra::Quaternion<double> qd_t ;


// we need to access this from a few of the functions just down from here:

namespace ui
{
  sensor_settings_type sensor_settings ;
  std::string font_filename ;
  float fgui = 1.0f ;

  bool show_modal_dialog = false ;
  bool show_imgui = false ;
  bool show_imgui_demo = false ;
  bool show_main_menu = false ;
  bool show_geometry_panel = false ;
  bool show_conditioning_panel = false ;
  bool show_export_panel = false ;
  bool show_behaviour_panel = false ;
  bool show_view_control_panel = false ;

  extern void close_all_panels() ;
} ;

// In pv we differentiate conceptually between 'chronic' and 'acute' user input.
// chronic input is input which is present for some time, like, while a key is
// held pressed down and is meant to produce some steady change, like panning
// or zooming. acute input is input which changes the state in some discrete way,
// like halting or continuing a pan. chronic input affects 'change', which takes
// on a specific value throughout the presence of the chronic input, but is only
// modulated onto 'state' whenever a job_type object is created. By this mechanism
// state changes smoothly.
// 'chronic' user input is processed by the next few routines. Here, 'state'
// holds the current state of the UI, while 'change' holds the intended change
// of this state. Note how here 'state' is constant, since it will not be
// modified by these routines, but at times is used to adapt the magnitude of
// the change. 'intensity' is used to attenuate the effect.
// Often there is some 'leverage' factor which modifies the magnitude of the
// change. These factors have evolved and may not be optimal for all tastes and
// systems. TODO: make them configurable

void turn ( const display_type & state , display_type & change , double intensity )
{
  // user affects a camera roll. positive intensity is clockwise
  double roll = - intensity ;
  quaternion_type q_roll ( cos ( roll / 2.0 ) , sin ( roll / 2.0 ) , 0.0 , 0.0 ) ;
  change.orientation *= q_roll ;
  change.dr += roll ;
}

void tilt ( const display_type & state , display_type & change , double intensity )
{
  // when magnifying glass is active, adapt intensity accordingly
  intensity /= ui::sensor_settings.magnifying_glass ;
  // user affects a camera tilt. positive intensity is upwards
  double pitch = intensity ;
  quaternion_type q_pitch ( cos ( pitch / 2.0 ) , 0.0 , sin ( pitch / 2.0 ) , 0.0 ) ;
  change.orientation *= q_pitch ;
  change.dy += pitch ;
}

void tilt_sensor ( const display_type & state , display_type & change ,
                   double vertical , double horizontal )
{
  // user affects a sensor tilt.
  change.sensor_tilt_h = horizontal ;
  change.sensor_tilt_v = vertical ;
}

void pan ( const display_type & state , display_type & change , double intensity )
{
  // when magnifying glass is active, adapt intensity accordingly
  intensity /= ui::sensor_settings.magnifying_glass ;
  // user affects a camera pan. positive intensity is towards the right
  double pan = - intensity ;
  quaternion_type q_pan ( cos ( pan / 2.0 ) , 0.0 , 0.0 , - sin ( pan / 2.0 ) ) ;
  change.orientation *= q_pan ;
  change.dx += pan ;
}

void yaw ( const display_type & state , display_type & change , double intensity )
{
  // when magnifying glass is active, adapt intensity accordingly
  intensity /= ui::sensor_settings.magnifying_glass ;
  // user affects bias-relative yaw. positive intensity is towards the right.
  // note how this differs from pan() above.
  double yaw = - intensity ;
  quaternion_type q_yaw ( cos ( yaw / 2.0 ) , 0.0 , 0.0 , - sin ( yaw / 2.0 ) ) ;
  change.preadjust = q_yaw * change.preadjust ;
  change.dx += yaw ;
}

void rise ( const display_type & state , display_type & change , double intensity )
{
  // when magnifying glass is active, adapt intensity accordingly
  intensity /= ui::sensor_settings.magnifying_glass ;
  // user affects a rise/drop of the viewpoint.
  change.rise = intensity ;
}

void zoom ( const display_type & state , display_type & change ,
            double intensity , double zoom_min )
{
  // user affects a camera zoom. positive intensity magnifies the image.
  // the closer zoom_factor comes to ui::zoom_min, which corresponds with 180 degrees
  // hfov, the slower the change becomes, so that 180 degrees is never reached.
  // there is no upper limit for zoom.

  // check how far away we are from ui::zoom_min. When zooming out (positive intensity)
  // we make sure we're not 'caught in the trough': if zoom_factor is very near
  // or even equal to ui::zoom_min, we'd never get it to rise otherwise.

  double clearance = state.zoom_factor - zoom_min ;
  if ( intensity > 0 && clearance < .01 )
    clearance = .01 ;

  change.zoom_factor = intensity * ( clearance ) ;
}

// TODO maybe speed should stay above zero?

void accelerate ( const display_type & state , display_type & change , double intensity )
{
  // user affects a speedup. positive intensity means 'faster'
  change.speed = state.speed * intensity ;
}

void pull ( const display_type & state , display_type & change ,
            int displace_x , int displace_y )
{
  // user affects a combined pan and tilt via click+drag.
  // the movement is codified in displace_x and displace_y.
  pan ( state , change , displace_x / 100.0 ) ;
  tilt ( state , change , - displace_y / 100.0 ) ;
}

// modification of black point and white point. The large difference
// in the factors applied to the levels (.05 vs. 1.4) is due to the
// fact that both points are interpreted in linear brightness and
// they are chosen to produce a change which is 'just noticeable'
// for a 'short depression of the key' for 'normal content'...
// so this is heuristic.

void change_brightness ( const display_type & state ,
                         display_type & change ,
                         double intensity )
{
  change.brightness = intensity ;
}

void reset_brightness ( display_type & state )
{
  state.brightness = 1.0 ;
}

void change_black_point ( const display_type & state ,
                          display_type & change ,
                          double intensity )
{
  double range = ( state.white_point - state.black_point ) / 255.0 ;
  change.black_point = intensity * range ;
}

void reset_black_point ( display_type & state )
{
  if ( state.black_point != 0.0 )
  {
    state.black_point = 0.0 ;
  }
}

void change_white_point ( const display_type & state ,
                          display_type & change ,
                          double intensity )
{
  double range = ( state.white_point - state.black_point ) / 255.0 ;
  change.white_point = intensity * range ;
}

void reset_white_point ( display_type & state )
{
  if ( state.white_point != 255.0 )
  {
    state.white_point = 255.0 ;
  }
}

// reverted to simply summing up intensities.
// I'm now returning the readily calculated white balance instead of
// the average, putting the math where it belongs (namely, right here)

pixel_type calculate_white_balance ( view_type & sample )
{
  double r, g, b ;
  double sz = sample.size() ;

  for ( auto px : sample )
  {
    double rc = px[0] ;
    double gc = px[1] ;
    double bc = px[2] ;
    if (    rc > 0.0f
         && rc < 220.0f 
         && gc > 0.0f 
         && gc < 220.0f
         && bc > 0.0f 
         && bc < 220.0f )
    {
      r += rc ;
      g += gc ;
      b += bc ;
    }
  }
  if ( g == 0 )
    g = .000001 ;

  return pixel_type ( g / r , 1 , g / b ) ;
}

/// cl_tokenize tokenizes a std::string containing arguments for an
/// invocation of a program with the standard C/C++ argc, argv pair.
/// This is used in pv to tokenize argument sets from the 'file queue',
/// which is, really, an 'argument set queue'.

int cl_tokenize ( const std::string & cl ,
                  std::vector < std::string > & result )
{
  int ntokens = 0 ;
//   assert ( result.size() == 0 ) ;
  int cl_length = cl.length() ;

  std::string current ;
  int read_pos = 0 ;
  while ( true )
  {
    if ( read_pos >= cl_length )
    {
      if ( current.length() )
        result.push_back ( current ) ;
      break ;
    }
    auto c = cl [ read_pos ] ;

    // handle quoted strings

    if ( c == '\'' || c == '"' )
    {
      auto q = c ;
      ++read_pos ;
      while ( true )
      {
        if ( read_pos >= cl_length )
        {
          std::cerr << "unterminated quoted string" << std::endl ;
          return -1 ;
        }
        c = cl [ read_pos ] ;
        ++ read_pos ;
        if ( c == q )
        {
          c = cl [ read_pos ] ;
          break ;
        }
        else
        {
          current.push_back ( c ) ;
        }
      }
    }

    // not a quoted string; maybe white space?
    if ( c == ' ' || c == '\t' || c == '\n' || c == '\r' )
    {
      result.push_back ( current ) ;
      current.clear() ;
      ++read_pos ;
      while ( read_pos < cl_length )
      {
        auto n = cl [ read_pos ] ;
        if ( n != ' ' && n != '\t' )
          break ;
        ++read_pos ;
      }
    }
    else
    {
      ++read_pos ;
      current.push_back ( c ) ;
    }
  }
  return result.size() ;
}

// if pv receives a SIGINT signal, it will behave as if the user had pressed
// the 'F1' key to 'relaunch' the current view. This can be exploited by
// external programs for 'tethering' pv and using it as the external program's
// display engine. If the external program modifies, e.g., an ini file and
// wants pv to react to the change, it can issue the SIGINT signal to make
// pv honour the change quickly without any user interaction. With the current
// implementation, this won't work more often than a few times per second,
// probably due to the large overhead of rebuilding pretty much everything.
// This is work in progress.

std::atomic<bool> sigint_signal ( false ) ;

void sigint_signal_handler ( int signal )
{
  sigint_signal = true ;
}

// file name of imgui.ini file, will be set in setup_window, but must persist,
// hence the global variable.

std::string imgui_ini ;

// struct screen_type holds the SFML window used to render stuff to. This object
// persists throughout pv's run time; when new images are displayed they will
// be rendered to the window living in here.

struct screen_type
{
  sf::RenderWindow * p_window = nullptr ;
  bool full_screen ;
  sf::Vector2u save_window_size ;
  sf::ContextSettings settings ; // is set in c'tor
  const char * p_title ;

  // when running on some macs, no fullscreen modes are available to SFML,
  // even though switching to fullscreen mode via the window's controls
  // is possible. To safeguard against this situation (which is probably
  // a bug in SFML, see https://en.sfml-dev.org/forums/index.php?topic=28601.0)
  // we disable switching to fullscreen mode by creating the sf::RenderWindow
  // with sf::Style::Fulscreen altogether whenever may_use_fullscreen is false.
  // This makes some UI elements useless (like the 'WINDOW' button in the GUI
  // and the fullscreen key ('B' on macs because they don't pass F11) but avoids
  // the crash which occurs otherwise.

  const bool may_use_fullscreen ;

  // in this function, I call setMouseCursorVisible ( false ) a few
  // times: after a new window is created, before an extant window
  // is re-created and after an extant window was recreated. This is
  // to fight the 'mouse-cursor-confined-to-part-of-screen' bug.
  // The new logic is now to keep the Mouse off unless there is a
  // good reason not to: GUI interaction or current mouse activity.
  // I try and work around the bug by having the mouse off while I
  // switch to fullscreen mode.

  void setup_window ( const char * p_title )
  {
    auto entered = pv_clock::now() ;

    // fetch 'desktop' again, to make sure we have a correct
    // value - something outside the program may have messed
    // with the desktop, e.g. the monitor was switched to
    // portrait.

    desktop = sf::VideoMode::getDesktopMode() ; 

    if ( p_window == nullptr )
    {
      // p_window is nullptr at program startup. Later on, we use
      // 'create' on the existing window, which keeps it's structure
      // alive and the binding with ImGui doesn't have to be formed
      // anew.

      if ( may_use_fullscreen && full_screen )
      {
        // running on Linux, sf::Style::Fullscreen is not always
        // behaving correctly: at times, a white bar appears at the
        // top of the screen, the return of p_window->getSize() does
        // sometimes reflect the wrong size, but not always. But
        // if this happens, we get an SFML Resize event with the
        // actual size and then we can fight the bug by re-creating
        // the window.

        p_window = mem_in() << new sf::RenderWindow
                                    ( desktop ,
                                      std::string ( "" ) ,
                                      sf::Style::Fullscreen ,
                                      settings ) ;
      }
      else
      {
        sf::VideoMode window_mode ( save_window_size.x ,
                                    save_window_size.y ) ;

        p_window = mem_in() << new sf::RenderWindow
                                    ( window_mode ,
                                      p_title ,
                                      sf::Style::Default ,
                                      settings ) ;
      }

#ifdef USE_IMGUI

      // initialize the interface to ImGui

      if ( ! ImGui::SFML::Init ( *p_window , false ) )
      {
        std::cerr << "failed to init ImGui::SFML" << std::endl ;
        exit ( -7 ) ;
      }

      auto & IO = ImGui::GetIO() ;
      auto font_size = ini::args.imgui_font_size * ui::fgui ;

      IO.Fonts->Clear();
      IO.Fonts->AddFontFromFileTTF
        ( ui::font_filename.c_str() , font_size ) ;

      std::cout << "ImGui uses GUI scaling factor "
                << ui::fgui << std::endl ;

      // bend the imgui.ini file's path to the user's HOME directory
      // - by default it's a relative path and then clutters all folders
      // lux visits. If HOME is not found, the default will kick in,
      // using the realtive path.

      const char * _home = std::getenv ( "HOME" ) ;
      imgui_ini = std::string ( _home ? _home : "" ) ;

      if ( _home )
      {
        imgui_ini += "/imgui.ini" ;
        IO.IniFilename = imgui_ini.c_str() ;
      }

#ifdef HAVE_MD_ICONS

      // code to use Google's Material Design Icon font. Currently,
      // this isn't used extensively, but it makes for nice compact
      // GUI elements, in contrast to my rather 'pedestrian' GUI
      // using buttons etc. with text labels.
      // the code is adapted from
      // https://github.com/juliettef/IconFontCppHeaders
      // where the use of their icon font headers is demonstrated
      // for use with Dear ImGui. The font itself is best obtained
      // via a link from https://github.com/juliettef/IconFontCppHeaders
      // I used this Link which they provided:
      // https://github.com/google/material-design-icons/blob/master/font/MaterialIcons-Regular.ttf
      // I haven't yet settled on a specific path where lux will
      // look for this font; currently lux will look for it in the
      // directory where it's launched.

      float baseFontSize = font_size ;
      float iconFontSize = baseFontSize ;

      // merge in icons from google's Material Design Icon Font

      static const ImWchar icons_ranges[]
        = { ICON_MIN_MD, ICON_MAX_16_MD, 0 } ;

      ImFontConfig icons_config ; 
      icons_config.MergeMode = true ; 
      icons_config.PixelSnapH = true ; 

      // took a hint from https://github.com/ocornut/imgui/issues/4127
      // to add a glyph offset; without it, the icons align to the
      // top of my buttons rather than with the text. The added fgui
      // factor is a guess, have to see how it pans out an my macs.

      icons_config.GlyphOffset = { 0.f, 4.f * ui::fgui };

      icons_config.GlyphMinAdvanceX = iconFontSize;
      IO.Fonts->AddFontFromFileTTF( FONT_ICON_FILE_NAME_MD,
                                    iconFontSize,
                                    &icons_config, icons_ranges );
      // End of code adapted from
      // https://github.com/juliettef/IconFontCppHeaders

#endif // #ifdef HAVE_MD_ICONS

      if ( ! ImGui::SFML::UpdateFontTexture() )
        exit ( -8 ) ;

#endif // #ifdef USE_IMGUI

       p_window->setMouseCursorVisible ( false ) ;
     }
    else
    {
      p_window->setMouseCursorVisible ( false ) ;

      // we already have a window, so we use 'create' to re-create
      // it with the requested settings.

      if ( may_use_fullscreen && full_screen )
      {
        // running on Linux, sf::Style::Fullscreen is not always
        // behaving correctly: at times, a white bar appears at the
        // top of the screen, the return of p_window->getSize() does
        // sometimes reflect the wrong size, but not always. But
        // if this happens, we get an SFML Resize event with the
        // actual size and then we can fight the bug by re-creating
        // the window.

        p_window->create ( desktop ,
                           std::string ( "" ) ,
                           sf::Style::Fullscreen ,
                           settings ) ;
      }
      else
      {
        sf::VideoMode window_mode ( save_window_size.x ,
                                    save_window_size.y ) ;

        p_window->create ( window_mode ,
                           p_title ,
                           sf::Style::Default ,
                           settings ) ;
      }
      p_window->setMouseCursorVisible ( false ) ;
    }

    // sync to vsync to avoid stutter
    // pv is driven by the vertical sync. This way, as long as the frames keep coming in
    // fast enough, the display will be stutter-free. If the frames take too long and
    // stutter occurs, the user can take action and do one or several of:
    // - use 'global scaling' (-m, M key)
    // - lower the frame rate/screen resolution in the system settings
    // - use a faster interpolator
    // - use another magnification
    // - look at a part of the panorama which is easier to compute
    // SFML also has an alternative method to show frames at a fixed rate, which
    // is activated by calling setFramerateLimit. The two methods don't work together,
    // so we use either one or the other - or, if use_vsync is false and there is no
    // valid frame rate limit, we let pv run 'unleashed' producing frames as fast
    // as it can and calling display() for every frame as soon as possible.
    // This can be used for benchmarking, since it will eat up all the processing power
    // it can get.

    if ( ini::args.use_vsync )
    {
       p_window->setVerticalSyncEnabled ( true ) ;
    }
    else if ( ini::args.frame_rate_limit > 0 )
    {
      p_window->setVerticalSyncEnabled ( false ) ;
      p_window->setFramerateLimit ( ini::args.frame_rate_limit ) ;
    }

    // at times, lux is slow to start up. I suspect it might have
    // something to do with setting up the window, so I'm measuring
    // the time it takes.

    auto finished = pv_clock::now() ;

    // auto elapsed
    //   = float ( std::chrono::duration_cast<std::chrono::microseconds>
    //                 ( finished - entered ) . count() ) ;
    // std::cout << "setup_screen took " << elapsed << " microseconds"
    //           << std::endl ;
  }

  // void setup_window ( const char * p_title )
  // {
  //   std::cout << "setup_window: full_screen = " << full_screen << std::endl ;
  // 
  //   if ( p_window == nullptr )
  //   {
  //     if ( may_use_fullscreen && full_screen )
  //       p_window = mem_in() << new sf::RenderWindow
  //                                   ( desktop ,
  //                                     std::string() ,
  //                                     sf::Style::Fullscreen ,
  //                                     settings ) ;
  //     else
  //       p_window = mem_in() << new sf::RenderWindow
  //                                   ( sf::VideoMode ( save_window_size.x ,
  //                                                     save_window_size.y ) ,
  //                                     p_title ,
  //                                     sf::Style::Default ,
  //                                     settings ) ;
  //     settings = p_window->getSettings() ;
  //   }
  //   else
  //   {
  //     if ( may_use_fullscreen && full_screen )
  //       p_window->create ( desktop ,
  //                          std::string() ,
  //                          sf::Style::Fullscreen ,
  //                          settings ) ;
  //     else
  //       p_window->create ( sf::VideoMode ( save_window_size.x ,
  //                          save_window_size.y ) ,
  //                          p_title ,
  //                          sf::Style::Default ,
  //                          settings ) ;
  //   }
  // 
  // }
  // 
  bool srgb_capable() const
  {
    return settings.sRgbCapable ;
  }

  // a note on sf::VideoMode::getFullscreenModes(): on some macs
  // this call seems to not produce any fullscreen modes at all.
  // nevertheless, switching to fullscreen via the OS controls
  // works okay.

  screen_type ( bool _full_screen ,
                const char * _p_title ,
                int window_width ,
                int window_height ,
                bool srgb_capable
              )
  : p_title ( _p_title ) ,
    may_use_fullscreen ( sf::VideoMode::getFullscreenModes().size() ) , 	
    settings ( 0 , 0 , 0 , 1 , 1 ,
               sf::ContextSettings::Attribute::Default ,
               srgb_capable )
  {
    full_screen = _full_screen ;
    save_window_size = sf::Vector2u ( window_width ,
                                      window_height ) ;
    setup_window ( p_title ) ;
  }

  void set_fullscreen ( bool _full_screen, const char * p_title )
  {
    if ( _full_screen != full_screen )
    {
      if ( ! full_screen )
        save_window_size = p_window->getSize() ;
      full_screen = _full_screen ;
      setup_window ( p_title ) ;
    }
    else
    {
      p_window->setTitle ( p_title ) ;
    }
  }

  void close_window()
  {
    p_window->setActive ( false ) ;
    p_window->close() ;
#ifdef USE_IMGUI
    ImGui::SFML::Shutdown() ;
#endif // #ifdef USE_IMGUI
    memlog >> p_window ;
    p_window = nullptr ;
  }

  ~screen_type()
  {
    close_window() ;
  }
} ;

// implementation of class sensor_settings_type's methods.
// Had to be moved here, so that rendering code doesn't inline it.

sensor_settings_type::sensor_settings_type()
{
  magnifying_glass = 1.0 ;
  magnifying_glass_factor = 10.0 ;

  sensor_size = { 1.0 , 1.0 , 1.0 } ;
  sensor_tilt = get_rotation_q ( 0.0 , 0.0 , 0.0 ) ;
  sensor_shift = { 0.0 , 0.0 , 0.0 } ;
}

bool sensor_settings_type::operator==
     ( const sensor_settings_type & other ) const
{
  return    magnifying_glass == other.magnifying_glass
          && magnifying_glass_factor == other.magnifying_glass_factor
          && sensor_size == other.sensor_size
          && sensor_tilt == other.sensor_tilt
          && sensor_shift == other.sensor_shift ;
}

/// sensor size is the first sensor modification to be applied. At this point, the
/// sensor is still in it's initial position, touching the unit sphere at (-1,0,0)
/// with it's center. The size change is applied as a multiplication with 'factor',
/// where factor[0] should normally be 1.0 (it signifies the sensor's distance from
/// the unit sphere's center, which may be adjusted by sensor shift, further down)

void sensor_settings_type::apply_sensor_size
                        ( point_3d_d_type & origin ,
                          point_3d_d_type & ur ,
                          point_3d_d_type & ll ,
                          point_3d_d_type & lr ,
                          point_3d_d_type factor ) const
{
  origin *= factor ;
  ur *= factor ;
  ll *= factor ;
  lr *= factor ;
}

/// sensor tilt is the second sensor modification applied. It moves the sensor so
/// that it's center is at the unit sphere's center, then applies the quaternion
/// encoding the desired rotation, then moves the sensor's center back to where it was.

void sensor_settings_type::apply_sensor_tilt
                        ( point_3d_d_type & origin ,
                          point_3d_d_type & ur ,
                          point_3d_d_type & ll ,
                          point_3d_d_type & lr ,
                          quaternion_type orientation ) const
{
  // move sensor to origin

  auto center = ( ur + ll ) / 2.0 ;

  origin[0] -= center[0] ;
  ur[0] -= center[0] ;
  ll[0] -= center[0] ;
  lr[0] -= center[0] ;

  // apply rotation

  origin = rotate_q ( origin , orientation ) ;
  ur = rotate_q ( ur , orientation ) ;
  ll = rotate_q ( ll , orientation ) ;
  lr = rotate_q ( lr , orientation ) ;

  // move back

  origin[0] += center[0] ;
  ur[0] += center[0] ;
  ll[0] += center[0] ;
  lr[0] += center[0] ;
}

/// the third sensor manipulation, after size and tilt, modifies the sensor's
/// position. All three points defining the sensor are shifted by the same amounts.

void sensor_settings_type::apply_sensor_shift
                        ( point_3d_d_type & origin ,
                          point_3d_d_type & ur ,
                          point_3d_d_type & ll ,
                          point_3d_d_type & lr ,
                          point_3d_d_type factor ) const
{
  origin += factor ;
  ur += factor ;
  ll += factor ;
  lr += factor ;
}

/// apply() bundles the application of the three sensor manipulations,
/// also taking into account the 'magnifying glass' constant by adjusting
/// sensor_size accordingly.

void sensor_settings_type::apply
            ( point_3d_d_type & origin ,
              point_3d_d_type & ur ,
              point_3d_d_type & ll ,
              point_3d_d_type & lr ) const
{
  apply_sensor_size ( origin , ur , ll , lr ,
                      point_3d_d_type ( 1.0 ,
                                        1.0 / magnifying_glass ,
                                        1.0 / magnifying_glass ) ) ;
  apply_sensor_tilt ( origin , ur , ll , lr , sensor_tilt ) ;
  apply_sensor_shift ( origin , ur , ll , lr , sensor_shift ) ;
}

light_settings_type neutral_light
{
  { 1.0 , 1.0 , 1.0 } , // vigra::TinyVector < double , 3 > white_balance ;
  1.0 , // double brighten ;
  0.0 , // double black_point ;
  255.0 , // double white_point ;
  false , // bool apply_gradation ;
  false  // bool cap_brightness ;
} ;

// given hfov, width, height and projection, calculate the vfov

double calculate_vfov ( double hfov ,
                        int width ,
                        int height ,
                        projection_type projection )
{
  double vfov ;
  double w_rad , h_rad ;
  double pixels_per_rad ;

  switch ( projection )
  {
    case RECTILINEAR:
      // as a one-liner, this is probably clearer than the code below
      vfov = 2.0 * atan ( height * tan ( hfov / 2.0 ) / width ) ;
      // this code produces the same result:
      // w_rad = 2.0 * tan ( hfov / 2.0 ) ;
      // pixels_per_rad = width / w_rad ;
      // h_rad = height / pixels_per_rad ;
      // vfov = 2.0 * atan ( h_rad / 2.0 ) ;
      break ;
    case CYLINDRIC:
      pixels_per_rad = width / hfov ;
      h_rad = height / pixels_per_rad ;
      vfov = 2.0 * atan ( h_rad / 2.0 ) ;
      break ;
    case STEREOGRAPHIC:
      w_rad = 2.0 * tan ( hfov / 4.0 ) ;
      pixels_per_rad = width / w_rad ;
      h_rad = height / pixels_per_rad ;
      vfov = 4.0 * atan ( h_rad / 2.0 ) ;
      break ;
    case SPHERICAL:
    case FISHEYE:
    case MOSAIC:
      vfov = hfov * height / width ;
      break ;
    default:
      vfov = hfov ; // debatable...
      break ;
  }

  return vfov ;
}

// forward declaration

void store_rendered_image ( const job_type * p_job ,
                            view_type * p_frgb ,
                            view4_type * p_frgba ,
                            bool frame_is_linear ,
                            const std::string & filename ) ;

// forward declaration

void calculate_brightness() ;

  // forward declaration

void signal_light_balance ( bool on ) ;

// namespace ui has everything which is used by the user interface.
// This is not yet fully true - some ui-relevant variables are in
// inner_main - but the ones in here are those which are accessed by
// user interface code in the do_... or on_... routines mediating
// user interactions. Namespace ui also has the mediation routines
// and stuff like keyboard-triggered jump tables.

namespace ui
{
bool legacy_gui ;
int mouse_off_after = 50 ;

// variables and functions residing in lux_imgui.cc:

extern bool file_select_pending ;
extern bool imgui_reload_geometry ;
extern bool imgui_reload_conditioning ;
extern bool imgui_reload_export ;
extern bool imgui_reload_behaviour ;
extern bool imgui_reload_view_control ;
// extern bool imgui_reload_transport ;
extern void ShowImGui() ;

job_type job ;

// containers for unused and used files/subinvocations

std::deque < std::string > file_queue ;
std::deque < std::string > used_files ;
std::string current_file ;

// bool has_subimages = false ;
int subimages = 0 ;
int subimage = 0 ;

std::size_t oiio_arg_hash = 0 ;

// show_status_line will be true if any status information should be
// displayed at all

bool show_status_line = true ;
bool reverse_drag = false ;
bool reverse_secondary_drag = false ;
bool show_error_dialog = false ;
bool snap_to_hq = true ;
bool snap_to_fusion = true ;
bool snap_to_stitch = true ;

// variables used for status line display

std::string status_line ;
std::vector < std::string > metadata_query_v ;
std::vector < std::string > metadata_result_v ;
std::string metadata_format = std::string() ;
std::vector < std::string > oiio_arg ;
std::vector < std::string > oiio_type ;
std::vector < std::string > oiio_val ;

// OIIO plugins can take a whole range of extra configuration
// arguments which instruct the plugins to behave in a certain way.
// lux doesn't deal with all these arguments separately but uses
// OIIO infrastructure code to make them 'palatable' to OIIO.
// Most arguments are simple single values, and they can be passed
// as key-value assignments, like --oiio_arg=key=value.
// Some arguments require additional type information - especially
// those which take several values. For these, lux uses a special
// syntax: an OIIO typestring is suffixed to the key, separated
// by an '@' sign, like "--oiio_arg=key@typestr=val val ..."
// note the quotes: the values have to be separated by space or
// tab, so the entire argument is quoted to 'hold it together'.
// split_oiio_args feeds the three vectors above, which are
// processed in fileio.cc (see add_oiio_args). this is where the
// generation of the type descriptor and other OIIO-specific
// functionality reside - we want access to OIIO code restricted
// to fileio, so here we only do the string manipulation.

void split_oiio_args()
{
  oiio_arg.clear() ;
  oiio_val.clear() ;
  for ( const std::string & attr : ini::args.oiio_arg )
  {
    auto pos = attr.find_first_of ( "=" ) ;
    if ( pos != attr.npos )
    {
      auto lhs = attr.substr ( 0 , pos ) ;
      auto at_pos = lhs.find_first_of ( "@" ) ;
      if ( at_pos != lhs.npos )
      {
        // this argument is suffixed with an OIIO typestring
        oiio_arg.push_back ( lhs.substr ( 0 , at_pos ) ) ;
        oiio_type.push_back ( lhs.substr ( at_pos + 1 ) ) ;
      }
      else
      {
        // no typestring
        oiio_arg.push_back ( lhs ) ;
        oiio_type.push_back ( std::string() ) ;
      }
      // take the remainder after the '=' as the attribute's value
      oiio_val.push_back ( attr.substr ( pos + 1 ) ) ;
    }
    else
    {
      oiio_arg.push_back ( attr ) ;
      oiio_type.push_back ( std::string() ) ;
      oiio_val.push_back ( std::string() ) ;
    }
  }
}

typedef void (*trigger_type)() ;

// we initialize all trigger_type objects to 'ignore', resulting in no
// action if they are triggered.

void ignore() { }

// dispatch table for keyboard events. There is only a relatively small
// number of individual keys which produce SFML Keyboard events, so we
// can easily build a table holding a trigger for each key. Many of these
// will be bound to 'ignore' and produce no action. Going via a jump table
// is quicker than testing for each possible event in turn. If the trigger
// was activated by a 'genuine' keyboard event, the trigger function can
// additionally access the SFML event object to check for Shift etc.
// We have two tables, one for acute and one for chronic keys.

trigger_type keyboard_triggered [ 2 * sf::Keyboard::KeyCount ] ;
trigger_type ckeyboard_triggered [ 2 * sf::Keyboard::KeyCount ] ;

// when a KeyPressed event is detected for a 'chronic' key, we record
// the fact in this vector. Then, when we look for the real-time state
// of chronic keys, we can limit the test to the 'in-play' keys.

std::vector < sf::Keyboard::Key > chronic_in_play ;

// binding a trigger to an sf::Keyboard::Key value sets the corresponding
// entry in the keyboard_triggered table

void bind ( sf::Keyboard::Key key , trigger_type trigger , bool shift = false )
{
  assert ( key >= 0 && key < sf::Keyboard::KeyCount ) ;
  keyboard_triggered [ key + ( shift ? sf::Keyboard::KeyCount : 0 ) ]
    = trigger ;
}

// do a binding for a chronic key. We need to keep these separate from
// the acute keys, because otherwise the SFML key pressed event which
// is produced when the 'chronic' key is initially depressed would
// trigger the action attached to this key.

void cbind ( sf::Keyboard::Key key , trigger_type trigger , bool shift = false )
{
  assert ( key >= 0 && key < sf::Keyboard::KeyCount ) ;
  ckeyboard_triggered [ key + ( shift ? sf::Keyboard::KeyCount : 0 ) ]
    = trigger ;
}

// deactivates a binding for a keyboard key
// TODO: unbind routine for chronic keys, removing the key from chronic_keys

void unbind ( sf::Keyboard::Key key , bool shift = false )
{
  assert ( key >= 0 && key < sf::Keyboard::KeyCount ) ;
  keyboard_triggered [ key + ( shift ? sf::Keyboard::KeyCount : 0 ) ]
    = ignore ;
}

// dispatch routine. the incoming key event's key code is used as an index
// into the keyboard_triggered table and the trigger found there is executed,
// receiving the key event as it's parameter.

bool react ( const sf::Event::KeyEvent & event )
{
  if ( event.code >= 0 && event.code < sf::Keyboard::KeyCount )
  {
    int index = event.code ;
    if ( event.shift )
      index += sf::Keyboard::KeyCount ;
    trigger_type action = keyboard_triggered [ index ] ;
    if ( action != ignore )
    {
      action() ;
      return true ;
    }
  }
  else
  {
    std::cerr << "react received invalid key code "
              << event.code << std::endl ;
  }
  return false ;
}

// variant of react, directly taking the key code and shift flag. This
// overload is for chronic input, where the ui tests for pressed and held
// keys.

bool creact ( const sf::Keyboard::Key & key , bool shift = false )
{
  if ( key >= 0 && key < sf::Keyboard::KeyCount )
  {
    int index = key ;
    if ( shift )
      index += sf::Keyboard::KeyCount ;
    trigger_type action = ckeyboard_triggered [ index ] ;
    if ( action != ignore )
    {
      action() ;
      return true ;
    }
  }
  else
  {
    std::cerr << "creact received invalid key code "
              << key << std::endl ;
  }
  return false ;
}

// test and ctest only check if a trigger is bound to a key and
// return true if one is found. They do not execute the action.

bool test ( const sf::Event::KeyEvent & event )
{
  if ( event.code >= 0 && event.code < sf::Keyboard::KeyCount )
  {
    int index = event.code ;
    if ( event.shift )
      index += sf::Keyboard::KeyCount ;
    trigger_type action = keyboard_triggered [ index ] ;
    return ( action != ignore ) ;
  }
  return false ;
}

bool ctest ( const sf::Keyboard::Key & key , bool shift = false )
{
  if ( key >= 0 && key < sf::Keyboard::KeyCount )
  {
    int index = key ;
    if ( shift )
      index += sf::Keyboard::KeyCount ;
    trigger_type action = ckeyboard_triggered [ index ] ;
    return ( action != ignore ) ;
  }
  return false ;
}

// next we have variables containing the user interface's state.

std::string pwd ;

source_type source ;
target_type target ;

target_type nonstandard_target ;
quaternion_type nonstandard_orientation ;

double hfov_norm ;

sf::String extra_args ;

// we start out in IMMEDIATE mode, which calculates every frame individually.

display_mode mode = IMMEDIATE ;
display_mode save_mode = IMMEDIATE ;

bool clear_before_draw = false ;

// blending mode for facet maps, which can be either BLEND_RANKED or
// BLEND_HDR. For cubemaps it's always BLEND_RANKED, and for other
// projections it's irrelevant

blending_mode blending = BLEND_RANKED ;

// relaunch is a flag which is set to true when the viewer is changing
// from fullscreen mode to a window or back. This change requires resetting
// several parameters, so it's a bit like a new start - hence 'relaunch'

bool relaunch = true ;

// variables used for crossfading

float fade_factor = 1.0 ;
float crossfade_delta = .03 ;

bool crossfade = true ;

float feathering = 0.0 ;

// this flag is set to true to signals that a snapshot of the current view
// should be made. TODO: factor out the snapshot code - rather than basing
// the snapshot on the result of the last 'regular' viewer cycle, build the
// job from scratch.

bool snapshot = false ;

bool exposure_fusion = false ;
bool stitch_images = false ;
bool use_nonstandard_target = false ;

// this flag is set to true if pv is meant to skip to the next image
// (if any) after a snapshot was taken.

bool batch_flag = false ;
bool next_after_snapshot = false ;
bool next_after_fusion = false ;
bool next_after_stitch = false ;

int snapshot_threads = std::thread::hardware_concurrency() ;
std::string snapshot_prefix ;
std::string snapshot_extension ;
int compression = 90 ;
float output_magnification =1.0f ;

// facet brightness values as calculated by calculate_brightness.

std::vector < double > facet_brightness ;

// another flag to signal that pv should create some 'special' frame, here
// it's a frame which is used to calculate a new white balance. TODO as above

bool capture_white_balance = false ;

bool capture_light = false ;
std::string light_balance ;

// flag indicating whether the viewer should run in fullscreen mode or not.
// TODO: this is a bit redundant with the value in the screen_type object.
// This object is only created once it's known whether the current invocation
// is meant to run fullscreen or windowed. The logic could be changed.

bool run_fullscreen = true ;

// pan or yaw. If false, horizontal movement becomes view-relative,
// which can result in losing the vertical. If true, horizontal
// movement is bias-relative, so vertical lines remain in this
// orientation. This is the default.

bool lock_vertical = true ;

// do not show frames (used for benchmarking)

bool suppress_display = false ;

// show only one specific image out of a facet map

int solo = -1 ;
bool show_facet_dry = false ;

bool use_rank = true ;

// allows or disallows use of PAN mode. This mode uses stored precalculated
// coordinates into the source image and increases their x component when
// panning. This only works with some projections and can be switched off
// altogether - mainly for debugging purposes.

bool allow_pan_mode = true ;

// use 'area' type decimator for rendering

bool decimate_area = false ;

double pyramid_scaling_step ;
int pyramid_smoothing_level ;
bool grey_edge ;

// still images are - per default - rendered with the 'high quality'
// interpolator. By setting this flag to false, all rendering is done with
// the 'low quality' interpolator.

bool snap_to_hq_interpolator = true ;

stack_mode stacking ;

// if this flag is found to be false, lux will 'power down' and
// not try and get more images from the queue or the pipe.

bool show_more_images = true ;

// go to the previous image rather than the next one

bool show_previous = false ;

// this flag is set if the current image should be displayed again
// in a new cycle with additional override arguments

bool show_again = false ;

long grace_period ;
bool slideshow_on = false ;
double slide_interval = 7.0 ;

// if true, pv will try and obtain more 'subinvocations' from cin.

bool files_from_pipe = false ;

// some initial values for viewer state.

bool auto_position = true ;

double initial_yaw = 0.0 ;
double initial_pitch = 0.0 ;
double initial_roll = 0.0 ;

double initial_dx = 0.0 ;
double initial_dy = 0.0 ;
double initial_dr = 0.0 ;

double initial_speed = 1.0 ;
double initial_zoom , zoom_min ;

// hfov of the view, not the source image

double hfov ;

// for show_again, this holds the hfov to come back to

double save_hfov ;

// for show_again, this holds the projection which was active
// when the show_again sequence was triggered.

projection_type save_projection = PRJ_NONE ;

// this holds the view hfov to come back to

double save_hfov_view ;
double save_dy ;

// holds stuff like sensor tilt etc.

// sensor_settings_type sensor_settings ;

// b-spline degree for fast and hq interpolators

int fast_interpolator_degree = 1 ;
int quality_interpolator_degree = 3 ;

std::vector < std::string > cubemap ( 6 ) ;
double cubeface_fov ;

std::vector < projection_type > facet_projection ;
std::vector < quaternion_type > facet_orientation ;

// holds source_type structures for cubemaps and facet maps
// I'd rather have the vector of source_type in the interpolator, but
// I have an issue with copying it in the rendering code: the rendering
// code produces ISA-specific binary for the vector copy operation, and
// the linker picks an arbitrary version, because all binary versions
// come out with the same mangled name.

std::vector < source_type > source_v ;

bool overrides_active = false ;
std::vector < std::string > override_args ;
std::vector < std::string > sessionwide_args ;

// Holds values pertaining to the current view and the window displaying
// it. TODO: should rather have an instance here and handle initialization
// differently

screen_type * p_screen = nullptr ;

// this value encodes the speed of the automatic pan.
// TODO: might switch to use a bool on/off flag plus a value

double autopan_ramp ;
double autopan ;
double save_autopan ;

// flag switching 'auto quality' on/off. auto quality tries to settle on
// a moving image scaling factor which keeps the frame rendering time for
// animated sequences low enough to avoid stutter.

bool auto_quality = false ;

// flag indicating whether internal light processing should be done in
// linear RGB. This is true by default, which is mathematically correct.

bool process_linear = true ;

// if true, apply simple global tonemapping operator

bool tonemap = false ;

// if true, solo images in alpha mode will be displayed without alpha
// channel, revealing the 'filled-in' parts

bool heal = false ;

// if true, don't fuse separate exposures but create a 'faux bracket'
// from differently brightened snapshots

bool faux_bracket = false ;

// when rendering single still images, especially with pyramid
// levels > 0 and/or low degree b-splines, image quality can be
// disappointing. But single images are rendered only once, so the
// rendering time is not as critical as for animated sequences.
// One way to increase image quality with relatively low cost is
// supersampling the image, and then having it scaled down ba the GPU.

float still_image_scaling = sqrt ( 2.0 ) ;

// snapshots are renderedscaled-up by this factor:

float snapshot_magnification = 2.0 ;

int serial_number = 0 ; // serial number for the snapshot

// leverage factor influencing all intensity-sensitive user actions.
// TODO check if it's really used for all of these. Go through code
// using leverage factors and make this less haphazard and possibly
// configurable

double snappiness = .005 ;

double angular_intensity ;

// orientation bias is applied before state.orientation and allow to
// make user-defined rotations relative to this bias rather than to some
// fixed default value.

quaternion_type bias = get_rotation_q ( 0.0 , 0.0 , 0.0 ) ;

// ultimately, all user interaction is used to set 'state', which is
// where the parameters for a rendering job originate. 'change' is used for
// 'chronic' interactions: rather than directly modifying 'state', change is
// modified, and the value in 'change' is modulated upon 'state' when a
// rendering job is created. If the value in change is held constant, this
// results in a 'smooth' operation (first derivative is constant)

display_type state ;
display_type change ;
display_type no_change ;

bool & change_draw ( change.draw ) ;

// in animated sequences, lower horizontal resolution:

double horizontal_squash = 1.0 / sqrt ( 2.0 ) ;

bool active_change = false ;

bool gui_automatic = true ;

interpolator_base * p_itp = 0 ;

// at times we want to make sure that the current interpolator
// can't be reused. Note, though, that this mustn't be done while
// the current interpolator is still in use. The moment to call
// this function is after all pending jobs have terminated and
// the current cycle ends.

void destroy_current_itp()
{
  if ( p_itp != nullptr )
  {
    memlog >> p_itp ;
    p_itp = nullptr ;
  }
}

// get the effective degree of the interpolators; this may differ
// from the initial values due to shifting

projection_type projection = PRJ_NONE ;

double zoom_buffer = 0.0 ;
double zoom_decay = 0.0f ;

display_type saved_state ;
bool have_state ;

point_2d_d_type zoom_locus ;
point_3d_d_type zoom_focus ;

void metadata_info_string()
{
  if ( ui::metadata_result_v.size() == 0 )
    return ;

  if ( metadata_format == std::string() )
  {
    for ( int i = 0 ; i < ui::metadata_result_v.size() ; i++ )
    {
      ui::status_line += ui::metadata_query_v [ i ] ;
      ui::status_line += ": " ;
      auto value = ui::metadata_result_v [ i ] ;
      ui::status_line += value ;
      ui::status_line += "  " ;
    }
  }
  else
  {
    for ( int i = 0 ; i < metadata_format.size() ; ++i )
    {
      switch ( metadata_format[i] )
      {
        case '%' :
        {
          ++i ;
          auto c = metadata_format[i] ;
          if ( c == 'n' )
          {
            ui::status_line += ui::used_files.back() ;
           }
          else if ( c == 'h' )
          {
            // TODO: might pick PTO p-line hfov for facet maps

            if ( projection == FACET_MAP || projection == CUBEMAP )
              ui::status_line += "360.0" ;
            else
              ui::status_line
                += std::to_string ( source.hfov * 180.0 / M_PI ) ;
          }
          else if ( c == 'p' )
          {
            ui::status_line += projection_name [ projection ] ;
          }
          else if ( c == 'P' )
          {
            ui::status_line += projection_name [ target.projection ] ;
          }
          else if ( c >= '0' && c <= '9' )
          {
            // for now, only 10 slots for metadata
            int n = metadata_format[i] - '0' ;
            ui::status_line += ui::metadata_result_v [ n ] ;
          }
          break ;
        }
        default:
        {
          ui::status_line += metadata_format[i] ;
          break ;
        }
      }
    }
  }
}

// the next section has trigger functions. These react to user input.
// They will be invoked as a reaction to SFML key events. The functions
// staring with 'on_' mediate acute key input, the ones starting with
// 'do_' mediate chronic key input.

// on_terminate ends the program

void on_terminate()
{
  std::cout << "on_terminate is called" << std::endl ;
  mode = TERMINATE ;
  relaunch = false ;
  show_more_images = false ;
}

extern bool any_panel() ;

void on_escape()
{
  if ( any_panel() )
    close_all_panels() ;
  else
    on_terminate() ;
}

// leave inner_main, then call it again. The iterator may persist
// an be immediately available. The tests whether the interpolator
// can be reused are quite involved and they are performed in the
// rendering thread, we needn't worry about that here.

void on_show_again()
{
  if ( ! show_again )
  {
    saved_state = state ;
    have_state = true ;
    std::string arg_text ( ui::extra_args ) ;
    int nargs = cl_tokenize ( arg_text , override_args ) ;
    mode = TERMINATE ;
    relaunch = false ;
    show_again = true ;
  }
}

// return to some defined initial state

void on_reset()
{
  // clear 'preadjust'.
  state.preadjust = get_rotation_q ( 0.0 , 0.0 , 0.0 ) ;
  // set state.orientation to initial values
  state.orientation = get_rotation_q ( ui::initial_yaw , 0.0 , 0.0 ) ;
  state.orientation *= get_rotation_q ( 0.0 , ui::initial_pitch , 0.0 ) ;
  state.orientation *= get_rotation_q ( 0.0 , 0.0 , ui::initial_roll ) ;
  // reset dx, dy and dr to the initial values, which are either the
  // values set initially, or after having been saved by on_set_bias.
  // This is the equivalent of the code above for mosaic projection.
  state.dx = initial_dx ;
  state.dy = initial_dy ;
  state.dr = initial_dr ;
  state.speed = initial_speed ;
  state.zoom_factor = initial_zoom ;
  state.rise = 0 ;
  state.sensor_tilt_v = 0.0 ;
  state.sensor_tilt_h = 0.0 ;
  state.white_balance = 1.0 ;
  reset_brightness ( state ) ;
  reset_black_point ( state ) ;
  reset_white_point ( state ) ;
  sensor_settings = sensor_settings_type() ;
  change.draw = true ;
}

// on_next_image will cause the next image in the queue to be displayed,
// or terminate the program if there are no more images.

void on_load_images() ;

void on_next_image()
{
  if ( show_previous || file_queue.size() || files_from_pipe )
  {
    mode = TERMINATE ;
    relaunch = false ;
    subimage = 0 ;
  }
  else
  {
    // 'nudge' the user to put more files into the queue.
    // If none arrive, just stay put.

    on_load_images() ;
  }
}

void on_toggle_status()
{
  change.draw = true ;
  show_status_line ^= true ;
  ini::args.show_status_line = show_status_line ;
  if ( show_status_line )
    status_flag = true ;
  else
    ui::status_line.clear() ;
}

// forward declaration; accesses variable 'gui' which is not
// declared yet

void on_toggle_legacy_gui() ;
    
// on_previous_image will signal that the previous image should be displayed

void on_previous_image()
{
  // check used_files: to be able to show the previous image, there must
  // be at least two entries there: the previous image and the current image.

  if ( ui::used_files.size() >= 2 )
  {
    show_previous = true ;
    on_next_image() ;
  }
}

void on_solo_up()
{
  if ( subimages > 1 && subimage < ( subimages - 1 ) )
  {
    subimage ++ ;
    on_show_again() ;
  }
  else
  {
    ++solo ;
    change.draw = true ;
  }
}

void on_solo_down()
{
  if ( subimages > 1 && subimage > 0 )
  {
    subimage -- ;
    on_show_again() ;
  }
  else if ( solo >= 0 )
  {
    --solo ;
    change.draw = true ;
  }
}

void on_toggle_facet_dry()
{
  change.draw = true ;
  show_facet_dry = ! show_facet_dry ;
}

// toggle between automatic panning on/off

void on_toggle_autopan()
{
  if ( autopan )
  {
    save_autopan = autopan ;
    autopan = 0 ;
  }
  else
  {
    autopan = save_autopan ;
    autopan_ramp = 0.0 ;
  }
}

point_3d_d_type get_axis ( const qd_t & q )
{
  point_3d_d_type ijk { q[1] , q[2] , q[3] } ;
  auto n = norm ( ijk ) ;
  if ( n )
    ijk /= n ;
  return ijk ;
}

double get_angle ( const qd_t & q )
{
  point_3d_d_type ijk { q[1] , q[2] , q[3] } ;
  auto n = norm ( ijk ) ;
  return 2.0 * atan2 ( n , q[0] ) ;
}

// calculate the angle between two 3D rays. a3 and b3 must be
// of unit length

double get_angle ( const point_3d_d_type & a3 ,
                   point_3d_d_type b3 )
{
  // if a3 and b3 are almost opposite to each other, we invert
  // b3 and take note. We'll now get a small angle which we'll
  // subtract from pi in the end.

  bool near_opposite = false ;

  if ( norm ( a3 + b3 ) < .0001 )
  {
    near_opposite = true ;
    b3 = -b3 ;
  }

  // we now have two points which are not precisely opposed,
  // so we can safely calculate the dot product to get the angle
  // - but if the distance of the two points is very small, we
  // use the euclidian distance instead of the angle between
  // the two rays, to avoid trouble with catastrophic cancellation
  // when forming the dot product

  double d = norm ( b3 - a3 ) ;
  double angle = d ;

  if ( fabs ( d ) > .001 )
  {
    double dt = dot ( b3 , a3 ) ;
    dt = std::min ( 1.0 , dt ) ;
    dt = std::max ( -1.0 , dt ) ;
    angle = acos ( dt ) ;
  }

  if ( near_opposite )
  {
    angle = M_PI - angle ;
  }

  return angle ;
}

point_3d_d_type normalize ( const point_3d_d_type & p )
{
  auto n = norm ( p ) ;
  assert ( n != 0.0 ) ;
  return p / n ;
}

// given p_job, get the transformation function from
// 2D window coordinates to 3D ray coordinates.
// The default is to normalize the result.

to_model_f get_to_model_tf
  ( const job_type * const p_job = & ui::job ,
    bool normalize_result = true )
{
  auto tf = dispatcher->get_target_to_model_tf ( p_job ) ;
  if ( ( ui::projection != MOSAIC ) && normalize_result )
  {
    return [=] ( const point_2d_d_type & p )
    {
      return normalize ( tf ( p ) ) ;
    } ;
  }
  else
  {
    return tf ;
  }
}

// calculate a rotational quaternion from ray a3 to ray b3.
// for MOSAIC projection, this produces a simple shift which
// is 'packaged' in the quaternion.

qd_t get_trajectory ( const point_3d_d_type & a3 ,
                      point_3d_d_type b3 )
{
  qd_t trajectory ;

  if ( projection == MOSAIC )
  {
    // this is easy - we're working on a plane

    auto u = b3 - a3 ;

    auto dx = u[1] ; // note that to_model does not produce
    auto dy = u[2] ; // roman book order but 'conventional' 3D

    // we 'abuse' the trajectory quaternion to encode the shift

    trajectory [ 0 ] = -dx ;
    trajectory [ 1 ] = -dy ;
    trajectory [ 2 ] = 0.0 ;
    trajectory [ 3 ] = 0.0 ;

    return trajectory ;
  }

  // special case: a2 and b2 transform to near opposite points.

  bool near_opposite = false ;

  if ( norm ( a3 + b3 ) < .0001 )
  {
    near_opposite = true ;
    b3 = -b3 ;
  }

  // we now have two points which are not precisely opposed,
  // so we can safely calculate the dot product to get the angle
  // - but if the distance of the two points is very small, we
  // use the euclidian distance instead of the angle between
  // the two rays, to avoid trouble with catastrophic cancellation
  // when forming the dot product

  double d = norm ( b3 - a3 ) ;
  double angle = d ;

  if ( fabs ( d ) > .001 )
  {
    double dt = dot ( b3 , a3 ) ;
    dt = std::min ( 1.0 , dt ) ;
    dt = std::max ( -1.0 , dt ) ;
    angle = acos ( dt ) ;
  }

  if ( near_opposite )
    angle = M_PI - angle ;

  if ( fabs ( angle ) < 1e-9 )
  {
    return get_rotation_q ( 0.0 , 0.0 , 0.0 ) ;
  }

  auto axis = cross ( b3 , a3 ) ;

  typedef vigra::Quaternion<double> qd_t ;
  qd_t to_view = conj ( ui::bias * state.preadjust ) ;
  axis = rotate_q ( axis , to_view ) ;

  trajectory = qd_t::createRotation ( -angle , axis ) ;
  return trajectory ;
}

// same as above, but starting out with view coordinates, which
// are converted to ray coordinates with the current parameters
// held in ui::job

qd_t get_trajectory ( const point_2d_d_type & a2 ,
                      const point_2d_d_type & b2 )
{
  // we start out by calculating the 3D ray coordinates in
  // model space for the two points

  auto to_model = get_to_model_tf() ;

  auto a3 = to_model ( a2 ) ;
  auto b3 = to_model ( b2 ) ;
  return get_trajectory ( a3 , b3 ) ;
}

// move the view's center to the current mouse position

void on_center_here()
{
  double y , p , r ;
  toEulerAngle ( state.orientation , y , p , r ) ;
  auto initial_roll = r ;

  sf::RenderWindow * & p_window ( p_screen->p_window ) ;

  auto sz = p_window->getSize() ;
  auto a2 = point_2d_d_type ( ( sz.x - 1 ) / 2.0 ,
                              ( sz.y - 1 ) / 2.0 ) ;

  sf::Vector2i mouse = sf::Mouse::getPosition ( *p_window ) ;
  auto b2 = point_2d_d_type ( mouse.x , mouse.y ) ;

  auto trj = get_trajectory ( a2 , b2 ) ;

  if ( projection == MOSAIC )
  {
    state.dx += trj[0] ;
    state.dy += trj[1] ;
  }
  else
  {
    state.orientation = trj * state.orientation ;

    if ( ui::lock_vertical )
    {
      toEulerAngle ( state.orientation , y , p , r ) ;
      state.orientation
        = get_rotation_q ( -y , p , initial_roll ) ;
    }
  }

  change.draw = true ;
}

// reverse direction of autopan

void on_reverse_autopan()
{
  autopan = - autopan ;
}

// set current state as bias. This is quaternion magic: we get
// a quaternion representing the yaw, pitch and roll of the current
// state. we multiply bias with this quaternion, receiving a new
// bias which represents the concatenation of the old bias' effect
// and the rotations codified in state. Since this movement is now
// 'dealt with' we reset state's orientation.
// Note that this should be done only when the view's center is
// on the horizon and the horizon is level. For mosaic projection,
// this function merely sets a new starting position and orientation
// to 'return to', which may be anywhere.

void on_set_bias()
{
  change.draw = true ;

  if ( projection == MOSAIC )
  {
    // for mosaic images, we take over the current dx, dy and dr as the
    // initial ones and on_return will return to them.

    initial_dx = state.dx ;
    initial_dy = state.dy ;
    initial_dr = state.dr ;
  }
  else
  {
    // apply orientation to bias

    bias *= state.preadjust ;
    bias *= state.orientation ;

    // then reset orientation

    state.preadjust = get_rotation_q ( 0.0 , 0.0 , 0.0 ) ;
    state.orientation = get_rotation_q ( 0.0 , 0.0 , 0.0 ) ;
    state.dr = 0.0 ;

    // set initial yaw, pitch and roll to zero, so that on_reset does
    // not use these values anymore - on_reset will now return only to
    // the position saved in 'bias'.

    ui::initial_yaw = ui::initial_pitch = ui::initial_roll = 0.0 ;
  }

}

void on_level_bias_up()
{
  change.draw = true ;
  state.level_bias -= .5 ;
}

void on_level_bias_down()
{
  change.draw = true ;
  state.level_bias += .5 ;
}

void on_toggle_lock_vertical()
{
  change.draw = true ;
  lock_vertical = ! lock_vertical ;
}

void on_toggle_allow_pan_mode()
{
  // toggle permission to use pan mode
  allow_pan_mode = ! allow_pan_mode ;
  change.draw = true ;
}

void on_vertical_fit()
{
  // fit the image into the view vertically
  // TODO: might do sth. similar for other sourceprojections

  change.draw = true ;
  if ( ui::projection == MOSAIC )
  {
    state.zoom_factor = double(target.height) / source.height ;
    target.vfov = source.vfov ;
    target.hfov = ( target.vfov * target.width ) / target.height ; 
    state.dy = 0.0 ;
  }
  else if ( ui::projection == RECTILINEAR )
  {
    target.vfov = source.vfov ;
    target.hfov = calculate_vfov ( target.vfov ,
                                   target.height ,
                                   target.width ,
                                   RECTILINEAR ) ;
    state.zoom_factor = ui::hfov_norm / target.hfov ;
    state.orientation = get_rotation_q ( 0.0 , 0.0 , 0.0 ) ;
  }
}

void on_horizontal_fit()
{
  // fit the image into the view horizontally
  // TODO: might do sth. similar for other source projections

  change.draw = true ;
  if ( ui::projection == MOSAIC )
  {
    state.zoom_factor = double(target.width) / source.width ;
    // we set target.hfov and target.vfov here because on_home
    // calls on_page_up after this function and relies on the
    // fov values being set correctly - normally, this would be
    // left to the stage where the next rendering job is set up.
    target.hfov = source.hfov ;
    target.vfov = ( target.hfov * target.height ) / target.width ; 
    state.dx = 0.0 ;
  }
  else if ( ui::projection == RECTILINEAR )
  {
    target.hfov = source.hfov ;
    target.vfov = calculate_vfov ( target.hfov ,
                                   target.width ,
                                   target.height ,
                                   RECTILINEAR ) ;
    state.zoom_factor = ui::hfov_norm / target.hfov ;
    state.orientation = get_rotation_q ( 0.0 , 0.0 , 0.0 ) ;
  }
}

void on_page_up()
{
  if ( projection == MOSAIC )
  {
    // 'step' is the dy value needed to make top image and view
    // margins coincide
    double step = ( source.vfov - target.vfov ) / 2.0 ;
    // we increase dy by this value, but we don't let it exceed it
    state.dy += step ;
    if ( state.dy < ( 0.1 * step ) && state.dy > ( -0.1 * step ) )
    {
      // snap to center from near-center
      state.dy = 0.0 ;
    }
    else if ( state.dy > ( 1.1 * step ) )
    {
      save_hfov_view = hfov ;
      // save_dy will set initial_dy for the next page, and initial_dy
      // is given in multiples of the source image's vfov, so we must
      // divide by source.vfov here:
      save_dy = step / source.vfov ;
      on_previous_image() ;
    }
  }
  else
  {
    state.orientation = get_rotation_q ( 0.0 , M_PI_2 , 0.0 ) ;
  }
  change.draw = true ;
}

void on_page_down()
{
  if ( projection == MOSAIC )
  {
    // 'step' is the dy value needed to make top image and view
    // margins coincide
    double step = ( source.vfov - target.vfov ) / 2.0 ;
    // we decrease dy by this value, but we don't let it fall too far
    state.dy -= step ;
    if ( state.dy < ( 0.1 * step ) && state.dy > ( -0.1 * step ) )
    {
      // snap to center from near-center
      state.dy = 0.0 ;
    }
    else if ( state.dy < - ( 1.1 * step ) )
    {
      save_hfov_view = hfov ;
      // save_dy will set initial_dy for the next page, and initial_dy
      // is given in multiples of the source image's vfov, so we must
      // divide by source.vfov here:
      save_dy = - step / source.vfov ;
      on_next_image() ;
    }
  }
  else
  {
    state.orientation = get_rotation_q ( 0.0 , - M_PI_2 , 0.0 ) ;
  }
  change.draw = true ;
}

void on_home()
{
  // make the image's top margin coincide with the view's top
  // margin for MOSAIC projection, and also make the view fit
  // the image horizontally

  if ( projection == MOSAIC )
  {
    std::cout << "on_home: state.dy initially " << state.dy << std::endl ;
    state.dy = 0.0 ;
    on_horizontal_fit() ;
    on_page_up() ;
    std::cout << "on_home: state.dy finally " << state.dy << std::endl ;
  }
}

void on_end()
{
  // make the image's bottom margin coincide with the view's
  // bottom margin for MOSAIC projection, and also make the view
  // fit the image horizontally

  if ( projection == MOSAIC )
  {
    state.dy = 0.0 ;
    on_horizontal_fit() ;
    on_page_down() ;
  }
}

void on_roll_90()
{
  // rotate view by 90 degrees

  state.orientation *= get_rotation_q ( 0.0 , 0.0 , -M_PI_2 ) ;
  change.draw = true ;
  state.dr -= M_PI_2 ;
}

void on_one_quadrant()
{
  // jump by 90 degrees

  change.draw = true ;
  auto step = get_rotation_q ( -0.5 * M_PI , 0.0 , 0.0 ) ;
  auto help = step * state.orientation ;
  state.orientation = help ;
}

void on_step_forward()
{
  // jump horizontally roughly by 1/2 of the view's hfov

  change.draw = true ;
  if ( projection == MOSAIC )
  {
    state.dx -= 0.5 * ui::hfov ;
  }
  else
  {
    auto step = get_rotation_q ( -0.5 * ui::hfov , 0.0 , 0.0 ) ;
    auto help = step * state.orientation ;
    state.orientation = help ;
    state.dx += 0.5 * ui::hfov ;
  }
}

void on_step_back()
{
  // jump horizontally roughly by -1/2 of the view's hfov

  change.draw = true ;
  if ( projection == MOSAIC )
  {
    state.dx += 0.5 * ui::hfov ;
  }
  else
  {
    auto step = get_rotation_q ( 0.5 * ui::hfov , 0.0 , 0.0 ) ;
    auto help = step * state.orientation ;
    state.orientation = help ;
    state.dx -= 0.5 * ui::hfov ;
  }
}

void on_toggle_fullscreen()
{
  // switch from full screen to window and back

  save_mode = mode ;
  mode = TERMINATE ;
  relaunch = true ;
  run_fullscreen = ! run_fullscreen ;
}

void on_toggle_gui_automatic()
{
  gui_automatic ^= true ;
}

// new snapshot code, shared between view-like and source-like
// snapshots. The job_type object for the snapshot is now derived
// from ui::job, and the snapshot is produced directly once it's
// triggered.


extern void show_popup ( const char * title ,
                         const char * message ,
                         const char * left_text ,
                         const char * right_text ,
                  std::function < void() > on_left = []() { } ,
                  std::function < void() > on_right = []() { } ) ;

void _snapshot ( bool force = false )
{
  const auto & args ( ini::args ) ;

  // put together a name for the snapshot file

  std::string next_name ;
  if ( args.snapshot_basename.size() )
  {
    next_name =   args.snapshot_basename
                + ui::snapshot_extension ;
  }
  else
  {
    next_name =   ui::snapshot_prefix
                + std::to_string ( ui::serial_number )
                + ui::snapshot_extension ;
  }

  if ( force == false )
  {
    FILE * fp_test = fopen ( next_name.c_str() , "rb" ) ;
    if ( fp_test != NULL )
    {
      fclose ( fp_test ) ;
      std::cout << "snapshot: file exists: " << next_name << std::endl ;

      ui::p_screen->p_window->setMouseCursorVisible ( true ) ;
      show_popup ( "snapshot: file exists:" ,
                   next_name.c_str() ,
                   "overwrite" ,
                   "cancel" ,
                   [&]() { _snapshot ( true ) ; } ) ;
      return ;
    }
  }

  if ( args.snapshot_basename.size() == 0 )
    ui::serial_number++ ;

  // take a snapshot of the current view

  bool frame_is_linear = true ;

  // capture the current view to an image file
  // produce the snapshot in a dedicated thread running
  // 'on the back burner' with a single worker.
  // The image is done in larger size and good quality.

  ui::snapshot = false ;
  std::cout << "producing snapshot " << next_name << std::endl ;

  // for the snapshot we want the current job redone with modified
  // parameters - we'll use the HQ interpolator always and we may
  // render a differently-sized frame

  struct job_type * p_exposure_job = mem_in()
          << new job_type ( ui::job ) ;

  p_exposure_job->created = pv_clock::now() ;
  p_exposure_job->frame.wait_for_stage = 3 ; // require fully built itp

  // exposure job is to be run as a single thread in the background.
  // this way it doesn't get too much in the way of pv's workings and
  // if the snapshot was done 'while running' there will likely be
  // no discernible stutter. This seems like a redundant specification;
  // after all we launch a single thread further down to execute the
  // snapshot. But the snapshot payload code will eventually call
  // vspline::transform, and this is where p_exposure_job->njobs will
  // take effect by launching the transform with one job only. Without
  // this measure we'd launch a single snapshot thread all right but
  // it would in turn launch many worker threads to get the snapshot
  // calculated.

  // ui::snapshot_threads may have been increased for this job to a value
  // greater than one (e.g. when next_after_snapshot is used). After
  // reading the datum, we set it back.

  p_exposure_job->njobs = ui::snapshot_threads ;
  ui::snapshot_threads = (   args.snapshot_threads <= 0
                            ? std::thread::hardware_concurrency()
                            : args.snapshot_threads ) ;

  // for notational convenience:

  frame_type & frame ( p_exposure_job->frame ) ;
  target_type & target ( p_exposure_job->target ) ;

  int snap_width ;
  int snap_height ;

  frame.solo = ui::solo ;
  frame.heal = ui::heal ;
  frame.use_rank = ui::use_rank ;

  // output_magnification is applied unconditionally

  if ( args.output_magnification != 1.0 )
  {
    // optionally use output magnification - calling 'inflate'
    // modifies 'target'.

    p_exposure_job->inflate ( args.output_magnification ) ;
  }

  if ( ui::use_nonstandard_target || args.snapshot_like_source )
  {
    // snapshot_like_source sets snapshot mode to 'echo' the
    // source. It's used in conjunction with next_after_snapshot;
    // for interactive use ui::use_nonstandard_target has the
    // same effect, but is transitory and set in on_snapshot,
    // or on_facet_echo for each invocation via E/Shift+E

    // ui::nonstandard_target and  ui::nonstandard_orientation have been
    // preset with the metrics to use for this special snapshot,
    // which produces an image 'like' the source (or the first
    // facet, respectively). Note that 'output magnification'
    // is still applied. The default value for this magnification
    // is 1.0, so 'source-like' snapshots will have the same
    // extent as the source by default. Using different output
    // magnification values will produce a resized snapshot, but
    // still with the correct aspect ratio. This is useful for
    // producing snapshots for web export, where the full size
    // of the source image is oftentimes overkill.

    target = ui::nonstandard_target ;
    frame.orientation = ui::nonstandard_orientation ;
    frame.zoom_factor = 1.0 ;

    if ( args.have_crop )
    {
      // if output needs cropping, set the relevant members in target

      target.cropping_active = true ;
      target.crop_x = args.p_crop_x0 ;
      target.crop_y = args.p_crop_y0 ;
      target.crop_width = args.p_crop_x1 - args.p_crop_x0 ;
      target.crop_height = args.p_crop_y1 - args.p_crop_y0 ;
    }

    // set snap_width and snap_height

    if ( args.have_crop )
    {
      snap_width = target.crop_width ;
      snap_height = target.crop_height ;
    }
    else
    {
      snap_width = target.width ;
      snap_height = target.height ;
    }

  }
  else
  {
    p_exposure_job->target = target ;

    // apply snapshot magnification to target size. This result in
    // the production of an enlarged image if snapshot_magnification
    // is greater than one. snap_width and snap_height are taken from
    // the target after the magnification was applied.

    p_exposure_job->inflate ( ui::snapshot_magnification ) ;
    snap_width = target.width ;
    snap_height = target.height ;
  }

  // we want the frame rendered as float RGB(A), rather than as 8bit
  // RGBA (format 0) which is produced for displaying on screen
  // TODO: currently, no alpha processing on cubemaps

  if ( frame.p_itp->mode == WITH_ALPHA )
  {
    frame.format = FRAME_FRGBA ;

    frame.p_frgba = mem_in() << new frgba_image_type
      ( vigra::Shape2 ( snap_width , snap_height ) ) ;
  }
  else
  {
    frame.format = FRAME_FRGB ;

    frame.p_frgb = mem_in() << new frgb_image_type
      ( vigra::Shape2 ( snap_width , snap_height ) ) ;
  }

  // TODO: copy this logic to stitch job, maybe factor out.

  if (    ui::snapshot_extension == std::string ( ".exr" )
        || ui::snapshot_extension == std::string ( ".EXR" ) )
  {
    // for EXR files, do not apply sRGB curve. IF process_linear
    // was false, we have to convert to linear RGB further down

    frame.light_settings.apply_gradation = false ;
    frame.light_settings.cap_brightness = false ;
    frame_is_linear = true ;
  }
  else if (    ui::snapshot_extension == std::string ( ".jpg" )
            || ui::snapshot_extension == std::string ( ".JPG" )
            || ui::snapshot_extension == std::string ( ".png" )
            || ui::snapshot_extension == std::string ( ".PNG" ) )
  {
    frame.light_settings.apply_gradation = ui::process_linear ;
    frame.light_settings.cap_brightness = true ;
    frame_is_linear = false ;
  }
  else if (    ui::snapshot_extension == std::string ( ".tif" )
            || ui::snapshot_extension == std::string ( ".TIF" ) )
  {
    frame.light_settings.cap_brightness = true ;

    if ( args.snapshot_tiff_linear )
    {
      assert ( ui::process_linear ) ;
      frame.light_settings.apply_gradation = false ;
      frame_is_linear = true ;
    }
    else
    {
      frame.light_settings.apply_gradation = ui::process_linear ;
      frame_is_linear = false ;
    }
  }
  else
  {
    // format not supported

    std::cerr << "can't store to format '"
              << ui::snapshot_extension << "'" << std::endl ;
  }

  // using 'SINGLE' mode, which renders just this single frame

  frame.mode = SINGLE ;
  frame.blending = ui::blending ;
  frame.tonemap = ui::tonemap ;
  frame.feathering = ui::feathering ;

  // if we have an HQ interpolator, we use it for the snapshot.
  // otherwise we fall back to the LQ interpolator. In both cases,
  // we doublecheck to make sure the desired level is present.

  // use hq interpolator for snapshots (if possible)

  frame.hq = true ;

  // set up a payload routine for the exposure thread and increase
  // jobs_pending atomically to keep a handle on snapshot progress

  ++snapshots_pending ;
  status_flag = true ;
  job_begins() ;

  auto payload = [=]()
  {
    // render the frame

    frame.p_itp->process_job ( p_exposure_job ) ;

    store_rendered_image ( p_exposure_job ,
                            frame.p_frgb ,
                            frame.p_frgba ,
                            frame_is_linear ,
                            next_name ) ;
                          
      // destroy the RGB(A) data

    if ( frame.p_itp->mode == WITH_ALPHA )
    {
      memlog >> ((frgba_image_type*) frame.p_frgba) ;
    }
    else
    {
      memlog >> ((frgb_image_type*) frame.p_frgb) ;
    }

    // delete the job_type object

    memlog >> p_exposure_job ;

    // atomically decrease jobs_pending

    job_ends() ;
    --snapshots_pending ;
    status_flag = true ;
  } ;

  // we use vspline's common thread pool to launch this thread
  // singly, so that it's crowded out by other workload which will
  // be distributed to many more threads. This way the snapshot is
  // being made 'on the back burner' and the remainder of the
  // program will hardly be affected.

  dispatcher->launch ( payload ) ;
}

void on_snapshot()
{
  use_nonstandard_target = false ;
  _snapshot() ;
}

void on_facet_echo()
{
  // take a 'source-like' snapshot using 'nonstandard target'

  use_nonstandard_target = true ;
  _snapshot() ;
}


void _exposure_fusion ( bool force = false )
{
  const auto & args ( ini::args ) ;

  std::string next_name ;
  if ( args.snapshot_basename.size() )
  {
    next_name =   args.snapshot_basename
                + ui::snapshot_extension ;
  }
  else
  {
    next_name =   ui::snapshot_prefix
                + std::to_string ( ui::serial_number )
                + ".fused."
                + args.snapshot_extension ;
  }

  if ( force == false )
  {
    FILE * fp_test = fopen ( next_name.c_str() , "rb" ) ;
    if ( fp_test != NULL )
    {
      fclose ( fp_test ) ;
      std::cout << "exposure fusion: file exists: "
                << next_name << std::endl ;

      ui::p_screen->p_window->setMouseCursorVisible ( true ) ;
      show_popup ( "exposure fusion: file exists:" ,
                  next_name.c_str() ,
                  "overwrite" ,
                  "cancel" ,
                  [&]() { _exposure_fusion ( true ) ; }
                  ) ;
    return ;
    }
  }

  if ( args.snapshot_basename.size() == 0 )
    ui::serial_number++ ;

  ui::exposure_fusion = false ;

  int nfacets = args.facet.size() ; // may be zero

  // set up a job_type object for the 'baseline' image

  // capture the current view to a float RGB frame, with no
  // further processing - this captures a geometrically transformed
  // rendering using the same interpolator as the current frame,
  // without any gradation applied, and in linear RGB if internal
  // processing is done in linear.

  struct job_type *p_baseline_job = mem_in() << new
    job_type ( ui::job ) ;

  p_baseline_job->created = pv_clock::now() ;

  frame_type & frame ( p_baseline_job->frame ) ;
  target_type & target ( p_baseline_job->target ) ;

  // take note of the brightness with which the 'mother' job
  // was rendered. We'll apply this to the partials, to get
  // the same overall brightness as the current view
  // TODO: if 'brightness' is quite far from 1.0, measuring
  // well-exposedness without modification of mu and sigma
  // may become dodgy.

  double brightness = frame.light_settings.brighten ;
  std::vector < double > partial_brightness ;

  int npartial = 0 ;

  // if the user has passed faux_bracket_ev, we use these values,
  // if not, we calculate them from the current view's brightness
  // and the facet brightness factors

  if ( ui::faux_bracket && ( args.faux_bracket_ev.size() != 0 ) )
  {
    npartial = args.faux_bracket_ev.size() ;

    for ( int i = 0 ; i < npartial ; i++ )
    {
      float pb = pow ( 2.0 , args.faux_bracket_ev [ i ] ) ;
      partial_brightness.push_back ( pb ) ;
    }
  }
  else
  {
    // either it's not a faux bracket job, or the user hasn't passed
    // faux_bracket_ev

    assert ( nfacets ) ;
    npartial = nfacets ;

    for ( int i = 0 ; i < npartial ; i++ )
    {
      float pb = brightness / ui::source_v[i].brightness ;
      partial_brightness.push_back ( pb ) ;
    }
  }

  // we want the frame rendered as float RGB, rather than as 8bit
  // RGBA (format 0) which is produced for displaying on screen

  frame.format = FRAME_FRGB ;

  // we want partial images in sRGB, not linear RGB. AFAICT,
  // the fusion algorithm doesn't work with linear images.
  // but we render to RGB first and convert after, using the
  // 'true' conversion function, see blow

  frame.light_settings.apply_gradation = false ;
  frame.light_settings.cap_brightness = false ;

  // tentative introduction of 'hdr spread'. This is a measure
  // for the width of the dynamic range which exposure fusions
  // will retain from the original range of the bracket.
  // A value of one will retain the entire dynamic range, in
  // effect producing an HDR blend. A value of zero will compress
  // the dynamic range completely into the 'standard' range of
  // [0-255], which is a 'normal' exposure fusion.

  frame.light_settings.hdr_spread = args.hdr_spread ;

  // using 'SINGLE' mode, which renders just this single frame

  frame.blending = ui::blending ;
  frame.tonemap = ui::tonemap ;
  frame.feathering = ui::feathering ;

  frame.mode = SINGLE ;
  frame.hq = true ;

  if ( ui::use_nonstandard_target || args.snapshot_like_source )
  {
    // we want to represent roughly the whole source image, rather
    // than looking at the current view. This choice is debatable;
    // one might let the user choose an ROI which she wants to
    // use to fix the brightness values, but most of the time
    // this operation will be batched anyway, and if the user had
    // a choice it would usually be to process the 'whole' image
    // anyway. So here goes:

    target = ui::nonstandard_target ;

    if ( args.have_crop )
    {
      target.cropping_active = true ;
      target.crop_x = args.p_crop_x0 ;
      target.crop_y = args.p_crop_y0 ;
      target.crop_width = args.p_crop_x1 - args.p_crop_x0 ;
      target.crop_height = args.p_crop_y1 - args.p_crop_y0 ;
    }

    // ui::nonstandard_orientation will usually be the orientation of
    // facet 0 - or the facet passed by snapshot_facet. It will
    // be the same for all the frames in the series - the 'solo'
    // argument passed below does not affect this datum

    frame.orientation = ui::nonstandard_orientation ;

    // zoom factor 1.0 pertains to full-size images. To be really
    // precise we have to look at the images in original size.
    // Even then the result can't be perfect, because only one
    // facet's pixels will be looked at without interpolation,
    // but at 100% magnification and with the capping used in
    // calculate_brightness the result is sufficiently precise
    // - or so I reckon ;)

    frame.zoom_factor = 1.0 ;

    p_baseline_job->inflate ( args.output_magnification ) ;
  }
  else
  {
    // apply snapshot magnification to target size. This result in
    // the production of an enlarged image if snapshot_magnification
    // is greater than one.

    p_baseline_job->inflate ( ui::snapshot_magnification ) ;
  }

  // ui::snapshot_threads may have been increased for this job to
  // a value greater than one (e.g. when next_after_snapshot is used).
  // Once we're done, we reset it.

  p_baseline_job->njobs = ui::snapshot_threads ;

  // TODO: move the quality detection code into a separate routine.
  // Then call that routine for each partial, add it's result to
  // a sum-up frame, but feed the partial and that mask to the
  // fusion code singly, like for stitching. Then, when all partials
  // are done, divide the result by the sum of the masks. The result
  // should be the same as first normailzing the masks - or even
  // better, due to less error accumulation.

  // TODO: put the part of class vspline::bspline which does not
  // depend on ISA-specific code in a separate namespace which can be
  // shared by the rendering and non-rendering code, so that we can
  // pass bspline objects to and fro.

  blending_settings_type & bls ( frame.blending_settings ) ;
  
  bls.mode = (   ui::faux_bracket
                ? FAUX_BRACKET
                : EXPOSURE_FUSION ) ;

  bls.nfacets = nfacets ;
  bls.npartial = npartial ;
  
  bls.exposure_weight = args.exposure_weight ;
  bls.contrast_weight = args.contrast_weight ;
  bls.exposure_sigma = args.exposure_sigma ;
  bls.exposure_mu = args.exposure_mu ;

  bls.scaling_step = args.exposure_scaling_step ;
  bls.pyramid_floor = args.exposure_pyramid_floor ;

  bls.i_spline_degree = args.bls_i_spline_degree ;
  bls.i_spline_decimator = args.bls_i_spline_decimator ;
  bls.i_spline_shift_to = args.bls_i_spline_shift_to ;

  bls.q_spline_degree = args.bls_q_spline_degree ;
  bls.q_spline_decimator = args.bls_q_spline_decimator ;
  bls.q_spline_shift_to = args.bls_q_spline_shift_to ;

  bls.partial_brightness = partial_brightness ;

  bls.process_linear = ui::process_linear ;
  bls.filename = next_name ;

  job_begins() ;
  ++snapshots_pending ;
  status_flag = true ;

  auto payload = [=]()
  {
    // launch the exposure fusion job

    dispatcher->process_fusion_job ( p_baseline_job ) ;

    // delete the job_type object

    memlog >> p_baseline_job ;

    // one more job done...

    job_ends() ;
    --snapshots_pending ;
    status_flag = true ;

  } ; // end of payload

  // launch a thread to process the payload in the background

  dispatcher->launch ( payload ) ;

  // reset snapshot_threads, in case it was altered.

  ui::snapshot_threads = (   args.snapshot_threads <= 0
                            ? std::thread::hardware_concurrency()
                            : args.snapshot_threads ) ;
}

void on_exposure_fusion()
{
  use_nonstandard_target = false ;
  _exposure_fusion() ;
}

void on_exposure_echo()
{
  use_nonstandard_target = true ;
  _exposure_fusion() ;
}

void _stitch_images ( bool force = false )
{
  const auto & args ( ini::args ) ;

  // ui::serial_number++ ;

  std::string next_name ;
  if ( args.snapshot_basename.size() )
  {
    next_name =   args.snapshot_basename
                + ui::snapshot_extension ;
  }
  else
  {
    next_name =   ui::snapshot_prefix
                + std::to_string ( ui::serial_number )
                + ".pano."
                + args.snapshot_extension ;
  }

  if ( force == false )
  {
    FILE * fp_test = fopen ( next_name.c_str() , "rb" ) ;
    if ( fp_test != NULL )
    {
      fclose ( fp_test ) ;

      std::cout << "stitch images: file exists: "
                << next_name << std::endl ;

      ui::p_screen->p_window->setMouseCursorVisible ( true ) ;
      show_popup ( "stitch images: file exists:" ,
                  next_name.c_str() ,
                  "overwrite" ,
                  "cancel" ,
                  [&]() { _stitch_images ( true ) ; }
                  ) ;
      return ;
    }
  }

  if ( args.snapshot_basename.size() == 0 )
    ui::serial_number++ ;

  ui::stitch_images = false ;

  // TODO: might simply do a snapshot if no facet map

  if ( projection == FACET_MAP )
  {
    int nfacets = args.facet.size() ;

    // capture the current view to a float RGB frame, with no
    // further processing - this captures a geometrically transformed
    // rendering using the same interpolator as the current frame,
    // without any gradation applied, and in linear RGB if internal
    // processing is done in linear.

    struct job_type * p_raw_job = mem_in() << new
      job_type ( ui::job ) ;

    p_raw_job->created = pv_clock::now() ;

    frame_type & frame ( p_raw_job->frame ) ;
    target_type & target ( p_raw_job->target ) ;

    // we want the frame rendered as float RGB, rather than as 8bit
    // RGBA (format 0) which is produced for displaying on screen

    frame.format = FRAME_FRGB ;
    
    // the rendering will be done in linear RGB and converted after,
    // for most accurate results and wider dynamic range

    frame.light_settings.apply_gradation = false ;
    frame.light_settings.cap_brightness = false ;
    frame.light_settings.hdr_spread = args.hdr_spread ;

    // using 'SINGLE' mode, which renders just this single frame

    frame.blending = ui::blending ;
    frame.tonemap = ui::tonemap ;
    frame.feathering = ui::feathering ;

    frame.mode = SINGLE ;
    frame.hq = true ;

    if ( ui::use_nonstandard_target || args.snapshot_like_source )
    {
      // we want to represent roughly the whole source image, rather
      // than looking at the current view. This choice is debatable;
      // one might let the user choose an ROI which she wants to
      // use to fix the brightness values, but most of the time
      // this operation will be batched anyway, and if the user had
      // a choice it would usually be to process the 'whole' image
      // anyway. So here goes:

      target = ui::nonstandard_target ;

      if ( args.have_crop )
      {
        target.cropping_active = true ;
        target.crop_x = args.p_crop_x0 ;
        target.crop_y = args.p_crop_y0 ;
        target.crop_width = args.p_crop_x1 - args.p_crop_x0 ;
        target.crop_height = args.p_crop_y1 - args.p_crop_y0 ;
      }

      // ui::nonstandard_orientation will usually be the orientation of
      // facet 0 - or the facet passed by snapshot_facet. It will
      // be the same for all the frames in the series - the 'solo'
      // argument passed below does not affect this datum

      frame.orientation = ui::nonstandard_orientation ;

      // zoom factor 1.0 pertains to full-size images. To be really
      // precise we have to look at the images in original size.
      // Even then the result can't be perfect, because only one
      // facet's pixels will be looked at without interpolation,
      // but at 100% magnification and with the capping used in
      // calculate_brightness the result is sufficiently precise
      // - or so I reckon ;)

      frame.zoom_factor = 1.0 ;

      p_raw_job->inflate ( args.output_magnification ) ;
    }
    else
    {
      // apply snapshot magnification to target size. This result in
      // the production of an enlarged image if snapshot_magnification
      // is greater than one.

      p_raw_job->inflate ( ui::snapshot_magnification ) ;
    }


    // ui::snapshot_threads may have been increased for this job to
    // a value greater than one (e.g. when next_after_snapshot is used).
    // Once we're done, we set it back to one.

    p_raw_job->njobs = ui::snapshot_threads ;

    double brightness = frame.light_settings.brighten ;
    std::vector < double > partial_brightness ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      float pb = brightness / ui::source_v[i].brightness ;
      partial_brightness.push_back ( pb ) ;
    }

    blending_settings_type & bls ( frame.blending_settings ) ;
    
    bls.mode = ( STITCH_IMAGES ) ;

    bls.stacking = ui::stacking ;

    bls.nfacets = nfacets ;
    bls.npartial = nfacets ;
    
    bls.exposure_weight = args.exposure_weight ;
    bls.contrast_weight = args.contrast_weight ;
    bls.exposure_sigma = args.exposure_sigma ;
    bls.exposure_mu = args.exposure_mu ;

    bls.scaling_step = args.exposure_scaling_step ;
    bls.pyramid_floor = args.exposure_pyramid_floor ;

    bls.i_spline_degree = args.bls_i_spline_degree ;
    bls.i_spline_decimator = args.bls_i_spline_decimator ;
    bls.i_spline_shift_to = args.bls_i_spline_shift_to ;

    bls.q_spline_degree = args.bls_q_spline_degree ;
    bls.q_spline_decimator = args.bls_q_spline_decimator ;
    bls.q_spline_shift_to = args.bls_q_spline_shift_to ;

    bls.partial_brightness = partial_brightness ;

    bls.process_linear = ui::process_linear ;
    bls.filename = next_name ;

    job_begins() ;
    ++snapshots_pending ;
    status_flag = true ;

    auto payload = [=]()
    {
      // launch the stitching job

      dispatcher->process_stitching_job ( p_raw_job ) ;

      // delete the job_type object

      memlog >> p_raw_job ;

      // one more job done...

      job_ends() ;
      --snapshots_pending ;
      status_flag = true ;

    } ; // end of payload

    // launch a thread to process the payload in the background

    dispatcher->launch ( payload ) ;

    // reset snapshot_threads, in case it was altered.

    ui::snapshot_threads = (   args.snapshot_threads <= 0
                              ? std::thread::hardware_concurrency()
                              : args.snapshot_threads ) ;
  }
}

void on_stitch_images()
{
  use_nonstandard_target = false ;
  _stitch_images() ;
}

void on_stitch_echo()
{
  use_nonstandard_target = true ;
  _stitch_images() ;
}

void on_view_snapshot()
{
  switch ( ui::blending )
  {
    case BLEND_RANKED: // used for panoramas
    {
      if ( ini::args.snap_to_stitch )
        on_stitch_images() ;
      else
        on_snapshot() ;
      break ;
    }
    case BLEND_HDR: // for hdr merging, exposure fusion, focus stacks
    {
      if ( ini::args.snap_to_fusion )
        on_exposure_fusion() ;
      else
        on_snapshot() ;
      break ;
    }
    default: // do an 'ordinary' snapshot
    {
      on_snapshot() ;
      break ;
    }
  }
}

void on_source_like_snapshot()
{
  switch ( ui::blending )
  {
    case BLEND_RANKED: // used for panoramas
    {
      if ( ini::args.snap_to_stitch )
        on_stitch_echo() ;
      else
        on_facet_echo() ;
      break ;
    }
    case BLEND_HDR: // for hdr merging, exposure fusion, focus stacks
    {
      if ( ini::args.snap_to_fusion )
        on_exposure_echo() ;
      else
        on_facet_echo() ;
      break ;
    }
    default: // do an 'ordinary' source-like snapshot
    {
      on_facet_echo() ;
      break ;
    }
  }
}

void on_capture_wb()
{
  // use current frame to calculate new white balance
  // TODO: create and launch the job here

//   capture_white_balance = true ;
//   change.draw = true ;

  // produce a frame 'straight from the interpolator' and calculate
  // the new white balance based on the average of this frame's
  // content

  ui::capture_white_balance = false ;

  // we need to set change.draw to have a new frame made which will
  // actually use the new white balance

  change.draw = true ;

  // capture the current view to a float RGB frame, with no
  // further processing - this captures a geometrically transformed
  // rendering using the same interpolator as the current frame,
  // without any gradation applied, and in linear RGB if internal
  // processing is done in linear.

  struct job_type job ( ui::job ) ;
  frame_type & frame ( job.frame ) ;

  // we want the frame rendered as float RGB, rather than as 8bit
  // RGBA (format 0) which is produced for displaying on screen

  frame.format = FRAME_FRGB ;
  frame.light_settings = neutral_light ;

  // using 'SINGLE' mode, which renders just this single frame

  frame.mode = SINGLE ;

  // capture the result in 'sample'

  frgb_image_type sample ( target.width , target.height ) ;
  frame.p_frgb = &sample ;

  frame.p_itp->process_job ( &job ) ;

  // use the frame to calculate the new white balance

  state.white_balance = calculate_white_balance ( sample ) ;

  std::cout << "new white balance: "
            << state.white_balance << std::endl ;

}

void on_reset_wb()
{
  // reset white balance to neutral

  state.white_balance = 1.0f ;
  change.draw = true ;
}

void on_capture_light()
{
  signal_light_balance ( true ) ;
  calculate_brightness() ;
  signal_light_balance ( false ) ;
}

void on_toggle_tonemap()
{
  tonemap = ! tonemap ;
  change.draw = true ;
}

void on_toggle_heal()
{
  heal = ! heal ;
  change.draw = true ;
}

void on_reset_light()
{
  // reset white balance, brightness, black and white point
  change.draw = true ;
  state.white_balance = 1.0f ;
  reset_brightness ( state ) ;
  reset_black_point ( state ) ;
  reset_white_point ( state ) ;
}

void on_shift_up()
{
  // shift spline up (results in softening)
  
  if ( p_itp )
  {
    change.draw = true ;
    if ( mode == SINGLE )
      p_itp->shift ( 1 , 1 ) ;
    else
      p_itp->shift ( 0 , 1 ) ;
  }
} 

void on_shift_down()
{
  // shift spline down (results in sharpening)
  
  if ( p_itp )
  {
    change.draw = true ;
    if ( mode == SINGLE )
      p_itp->shift ( 1 , -1 ) ;
    else
      p_itp->shift ( 0 , -1 ) ;
  }
}

void on_toggle_snap_to_hq()
{
  // toggle snap_to_hq_interpolator. This flag, when true, causes pv to switch
  // to the high quality interpolator whenever it becomes idle. This will usually
  // be desirable, but when viewing scaled-down images, we get aliasing with the
  // HQ interpolator (if we're not using an image pyramid for it), so switching
  // the automatism off may be better in this situation.
  change.draw = true ;
  snap_to_hq_interpolator = ! snap_to_hq_interpolator ;
}

void on_toggle_decimator()
{
  // toggle moving image scaling mode between auto and fixed
  
  decimate_area = ! decimate_area ;
  change.draw = true ;
}

void on_auto_quality()
{
  // toggle moving image scaling mode between auto and fixed
  
  auto_quality = ! auto_quality ;
}

// magnifying glass now zooms in/out by magnifying_glass_factor,
// the operation keeping the interpolator and modifying only the
// sensor settings in now one down: on_blow_up

bool magnify_on = false ;

void on_magnifying_glass()
{
  if ( magnify_on )
    state.zoom_factor /= sensor_settings.magnifying_glass_factor ;
  else
    state.zoom_factor *= sensor_settings.magnifying_glass_factor ;
  magnify_on ^= true ;
  change.draw = true ;
}

double & blow_up ( sensor_settings.magnifying_glass ) ;

void on_blow_up()
{
  if ( sensor_settings.magnifying_glass == 1.0 )
    sensor_settings.magnifying_glass = sensor_settings.magnifying_glass_factor ;
  else
    sensor_settings.magnifying_glass = 1.0 ;
  change.draw = true ;
}

void on_100_percent()
{
  change.draw = true ;
  state.zoom_factor = 1 ;
}

void on_double_zoom()
{
  change.draw = true ;
  state.zoom_factor *= 2 ;
}

void on_halve_zoom()
{
  change.draw = true ;
  state.zoom_factor = std::max ( state.zoom_factor *= .5 ,
                                 zoom_min + .1 ) ;
}

void on_level_on_horizon()
{
  // set camera rotation to zero and center vertically

  state.dr = 0.0 ;

  change.draw = true ;
  if ( projection == MOSAIC )
  {
    state.dy = 0.0 ;
    state.dr = 0.0 ;
  }
  else
  {
    double y, p, r ;
    toEulerAngle ( state.orientation , y , p , r ) ;
    // set pitch and roll to zero, keep yaw
    state.orientation = get_rotation_q ( -y , 0.0 , 0.0 ) ;
  }
}

void on_level_out()
{
  // set camera rotation to zero. like above, but keeps pitch

  state.dr = 0.0 ;

  change.draw = true ;
  if ( projection == MOSAIC )
  {
    state.dr = 0.0 ;
  }
  else
  {
    double y, p, r ;
    toEulerAngle ( state.orientation , y , p , r ) ;
    // set roll to zero, keep yaw and pitch
    state.orientation = get_rotation_q ( -y , p , 0.0 ) ;
  }
}

void on_readjust()
{
  // set camera rotation to state.dr

  if ( projection != MOSAIC )
  {
    change.draw = true ;
    double y, p, r ;
    toEulerAngle ( state.orientation , y , p , r ) ;
    // set roll to dr, keep yaw and pitch
    state.orientation = get_rotation_q ( -y , p , state.dr ) ;
  }
}

void on_load_images()
{
  // show a file select dialog

  sf::RenderWindow * & p_window ( p_screen->p_window ) ;

  bool return_to_fullscreen = false ;

  if ( run_fullscreen )
  {
    // the tinyfiledialogs call below does not succeed
    // over a full-screen view

    // p_window->setActive ( false ) ;
    p_window->close() ;
    // ui::p_screen->close_window() ;
    return_to_fullscreen = true ;
  }

  auto selection = select_files ( ini::args.path ) ;

  if ( return_to_fullscreen == true )
  {
    ui::p_screen->setup_window ( "" ) ;
  }

  if ( selection.size() )
  {
    // if the dialog yielded something, we terminate
    // so that 'outer' main can start afresh with the new files
    // which we've pushed to the front of the queue to be used next

    for ( int i = selection.size() -1 ;
          i >= 0 ;
          --i )
    {
      // last one first!
      file_queue.push_front ( selection[i] ) ;
    }

    mode = TERMINATE ;
    relaunch = false ;
  }
}

// chronic user input is handled by functions with a 'do_' prefix to keep
// them apart from the functions handling acute input.

void do_increase_autopan()
{
  // increase speed of autopan

  accelerate ( state , change , 5 * snappiness ) ;
}

void do_decrease_autopan()
{
  // decrease speed of autopan

  accelerate ( state , change , -5 * snappiness ) ;
}

void do_increase_snappiness()
{
  // increase snappiness

  snappiness *= 1.01 ;
}

void do_decrease_snappiness()
{
  // decrease snappiness

  if ( snappiness >= 0.0005 )
    snappiness *= .99 ;
}

// affect a continuous zoom

void do_zoom_in()
{
  active_change = true ;
  zoom ( state , change , 2 * snappiness , zoom_min ) ;
}

void do_zoom_out()
{
  active_change = true ;
  zoom ( state , change , -2 * snappiness , zoom_min ) ;
}

// modify animation quality

void do_aq_up()
{
  active_change = true ;
  change.moving_image_scaling = .003 ;
}

void do_aq_down()
{
  active_change = true ;
  change.moving_image_scaling = - .003 ;
}

void do_brightness_up()
{
  change_brightness ( state , change , snappiness * .5 ) ;
}

void do_brightness_down()
{
  change_brightness ( state , change , - snappiness *.5 ) ;
}

void do_black_up()
{
  change_black_point ( state , change ,
                       process_linear ? 10 * snappiness : 30 * snappiness ) ;
}

void do_black_down()
{
  change_black_point ( state , change ,
                       process_linear ? -10 * snappiness : - 30 * snappiness ) ;
}

void do_white_up()
{
  change_white_point ( state , change , 100 * snappiness ) ;
}

void do_white_down()
{
  change_white_point ( state , change , -100 * snappiness ) ;
}

void do_camera_left()
{
  active_change = true ;

  // go left. If lock_vertical is not set, the move is performed relative
  // to the current view's center. If it's set, the move is performed
  // relative to the anchor point defined by the current bias. This is the
  // default, because panning relative to the current view can tilt the
  // horizon and therefore has to be explicitly activated. It might be
  // better to do this via the shift key...
  // Why allow the other mode? To allow natural handling of views near
  // the poles.
  // typical case of fov-dependent chronic user input:
  // 'pan' is an angular measure, so we need 'angular_intensity' here.

  if ( lock_vertical )
    yaw ( state , change , -angular_intensity ) ;
  else
    pan ( state , change , -angular_intensity ) ;
}

void do_camera_right()
{
  active_change = true ;
  if ( lock_vertical )
    yaw ( state , change , angular_intensity ) ;
  else
    pan ( state , change , angular_intensity ) ;
}

void do_camera_up()
{
  active_change = true ;
  // tilt upwards. The tilt is always performed relative to the
  // current view's center, because, while anywhere but at the current
  // bias position, affecting a bias-relative tilt would feel strange.
  tilt ( state , change , angular_intensity ) ;
}

void do_camera_down()
{
  active_change = true ;
  tilt ( state , change , - angular_intensity ) ;
}

void do_rotate_clockwise()
{
  active_change = true ;
  // rotate clockwise. The rotation is always performed relative
  // to the current view's center, for the same reason as above
  // even though we modify an angular measure (roll), this is *not*
  // influenced by the target's hfov, so we use snappiness as intensity:
  turn ( state , change , - snappiness ) ;
}

void do_rotate_counter_clockwise()
{
  active_change = true ;
  // rotate clockwise. The rotation is always performed relative
  // to the current view's center, for the same reason as above
  // even though we modify an angular measure (roll), this is *not*
  // influenced by the target's hfov, so we use snappiness as intensity:
  turn ( state , change , snappiness ) ;
}

void do_raise_viewpoint()
{
  if (    projection != FISHEYE
       && projection != MOSAIC
       && projection != STEREOGRAPHIC )
  {
    active_change = true ;
    rise ( state , change , angular_intensity * .1 ) ;
  }
}

void do_lower_viewpoint()
{
  if (    projection != FISHEYE
       && projection != MOSAIC
       && projection != STEREOGRAPHIC )
  {
    active_change = true ;
    rise ( state , change , - angular_intensity * .1 ) ;
  }
}

void do_sensor_shift_x_up()
{
  active_change = true ;
  tilt_sensor ( state , change , 0.0 , snappiness * .3 ) ;
}

void do_sensor_shift_x_down()
{
  active_change = true ;
  tilt_sensor ( state , change , 0.0 , - snappiness * .3 ) ;
}

void do_sensor_shift_y_up()
{
  active_change = true ;
  tilt_sensor ( state , change , snappiness * .3 , 0.0 ) ;
}

void do_sensor_shift_y_down()
{
  active_change = true ;
  tilt_sensor ( state , change , - snappiness * .3 , 0.0 ) ;
}

extern void on_test_popup() ;

void init()
{
  // initialize the jump table for key event triggered functions
  // to 'ignore', which does nothing

  for ( int key = 0 ; key < 2 * sf::Keyboard::KeyCount ; key++ )
  {
    keyboard_triggered [ key ] = ignore ;
    ckeyboard_triggered [ key ] = ignore ;
  }

  // now bind key events and trigger functions. There are two
  // tables to fill: one for acute and one for chronic key input.
  // This is done with bind and cbind, respectively. To have a
  // nice list where keys are alphabetically ordered, I've mixed
  // the bind and cbind invocations and ordered by key.
  // There are some redundancies; I found that ::Add and ::Subtract
  // aren't always found (seems to need the Num Pad keys + and -)
  // So I have 'z' and 'shift+z' to stand in for them.

   bind ( sf::Keyboard::F1 , on_show_again ) ;
   bind ( sf::Keyboard::F2 , on_toggle_lock_vertical ) ;
  cbind ( sf::Keyboard::F3 , do_black_up ) ;
  cbind ( sf::Keyboard::F3 , do_black_down , true ) ;
  cbind ( sf::Keyboard::F4 , do_white_down ) ;
  cbind ( sf::Keyboard::F4 , do_white_up , true ) ;
  cbind ( sf::Keyboard::F5 , do_brightness_down ) ;
  cbind ( sf::Keyboard::F6 , do_brightness_up ) ;
   bind ( sf::Keyboard::F7 , on_reset_light ) ;
   bind ( sf::Keyboard::F8 , on_toggle_allow_pan_mode ) ;
   bind ( sf::Keyboard::F9 , on_toggle_tonemap ) ;
   bind ( sf::Keyboard::F10 , on_set_bias ) ;
   bind ( sf::Keyboard::F11 , on_toggle_fullscreen ) ;
   bind ( sf::Keyboard::F12 , on_toggle_snap_to_hq ) ;
  
  cbind ( sf::Keyboard::A , do_increase_autopan ) ; 
  cbind ( sf::Keyboard::A , do_decrease_autopan , true ) ; 
   bind ( sf::Keyboard::B , on_toggle_fullscreen ) ;
  cbind ( sf::Keyboard::Add , do_zoom_in ) ;
#ifdef USE_IMGUI
   bind ( sf::Keyboard::C , on_toggle_legacy_gui ) ;
#endif
   bind ( sf::Keyboard::D , on_toggle_decimator ) ; 
   bind ( sf::Keyboard::D , on_toggle_facet_dry , true ) ; 
  cbind ( sf::Keyboard::Down , do_camera_down ) ;
  cbind ( sf::Keyboard::Down , do_sensor_shift_y_down , true ) ;
   bind ( sf::Keyboard::E , on_view_snapshot ) ; 
   bind ( sf::Keyboard::E , on_source_like_snapshot , true ) ;
   bind ( sf::Keyboard::Multiply , on_view_snapshot ) ; 
   bind ( sf::Keyboard::Multiply , on_source_like_snapshot , true ) ;
   bind ( sf::Keyboard::End , on_end ) ; 
   bind ( sf::Keyboard::Escape , on_escape ) ; 
   bind ( sf::Keyboard::F , on_load_images ) ; 
   bind ( sf::Keyboard::G , on_auto_quality ) ;
   bind ( sf::Keyboard::G , on_toggle_gui_automatic, true ) ;
  cbind ( sf::Keyboard::H , do_raise_viewpoint ) ;
  cbind ( sf::Keyboard::H , do_lower_viewpoint , true ) ;
   bind ( sf::Keyboard::Home , on_home ) ; 
   bind ( sf::Keyboard::I , on_magnifying_glass ) ;
   bind ( sf::Keyboard::I , on_blow_up , true ) ;
   bind ( sf::Keyboard::J , on_level_out ) ;
   bind ( sf::Keyboard::J , on_readjust , true ) ;
   bind ( sf::Keyboard::K , on_toggle_heal ) ;
   bind ( sf::Keyboard::L , on_level_on_horizon ) ;
   bind ( sf::Keyboard::L , on_capture_light , true ) ;
  cbind ( sf::Keyboard::Left , do_camera_left ) ;
   bind ( sf::Keyboard::Left , on_solo_down , true ) ;
  cbind ( sf::Keyboard::M , do_aq_down ) ;
  cbind ( sf::Keyboard::M , do_aq_up , true ) ;
   bind ( sf::Keyboard::Num1 , on_100_percent ) ;
   bind ( sf::Keyboard::Numpad1 , on_100_percent ) ;
   bind ( sf::Keyboard::Num2 , on_double_zoom ) ;
   bind ( sf::Keyboard::Numpad2 , on_double_zoom ) ;
   bind ( sf::Keyboard::Num3 , on_halve_zoom ) ;
   bind ( sf::Keyboard::Numpad3 , on_halve_zoom ) ;
   bind ( sf::Keyboard::Num4 , on_one_quadrant ) ;
   bind ( sf::Keyboard::Numpad4 , on_one_quadrant ) ;
   bind ( sf::Keyboard::Num9 , on_roll_90 ) ;
   bind ( sf::Keyboard::Numpad9 , on_roll_90 ) ; 
   bind ( sf::Keyboard::O , on_reverse_autopan ) ; 
   bind ( sf::Keyboard::PageDown , on_page_down ) ;
   bind ( sf::Keyboard::PageUp , on_page_up ) ;
   bind ( sf::Keyboard::Q , on_level_bias_up ) ;
   bind ( sf::Keyboard::Q , on_level_bias_down , true ) ;
  cbind ( sf::Keyboard::R , do_rotate_clockwise ) ;
  cbind ( sf::Keyboard::R , do_rotate_counter_clockwise , true ) ;
   bind ( sf::Keyboard::Return , on_reset ) ;
  cbind ( sf::Keyboard::Right , do_camera_right ) ;
   bind ( sf::Keyboard::Right , on_solo_up , true ) ;
   bind ( sf::Keyboard::S , on_shift_up ) ;
   bind ( sf::Keyboard::S , on_shift_down , true ) ; 
  cbind ( sf::Keyboard::Subtract , do_zoom_out ) ;
   bind ( sf::Keyboard::Space , on_toggle_autopan ) ;
   bind ( sf::Keyboard::Tab , on_next_image ) ;
   bind ( sf::Keyboard::Tab , on_previous_image , true ) ; // Shift + Tab
   bind ( sf::Keyboard::T , on_step_back , true ) ; // Shift + T
   bind ( sf::Keyboard::T , on_step_forward ) ;
  cbind ( sf::Keyboard::Up , do_camera_up ) ;
  cbind ( sf::Keyboard::Up , do_sensor_shift_y_up , true ) ;
   bind ( sf::Keyboard::V , on_toggle_status ) ;
   bind ( sf::Keyboard::V , on_test_popup , true ) ;
   bind ( sf::Keyboard::W , on_capture_wb ) ;
   bind ( sf::Keyboard::W , on_reset_wb , true ) ; // Shift + W
  cbind ( sf::Keyboard::X , do_increase_snappiness ) ;
  cbind ( sf::Keyboard::X , do_decrease_snappiness , true ) ;
   bind ( sf::Keyboard::Y , on_vertical_fit ) ;
   bind ( sf::Keyboard::Y , on_horizontal_fit , true ) ; // Shift + Y
  cbind ( sf::Keyboard::Z , do_zoom_in ) ; // same as 'Add'
  cbind ( sf::Keyboard::Z , do_zoom_out , true ) ; // same as 'subtract'
}

} ; // end of namespace ui

// This function is called when an unrecoverable error is detected.
// If this occurs in the main thread, we can display a message box
// and exit. In other threads, we have to go a roundabout way and
// raise an exception, which is processed by the payload routines
// by setting error_flag and storing the message to fatal_error.
// Note that invocation is via a macro 'abort_lux' (in pv_common.h)
// Which puts together the error message and then invokes this
// function. The detour is needed to insert the source file name
// and line number into the error message.

extern char tinyfd_needs[] ;

void lux_abort ( const std::string & message )
{
  if ( std::this_thread::get_id() == main_thread_id )
  {
    std::cerr << "unrecoverable error: " << message << std::endl ;

    if ( ui::p_screen && ui::p_screen->p_window && ui::run_fullscreen )
      ui::p_screen->p_window->setVisible ( false ) ;
    
    std::string _message = message ;
    for ( auto & c : _message )
    {
      // replace quotes with white space - tinyfiledialogs can't
      // handle quotes in the message
      if ( c == '"' || c == '\'' )
        c = ' ' ;
    }
    
    tinyfd_messageBox( "Lux - Unrecoverable Error",
                       _message.c_str() ,
                      "ok",
                      "error",
                      1 ) ;

    // TODO: this is a bit weak: might produce more meaningful
    // exit codes depending on the cause of the abortion.

    std::exit ( -1 ) ;
  }

  throw std::runtime_error ( message ) ;
}

void lux_error ( std::string message , bool force = false )
{
  if ( ( ! ui::show_error_dialog ) && ( ! force ) )
  {
    std::cerr << "ignoring error: " << message << std::endl ;
    return ;
  }

  assert ( std::this_thread::get_id() == main_thread_id ) ;

  std::cerr << "recoverable error: " << message << std::endl ;

  bool hidden = false ;

  if ( ui::p_screen && ui::p_screen->p_window && ui::run_fullscreen )
  {
    ui::p_screen->p_window->setVisible ( false ) ;
    hidden = true ;
  }

  std::string _message = message ;
  for ( auto & c : _message )
  {
    // replace quotes with white space - tinyfiledialogs can't
    // handle quotes in the message
    if ( c == '"' || c == '\'' )
      c = ' ' ;
  }
  _message += std::string ( "\nignore error and continue?" ) ;

  auto result = tinyfd_messageBox( "Lux Error",
                                   _message.c_str() ,
                                   "yesno",
                                   "error",
                                   1 ) ;

  if ( result == 0 )
  {
    std::cerr << "user chooses to end session" << std::endl ;
    std::exit ( -1 ) ;
  }

  std::cerr << "user ignores error and continues" << std::endl ;

  if ( hidden )
    ui::p_screen->p_window->setVisible ( true ) ;
}

// calculate_brightness correlates brightness values between all facets
// in a facet map. First, a separate job is rendered for each facet which
// has metrics to cover that facet only. Each of these jobs will produce
// correlation values where other facets overlap. In the end we'll have
// looked at each overlap several times, but at slightly different pixel
// positions, because the 'lead' facet determines the precise sampling
// points.
// Once we have the raw data, we proceed to calculate the factors we
// need to make all facets as 'balanced' as possible and apply these
// factors as a set of overrides.

void calculate_brightness()
{
  // if calculate_brightness is called, we override the initial
  // value of ui::light_balance (gleaned from the command line
  // argument --light_balance=...) and set it to "auto", because
  // otherwise the laborious calculation would not make any sense.

  ui::light_balance = "auto" ;

  int nfacets = ui::source_v.size() ;
  vigra::Shape2 cref_shape ( nfacets , nfacets ) ;
  
  job_type job ;
  job.created = pv_clock::now() ;
  frame_type & frame ( job.frame ) ;
  target_type & target ( job.target ) ;

  frame.format = FRAME_FRGB ;
  frame.blending = BLEND_CORRELATE ;
  frame.hq = false ;
  frame.mode = SINGLE ;
  frame.p_itp = ui::p_itp ;
  // stage two would be enough, but waiting for stage 3 makes sure that
  // all interplator-building has finished before this coputationally
  // expensive task is started.
  frame.wait_for_stage = 3 ;
  // zoom factor is set per-facet
  frame.level_bias = 0 ;
  frame.solo = -1 ;
  frame.mask_for = -1 ;
  frame.mask_stack = -1 ;
  frame.use_rank = false ;
  frame.feathering = 0.0 ;
  frame.tonemap = false ;
  frame.stack_only = 0 ;
  frame.light_settings = neutral_light ;
  frame.sensor_settings = sensor_settings_type() ;
  frame.blending_settings = blending_settings_type() ;

  // The array to receive the cross-reference values is
  // attached to the job's dock

  vigra::MultiArray < 2 , double > cref ( cref_shape ) ;

  job.dock.p_correlate = &cref ;

  for ( int fc = 0 ; fc < nfacets ; fc++ )
  {
    frame.orientation = conj ( ui::facet_orientation[fc] ) ;

    double w = ui::source_v[fc].width ;
    double h = ui::source_v[fc].height ;
    double e = 512.0 ;

    if ( w > h )
    {
      double f = e / h ;
      w *= f ;
      h = e ;
    }
    else
    {
      double f = e / w ;
      h *= f ;
      w = e ;
    }

    // for every facet, we set up a target which coincides
    // with the facet's metrics (like snapshot_facet does),
    // apart from the size, which we reduce to a 'sensible'
    // value to save processing time.

    target.projection = ui::facet_projection[fc] ;
    target.width = w ;
    target.height = h ;
    target.cropping_active = false ;
    target.hfov = ui::source_v[fc].hfov ;
    target.vfov = calculate_vfov ( target.hfov ,
                                   target.width ,
                                   target.height ,
                                   target.projection ) ;

    frame.zoom_factor = w / ui::source_v[fc].width ;

    // we render the correlation job with this target;
    // the partial result is summed up to cref.

    frame.p_itp->process_job ( &job ) ;
  }

  vigra::MultiArray < 2 , double > relation ( cref_shape ) ;

  bool tag [ nfacets ] ;
  for ( int f = 0 ; f < nfacets ; f++ )
    tag [ f ] = false ;
  bool have_tagged = false ;

  for ( int fca = 0 ; fca < nfacets - 1 ; fca++ )
  {
    for ( int fcb = fca + 1 ; fcb < nfacets ; fcb++ )
    {
      if ( cref [ { fca , fcb } ] == 0 )
      {
        // no overlap detected, we can skip this pair
        continue ;
      }

      // for these two facets we have detected overlap

      tag [ fca ] |= true ;
      tag [ fcb ] |= true ;
      have_tagged = true ;

      // we obtain the 'relation' information from the summed data,
      // which were accumulated through all the per-facet jobs

      relation [ { fca , fcb } ]
        = cref [ { fca , fcb } ] / cref [ { fcb , fca } ] ;

      relation [ { fcb , fca } ]
        = cref [ { fcb , fca } ] / cref [ { fca , fcb } ] ;
    }
  }

  // if there were no overlaps detected at all, there is nothing
  // to do, we can't form a balance.

  if ( ! have_tagged )
  {
    std::cout << "no facet overlaps detected, brightness values unchanged"
              << std::endl ;
    return ;
  }

  // now we process the 'relation' information. We interpret the
  // facet map as a graph, with the facets as knots and overlap
  // as edges. A single breadth-first traversal of the graph would
  // suffice to determine the relative brightness if the measurements
  // were perfect, but it' safer to repeat the graph traversal for
  // every single facet as starting point, which eliminates some
  // possible flaws in the measurement. Still there are flaws which
  // we can't detect, like a consistent horizontal gradient in all
  // facets of a stripe panorama - this will result in a false
  // increase in brightness from one end to the other. I don't
  // make an attempt here to deal with such problems.
  // We needn't make the graph explicit - all the information we need
  // is already in the structures we've built so far, and we just need
  // a few extra arrays to control the traversal and build up the
  // result:

  bool visited [ nfacets ] ;
  double sum_ev [ nfacets ] ;
  double current_ev [ nfacets ] ;

  for ( int f = 0 ; f < nfacets ; f++ )
    current_ev [ f ] = sum_ev [ f ] = 0.0 ;

  std::vector < int > lineup ;
  std::vector < int > new_lineup ;

  // pick each tagged facet in turn as starting point 'fc0'

  for ( int fc0 = 0 ; fc0 < nfacets ; fc0++ )
  {
    if ( ! tag [ fc0 ] )
      continue ;

    // clear 'visited' and set it only for fc0

    for ( int f = 0 ; f < nfacets ; f++ )
      visited [ f ] = false ;

    visited [ fc0 ] = true ;

    // fc0 is the only facet we've 'visited' so far; we'll put it
    // into the 'lineup', the set of facets which will be scanned
    // for viable neighbours. We assign an arbitrary initial Ev of 0;
    // we'll be using the log2 of the brightness difference to be able
    // to simply sum up and form an average.

    lineup.push_back ( fc0 ) ;
    current_ev [ fc0 ] = 0.0 ;

    // the traversal ends once the 'lineup' is empty

    while ( lineup.size() )
    {
      // look at all facets in the lineup

      for ( const auto & fca : lineup )
      {
        // any facet which is a viable neighbour will become part
        // of the lineup for the next cycle

        for ( int fcb = 0 ; fcb < nfacets ; fcb++ )
        {
          // some candidates are obviously not viable:

          if ( fca == fcb )
            continue ;

          if ( visited [ fcb ] )
            continue ;

          if ( ! tag [ fcb ] )
            continue ;

          // of the remaining candidates, we only consider those viable
          // which have shared valid pixels with 'fca', which we can
          // infer from 'cref':

          if ( cref [ { fca , fcb } ] != 0.0 )
          {
            // found viable neighbour, let's calculate the Ev, sum it up
            // and push the neighbour to the new lineup. Also set 'visited'
            // for the new neighbour.

            double evb = log2 ( relation [ { fca , fcb } ] ) ;
            current_ev [ fcb ] = current_ev [ fca ] + evb ;
            sum_ev [ fcb ] += current_ev [ fcb ] ;
            new_lineup.push_back ( fcb ) ;
            visited [ fcb ] = true ;
          }
        }
      }
      // we're done going through the current lineup, so we discard it
      // and take 'new_lineup' instead, swapping it into 'lineup' for the
      // next cycle

      lineup.clear() ;
      std::swap ( lineup , new_lineup ) ;
    }
  }

  int tagged = 0 ;
  for ( int f = 0 ; f < nfacets ; f++ )
  {
    if ( tag [ f ] )
      tagged++ ;
  }

  std::cout << "there were " << tagged << " tagged facets" << std::endl ;
  
  // we've summed up the Ev with every cycle, so now we must divide
  // by the number of tagged facets to get the average

  for ( int f = 0 ; f < nfacets ; f++ )
  {
    sum_ev [ f ] /= tagged ;
  }

  // now we convert the Ev values back to multiplicative factors
  // and use these factors as override arguments, which will
  // override the previous facet brightness values

  for ( int facet = 0 ; facet < nfacets ; facet++ )
  {
    // sum_ev has the brightness adaptation as an Ev value, which we
    // convert to a multiplicative factor. To get the new value for
    // the facet's brightness, we multiply this factor with the
    // current facet brightness value and produce the override.

    auto factor = pow ( 2.0 , sum_ev [ facet ] ) ;
    auto new_brightness = factor * ui::source_v[facet].brightness ;

    std::cout << "facet " << facet << " will be brightened by "
              << factor << std::endl << "new brightness value will be "
              << new_brightness << std::endl ;

    ui::override_args.push_back (   std::string ( "--facet_brightness=" )
                                  + std::to_string ( new_brightness ) ) ;
  }

  // finally we trigger a show-again sequence to display the effect

  ui::on_show_again() ;
}

// hfov to use for 'mosaic' images, using 90 degrees. This is just a
// reference point, flat images don't really have hfov.

double mosaic_hfov = M_PI_2 ;

// struct source_type only holds data - this is best to make it
// safe to pass it around to code using unknown ISAs, like the
// redering code: it's layout will be the same, and there are no
// methods, c'tors, d'tors etc which can throw a spanner in the
// works. But the content of source_type is complex and takes
// some effort to set up, which is tedious without methods.
// So I use 'scaffolding': an object inheriting from source_type
// which does the setting up. Elements in source_v are
// reinterpret_cast to the scaffolding type, the setup is done
// via the scaffolding, leaving the data in the 'plain' source_type
// object in source_v. As long as the scaffolding object does not
// use virtual functions, this is fine.

template < typename inner_type >
struct scaffolding_type
{ } ;

template<> struct scaffolding_type < source_type >
: public source_type
{
  void init()
  {
    // all source images are 'active' by default, this has to be switched
    // off actively to deactivate an image, and this will only ever happen
    // for facets in facet maps - for single images or cubemaps it makes
    // no sense.

    active = true ;

    // first we have essential metadata concerning extent and geometry.
    // These data can be gleaned from an image file's metadata, if they
    // are present, but may be overruled by comand line arguments.

    filename = std::string ( "" ) ;
    width = height = 0 ;
    exif_orientation = 0 ;
    projection = PRJ_NONE ;
    hofs = vofs = -1.0 ;
    hfov = vfov = 0.0 ;

    // the remainder of the member variables hold values which describe
    // the interpretation of the data by lux. They stem from command
    // line arguments or PTO or ini files, or are derived from the
    // essential metadata to make subsequent calculations simpler.

    squash = 0 ;
    brightness = 1.0 ;
    handicap = 0.0 ;
    p_priority_map = nullptr ;
    lens_correction_active = false ;
    s = a = b = c = d = h = v = 0 ;
    vignetting_correction_active = false ;
    va = vb = vc = vd = vx = vy = 0.0 ;
    extent = { 0.0 , 0.0 , 0.0 , 0.0 } ;
    r_max = 0.0 ;
    ul = ur = lr = ll = point_3d_d_type ( 1.0 , 0.0 , 0.0 ) ;
    ul2 = ur2 = lr2 = ll2 = coordinate_type ( 0.0 , 0.0 ) ;
    theta = 0.0 ;
    x_step = 0.0 ;
    full_width = full_height = is_linear = false ;
  }

  // read_metadata gleans the width, height and orientation of
  // the source image - and, optionally, all the metadata
  // concerning projection, hfov, vfov, hofs, and vofs.
  // The latter four values are only taken from the metadata
  // if read_geometry is passed 'true', and they may not be
  // present in the source file, in which case they remain at
  // default values which indicate that they were not yet set.
  // read_geometry is set to false if the projection and FOV
  // are known previously - e.g. because they were explicitly
  // given on the command line, which takes preference, or
  // because the image is a partial image of a synoptic composite,
  // which gleans the projection and FOV from the PTO or lux ini
  // file.

  void read_metadata ( const std::string & _filename ,
                       bool read_geometry = false )
  {
    fileio::image_info imageInfo = open_image_file ( _filename ) ;

    width = imageInfo.width() ;
    height = imageInfo.height() ;
    filename = _filename ;

    std::cout << "read_metadata gets width, height from image_info: "
              << width << ", " << height << std::endl ;

    // We need to look at the metadata in any case - even if the user
    // has supplied source image metrics, we need to check, e.g., if
    // there is an EXIF orientation tag set. Metadata processing is
    // now in pv_metadata.h, which provides a helper object. The last
    // argument contains a list of exif tags the user has requested
    // to be checked, so as to display the values in the status line.

    metadata_type metadata ( filename.c_str() ,
                             read_geometry ,
                             ui::metadata_query_v ,
                             ini::args.lens_crop_factor ) ;

    // we take note of an EXIF orientation tag and copy the additional
    // exif tags requested by the user

    exif_orientation = metadata.exif_orientation ;
    ui::subimages = metadata.subimages ;
    ui::metadata_result_v = metadata.metadata_value_v ;
    std::string metadata_source ;

    // if read_geometry was set, we now check what metadata were found
    // in the image and - if possible - set projection, FOV and OFS
    // accordingly.

    if ( read_geometry )
    {
      hfov = vfov = 0.0 ;
      hofs = vofs = -1.0 ;
      projection = PRJ_NONE ;

      // special case: user has set rectilinear_as_mosaic, so we
      // set metadata.projection to PRJ_NONE, effectively 'spoiling'
      // the metadata which may have been gleaned.

      if (    ini::args.rectilinear_as_mosaic
           && ( metadata.projection == RECTILINEAR ) )
      {
        metadata.projection = PRJ_NONE ;
      }

      // Now if any geometry-relevant matadata were gleaned, the
      // detection has set metadata.projection. If it's not set we
      // don't have any geometry information at all: neither via the
      // command line or PTO or lux ini file, nor via exif data. In
      // that case, projection will remain PRJ_NONE and the calling
      // code has to decide what to do about it.

      if ( metadata.projection != PRJ_NONE )
      {
        // if we have a projection from the metadata, use it.

        projection = metadata.projection ;

        if ( metadata.lux_version_string != std::string() )
        {
          // winner. we have lux metadata

          metadata_source = "lux XMP" ;
          hfov = metadata.lux_hfov ;
          vfov = metadata.lux_vfov ;
          hofs = metadata.lux_hofs ;
          vofs = metadata.lux_vofs ;
        }
        else if ( metadata.gpano_hofs != -1.0 )
        {
          // GPano is second best

          metadata_source = "GPano XMP" ;
          hfov = metadata.gpano_hfov ;
          vfov = metadata.gpano_vfov ;
          hofs = metadata.gpano_hofs ;
          vofs = metadata.gpano_vofs + M_PI_2 ;
        }
        else if ( metadata.hugin_hfov != 0 )
        {
          // we have the hfov value from the UserComment EXIF tag,
          // which is better than nothing, so let's use it but emit
          // a warning if the image seems to be cropped.
          // I used to accept the vfov as well, but if the image is
          // cropped, this often results in anisotropic distortion.
          // So now I check: I calculate the vfov from the hfov,
          // projection and image extent. For uncropped images, the
          // result should agree with the value in the UserComment
          // tag. If there is a discrepancy, I assume the image was
          // cropped and overrule the UserComment Value with the one
          // calculated from the hfov, which produces a plausible
          // result (no anisotropic distortion) even if the hfov
          // is not entirely correct due to cropping.

          metadata_source = "UserComment EXIF" ;

          hfov = metadata.hugin_hfov ;
          vfov = metadata.hugin_vfov ;

          auto calc_vfov
            = calculate_vfov ( hfov , width , height , projection ) ;

          // accept a good half degree discrepancy

          double rad_0_6 = 0.6 * M_PI / 180.0 ;

          if ( fabs ( calc_vfov - vfov ) > rad_0_6 )
          {
            vfov = calc_vfov ;

            std::cout << "this seems to be a cropped image"
                      << std::endl ;
            std::cout << "ignoring UserComment vfov "
                      << metadata.hugin_vfov * 180.0 / M_PI
                      << std::endl ;
            std::cout << "instead using vfov calculated from hfov: "
                      << vfov * 180.0 / M_PI
                      << std::endl ;
          }
        }
        else if ( metadata.exif_hfov != 0.0 )
        {
          // none of the above has given us usable image geometry data,
          // so we look if it's maybe an image 'straight from the camera'
          // and gives us the geometry via focal length etc.

          metadata_source = "Camera EXIF" ;
          hfov = metadata.exif_hfov ;
          hofs = M_PI - metadata.exif_hfov / 2.0 ;
          vfov = metadata.exif_vfov ;
          vofs = M_PI - metadata.exif_vfov / 2.0 ;
        }
        else
        {
          std::cout << "no image geometry metadata for "
                    << filename << std::endl ;
        }

        if ( metadata_source != std::string() )
        {
          std::cout << "based on " << metadata_source << " metadata:" << std::endl
            << "projection " << projection_name [ projection ] << std::endl
            << "hfov       " << hfov * 180.0 / M_PI << " deg " << std::endl
            << "vfov       " << vfov * 180.0 / M_PI << " deg " << std::endl ;
          if ( metadata_source != "UserComment EXIF" )
            std::cout
            << "hofs       " << hofs * 180.0 / M_PI << " deg " << std::endl
            << "vofs       " << vofs * 180.0 / M_PI << " deg " << std::endl ;
        }
      }

      // handle images in portrait format

      if ( exif_orientation >= 5 )
      {
        // image is definitely portrait

        std::cout << "EXIF orientation is portrait" << std::endl ;
        std::cout << "swapping width and height" << std::endl ;
        std::cout << "swapping hfov and vfov" << std::endl ;
        std::cout << "swapping hofs and vofs" << std::endl ;

        std::swap ( width , height ) ;
        std::swap ( hfov , vfov ) ;
        std::swap ( hofs , vofs ) ;
      }

      // sanity check

      if (    ( width > height && hfov < vfov )
           || ( width < height && hfov > vfov ) )
      {
        // oops... that's fishy. if width >/< height,
        // hfov must be >/< vfov

        if ( width > height && hfov < vfov )
          std::cout << "oops: width > height && hfov < vfov"
          << std::endl ;
        if ( width < height && hfov > vfov )
          std::cout << "oops: width < height && hfov > vfov"
          << std::endl ;
        std::cout << "swapping hfov and vfov" << std::endl ;
        std::cout << "swapping hofs and vofs" << std::endl ;
        std::swap ( hfov , vfov ) ;
        std::swap ( hofs , vofs ) ;
      }
    }
    else // read geometry
    {
      // without read_geometry, we only look at the exif orientation.

      if ( exif_orientation >= 5 )
      {
        // definitely portrait

        std::cout << "EXIF orientation is portrait" << std::endl ;
        std::cout << "swapping width and height" << std::endl ;

        std::swap ( width , height ) ;
      }
    }

    // this heuristic works for panoramas made with iPhones
    // and with my samsung xcover4, but not my old S3 mini which
    // lacks some exif tags. exif_se_f holds the sensor's longer
    // extent in units of the focal length - in other words, in
    // model space units, where the focal length is 1.0 and
    // coincides with the unit sphere radius. In model space units,
    // exif_se_f is equal to twice the tangens of half the FOV.
    // The heuristic assumes that camera-generated panoramas are
    // done in portrait mode and takes it from there. Note that
    // the heuristic is only used if no panorama-specific metadata
    // were found and the geometry was inferred from exif tags
    // written by the camera. The threshold of 2:1 for the aspect
    // ratio is conservative - but I think the highest apect ratio
    // commonly found in cameras should be 16:9, which is just
    // below 2:1

    if (    metadata_source == "Camera EXIF"
         && width >= 2.0 * height )
    {
      std::cout << "applying heuristic for camera-generated panorama:"
                << std::endl ;

      // camera-generated panoramas are asumed to have been put
      // together from narrow stripes cut from the center of a
      // rectilinear image, so the projection must by cylindric

      projection = CYLINDRIC ;

      // get the scale factor from pixels to model space units

      auto scf = height / metadata.exif_se_f ;

      // recalculate hfov, vfov, hofs, vofs

      hfov = width / scf ;
      vfov = 2.0 * atan ( metadata.exif_se_f / 2.0 ) ;
      vofs = M_PI - vfov / 2.0 ;
      hofs = M_PI - hfov / 2.0 ;

      std::cout
        << "projection " << projection_name [ projection ] << std::endl
        << "hfov       " << hfov * 180.0 / M_PI << " deg " << std::endl
        << "vfov       " << vfov * 180.0 / M_PI << " deg " << std::endl
        << "hofs       " << hofs * 180.0 / M_PI << " deg " << std::endl
        << "vofs       " << vofs * 180.0 / M_PI << " deg " << std::endl ;
    }
  }

  bool digest ( const std::string & _filename ,
                projection_type _projection ,
                double _hfov ,
                double _hofs ,
                double _vfov ,
                double _vofs ,
                bool _is_linear )
  {
    {
      std::ifstream test ( _filename ) ;
      if ( ! test.good() )
      {
        abort_lux
          (   "unrecoverable error processing "
            + _filename
            + ":\ncannot find or open this file"
          ) ;
      }
    }
    std::cout << "reading metadata from " << _filename << std::endl ;

    bool read_geometry = true ;

    // if any image geometry values have been passed, the values
    // in the metadata - if any - are ignored, and the complete set
    // is taken over from the arguments to this function. This means
    // that by passing, e.g. projection, the calling code claims
    // responsibility for *all* geometry data and has to provide
    // enough (usually projection and hfov) to make calculation
    // of the full set possible

    if (    _projection != PRJ_NONE
         || _hfov != 0.0
         || _vfov != 0.0
         || _hofs != -1.0
         || _vofs != -1.0 )
    {
      std::cout << "image geometry metadata will be ignored"
                << std::endl ;

      projection = _projection ;

      if ( projection == MOSAIC && _hfov == 0.0 )
        hfov = mosaic_hfov ;
      else
        hfov = _hfov ;

      vfov = _vfov ;
      hofs = _hofs ;
      vofs = _vofs ;

      // Clear this flag to make read_metadata ignore the metadata

      read_geometry = false ;
    }

    try
    {
      // Now process the metadata. Even with read_geometry set to false,
      // some metadata will be extracted, namely the image's orientation.

      read_metadata ( _filename , read_geometry ) ;

      // some file types imply is_linear

      fileio::image_info imageInfo = open_image_file ( _filename ) ;

      if ( std::strcmp ( imageInfo.getFileType() , "EXR" ) == 0 )
      {
        is_linear = true ;
      }
      else if ( std::strcmp ( imageInfo.getFileType() , "openexr" ) == 0 )
      {
        is_linear = true ;
      }
      else if ( std::strcmp ( imageInfo.getFileType() , "JPEG" ) == 0 )
      {
        is_linear = false ;
      }
      else if ( std::strcmp ( imageInfo.getFileType() , "PNG" ) == 0 )
      {
        is_linear = false ;
      }
      else
      {
        is_linear = _is_linear ; // take over from argument
      }
    }
    catch ( ... )
    {
      abort_lux
        (   "unrecoverable error processing "
          + _filename
          + ":\nan exception occured when accessing this file"
          + "\nis this a valid image file?"
        ) ;
    }

    // if projection hasn't yet been set, we assume it's a 'flat' image
    // and assign an arbitrary small hfov

    if ( projection == PRJ_NONE )
    {
      projection = MOSAIC ;
      hfov = mosaic_hfov ;
      vfov = 0 ;
      hofs = vofs = -1 ;
    }

    // if we have no hfov value by now, fail

    if ( hfov == 0.0 )
    {
      abort_lux
        (   "unrecoverable error processing " + _filename
          + ":\ncannot determine horizontal field of view"
          + "\n(pass --hfov=... on the command line)"
        ) ;
    }

    if ( vfov == 0.0 )
    {
      // so far we have no vfov value, let's calculate it from the hfov

      std::cout << "no vfov data, calculating vfov from hfov "
                << hfov * 180.0 / M_PI << std::endl ;

      // no vfov given, calculate it automatically, assuming the
      // optical axis meets the image at half height.

      if ( exif_orientation < 5 || ini::args.input_is_pto == false )
      {
        vfov = calculate_vfov ( hfov , width , height , projection ) ;
      }
      else
      {
        // special case for PTO data in portrait orientation

        vfov = calculate_vfov ( hfov , height , width , projection ) ;
        std::cout << "partial image in portrait orientation, swap hfov and vfov"
                  << std::endl ;
        std::swap ( hfov , vfov ) ;
      }

      std::cout << "calculated hfov: "
                << hfov * 180.0 / M_PI
                << " vfov: "
                << vfov * 180.0 / M_PI << std::endl ;
    }

    // if hfov/vfov values are clearly out of range, fail

    if ( hfov <= 0.0 || vfov <= 0.0 )
    {
      // this shouldn't really happen:

      abort_lux
        (   "unrecoverable error processing " + _filename
          + ":\ncannot handle negative field of view" ) ;

      return false ;
    }

    double hfov_max = FLT_MAX , vfov_max = FLT_MAX ;

    // now the capping is less strict, allowing, e.g., fov of more
    // than 360 degrees for fisheye shots. Parts of the image outside
    // the central 360 degrees will simply not show - at least that's
    // the idea. Whether this modification works out as intended
    // remains to be seen.

    if ( projection == STEREOGRAPHIC )
    {
      hfov_max = vfov_max = 2.0 * M_PI ;
    }
    else if ( projection == CYLINDRIC )
    {
      vfov_max = M_PI ;
    }
    else if ( projection == RECTILINEAR )
    {
      hfov_max = M_PI ;
      vfov_max = M_PI ;
    }

    if ( hfov > hfov_max )
    {
      hfov = hfov_max ;

      std::cerr << "hfov capped at " << 180.0 / M_PI * hfov
                << " deg" << std::endl ;
    }
    
    if ( vfov > vfov_max )
    {
      vfov = vfov_max ;

      std::cerr << "vfov capped at " << 180.0 / M_PI * vfov
                << " deg" << std::endl ;
    }
    
    if ( hofs == -1.0 )
    {
      // if no horizontal offset angle is given, we assume the image
      // is horizontally centered and calculate hofs accordingly

      std::cout << "assuming that image is horizontally centered"
                << std::endl ;

      hofs = M_PI - hfov / 2.0 ;
    }

    if ( vofs == -1.0 )
    {
      // if no vertical offset angle is given, we assume the image
      // is vertically centered and calculate vofs accordingly

      std::cout << "assuming that image is vertically centered"
                << std::endl ;

      vofs = M_PI - vfov / 2.0 ;
    }

    // look for 'special' large extents to set full_height and full_width
    // we mustn't be too precise here, hfov calculation may have been
    // subject to rounding errors, so we accept if the value is a
    // very small way below the threshold
  
    full_width = ( hfov >= 1.99999999999 * M_PI ) ;

    if ( projection == FISHEYE || projection == STEREOGRAPHIC )
    {
      // fisheyes can have (sensible) vfov up to 360 degrees
      full_height = ( vfov >= 1.99999999999 * M_PI ) ;
    }
    else
    {
      if ( projection == MOSAIC )
        full_width = full_height = false ;
      else
        full_height = ( vfov >= .99999999999 * M_PI ) ;
    }

    if ( full_height || full_width )
      ui::grey_edge = false ;

    // set the source_type object's 'extent'.

    set_extent ( *this ) ;

    return true ;
  }

  // process_lc converts PTO-specific lens correction parameters
  // to the values used in pv

  void process_lc()
  {
    lens_correction_active
      = ( a != 0.0 || b != 0.0 || c != 0.0 || h != 0.0 || v != 0.0 ) ;

    if ( ! lens_correction_active )
    {
      s = 0.0 ;
      d = 1.0 ;
      return ;
    }

    // reference radius in PTO is half the extent of the smaller edge

    double dv = fabs ( extent.y1 - extent.y0 ) / 2.0 ;
    double dh = fabs ( extent.x1 - extent.x0 ) / 2.0 ;

    s = ( dh < dv ) ? dh : dv ;

    // set d so that the image is not scaled

    d = 1.0 - ( a + b + c ) ;

    // the PTO d and e parameters are in pixels, h and v in unit radii

    double factor = fabs ( extent.x1 - extent.x0 ) / width ;
    h *= factor ;
    v *= factor ;
  }

  void process_vc()
  {
    vignetting_correction_active
      = ( va != 1.0 || vb != 0.0 || vc != 0.0 || vd != 0.0 ) ;

    if ( ! vignetting_correction_active )
    {
      vs = vx = vy = 0 ;
      return ;
    }

    // vignetting reference radius in PTO is half the extent
    // of the diagonal

    double dv = fabs ( extent.y1 - extent.y0 ) / 2.0 ;
    double dh = fabs ( extent.x1 - extent.x0 ) / 2.0 ;

    vs = sqrt ( dv * dv + dh * dh ) ;

    // the PTO Vx and Vy parameters are in pixels,
    // vx and vy in unit radii

    double factor = fabs ( extent.x1 - extent.x0 ) / width ;
    vx *= factor ;
    vy *= factor ;
  }

  // bounds_3d calculates two metrics specific to each facet:
  // - the radial angle of it's bounding cone
  // - the 3D corner points of a frustum enclosing the facet
  // these calculations are quite involved, especially due to the fact
  // that facets can be subject to lens correction with a lens correction
  // polynomial and lens shift. Lens correction is a radial function, so
  // it really only makes sense with facets in rectilinear, stereographic
  // and fisheye projections, but there is no harm in allowing it for
  // spherical and cylindric facets as well, because the calculations
  // can be done meaningfully. I leave it up to the user - there may even
  // be cases where lens correction for spherical and cylindric facets
  // does make sense.
  // Both facet metrics will be used in the rendering code to determine
  // which facets can at all contribute to a given view. This is an
  // important optimization, because it can reduce the number of facets
  // which have to be tested for visibility at a given target image point.
  // For single-image views, this code is currently not used, even though
  // the metric could determine cases where the single image is totally
  // invisible with the current view.

  void bounds_3d()
  {
    // First we need to find out which points on the image plane correspond
    // to the four corner points of the facet without lens correction. To
    // find these points, we have to use the inverse lens correction polynomial.
    // What we'll process here is not the commonly used third degree polynomial,
    // but one degree up, taking the unmodified radius as argument and producing
    // the modified radius. The coefficients are the same, and DC is zero.

    double coefficients[] { a , b , c , d , 0.0 } ;

    pv_polynomial < double , 4 > lcp ( coefficients ) ;

    ul2 = point_2d_d_type ( extent.x0 , extent.y0 ) ;
    ur2 = point_2d_d_type ( extent.x1 , extent.y0 ) ;
    lr2 = point_2d_d_type ( extent.x1 , extent.y1 ) ;
    ll2 = point_2d_d_type ( extent.x0 , extent.y1 ) ;

    double factor = 1.0 ;

    if ( lens_correction_active )
    {
      // we need the distance ('radius') from the center (0,0) to one
      // of the corners. TODO: this assumes that the facet has symmetric
      // extent, so no cropping

      double rul = norm ( ul2 ) ;

      // the lens correction is working on radii scaled to multiples
      // of the reference radius (the shorter edge), So we convert to
      // a multiple of the reference radius, which gives us the value
      // we need to feed to the inverse lens control polynomial as the
      // desired result, to obtain the required input.

      auto feed = rul / s ;

      // rinv will be the input to the lens correction polynomial
      // which produces the desired output 'feed'

      double rinv = 1.0 ;

      // obtain rinv, asserting that the inverse polynomial calculation
      // did succeed - it's an iteration, which might not converge, but
      // this should only happen if the lens correction polynomial is
      // not strictly monotonously rising, which we don't test.

      assert ( lcp.inverse ( feed , rinv ) ) ;

      // we get the scaling factor we need to apply to the corner points
      // as the ratio between rinv and feed

      factor = rinv / feed ;
    }

    if ( factor > 1.0 )
    {
      // factor is greater than one. This means that rays hitting outside
      // the unmodified facet's boundaries will be 'bent' into the
      // boundaries. We need to extend the frustum to compensate that.
      // the case with factor <= 1.0 does not result in a frustum
      // modifaction, because such a distortion has edges 'protruding'
      // which would make them 'stick out' of the modified frustum,
      // invalidating the requirement that all of the facet be contained
      // in the frustum.

      ul2 *= factor ;
      ur2 *= factor ;
      lr2 *= factor ;
      ll2 *= factor ;
    }

    // next we apply shift. The two values h and v correspond to panotools
    // and e, but here we have values scaled to model space units

    point_2d_d_type shift { h , v } ;

    ul2 -= shift ;
    ur2 -= shift ;
    lr2 -= shift ;
    ll2 -= shift ;

    // now we calculate the distance of each corner to the center of the
    // image plane

    double rul = norm ( ul2 ) ;
    double rur = norm ( ur2 ) ;
    double rlr = norm ( lr2 ) ;
    double rll = norm ( ll2 ) ;

    // the largest of these values is helpful for producing the bounding
    // cone angle for most projections

    r_max = std::max ( std::max ( rul , rur ) ,
                       std::max ( rlr , rll ) ) ;

    point_3d_d_type front ( 1.0 , 0.0 , 0.0 ) ;

    // the calculation of the 3D coordinates of the four corner points
    // of the frustum depends on the facet's projection:

    switch ( projection )
    {
      case RECTILINEAR :
      {
        ul = point_3d_d_type ( 1.0 , ul2[0] , ul2[1] ) ;
        ur = point_3d_d_type ( 1.0 , ur2[0] , ur2[1] ) ;
        lr = point_3d_d_type ( 1.0 , lr2[0] , lr2[1] ) ;
        ll = point_3d_d_type ( 1.0 , ll2[0] , ll2[1] ) ;

        theta = atan ( r_max ) ;
        theta = std::max ( theta , hfov / 2.0 ) ;
        theta = std::max ( theta , vfov / 2.0 ) ;
        break ;
      }
      case STEREOGRAPHIC :
      {
        theta = atan ( r_max / 2.0 ) * 2.0 ;
        theta = std::max ( theta , hfov / 2.0 ) ;
        theta = std::max ( theta , vfov / 2.0 ) ;

        {
          const auto & u ( ul2[0] ) ;
          const auto & v ( ul2[1] ) ;

          auto radius = 2.0 * atan ( sqrt ( u * u + v * v ) / 2.0 ) ;
          auto angle = atan2 ( u , - v ) ;

          ul = polar_to_cartesian ( radius , angle ) ;
        }
        {
          const auto & u ( ur2[0] ) ;
          const auto & v ( ur2[1] ) ;

          auto radius = 2.0 * atan ( sqrt ( u * u + v * v ) / 2.0 ) ;
          auto angle = atan2 ( u , - v ) ;

          ur = polar_to_cartesian ( radius , angle ) ;
        }
        {
          const auto & u ( lr2[0] ) ;
          const auto & v ( lr2[1] ) ;

          auto radius = 2.0 * atan ( sqrt ( u * u + v * v ) / 2.0 ) ;
          auto angle = atan2 ( u , - v ) ;

          lr = polar_to_cartesian ( radius , angle ) ;
        }
        {
          const auto & u ( ll2[0] ) ;
          const auto & v ( ll2[1] ) ;

          auto radius = 2.0 * atan ( sqrt ( u * u + v * v ) / 2.0 ) ;
          auto angle = atan2 ( u , - v ) ;

          ll = polar_to_cartesian ( radius , angle ) ;
        }
        break ;
      }
      case FISHEYE :
      {
        theta = r_max ;
        theta = std::max ( theta , hfov / 2.0 ) ;
        theta = std::max ( theta , vfov / 2.0 ) ;

        {
          const auto & u ( ul2[0] ) ;
          const auto & v ( ul2[1] ) ;

          auto radius = sqrt ( u * u + v * v ) ;
          auto angle = atan2 ( u , - v ) ;

          ul = polar_to_cartesian ( radius , angle ) ;
        }
        {
          const auto & u ( ur2[0] ) ;
          const auto & v ( ur2[1] ) ;

          auto radius = sqrt ( u * u + v * v ) ;
          auto angle = atan2 ( u , - v ) ;

          ur = polar_to_cartesian ( radius , angle ) ;
        }
        {
          const auto & u ( lr2[0] ) ;
          const auto & v ( lr2[1] ) ;

          auto radius = sqrt ( u * u + v * v ) ;
          auto angle = atan2 ( u , - v ) ;

          lr = polar_to_cartesian ( radius , angle ) ;
        }
        {
          const auto & u ( ll2[0] ) ;
          const auto & v ( ll2[1] ) ;

          auto radius = sqrt ( u * u + v * v ) ;
          auto angle = atan2 ( u , - v ) ;

          ll = polar_to_cartesian ( radius , angle ) ;
        }
        break ;
      }
      case SPHERICAL :
      {
        ul = ae_to_cartesian ( ul2[0] , -ul2[1] ) ;
        ur = ae_to_cartesian ( ur2[0] , -ur2[1] ) ;
        lr = ae_to_cartesian ( lr2[0] , -lr2[1] ) ;
        ll = ae_to_cartesian ( ll2[0] , -ll2[1] ) ;

        theta = angle3d ( front , ul ) ;
        theta = std::max ( theta , angle3d ( front , ur ) ) ;
        theta = std::max ( theta , angle3d ( front , lr ) ) ;
        theta = std::max ( theta , angle3d ( front , ll ) ) ;
        theta = std::max ( theta , hfov / 2.0 ) ;
        theta = std::max ( theta , vfov / 2.0 ) ;

        break ;
      }
      case CYLINDRIC :
      {
        ul = ae_to_cartesian ( ul2[0] , atan ( -ul2[1] ) ) ;
        ur = ae_to_cartesian ( ur2[0] , atan ( -ur2[1] ) ) ;
        lr = ae_to_cartesian ( lr2[0] , atan ( -lr2[1] ) ) ;
        ll = ae_to_cartesian ( ll2[0] , atan ( -ll2[1] ) ) ;

        theta = angle3d ( front , ul ) ;
        theta = std::max ( theta , angle3d ( front , ur ) ) ;
        theta = std::max ( theta , angle3d ( front , lr ) ) ;
        theta = std::max ( theta , angle3d ( front , ll ) ) ;
        theta = std::max ( theta , hfov / 2.0 ) ;
        theta = std::max ( theta , vfov / 2.0 ) ;

        break ;
      }
      case CUBEMAP:
      case FACET_MAP:
      case MOSAIC:
      {
        ul = ur = lr = ll = front ;
        theta = 0.0 ;
        break ;
      }
      default:
      {
        abort_lux ( "unhandled projection in bounds_3d" ) ;
      }
    }

    // as a finishing touch, we normalize the 3D corners of the frustum
    // to make comparisons with other frusta easier to follow - the
    // frustum intersection code itself is immune to scale.

    ul /= norm ( ul ) ;
    ur /= norm ( ur ) ;
    lr /= norm ( lr ) ;
    ll /= norm ( ll ) ;
  }

  void echo()
  {
    // to echo angles in degrees instead of radians:

    double df = 180.0 / M_PI ;

    std::cout << std::fixed << std::showpoint << std::setprecision(5) ;

    std::cout << "file:        " << filename << std::endl ;
    std::cout << "width:       " << width << std::endl ;
    std::cout << "height:      " << height << std::endl ;
    std::cout << "exif orient: " << exif_orientation << std::endl ;
    std::cout << "lens number: " << lens_number << std::endl ;
    std::cout << "projection:  " << projection_name[projection] << std::endl ;
    std::cout << "hfov:        " << df * hfov << " deg" << std::endl ;
    std::cout << "hofs:        " << df * hofs << " deg" << std::endl ;
    std::cout << "vfov:        " << df * vfov << " deg" << std::endl ;
    std::cout << "vofs:        " << df * vofs << " deg" << std::endl ;

    std::cout << "brightness:  " << brightness << std::endl ;
    std::cout << "handicap:    " << handicap << std::endl ;
    std::cout << "squash:      " << squash << std::endl ;
    std::cout << "is_linear:   " << is_linear << std::endl ;
    std::cout << "full_width:  " << full_width << std::endl ;
    std::cout << "full_height: " << full_height << std::endl ;
    std::cout << "exent:       " << extent.x0 << " " << extent.x1
                          << " " << extent.y0 << " " << extent.y1 << std::endl ;
    std::cout << "ul corner:   " << ul << std::endl ;
    std::cout << "ur corner:   " << ur << std::endl ;
    std::cout << "lr corner:   " << lr << std::endl ;
    std::cout << "ll corner:   " << ll << std::endl ;
    std::cout << "cone theta:  " << df * theta << " deg" << std::endl ;
    std::cout << "x_step:      " << x_step << std::endl ;
    if ( lens_correction_active )
    {
      std::cout << "lcp abcd     "
                << a << " " << b << " " << c << " " << d << std::endl ;
      std::cout << "lcp h, v     "
                << h << " " << v << std::endl ;
    }
    else
    {
      std::cout << "no lens correction" << std::endl ;
    }
    if ( vignetting_correction_active )
    {
      std::cout << "vcp abcd     "
                << va << " " << vb << " "
                << vc << " " << vd << std::endl ;
      std::cout << "vcp x, y     "
                << vx << " " << vy << std::endl ;
    }
    else
    {
      std::cout << "no vignetting correction" << std::endl ;
    }
    if ( cropping_active )
    {
      std::cout << ( cropping_elliptic ? "circular" : "rectangular" )
                << " cropping active: "
                << " x0 = " << crop_x0
                << " x1 = " << crop_x1
                << " y0 = " << crop_y0
                << " y1 = " << crop_y1 << std::endl ;
    }
    std::cout << "stack parent " << stack_parent << std::endl ;
  }
} ;

// polygon filling code gleaned from:
// http://alienryderflex.com/polygon_fill/
// where a C version is available in the public domain.
// thanks :D
// I modified the code to take the winding order into account and produce
// polygon filling behaviour like panotools: if the polygon self-intersects,
// the intersections are also filled.

void fill_polygon ( const std::vector<float> & px ,
                    const std::vector<float> & py ,
                    int IMAGE_LEFT , int IMAGE_TOP ,
                    int IMAGE_RIGHT , int IMAGE_BOT ,
                    std::function < void ( int , int ) > fillPixel )
{
  int N = px.size() ;
  assert ( px.size() == py.size() ) ;

  int  nodes, nodeX[N], dir[N] ,pixelX, pixelY, i, j, swap ;

//  Loop through the rows of the image.
  for (int pixelY=IMAGE_TOP; pixelY<IMAGE_BOT; pixelY++)
  {

    //  Build a list of nodes.
    nodes=0; j=N-1;
    for (i=0; i<N; i++)
    {
      // in addition to testing for the crossing, we also take note
      // of the direction of the crossing: are we passing into the
      // edge 'from the left' or 'from the right'?
      int cross = 0 ;
      if ( py[i]<(float) pixelY && py[j]>=(float) pixelY )
        cross = 1 ;
      else if ( py[j]<(float) pixelY && py[i]>=(float) pixelY)
        cross = -1 ;
      if ( cross )
      {
        nodeX[nodes]=(int) (px[i]+(pixelY-py[i])/(py[j]-py[i])
        *(px[j]-px[i]));
        dir[nodes++] = cross ;
      }
      j=i;
    }

    // Sort the nodes, via a simple “Bubble” sort.
    // extended to also sort the crossing information

    i=0 ;

    while (i<nodes-1)
    {
      if (nodeX[i]>nodeX[i+1])
      {
        swap=nodeX[i]; nodeX[i]=nodeX[i+1]; nodeX[i+1]=swap;
        swap=dir[i]; dir[i]=dir[i+1]; dir[i+1]=swap;
        if (i) i--;
      }
      else
      {
        i++;
      }
    }

    // Fill the pixels between node pairs if the winding order isn't zero.
    // We obtain the winding order by cumulating the 'cross' values.
    int w_ord = 0 ;
    for (i=0; i<nodes; i++)
    {
      w_ord += dir[i] ;
      // if the winding order is zero, skip to the next segment
      if ( ! w_ord )
        continue ;

      // otherwise, the algorithm progresses as in the variant using
      // 'alternate filling'

      if   ( nodeX[i] >= IMAGE_RIGHT )
        break ;
      if   ( nodeX[i+1] > IMAGE_LEFT )
      {
        if ( nodeX[i] < IMAGE_LEFT )
          nodeX[i] = IMAGE_LEFT ;

        if ( nodeX[i+1] > IMAGE_RIGHT )
          nodeX[i+1] = IMAGE_RIGHT ;

        for ( pixelX = nodeX[i] ; pixelX < nodeX[i+1] ; pixelX++)
          fillPixel(pixelX,pixelY);
        
      }
    }
  }
}

// the 'lens number' is not available explicitly, we have to look
// at the lens parameters for each source image and figure out which
// of the source images have identical lens parameters. I check for
// hfov, projection, and PTO a, b, c, d, and e and omit checking for
// vignetting parameters. This is debatable.

struct lens_parameters
{
  double hfov, h, v, a, b, c ;
  int projection ;
} ;

bool operator< ( const lens_parameters & lhs ,
                 const lens_parameters & rhs )
{
  if ( lhs.hfov < rhs.hfov )
    return true ;
  else if ( lhs.hfov > rhs.hfov )
    return false ;

  if ( int(lhs.projection) < int(rhs.projection) )
    return true ;
  if ( int(lhs.projection) > int(rhs.projection) )
    return false ;

  if ( lhs.h < rhs.h )
    return true ;
  else if ( lhs.h > rhs.h )
    return false ;

  if ( lhs.v < rhs.v )
    return true ;
  else if ( lhs.v > rhs.v )
    return false ;

  if ( lhs.a < rhs.a )
    return true ;
  else if ( lhs.a > rhs.a )
    return false ;

  if ( lhs.b < rhs.b )
    return true ;
  else if ( lhs.b > rhs.b )
    return false ;

  if ( lhs.c < rhs.c )
    return true ;
  
  return false ;
}

bool build_interpolator ( const char * const filename ,
                          source_type & source )
{
  // shorthand for arguments

  auto & args ( ini::state ) ;

  // initially, orientation is unknown

  source.exif_orientation = 0 ;

  // shorthand

  projection_type & projection ( ui::projection ) ;

  // set alpha processing mode. The value in args.alpha is a string,
  // here we move to enum alpha_mode (see common.h). the option-processing
  // code only allows the strings for which we test here, but we test
  // anyway

  alpha_mode initial_alpha_mode = ALPHA_MODE_NOT_SET ;

  if ( args.alpha == "no" )
    initial_alpha_mode = NO_ALPHA ;
  else if ( args.alpha == "auto" )
    initial_alpha_mode = NO_ALPHA_IF_OPAQUE ;
  else if ( args.alpha == "as-file" )
    initial_alpha_mode = ALPHA_AS_SRC ;
  else if ( args.alpha == "yes" )
    initial_alpha_mode = WITH_ALPHA ;

  assert ( initial_alpha_mode != ALPHA_MODE_NOT_SET ) ;

  if ( projection == CUBEMAP )
  {
    // assign the cubemap images to their variables in namespace ui
    // The calling code has already checked that they are all present.

    ui::cubemap [ LEFT ] = args.cube_left ;
    ui::cubemap [ RIGHT ] = args.cube_right ;
    ui::cubemap [ TOP ] = args.cube_top ;
    ui::cubemap [ BOTTOM ] = args.cube_bottom ;
    ui::cubemap [ FRONT ] = args.cube_front ;
    ui::cubemap [ BACK ] = args.cube_back ;

    // cube faces must have at least 90 degrees fov

    if ( args.cubeface_fov == 0.0 )
    {
      std::cout << "cubeface_fov argument missing, assuming 90 degrees"
                << std::endl ;

      ui::cubeface_fov = M_PI_2 ;
    }
    else
    {
      if ( args.cubeface_fov < M_PI_2 )
      {
        std::string message ( "unrecoverable error processing " ) ;
        message += filename ;
        message += "\ncubeface_fov must be at least 90 degrees" ;
        abort_lux ( message ) ;
      }

      ui::cubeface_fov = args.cubeface_fov ;
    }

    // cubemaps always cover 360X180 degrees:

    source.hofs = 0.0 ;
    source.hfov = 2.0 * M_PI ;
    source.vofs = M_PI / 2.0 ;
    source.vfov = M_PI ;
    source.projection = CUBEMAP ;

    // we need to extract some 'exemplary' data

    fileio::image_info imageInfo =
      open_image_file ( args.cube_front ) ;

    // we set source width and height to a single face's dimensions
    // so that showing the view 1:1 comes out right

    source.width = imageInfo.width() ;
    source.height = imageInfo.height() ;

    // now we build source_type structures for the cube faces.

    ui::source_v.clear() ;
    ui::source_v.resize ( 6 ) ;

    for ( int i = 0 ; i < 6 ; i++ )
    {
      auto & source_builder
             ( reinterpret_cast < scaffolding_type < source_type > & >
                 ( ui::source_v[i] ) ) ;

      source_builder.init() ;

      // TODO: allow cubemap faces with different brightness

      source_builder.brightness = 1.0 ; // like: args.facet_brightness[i] ;

      bool success
        = source_builder.digest ( ui::cubemap[i] ,
                                  RECTILINEAR ,
                                  ui::cubeface_fov ,
                                  -1.0 ,
                                  ui::cubeface_fov ,
                                  -1.0 ,
                                  args.is_linear ) ;

      if ( ! success )
      {
        return false ;
      }

      // for cubemaps, the 'overall' squash factor is passed on to
      // all the cube faces, there are no individual squash values.

      source_builder.squash = args.squash ;

      // not really necessary for cubemaps, but does no harm

      source_builder.bounds_3d() ;

      std::cout << "cube face " << i << " set up to:" << std::endl ;
      source_builder.echo() ;
    }

    // make_cube_itp receives the current interpolator (if any) as it's
    // last argument. The rendering thread is free to decide that this (old)
    // interpolator can be reused - the test is quite involved. If the old
    // interpolator is not reused, the rendering thread deletes the old
    // interpolator object and returns a new one.

    ui::p_itp = dispatcher->make_cube_itp (
      ui::source_v ,
      ui::process_linear ,
      initial_alpha_mode ,
      ui::fast_interpolator_degree ,
      ui::quality_interpolator_degree ,
      std::max ( 1.25 , ui::pyramid_scaling_step ) ,
      ui::pyramid_smoothing_level ,
      args.cbf_degree ,
      1 , // tentative
      args.build_pyramids ,
      args.build_raw_pyramids ,
      ui::p_itp ) ;

  }
  else if ( projection == FACET_MAP )
  {
    // facet maps always cover 360X180 degrees:
    // TODO: no, they don't. But figuring out and encoding the cover
    // is non-trivial

    source.hofs = 0.0 ;
    source.hfov = 2.0 * M_PI ;
    source.vofs = M_PI / 2.0 ;
    source.vfov = M_PI ;
    source.projection = FACET_MAP ;

    // we need to extract some 'exemplary' data

    fileio::image_info imageInfo =
      open_image_file ( args.facet[0] ) ;

    // we set source width and height to a single face's dimensions
    // so that showing the view 1:1 comes out right

    source.width = imageInfo.width() ;
    source.height = imageInfo.height() ;

    // now we build source_type structures for the facets.

    int nfacets = args.facet.size() ;
    assert ( ui::facet_projection.size() == nfacets ) ;
    assert ( ui::facet_orientation.size() == nfacets ) ;
    assert ( args.facet_hfov.size() == nfacets ) ;
    assert ( args.facet_brightness.size() >= nfacets ) ;

    ui::source_v.clear() ;
    ui::source_v.resize ( nfacets ) ;
    bool any_cropped = false ;

    // if input is PTO, coordinates obtained from the PTO will pertain
    // to the image in 'native' landscape format. If such images have
    // EXIF orientation other than none or 1, the coordinates will be
    // wrong for lux, which uses coordinates pertaining to the image
    // after applying the EXIF orientation.
    // tf_from_native will provide a transformation function for each
    // image which will produce the coordinates suitable for lux from
    // the coordinates found in the PTO file.

    typedef std::function < void ( float& , float& ) > tf_type ;
    std::vector < tf_type > tf_from_native ( nfacets ) ;
    for ( auto & tf : tf_from_native )
      tf = [] ( float & x , float & y ) { } ;

    std::map < lens_parameters , int > lens_map ;
    int lens_number = 0 ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      auto & source_builder
             ( reinterpret_cast < scaffolding_type < source_type > & >
                 ( ui::source_v[i] ) ) ;

      source_builder.init() ;

      if ( ui::light_balance == "hedged" )
        source_builder.brightness = 1.0 ;
      else
        source_builder.brightness = args.facet_brightness[i] ;

      // as a quick shot solution, I set the facet handicap to a multiple
      // of the facet's hfov. The effect is to move facets with larger hfov
      // 'further out', which avoids seams with facets with smaller hfov.
      // With this method, tele shots of 'interesting' details are placed
      // in front of the 'boring' lower-res background - the longer the
      // lens, the higher the 'priority', which might be an alternative
      // way to put it.
      // TODO: this should be the "auto" way of prioritizing, and the user
      // should be free to assign values explicitly.

      source_builder.handicap = args.facet_handicap[i] ;

      bool success
        = source_builder.digest ( args.facet[i] ,
                                  ui::facet_projection[i] ,
                                  args.facet_hfov[i] ,
                                  -1.0 ,
                                  0.0 ,
                                  -1.0 ,
                                  args.facet_is_linear[i] ) ;

      args.facet_hfov[i] = source_builder.hfov ;

      if ( ! success )
      {
        return false ;
      }

      if (    args.facet_lch[i] == 0.0
           && args.facet_lcv[i] == 0.0
           && args.facet_lca[i] == 0.0
           && args.facet_lcb[i] == 0.0
           && args.facet_lcc[i] == 0.0 )
      {
        source_builder.lens_correction_active = false ;
      }
      else
      {
        source_builder.lens_correction_active = true ;
        source_builder.h = args.facet_lch[i] ;
        source_builder.v = args.facet_lcv[i] ;
        source_builder.a = args.facet_lca[i] ;
        source_builder.b = args.facet_lcb[i] ;
        source_builder.c = args.facet_lcc[i] ;
        source_builder.process_lc() ; // this will set s and d

        // For PTO input only:
        // if the source image had an EXIF orientation different
        // from the 'generic' one, we need to adjust the h and v
        // parameters (PTO lens parameters d and e), because in pv
        // an EXIF-rotated image is rotated on import, while in PTO
        // the d and e parameters refer to the unrotated image.
  
        bool flipped = false ;
        if ( args.input_is_pto )
        {
          switch ( source_builder.exif_orientation )
          {
            case 2:
              flipped = true ;
            case 0:
            case 1:
              break ;
            case 4:
              flipped = true ;
            case 3: // upside down
              source_builder.h = -source_builder.h ;
              source_builder.v = -source_builder.v ;
              break ;
            case 7:
              flipped = true ;
            case 6: // sensor's left edge is image top edge
              std::swap ( source_builder.h , source_builder.v ) ;
              source_builder.h = -source_builder.h ;
              break ;
            case 5:
              flipped = true ;
            case 8: // sensor's right edge is image top edge
              std::swap ( source_builder.h , source_builder.v ) ;
              source_builder.v = -source_builder.v ;
              break ;
            default: // unknown
              break ;
          }
          if ( flipped )
          {
            std::string error ( "EXIF orientation of facet " ) ;
            error += args.facet[i] ;
            error += " is a flipped orientation, which lux can't handle" ;
            abort_lux ( error ) ;
          }
        }
      }

      if ( args.input_is_pto )
      {
        // in PTO, masking information is given in sensor-relative
        // coordinates, but lux wants the masks in coordinates referring
        // to the image data it holds, which have already been submitted
        // to the appropriate rotation, so as to have an 'upright' image
        // in memory. To handle such sensor-relative coordinates correctly,
        // lux needs to convert them:

        const auto & exif_orientation ( ui::source_v[i].exif_orientation ) ;
        
        // get sensor width and height (width is the long edge, height the
        // short edge)
        
        int width = std::max ( source_builder.width ,
                               source_builder.height ) ;

        int height = std::min ( source_builder.width ,
                                source_builder.height ) ;
        bool flipped = false ;

        switch ( exif_orientation )
        {
          case 2:
          {
            flipped = true ;
            // fallthrough is deliberate
          }
          case 0: // no EXIF orientation found
          case 1: // file and memory order coincide
          {
            // both types of coordinate coincide, no action necessary
            break ;
          }
          case 4:
          {
            flipped = true ;
            // fallthrough is deliberate
          }
          case 3: // 180 degree rotation
          {
            // origin of incoming coordinates is (x0,y0) in result coordinates
            // here we needn't buffer the results and directly assign the result
            int x0 = width - 1 ;
            int y0 = height - 1 ;
            tf_from_native[i] = [=] ( float & x , float & y )
            {
              x = x0 - x ;
              y = y0 - y ;
            } ;
            break ;
          }
          case 7:
          {
            flipped = true ;
            // fallthrough is deliberate
          }
          case 6: // 90 degrees clockwise
          {
            int x0 = height - 1 ;
            tf_from_native[i] = [=] ( float & x , float & y )
            {
              // result x depends on incoming y, result y depends on incoming x
              // so we need to buffer the results and assign afterwards
              float xx = x0 - y ;
              float yy = x ;
              x = xx ;
              y = yy ;
            } ;
            break ;
          }
          case 5:
          {
            flipped = true ;
            // fallthrough is deliberate
          }
          case 8: // 90 degrees counterclockwise
          {
            int y0 = width - 1 ;
            tf_from_native[i] = [=] ( float & x , float & y )
            {
              // result x depends on incoming y, result y depends on incoming x
              // so we need to buffer the results and assign afterwards
              float xx = y ;
              float yy = y0 - x ;
              x = xx ;
              y = yy ;
            } ;
            break ;
          }
          default:
          {
            std::cerr << "unhandled EXIF orientation value "
                      << exif_orientation << std::endl ;
            break ;
          }
        }
        if ( flipped )
        {
          std::string error ( "EXIF orientation of facet " ) ;
          error += ui::source_v[i].filename ;
          error += " is a flipped orientation, which lux can't handle" ;
          abort_lux ( error ) ;
        }
      }


      // tentative - tests/simpleStitch/simple.pto comes with
      // Va == 0.0, which produces 'strange' results, hence:

      if ( args.facet_vca[i] == 0.0 )
      {
        // must be PTO dialect, we use Va = 1.0
        args.facet_vca[i] = 1.0 ;
      }

      if (    args.facet_vca[i] == 1.0
           && args.facet_vcb[i] == 0.0
           && args.facet_vcc[i] == 0.0
           && args.facet_vcd[i] == 0.0 )
      {
        source_builder.vignetting_correction_active = false ;
      }
      else
      {
        source_builder.vignetting_correction_active = true ;
        source_builder.va = args.facet_vca[i] ;
        source_builder.vb = args.facet_vcb[i] ;
        source_builder.vc = args.facet_vcc[i] ;
        source_builder.vd = args.facet_vcd[i] ;
        source_builder.vx = args.facet_vcx[i] ;
        source_builder.vy = args.facet_vcy[i] ;
        source_builder.process_vc() ; // this will set vs
      }

      // we need to calculate the 3D bounds of the facet

      source_builder.bounds_3d() ;

      // transfer cropping information

      source_builder.cropping_active = args.facet_crop_active[i] ;
      source_builder.cropping_elliptic = args.facet_crop_elliptic[i] ;
      source_builder.crop_fade = args.facet_crop_fade[i] ;
      source_builder.crop_x0 = args.facet_crop_x0[i] ;
      source_builder.crop_x1 = args.facet_crop_x1[i] ;
      source_builder.crop_y0 = args.facet_crop_y0[i] ;
      source_builder.crop_y1 = args.facet_crop_y1[i] ;

      if ( args.input_is_pto )
      {
        // input is PTO, so the coordinates pertain to the sensor in
        // 'native' landscape and may need to be transformed if the source
        // image has different EXIF orientation.

        tf_from_native[i] ( source_builder.crop_x0 , source_builder.crop_y0 ) ;
        tf_from_native[i] ( source_builder.crop_x1 , source_builder.crop_y1 ) ;
      }

      any_cropped |= source_builder.cropping_active ;

      // for facet maps, facets may have individual squash factors.

      source_builder.squash = args.facet_squash[i] ;
      source_builder.stack_parent = args.facet_stack_parent[i] ;

      // TODO: set source_builder.handicap

      // the PTO file doas not contain explicit lens numbers, so we
      // have to figure out which images share the same lens

      lens_parameters lp { source_builder.hfov ,
                           source_builder.h ,
                           source_builder.v ,
                           source_builder.a ,
                           source_builder.b ,
                           source_builder.c ,
                           source_builder.projection } ;

      auto known = lens_map.find ( lp ) ;

      if ( known != lens_map.end() )
      {
        source_builder.lens_number = known->second ;
      }
      else
      {
        source_builder.lens_number = lens_number ;
        lens_map [ lp ] = lens_number ;
        ++ lens_number ;
      }

      std::cout << "facet " << i << " set up to:" << std::endl ;

      source_builder.echo() ;
    }

    if ( args.input_is_pto && args.use_pto_masks )
    {
      for ( const auto & mask : args.maskv )
      {
// from http://hugin.sourceforge.net/docs/nona/nona.txt:

// 'k'-line options
// ----------------
// Optional image masks are described by a 'k' line
//
//  i2           Set image number this mask applies to
//
//  t0           Type for mask:
//                   0 - negative (exclude region)
//                   1 - positive (include region)
//                   2 - negative, stack aware (exclude region from stack)
//                   3 - positive, stack aware (include region from stack)
//                   4 - negative, lens (exclude region from all images of same lens)
//
//  p"1262 2159 1402 2065 1468 2003"  List of node coordinates
//               Coordinates are in pairs, at least three pairs are required

        // if an exclude  mask occurs, set any_cropped, which will
        // trigger --alpha=yes. include masks are processed differently

        if ( mask.variant != 1 && mask.variant != 3 )
          any_cropped = true ;

        auto & source_builder
             ( reinterpret_cast < scaffolding_type < source_type > & >
                 ( ui::source_v[mask.image] ) ) ;

        source_type::polygon_type p ;
        p.variant = mask.variant ;
        p.vx = mask.vx ;
        p.vy = mask.vy ;

        // now we use the coordinate transformation function which is
        // specific to the EXIF orientation, and we apply it to pairs of
        // values picked from identical positions in p.vx and p.vy

        for ( int i = 0 ; i < p.vx.size() ; i++ )
        {
          tf_from_native[mask.image] ( p.vx[i] , p.vy[i] ) ;
        }

        source_builder.has_masks = true ;
        source_builder.masks.push_back ( p ) ;
      }
    }

    // if we encounter any cropped facets, we activate alpha processing
    // for *all* facets unless the user has specified --alpha=no to
    // ignore all alpha channels.

    if ( any_cropped && ( initial_alpha_mode != NO_ALPHA ) )
      initial_alpha_mode = WITH_ALPHA ;

    // type-2 and 3 masks are taken over to all facets of the same stack.
    // first we gather all facets sharing a specific stack parent:

    std::vector < std::vector < int > > family ( nfacets ) ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      auto & src ( ui::source_v[i] ) ;

      family [ src.stack_parent ] . push_back ( i ) ;
    }

    // Now we copy type-2 and type-3 masks to all other images in
    // the same stack. If the stack images are 'linked' - meaning
    // they are all equally oriented - we can copy the masks as they
    // are. If the stack is 'unlinked', we need to transform the masks
    // so that they will mask out the same content in the overlapping
    // images, rather than mask out the same part of the image.
    // To put it differently: stack masks refer to scene content,
    // not to image regions.

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      auto & src ( ui::source_v[i] ) ; // shorthand

      for ( auto & mask : src.masks )
      {
        if ( mask.variant == 2 || mask.variant == 3 )
        {
          // the mask variants 2 and 3 refer to stacks, subtracting
          // two gets the corresponding single-image variants

          mask.variant -= 2 ;

          for ( int & child : family [ src.stack_parent ] )
          {
            if ( child != i )
            {
              ui::source_v[child].has_masks = true ;

              // this is the point where the masks may need to be
              // geometrically transformed from the orientation in
              // image i to the other image 'child'.

              if (    ui::facet_orientation[child]
                   == ui::facet_orientation[i] )
              {
                // if both images have the same orientation (e.g. because
                // the image positions in the stack are 'linked'), we
                // can simply copy the mask unmodified

                ui::source_v[child].masks.push_back ( mask ) ;
              }
              else
              {
                // the image orientation differs, the stack is unlinked.
                // we ask the rendering code to buuld a suitable functor
                // to convert the coordinates from one facet's coordinate
                // system to the other's.

                // the source_type objects don't have rotation information,
                // we need to calculate the rotation quaternion from one
                // facet to the other from the values stored in ui::

                quaternion_type rotation
                  =   ui::facet_orientation[child]
                    * conj ( ui::facet_orientation[i] ) ;

                // obtain the functor

                auto crdtf = dispatcher->get_inter_facet_tf
                  ( ui::source_v[child] ,
                    ui::source_v[i] ,
                    rotation ) ;

                // set up a polygon_type object to hold the transformed
                // mask, copying the stack parent's mask type

                source_type::polygon_type _mask ;
                _mask.variant = mask.variant ;
                int n = mask.vx.size() ;

                // now convert each polygon corner point with the
                // conversion functor

                for ( int i = 0 ; i < n ; i++ )
                {
                  point_2d_d_type trg { mask.vx[i] , mask.vy[i] } ;
                  point_2d_d_type src ;
                  crdtf ( trg , src ) ;
                  _mask.vx.push_back ( src[0] ) ;
                  _mask.vy.push_back ( src[1] ) ;
                }

                // attach the result to the child's mask vector

                ui::source_v[child].masks.push_back ( _mask ) ;
              }
            }
          }
        }
      }
    }
    
    // now we copy per-lens masks to all source images with
    // the same lens. We use the same infrastructure, only the
    // 'family' criterion differs.

    for ( auto & m : family )
      m.clear() ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      auto & src ( ui::source_v[i] ) ;

      family [ src.lens_number ] . push_back ( i ) ;
    }

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      auto & src ( ui::source_v[i] ) ;
      for ( auto & mask : src.masks )
      {
        if ( mask.variant == 4 )
        {
          mask.variant = 0 ;
          for ( int & child : family [ src.lens_number ] )
          {
            if ( child != i )
            {
              // here, copying the masks is entirely correct,
              // because the masks are applied per-lens, so they
              // are meant to mask out image components induced
              // by the lens, rather than unwanted scene content

              ui::source_v[child].masks.push_back ( mask ) ;
              ui::source_v[child].has_masks = true ;
            }
          }
        }
      }
    }
    
    // now we look for facets with include masks and set up priority
    // maps for them. This is quite different from exclude masks which
    // result in an alpha channel manipulation. The priority map which
    // is attached to images with 'include' masks will make corresponding
    // pixels receive higher priority in ranked blending.

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      auto & src ( ui::source_v[i] ) ;
      if ( src.has_masks )
      {
        for ( auto & m : src.masks )
        {
          if ( m.variant == 1 )
          {
            // For full_width images we need a slightly larger array
            // for the priority map because we want to allow coordinates
            // up to src.width.

            int w = src.width ;
            if ( src.full_width )
              ++w ;

            int h = src.height ;

            // if the image doesn't yet have a priority map attached,
            // we set up a new one

            if ( src.p_priority_map == nullptr )
            {
              src.p_priority_map =
                std::make_shared < vigra::MultiArray < 2 , float > >
                  ( vigra::Shape2 ( w , h ) ) ;
            }

            // now we use the mask to set very low priority values
            // fo all points inside the mask (lower priority values
            // 'win')

            fill_polygon ( m.vx , m.vy , 0 , 0 , w , h ,
                            [&] ( int x , int y )
                              { (*(src.p_priority_map)) [ { x , y } ]
                                  = -1000.0f ; }
                          ) ;
          }
        }
      }
    }

    // make_facet_itp receives the current interpolator (if any) as it's
    // last argument. The rendering thread is free to decide that this (old)
    // interpolator can be reused - the test is quite involved. If the old
    // interpolator is not reused, the rendering thread deletes the old
    // interpolator object and returns a new one.
    // The first three arguments hold the information about the facets.

    ui::p_itp = dispatcher->make_facet_itp (
      ui::source_v ,
      ui::facet_orientation ,
      args.fully_covered ,
      ui::process_linear ,
      initial_alpha_mode ,
      ui::grey_edge ,
      ui::fast_interpolator_degree ,
      ui::quality_interpolator_degree ,
      std::max ( 1.25 , ui::pyramid_scaling_step ) ,
      ui::pyramid_smoothing_level ,
      args.cbf_degree ,
      1 , // tentative
      args.build_pyramids ,
      args.build_raw_pyramids ,
      ui::p_itp ) ;

  }
  else
  {
    // not a cubemap or facet map - ordinary single-image job.

    if ( ui::show_again && ( source.hfov == 0.0 ) )
    {
      // if this is a show-again cycle, 'remember' hfov from previous cycle

      source.hfov = ui::save_hfov ;

      std::cout << "will try reusing source hfov from previous cycle: "
                << ( source.hfov * 180.0 / M_PI ) << " deg" << std::endl ;

      if ( ui::save_projection != ui::projection )
      {
        // If the projection has changed, the 'remembered' value may not
        // be usable with the new projection, in which case we intervene:

        bool override = false ;

        if ( ui::save_projection == MOSAIC )
        {
          // the previous cycle was using mosaic projection, in which
          // case we settle on some 'sensible' value. TODO: debatable.

          source.hfov = M_PI_2 ;
          override = true ;
        }

        if ( projection == RECTILINEAR )
        {
          // this check is mainly for the case where previously we had wide
          // fov projection like spherical.

          if ( source.hfov > M_PI )
          {
            std::cout << "rectilinear projection; lowering hfov to 90 deg"
                      << std::endl ;

            source.hfov = M_PI_2 ;

            // we reset the orientation in saved_state, to avoid landing
            // in an 'impossible situation' when the saved state is reloaded

            ui::saved_state.orientation = get_rotation_q ( 0.0 , 0.0 , 0.0 ) ;

            override = true ;
          }
        }
        else if ( projection == STEREOGRAPHIC )
        {
          if ( source.hfov > 6.2 )
          {
            std::cout << "stereographic projection; lowering hfov to "
                      << ( 6.2 * 180.0 / M_PI ) << " deg"
                      << std::endl ;

            source.hfov = 6.2 ;
            override = true ;
          }
        }
        else if ( projection == MOSAIC )
        {
          std::cout << "mosaic projection; lowering nominal hfov to "
                    << ( mosaic_hfov * 180.0 / M_PI ) << " deg"
                    << std::endl ;

          source.hfov = mosaic_hfov ;
          override = true ;
        }

        if ( override )
        {
          // if an override happened above, the other geometry values
          // will be calculated automatically. If not, we assume that
          // they are valid and they will be taken over by 'digest'.

          source.vfov = 0.0 ;
          source.hofs = -1.0 ;
          source.vofs = -1.0 ;
        }
      }
    }

    if ( args.cropping_active )
    {
      // assume we have complete cropping metadata set. This is taken over
      // unconditionally, digest will ignore geometry metadata.

      process_cropping::set_ofs_and_angle
        ( args.uncropped_hfov ,
          args.uncropped_vfov ,
          projection ,
          args.uncropped_width ,
          args.uncropped_height ,
          args.crop_x0 ,
          args.crop_x1 ,
          args.crop_y0 ,
          args.crop_y1 ,
          source.hofs ,
          source.hfov ,
          source.vofs ,
          source.vfov ) ;
    }

    // initialize the source_type object with data from the file

    auto & source_builder
            ( reinterpret_cast < scaffolding_type < source_type > & >
                ( source ) ) ;

    bool success = source_builder.digest ( std::string ( filename ) ,
                                           projection ,
                                           source.hfov ,
                                           source.hofs ,
                                           source.vfov ,
                                           source.vofs ,
                                           args.is_linear ) ;

    if ( ! success )
    {
      return false ;
    }

    ui::projection = source.projection ;

    // currently unused for single images, but does no harm

    source_builder.bounds_3d() ;
    source_builder.squash = args.squash ;

    std::cout << "source image set up to:" << std::endl ;
    source_builder.echo() ;

    // make_interpolator receives the current interpolator (if any) as it's
    // last argument. The rendering thread is free to decide that this (old)
    // interpolator can be reused - the test is quite involved. If the old
    // interpolator is not reused, the rendering thread deletes the old
    // interpolator object and returns a new one.

    ui::p_itp = dispatcher->make_interpolator (
      filename ,
      source ,
      ui::process_linear ,
      initial_alpha_mode ,
      ui::grey_edge ,
      ui::fast_interpolator_degree ,
      ui::quality_interpolator_degree ,
      std::max ( 1.25 , ui::pyramid_scaling_step ) ,
      ui::pyramid_smoothing_level ,
      args.cbf_degree ,
      1 , // tentative
      args.build_pyramids ,
      args.build_raw_pyramids ,
      ui::subimage ,
      ui::p_itp ) ;

   // use this one instead for the 'synthetic image' attempt:

   // ui::p_itp = dispatcher->make_sample_synthetic_image() ;
  }

  return true ;
}

// forward declaration

struct gui_type ;

// now comes the GUI code.

// look_type holds the appearance of an entity like a button or buzzer.
// This can either be a simple text which is rendered on the entity's
// surface or an icon held in a texture. Currently, only text labels
// ar used.

struct look_type
{
  bool has_icon ;
  sf::String label ;
  sf::Texture texture ;

  look_type ( const sf::String & _label = "no label" )
  : has_icon ( false ) ,
    label ( _label )
    { }

  look_type ( const char * cp )
  : has_icon ( false ) ,
    label ( sf::String ( cp ) )
    { }

  look_type ( sf::Texture _texture )
  : has_icon ( true ) ,
    texture ( _texture )
    { }

  void show ( sf::RenderTarget & target ,
              const sf::Vector2f & position ,
              const sf::Vector2f & size) ;
} ;

// entity_type is the base class for data entities which can be handled
// by the GUI. It's a virtual base class, functionality is implemented
// only in classes derived from it. It's class definition declares all
// the virtual functions the GUI will call - some of them are pure virtual,
// like show(), which is entirely the entity's responsibility.

struct entity_type
{
  sf::FloatRect bounds ;
  float & x ;
  float & y ;
  float & w ;
  float & h ;
  look_type look ;
  std::vector < entity_type* > branch ;

  entity_type ( sf::FloatRect _bounds = sf::FloatRect() ,
                const look_type & _look = look_type() )
  : bounds ( _bounds ) ,
    look ( _look ) ,
    x ( bounds.left ) ,
    y ( bounds.top ) ,
    w ( bounds.width ) ,
    h ( bounds.height )
  { } ;

  void attach ( entity_type * pe )
  {
    branch.push_back ( pe ) ;
  }

  void discard ( entity_type * pe )
  {
    auto it = branch.begin() ;
    auto ite = branch.end() ;
    while ( it != ite )
    {
      if ( *it == pe )
      {
        branch.erase ( it ) ;
        return ;
      }
    }
  }

  // this method will display the graphical representation of the entity,
  // reflecting both it's internal state and the state of the GUI.

  virtual void gained_focus() { } ;
  virtual entity_type * describe() ;
  virtual void commit() { } ;

  virtual void show()
  {
    for ( entity_type * ep : branch )
      ep->show() ;
  }

  // entities taking keyboard input will override this method and process
  // keystrokes. They will return true if the keystroke was 'digested',
  // and false otherwise, allowing other code to pick up the keystroke.
  // Only entities which have focus can take keystrokes; when a keystroke
  // occurs, the GUI passes it straight to the entity in focus, if any.

  virtual bool take ( sf::Keyboard::Key key )
  {
    for ( entity_type * ep : branch )
    {
      if ( ep->take ( key ) )
        return true ;
    }
    return false ;
  }

  virtual bool take ( unsigned int unicode )
  {
    for ( entity_type * ep : branch )
    {
      if ( ep->take ( unicode ) )
        return true ;
    }
    return false ;
  }

  virtual bool take ( const sf::Event & event )
  {
    for ( entity_type * ep : branch )
    {
      if ( ep->take ( event ) )
        return true ;
    }
    return false ;
  }

  virtual ~entity_type()
  {
    for ( entity_type * ep : branch )
      memlog >> ep ;
  } ;
} ;

// class gui_type is the class of the gui object. It holds a set
// of pointers to entities and orchestrates their display and interaction
// with events.
// The graphical representation of the GUI is drawn to a RenderTexture
// and persists, it will only be updated if there was user activity
// relevant to the GUI.
// The top-level method is render(). It calls handle for all entities, and
// when it's finished, the graphical representation in the RenderTexture
// will reflect the state of the GUI with all alterations which may have
// occured. This is a slight deviation from a 'pure' imgui insofar as
// we don't rely on the next frame to render the new state: if there is
// no new user input relevant to the GUI, render won't be called again
// as it would be in a pure imgui. So in this GUI implementation, if focus
// changes or labels change, the corresponding entity has to be drawn
// again. While this seems wasteful (after all, the initial state was
// just drawn and is now overwritten) it saves much more by making sure
// that at the end of render() the graphical representation in the
// RenderTexture is up-to-date.
// An alternative would be to use a carry flag that triggers a re-render
// in the next main loop iteration.

// TODO rendering GUI to fullHD is hard-coded

// forward declaration of build_gui, which is coded after the definitions
// of the specific entity types it will create

void build_gui() ;

struct gui_type
{
  // on/off switch. If false, the GUI is invisible and does not
  // respond to any events
  bool on ;

  // flag to signal that the user has affected a change
  bool active_change ;

  bool need_display ;

  // this RenderTexture holds the graphical representation of the GUI
  sf::RenderTexture gui_texture ;

  // this font is used for text
  sf::Font font ;

  // this RenderTexture holds the graphical representation of the
  // status line
  sf::RenderTexture status_texture ;

  // this vector holds pointers to all entities managed by the GUI
  std::vector < entity_type * > eq ;

  entity_type * p_tooltip ;

  // this pointer points to the entity which holds the focus - if any.
  // If no entity holds the focus, this pointer will be NULL.
  entity_type * focus ;

  // mousex and mousey hold the current mouse coordinates. These two values
  // are updated in the main loop whenever a sf::MouseMoved event occurs.
  // They are scaled by the main program to GUI-relative (full HD) coordinates

  unsigned int mousex ;
  unsigned int mousey ;

  // key_pending is set to true by the main loop when a
  // sf::Event::MouseButtonReleased event occurs.
  bool key_pending ;
  bool dscr_pending ;

  // activity is set to true by the main loop if any user activity at all
  // has accured, like mouse movement or key pressing.
  bool activity ;

  int lower_border ;
  int right_border ;
  int show_at ;
  int hide_at ;
  float horizontal_offset ;
  float scale_to_gui ;

  // this view defines the mapping of the GUI texture to the window
  sf::View view ;
  // sf::VideoMode desktop ;

  double gui_extent ;

  // handling of trigger functions.

  typedef void (*trigger_type)() ;

  // buzzer-triggered ui functions are pushed to this vector
  // and executed in the main loop of the viewer.

  std::vector < trigger_type > buzzer_triggered ;

  void push_trigger ( trigger_type trigger )
  {
    for ( auto const & t : buzzer_triggered )
    {
      if ( t == trigger )
        return ;
    }
    buzzer_triggered.push_back ( trigger ) ;
  }

  // once the buzzer is released, pop_trigger removes the
  // corresponding trigger from the vector

  void pop_trigger ( trigger_type trigger )
  {
    for ( auto it = buzzer_triggered.begin() ; it != buzzer_triggered.end() ; )
    {
      if ( *it == trigger )
        buzzer_triggered.erase ( it ) ;
    }
  }

  enum
  {
    BACKGROUND ,
    OUTLINE ,
    BUTTON ,
    BUTTON_TEXT ,
    TOGGLE_ON ,
    TOGGLE_SUSP ,
    TOGGLE_OFF ,
    TOGGLE_TEXT ,
    BUZZER_ON ,
    BUZZER_OFF ,
    BUZZER_TEXT ,
    IO_ON ,
    IO_OFF ,
    IO_TEXT ,
    CURSOR ,
    nr_colours
  } ;

  sf::Color colour [ nr_colours ]
  {
    sf::Color ( 0 , 0 , 0 , 50 ) , //  BACKGROUND ,
    sf::Color ( 180 , 180 , 180 , 180 ) , // OUTLINE ,
    sf::Color ( 50 , 50 , 50 , 255 ) , // BUTTON ,
    sf::Color ( 220 , 220 , 220 , 255 ) , // BUTTON_TEXT ,
    sf::Color ( 100 , 150 , 100 , 255 ) , // TOGGLE_ON ,
    sf::Color ( 200 , 170 , 0 , 255 ) , // TOGGLE_SUSP ,
    sf::Color ( 80 , 50 , 50 , 255 ) , // TOGGLE_OFF ,
    sf::Color ( 200 , 200 , 200 , 255 ) , // TOGGLE_TEXT ,
    sf::Color ( 80 , 80 , 80 , 255 ) , // BUZZER_ON ,
    sf::Color ( 0 , 0 , 0 , 255 ) , // BUZZER_OFF ,
    sf::Color ( 200 , 200 , 200 , 255 ) , // BUZZER_TEXT ,
    sf::Color ( 80 , 80 , 80 , 255 ) , // IO_ON ,
    sf::Color ( 0 , 0 , 0 , 255 ) , // IO_OFF ,
    sf::Color ( 200 , 200 , 200 , 255 ) , // IO_TEXT ,
    sf::Color ( 100 , 100 , 100 , 255 ) // CURSOR ,
  } ;

  unsigned int label_style ; // text style for labels sf::Text::Bold or 0

  unsigned int label_character_size ; // character size for labels

  unsigned int tooltip_character_size ; // character size for tooltips

  void paint_background()
  {
    gui_texture.clear ( sf::Color ( 0 , 0 , 0 , 0 ) ) ;

    sf::RectangleShape rectangle
          ( sf::Vector2f ( right_border , lower_border ) ) ;
    rectangle.setPosition ( sf::Vector2f ( 0 , 0 ) ) ;
    rectangle.setFillColor ( colour [ BACKGROUND ] ) ;
    gui_texture.draw ( rectangle ) ;
  }

  bool set_font ( const std::string & font_filename )
  {
    ui::font_filename = font_filename ;
    return font.loadFromFile ( font_filename ) ;
  }

  // calculate a rectangle relative to a reference. The position
  // of the rectangle will be set off by (dx,dy), and it's size
  // will be the same as the reference's if not specified.
  // currently unused.

  sf::FloatRect position_relative ( const entity_type * reference ,
                                    float dx , float dy ,
                                    float w = 0 , float h = 0 )
  {
    auto const & bounds ( reference->bounds ) ;
    return sf::FloatRect ( bounds.left + dx ,
                           bounds.top + dy ,
                           w == 0 ? bounds.width : w ,
                           h == 0 ? bounds.height : h ) ;
  }

  bool is_initialized ;

  gui_type()
  { 
    is_initialized = false ;
  } ;

  void init()
  {
    if ( is_initialized )
      return ;

    is_initialized = true ;

    // limits to GUI's display and visibility, in GUI coordinates

    lower_border = 150 * ui::fgui ;
    right_border = 1920 * ui::fgui ;

    show_at = 50 * ui::fgui ;
    hide_at = lower_border + 50 * ui::fgui ;

    p_tooltip = 0 ;
    label_style = sf::Text::Bold ;
    label_character_size = 16 * ui::fgui ;
    tooltip_character_size = 24 * ui::fgui ;

    on = false ;
    gui_texture.create ( right_border , hide_at ) ;
    gui_texture.setSmooth ( true ) ;

    status_texture.create ( right_border , 24 * ui::fgui ) ;
    status_texture.setSmooth ( true ) ;
    status_texture.clear ( sf::Color ( 0 , 0 , 0 , 128 ) ) ;

    horizontal_offset = 0.0f ;

    build_gui() ;
    reset() ;
  }

  ~gui_type()
  {
    for ( auto * pe : eq )
      memlog >> pe ;
  }

  // the GUI's 'take' method immediately passes the key on to the
  // entity in focus, if there is one.

  bool take ( sf::Keyboard::Key k )
  {
    if ( on && ( focus != 0 ) )
      return focus->take ( k ) ;
    return false ;
  }

  bool take ( unsigned int unicode )
  {
    if ( on && ( focus != 0 ) )
      return focus->take ( unicode ) ;
    return false ;
  }

  bool take ( const sf::Event & event )
  {
    if ( event.type == sf::Event::MouseWheelScrolled )
    {
      auto delta = 100 * event.mouseWheelScroll.delta ;

      if (    sf::Keyboard::isKeyPressed ( sf::Keyboard::LControl )
           || sf::Keyboard::isKeyPressed ( sf::Keyboard::RControl ) )
      {
        // Ctrl + MouseWheelScrolled modifies the size of the GUI

        gui_extent += delta ;
      }
      else
      {
        // plain MouseWheelScrolled modifies the position of the GUI

        auto sz = ui::p_screen->p_window->getSize() ;

        horizontal_offset -= delta ;

        if ( horizontal_offset < 0 )
          horizontal_offset = 0 ;

        if ( horizontal_offset + sz.x > gui_extent )
          horizontal_offset = gui_extent - sz.x ;
      }

      return true ;
    }

    // if the GUI is off, or the event occured outside
    // the sensitive area, return false straight away

    if ( event.type == sf::Event::MouseButtonReleased )
    {
      // idea: some entities take mouse button release events
      // like 'increase' and 'decrease' buttons providing the
      // same functionality as pressed and held keys. If such
      // an entity is in focus, it can react to such an event.
      // We trust that such event will only be routed here if
      // no click-and-drag is in process.
      if ( event.mouseButton.button == sf::Mouse::Left )
      {
        if ( focus && focus->take ( event ) )
          return true ;
      }
      else if ( event.mouseButton.button == sf::Mouse::Right )
      {
        if ( p_tooltip )
        {
          memlog >> p_tooltip ;
          p_tooltip = 0 ;
        }
      }
    }

    // to check against lower_border, we need to convert fullHD-relative
    // coordinates to window coordinates:
    {
      double screen_to_gui = right_border / gui_extent ;

      double limit = lower_border / screen_to_gui ;
      if ( ( ! on ) || ( event.mouseButton.y > limit ) )
        return false ;
    }

    if ( event.type == sf::Event::MouseButtonPressed )
    {
      // gui is on and there was a mouse click in the sensitive
      // area. We'll process this event.
      if ( event.mouseButton.button == sf::Mouse::Left )
        key_pending = true ;
      else if ( event.mouseButton.button == sf::Mouse::Right )
        dscr_pending = true ;
    }
    // Since the event occured while the GUI is on and, and the
    // event occured inside the GUI's sensitive area, we return
    // true, event if we did not react to the event.
    return true ;
  }

  void handle ( entity_type * pe )
  {
    if ( ! pe )
      return ;

    // if there was a mouse button click, check it's position against
    // the rectangle which the entity occupies. If the click occured
    // inside this area, set the focus to the entity. If another entity
    // previously held the focus, redraw this entity after the focus was
    // taken away from it. We must take into account the horizontal
    // offset of the part of the GUI stripe we show, in GUI units

    double scaled_offset = horizontal_offset * right_border / gui_extent ;

    if (    ( mousex + scaled_offset ) >= pe->x
         && ( mousex + scaled_offset ) < pe->x + pe->w
         && mousey >= pe->y
         && mousey < pe->y + pe->h )
    {
      if ( key_pending )
      {
        auto old_focus = focus ;
        focus = pe ;
        pe->gained_focus() ;
        key_pending = false ;
        if ( focus != old_focus && old_focus != 0 )
          old_focus->show() ;
      }
      if ( dscr_pending )
      {
        p_tooltip = pe->describe() ;
        dscr_pending = false ;
      }
    }

    // let the entity display itself

    pe->show() ;

    // show subentities, if any

    for ( entity_type * ep : pe->branch )
      handle ( ep ) ;
  }

  void reset()
  {
    mousex = mousey = 0 ;
    activity = false ;

    // calculate the desktop's aspect ratio

    double aspect_ratio = desktop.width ;
    aspect_ratio /= desktop.height ;

    // clamp that at 16:9 to avoid a huge GUI on
    // ultrawide screens - for smaller aspect ratios
    // (like 19:10, 3:2) we fill the width

    const double fhd_aspect_ratio = 16.0 / 9.0 ;
    if ( aspect_ratio > fhd_aspect_ratio )
      aspect_ratio = fhd_aspect_ratio ;

// initially I coded to always fill the desktop width:
//     gui_extent =   ( ini::args.gui_extent <= 0 )
//                  ? desktop.width
//                  : ini::args.gui_extent ;

    gui_extent =   ( ini::args.gui_extent <= 0 )
                 ? desktop.height * aspect_ratio
                 : ini::args.gui_extent ;

    std::cout << "gui_type::reset() was called, gui_extent = "
              << gui_extent << std::endl ;
  }

  // render checks if there was any activity. If not, it returns straight
  // away, but if yes, it redraws all ui elements. Here we can implement
  // the ui as an imgui, by processing more data from the event loop and
  // adapting the rendering process accordingly - and by gleaning the state
  // of the ui from direct or indirect return values.

  void render()
  {
    if ( ( ! on ) || ( ! activity ) )
      return ;

    paint_background() ;

    // call 'handle' on all entities in the entity vector
    for ( auto & pe : eq )
      handle ( pe ) ;

    if ( p_tooltip )
    {
      p_tooltip->show() ;
    }
    // manifest the cumulated draw actions on the render texture
    gui_texture.display() ;
    activity = false ;
    key_pending = false ;
    dscr_pending = false ;
  }

  void draw_status_line ( sf::RenderWindow * p_window )
  {
    // some modes of operation suppress the status line altogether

    if ( ui::suppress_display || ui::slideshow_on )
      return ;

    // if the status line is switched off altogether, return immediately

    if ( ! ui::show_status_line )
      return ;

    // and if the status has not changed, return as well.

    if ( ! status_flag )
      return ;

    // status is on, and has changed. We need to display:

    need_display = true ;

    // we clear the status line's string. If no new content is
    // added below, the empty string will signal to the presenter
    // not to display the status line at all.

    ui::status_line = std::string() ;

    // under status_mutex, we look at the status-relevant variables
    // and build the status line's string accordingly.

    {
      std::lock_guard < std::mutex > lk ( status_mutex ) ;

      if ( status_building || now_loading.size() )
      {
        ui::status_line += "building interpolators...   " ;
        if ( now_loading.size() )
          ui::status_line += "loading image " + now_loading + "   " ;
      }

      if ( status_light_balancing )
      {
        ui::status_line += "calculating light balance" ;
      }

      int snapshots = snapshots_pending ;
      if ( snapshots  )
        ui::status_line += "snapshots pending: "
        + std::to_string ( snapshots  ) ;

      // we have honoured the status flag, which was set by code
      // producing a status change. Now we reset the flag to false.

      status_flag = false ;
    }

    // if the status line is empty, there is no status to display,
    // and we display metadata in the status line, if the user has
    // opted to do so.

    if ( ui::status_line.size() == 0 )
    {
      ui::metadata_info_string() ;
    }

    // if the status line is not empty, we need to draw the text
    // to the status line's render texture. The presenter will
    // display the status line's render texture on the window.
    // If the status line is empty, there is neither 'proper'
    // status text nor metadata display, and the empty status line
    // string will keep the presenter from displaying it at all.

    if ( ui::status_line.size() != 0 )
    {
      sf::Text text ( ui::status_line , font ) ;
      text.setCharacterSize ( 20 * ui::fgui ) ;
      text.setStyle ( sf::Text::Regular ) ;
      text.setFillColor ( colour [ gui_type::BUTTON_TEXT ] ) ;
      sf::Vector2f pos ( 10 * ui::fgui , 0 ) ;
      text.setPosition ( pos ) ;

      status_texture.clear ( sf::Color ( 0 , 0 , 0 , 128 ) ) ;
      status_texture.draw ( text ) ;
      status_texture.display() ;
    }
  }

  // draw draws the ui's render texture onto the main window.
  // Since this is GPU only stuff, it's fast and can be done every frame
  // without impeding overall performance much.

  void draw ( sf::RenderWindow * p_window )
  {
    render() ;
    sf::Sprite sprite ( gui_texture.getTexture() ) ;
    auto sz = p_window->getSize() ;

    // gui_to_screen is the factor we need to convert GUI-internal
    // coordinates (up to 1920X200) to screen coordinates, which
    // depend on the current video mode.

    double gui_to_screen = gui_extent / right_border ;

    // horizontal_offset is in screen pixel units, and the sum of
    // horizontal_offset and the window's size must not exceed the
    // desktop width or be negative.

    if ( horizontal_offset + sz.x > gui_extent )
      horizontal_offset = gui_extent - sz.x ;

    if ( horizontal_offset < 0 )
      horizontal_offset = 0 ;

    // to set the view's horizontal offset and extent, we need values
    // in GUI pixel units. The horizontal offset defines how far away
    // from the GUI stripe's left margin we position the 'pickup'
    // window of the view.

    float hofs_source = horizontal_offset / gui_to_screen ;
    float w_source = sz.x / gui_to_screen ;

    view.reset ( { hofs_source , 0.0f , w_source , float(hide_at) } ) ;

    // To define the viewport, we need fractions of window size

    float gui_height_on_screen = float(hide_at) * gui_to_screen ;
    float gui_height_fraction = gui_height_on_screen / sz.y ;

    // The section of the GUI stripe we show on-screen will always
    // occupy the entire window width (hence, 1.0) and just so much
    // of the window's height to keep the aspect ratio 1:1

    view.setViewport ( { 0.0f , 0.0f , 1.0f , gui_height_fraction } ) ;

    p_window->setView ( view ) ;
    p_window->draw ( sprite ) ;
  }

  // to draw an editable string with a cursor, we need to know the width
  // in pixels of a given string in the gui's font with a specific character
  // size and in bold or not. We get the figure by iterating over the string,
  // obtaining the glyph for each character and summing up, adding additional
  // kerning the font may prescribe.

  float px_size ( sf::String s , unsigned int character_size , bool bold )
  {
    float result = 0 ;
    for ( int pos = 0 ; pos < s.getSize() ; pos++ )
    {
      auto c = s[pos] ;
      auto g = font.getGlyph ( c , character_size , bold ) ;
      result += g.advance ;
      if ( pos > 0 )
        result += font.getKerning ( s[pos-1] , c , character_size ) ;
    }
    return result ;
  }
} ;

// gui_type has a single instance 'gui' which the entity code will access
// in it's methods. The gui, on the other hand, accesses entities only via
// the virtual methods defined in class entity's definition; it cannot know
// any specific functionality an entity may have, since it's not aware of
// any of the derived entity classes.

gui_type gui ;

#ifdef USE_IMGUI

namespace ui
{
  void on_toggle_legacy_gui()
  {
    sf::Vector2i mouse
      = sf::Mouse::getPosition ( *(p_screen->p_window) ) ;

    if ( ui::legacy_gui == true )
    {
      ui::legacy_gui = false ;
      gui.on = false ;
      gui.focus = NULL ;
      ui::show_imgui = true ;
      if ( mouse.y < 200 )
        show_main_menu = true ;
    }
    else
    {
      ui::legacy_gui = true ;
      if ( mouse.y < 200 )
        gui.on = true ;
      gui.focus = NULL ;
      ui::show_imgui = false ;
      show_main_menu = false ;
    }
    gui.need_display = true ;
  }
} ;

#endif // #ifdef USE_IMGUI

void look_type::show ( sf::RenderTarget & target ,
                       const sf::Vector2f & position ,
                       const sf::Vector2f & size )
{
  if ( has_icon )
  {
    sf::Sprite sprite ( texture ) ;
    sprite.setPosition ( position ) ;
    target.draw ( sprite ) ; // rely on sprite's correct size
  }
  else
  {
    sf::Text text ( label , gui.font ) ;
    text.setCharacterSize ( gui.label_character_size ) ;
    text.setStyle ( gui.label_style ) ;
    text.setFillColor ( gui.colour [ gui_type::BUTTON_TEXT ] ) ;
    // to draw the label centered, we check the text's rendered size
    auto bounds = text.getLocalBounds() ;
    float empty_x = size.x - bounds.width ;
    float empty_y = size.y - bounds.height ;
    sf::Vector2f pos = position ;
    pos.x += empty_x / 2 ;
    // here I'd code: pos.y += empty_y / 2 ;
    // empty_y has the correct value, but the placement goes wrong.
    // SO TODO: hardcoded, depends on gui.label_character_size
    pos.y += ( 4 * ui::fgui ) ; // TODO: doesn't work as expected empty_y / 2 ;
    text.setPosition ( pos ) ;
    target.draw ( text ) ;
  }
}

// int_entity_type is a concrete class derived from entity_type which
// refers to an int value and provides functionality to have this value
// displayed and modified by the GUI.

struct button_entity_type
: public entity_type
{
  std::function < void() > action ;
  look_type label ;
  sf::Color tx_colour ;
  sf::Color colour ;

  button_entity_type ( std::function < void() > _action ,
                       look_type _label ,
                       sf::FloatRect _bounds ,
                       const look_type & _dsc = "button entity, no description" )
  : action ( _action ) ,
    label ( _label ) ,
    entity_type ( _bounds , _dsc )
    {
      colour = gui.colour [ gui_type::BUTTON ] ;
      tx_colour = gui.colour [ gui_type::BUTTON_TEXT ] ;
    } ;

  // a button, on gaining focus, executes it's action and immediately
  // relinquishes focus

  virtual void gained_focus()
  {
    action() ;
    gui.focus = 0 ;
  }

  virtual void show()
  {
    sf::RectangleShape rectangle ( sf::Vector2f ( w , h ) ) ;
    rectangle.setPosition ( sf::Vector2f ( x , y ) ) ;
    rectangle.setFillColor ( colour ) ;
    rectangle.setOutlineThickness ( 1 ) ;
    rectangle.setOutlineColor ( gui.colour [ gui_type::OUTLINE ] ) ;
    gui.gui_texture.draw ( rectangle ) ;

    label.show ( gui.gui_texture ,
                 sf::Vector2f ( x , y ) ,
                 sf::Vector2f ( w , h ) ) ;

    entity_type::show() ;
  }
} ;

struct toggle_entity_type
: public button_entity_type
{
  std::function < void() > action ;
  std::function < bool() > get_state ;
  look_type label ;
  sf::Color colour ;
  sf::Color colour2 ;
  sf::Color tx_colour ;

  toggle_entity_type ( std::function < void() > _action ,
                       look_type _label ,
                       std::function < bool() > _get_state ,
                       sf::FloatRect _bounds ,
                       const look_type & _dsc = "toggle entity, no description" )
  : action ( _action ) ,
    label ( _label ) ,
    get_state ( _get_state ) ,
    button_entity_type ( _action , _label , _bounds , _dsc )
    {
      colour = gui.colour [ gui_type::TOGGLE_OFF ] ;
      colour2 = gui.colour [ gui_type::TOGGLE_ON ] ;
      tx_colour = gui.colour [ gui_type::TOGGLE_TEXT ] ;
    } ;

// a toggle, on gaining focus, executes it's action and immediately
// relinquishes focus

  virtual void gained_focus()
  {
    action() ;
    gui.focus = 0 ;
  }

  virtual void show()
  {
    sf::RectangleShape rectangle ( sf::Vector2f ( w , h ) ) ;
    rectangle.setPosition ( sf::Vector2f ( x , y ) ) ;
    rectangle.setFillColor ( get_state() ? colour2 : colour ) ;
    rectangle.setOutlineThickness ( 1 ) ;
    rectangle.setOutlineColor ( gui.colour [ gui_type::OUTLINE ] ) ;
    gui.gui_texture.draw ( rectangle ) ;

    label.show ( gui.gui_texture ,
                 sf::Vector2f ( x , y ) ,
                 sf::Vector2f ( w , h ) ) ;

    entity_type::show() ;
  }
} ;

struct toggle3_entity_type
: public toggle_entity_type
{
  std::function < bool() > get_susp ;
  sf::Color colour3 ;

  toggle3_entity_type ( std::function < void() > _action ,
                        look_type _label ,
                        std::function < bool() > _get_state ,
                        std::function < bool() > _get_susp ,
                        sf::FloatRect _bounds ,
                        const look_type & _dsc = "toggle3 entity, no description" )
  : get_susp ( _get_susp ) ,
    toggle_entity_type ( _action , _label , _get_state , _bounds , _dsc )
    {
      colour3 = gui.colour [ gui_type::TOGGLE_SUSP ] ;
    } ;

// a toggle, on gaining focus, executes it's action and immediately
// relinquishes focus

  virtual void gained_focus()
  {
    action() ;
    gui.focus = 0 ;
  }

  virtual void show()
  {
    sf::RectangleShape rectangle ( sf::Vector2f ( w , h ) ) ;
    rectangle.setPosition ( sf::Vector2f ( x , y ) ) ;
    sf::Color fill_colour = colour ;
    if ( get_state() )
      fill_colour = get_susp() ? colour3 : colour2 ;
    rectangle.setFillColor ( fill_colour ) ;
    rectangle.setOutlineThickness ( 1 ) ;
    rectangle.setOutlineColor ( gui.colour [ gui_type::OUTLINE ] ) ;
    gui.gui_texture.draw ( rectangle ) ;

    label.show ( gui.gui_texture ,
                 sf::Vector2f ( x , y ) ,
                 sf::Vector2f ( w , h ) ) ;

    entity_type::show() ;
  }
} ;

// buzzer_entity_type is similar to a toggle_entity_type. It's used
// in the same way as held keyboard keys: When the user depresses the
// mouse button (TODO: currently, any button), the button is highlited
// and remains so until the mouse button is released again.
// A buzzer does not execute it's 'action' on gaining focus, instead
// it 'pushes' the action to a vector in the gui object, where it is
// executed repeatedly while the buzzer is held.
// Why 'buzzer'? It's like a door buzzer, which has an effect while
// it's held depressed.

struct buzzer_entity_type
: public entity_type
{
  void (*action)() ;
  look_type label ;
  sf::Color tx_colour ;
  sf::Color colour ;
  sf::Color colour2 ;

  buzzer_entity_type ( void (*_action)() ,
                       look_type _label ,
                       sf::FloatRect _bounds ,
                       const look_type & _dsc = "buzzer entity, no description" )
  : action ( _action ) ,
    label ( _label ) ,
    entity_type ( _bounds , _dsc )
    {
      colour = gui.colour [ gui_type::BUZZER_OFF ] ;
      colour2 = gui.colour [ gui_type::BUZZER_ON ] ;
      tx_colour = gui.colour [ gui_type::BUZZER_TEXT ] ;
    } ;

  // a buzzer, on gaining focus, 'pushes' it's action and *keeps* focus
  // the action will be called later when the gui's set of buzzer-
  // triggered functions is executed. Note this difference: other
  // entities execute their action when they gain focus, because their
  // action is something singular: an 'acute' event. buzzers are for
  // 'chronic' events which persist for some time, like keys which
  // are held depressed.

  virtual void gained_focus()
  {
    gui.push_trigger ( action ) ;
  }

  // on mouse button release, a buzzer relinquishes focus after calling
  // the action with a 'false' argument

  virtual bool take ( const sf::Event & event )
  {
    if ( event.type == sf::Event::MouseButtonReleased )
    {
      gui.pop_trigger ( action ) ;
      gui.focus = 0 ;
    }
    return true ;
  }

  virtual void show()
  {
    sf::RectangleShape rectangle ( sf::Vector2f ( w , h ) ) ;
    rectangle.setPosition ( sf::Vector2f ( x , y ) ) ;
    if ( gui.focus == this )
      rectangle.setFillColor ( colour2 ) ;
    else
      rectangle.setFillColor ( colour ) ;
    rectangle.setOutlineThickness ( 1 ) ;
    rectangle.setOutlineColor ( gui.colour [ gui_type::OUTLINE ] ) ;
    gui.gui_texture.draw ( rectangle ) ;

    label.show ( gui.gui_texture ,
                 sf::Vector2f ( x , y ) ,
                 sf::Vector2f ( w , h ) ) ;

    entity_type::show() ;
  }
} ;

struct editable_string
: public sf::String
{
  unsigned int cursor ;

  template < typename ... argtypes >
  editable_string ( argtypes ... args )
  : sf::String ( args ... ) ,
    cursor ( 0 )
  { } ;

  void set_string ( const sf::String & str )
  {
    sf::String & sr ( *this ) ;
    sr = str ;
  }

  void cursor_left()
  {
    if ( cursor > 0 )
      cursor-- ;
  }

  void cursor_right()
  {
    if ( cursor < getSize() )
      cursor++ ;
  }

  void insert ( const sf::String &str )
  {
    sf::String::insert ( cursor , str ) ;
    cursor += str.getSize() ;
  }

  void erase ( int n = 1 )
  {
    sf::String:: erase ( cursor , n ) ;
  }

  void backspace()
  {
    if ( cursor > 0 )
    {
      --cursor ;
      erase() ;
    }
  }

  // showing an editable entity is potentially quite involved.
  // The method takes a reference to a small RenderTexture of the size
  // the field has on-screen. Rendering to this texture automatically
  // clips the content if it's too large - this is a safety measure, we
  // might instead accept a 2D size and rely on the code not to
  // overshoot it (TODO try).
  // It also receives font and character size. Next are two references
  // to values held in the string_entity_type (TODO why not here?) which
  // help in showing only a part of the string where the cursor is, in case
  // the text is too large. Finally, a flag determining whether the cursor
  // should be shown or not.
  // TODO text style and text and background colours

  void show ( sf::RenderTexture & buffer ,
              sf::Font & font ,
              unsigned int charsz ,
              unsigned int text_style ,
              unsigned int & left_border ,
              unsigned int & right_border ,
              bool show_cursor = false )
  {
    auto buffer_size = buffer.getSize() ;

//     std::cout << "buffer size     " << buffer_size.x
//               << ", " << buffer_size.y << std::endl ;
//
//     std::cout << "incoming character size " << charsz << std::endl ;
//
//     std::cout << "string is      <"
//               << std::string(*this) << ">" << std::endl ;

    sf::Text text ( *this , font , charsz ) ;
    text.setStyle ( text_style ) ;
    text.setFillColor ( gui.colour [ gui_type::IO_TEXT ] ) ;

    auto tx_bounds = text.getLocalBounds() ;

//     std::cout << "tx_bounds: " << tx_bounds.width
//               << ", " << tx_bounds.height << std::endl ;

    unsigned int tx_width = tx_bounds.width ;

    // if the cursor is right at the end, we have a bit more to display

    unsigned int empty_cursor_width = 10 * ui::fgui ;

    if ( cursor == getSize() )
      tx_width += empty_cursor_width ;

//     std::cout << "tx_width: " << tx_width << std::endl ;

    // now we figure out the pixel position where the cursor
    // should display, relative to the whole text, and also the
    // position for the next character.

    auto v2f_cursor_px = text.findCharacterPos ( cursor ) ;
    auto v2f_next_pos_px = text.findCharacterPos ( cursor + 1 ) ;

    // if we're right at the end of the string, both values are
    // the same, in which case we use a 10 pixel wide cursor

    unsigned int cursor_starts_at = v2f_cursor_px.x ;
    unsigned int cursor_ends_at = v2f_next_pos_px.x ;
    if ( cursor == getSize() )
      cursor_ends_at = tx_width ;
    unsigned int cursor_width_px = cursor_ends_at - cursor_starts_at ;

//     std::cout << "cursor width: " << cursor_width_px << std::endl ;
//     std::cout << "cursor start: " << cursor_starts_at << std::endl ;
//     std::cout << "cursor end:   " << cursor_ends_at << std::endl ;

    if ( tx_width <= buffer_size.x )
    {
//       std::cout << "text fits buffer" << std::endl ;
      left_border = 0 ;
      right_border = tx_width ;
    }
    else if (    cursor_starts_at < left_border
              || cursor_ends_at > right_border )
    {
      // cursor in not inside the current text window boundaries

//       std::cout << "left_border: " << left_border << std::endl ;
//       std::cout << "right_border: " << right_border << std::endl ;

      if ( cursor_ends_at > buffer_size.x )
      {
//         std::cout << "cursor end beyond buffer's end, adapt borders"
//                   << std::endl ;
        right_border = cursor_ends_at ;
        left_border = right_border - buffer_size.x ;
//         std::cout << "new left_border: " << left_border << std::endl ;
//         std::cout << "new right_border: " << right_border << std::endl ;
      }

      if ( cursor_starts_at < left_border )
      {
//         std::cout << "cursor start before buffer's start, adapt borders"
//                   << std::endl ;
        left_border = cursor_starts_at ;
        right_border = left_border + buffer_size.x ;
//         std::cout << "new left_border: " << left_border << std::endl ;
//         std::cout << "new right_border: " << right_border << std::endl ;
      }
    }
 
    buffer.clear ( sf::Color ( 0 , 0 , 0 , 0 ) ) ;

    if ( show_cursor )
    {
      // draw cursor as a small rectangle right where the character
      // on the cursor will appear, or after the last character, if
      // it is at the end of the editable.
      sf::RectangleShape cursor
        ( sf::Vector2f ( cursor_width_px , buffer_size.y ) ) ;
      cursor.setPosition
      ( sf::Vector2f ( cursor_starts_at - left_border , 0 ) ) ;
      // TODO: cursor colour
      cursor.setFillColor ( gui.colour [ gui_type::CURSOR ] ) ;
      buffer.draw ( cursor ) ;
    }

    // we use a scratch texture if the beginning of the
    // string does not fit, which should be rare. This is due to the fact
    // that a negative position does not work - this would be the most
    // elegant solution, instead we have to draw the whole text and
    // transport only the portion starting at left_border into the
    // buffer via a sprite with the scratch buffer's texture.

    if ( left_border > 0 )
    {
      sf::RenderTexture scratch ;
      scratch.create ( right_border , buffer_size.y ) ;
      scratch.clear ( sf::Color ( 0 , 0 , 0 , 0 ) ) ;
      scratch.draw ( text ) ;
      scratch.display() ;
      sf::Sprite sprite ( scratch.getTexture() ) ;
      sprite.setOrigin ( left_border , 0 ) ;
      buffer.draw ( sprite ) ;
      buffer.display() ;
    }
    else
    {
//       std::cout << "calling buffer.draw()" << std::endl ;
      buffer.draw ( text ) ;
//       std::cout << "calling buffer.display()" << std::endl ;
      buffer.display() ;
//       std::cout << "show() is done...." << std::endl ;
    }
  }
} ;

// string_entity_type handles an editable string. It's not meant to
// be used to manage sf::String instances (use text_entity_type)
// If it's used as an entity by the GUI, it's content has to be
// extracted manually by accessing the contained editable_string.
// TODO: position cursor with mouse click
// TODO: can't regain focus with keyboard

struct string_entity_type
: public entity_type
{
  editable_string value ;
  unsigned int left_border , right_border ;
  unsigned int character_size ;
  unsigned int text_style ;
  sf::Color tx_colour ;
  sf::Color colour ;
  sf::Color colour2 ;
  sf::RenderTexture buffer ;

  string_entity_type ( const sf::String & _value ,
                       sf::FloatRect _bounds ,
                       const look_type & _dsc = "string entity, no description")
  : value ( _value ) ,
    entity_type ( _bounds , _dsc )
    {
      buffer.create ( ( w - 6 ) , ( h - 4 ) ) ;
      colour = gui.colour [ gui_type::IO_OFF ] ;
      colour2 = gui.colour [ gui_type::IO_ON ] ;
      tx_colour = gui.colour [ gui_type::IO_TEXT ] ;
      character_size = 20 * ui::fgui ;
      text_style = sf::Text::Bold ;
      left_border = 0 ;
      right_border = ( w - 6 ) ;
    } ;

  virtual void gained_focus()
  {
    // make a sf::Text from 'value' and get the offset to the
    // glyph the cursor points to, minus what's in 'left_border'
    sf::Text text ( value , gui.font , character_size ) ;
    // must set text to bold, otherwise the count won't match
    text.setStyle ( text_style ) ;

    auto len = value.getSize() ;
    if ( value.cursor > len )
      value.cursor = len ;

    auto getx = [&]( unsigned int d ) -> unsigned int
    {
      auto c = text.findCharacterPos ( value.cursor + d ) ;
      return c.x - left_border ;
    } ;

    auto cursor_px = getx ( 0 ) ;
    auto cursor_px_next = getx ( 1 ) ;
    
    double scaled_offset = gui.horizontal_offset * gui.right_border / gui.gui_extent ;

    float mouse_px = gui.mousex + scaled_offset - x - 3 ;
    if ( mouse_px < 0 )
      mouse_px = 0 ;

    while ( true )
    {
      if ( mouse_px >= cursor_px_next )
      {
        // mouse click happened on or to the right of cursor_px_next
        if ( cursor_px_next == cursor_px )
        {
          // that's the right end already
          // don't increase further, stop here
          break ;
        }
        else
        {
          // there is still another character to the right, try there
          value.cursor++ ;
          cursor_px = cursor_px_next ;
          cursor_px_next = getx ( 1 ) ;
        }
      }
      else if ( mouse_px < cursor_px )
      {
        // mouse click happend to the left of the cursor
        value.cursor-- ;
        cursor_px_next = cursor_px ;
        cursor_px = getx ( 0 ) ;
      }
      else
      {
        // mouse_px must be in [ cursor_px , cursor_px_next [
        break ;
      }
    }
  }

  virtual void show()
  {
    // draw the background rectangle

    sf::RectangleShape rectangle ( sf::Vector2f ( w , h ) ) ;
    rectangle.setPosition ( sf::Vector2f ( x , y ) ) ;
    rectangle.setFillColor ( gui.focus == this ? colour : colour2 ) ;
    rectangle.setOutlineThickness ( 1 ) ;
    rectangle.setOutlineColor ( gui.colour [ gui_type::OUTLINE ] ) ;
    gui.gui_texture.draw ( rectangle ) ;

    // get the text (or part of it) plus cursor into the buffer

    value.show ( buffer , gui.font , character_size , text_style ,
                 left_border , right_border ,
                 gui.focus == this ) ;

    // now make a sprite from the buffer and draw it to the GUI's
    // RenderTexture

    sf::Sprite sprite ( buffer.getTexture() ) ;
    sprite.setPosition ( x + 3 * ui::fgui ,
                         y + 2 * ui::fgui ) ; // TODO: hardcoded.
    gui.gui_texture.draw ( sprite ) ;

    entity_type::show() ;
  }

  virtual bool take ( unsigned int unicode )
  {
    // TODO: better test
    if ( unicode >= 32 && unicode < 127 )
    {
      value.insert ( sf::String ( unicode ) ) ;
      return true ;
    }
//     else
//     {
//       std::cout << "unhandled unicode " << unicode << std::endl ;
//     }
    return false ;
  } ;

  virtual bool take ( sf::Keyboard::Key k )
  {
    // when offered KeyPressed events, we have to be careful not
    // to collide with processing of TextEntered events, and also
    // not to touch KeyPressed events which 'normal' pv processing
    // will react to, like number keys and letters assigned to
    // specific actions.
    if ( k >= sf::Keyboard::Num0 && k <= sf::Keyboard::Num9 )
    {
      // ignore number keys, they are accepted as unicode text
      // already - but return true, to signal that the key should
      // *not* be looked at by 'normal' pv processing
      return true ;
    }
    if ( k >= sf::Keyboard::A && k <= sf::Keyboard::Z )
    {
      // ignore alphabetic keys, they are accepted as unicode text
      // already - but return true, to signal that the key should
      // *not* be looked at by 'normal' pv processing
      return true ;
    }
    if (   k == sf::Keyboard::Add
        || k == sf::Keyboard::Subtract
        || k == sf::Keyboard::Multiply
        || k == sf::Keyboard::Space
        )
    {
      // pv-specific: these keys can also be taken into a string as
      // Text events
      return true ;
    }
    // process text-editing keys, like cursor keys etc.
    switch ( k )
    {
      case sf::Keyboard::Home:
        value.cursor = 0 ;
        break ;
      case sf::Keyboard::End:
        value.cursor = value.getSize() ;
        break ;
      case sf::Keyboard::BackSpace:
        value.backspace() ;
        break ;
      case sf::Keyboard::Delete:
        value.erase() ;
        break ;
      case sf::Keyboard::Left:
        value.cursor_left() ;
        break ;
      case sf::Keyboard::Right:
        value.cursor_right() ;
        break ;
      case sf::Keyboard::Return:
        commit() ;
        gui.focus = 0 ;
        break ;
      default:
        // all other keys are *not* taken. this includes function
        // keys, escape etc., which must be processed by 'normal'
        // pv processing.
        return false ;
        break ;
    }
    return true ;
  }

} ;

// text_entity_type manages a sf::String via a string_entity_type.
// on commit, the referenced sf::String receives a copy of the
// content of the editable_string.

struct text_entity_type
: public string_entity_type
{
  sf::String & value ;

  // the constructor takes a reference to the integer value to be handled,
  // and the position and size of the field where the user can interact
  // with this value.

  text_entity_type ( sf::String & _value ,
                     sf::FloatRect _bounds ,
                     const sf::String & _dsc = "text entity, no description" )
  : value ( _value ) ,
    string_entity_type ( _value , _bounds , _dsc )
  { } ;

  virtual void show()
  {
    if ( this != gui.focus )
    {
      string_entity_type::value.set_string ( value ) ;
    }
    string_entity_type::show() ;
  }

  virtual void commit()
  {
    value = string_entity_type::value ;

    gui.need_display = true ; // TODO do we need this
  }
} ;

struct active_text_entity_type
: public text_entity_type
{
  std::function < void() > action ;

  active_text_entity_type ( std::function < void() > _action ,
                            sf::String & _value ,
                            sf::FloatRect _bounds ,
                            const sf::String & _dsc = "active text entity, no description" )
  : text_entity_type ( _value , _bounds , _dsc ) ,
    action ( _action )
    { } ;

  virtual void commit()
  {
    text_entity_type::commit() ;
    action() ;
  }
} ;

// passive_text_entity_type is used to display text like tool tips, which
// may carry on over several lines. The text is not editable. If the text
// would extend beyond the right border of the GUI's RenderTarget, it's
// shifted to the left.

struct passive_text_entity_type
: public entity_type
{
  sf::String value ;

  passive_text_entity_type ( const sf::String & _value ,
                             sf::FloatRect _bounds )
  : value ( _value ) ,
    entity_type ( _bounds )
  { } ;

  virtual bool take ( unsigned int unicode )
  {
    return false ;
  } ;

  virtual bool take ( sf::Keyboard::Key k )
  {
    return false ;
  } ;

  virtual void show()
  {
    sf::Text text ( value , gui.font ) ;
    text.setCharacterSize ( gui.tooltip_character_size ) ;
    text.setStyle ( sf::Text::Bold ) ;
    text.setFillColor ( gui.colour [ gui_type::IO_TEXT ] ) ;
    text.setPosition ( sf::Vector2f ( x , y ) ) ;

    auto bounds = text.getLocalBounds() ;

    sf::RectangleShape rectangle
      ( { bounds.width + 24 * ui::fgui ,
          bounds.height + 24 * ui::fgui } ) ;

    int offset = -10 * ui::fgui ;
    auto sz = ui::p_screen->p_window->getSize() ;
    auto right_limit = sz.x ;
    if ( gui.gui_extent < right_limit )
      right_limit = gui.gui_extent ;

    auto width =   ( gui.horizontal_offset + right_limit )
                 * gui.right_border / gui.gui_extent ;

    auto x_max = width - 30 * ui::fgui ;

    if ( x + bounds.width >= x_max )
      offset = ( x + bounds.width ) - x_max ;

    // we don't want to have the text box extend too far down:

    int max_y = y + bounds.height + 24 * ui::fgui ;
    if ( max_y > gui.hide_at )
      max_y = gui.hide_at ; 
    int top_y = max_y - ( bounds.height + 24 * ui::fgui ) ;

    text.setPosition ( sf::Vector2f ( x - offset + 8 * ui::fgui ,
                                      top_y + 4 * ui::fgui ) ) ;
    rectangle.setPosition ( sf::Vector2f ( x - offset - 4 * ui::fgui ,
                                           top_y ) ) ;
    rectangle.setFillColor ( gui.colour [ gui_type::IO_OFF ] ) ;
    rectangle.setOutlineThickness ( 1 ) ;
    rectangle.setOutlineColor ( gui.colour [ gui_type::OUTLINE ] ) ;

    gui.gui_texture.draw ( rectangle ) ;

    gui.gui_texture.draw ( text ) ;

    entity_type::show() ;
  }
} ;

entity_type * entity_type::describe()
{
  // we don't want the tooltip to be cut off if the button it belongs
  // to has it's left border outside the displayed portion of the GUI

  double scaled_offset
    = gui.horizontal_offset * gui.right_border / gui.gui_extent ;
  double xmin = std::max ( scaled_offset , double ( x ) ) ;

  return new passive_text_entity_type
             ( look.label ,
               sf::FloatRect ( xmin , y , 100 * ui::fgui , 50  * ui::fgui) ) ;
}

// int_entity_type handles an integral value by reference. It inherits
// from string_entity_type, which provides the editing capability, and
// on commit, it tries to convert the editable string to an int and
// to set the referenced value with it.

struct int_entity_type
: public string_entity_type
{
  int & value ;
  const char* format ;

  // the constructor takes a reference to the integer value to be handled,
  // and the position and size of the field where the user can interact
  // with this value.

  int_entity_type ( int & _value ,
                    const char* _format ,
                    sf::FloatRect _bounds  ,
                    const sf::String & _dsc = "integer entity, no description")
  : value ( _value ) ,
    format ( _format ) ,
    string_entity_type ( sf::String ( std::to_string ( _value ) ) ,
                         _bounds , _dsc )
  { } ;

  virtual void show()
  {
    if ( this != gui.focus )
    {
      char buffer[256] ;
      std::snprintf ( buffer , 256 , format , value ) ;
      string_entity_type::value.set_string ( sf::String ( buffer ) ) ;
    }
    string_entity_type::show() ;
  }

  virtual void commit()
  {
    try
    {
      value = std::stoi ( std::string ( string_entity_type::value ) ) ;
      gui.need_display = true ;
    }
    catch ( ... )
    {
      // reset editable string to 'value'
      string_entity_type::value = std::to_string ( value ) ;
    }
//     std::cout << "iet commits " << value << std::endl ;
  }
} ;

struct float_entity_type
: public string_entity_type
{
  double & value ;
  const char* format ;

  // the constructor takes a reference to the integer value to be handled,
  // and the position and size of the field where the user can interact
  // with this value.
  // TODO don't like trailing zeros, would like format string

  float_entity_type ( double & _value ,
                      const char* _format ,
                      sf::FloatRect _bounds ,
                      const sf::String & _dsc = "float entity, no description")
  : value ( _value ) ,
    format ( _format ) ,
    string_entity_type ( sf::String ( std::to_string ( _value ) ) ,
                         _bounds , _dsc )
  { } ;

  virtual void show()
  {
    if ( this != gui.focus )
    {
      char buffer[256] ;
      std::snprintf ( buffer , 256 , format , value ) ;
      string_entity_type::value.set_string ( sf::String ( buffer ) ) ;
    }
    string_entity_type::show() ;
  }

  virtual void commit()
  {
    try
    {
      value = std::stof ( std::string ( string_entity_type::value ) ) ;
      gui.need_display = true ;
    }
    catch ( ... )
    {
      // reset editable string to 'value'
      string_entity_type::value = std::to_string ( value ) ;
    }
//     std::cout << "fet commits " << value << std::endl ;
  }
} ;

struct xlat_float_entity_type
: public float_entity_type
{
  double & value ;
  double display ;
  std::function < double ( double ) > to_display ;
  std::function < double ( double ) > to_value ;

  // the constructor takes a reference to the integer value to be handled,
  // and the position and size of the field where the user can interact
  // with this value.
  // TODO don't like trailing zeros, would like format string

  xlat_float_entity_type ( double & _value ,
                      const char* _format ,
                      std::function < double ( double ) > _to_display ,
                      std::function < double ( double ) > _to_value ,
                      sf::FloatRect _bounds ,
                      const sf::String & _dsc = "xlat float entity, no description")
  : value ( _value ) ,
    to_display ( _to_display ) ,
    to_value ( _to_value ) ,
    float_entity_type ( display , _format , _bounds , _dsc )
  { } ;

  virtual void show()
  {
    if ( this != gui.focus )
    {
      display = to_display ( value ) ;
    }
    float_entity_type::show() ;
  }

  virtual void commit()
  {
    float_entity_type::commit() ;
    value = to_value ( display ) ;
  }
} ;

// placement_type is a helper class to arrange a set of rectangles in a grid.
// This is not a very elaborate class; for the use in pv we just hard-code
// the metrics for single fields and the gaps between them, but this is already
// enough to create a visually appealing and orderly arrangement.
// Each placement 'rules' ofer a grid of fields, and the next placement is
// created with a reference to a successor, which is either to the left or
// above the new placement. This way, all columns are in lime, and there are
// small structuring extra gaps between two placements.
// When calling placement's operator(), it simply returns the next rectangle
// in the grid, moving left-to-right, then down if a line was used up. It's
// also possible to ask for specific rectangles by giving their grid positions,
// which also affects 'current':the next call to operator() without arguments
// will yield the rectangle after the one just returned.

struct placement_type
{
  sf::Vector2u grid ;
  sf::Vector2f size ;
  sf::Vector2f position ;
  sf::Vector2f esize ;
  sf::Vector2f margin ;
  sf::Vector2f step ;
  sf::Vector2f gap ;

  sf::Vector2u current ;

  placement_type ( sf::Vector2u _grid ,
                   sf::Vector2f _position = sf::Vector2f() )
  : grid ( _grid ) ,
    position ( _position ) ,
    current ( { 0 , 0 } )
  {
    esize = { 100.0f * ui::fgui , 30.0f * ui::fgui } ;
    gap = { 10.0f * ui::fgui , 10.0f * ui::fgui } ;
    margin = gap ;
    step = esize + gap ;
    size.x = grid.x * esize.x + 2 * margin.x + ( grid.x - 1 ) * gap.x ;
    size.y = grid.y * esize.y + 2 * margin.y + ( grid.y - 1 ) * gap.y ;
  }

  sf::FloatRect operator() ( sf::Vector2u index ,
                             unsigned int field_width = 1 )
  {
    assert ( index.x < grid.x && index.y < grid.y ) ;

    sf::Vector2f offset ( index ) ;
    offset.x *= step.x ;
    offset.y *= step.y ;
    sf::Vector2f size ( esize ) ;
    size.x *= field_width ;
    size.x += gap.x * ( field_width - 1 ) ;

    current = index ;
    current.x += field_width ;
    if ( current.x >= grid.x )
    {
      ++current.y ;
      current.x = 0 ;
    }

    return { margin + offset + position , size  } ;
  }

  sf::FloatRect operator()( unsigned int field_width = 1 )
  {
    auto help = current ;
    current.x += field_width ;
    if ( current.x >= grid.x )
    {
      ++current.y ;
      current.x = 0 ;
    }
    return operator() ( help , field_width ) ;
  }

  sf::FloatRect operator() ( unsigned int x , unsigned int y )
  {
    return operator() ( { x , y } ) ;
  }

  sf::FloatRect get_extent()
  {
    return { position , size } ;
  }

  sf::Vector2f next_right ( float distance = 0.0 )
  {
    sf::Vector2f result = position ;
    result.x += ( size.x + distance ) ;
    return result ;
  }

  sf::Vector2f next_down ( float distance = 0.0 )
  {
    sf::Vector2f result = position ;
    result.y += ( size.y + distance ) ;
    return result ;
  }
} ;

// build_gui creates all GUI entities (now that we have their classes all defined)
// and pushes them to the entity vector in the gui object.

void build_gui()
{
  placement_type pl ( { 4 , 3 } ,
                      { 0.0f , 0.0f } ) ;

  gui.eq.push_back ( new button_entity_type
                          ( [&]() { ui::on_terminate() ; } ,
                            // was: [&]() { click = kill = true ; } ,
                            "EXIT" ,
                            pl() ,
                            "terminate program\nyou can also press the 'escape' key.\n" ) ) ;

  gui.eq.push_back ( new button_entity_type
                          ( [&]() { ui::on_load_images() ; } ,
                            "IMAGES" , pl() ,
                            "open more image files\nyou can also press the 'F' key" ) ) ;

  gui.eq.push_back ( new toggle3_entity_type
                          ( [&]() { if ( ui::slideshow_on )
                                    {
                                      if ( ui::grace_period < 0 )
                                      {
                                        ui::on_next_image() ;
                                      }
                                      else
                                      {
                                        ui::slideshow_on = false ;
                                      }
                                    }
                                    else
                                    {
                                      ui::slideshow_on = true ;
                                      ui::on_next_image() ;
                                    }
                                  } ,
                            "SLIDES" ,
                            [&]() { return ui::slideshow_on ; } ,
                            [&]() { return ui::grace_period < 0 ; } ,
                            pl() ,
                            "switch slideshow mode on/off\nthis will automatically load a new image\nafter some time, unless you interact\nwith the image: then the button turns yellow and you must\nmanually 'Tab' to the next image to return to slide show mode." ) ) ;

  gui.eq.push_back ( new float_entity_type
                          ( ui::slide_interval ,
                            "%.1f" ,
                            pl ( ) ,
                            "set slide show interval (in seconds)" ) ) ;

  // gui.eq.push_back ( new toggle_entity_type
  //                         ( [&]() { gui.automatic ^= true ; } ,
  //                           "HIDE GUI" ,
  //                           [&]() { return gui.automatic ; } ,
  //                           pl() ,
  //                           "switch permanent GUI visibility on/off\nwhen 'off', the GUI is hidden when moving the mouse down" ) ) ;

  gui.eq.push_back ( new button_entity_type
                          ( [&]() { ui::on_next_image() ; } ,
                            "NEXT IMG" , pl() ,
                            "show the next image in the image queue\nyou can also press 'TAB', and 'Shift+TAB'\nto go back to the previous image" ) ) ;

  gui.eq.push_back ( new toggle_entity_type
                          ( [&]() { ui::on_toggle_autopan() ; } ,
                            "AUTO PAN" ,
                            [&]() { return ui::autopan != 0 ; } ,
                            pl() ,
                            "switch automatic pan on/off\nyou can also do a single primary mouse click" ) ) ;

  gui.eq.push_back ( new button_entity_type
                          ( [&]() { ui::on_reverse_autopan() ; } ,
                            "REVERSE" , pl() ,
                            "reverse direction of automatic pan\nyou can also press the 'O' (capital letter O) key" ) ) ;

  gui.eq.push_back ( new button_entity_type
                          ( [&]() { ui::on_reset() ; } ,
                            "RESET" , pl() ,
                            "reset the view to the initial view\nor the view that was seen when\nF10 or 'SET REF' was last pressed\nyou can also press the 'Return' key" ) ) ;

//     gui.eq.push_back ( new button_entity_type
//                            ( [&]() { click = tab = true ; } ,
//                              "STEP >" ,
//                              pl() ,
//                              "move half a frame to the right\nyou can also press the 'Tab' key" ) ) ;

  // gui.eq.push_back ( new toggle_entity_type
  //                         ( [&]() { ui::on_toggle_fullscreen() ; } ,
  //                           "WINDOW" ,
  //                           [&]() { return ui::run_fullscreen ; } ,
  //                           pl() ,
  //                           "toggle between full screen and window\nyou can also press the 'F11' key" ) ) ;

  gui.eq.push_back ( new button_entity_type
                          ( [&]() { ui::on_previous_image() ; } ,
                            "PREV IMG" , pl() ,
                            "show the previous image in the image queue\nyou can also press 'Shift+TAB', or\n'TAB' to go to the next image" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_increase_autopan ,
                            "AUTOPAN+" ,
                            pl() ,
                            "accelerate automtaic pan\nyou can also use the 'A' key" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_decrease_autopan ,
                            "AUTOPAN-" ,
                            pl() ,
                            "slow down automatic pan\nyou can also use Shift+A key" ) ) ;

  gui.eq.push_back ( new toggle_entity_type
                          ( [&]() { ui::on_toggle_snap_to_hq() ; } ,
                            "IDLE HQ" ,
                            [&]() { return ui::snap_to_hq_interpolator ; } ,
                            pl() ,
                            "toggle 'snap to HQ'.\nThis uses the high quality interpolator\nif the view is at rest\nyou can also press the 'F12' key" ) ) ;

    pl = placement_type ( { 6 , 3 } ,
                        pl.next_right() ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_zoom_in ,
                            "ZOOM IN" ,
                            pl ( 0 , 0 ) ,
                            "zoom in\nyou can also use the '+' key,\nthe 'Z' key,\nor an upward secondary\n mouse button click-drag" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_zoom_out ,
                            "ZOOM OUT" ,
                            pl ( 0 , 1 ) ,
                            "zoom out\nyou can also use the '-' key,\nthe Shift+Z key\nor a downward secondary\n mouse button click-drag" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_camera_right ,
                            "CAM RIGHT" ,
                            pl ( 1 , 0 ) ,
                            "camera right\nyou can also use the 'right' key,\nor primary click-drag" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_camera_left ,
                            "CAM LEFT" ,
                            pl ( 1 , 1 ) ,
                            "camera left\nyou can also use the 'left' key,\nor primary click-drag" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_camera_up ,
                            "CAM UP" ,
                            pl ( 2 , 0 ) ,
                            "camera up\nyou can also use the 'up' key,\nor primary click-drag" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_camera_down ,
                            "CAM DOWN" ,
                            pl ( 2 , 1 ) ,
                            "camera down\nyou can also use the 'down' key,\nor primary click-drag" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_rotate_clockwise ,
                            "CAM CLK" ,
                            pl ( 3 , 0 ) ,
                            "rotate camera clockwise\nyou can also use the 'R' key" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_rotate_counter_clockwise ,
                            "CAM ACLK" ,
                            pl ( 3 , 1 ) ,
                            "rotate camera anticlockwise\nyou can also use the Shift+R key" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_raise_viewpoint ,
                            "HOR UP" ,
                            pl ( 4 , 0 ) ,
                            "raise horizon (if possible)\nthis is to view sections of images\nwhere the horizon is off the vertical center\nyou can also use the 'H' key" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_lower_viewpoint ,
                            "HOR DOWN" ,
                            pl ( 4 , 1 ) ,
                            "lower horizon (if possible)\nthis is to view sections of iamges\nwhere the horizon is off the vertical center\nyou can also use the Shift+H key" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_aq_down ,
                            "AQ DOWN" ,
                            pl ( 5 , 0 ) ,
                            "decrease animation quality to use\nless CPU time when\nexperiencing dropped frames\nyou can also use the 'M' key" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_aq_up ,
                            "AQ UP" ,
                            pl ( 5 , 1 ) ,
                            "increase animation quality\nmaximum is 100% for best quality, see value below\nyou can also use the Shift+M key" ) ) ;

  // user may enter values below zoom_min, but the sanity check will override
  // them and they won't have an effect beyond lowering the zoom to zoom-min.

  gui.eq.push_back ( new float_entity_type
                          ( ui::state.zoom_factor ,
                            "%.2fX" ,
                            pl ( 0 , 2 ) ,
                            "zoom factor, relative to input image\nclick left to edit" ) ) ;

  gui.eq.push_back ( new button_entity_type
                          ( [&]() { ui::on_page_down() ; } ,
                            "NADIR" , pl() ,
                            "change view to nadir (straight down)\nyou can also press the 'Page Down' key" ) ) ;

  gui.eq.push_back ( new button_entity_type
                          ( [&]() { ui::on_page_up() ; } ,
                            "ZENITH" , pl() ,
                            "change view to zenith (straight up)\nyou can also press the 'Page Up' key" ) ) ;

  gui.eq.push_back ( new button_entity_type
                          ( [&]() { ui::on_roll_90() ; } ,
                            "ROLL 90" , pl() ,
                            "roll view by 90 degrees\nyou can also press the '9' key" ) ) ;

  gui.eq.push_back ( new button_entity_type
                          ( [&]() { ui::on_set_bias() ; } ,
                            "SET REF" , pl() ,
                            "set the current view as reference position\npressing Return will now get you back to this spot\nthis sets the display horizon to the current center horizontal\nyou can also press the 'F10' key" ) ) ;

  gui.eq.push_back ( new xlat_float_entity_type
                          ( ui::state.moving_image_scaling ,
                            "%.1f%%" ,
                            [](double x) { return 100.0 * ( x * x ) ; } ,
                            [](double x) { return sqrt ( x / 100.0 ) ; } ,
                            pl ( { 5 , 2 } , 1 ) ,
                            "animation rendering quality in percent\nthis corresponds to moving_image_scaling:\nquality == 100 * scaling * scaling" ) ) ;

  pl = placement_type ( { 3 , 3 } ,
                        pl.next_right() ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_brightness_down ,
                            "DARKER" ,
                            pl() ,
                            "decrease brightness\nyou can also use the 'F5' key\nor secondary-button-click-drag to the left" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_black_up ,
                            "BLACK >" ,
                            pl() ,
                            "raise black point\nyou can also use F3" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_white_up ,
                            "WHITE >" ,
                            pl() ,
                            "raise white point\nyou can also use Shift+F4" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_brightness_up ,
                            "BRIGHTER" ,
                            pl() ,
                            "increase brightness\nyou can also use the 'F6' key\nor secondary-button-click-drag to the right" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_black_down ,
                            "BLACK <" ,
                            pl() ,
                            "lower black point\nyou can also use Shift+F3" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_white_down ,
                            "WHITE <" ,
                            pl() ,
                            "lower white point\nyou can also use Shift+F4" ) ) ;

  gui.eq.push_back ( new float_entity_type
                          ( ui::state.brightness ,
                            "%.2f" ,
                            pl() ,
                            "brightness\nclick left to edit" ) ) ;

  gui.eq.push_back ( new float_entity_type
                          ( ui::state.black_point ,
                            "%.2f" ,
                            pl() ,
                            "black point\nclick left to edit" ) ) ;

  gui.eq.push_back ( new float_entity_type
                          ( ui::state.white_point ,
                            "%.1f" ,
                            pl() ,
                            "white point\nclick left to edit" ) ) ;

  pl = placement_type ( { 4 , 3 } ,
                        pl.next_right() ) ;

  // TODO: may grey out WB option if -l is set

  // first row

  gui.eq.push_back ( new button_entity_type
                          ( [&]() { ui::on_capture_wb() ; } ,
                            "AVG -> WB" , pl() ,
                            "calculate white balance from average of current view\nyou can also press the 'W' key\nnote that this may go wrong when\nthe -l flag was set: correct WB\nneeds linear processing" ) ) ;

  gui.eq.push_back ( new toggle_entity_type
                          ( [&]() { ui::on_magnifying_glass() ; } ,
                            "MAGNIFY" ,
                            [&]() { return ui::sensor_settings.magnifying_glass != 1.0 ; } ,
                            pl() ,
                            "toggle 'magnigying glass' which\nmagnifies view by the value\npassed with the -I flag\n(default is 10X)\nyou can also press the 'I' (capital letter I) key" ) ) ;

//   gui.eq.push_back ( new buzzer_entity_type
//                           ( ui::do_sensor_shift_x_down ,
//                             "SENS HT-" ,
//                             pl () ,
//                             "'negative' horizontal sensor tilt\nyou can also press 'Shift + Left'" ) ) ;

  gui.eq.push_back ( new button_entity_type
                          ( [&]() { ui::on_solo_up() ; } ,
                            "SOLO UP" , pl() ,
                            "if not in solo mode, activate it.\nin solo mode, move to next image\nyou can also press 'Shift+Right'" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_sensor_shift_y_down ,
                            "SENS VT-" ,
                            pl () ,
                            "'negative' vertical sensor tilt\nyou can also press 'Shift + Down'" ) ) ;

  // second row

  gui.eq.push_back ( new button_entity_type
                          ( [&]() { ui::on_reset_wb() ; } ,
                            "RES WB" , pl() ,
                            "reset white balance to 1:1:1\nyou can also press 'Shift+W'" ) ) ;

  gui.eq.push_back ( new button_entity_type
                          ( [&]() { ui::on_100_percent() ; } ,
                            "1:1" , pl() ,
                            "set scale to one view pixel to one screen pixel\nyou can also press the '1' key\nor enter '1' in the zoom factor field" ) ) ;

//   gui.eq.push_back ( new buzzer_entity_type
//                           ( ui::do_sensor_shift_x_up ,
//                             "SENS HT+" ,
//                             pl () ,
//                             "'positive' horizontal sensor tilt\nyou can also press 'Shift + Right'" ) ) ;

  gui.eq.push_back ( new button_entity_type
                          ( [&]() { ui::on_solo_down() ; } ,
                            "SOLO DN" , pl() ,
                            "move to previous solo image\nused on the first solo image\nthis button ends solo mode\nyou can also press 'Shift+Left'" ) ) ;

  gui.eq.push_back ( new buzzer_entity_type
                          ( ui::do_sensor_shift_y_up ,
                            "SENS VT+" ,
                            pl () ,
                            "'positive' vertical sensor tilt\nyou can also press 'Shift + Up'" ) ) ;

  // third row

  gui.eq.push_back ( new button_entity_type
                          ( [&]() { ui::on_reset_light() ; } ,
                            "RES LIGHT" , pl() ,
                            "reset light parameters\nyou can also press the 'F7' key" ) ) ;

  gui.eq.push_back ( new button_entity_type
                          ( [&]() { ui::on_view_snapshot() ; } ,
                            "SNAPSHOT" , pl() ,
                            "make a snapshot of the current view\nyou can also press the 'E' key" ) ) ;

  auto rect = pl() ;
  rect.width = 210 * ui::fgui ;

  gui.eq.push_back ( new active_text_entity_type
                          ( []()
                            {
                              ui::on_show_again() ;
                              ui::extra_args.clear() ;
                            } ,
                            ui::extra_args ,
                            rect ,
                            "show image again with additional\ncommand line arguments\nclick left to edit, commit with Return\nthis will reuse the current interpolator\nunless you start the line with '! '" ) ) ;
}

enum
{
  NO_CHANGE ,
  BRIGHTNESS_UP ,
  BRIGHTNESS_DOWN ,
  BLACK_UP ,
  BLACK_DOWN ,
  WHITE_UP ,
  WHITE_DOWN
} ;

// initialize these time points to zero:

pv_clock::time_point last_mouse_activity ;
pv_clock::time_point primary_mouse_button_pressed ;
pv_clock::time_point secondary_mouse_button_pressed ;

// this holds the texture made from the last frame. The texture is
// held until the next frame is ready to be displayed, so it can be picked
// up as 'backdrop' during idle-time processing, if GUI elements need to be
// layered on top of the static idle-time image without having to recalculate
// the image. Just making a new sprite from the texture and drawing it on the
// window before the GUI stuff is cheap and GPU only.

sf::Texture image_texture ;
sf::Texture previous_texture ;

bool virgin = true ;

// store_rendered_image is a function to store snapshots,  either
// 'ordinary' snapshots or exposure fusions. p_job is a pointer to
// the job_type object which was used to render the frame. Next in
// line, we need either p_rgba or p_frgba pointing to image data.
// frame_is_linear indicates whether the data are in linear RGB
// or sRGB, and finally we have the filename to store to.
// TODO move to rendering code?

void store_rendered_image ( const job_type * p_job ,
                            view_type * p_frgb ,
                            view4_type * p_frgba ,
                            bool frame_is_linear ,
                            const std::string & filename )
{
  auto & args ( ini::state ) ;
  const frame_type & frame ( p_job->frame ) ;

  vigra::ImageExportInfo imageInfo ( filename.c_str() );

  int compression = ui::compression ;
  if ( compression <= 1 )
    compression = 1 ;
  else if ( compression >= 100 )
    compression = 100 ;

  // TODO: take extension from filename, rather

  if (     ui::snapshot_extension == std::string ( ".exr" )
        || ui::snapshot_extension == std::string ( ".EXR" ) )
  {
    // store in openEXR format. If the data aren't linear RGB,
    // they are converted to linear RGB first. If no alpha channel
    // is present, we add a fully opaque alpha channel.

    if ( p_frgba != nullptr )
    {
      // we have RGBA data.

      if ( ! frame_is_linear )
      {
        // data are in sRGB, convert to linear RGB

        dispatcher->srgba2rgba ( *p_frgba , p_job->njobs ) ;
      }

      // convert linear RGB to exr intensity values (incl. alpha)

      float factor = 1.0 / 255.0 ;

      for ( auto & px : *p_frgba )
        px *= factor ;

      // export to file

      vigra::exportImage ( *p_frgba , imageInfo ) ;
    }
    else
    {
      // we have RGB data and no alpha channel.

      assert ( p_frgb != nullptr ) ;

      vigra::MultiArray < 2 , pixel4_type > output ( p_frgb->shape() ) ;
      auto input = p_frgb->data() ;

      // have float RGB, want float RGBA. Note that if grey_edge
      // was in use, the grey edge can't be changed into an alpha
      // fade-out - the frame has to be rendered with grey_edge=no
      // or with alpha processing on. Still we accept the job.

      for ( auto & trg : output )
      {
        trg[0] = (*input)[0] ;
        trg[1] = (*input)[1] ;
        trg[2] = (*input)[2] ;
        trg[3] = 255.0 ;

        ++input ;
      }

      if ( ! frame_is_linear )
      {
        // data are in sRGB, convert to linear RGB

        dispatcher->srgba2rgba ( output , p_job->njobs ) ;
      }

      // convert linear RGB to exr intensity values (incl. alpha)

      float factor = 1.0 / 255.0 ;

      for ( auto & px : output )
        px *= factor ;

      // export to file

      vigra::exportImage ( output , imageInfo ) ;
    }
  }
  else if (    ui::snapshot_extension == std::string ( ".jpg" )
            || ui::snapshot_extension == std::string ( ".JPG" )
            || ui::snapshot_extension == std::string ( ".png" )
            || ui::snapshot_extension == std::string ( ".PNG" ) )
  {
    // for JPG and PNG output, we'll convert linear data to sRGB.
    // if an alpha channel is present, it is applied to the RGB
    // data; JPG and PNG don't use alpha channel data.

    if ( p_frgb != nullptr )
    {
      if ( frame_is_linear )
      {
        // data are in linear RGB, convert to sRGB

        dispatcher->rgb2srgb ( *p_frgb , p_job->njobs ) ;
      }

      vigra::exportImage
        ( *p_frgb ,
          imageInfo
          .setPixelType ( "UINT8" )
          .setCompression ( std::to_string(compression).c_str()  )
          .setForcedRangeMapping ( 0 , 255 , 0 , 255 )
        ) ;
    }
    else
    {
      assert ( p_frgba != nullptr ) ;

      // frame holds RGBA data, apply alpha information to RGB data

      vigra::MultiArray < 2 , pixel_type > output ( p_frgba->shape() ) ;
      auto input = p_frgba->data() ;

      // have float RGBA, want float RGB:

      for ( auto & trg : output )
      {
        // convert alpha channel in [0..255] to factor in [0..1]

        auto factor = (*input)[3] / 255.0f ;

        // apply alpha channel

        trg[0] = (*input)[0] * factor ;
        trg[1] = (*input)[1] * factor ;
        trg[2] = (*input)[2] * factor ;

        ++input ;
      }

      if ( frame_is_linear )
      {
        // data are in linear RGB, convert to sRGB

        dispatcher->rgb2srgb ( output , p_job->njobs ) ;
      }

      vigra::exportImage
        ( output ,
          imageInfo
          .setPixelType ( "UINT8" )
          .setCompression ( std::to_string(compression).c_str()  )
          .setForcedRangeMapping ( 0 , 255 , 0 , 255 )
        ) ;
    }
  }
  else if (    ui::snapshot_extension == std::string ( ".tif" )
            || ui::snapshot_extension == std::string ( ".TIF" ) )
  {
    // if the incoming data are linear RGB, they will be stored
    // as such. TODO: process snapshot_tiff_linear, allow 8bit etc.
    // for now, all TIFF output is in 16bit

    if ( p_frgba != nullptr )
    {
      // export with alpha channel
      // convert between RGB/sRGB as needed, leave channel4 (alpha) unchanged

      if ( frame_is_linear && ( ! args.snapshot_tiff_linear ) )
      {
        // convert to sRGB

        dispatcher->rgba2srgba ( *p_frgba , p_job->njobs ) ;
      }
      else if ( ( ! frame_is_linear ) && args.snapshot_tiff_linear )
      {
        // convert to linear RGB

        dispatcher->srgba2rgba ( *p_frgba , p_job->njobs ) ;
      }
      vigra::exportImage
        ( *p_frgba ,
          imageInfo
          .setPixelType("UINT16")
          .setForcedRangeMapping ( 0 , 255 , 0 , 65535 )
          .setCompression("DEFLATE")
          .setCompression ( std::to_string(compression).c_str()  )
        ) ;
    }
    else
    {
      assert ( p_frgb != nullptr ) ;

      // export without alpha channel
      // convert between RGB/sRGB as needed

      if ( frame_is_linear && ( ! args.snapshot_tiff_linear ) )
      {
        // convert to sRGB

        dispatcher->rgb2srgb ( *p_frgb , p_job->njobs ) ;
      }
      else if ( ( ! frame_is_linear ) && args.snapshot_tiff_linear )
      {
        // convert to linear RGB

        dispatcher->srgb2rgb ( *p_frgb , p_job->njobs ) ;
      }
      vigra::exportImage
        ( *p_frgb ,
          imageInfo
          .setPixelType("UINT16")
          .setForcedRangeMapping ( 0 , 255 , 0 , 65535 )
          .setCompression("DEFLATE")
          .setCompression ( std::to_string(compression).c_str()  )
        ) ;
    }
  }
  else
  {
    // format not supported

    std::cerr << "can't store to format "
              << ui::snapshot_extension << std::endl ;
  }

  // TODO: serialize job to format like JSON or .ini, store alongside
  // to make snapshot reproducable
  
  std::string prj_name_string ;
  std::string prj_for_ini ;

  std::string hfov_string
    = std::to_string
      ( p_job->target.hfov * 180.0 / M_PI ) ;

  double vfov = calculate_vfov ( p_job->target.hfov ,
                                p_job->target.width ,
                                p_job->target.height ,
                                p_job->target.projection ) ;

  switch ( p_job->target.projection )
  {
    case RECTILINEAR:
      prj_name_string = "Rectilinear (0)" ; 
      prj_for_ini = "rectilinear" ;
      break ;
    case CYLINDRIC:
      prj_name_string = "Cylindrical (1)" ;
      prj_for_ini = "cylindric" ;
      break ;
    case SPHERICAL:
      prj_name_string = "Equirectangular (2)" ;
      prj_for_ini = "spherical" ;
      break ;
    case STEREOGRAPHIC:
      prj_name_string = "Stereographic (10)" ;
      prj_for_ini = "stereographic" ;
      break ;
    case FISHEYE:
      prj_name_string = "Fisheye (3)" ;
      prj_for_ini = "fisheye" ;
      break ;
    default:
      prj_name_string = "Mosaic (0)" ;
      prj_for_ini = "mosaic" ;
      break ;
  }

  std::string vfov_string
    = std::to_string
      ( vfov * 180.0 / M_PI ) ;


  if (     ui::snapshot_extension == std::string ( ".exr" )
        || ui::snapshot_extension == std::string ( ".EXR" ) )
  {
    // for openEXR files, which don't use EXIV2 metadata, we
    // add an ini file as a 'sidecar', which opens the file with
    // the correct projection and FOV.

    std::string sidecar_name = filename + std::string ( ".lux" ) ;
    std::ofstream sc ( sidecar_name ) ;

    sc << "# ini file for lux, for image " << filename << std::endl ;
    sc << "image=" << filename << std::endl ;
    sc << "projection=" << prj_for_ini << std::endl ;
    sc << "hfov=" << hfov_string << std::endl ;
    sc << "vfov=" << vfov_string << std::endl ;

    // if we have active cropping, we add the relevant information
    // to the sidecar file. Note that, for now, these options are not
    // documented - they are a hidden feature used by lux internally.
    // users are free to use them, but if cropping_active is set to
    // 'true', *all* the other values must be set correctly.

    const target_type & target ( p_job->target ) ;
    if ( target.cropping_active )
    {
      sc << "cropping_active=yes" << std::endl ;
      sc << "uncropped_hfov=" << target.hfov * ( 180.0 / M_PI ) << std::endl ;
      sc << "uncropped_vfov=" << target.vfov * ( 180.0 / M_PI ) << std::endl ;
      sc << "uncropped_width=" << target.width << std::endl ;
      sc << "uncropped_height=" << target.height << std::endl ;
      sc << "crop_x0=" << target.crop_x << std::endl ;
      sc << "crop_y0=" << target.crop_y << std::endl ;
      sc << "crop_x1=" << target.crop_x + target.crop_width << std::endl ;
      sc << "crop_y1=" << target.crop_y + target.crop_height << std::endl ;
    }
    else
    {
      sc << "cropping_active=no" << std::endl ;
    }
    std::cout << "stored sidecar lux ini file "
              << sidecar_name << std::endl ;
  }
  else
  {
    try
    {
      // we add a 'UserComment' EXIF tag using the same format
      // hugin uses to encode projection and FOV.
      // This is enough to allow pv to open a snapshot 'correctly',
      // but of course it'll only work for image formats which
      // support EXIF tags, and other software will not be able to
      // make sense of the data. I even failed to get hugin to read
      // such snapshots correctly unless they were in spherical
      // projection.

      auto image = Exiv2::ImageFactory::open(filename);
      assert (image.get() != 0);
      image->readMetadata();
      Exiv2::ExifData &exifData = image->exifData();

      // TODO: add Ev

      std::string user_comment =
        "charset=Unicode .Projection: "
        + prj_name_string
        + "\nFOV: "
        + hfov_string
        + " x "
        + vfov_string ;

      exifData["Exif.Photo.UserComment"] = user_comment ;

      std::cout << "Writing user comment EXIF tag: "
                << exifData [ "Exif.Photo.UserComment" ]
                << std::endl ;

      {
        const target_type & target ( p_job->target ) ;

        Exiv2::XmpData xmpData ;
        
        xmpData["Xmp.dc.lux_version"] = std::string ( pv_version ) ;
        xmpData["Xmp.dc.cropping_active"] = target.cropping_active ;
        xmpData["Xmp.dc.uncropped_hfov"] = target.hfov * ( 180.0 / M_PI ) ;
        xmpData["Xmp.dc.uncropped_vfov"] = target.vfov * ( 180.0 / M_PI ) ;
        xmpData["Xmp.dc.projection"]
          = projection_name [ target.projection ]  ;
        xmpData["Xmp.dc.uncropped_width"] = target.width ;
        xmpData["Xmp.dc.uncropped_height"] = target.height ;
        if ( target.cropping_active )
        {
          xmpData["Xmp.dc.cropped_width"] = target.crop_width ;
          xmpData["Xmp.dc.cropped_height"] = target.crop_height ;
          xmpData["Xmp.dc.crop_x0"] = target.crop_x ;
          xmpData["Xmp.dc.crop_y0"] = target.crop_y ;
          xmpData["Xmp.dc.crop_x1"]
            = target.crop_x + target.crop_width ;
          xmpData["Xmp.dc.crop_y1"]
            = target.crop_y + target.crop_height ;
        }
        
        image->setXmpData(xmpData);
      }
 
      image->writeMetadata();
    }
    // catch ( Exiv2::AnyError& e )
    // {
      // std::cerr << "Caught Exiv2 exception '"
                // << e << "'\n" ;
    // }
    catch ( ... )
    {
      std::cerr << "unknown exception" << std::endl ;
    }
  }
}

void cleanup ( job_type * p_job )
{
  // p_frame should point to an 8bit rgba image, which is recycled

  auto & frame ( p_job->frame ) ;
  if ( frame.p_frame )
  {
    std::lock_guard<std::mutex> lk ( rgb_mutex ) ;
    rgb_queue.push ( frame.p_frame ) ;
  }

  // the other two pointers should not be set at all, they are set
  // for a specific purpose and reset to nullptr directly after the
  // special-purpose job was run.

  if ( frame.p_frgb )
  {
    std::cerr << "****** error: p_frgb set in cleanup!" << std::endl ;
  }

  if ( frame.p_frgba )
  {
    std::cerr << "****** error: p_frgba set in cleanup!" << std::endl ;
  }

  memlog >> p_job ;
}

sf::Clock deltaClock ;

// this class takes care of jobs when they come back from rendering.
// It makes sure the data are in sequence, discards out-of-sequence
// jobs, which have been 'overtaken' - like slow background jobs,
// which have become obsolte because there was user interaction
// after they were launched - and disposes of such jobs and frames.
// In-sequence jobs pass back to the caller, other situations return
// nullptr.
// The idea is to put more functionality into this class over time,
// to make the main loop more compact.

struct presenter_type
{
  bool have_content ;
  pv_clock::time_point event_horizon ;
  // int epoch , _epoch ;
  // int frame_count ;

  presenter_type()
  : event_horizon ( pv_clock::now() ) ,
    have_content ( false )
  {
     // epoch = _epoch = std::chrono::duration_cast<std::chrono::seconds>
     //   ( event_horizon.time_since_epoch()).count() ;
     // frame_count = 0 ;
  }

  // poll first tries to get a job from the frame queue. If that
  // succeeds, it makes sure it's in-sequence and discards the
  // job if its out-of-sequence. In-sequence jobs are passed
  // back, all other situations result in nullptr being returned.

  job_type * poll()
  {
    job_type * p_job = nullptr ;

    {
      std::lock_guard<std::mutex> lk ( frame_mutex ) ;
      if ( ! frame_queue.empty() )
      {
        p_job = frame_queue.front() ;
        frame_queue.pop() ;
      }
    }

    if ( p_job == nullptr )
    {
      // if the frame queue is empty, we wait on content_cv for
      // up to ten milliseconds. If the cv is notified during
      // this period, we obtain the frame, otherwise we continue,
      // returning p_job == nullptr, to signal the polling code
      // that no content is to be had.

      std::unique_lock < std::mutex > lk ( content_mutex ) ;

      auto result = content_cv.wait_for
        ( lk , std::chrono::milliseconds ( 10 )  ) ;

      if ( result == std::cv_status::no_timeout )
      {
        std::lock_guard<std::mutex> lk ( frame_mutex ) ;
        if ( ! frame_queue.empty() )
        {
          p_job = frame_queue.front() ;
          frame_queue.pop() ;
        }
      }
    }

    if ( p_job != nullptr )
    {
      // std::cout << "**** poll acquired a frame" << std::endl ;

      // epoch = std::chrono::duration_cast<std::chrono::seconds>
      //   ( event_horizon.time_since_epoch()).count() ;
      // 
      // if ( epoch == _epoch )
      // {
      //   ++frame_count ;
      // }
      // else
      // {
      //   std::cout << "***** in epoch " << _epoch << ": "
      //             << frame_count << " incoming frames" << std::endl ;
      //   frame_count = 1 ;
      //   _epoch = epoch ;
      // }
        
      have_content = true ;

      if ( p_job->created < event_horizon )
      {
        // discard out-of-sequence frame

        // std::cout << "**** discard out-of-sequence frame" << std::endl ;
        cleanup ( p_job ) ;
        p_job = nullptr ;
      }
      else
      {
        // in-sequence frame. update event_horizon

        event_horizon = p_job->created ;
        gui.need_display = true ;

        // if ui::suppress_display is set, we'll not update the image
        // texture - it will remain the same, and when lux has just
        // started up it holds the splash screen, which is okay to show.

        if ( ! ui::suppress_display )
        {
          int frame_width = p_job->frame.p_frame->shape(0) ;
          int frame_height = p_job->frame.p_frame->shape(1) ;

          // now image_texture is (re)created from the data in
          // the frame. The resulting texture will remain available
          // until the next frame comes in, so that it can be reused
          // if necessary, like when GUI elements have to be drawn
          // on top of a static image in idle mode.

          // a little gimmick: if the incoming frame is single-mode,
          // meaning it's an HQ frame, possibly even a rendition done
          // with B&A image splining, we fade it in rather than simply
          // replacing the current view. This looks nicer, especially
          // for exposure fusions where the transition can be quite
          // rough otherwise. We do this only if we're in SINGLE mode
          // already, fading the currently displayed still image over
          // to the HQ rendition.

          if (    ui::crossfade
               && ui::mode == SINGLE
               && p_job->frame.mode == SINGLE )
          {
            previous_texture = image_texture ;
            ui::fade_factor = 0.2 ;
          }

          if ( ! image_texture.create ( frame_width , frame_height ) )
          {
            abort_lux ( "create texture failed" ) ;
          }

          image_texture.setSmooth ( true ) ;
          image_texture.setSrgb ( false ) ;

          // transfer the frame's data into the texture

          image_texture.update
            ( (unsigned char*) ( p_job->frame.p_frame->data() ) ) ;
          // std::cout << "**** image_texture was updated" << std::endl ;
        }
      }
    }

    return p_job ;
  }

  void show ( sf::RenderWindow * p_window )
  {
    if ( p_window == nullptr )
      return ;

    p_window->setActive ( true ) ;

    auto sz = p_window->getSize() ;

    // create a view to the size of the image texture. This magnifies
    // or shrinks the display of the sprite to fit the size of the
    // window. All textures/sprites we create are meant to fill the
    // entire window, but due to global scaling or supersampling for
    // single mode frames the textures we have to display can vary in
    // size, so we need to take that into account.

    auto texture_size = image_texture.getSize() ;

    // we rely on the image texture having been created when the
    // screen_type object was set up at program startup.

    assert ( texture_size.x && texture_size.y ) ;

    // We set the view so that the texture's width will be
    // scaled to the window's width. At the same time, we
    // want the image to be scaled isotropically, so we have
    // to calculate the view's height accordingly:

    // factor to scale from texture units to window units:
    double scaling_factor = double ( texture_size.x ) / sz.x ;
    // that gives us the view's height
    double view_height = sz.y * scaling_factor ; ;
    // but we want the view centered vertically, hence
    double excess = ( view_height - texture_size.y ) / 2.0 ;

    sf::View view ( sf::FloatRect ( 0.0f ,
                                    -excess ,
                                    texture_size.x ,
                                    view_height ) ) ;

    // The original code stretched the window to fill the
    // window, at times resulting in anisotropic scaling.
    // This avoids black bars showing at the upper and lower
    // margin when the window is scaled, but I find the
    // anisotropic scaling more annoying.

    // sf::View view ( sf::FloatRect ( 0.0f ,
    //                                 0.0f ,
    //                                 texture_size.x ,
    //                                 texture_size.y ) ) ;

    p_window->setView ( view ) ;

    // pre-clearing only makes sense if screen data come with
    // non-opaque alpha values or -o is set - doesn't take much
    // time, though. I keep it optional for now. Pre-clearing
    // should not be done while the initial banner is displayed,
    // hence the test for have_content, which is only set by
    // poll() when the first frame comes in from the rendering
    // thread.

    if ( have_content )
      p_window->clear ( sf::Color::Black ) ;

    // apply the image texture to a sprite

    sf::Sprite sprite ( image_texture ) ;

    // now, with the view in place, we draw the sprite to the window

    p_window->draw ( sprite ) ;

    texture_size = previous_texture.getSize() ;

    if ( ui::fade_factor < 1.0 && texture_size.x && texture_size.y )
    {
      double scaling_factor = double ( texture_size.x ) / sz.x ;
      double view_height = sz.y * scaling_factor ; ;
      double excess = ( view_height - texture_size.y ) / 2.0 ;

      sf::View view ( sf::FloatRect ( 0.0f ,
                                      -excess ,
                                      texture_size.x ,
                                      view_height ) ) ;

      p_window->setView ( view ) ;
      sf::Sprite sprite ( previous_texture ) ;
      int opacity = int ( ( 1.0 - ui::fade_factor ) * 255.0 ) ;
      opacity = std::min ( 255 , opacity ) ;
      opacity = std::max ( 0 , opacity ) ;
      sprite.setColor ( sf::Color ( 255 , 255 , 255 , opacity ) ) ;
      p_window->draw ( sprite ) ;
    }

    // optionally draw GUI elements on top of the image and clear the
    // gui.on flag: if the GUI code does not set it to true again because
    // there was GUI activity resulting in a change of the GUI's graphical
    // representation, we don't want to draw it again.

    texture_size = gui.status_texture.getSize() ;

    if ( texture_size.x && texture_size.y && ui::status_line.size() )
    {
      sf::View view ( sf::FloatRect ( 0 , 0 , sz.x , sz.y ) ) ;
      p_window->setView ( view ) ;
      
      sf::Sprite sprite ( gui.status_texture.getTexture() ) ;
      sprite.setPosition ( 0.0f , sz.y - 24.0f * ui::fgui ) ;
      p_window->draw ( sprite ) ;
    }
    
    if ( gui.on )
    {
      gui.draw ( p_window ) ;
    }

#ifdef USE_IMGUI

          ImGui::SFML::Update(*p_window, deltaClock.restart());
          ui::ShowImGui() ;
          ImGui::SFML::Render(*p_window);

#endif // #ifdef USE_IMGUI

    p_window->display() ;
  }
} ;

presenter_type presenter ;

void signal_light_balance ( bool on )
{
  status_light_balancing = on ;
  status_flag = true ;

  gui.draw_status_line ( ui::p_screen->p_window ) ;

  if ( gui.need_display )
  {
    presenter.show ( ui::p_screen->p_window ) ;
  }
}

// finally we arrive at inner_main(). Here we have the processing
// of argumemts, setting up of interpolators, and the event loop.
// Initially this was main(), it's now been 'demoted' to a mere
// function because I added multiple image processing.

int inner_main()
{
  // if the set of OIIO arguments has changed since the last cycle,
  // destroy the current interpolator (to make sure it's not
  // recycled). Recycling it might be possible, but to figure out
  // if that's an option, we'd have to look into the argument set,
  // which is beyond the scope of lux. The chance to get the same
  // hash with different args is so small that we don't care - then,
  // an itp recycle might happen and the user would experience no
  // effect of the oiio parameter change.
  // TODO: there are some other changes in parameterization which
  // aren't detected by the itp recycling code and should also
  // result in a call to destroy_current_itp.

  if ( ini::args.oiio_arg.size() )
  {
    std::size_t h = 0 ;
    for ( int i = 0 ; i < ini::args.oiio_arg.size() ; i++ )
    {
      h |= std::hash < std::string >{} ( ini::args.oiio_arg[i] ) ;
    }
    if ( h != ui::oiio_arg_hash )
    {
      ui::oiio_arg_hash = h ;
      ui::destroy_current_itp() ;
    }
  }

  // data hygiene: this should not be necessary, but the job_type
  // object is very large, and it's safer to clear it at the start
  // of every cycle lest remnants of the previous cycle persist.

  ui::job = job_type() ;

  std::cout << "inner_main called: starting display cycle" << std::endl ;

#ifdef USE_IMGUI
  ui::legacy_gui = ini::args.legacy_gui ;
#else
  ui::legacy_gui = true ;
#endif

  // signal to ImGui code in lux_imgui.cc that a new cycle has begun.
  // This results in an update of some state in the ImGui-specific code.

  ui::imgui_reload_geometry = true ;
  ui::imgui_reload_conditioning = true ;
  ui::imgui_reload_export = true ;
  ui::imgui_reload_behaviour = true ;
  ui::imgui_reload_view_control = true ;

  ui::reverse_drag = ini::args.reverse_drag ;
  ui::reverse_secondary_drag = ini::args.reverse_secondary_drag ;
  ui::show_status_line = ini::args.show_status_line ;
  ui::show_error_dialog = ini::args.show_error_dialog ;
  ui::snap_to_hq = ini::args.snap_to_hq ;
  ui::snap_to_fusion = ini::args.snap_to_fusion ;
  ui::snap_to_stitch = ini::args.snap_to_stitch ;

  // We start out setting a few variables local to main

  // we won't push more jobs if the frame queue holds more frames
  // than the threshold value. Lowering this value decreases
  // latency, but makes dropped frames more likely. The default
  // in options.h is heuristic and attempts a compromise between
  // memory consumption, latency, and frame losses.

  const int frame_threshold = ini::args.frame_queue_limit ;

  // or if the job queue holds two or more jobs
  // heuristic. 1 degrades performance, more than two
  // increases latency without benefit.

  const int job_threshold = ini::args.job_queue_limit ;

  // This variable holds the maximum b-spline degree.
  // We need to take into account that we're working in single
  // precision float, and very high spline degrees require higher
  // precision, so this level is 'technical', and using very high
  // degrees is bound to fail

  const int max_degree = 45 ;

  // now we extract the initial values in 'args' to variables in main
  // and namespace ui, creating the initial state of the viewer before
  // starting it's main loop

  // a shorthand: we'll use this often during program initialization

  auto & args ( ini::state ) ;
  std::cout << "args.blending initially " << args.blending << std::endl ;

  int stop_after = args.stop_after ;

  // now initialize variables which are in namespace ui

  ui::snappiness = args.snappiness ;
  ui::autopan = args.autopan ;
  if ( ui::autopan )
    ui::autopan_ramp = 0.0 ;
  ui::allow_pan_mode = args.allow_pan_mode ;
  ui::auto_quality = args.auto_quality ;
  ui::snap_to_hq_interpolator = args.snap_to_hq ;

  if ( args.stack == "first" )
    ui::stacking = STACK_FIRST ;
  else if ( args.stack == "hdr" )
    ui::stacking = STACK_HDR ;
  if ( args.stack == "fusion" )
    ui::stacking = STACK_FUSION ;

  ui::crossfade = args.crossfade ;
  ui::crossfade_delta = args.crossfade_delta ;

  // set view's hfov

  ui::hfov = args.hfov_view ;

  // set frame rendering time budget (works when auto_quality is on)

  double budget = 15.0 ;   // initial time budget in ms to create a frame
  bool auto_budget = true ;

  if ( args.budget )
  {
    budget = args.budget ;
    auto_budget = false ;
  }

  ui::suppress_display = args.suppress_display ;
  ui::process_linear = args.process_linear ;
  ui::still_image_scaling = args.still_image_scaling ;
  ui::sensor_settings.magnifying_glass_factor = args.magnifying_glass_factor ;

  ui::solo = args.solo ;

  ui::next_after_snapshot = args.next_after_snapshot ;
  ui::next_after_fusion = args.next_after_fusion ;
  ui::next_after_stitch = args.next_after_stitch ;
  ui::snapshot_magnification = args.snapshot_magnification ;
  ui::snapshot_prefix = args.snapshot_prefix ;
  ui::snapshot_extension = "." + args.snapshot_extension ;
  ui::snapshot = false ;
  ui::snapshot_threads = (   args.snapshot_threads <= 0
                           ? std::thread::hardware_concurrency()
                           : args.snapshot_threads ) ;
  ui::compression = args.snapshot_compression ;
  ui::output_magnification = args.output_magnification ;
  ui::faux_bracket = args.faux_bracket || args.compress ;

  if ( ui::faux_bracket )
  {
    // if the user has switched on faux bracketing mode
    // and we don't have a vector of Ev values, we try and
    // resort to a pair of size and step values to generate
    // a regularly spaced set of Ev values. If there are no
    // suitable arguments, the faux bracket won't happen but
    // since there are defaults for size and step (3 and 2.0)
    // this will rarely occur.

    if ( args.faux_bracket_ev.size() == 0 )
    {
      if (    args.faux_bracket_size > 0
           && args.faux_bracket_step > 0.0 )
      {
        auto fb_range =   ( args.faux_bracket_size - 1 )
                        * args.faux_bracket_step ;
        auto fb_ev = - ( fb_range / 2.0 ) ;
        for ( int i = 0 ; i < args.faux_bracket_size ; i++ )
        {
          args.faux_bracket_ev.push_back ( fb_ev ) ;
          fb_ev += args.faux_bracket_step ;
        }
      }
    }
  }

  ui::capture_light = false ;

  // if this is not a show-again sequence, we take over the
  // value of 'light_balance' from the command line. In a
  // show-again sequence we don't do that because we want the
  // value in namespace ui to persist until the next image.
  // Also, in a show-again sequence, we don't want to set
  // ui::capture_light again - either this was done when the
  // image came on, so it's done already, or it was done later
  // by user interaction - or it wasn't done at all.

  if ( ! ui::show_again )
  {
    ui::light_balance = args.light_balance ;
    if ( ui::light_balance == "auto" )
      ui::capture_light = true ;

    ui::subimage = 0 ;
    ui::subimages = 1 ;
  }

  if (    ui::snapshot_extension == std::string ( ".exr" )
       || ui::snapshot_extension == std::string ( ".EXR" ) )
  {
    std::cout << "exr snapshots requested, forcing alpha processing" << std::endl ;
    args.alpha = "yes" ;
  }

  if (    ui::snapshot_extension == std::string ( ".tif" )
       || ui::snapshot_extension == std::string ( ".TIF" ) )
  {
    if ( args.snapshot_tiff_linear && ( ui::process_linear == false ) )
    {
      std::cout << "linear TIFF output, forcing linear processing"
                << std::endl ;
      ui::process_linear = true ;
    }
  }

  // set spline degree of the fast and quality interpolators

  if ( args.fast_interpolator_degree < 0 )
    args.fast_interpolator_degree = 0 ;
  if ( args.fast_interpolator_degree > max_degree )
    args.fast_interpolator_degree = max_degree ;
  ui::fast_interpolator_degree = args.fast_interpolator_degree ;

  if ( args.quality_interpolator_degree < 0 )
    args.quality_interpolator_degree = 0 ;
  if ( args.quality_interpolator_degree > max_degree )
    args.quality_interpolator_degree = max_degree ;
  ui::quality_interpolator_degree = args.quality_interpolator_degree ;

  if ( args.pyramid_smoothing_level > max_degree )
    args.pyramid_smoothing_level = max_degree ;

  if ( args.squash < 0 )
  {
    std::cerr << "squash must not be negative, setting it to 0"
              << std::endl ;
    args.squash = 0 ;
  }

  ui::decimate_area = args.decimate_area ;
  ui::tonemap = args.tonemap ;

  ui::save_autopan = ( ui::autopan == 0.0 ? 0.05 : 0.0 ) ;
  ui::initial_pitch = args.initial_pitch ;
  ui::initial_yaw = - args.initial_yaw ;
  ui::initial_roll = args.initial_roll ;

  ui::initial_dx = args.initial_dx ;
  ui::initial_dy = args.initial_dy ;
  ui::initial_dr = args.initial_dr ;

  ui::grey_edge = args.grey_edge ;
  ui::pyramid_scaling_step = args.pyramid_scaling_step ;
  ui::pyramid_smoothing_level = args.pyramid_smoothing_level ;

//   ui::bias = get_rotation_q ( 0.0 , 0.0 , 0.0 ) ;

  if ( virgin )
  {
    // take over slideshow args only when just started - otherwise
    // values in ui are persistent

    ui::slide_interval = args.slide_interval ;
    ui::slideshow_on = args.slideshow_on ;

    // ditto for status line visibility

    ui::show_status_line = args.show_status_line ;
  }

  // we're using vigraimpex to import image files. The argument processing
  // code should have put the filename in args.image[0].

  auto filename = "" ;

  // now we're using OIIO, and OIIO args may influence the outcome
  // of analyzing the image.

  ui::split_oiio_args() ;

  if ( args.image.size() )
  {
    filename = args.image[0].c_str() ;

    if ( ! fileio::is_image ( filename ) )
    {
      std::cerr << filename << " is not recognized as an image file" << std::endl ;
      // return from inner_main, try next image if there is one.
      return -1 ;
    }

    if ( args.path.empty() )
    {
      // args.path was not set, set it to the image's path, if it has one
      auto slashpos = args.image[0].rfind ( '/' ) ;
      if ( slashpos == std::string::npos )
        slashpos = args.image[0].rfind ( '\\' ) ;
      if ( slashpos != std::string::npos )
      {
        args.path = args.image[0].substr ( 0 , slashpos + 1 ) ;
      }
    }
  }
  else
  {
    // TODO: does this ever happen?
  
    filename = ui::used_files.back().c_str() ;
  }

  ui::metadata_query_v = args.metadata_query ;
  ui::metadata_format = args.metadata_format ;

  gui.init() ;

  if ( args.window_width == 0 )
    args.window_width = desktop.width * .8 ;
  if ( args.window_height == 0 )
    args.window_height = desktop.height * .8 ;

  if ( ui::p_screen == nullptr )
  {
    // p_screen is 0 only at program startup and signals that wee need to
    // set up a new screen_type object. At this point, there is no image
    // to display, but rather than displaying an empty screen, we show a
    // lux banner (by placing it into the newly-created image_texture)
    // which will replaced by the first frame coming from the rendering
    // thread.

    ui::run_fullscreen = args.fullscreen ;

    // if process_linear is set, we pass true for the srgb_capable
    // argument of screen_type. If the system can indeed create an
    // srgb-capable frame buffer, we can avoid converting image
    // data to sRGB 'manually' in the rendering code and rely on
    // the GPU to handle incoming linear data which is a great
    // time-saver. If the system can't provide an srgb-capable
    // frame buffer, frame.light_settings.apply_gradation will be
    // set to true and the 'manual' conversion will be done.
    // if process_linear is false, we also pass false for the
    // srgb_capable argument of screen_type. This results in
    // the creation of a frame buffer which needs to be fed srgb
    // data, but since we're operating on srgb data anyway this
    // is correct, and apply_gradation will be set to false.

    ui::p_screen = mem_in() << new
      screen_type ( ui::run_fullscreen ,
                    filename ,
                    args.window_width ,
                    args.window_height ,
                    args.gpu_for_srgb && ui::process_linear ) ;

    auto sz = ui::p_screen->p_window->getSize() ;

    sf::RenderTexture banner ;
    banner.create ( sz.x , sz.y ) ;

    sf::Text text ( "lux" , gui.font ) ;
    text.setCharacterSize ( 900 ) ;
    text.setStyle ( sf::Text::Regular ) ;
    text.setFillColor ( sf::Color ( 128 , 128 , 128 , 255 ) ) ;
    sf::Vector2f pos ( 0 , -375 ) ;
    text.setPosition ( pos ) ;
    auto bounds = text.getGlobalBounds() ;

    sf::View view ( { 0.0f , 0.0f ,
                      bounds.width * 1.1f , bounds.height } ) ;
    view.setViewport ( { 0.15f , 0.1f , 0.75f , 0.9f } ) ;
    banner.setView ( view ) ;

    banner.clear ( sf::Color::Black ) ;
    banner.draw ( text ) ;
    banner.display() ;
    
    image_texture.create ( sz.x , sz.y ) ;
    image_texture = banner.getTexture() ;
  }
  else
  {
    ui::run_fullscreen = ui::p_screen->full_screen ;
    ui::p_screen->p_window->setTitle ( filename ) ;
  }

  target_type & target ( ui::target ) ;

  if ( args.target_projection == "rectilinear" )
    target.projection = RECTILINEAR ;
  else if ( args.target_projection == "spherical" )
    target.projection = SPHERICAL ;
  else if ( args.target_projection == "cylindric" )
    target.projection = CYLINDRIC ;
  else if ( args.target_projection == "fisheye" )
    target.projection = FISHEYE ;
  else if ( args.target_projection == "stereographic" )
    target.projection = STEREOGRAPHIC ;
  else if ( args.target_projection == "mosaic" )
    target.projection = MOSAIC ;

  // obtain a reference to the pointer to the current window.
  // This pointer is held in the 'screen_type' object ui::p_screen
  // points to, which is held by main() and persists untill
  // all invocations of inner_main are done.

  sf::RenderWindow * & p_window ( ui::p_screen->p_window ) ;
  assert ( p_window != nullptr ) ;

  // if ( ! virgin )
  //   presenter.show ( p_window ) ;

  auto window_size = p_window->getSize() ;

  if ( ui::run_fullscreen )
  {
    target.width = desktop.width ;
    target.height = desktop.height ;

    if (    desktop.width != window_size.x
         || desktop.height != window_size.y )
    {
      std::cout << "**** unexpected window size " << window_size.x
                << "X" << window_size.y << " in fullscreen mode"
                << std::endl ;
    }
  }
  else
  {
    target.width = window_size.x ;
    target.height = window_size.y ;
  }

  // The next step is to figure out what type of geometry is correct
  // for the image. We need to determine a projection and the field
  // of view - normally just the horizontal field of view, because
  // with a given projection and image size, we can infer the vertical
  // field of view. On top of projection and fov we can process 'offset'
  // information, which indicates where the image's top and left
  // margins are positioned in the 'panosphere'.

  // set the projection. We use a shorthand for ui::projection because
  // it's used so much.

  projection_type & projection ( ui::projection ) ;

  // 'mosaic' target projection implies 'mosaic' source projection

  if (    args.projection == "s"
       || args.projection == "spherical" )
    projection = SPHERICAL ;

  else if (    args.projection == "c"
            || args.projection == "cylindric" )
    projection = CYLINDRIC ;

  else if (    args.projection == "r"
            || args.projection == "rectilinear" )
    projection = RECTILINEAR ;
  
  else if (    args.projection == "m"
            || args.projection == "mosaic"
            || args.projection == "map" )
    projection = MOSAIC ;

  else if (    args.projection == "g"
            || args.projection == "stereographic" )
    projection = STEREOGRAPHIC ;
  
  else if (    args.projection == "f"
            || args.projection == "fisheye" )
    projection = FISHEYE ;

  else if ( args.projection == "cubemap" )
  {
    projection = CUBEMAP ;

    // cube maps should not use grey_edge, so we force it to false.

    ui::grey_edge = false ;

    // We require all six cube faces to be present:

    if ( args.cube_left == "" )
    {
      abort_lux ( "left cube face missing" ) ;
    }
    args.cube_left = ini::locate_file ( args.cube_left ) ;

    if ( args.cube_right == "" )
    {
      abort_lux ( "right cube face missing" ) ;
    }
    args.cube_right = ini::locate_file ( args.cube_right ) ;

    if ( args.cube_top == "" )
    {
      abort_lux ( "top cube face missing" ) ;
    }
    args.cube_top = ini::locate_file ( args.cube_top ) ;

    if ( args.cube_bottom == "" )
    {
      abort_lux ( "bottom cube face missing" ) ;
    }
    args.cube_bottom = ini::locate_file ( args.cube_bottom ) ;

    if ( args.cube_front == "" )
    {
      abort_lux ( "front cube face missing" ) ;
    }
    args.cube_front = ini::locate_file ( args.cube_front ) ;

    if ( args.cube_back == "" )
    {
      abort_lux ( "back cube face missing" ) ;
    }
    args.cube_back = ini::locate_file ( args.cube_back ) ;
  }
  else if ( args.projection == "facet_map" )
  {
    projection = FACET_MAP ;

    // facet maps should not use grey_edge, so we force it to false.

    ui::grey_edge = false ;

    int nfacets = args.facet.size() ;

    // we need at least one hfov TODO: max come from EXIF

    if ( args.facet_hfov.size() == 1 )
    {
      for ( int i = 1 ; i < nfacets ; i++ )
        args.facet_hfov.push_back ( args.facet_hfov[0] ) ;
    }
    assert ( args.facet_hfov.size() >= nfacets ) ;

    if ( args.facet_squash.size() == 0 && args.squash != 0 )
    {
      // if no facet_squash arguments were passed, but we have 'global'
      // squash, we 'roll out' the 'global' squash to all facets by
      // putting a single facet_squash value into the vector, which will
      // be rolled out to all facets:

      args.facet_squash.push_back ( args.squash ) ;
    }

    // for the other elements, we accept
    // - one entry, which is taken over for all others
    // - no entry, which sets everything to a no-effect default
    // - precisely nfacets entries

#define broadcast(property,plain) \
    if ( args.property.size() == 1 ) \
    { \
      for ( int i = 1 ; i < nfacets ; i++ ) \
        args.property.push_back ( args.property[0] ) ; \
    } \
    else if ( args.property.size() == 0 ) \
    { \
      for ( int i = 0 ; i < nfacets ; i++ ) \
        args.property.push_back ( plain ) ; \
    } \
    assert ( args.property.size() >= nfacets ) ;

    broadcast ( facet_projection , "rectilinear" )
    broadcast ( facet_brightness , 1.0 )
    broadcast ( facet_is_linear , args.is_linear )
    broadcast ( facet_squash , 0.0 )
    broadcast ( facet_roll , 0.0 )
    broadcast ( facet_pitch , 0.0 )
    broadcast ( facet_yaw , 0.0 )
    broadcast ( facet_lcs , 0.0 )
    broadcast ( facet_lch , 0.0 )
    broadcast ( facet_lcv , 0.0 )
    broadcast ( facet_lca , 0.0 )
    broadcast ( facet_lcb , 0.0 )
    broadcast ( facet_lcc , 0.0 )

    broadcast ( facet_vca , 1.0 )
    broadcast ( facet_vcb , 0.0 )
    broadcast ( facet_vcc , 0.0 )
    broadcast ( facet_vcd , 0.0 )
    broadcast ( facet_vcx , 0.0 )
    broadcast ( facet_vcy , 0.0 )

    broadcast ( facet_crop_active , false )
    broadcast ( facet_crop_elliptic , false )
    broadcast ( facet_crop_fade , 0.0 )
    broadcast ( facet_crop_x0 , 0.0 )
    broadcast ( facet_crop_x1 , 0.0 )
    broadcast ( facet_crop_y0 , 0.0 )
    broadcast ( facet_crop_y1 , 0.0 )

// special variant for the stack parent

#define autocast(property) \
    if ( args.property.size() == 1 ) \
    { \
      for ( int i = 1 ; i < nfacets ; i++ ) \
        args.property.push_back ( args.property[0] ) ; \
    } \
    else if ( args.property.size() == 0 ) \
    { \
      for ( int i = 0 ; i < nfacets ; i++ ) \
        args.property.push_back ( i ) ; \
    } \
    assert ( args.property.size() >= nfacets ) ;

    autocast ( facet_stack_parent ) ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      if ( args.facet_squash[i] < 0 )
      {
        std::cerr << "facet " << i << " has negative squash factor, " ;
        std::cerr << " setting it to 0" << std::endl ;
        args.facet_squash[i] = 0 ;
      }
    }

    if ( args.facet_priority == "none" )
    {
      for ( int i = 0 ; i < nfacets ; i++ )
        args.facet_handicap.push_back ( 0.0 ) ;
    }
    else if ( args.facet_priority == "explicit" )
    {
      broadcast ( facet_handicap , 0.0 )
    }
    else if ( args.facet_priority == "hfov" )
    {
      for ( int i = 0 ; i < nfacets ; i++ )
        args.facet_handicap.push_back
         ( 100.0 * args.facet_hfov[i] ) ;
    }
    else if ( args.facet_priority == "order" )
    {
      for ( int i = 0 ; i < nfacets ; i++ )
        args.facet_handicap.push_back ( 100.0 * i ) ;
    }

    // the orientation information will be stored as rotational quaternions
    // in ui::facet_orientation

    ui::facet_projection.resize ( nfacets ) ;
    ui::facet_orientation.resize ( nfacets ) ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      if ( target.projection == MOSAIC )
        ui::facet_projection[i] = MOSAIC ;
      else if ( args.facet_projection[i] == "rectilinear" )
        ui::facet_projection[i] = RECTILINEAR ;
      else if ( args.facet_projection[i] == "spherical" )
        ui::facet_projection[i] = SPHERICAL ;
      else if ( args.facet_projection[i] == "cylindric" )
        ui::facet_projection[i] = CYLINDRIC ;
      else if ( args.facet_projection[i] == "stereographic" )
        ui::facet_projection[i] = STEREOGRAPHIC ;
      else if ( args.facet_projection[i] == "fisheye" )
        ui::facet_projection[i] = FISHEYE ;
      else if ( args.facet_projection[i] == "mosaic" )
      {
        // facets in mosaic projection imply mosaic target projection, just
        // like for single-image views. If the first facet is in mosaic
        // projection, we also set target.projection to mosaic. If
        // target.projection is not mosaic and other facets than the first
        // come in with mosaic projection, this is an error.

        if ( target.projection != MOSAIC )
        {
          if ( i == 0 )
          {
            std::cout << "first facet is in mosaic projection" << std::endl ;
            std::cout << "setting target projection to mosaic" << std::endl ;
            target.projection = MOSAIC ;
          }
          else
          {
            abort_lux ( "cannot mix mosaic facets and non-mosaic target projection" ) ;
          }
        }

        ui::facet_projection[i] = MOSAIC ;
      }
      else
      {
        std::string error ( "unknown facet projection " ) ;
        error += args.facet_projection[i] ;
        error += " in facet " ;
        error += std::to_string ( i ) ;
        abort_lux ( error ) ;
      }

      // get the image file's path

      args.facet[i] = ini::locate_file ( args.facet[i] ) ;

      // read the euler angles

      double y = args.facet_yaw[i] ;
      double p = args.facet_pitch[i] ;
      double r = args.facet_roll[i] ;

      // to rotate the facets 'in place' we need the inverse rotation,
      // and we use 'yaw' the other way round

      auto rq = conj ( get_rotation_q ( -y , p , r ) ) ;

      // store

      ui::facet_orientation[i] = rq ;

      if ( target.projection == MOSAIC )
      {
        // these are the parameters of the geometrical transformation
        // which reflects the viewer's orientation - we store them in
        // a quaternion to use a uniform interface, but the quaternion's
        // content does not represent a rotational quaternion at all.

        ui::facet_orientation[i][0] = -y ;
        ui::facet_orientation[i][1] = p ;
        ui::facet_orientation[i][2] = r ;
        ui::facet_orientation[i][3] = 0.0 ;
      }
    }
  }
  else if (    args.projection == ""
            || args.projection == "auto" )
  {
    if ( ! ui::show_again )
      projection = PRJ_NONE ;
  }

  if ( target.projection == MOSAIC && projection != MOSAIC )
  {
    if ( projection == CUBEMAP )
    {
      std::string error ( "cannot display " ) ;
      error += projection_name [ projection ] ;
      error += " data with MOSAIC target projection" ;
      abort_lux ( error ) ;
    }

    if ( projection != CUBEMAP && projection != FACET_MAP )
    {
      std::cout << "input will be processed with MOSAIC projection"
                << std::endl ;
      projection = MOSAIC ;
    }
  }

  // set source image metrics. 'source' holds all information pertaining
  // to the source image, like it's projection, size, fovs and offsets.
  // we initialize it from command line arguments - which may not have been
  // passed, in which case we try and glean the values from metadata or
  // calculate sensible automatic values later on.

  auto & source ( ui::source ) ;

  auto & source_builder
          ( reinterpret_cast < scaffolding_type < source_type > & >
              ( source ) ) ;

  // set 'source' to a defined initial state

  source_builder.init() ;

  // copy hfov, vfov, hofs and vofs from the command line arguments

  source.hfov = args.hfov ;
  source.vfov = args.vfov ;
  source.hofs = args.horizontal_offset ;
  source.vofs = args.vertical_offset ;
  source.brightness = 1.0 ;

  std::atomic < bool > build_ready ( false ) ;
  bool build_success = false ;
  now_loading = "" ;
  status_building = 0 ;

  // we delegate the creation of the interpolator to a separate
  // thread, so we can remain active here in the main thread and
  // display information in the status line while the images are
  // loaded from disk. This should reassure the user that something
  // is indeed happening.

  auto payload = [&]()
  {
    build_success = build_interpolator ( filename , source ) ;
    build_ready = true ;
    std::lock_guard < std::mutex > lk ( status_mutex ) ;
    now_loading = "" ;
    status_flag = true ;
  } ;
  
  // now we launch the payload on a thread from the thread pool.

  dispatcher->launch ( payload ) ;

  while ( true )
  {
    // we run a rudimentary event loop while setting up.
    // The 'usual' closing mechanisms will be honoured, and also
    // pressing of Escape. In every case 'aborted' will be set,
    // which stops the rendering thread from reading and processing
    // more images.

    sf::Event event ;
    while ( p_window->pollEvent ( event ) )
    {

      #ifdef USE_IMGUI

      // ImGui::SFML needs to see all events, hence:

      ImGui::SFML::ProcessEvent(*p_window, event);

      #endif // #ifdef USE_IMGUI

      if ( event.type == sf::Event::Closed )
      {
        aborted = true ;
        ui::show_more_images = false ;
      }

      #ifdef WHITE_BAR_BUG_WORKAROUND

      else if ( event.type == sf::Event::Resized )
      {
        auto window_size = p_window->getSize() ;
      
        if (    ui::run_fullscreen
             && (    window_size.x != desktop.width
                  || window_size.y != desktop.height )
            )
        {
          // workaround: re-create the window with Fullscreen

          ui::p_screen->setup_window ( filename ) ;
        }
      }

      #endif // WHITE_BAR_BUG_WORKAROUND

      else if ( event.type == sf::Event::KeyPressed )
      {
        if ( event.key.code == sf::Keyboard::Escape )
        {
          aborted = true ;
          ui::show_more_images = false ;
        }
        else if ( event.key.code == sf::Keyboard::V )
        {
          ui::on_toggle_status() ;
        }
      }
      // TODO: maybe allow more interaction here?
    }

    gui.draw_status_line ( p_window ) ;

    if ( gui.need_display )
    {
      presenter.show ( p_window ) ;
    }

    // if an unrecoverable error occured in a different thread,
    // error_flag is set to true. We react here by showing a
    // message box and terminating lux - this has do be done
    // in this thread.

    if ( error_flag )
    {
      lux_abort ( fatal_error ) ;
    }

    // build_ready should only ever be true when the interpolator
    // was built without errors.

    if ( build_ready )
    {
      if ( build_success == false )
        ui::show_more_images = false ;

      // if an abort was triggered, we have show_more_images false.
      // If so, we return to main() directly, which will shut lux
      // down orderly.

      if ( ui::show_more_images == false )
        return ( 0 ) ;

      break ;
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(35));
  }
  
  if ( source.projection == FACET_MAP )
  {
    // for facet maps, we set 'source' so that it coincides with
    // the first facet horizontally, but we keep it level.

    double facet_yaw = args.facet_yaw[0] ;
    ui::initial_yaw -= facet_yaw ;
    source.hofs = ui::source_v[0].hofs ;
    source.hfov = ui::source_v[0].hfov ;
    ui::hfov = source.hfov ;
  }

  if ( ui::save_hfov_view != 0.0 )
  {
    // this is used in 'comic book mode'

    ui::hfov = ui::save_hfov_view ;
    ui::initial_dy = ui::save_dy ;
    ui::save_hfov_view = 0.0 ;
    ui::save_dy = 0.0 ;
  }
  else if ( args.hfov_view == 0.0 )
  {
    if ( source.projection == MOSAIC )
    {
      std::cout << "fitting mosaic image into the window" << std::endl ;

      double pxh = source.width / target.width ;
      double pxv = source.height / target.height ;

      if ( pxh > pxv )
      {
        ui::hfov = source.hfov ;
      }
      else
      {
        double vfov = source.vfov ;
        ui::hfov = vfov * target.width / target.height ;
      }
    }
    else
    {
      // for rectilinear images, our first guess at the viw's hfov is to
      // fit the image horizontally, taking over the image's hfov. For other
      // projections, we use an arbitrary, reasonably wide-angle value: 70°

      if ( projection == RECTILINEAR )
        ui::hfov = source.hfov ;
      else
        ui::hfov = 70.0 * M_PI / 180.0 ;

      // if the image's hfov is smaller than the desired view hfov, lower
      // the view hfov to the image's so the view is horizontally filled.
      // We only do this for rectilinear *targets*, for other targets we
      // accept any ui::hfov, for example to produce stripes repeating
      // the same full spherical several times. Then, for rectilinear
      // images only, we check if the image fits vertically. If not, we
      // fit it in vertically. We end up with all of the image fitted in.
      // This is new - before the image was fitted only horizontally.

      if ( ui::hfov >= source.hfov && target.projection == RECTILINEAR )
      {
        std::cout
        << "setting the view's hfov to the image's hfov: "
        << source.hfov * 180.0 / M_PI << " degrees" << std::endl ;
        ui::hfov = source.hfov ;

        if ( projection == RECTILINEAR )
        {
          double corresponding_vfov
            = 2.0 * atan (   tan ( ui::hfov / 2.0 )
                           * target.height / target.width ) ;

          if ( corresponding_vfov < source.vfov )
          {
            // image does not fit vertically

            std::cout
            << "setting the view's vfov to the image's vfov: "
            << source.vfov * 180.0 / M_PI << " degrees" << std::endl ;
            ui::hfov = 2.0 * atan (  tan ( source.vfov / 2.0 )
                                  * target.width / target.height ) ;
          }
        }
      }
    }
  }
  else
  {
    // hfov_view was passed on the command line, accept it unconditionally

    ui::hfov = args.hfov_view ;
  }

  // set lock_vertical

  if ( source.projection == MOSAIC )
  {
    ui::lock_vertical = false ;
  }
  else
  {
    ui::lock_vertical = true ;
  }

  ui::auto_position = args.auto_position ;

  if (    ui::auto_position
       && (    source.projection == CYLINDRIC
            || source.projection == SPHERICAL
            || source.projection == FISHEYE
            || source.projection == STEREOGRAPHIC )
     )
  {
    // for 'panoramic' projections, horizontally position view so that
    // the view's left margin coincides with the cropped area's left
    // margin. For rectilinear images, this is not done, because it
    // does not 'feel right' - this is debatable.

    double yaw = M_PI - source.hofs - ui::hfov / 2.0 ;
    ui::initial_yaw += yaw ;

    // vertically position view in the center of the visible area

    double pitch = M_PI - source.vofs - source.vfov / 2.0 ;
    ui::initial_pitch += pitch ;
  }

  double df = 180.0 / M_PI ;

  std::cout << "view hfov:     " << df * ui::hfov << std::endl ;

  // 'mosaic' source projection implies 'mosaic' target projection

  if ( projection == MOSAIC )
    target.projection = MOSAIC ;

  if ( target.projection == MOSAIC )
    ui::lock_vertical = false ;
  else
    ui::lock_vertical = true ;

  if ( ui::snapshot_prefix.empty() )
    ui::snapshot_prefix = std::string ( filename ) + std::string ( ".lux." ) ;

  // x_step is the distance in radians from one draped source image pixel
  // to the next, horizontally. For cubemaps and facet maps, we use the
  // x_step value from the first facet.

  switch ( projection )
  {
    case CUBEMAP:
      source.x_step = 2.0 * tan ( ui::cubeface_fov / 2.0 ) / source.width ;
      break ;
    case FACET_MAP:
      {
        // debatable: picking the first facet's x_step value here

        double f0_hfov = args.facet_hfov[0] ;

        switch ( ui::facet_projection[0] )
        {
          case SPHERICAL:
          case FISHEYE:
          case CYLINDRIC:
          case MOSAIC:
            source.x_step = f0_hfov / source.width ;
            break ;
          case RECTILINEAR:
            source.x_step = 2.0 * tan ( f0_hfov / 2.0 ) / source.width ;
            break ;
          case STEREOGRAPHIC:
            source.x_step = 4.0 * tan ( f0_hfov / 4.0 ) / source.width ;
            break ;
          default:
            abort_lux ( "unknown projection, cannot calculate x_step" ) ;
            break ;
        }
      }
      break ;
    default:
      break ;
  }

  // the 'feathering' command line argument is taken to be in the OOM
  // of pixels, but the rendering code takes model space units. Currently,
  // the UI does not access this value, but I prefer not to modify the
  // value in args.feathering

  ui::feathering = args.feathering * source.x_step ;

  if (    projection == FACET_MAP
       && args.input_is_pto == true )
  {
    // EXIF orientation is read and processed always, but if
    // input_is_pto is true, we have incoming PTO data, where
    // roll information pertains to the image as seen by the sensor,
    // ignoring EXIF orientation. So we have to *adapt the roll*.
    // The rotation has already been done by digest(). Initially I had
    // coded to avoid reacting to the EXIF orientation tag and use the
    // roll as given in the PTO file, but this produces a suboptimal
    // memory access pattern and makes handling less consistent.

    int nfacets = ui::source_v.size() ;

    for ( int i = 0 ; i < nfacets ; i++ )
    {
      bool flipped = false ;

      switch ( ui::source_v[i].exif_orientation )
      {
        case 2:
          flipped = true ;
        case 0:
        case 1:
          break ;
        // note that flipped images are not processed correctly.
        case 4:
          flipped = true ;
        case 3: // upside down
          args.facet_roll[i] += M_PI ;
          break ;
        case 7:
          flipped = true ;
        case 6: // sensor's left edge is image top edge
          args.facet_roll[i] -= M_PI_2 ;
          break ;
        case 5:
          flipped = true ;
        case 8: // sensor's right edge is image top edge
          args.facet_roll[i] += M_PI_2 ;
          break ;
        default: // landscape, flipped, or unknown
          break ;
      }

      if ( flipped )
      {
        std::string error ( "EXIF orientation of facet " ) ;
        error += ui::source_v[i].filename ;
        error += " is a flipped orientation, which lux can't handle" ;
        abort_lux ( error ) ;
      }

      // We repeat the code which encodes the facet orientation
      // in the quaternion in ui::facet_orientation[i],

      double y = args.facet_yaw[i] ;
      double p = args.facet_pitch[i] ;
      double r = args.facet_roll[i] ;

      auto rq = conj ( get_rotation_q ( -y , p , r ) ) ;
      ui::facet_orientation[i] = rq ;

      if ( target.projection == MOSAIC )
      {
        ui::facet_orientation[i][0] = -y ;
        ui::facet_orientation[i][1] = p ;
        ui::facet_orientation[i][2] = r ;
        ui::facet_orientation[i][3] = 0.0 ;
      }
    }
  }

  // snapshot_facet may be set to a facet which should be used as the
  // 'nonstandard target', which is the target used for 'source-like'
  // output done with Shift+E, Shift+P or Shift+U. The default for this
  // value is -1, indicating it was not set (or set deliberately to this
  // value). The default means: take facet 0, *unless* the input is a
  // PTO file with a p-line: in this case the 'nonstandard target' is
  // set up from the information in the p-line.

  int fc = args.snapshot_facet ;

  if ( args.snapshot_facet < 0 )
  {
    // snapshot_facet was not set explicitly, or set explicitly to a
    // value less than zero.

    if ( ! args.have_p_line )
    {
      // if the input wasn't a PTO with a p-line, we set snapshot_facet
      // to zero, meaning the first facet in the set.

      fc = 0 ;
    }
  }
  else
  {
    // snapshot_facet has a value referring to one of the facets.
    // in this case we have to ignore information from the p-line
    // if there is any - especially cropping information, because
    // we want the nonstandard target just like the specified facet.

    args.have_crop = false ;
    args.have_p_line = false ;
  }

  if ( projection == FACET_MAP )
  {
    if ( fc < 0 )
    {
      // set up the nonstandard target from the p-line in the PTO.
      // We've made sure above that we do in fact have the p-line info.

      ui::nonstandard_target.width = args.p_width ;
      ui::nonstandard_target.height = args.p_height ;
      if ( args.p_projection == PRJ_UNSUPPORTED )
      {
        std::string error ( "error processing: " ) ;
        error += filename ;
        error += "\nlux can not produce the the output projection\n" ;
        error += "specified in the PTO file and will stitch to\n" ;
        error += "spherical projection instead\n" ;
        lux_error ( error , true ) ;

        args.p_projection = SPHERICAL ;
      }
      ui::nonstandard_target.projection = args.p_projection ;
      ui::nonstandard_target.hfov = args.p_hfov * M_PI / 180.0 ;
      ui::nonstandard_orientation = get_rotation_q ( 0.0 , 0.0 , 0.0 ) ;
    }
    else
    {
      // set up the nonstandard target from the facet specfied in
      // the snapshot_facet parameter

      if ( fc >= ui::source_v.size() )
      {
        std::string error ( "snapshot_facet " ) ;
        error += std::to_string ( args.snapshot_facet ) ;
        error += " is too large, maximum is " ;
        error += std::to_string ( ui::source_v.size() - 1 ) ;
        abort_lux ( error ) ;
      }

      ui::nonstandard_target.width = ui::source_v[fc].width ;
      ui::nonstandard_target.height = ui::source_v[fc].height ;
      ui::nonstandard_target.projection = ui::facet_projection[fc] ;
      ui::nonstandard_target.hfov = args.facet_hfov[fc] ;
      ui::nonstandard_orientation = conj ( ui::facet_orientation[fc] ) ;
    }

    ui::nonstandard_target.vfov = calculate_vfov ( ui::nonstandard_target.hfov ,
                                               ui::nonstandard_target.width ,
                                               ui::nonstandard_target.height ,
                                               ui::nonstandard_target.projection ) ;
  }
  else if ( projection == CUBEMAP )
  {
    ui::nonstandard_target.width = ui::source_v[fc].width ;
    ui::nonstandard_target.height = ui::source_v[fc].height ;
    ui::nonstandard_target.projection = RECTILINEAR ;
    ui::nonstandard_target.hfov = ui::cubeface_fov ;
    ui::nonstandard_target.vfov = ui::cubeface_fov ;

    double yaw = 0.0 ;
    double pitch = 0.0 ;
    switch ( fc )
    {
      case 1:
        yaw = M_PI_2 ;
        break ;
      case 2:
        yaw = M_PI ;
        break ;
      case 3:
        yaw = M_PI + M_PI_2 ;
        break ;
      case 4:
        pitch = M_PI_2 ;
        break ;
      case 5:
        pitch = -M_PI_2 ;
        break ;
      default:
        break ;
    }
    ui::nonstandard_orientation = get_rotation_q ( yaw , pitch , 0.0 ) ;
  }
  else
  {
    ui::nonstandard_target.width = source.width ;
    ui::nonstandard_target.height = source.height ;
    ui::nonstandard_target.projection = source.projection ;
    ui::nonstandard_target.hfov = source.hfov ;
    ui::nonstandard_target.vfov = calculate_vfov ( ui::nonstandard_target.hfov ,
                                               ui::nonstandard_target.width ,
                                               ui::nonstandard_target.height ,
                                               ui::nonstandard_target.projection ) ;

    if ( ui::nonstandard_target.projection == MOSAIC )
    {
      // mosaic projection only uses the quaternion datatype, but the value
      // does not signify a rotational quaternion.

      ui::nonstandard_orientation = 0.0 ;
    }
    else
    {
      // for the other projections, the quaternion signifies a rotational
      // quaternion encoding the gaze direction.

      ui::nonstandard_orientation = get_rotation_q ( 0.0 , 0.0 , 0.0 ) ;
    }
  }

  // if we're not dealing with a facet map, silently ignore
  // next_after_fusion  and next_after_stitch

  if ( projection != FACET_MAP )
  {
    ui::next_after_fusion = ui::next_after_stitch = false ;
  }
  
  // next we process a few 'compound' arguments, which are used to trigger
  // specific rendering tasks which produce image output and proceed to
  // the next cycle. These arguments were introduced for user convenience;
  // they might be achieved by specifying several separate arguments as
  // well. All of these compound actions use the 'nonstandard target'.

  if ( ( projection == FACET_MAP ) && args.fuse )
  {
    // --fuse=yes triggers an exposure fusion of a facet map.

    ui::use_nonstandard_target = true ;
    ui::next_after_fusion = true ;
    ui::exposure_fusion = true ;
    ui::blending = BLEND_HDR ;
    args.exposure_weight = 1.0 ;
    args.contrast_weight = 0.0 ;
  }
  else if ( args.compress )
  {
    // --compress=yes triggers a 'standard' faux bracket. This may be
    // done from a single (usually HDR) image or from a facet map.

    ui::use_nonstandard_target = true ;
    ui::next_after_fusion = true ;
    ui::faux_bracket = true ;

    ui::exposure_fusion = true ;

    if ( projection != FACET_MAP )
    {
      // if we're not dealing with a facet map, we need to set the
      // blending mode to HDR now, because single images don't have
      // the blending mode set meaningfully. If input is a facet map,
      // we mustn't change the blending mode, to make sure that
      // panoramas with blending=ranked are stitched before the
      // faux bracketing is done.

      ui::blending = BLEND_HDR ;
    }

    // throw an exception if there are no ev values for the faux
    // bracket - this can only occur due to delibertely set wrong
    // arguments, so it should rarely happen.

    assert ( args.faux_bracket_ev.size() ) ;

    // since there is now a default setting for faux brackets via the
    // faux_bracket_size and faux_bracket_step options which default to
    // 3 and 2.0, respectively, I now use those faux bracketing settings
    // which are currently in place (and, likely, the defaults anyway).
    // If the user sets up different parameters for faux brackets, they
    // will be honoured with --compress=yes, which gives the option more
    // power than the previous fixed setting in the  old code:
    // args.faux_bracket_ev = std::vector<double> ( 3 ) ;
    // args.faux_bracket_ev[0] = -2.0 ;
    // args.faux_bracket_ev[1] = 0.0 ;
    // args.faux_bracket_ev[2] = 2.0 ;
  }
  else if ( ( projection == FACET_MAP ) && args.stitch )
  {
    // --stitch=yes stitches a panorama to pto specification

    ui::use_nonstandard_target = true ;
    ui::next_after_stitch = true ;
    ui::stitch_images = true ;
    ui::blending = BLEND_RANKED ;
  }
  else if ( ( projection == FACET_MAP ) && args.focus_stack )
  {
    // --focus_stack=yes produces an exposure fusion with contrast
    // weight only. The resulting image will have the sharpest pixels
    // from all contributing partial images.

    ui::use_nonstandard_target = true ;
    ui::next_after_fusion = true ;
    ui::exposure_fusion = true ;
    ui::blending = BLEND_HDR ;
    args.exposure_weight = 0.0 ;
    args.contrast_weight = 1.0 ;
  }
  else if ( ( projection == FACET_MAP ) && args.deghost )
  {
    // --deghost=yes will combine a set of partial images with 'quorate'
    // blending, weighting by 'agreement': outliers will not agree with
    // pixels in other images, and their weight will be (very) low.

    ui::use_nonstandard_target = true ;
    ui::next_after_snapshot = true ;
    ui::blending = BLEND_QUORATE ;
  }
  else if ( ( projection == FACET_MAP ) && args.hdr_merge )
  {
    // --hdr_merge=yes will create hdr-merged output of an image set.
  
    ui::use_nonstandard_target = true ;
    ui::next_after_snapshot = true ;
    args.snapshot_extension = "exr" ; // checked in store_rendered_image
    ui::snapshot_extension = ".exr" ;
    ui::blending = BLEND_HDR ;
  }
  else if ( ( projection == FACET_MAP ) && args.hdr_fuse )
  {
    // hdr_fuse creates an exposure fusion with hdr_spread == 1.
    // This uses pyramid blending and the result differs subtly
    // from the result obtained with hdr_mere (above), with
    // abvantages if there are 'islands' of bright content in
    // darker surroundings.

    ui::use_nonstandard_target = true ;
    ui::next_after_fusion = true ;
    ui::exposure_fusion = true ;
    ui::blending = BLEND_HDR ;
    args.snapshot_extension = "exr" ;
    ui::snapshot_extension = ".exr" ;
    args.exposure_weight = 1.0 ;
    args.contrast_weight = 0.0 ;
    args.hdr_spread = 1 ;
  }
  else if ( projection == FACET_MAP )
  {
    int nfacets = ui::source_v.size() ;

    // If automatic blending mode detection is on, we
    // guess the blending mode.

    if ( ( nfacets <= 1 ) || ( args.blending == "ranked" ) )
      ui::blending = BLEND_RANKED ;
    else if ( args.blending == "hdr" )
      ui::blending = BLEND_HDR ;
    else if ( args.blending == "quorate" )
      ui::blending = BLEND_QUORATE ;
    else if ( args.blending == "auto" )
    {
      double dy_max = FLT_MIN ;
      double dp_max = FLT_MIN ;
      double de_max = FLT_MIN ;
      double dfov_max = FLT_MIN ;

      for ( int i = 0 ; i < nfacets - 1 ; i++ )
      {
        for ( int j = i + 1 ; j < nfacets ; j++ )
        {
          auto angdiff = []( double a , double b )
          {
            if ( a < 0.0 )
              a += 2.0 * M_PI ;
            if ( b < 0.0 )
              b += 2.0 * M_PI ;
            auto d = fabs ( a - b ) ;
            if ( d < M_PI )
              return d ;
            else
              return 2.0 * M_PI - d ;
          } ;
          double dy = angdiff ( args.facet_yaw[i] , args.facet_yaw[j] ) ;
          double dp = angdiff ( args.facet_pitch[i] , args.facet_pitch[j] ) ;
          double de = fabs (   ui::source_v[i].brightness
                             - ui::source_v[j].brightness ) ;

          // to figure out if the images are similar and similarly placed,
          // we look at their fov in landscape format (with the wider side
          // horizontal) and at their yaw and pitch:

          double fova = std::max ( ui::source_v[i].hfov ,
                                   ui::source_v[i].vfov ) ;
          double fovb = std::max ( ui::source_v[j].hfov ,
                                   ui::source_v[j].vfov ) ;
          double dfov = fabs ( fova - fovb ) ;

          dy_max = ( std::max ( dy , dy_max ) ) ;
          dp_max = ( std::max ( dp , dp_max ) ) ;
          de_max = ( std::max ( de , de_max ) ) ;
          dfov_max = ( std::max ( dfov , dfov_max ) ) ;
        }
      }

      // we use heuristic values to guess the blending mode:
      // if none of the yaw and pitch values differ much from the
      // first facet's, and all the fov values along the wider
      // sides are also similar, we consider the images to be
      // 'on top of each other', implying a fusion.

      bool xph_close = ( dy_max < ( ui::source_v[0].hfov / 10.0 ) ) ;
      xph_close     &= ( dp_max < ( ui::source_v[0].vfov / 10.0 ) ) ;
      xph_close     &= ( dfov_max < ( ui::source_v[0].hfov / 10.0 ) ) ;

      // the automatically detected blending mode is written back to
      // args.blending, to make it stable. e.g. light balancing modifies
      // source data so that the exposures look very close, triggering
      // use of 'quorate' blending if blending is auto. This is new
      // behaviour, I think the whole facet brightnes/overall brightness
      // complex should be worked over.

      if ( xph_close )
      {
        // the yaw, pitch and hfov values don't differ very much,
        // we assume this is a bracket or a serial shot.

        // if the ev values are all within 0.1 of each other, we
        // assume this is an image set for deghosting. This does assume
        // that the given Ev values are meaningful - they may simply be
        // unset, and therefore zero.
        // There is one more scenario where Ev values are close: a focus
        // stack. Using lux for focus stack requires additional parameters,
        // namely --exposure_weight=0 and --contrast_weight=1

        bool has_focus_stack_parms = (    args.exposure_weight == 0
                                       && args.contrast_weight == 1 ) ;

        if ( de_max < 0.1 && has_focus_stack_parms == false )
        {
          // if the Ev values are also close, we assume it's
          // a serial shot for deghosting.

          ui::blending = BLEND_QUORATE ;
          std::cout << "setting blending mode 'quorate' for deghosting"
                    << std::endl ;
          args.blending = "quorate" ;
        }
        else
        {
          // with differing Ev values, or the specific argument pattern
          // for focus stacks, which also require BLEND_HDR, we set
          // blending mode BLEND_HDR

          ui::blending = BLEND_HDR ;
          std::cout << "setting blending mode 'hdr' for HDR or focus stack"
                    << std::endl ;
          args.blending = "hdr" ;
        }
      }
      else
      {
        // if the yaw, pitch and hfov values aren't all close
        // together, we assume it's a panorama

        ui::blending = BLEND_RANKED ;
        std::cout << "setting blending mode 'ranked' for stitching"
                  << std::endl ;
        args.blending = "ranked" ;
      }

    }
    else
    {
      // should not happen:

      std::string error ( "unknown blending mode: " ) ;
      error += args.blending ;
      abort_lux ( error ) ;
    }
  }
  else
  {
    ui::blending = BLEND_NONE ;
  }

  // This is for 'stacked panoramas', and sets all but
  // the 'stack parent' facets to inactive. Inactive facets will
  // only be processed if stacks are fused, otherwise they are
  // ignored to speed up processing.

  if ( projection == FACET_MAP && ui::blending == BLEND_RANKED )
  {
    for ( int i = 0 ; i < ui::source_v.size() ; i++ )
    {
      if ( ui::source_v[i].stack_parent != i )
        ui::source_v[i].active = false ;
    }
  }

  ui::use_rank = (    ui::blending == BLEND_RANKED
                   && args.facet_priority == "none" )
                 ? true
                 : false ;

  // area decimation for the B&A pyramids can only be done with
  // degree-1 splines, and the scaling step mustn't exceed two.
  // This is enforced silently.

  if ( args.bls_i_spline_decimator == -1 )
  {
    args.bls_i_spline_degree = 1 ;
    args.exposure_scaling_step
      = std::min ( args.exposure_scaling_step , 2.0 ) ;
  }
  
  if ( args.bls_q_spline_decimator == -1 )
  {
    args.bls_q_spline_degree = 1 ;
    args.exposure_scaling_step
      = std::min ( args.exposure_scaling_step , 2.0 ) ;
  }

  if ( ui::p_itp->mode == WITH_ALPHA )
  {
    // if there is an alpha channel, we need pre-clearing

    ui::clear_before_draw = true ;
  }
  else
  {
    // if there is no alpha channel, we don't need pre-clear

    ui::clear_before_draw = false ;
  }

  // ui::show_again has to be set true explicitly fo each occasion
  // where it's use is intended.

  ui::show_again = false ;

  // these display_type variables serve to hold the state of the UI and the
  // changes of this state which are gleaned from the user's actions.
  // no_change is just there as a reference for 'no change'

  display_type & state ( ui::state ) ;
  display_type & change ( ui::change ) ;
  display_type & no_change ( ui::no_change ) ;

  bool & active_change ( ui::active_change ) ;

  state = ui::no_change ;
  state.moving_image_scaling = args.moving_image_scaling ;
  state.level_bias = args.level_bias ;

  // TODO: add code to carry state to the next image if user asks for it
  // current behaviour initializes state afresh for each image

  // set state.zoom_factor to 0 to signal that it has to be initialized
  // because a new image was loaded.

  state.zoom_factor = 0 ;
  state.brightness = 1.0 ;
  state.black_point = 0.0 ;
  state.white_point = 255.0 ;

  // set bias and predajust to zero and build the initial orientation
  // from initial_***

  ui::bias = get_rotation_q ( 0.0 , 0.0 , 0.0 ) ;
  state.preadjust = get_rotation_q ( 0.0 , 0.0 , 0.0 ) ;
  state.orientation = get_rotation_q ( ui::initial_yaw , 0.0 , 0.0 ) ;
  state.orientation *= get_rotation_q ( 0.0 , ui::initial_pitch , 0.0 ) ;
  state.orientation *= get_rotation_q ( 0.0 , 0.0 , ui::initial_roll ) ;

  // for mosaic images, we set state.dx, dy and dr to the initial values

  ui::initial_dx *= - fabs ( source.extent.x1 - source.extent.x0 ) ;
  ui::initial_dy *= - fabs ( source.extent.y1 - source.extent.y0 ) ;

  state.dx = ui::initial_dx ;
  state.dy = ui::initial_dy ;
  state.dr = ui::initial_dr ;

  state.speed = ui::initial_speed ;

  // new code: when a 'show-again' happens - so, the same image is
  // displayed again, optionally with 'override arguments', the old
  // behaviour was to start with the display_type object 'state' reset
  // to 'fresh' values. Now the state is saved and restored here, which
  // feels more natural, because most overrides don't actually warrant
  // a reset of the viewer's state. But we need to take care not to
  // run into 'impossible' states, so next we have a sanity check to
  // try and avoid such pitfalls. Because the code is new, the check
  // may not suffice.

  if ( ui::have_state )
  {
    ui::have_state = false ;
    state = ui::saved_state ;

    // we pick up the 'rise' from the saved state and move it to 'change'
    // otherwise it won't have an effect on the rendering side, which has
    // no notion of 'rise' and keeps source.vfov only.

    change.rise = state.rise ;
    state.rise = 0.0 ;

    // now we need a sanity check

    if ( projection == RECTILINEAR && source.hfov > 160.0 * M_PI / 180.0 )
    {
      source.hfov = 160.0 * M_PI / 180.0 ;
      source.vfov = calculate_vfov ( source.hfov , source.width ,
                                     source.height , projection ) ;
    }

    if ( projection == STEREOGRAPHIC && source.hfov > 2.0 * M_PI )
    {
      source.hfov = 2.0 * M_PI ;
      source.vfov = calculate_vfov ( source.hfov , source.width ,
                                     source.height , projection ) ;
      if ( source.vfov > 2.0 * M_PI )
      {
        source.vfov = 2.0 * M_PI ;
        source.hfov = calculate_vfov ( source.vfov , source.height ,
                                      source.width , projection ) ;
      }
    }

    if ( target.projection == RECTILINEAR )
    {
      if ( ui::hfov > 160.0 )
        ui::hfov = 160.0 ;
    }
    // else if ( target.projection == MOSAIC )
    // {
    //   if ( ui::hfov > 1.0 )
    //     ui::hfov = 1.0 ;
    // }
  }

  // for click-and-drag processing, we use this set of variables.
  // We aren't relying on SFML's event processing for click-and-drag
  // but test the state of the mouse in real-time, so we need this
  // logic to process the user activity.

  int click_position_x ;
  int click_position_y ;
  double displace_x = 0.0 ;
  double displace_y = 0.0 ;
  bool click_and_drag = false ;
  bool sec_click_and_drag = false ;

  // create some SFML-compatible RGBA frames and queue them

  // having two more frames than 'frame_threshold' runs fastest on my system.
  // One less will do if pressed, as many as threshold is too low and can stall
  // the generating thread

  // the frame queue may already hold frames from a previous invocation
  // of inner_main, so we only add frames if there are less than the intended
  // minimum amount:

  for ( int i = rgb_queue.size() ; i < frame_threshold + job_threshold ; i++ )
  {
    rgb_queue.push
     ( mem_in() << new rgba_image_type
        ( vigra::Shape2 ( target.width , target.height ) ) ) ;
  }

  // some statistics

  double frame_time_moving_average = budget * .91 ;
  double frame_time_cumulated = 0.0 ;
  int frames_showed = 0 ;
  pv_clock::time_point last_time ;
  pv_clock::time_point back_again ;

  // to measure times from one call of display to the next while in an animated
  // sequence (pan, zoom etc.) we use this set of variables:

  pv_clock::time_point display_last_called = pv_clock::now();
  pv_clock::time_point display_called = pv_clock::now();
  int dt_count = 0 ;
  float total_ms = 0.0 ;
  int frames_in_sequence = 0 ;

  int pyramid_level = 0 ;
  int change_direction = 0 ;

  pv_clock::time_point end_interval ;

  // set up slide show operation if ui::slide_interval is greater then zero

  end_interval =   pv_clock::now()
                  + std::chrono::milliseconds
                    ( long ( ui::slide_interval * 1000.0 ) ) ;

  // if we arrive here and ui::show_previous is set, this has been done
  // during the last inner_main invocation. So this is in fact the invocation
  // displaying the previous image. For this special case we set ui::grace_period
  // to -1 to run in 'suspended slide show' mode. I reckon this is a good way
  // to act: if, during a slide show, user goes back to the previous image,
  // there is obviously an increased focus on that image, and proceeding
  // with the slide show would be mor irritating than the fact that, after
  // going back, the slideshow has to be restarted actively.

  if ( ui::show_previous )
  {
    ui::show_previous = false ;
    ui::grace_period = -1 ;
  }
  else
  {
    ui::grace_period = ui::slide_interval > 0 ? 1 : -1 ;
  }

  ui::relaunch = true ;

  // if any of the next_after flags are set, we take note and set
  // batch_flag. This will remain true until the next cycle resets
  // the next_after flags, so that main() can determine whether to
  // show a file select dialog on empty queue or not.

  ui::batch_flag =    ui::next_after_snapshot
                   || ui::next_after_fusion
                   || ui::next_after_stitch ;


  while ( ui::relaunch )
  {
    bool first_mouse_move = true ;
    ui::relaunch = false ; // is set true again actively when fullscreen/window mode is toggled
    ui::mode = ui::save_mode ; // start out with whatever is in save_mode
    ui::subimage = ui::p_itp->subimage ;

    change.draw = true ; // force redraw

    ui::p_screen->set_fullscreen ( ui::run_fullscreen , filename ) ;

    // this flag is set to true whenever scale or moving_image_scaling is changed. We need
    // to keep track of such changes, because when in PAN mode, we have to start
    // afresh if such changes happen, since PAN mode uses a 'warp' array of coordinates
    // which become invalid with a scale change.

    // enter the the main loop for the window we have just created.

    bool & need_display = gui.need_display ;

    pv_clock::time_point last_ui_poll = program_start ;

    // ui::hfov_norm and vfov_norm are those field of view values for which,
    // with the current window size, a window pixel will be as large as
    // a source image pixel. Reference is the window center. The vertical
    // values aren't used currently.

    auto window_size = p_window->getSize() ;

    if ( source.projection == MOSAIC )
      ui::hfov_norm = source.hfov * ( double(window_size.x) / source.width ) ;
    else
      ui::hfov_norm = 2.0 * atan ( source.x_step * window_size.x / 2.0 ) ;

    // the 'plain' zoom, ui::initial_zoom, is set so that, given the current window
    // size and desired hfov, setting state.zoom_factor to this value will result
    // in the desired hfov filling out the window.

    // TODO: when going fullscreen after showing a window with large hfov,
    // we come out wrongly.

    ui::initial_zoom = ui::hfov_norm / ui::hfov ;

    // for a rectilinear target, ui::zoom_min is set so that hfov
    // cannot reach 180 degrees by zooming, and for stereographic
    // projection we set a limit of 360 degrees.

    if ( target.projection == RECTILINEAR )
      ui::zoom_min = ui::hfov_norm / M_PI ;
    else if ( target.projection == STEREOGRAPHIC )
      ui::zoom_min = ui::hfov_norm / ( 2.0 * M_PI ) ;
    else
      ui::zoom_min = 0 ;

    // if state.zoom_factor is yet unset because a new image was loaded, this is
    // the initial value we pick. If it's just a 'relaunch' of the same image
    // due to switching between full-screen and windowed display, we carry
    // state.zoom_factor through, so that the visible size of the content remains
    // unchanged - if possible. This is new behaviour; previously the hfov was
    // kept constant.

    if ( state.zoom_factor == 0 )
    {
      state.zoom_factor = ui::initial_zoom ;
    }
    else
    {
      // sanity check. when 'relaunching' an image, the previous zoom factor
      // may be out of range.
      state.zoom_factor = std::max ( ui::zoom_min + .05 , state.zoom_factor ) ;
    }

    // this flag is set if an SFML key pressed event happens. If set, we check
    // for chronic keyboard input. If there is none, the flag is cleared.

    bool key_alert = false ;

    pv_clock::time_point last_time_active ;

    // call presenter.show unconditionally if suppress_display is set.
    // this will show the splash screen.

    if ( ui::suppress_display )
      presenter.show ( p_window ) ;

    // Finally, we enter the 'main loop'. The viewer will spend most
    // of it's time in this loop, checking for user input, processing it,
    // sending rendering jobs to the rendering threads, and displaying
    // any frames the rendering thread has produced. The loop will be
    // throttled by calls to presenter.poll() which result in a short
    // sleep of up to 10 msec if there are no frames to be had.

    // while ( ui::p_screen->p_window->pollEvent ( event ) )
    // {
    //   // discard any pending events
    // }

    while ( ui::mode != TERMINATE )
    {
      pv_clock::time_point cycle_start = pv_clock::now() ;

      back_again = cycle_start ;
      active_change = false ;

      // let the gui redraw the status line if there was a status change
      // this only renders to the status line's RenderTexture, and only
      // if the status line is switched on and there was a change, so
      // this call is not expensive and can be executed frequently

      gui.draw_status_line ( p_window ) ;

      bool change_draw_backup = change.draw ;
      change.draw = false ;

      // update last_ui_poll

      last_ui_poll = cycle_start ;

      // if we have a non-negative grace period, we're in slide-show mode

      if ( ui::grace_period >= 0 && ui::slideshow_on )
      {
        // how much time is left until the end of the slideshow interval?

        ui::grace_period = std::chrono::duration_cast < std::chrono::microseconds >
                        ( end_interval - cycle_start ) . count() ;

        // if time is up, act as if Escape was pressed, unless
        // the queue is empty: then, switch slide show mode off.
        // This avoids program termination on empty queue and
        // forces the user to interact.

        if ( ui::grace_period < 0 )
        {
          if ( ui::file_queue.size() || ui::files_from_pipe )
          {
            ui::mode = TERMINATE ;
            ui::relaunch = false ;
          }
          else
          {
            ui::slideshow_on = false ;
          }
        }
      }

      ui::hfov = ui::hfov_norm / state.zoom_factor ;
      ui::angular_intensity = ui::snappiness * ui::hfov ;

      if ( sigint_signal == true )
      {
        sigint_signal = false ;
        ui::on_show_again() ;
      }

      sf::Event event ;

      // first we poll for 'acute' events which result in changes to the overall state.
      // These changes are singular and affect the viewer's state directly.

      while ( ui::p_screen->p_window->pollEvent ( event ) )
      {
        // Some events are processed unconditionally:

        if ( event.type == sf::Event::Closed )
        {
          ui::on_terminate() ;
          break ;
        }
        else if ( event.type == sf::Event::Resized )
        {
          // if the window was resized, we need to recalculate ui::hfov_norm,
          // vfov_norm, and ui::initial_zoom which depend on the window size. With
          // the new values, the unchanged scale in state.zoom_factor will result
          // in the content being displayed in the same magnification as
          // before the change of the window's size.

          auto window_size = p_window->getSize() ;

          // as of this writing (late 2023), at times, on Linux, when
          // running in full-screen mode, a white bar appears at the top
          // of the screen. It seems like the window manager is trying
          // to show an (empty) title bar, as if the full screen were an
          // ordinary window. I have not yet found a fix for this bug,
          // but here's a workaround: I do at least get a Resized event
          // with te wrong height, and if I re-create the window with
          // Fullscreen, I get a bit of flicker but the erroneous state
          // does not persist. I posted the work-around here:
          // https://en.sfml-dev.org/forums/index.php?topic=29283.0


          #ifdef WHITE_BAR_BUG_WORKAROUND

          if (    ui::run_fullscreen
               && (    window_size.x != desktop.width
                    || window_size.y != desktop.height )
              )
          {
            // workaround: re-create the window with Fullscreen

            ui::p_screen->setup_window ( filename ) ;
          }

          #endif // WHITE_BAR_BUG_WORKAROUND

          if ( source.projection == MOSAIC )
            ui::hfov_norm = source.hfov * ( double(window_size.x) / source.width ) ;
          else
            ui::hfov_norm = 2.0 * atan ( source.x_step * window_size.x / 2.0 ) ;

          ui::initial_zoom = ui::hfov_norm / ui::hfov ;

          // animation is pretty jumpy when autopanning while resizing the window,
          // but stable. setting change.draw is necessary because otherwise when
          // the image is static the changed size does not have an effect.

          change.draw = true ;
        }

#ifdef USE_IMGUI

        // tentative introduction of ImGui content. show_imgui is switched on
        // and off by pressing 'C', clicking outside the demo window switches
        // it off as well. If ImGui is on, it gets to see most events which
        // it is 'interested in' and may consume them (by calling 'continue',
        // which skips over the 'normal' processing below.

        if ( ui::show_imgui )
        {
          auto & imgui_io = ImGui::GetIO();

          // if ImGui is on, we process some events here. First we see if
          // the events are 'meant for' ImGui: ImGui will tell us so by
          // setting the relevant IO flags. If these flags are *not* set,
          // we let the event 'slip through' past this if-else if-else
          // construct

          if (    ( ! imgui_io.WantCaptureKeyboard )
               && event.type == sf::Event::KeyPressed )
          {
            // if ImGui does not want keyboard input, we let the keyboard
            // event slip through. The empty scope is deliberate.
          }
          else if (    ( ! imgui_io.WantCaptureMouse )
                    && (    event.type == sf::Event::MouseButtonPressed
                         || event.type == sf::Event::MouseButtonReleased )
                  )
          {
            // mouse activity while ImGui does not care for mouse
            // events switches ImGui off. The mouse event slips through
            // to 'normal' processing.

            // ui::show_imgui = false ;
          }
          else if (    ( ! imgui_io.WantCaptureMouse )
                    && ( event.type == sf::Event::MouseWheelScrolled )
                  )
          {
            // scroll wheel activity slips through.
          }
          else if (    ( ! imgui_io.WantCaptureMouse )
                    && ( event.type == sf::Event::MouseMoved )
                  )
          {
            // if the mouse leaves the ImGui realm,
            // switch off the main menu
          
            if ( ui::show_main_menu && event.mouseMove.y >= 120 )
            {
              ui::show_main_menu = false ;
            }
          }
          else
          {
            // all remaining events are passed on to ImGui:

            ImGui::SFML::ProcessEvent(*p_window, event);
            
            // By issuing a 'continue' here, we effectively consume
            // the event, assuming that ImGui has taken care of it.
            // It should decidedly *not* have any effect outside
            // of ImGui.

            continue ;
          }
        }

#endif // #ifdef USE_IMGUI

        // arriving here means the event has not been consumed by ImGui,
        // either because it is not on, or because it has ignored it.
        // We carry on with 'ordinary' processing and rely on all Imgui
        // interaction having been taken care of.

        if ( ui::show_modal_dialog )
        {
          // during a modal dialog, the events are ignored: only
          // Imgui should deal with them.
          ImGui::SFML::ProcessEvent(*p_window, event);
          // make sure the Mouse Pointer Remains visible
          last_mouse_activity = cycle_start ;
          need_display = true ;
        }
        else if ( event.type == sf::Event::TextEntered )
        {
          if ( gui.on && gui.focus )
          {
            // if the GUI is 'on' and 'focused', we catch TextEntered events.
            if ( gui.take ( event.text.unicode ) )
            {
              gui.activity = need_display = true ;
            }
          }
          // if the gui is not both on and focused, we ignore TextEntered
          // events altogether, so no 'else' clause here
        }
        else if ( event.type == sf::Event::KeyPressed )
        {
//             std::cout << "key pressed event: " << event.key.code << std::endl ;

          // all key presses set active_change, unless the GUI 'catches' them

          active_change = true ;

          bool handled_by_gui = false ;
          if ( gui.on && gui.focus )
          {
            // if the gui 'takes' the key code, we set 'handled_by_gui'
            handled_by_gui = gui.take ( event.key.code ) ;
          }

          if ( handled_by_gui )
          {
            // if the key code was handled by the gui, we set the usual flags
            gui.activity = need_display = true ;
            // and we also clear active_change
            if ( event.key.code == sf::Keyboard::Return )
            {
              // TODO: think about this:
              // if the GUI takes a Return key press, the user must
              // have committed something, because editable entities
              // 'take' Return. We assume this is very likely to have
              // a visible effect, so we set the relevant flags
              change.draw = true ;
            }
            else
            {
              active_change = false ;
            }
          }
          else
          {
            // key is not handled by the GUI.

            // if a 'chronic' key is pressed, we set a flag which results
            // in a check for 'chronic' keyboard input further down.
            // The flag is cleared by the code handling chronic input,
            // if it can't detect any chronic input. Testing for chronic
            // input is done by going through all 'in-play' keys in turn
            // and checking their real-time state: pressed or not. This
            // is quite expensive, so we avoid it as best as we can. The
            // mechanism with key_alert does so efficiently: Once no more
            // chronic keys are found to be active, a KeyPressed event *must*
            // happen before the test *can* find any depressed keys at all.
            // There is another way of keeping track of 'chronic' keys:
            // looking for corresponding KayReleased events. But this is
            // unreliable: if the window loses focus while the 'chronic'
            // key is held depresed, no KeyReleased event is obtained.
            // The method used here is simple and foolproof, at the cost
            // of using the slightly more expensive real-time key check.
            // But this is limited to only those keys which are 'in play',
            // meaning that a KeyPressed event with this code was detected.

            if ( ui::ctest ( event.key.code , event.key.shift ) )
            {
              // there is a 'chronic' action bound to this key

              key_alert = true ;

              // we need to check if the key code is already 'in play'.
              // if so, we mustn't add it again. If a key is held depressed,
              // after a while the keyboard driver auto-repeats it and we
              // receive additional KeyPressed events for it, hence the check.

              bool found = false ;
              for ( auto const & candidate : ui::chronic_in_play )
                found |= ( candidate == event.key.code ) ;
              if ( ! found )
                ui::chronic_in_play.push_back ( event.key.code ) ;
            }

            // KFJ 2019-12-03 all acute key events are now handled via
            // a jump table in namespace ui. This is more efficient than
            // the series of else-if statements I've used before. The
            // jump table invokes functions which are also used by
            // the GUI (on_...), and the idea is to channel all user
            // interaction through such 'trigger' functions, and implement
            // new ways of triggering them (e.g. script-driven)

            // if the key was not a 'chronic' key, try and process it
            // as an 'acute' key

            else
            {
              ui::react ( event.key ) ;
            }
          }
        } // end of key pressed

        // process mouse events
        // contrary to a 'pure' imgui, we make the GUI decide on
        // the click of a mouse button if it wants to handle it.
        // This happens on the depression of the button and is done
        // by calling gui.click. If the GUI lets the click pass,
        // pv's normal key handling takes place.

        else if ( event.type == sf::Event::MouseButtonPressed )
        {
          last_mouse_activity = cycle_start ;
          active_change = true ; // mouse action also sets active_change

          click_position_x = event.mouseButton.x ;
          click_position_y = event.mouseButton.y ;

          // call gui.take with the mouse button event. If the GUI
          // wants to 'consume' the mouse click, it returns true.
          // If not, the subsequent code picks it up.
          if ( gui.take ( event ) )
          {
            gui.activity = need_display = true ;
          }
          else if ( event.mouseButton.button == sf::Mouse::Middle )
          {
            ui::mode = TERMINATE ;
            ui::relaunch = false ;
            break ;
          }
          else
          {
            // start click and drag processing
            change.draw = true ;
            ui::mode = IMMEDIATE ;
            displace_x = displace_y = 0 ;
            if ( event.mouseButton.button == sf::Mouse::Left )
            {
              click_and_drag = true ;
              primary_mouse_button_pressed = cycle_start ;
            }
            else
            {
              sec_click_and_drag = true ;
              secondary_mouse_button_pressed = cycle_start ;
            }
          }
        } // end of MouseButtonPressed

        else if ( event.type == sf::Event::MouseButtonReleased )
        {
          last_mouse_activity = cycle_start ;
          active_change = true ;

          // during click and drag processing, mouse button release
          // will not have an effect on the GUI. If no click and drag
          // operation is in progress, we check if the GUI wants to
          // handle the event. If not, we proceed with normal pv processing

          if (    ( ! click_and_drag )
                && ( ! sec_click_and_drag )
                && gui.take ( event ) )
          {
            // gui grabs the event
            gui.activity = need_display = true ;
            active_change = false ;
          }
          else if ( event.mouseButton.button == sf::Mouse::Left )
          {
            // if the user lets pass less than args.single_click_slot
            // msec between mouse button press and release, and if
            // there is only a small displacement during this time,
            // we assume she is issuing a click.

            auto dt = std::chrono::duration_cast<std::chrono::milliseconds>
                      ( cycle_start - primary_mouse_button_pressed )
                      .count() ;

            if ( dt < args.single_click_slot )
            {
              if ( std::abs ( displace_x ) > args.spin_threshold )
              {
                // fast displacement within the args.single_click_slot
                // time window. tentative new 'spin' gesture

                if ( ui::autopan * displace_x > 0 )
                  ui::autopan += ui::angular_intensity * displace_x / 7 ;
                else
                  ui::autopan = ui::angular_intensity * displace_x / 7 ;
              }
              else if ( ui::autopan )
              {
                // single click without large displacement:
                // stop active autopan

                ui::on_toggle_autopan() ;
              }
            }

            // terminate secondary click-drag gesture.

            click_and_drag = false ;
            displace_x = displace_y = 0 ;
          }
          else if ( event.mouseButton.button == sf::Mouse::Right )
          {
            // if the user lets pass less than args.single_click_slot
            // msec between mouse button press and release, and if
            // there is only a small displacement during this time,
            // we assume she is issuing a click.

            auto dt = std::chrono::duration_cast<std::chrono::milliseconds>
                      ( cycle_start - secondary_mouse_button_pressed )
                      .count() ;

            if ( dt < args.single_click_slot )
            {
              auto stim = sqrt (   displace_x * displace_x
                                 + displace_y * displace_y ) ;

              if ( stim <= 2.5 )
              {
                // it's just a click, no click and drag.
                // tentative use for click-to-center, may fit well with
                // center-focused zoom mediated by vertical secondary
                // click-drag

                ui::on_center_here() ;
              }
            }

            // terminate secondary click-drag gesture.

            sec_click_and_drag = false ;
            displace_x = displace_y = 0 ;
          }
        } // end of MouseButtonReleased

        else if ( event.type == sf::Event::MouseMoved )
        {
          // it seems like there is an artificial MouseMoved event
          // a few cycles after startup, which yields the initial
          // mouse position, without the mouse having been moved.

          if ( first_mouse_move )
          {
            first_mouse_move = false ;
          }
          else
          {
            last_mouse_activity = cycle_start ;
          }

          // We may need
          // to scale screen coordinates to GUI coordinates here;
          // the GUI is rendered to a fullHD RenderTarget and
          // displayed through a view. We scale on the ratio
          // between the widths, and proportionally, so the
          // y coordinate is scaled with the same factor

          auto sz = p_window->getSize() ;
          double scale_to_gui = gui.right_border / gui.gui_extent ;

          // if click and drag processing is active, calculate displacement
          // during click-and-drag processing, entering the GUI's
          // domain has no effect.
          if ( click_and_drag == true || sec_click_and_drag == true )
          {
            active_change = true ;
            displace_x = event.mouseMove.x - click_position_x ;
            displace_y = event.mouseMove.y - click_position_y ;
          }
          // no click-and-drag processing in process
          else if (    ui::legacy_gui == true
                    && event.mouseMove.y < ( gui.show_at / scale_to_gui ) )
          {
            // mouse has moved to menu activation area. We switch the
            // GUI on, irrespective of gui_automatic

            gui.on |= true ;
          }
          else if ( event.mouseMove.y > ( gui.hide_at / scale_to_gui ) )
          {
            // mouse has moved so far down that the menu is hidden

            if ( gui.on && ui::gui_automatic )
            {
              gui.on = false ;
              gui.activity = need_display = true ;
            }
          }
          else if (    ui::legacy_gui == false
                    && event.mouseMove.y < ( 100 ) )
          {
            // switch ImGui on if the mouse is near the top margin

            ui::show_imgui = true ;
            ui::show_main_menu = true ;
          }

          if ( gui.on )
          {
            // pass the new mouse coordinates on to the GUI. We need
            // to scale screen coordinates to GUI coordinates here;
            // the GUI is rendered to a fullHD RenderTarget and
            // displayed through a view. We scale on the ratio
            // between the widths, and proportionally, so the
            // y coordinate is scaled with the same factor
            gui.mousex = event.mouseMove.x * scale_to_gui ;
            gui.mousey = event.mouseMove.y * scale_to_gui ;
            gui.activity = need_display = true ;
          }

        } // end of MouseMoved

        else if ( event.type == sf::Event::MouseWheelScrolled )
        {
          double scale_to_gui = gui.right_border / gui.gui_extent ;

          // if the GUI is on and a mouse wheel scroll occurs
          // inside the GUI zone, the GUI is shifted horizontally.
          // Otherwise the result is a zoom. The check for
          // the mouse position when the scroll occurs is
          // needed, because the GUI may be switched on
          // permanently (HIDE GUI is off), in which case the
          // zoom-via-mouse gesture would be disabled and all
          // mouse wheel scrools would result in shifting the
          // GUI.

          if (    gui.on
                && event.mouseWheelScroll.y
                  <= ( gui.lower_border / scale_to_gui ) )
          {
            gui.take ( event ) ;
            gui.activity = need_display = true ;
          }
          else
          {
            last_mouse_activity = cycle_start ;
            active_change = true ;
            change.draw = true ;

            // if scw_focused_zoom is set, we initiate a focused zoom.
            // we only want to calculate the zoom locus (below)
            // if the gesture starts - if the buffer already
            // is non-zero, we keep on working with the initial
            // focus.

            if ( args.scw_focused_zoom && ( ui::zoom_buffer <= 0 ) )
            {
              auto to_model = ui::get_to_model_tf() ;

              // now get the 'locus' and 'focus' of the zoom.
              // locus is in 2D image coordinates, focus is in
              // 3D ray coordinates in model space.

              ui::zoom_locus
                = point_2d_d_type ( event.mouseWheelScroll.x ,
                                    event.mouseWheelScroll.y ) ;

              ui::zoom_focus = to_model ( ui::zoom_locus ) ;
            }

            // to achieve a smooth zoom, we buffer intensities generated
            // from the scroll delta. Whenever the buffer is modified,
            // the depletion rate (zoom_decay) is recalculated so that
            // it will draw out the zoom over 10 cycles. This way, when
            // the scroll wheel is scrolled much, the buffer receives
            // a large value but also depletes quickly. Depletion
            // happens when the change 'manifests' in a frame, and
            // the depletion rate steers the speed of the zoom. Once
            // the value in the buffer falls below the depletion rate,
            // (like, after 10 cycles) it is zeroed.

            auto intensity =   15.0 / scale_to_gui
                              * ui::snappiness
                              * event.mouseWheelScroll.delta ;
            ui::zoom_buffer += intensity ;
            ui::zoom_decay = ui::zoom_buffer / 10 ;
          }
        } // end of MouseWheelScrolled

#ifdef USE_IMGUI

        else
        {
          // run all remaining events by ImGui.
          // This keeps ImGui 'oriented'. Passing all the keyboard and mouse
          // events is unnecessary, though - if ImGui is not on, such events
          // can't have an effect.

          ImGui::SFML::ProcessEvent(*p_window, event);
        }

#endif // #ifdef USE_IMGUI

      }   // closes SFML event processing loop

      // if an unrecoverable error occured in a different thread,
      // error_flag is set to true. We react here by showing a
      // message box and terminating lux - this has do be done
      // in this thread.

      if ( error_flag )
      {
        lux_abort ( fatal_error ) ;
      }

      // if 'mode' was set to TERMINATE, leave main processing loop, either to
      // 'relaunch', if fullscreen/window mode was toggled, or to terminate the
      // program, because the window was clicked closed or Escape was pressed

      if ( ui::mode == TERMINATE )
      {
//           std::cout << "breaking on terminate" << std::endl ;
        break ;
      }

      // Now process 'chronic' user input. This is input from keys which
      // are held and have effect while held, rather than constituting singular
      // changes to the state. This type of input modifies 'change', which
      // holds the intended 'delta' of a change and, while present, is applied
      // (added, or multiplied for quaternions) to 'state' for every job which
      // is issued. This looks complicated: why can't we simply apply these
      // changes to 'state' straight away? Because depending on the state of the
      // queues, a cycle of the event loop may result in issuing of no, one,
      // or several jobs. If we were to apply a chronic change with every cycle
      // of the event loop, the cumulated effect would vary widely. Doing it
      // as we do, we get a smooth variation.

      // We have the parameter 'ui::snappiness', which is an overall multiplicative
      // factor representing the sensitivity to chronic user input. High ui::snappiness
      // means high sensitivity. Some chronic user input has scale-invariant effects,
      // like rotating the image or zooming. Here, we pass 'snappiness' as the
      // operation's 'intensity' parameter directly. Other chronic user input
      // depends on the target field of view: panning and pitching is expressed
      // in terms of an angle. Modifying an angle without looking at the current
      // target's field of view would result in strong response for small fov,
      // and weak response for high fov. So for these types of input we use
      // an intensity which is adapted to the current target's hfov, see
      // ui::angular_intensity

      // if the window has focus, we look at the state of some keys. Looking without
      // focus would usurp input from other processes which isn't what we want.
      // TODO: security? SFML can obviously look at the state of the keyboard no matter
      // what the focus is - this reflects (on Linux) what X provides: access to
      // the real-time state of any and all keys. Snooping is easy, but here
      // we explicitly code to show that we're not snooping:

      if ( p_window->hasFocus() )
      {
        // we check key_alert. If it is set, there may be 'chronic'
        // keyboard input. We only check those keys for which we have
        // previously received a KeyPressed event

        if ( key_alert )
        {
          // first we clear key_alert. If there is any chronic keyboard
          // input (keys held depressed) key_alert is set to true again,
          // and the next iteration of the main loop will check for
          // more chronic input. But if no chronic keyboard input is
          // detected, the flag will be off. Only if the user produces
          // a KeyPressed event, the flag is set true again and the
          // cycle starts afresh.
          // While there actually is chronic input, key_alert will
          // always be true for the next cycle.
          // Why the acrobatics? The code below unconditionally
          // checks each possible key in real time to see if it is
          // depressed or not. If we can avoid this series of checks
          // altogether, we gain much. The bit of extra code setting
          // the flag is negligible by comparison.

          key_alert = false ;

          // check if shift key is pressed

          bool shift_flag = false ;

          if (    sf::Keyboard::isKeyPressed ( sf::Keyboard::LShift )
               || sf::Keyboard::isKeyPressed ( sf::Keyboard::RShift ) )
          {
            shift_flag = true ;
          }

          // go through the 'in-play' keys and check their state in real time

          for ( auto const & key : ui::chronic_in_play )
          {
            if ( sf::Keyboard::isKeyPressed ( key ) )
            {
              key_alert = true ;
              ui::creact ( key , shift_flag ) ;
            }
          }

          // if no 'chronic' keys were found depressed; clear the in-play vector

          if ( ! key_alert )
            ui::chronic_in_play.clear() ;
        }

        // GUI 'buzzers' work like keyboard keys which are pressed
        // and held, and are used for 'chronic' input.
        // As long a buzzer is pressed, it's assigned trigger is
        // in gui_state.buzzer_triggered. Here we process these
        // triggers, which has the same effect as other events
        // bound to the same trigger: some 'chronic' ui function
        // is executed (ui::do_...)
        // if the vector is empty, we just 'pass by'.

        for ( const auto & trigger : gui.buzzer_triggered )
          trigger() ;

      }

      // If the user is currently pressing a buzzer type button,
      // don't switch off the mouse. TODO: might keep mouse on
      // unconditionally while gui is on and the mouse is inside
      // the sensitive area

      if ( gui.buzzer_triggered.size() )
      {
        last_mouse_activity = cycle_start ;
      }

      // process primary mouse key click and drag data, if any. This is
      // cumulative, if anything else has already produced a change,
      // this goes on top.

      // new code: threshold for click and drag processing, producing a
      // 'dead zone' near the initial click position where there is no
      // effect. If total displacement is less than click_drag_threshold,
      // the click-drag is silently ignored. For now, we apply this
      // logic for both primary and secondary click-drag (one might
      // use different thresholds). The thresholding is done by looking
      // at the absolute displacement, which makes the code slightly
      // more complex, but more intuitive than looking at dx and dy
      // individually. If the threshold is exceeded, cd_sensed is set
      // and cd_dx and cd_dy contain the displacements, diminished by
      // the threshold, and optionally reversed if reverse_drag is
      // set. These preprocessed displacement data are then processed
      // by the code as the un-thresholded data were processed before.

      int cd_dx = 0 ;
      int cd_dy = 0 ;
      bool cd_sensed = false ;

      if ( click_and_drag || sec_click_and_drag )
      {
        // find the absolute displacement

        auto displaced = sqrt (   displace_x * displace_x
                                + displace_y * displace_y ) ;

        if ( displaced > args.click_drag_threshold )
        {
          // the total displacement exceeds the threshold

          cd_sensed = true ;

          // stimulus is the total displacement minus the threshold

          auto stim = displaced - args.click_drag_threshold ;

          // figure out the displacement's x and y component
          // so that their sum equals one

          auto dxabs = std::abs ( displace_x ) ;
          auto dyabs = std::abs ( displace_y ) ;
          auto xcomp = dxabs / ( dxabs + dyabs ) ;
          auto ycomp = dyabs / ( dxabs + dyabs ) ;

          // and distribute the stimulus accordingly

          cd_dx = xcomp * stim ;
          cd_dy = ycomp * stim ;

          // copy the sign of the displacement

          if ( displace_x < 0 )
            cd_dx = - cd_dx ;
          if ( displace_y < 0 )
            cd_dy = - cd_dy ;

          // if reverse_drag is set, we produce the opposite effect:

          if ( click_and_drag && ui::reverse_drag )
          {
            cd_dx = - cd_dx ;
            cd_dy = - cd_dy ;
          }
          else if ( sec_click_and_drag && ui::reverse_secondary_drag )
          {
            cd_dx = - cd_dx ;
            cd_dy = - cd_dy ;
          }
        }
      }

      if ( click_and_drag && cd_sensed )
      {
        last_mouse_activity = cycle_start ;

        if (    sf::Keyboard::isKeyPressed ( sf::Keyboard::LShift )
             || sf::Keyboard::isKeyPressed ( sf::Keyboard::RShift ) )
        {
          // apply roll. this is only done if the vertical component of the
          // click-drag exceeds the horizontal component.

          if ( fabs ( displace_y ) > fabs ( displace_x ) )
            turn ( state , change , ui::snappiness * cd_dy * .02 ) ;
        }
        else if (    sf::Keyboard::isKeyPressed ( sf::Keyboard::LControl )
                  || sf::Keyboard::isKeyPressed ( sf::Keyboard::RControl ) )
        {
          if (    source.projection != FISHEYE
               && source.projection != STEREOGRAPHIC )
            rise ( state , change , ui::angular_intensity * cd_dy * .01 ) ;
        }
        else
        {
          tilt ( state , change ,  ui::angular_intensity * -.01 * cd_dy ) ;
          if ( ui::lock_vertical )
            yaw ( state , change ,  ui::angular_intensity * .01 * cd_dx ) ;
          else
            pan ( state , change ,  ui::angular_intensity * .01 * cd_dx ) ;
        }
      }

      // for click and drag with the secondary mouse key we produce either
      // a change in brightness (horizontal click and drag) or a zoom
      // (vertical click and drag). Whichever displacement is numerically
      // larger 'wins', we don't react with both actions at the same time
      // as this would be confusing.

      if ( sec_click_and_drag && cd_sensed )
      {
        if ( fabs ( displace_y ) > fabs ( displace_x ) )
        {
          last_mouse_activity = cycle_start ;

          zoom ( state , change , ui::snappiness * cd_dy * -.01 ,
                 ui::zoom_min ) ;

          // if scvd_focused_zoom is set, we initiate a focused zoom.

          if (    args.scvd_focused_zoom
               && ( ui::zoom_focus == point_3d_d_type ( 0.0 ) ) )
          {
            // get the 'focus' of the zoom - we produce a focused
            // zoom for this gesture, just like for a mouse wheel
            // roll. We only set the focus if it wasn't set before

            ui::zoom_locus
              = point_2d_d_type ( click_position_x ,
                                  click_position_y ) ;

            auto to_model = ui::get_to_model_tf ( & ui::job ) ;
            ui::zoom_focus = to_model ( ui::zoom_locus ) ;
          }
        }
        else if ( fabs ( displace_x ) > fabs ( displace_y ) )
        {
          last_mouse_activity = cycle_start ;

          // initially, this gesture changed the autopanning speed, but
          // this sometimes happened inadvertently and if it set the speed
          // very low, pan mode appeared to be broken. I changed the action
          // for this gesture to change brightness, which is immediately
          // obvious and nice to have via a mouse gesture.

          change_brightness ( state , change , ui::snappiness  * cd_dx * .01 ) ;
        }
      }

      // ui::autopan normally produces a yaw, a horizontal movement relative to the
      // anchor position currently held in 'bias'. This is to avoid confusion:
      // if ui::autopan would pan relative to the current view, we'd lose the image's
      // vertical easily, which is not what one would expect from an automatic pan.
      // if ui::lock_vertical is set, we produce a pan relative to the current view.
      // For 'mosaic' images, ui::lock_vertical is set by default, because here
      // this mode of movement is what would be conventionally expected when
      // viewing a 'flat' image.

      if ( ui::autopan )
      {
        auto by = ui::autopan ;
        if ( ui::autopan_ramp < .99 )
        {
          ui::autopan_ramp += .1 ;
          by *= ui::autopan_ramp ;
        }
        if ( ui::lock_vertical )
        {
          yaw ( state , change , state.speed * ui::hfov * .02 * by ) ;
        }
        else
        {
          pan ( state , change , state.speed * ui::hfov * .02 * by ) ;
        }
      }

      if ( ui::zoom_buffer )
      {
        zoom ( state , change , ui::zoom_decay , ui::zoom_min ) ;
      }

      if ( change.draw )
      {
        // if there really was user interaction, switch to immediate mode.
        // This is to 'wake up' the viewer if it's in SINGLE mode.

        ui::mode = IMMEDIATE ;
      }

      // p_window->setActive ( true ) ;

      // pick up old state of change.draw - this should be maintained
      // until it has resulted in a job pushed to the queue, and may
      // also have been set when lux has switched to single mode for
      // an hq frame. The flag will be cleared once it was honoured.

      change.draw |= change_draw_backup ;

      // now all acute and chronic user input has been seen. Before we do
      // anything else, we check if we have an in-sequence job in the
      // pipeline, and if so, display it. If no frames are immediately
      // available, presenter.poll() will wait for up to ten msec for
      // a frame to arrive. This slows things down more intelligently
      // than simply micro-sleeping for 1msec in every iteration if
      // nothing happens, as was done before - and it also insures that
      // a frame becoming available during the wait will be processed
      // immediately, rather than at the begining of the next loop
      // iteration.

      job_type * p_result = presenter.poll() ;

      if ( p_result == nullptr )
      {
        // no luck.
        frames_in_sequence = 0 ;
      }
      else
      {
        // std::cout << "**** poll yields a frame" << std::endl ;
        frames_in_sequence++ ;
      }

      if ( ui::fade_factor < 1.0 )
      {
        ui::fade_factor += ui::crossfade_delta ;
        need_display = true ;
      }

      if ( need_display || ui::show_imgui )
      {
        // clear the need_display flag. It will only be set to true again if
        // we get a new image frame or the GUI's graphical representation changes

        need_display = false ;

        // unless displaying the frame is disabled (which can be used for
        // benchmarking) we now display the data. The timing 'sorts itself
        // out': the call to show() will not return immediately, but only
        // once the GPU has found it feasible to accept the data into it's
        // own double- or triple buffering, so we'll be blocked here until
        // the GPU returns control.
        // Note that presenter.show() will also redraw the GUI bar if that's
        // necessary, and the redrawing is coupled with triggering any
        // actions bound to the GUI buttons (it's an immediate-mode GUI).
        // So any GUI-triggered activity will be manifest right after show
        // returns, which is why we test for mode == TERMINATE just below.

        // slight change in logic here: we now render ImGui stuff
        // whenever presenter.show() is called - otherwise there was
        // flicker when, at times, the image texture was displayed
        // without the GUI on to and then with it directly afterwards.

        if ( ! ui::suppress_display )
        {
          presenter.show ( p_window ) ;
          display_called = pv_clock::now() ;
        }

        // If user has clicked on EXIT we can bail out here:

        if ( ui::mode == TERMINATE )
        {
          break ;
        }

        // if we had a new frame coming in, there's lots of stuff to do now:

        if ( p_result )
        {
          if ( auto_budget )
          {
            // if auto_budget is set, we sample the time from one call to display() to the next.
            // If we're in an animated sequence (pan, zoom, etc., anything moving), these deltas
            // are cumulated and averaged, yielding the GPU frame time as dt_average.
            // The frame time budget is then set to 90% of this value, reckoning that if the
            // rendering time remains inside the allotted budget we'll get stutter-free animation.
            // KFJ 2018-12-1
            // Note how we check the current frame's display mode for 'SINGLE'. If the mode
            // is 'SINGLE', this was a single image created with the high-quality interpolator,
            // which is normally done during idle-time processing only, so when the display
            // is at rest. Such frames may nevertheless be produced repeatedly, so we exclude
            // them explicitly from our statistics.

            float dt = std::chrono::duration_cast<std::chrono::microseconds>
                        (display_called - display_last_called).count() ;
            dt /= 1000.0 ;
            display_last_called = display_called ;

            if (    frames_in_sequence > 1
                && p_result->frame.mode != SINGLE )
            {
              dt_count++ ;
              total_ms += dt ;
              if ( dt_count > 20 ) // only start using dt_average after having cumulated 20 deltas
              {
                float dt_average = total_ms / dt_count ;
                budget = dt_average * 0.8 ;
              }
            }
  //        std::cout << "dt count " << dt_count
  //                  << " budget " << budget << std::endl ;
          }

          // the job whose job-type object we're 'bending' for snapshots etc
          // may have been done with moving image scaling, but to use such
          // jobs as the base for snapshots etc., this has to be undone.
          // hence we reset the relevant member variables:
          // TODO: this may go now, the snapshots are done differently now

          p_result->frame.zoom_factor = state.zoom_factor ;
          auto sz = p_window->getSize() ;
          p_result->target.width = sz.x ;
          p_result->target.height = sz.y ;
          p_result->frame.light_settings.brighten = state.brightness ;

          // recycle RGBA frame

          rgb_mutex.lock() ;
          rgb_queue.push ( p_result->frame.p_frame ) ;
          rgb_mutex.unlock() ;

          // do some statistics

          float frame_ms = p_result->frame.cost / 1000.0 ;

//           std::cout << "frame generation time :   "
//           << frame_ms << " ms" << std::endl ;

          frame_time_cumulated += frame_ms ;

          // when in PAN or IMMEDIATE mode, we show an animated image sequence
          // and we have to guard against frames coming in too slow to prevent
          // stutter. With ui::auto_quality = true we try to adapt to the
          // average frame rendering time, and if that rises above the alloted
          // time budget, we use 'moving image scaling' to lower rendering time.
          // Note that when auto_budget is also true, we also adapt the budget
          // to be just under the time from successive calls to display().
          // This way we adapt to whatever FPS the system is producing.

          if (    ui::auto_quality == true
              && (    p_result->frame.mode == PAN
                   || p_result->frame.mode == IMMEDIATE ) )
          {
            // the moving average is quite inert:

            frame_time_moving_average
              = frame_time_moving_average * .99 + frame_ms * .01 ;

            // we lower moving image scaling as soon as the moving average exceeds
            // the budget, and lower it when it falls below a threshold. Note how
            // this 'corridor' aims to keep the frame time average below the budget
            // at all times.
            // We tolerate a certain bandwidth to prevent unnecessary activity,
            // because otherwise we may start the system 'pumping'. we also limit
            // changes to one *direction* and only allow changes in the opposite
            // direction if there was definite user activity. This way we don't
            // get pumping due to, e.g. pyramid level changes.

            double tolerance = .1 * frame_time_moving_average ;

            if (    frame_time_moving_average > budget
                 && change_direction >= 0 )
            {
              change_direction = 1 ; // lock direction of change to moving_image_scaling
              state.moving_image_scaling
                = sqrt (   state.moving_image_scaling
                         * state.moving_image_scaling
                         - .1 ) ;

              // set moving average to the center of the intended corridor. This way,
              // the moving average certainly takes some time to approach either end
              // of the range again, by which time any immediate effects from changing
              // the scaling have worn off - or, ideally, we remain near the center
              // of the corridor and don't exceed either boundary again.

              frame_time_moving_average = budget - tolerance / 2.0 ;
            }
            // we only switch back to a larger scaling factor if the frame time
            // moving average is below the budget by 'tolerance'. This is also to
            // prevent pumping.

            else if (    frame_time_moving_average + tolerance < budget
                      && change_direction <= 0 )
            {
              change_direction = -1 ; // lock direction of change to moving_image_scaling

              state.moving_image_scaling
                = sqrt (   state.moving_image_scaling
                         * state.moving_image_scaling
                         + .1 ) ;

              if ( state.moving_image_scaling > 1.0 )
                state.moving_image_scaling = 1.0 ;

              frame_time_moving_average = budget - tolerance / 2.0 ;
            }

            // sanity check. for automatic scaling, we don't
            // go lower than 10% AQ - lower values are only
            // available by manual setting via GUI or 'M'

            double scaling_floor = 1.0 / sqrt ( 10.0 ) ;
            if ( state.moving_image_scaling < scaling_floor )
              state.moving_image_scaling = scaling_floor ;
          }

          frames_showed ++ ;
          p_result->displayed = pv_clock::now() ;

          // finally, destroy the job_type object

          memlog >> p_result ;
          p_result = 0 ;
        }
      }

      // optionally, stop after a certain number of frames was displayed

      if ( stop_after > 0 && frames_showed >= stop_after )
      {
        ui::batch_flag = true ;

        std::cout << "reached frame count " << stop_after
                  << ", terminating." << std::endl ;
        break ;
      }

      // If there was a frame, we have dealt with it, it's been displayed.
      // now we react to user input, if any, and possibly generate new jobs.
      // first we check if anything at all was set in 'change' and do a test
      // to see if we're maybe idle and have idle-time processing to do.

      if ( change == no_change )
      {
        frames_in_sequence = 0 ;

        // let's see if we're maybe idle. The test is conservative
        // and will only switch to 'SINGLE' mode if there is
        // nothing going on at all - all ongoing user interaction,
        // background jobs, any jobs in any of the queues and
        // ongoing crossfades will be considered ongoing activity,
        // which has to conclude before idle-time processing will
        // commence.

        if (    ui::mode != SINGLE
             && ui::mode != TERMINATE
             && ui::fade_factor >= 1.0
             && ! ui::suppress_display )
        {
          bool activity =
                   ( jobs_busy() > 0 )
                || ( ui::zoom_buffer != 0.0 )
                || ( sf::Mouse::isButtonPressed ( sf::Mouse::Left ) )
                || ( sf::Mouse::isButtonPressed ( sf::Mouse::Right ) ) ;

          if ( ! activity )
          {   
            // check the queues. If they're empty, we're idle.
            {
              std::lock_guard<std::mutex> lk ( job_mutex ) ;

              if (    job_queue.size() == 0
                   && frame_queue.size() == 0 )
              {
                // switch to single mode.
                ui::mode = SINGLE ;

                if ( ui::snap_to_hq_interpolator == true )
                {
                  // setting change.draw in SINGLE mode will rerender the
                  // current frame with the best quality we can produce
                  change.draw = true ;
                }
              }
            }
          }
        }
      }
      else // if ( change == no_change )
      {
        last_time_active = pv_clock::now() ;

        // signal the change to the gui. If the gui is 'on', this will
        // make it redraw itself.

        gui.need_display = true ;
        gui.activity = true ;

        if ( active_change )
        {
          // all user interaction disarms the timer, so that slide show mode
          // is suspended until it is either actively started again or the
          // user tabs to the next image.

          // std::cout << "disarming timer" << std::endl ;

          ui::grace_period = -1 ;

          // if there was definite user interaction, we set change_direction to 0,
          // so when it is next looked at, it is 'neutral' and can be set to the
          // appropriate 'direction', where it remains until there is more user
          // interaction, which sets it back to the 'neutral' state.

          change_direction = 0 ;

          // stop any active crossfades

          ui::fade_factor = 1.0 ;
        }

        // there was some change.
        // we take note of change.draw. This may be set due to user input and
        // should produce a reaction, but if we can't push a job because of queue
        // overcrowding, we need to preserve this information until we get another
        // chance.

        bool carry_draw = change.draw ;

        if ( ui::mode == SINGLE )
        {
          // if, in single mode, anything apart from the draw flag is set in 'change',
          // we switch to immediate mode, because a change should result in a new
          // frame, and this should not be rendered in SINGLE mode ever, unless the
          // viewer is at rest.

          auto help = no_change ;
          help.draw = true ;
          if ( help != change )
            ui::mode = IMMEDIATE ;
        }

        // Now we try and push one job reflecting the change to the job
        // queue. We may not be able to push a job, because the job
        // queue is full, in which case we enter the next iteration of
        // the main loop. Then, if 'chronic' input has changed, the new
        // 'change' will be used, but changes to 'state' will be preserved.
        // Note that for 'chronic' input, the change remains stable as long
        // as the user's interaction remains the same - e.g. a 'chroinc'
        // key remains depressed or a click-and-drag remains at the same
        // displacement. Only 'acute' changes are singular and affect
        // 'state' directly.
        // Ff we're in SINGLE mode, we push the job even if the job queue
        // is already full - the change to single mode only occurs if mode
        // is not already SINGLE, so if we'd skip here because the queue is
        // full, the idle-time processing would fail.

        assert ( ui::mode != TERMINATE ) ;

        if ( job_queue.size() < job_threshold || ui::mode == SINGLE )
        {
          // we have a load of stuff to put into the 'frame'
          // component, so we access it via a reference:

          frame_type & frame ( ui::job.frame ) ;

          // first we process orientation changes which occur due to
          // pan, yaw, pitch etc. commands. state.dr keeps track of the
          // camera roll - the roll component in state.orientation
          // changes when moving the virtual camera around.

          state.dr += change.dr ;

          if ( projection == MOSAIC )
          {
            if ( ui::lock_vertical )
            {
              // with locked vertical, we directly change state with the
              // change signal, moving the view relative to the image's
              // native orientation

              state.dx += change.dx ;
              state.dy += change.dy ;
            }
            else
            {
              // we don't use dx, dy as change, but a vector of equal length
              // taking into account the current orientation (as given by
              // the roll value in state.dr). This produces 'movement' of
              // the view into the direction seen on-screen, rather than
              // movement relative the image's 'native' orientation. This is
              // the default for mosaic projection.

              point_2d_d_type p { change.dx , change.dy } ;

              double xx =   p[0] * cos ( -state.dr ) + p[1] * sin ( -state.dr ) ;
              double yy = - p[0] * sin ( -state.dr ) + p[1] * cos ( -state.dr ) ;

              state.dx += xx ;
              state.dy += yy ;
            }
          }
          else
          {
            // apply the changes to 'state'.
            // All changes are codified so that their application is
            // achieved by a simple addition - or a multiplication
            // (for the quaternions)

            quaternion_type help = change.preadjust * state.orientation ;
            state.orientation = help * change.orientation ;

            state.dx += change.dx ;
            state.dy += change.dy ;
          }

          state.zoom_factor += change.zoom_factor ;
          if ( ui::zoom_buffer )
          {
            ui::zoom_buffer -= ui::zoom_decay ;
            if ( fabs ( ui::zoom_buffer ) <= fabs ( ui::zoom_decay ) )
            {
              ui::zoom_buffer = ui::zoom_decay = 0.0 ;
            }
          }

          // sanity check for the zoom factor. Most ways of interacting with
          // the zoom factor use function zoom() which has a built-in sanity
          // check, but the user is free to enter a numerical value via the GUI.
          // Here we do a sanity check and clamp the value to zoom_min.

          if ( state.zoom_factor < ui::zoom_min )
            state.zoom_factor = ui::zoom_min ;

          state.speed += change.speed ;
          state.brightness *= ( 1.0 + change.brightness ) ;

          state.sensor_tilt_v += change.sensor_tilt_v ;
          state.sensor_tilt_h += change.sensor_tilt_h ;
          // sensor tilt is applied to sensor_settings
          ui::sensor_settings.sensor_tilt
            = get_rotation_q ( state.sensor_tilt_h ,
                               state.sensor_tilt_v ,
                               0.0 ) ;

          // change of black/white point

          if ( change.black_point != 0.0 )
          {
            state.black_point += change.black_point ;
          }

          if ( change.white_point != 0.0 )
          {
            state.white_point += change.white_point ;
          }

          state.moving_image_scaling += change.moving_image_scaling ;

          // sanity check:

          if ( state.moving_image_scaling <= .1 )
            state.moving_image_scaling = .1 ;

          // we have set up a persistent job_type object in namespace ui,
          // which we 'load' with everything a job needs.
          // When the set-up is done, we copy the object to a newly allocated
          // one and push the copy to the queue. The remaining object with all
          // the up-to-date information can be used by snapshot code to
          // derive it's own specific job_type objects, which made it
          // possible to take the snapshot code out of this loop, because
          // the snapshot code only needs a few modifications to the job_type
          // object which don't have to access variables in inner_main.
          // snapshots are okay with the last-updated state of ui::job,
          // because they don't modify the viewer's state at all.

          ui::job.created = pv_clock::now() ;
          ui::job.njobs = args.worker_threads ;
          ui::job.decimate_area = ui::decimate_area ;

          if ( change.rise == 0.0 )
            frame.rise = 0.0 ;
          else
          {
            double rise = apply_rise ( source , change.rise ) ;
            if ( rise )
            {
              frame.rise = rise ;
              state.rise += rise ;
            }
          }

          frame.format = FRAME_RGBA8 ;
          frame.solo = ui::solo ;
          frame.heal = ui::heal ;
          frame.mask_for = -1 ;
          frame.stack_only = 0 ;
          frame.use_rank = ui::use_rank ;
          frame.yield_argba = false ;
          frame.screen_needs_srgb = ! ui::p_screen->srgb_capable() ;

          // adapt to window size by obtaining it's size every time. This sets the
          // target size without applied global scaling, so if there is no global
          // scaling, the frame will be rendered to this size.

          auto sz = p_window->getSize() ;

          target.width = sz.x ;
          target.height = sz.y ;

          frame.p_itp = ui::p_itp ;

          if ( state.zoom_factor >= .5 )
          {
            // if the zoom is not 'too far' from 1, we allow using the first
            // stage of the interpolator, which provides bilinear interpolation
            // on the raw data. This stage is available immediately after the
            // image data have been read from disk.

            ui::job.frame.wait_for_stage = 1 ;
          }
          else
          {
            // if the zoom is lower (so, the view scales down), we require
            // image pyramids to be present, but if the interpolator has not yet
            // set up the HQ pyramid, we'll make do with the LQ pyramid. this
            // is stage 2.

            ui::job.frame.wait_for_stage = 2 ;
          }

          // TODO: this is just an approximation:
          target.hfov = ui::hfov_norm / state.zoom_factor ;

          // given the extent of the target window and the target
          // projection, we can calculate the target vfov:

          target.vfov = calculate_vfov ( target.hfov ,
                                         target.width ,
                                         target.height ,
                                         target.projection ) ;

          frame.zoom_factor = state.zoom_factor ;
          frame.level_bias = state.level_bias ;
          frame.mode = ui::mode ;
          frame.blending = ui::blending ;
          frame.tonemap = ui::tonemap ;
          frame.feathering = ui::feathering ;
          frame.light_settings.brighten = state.brightness ;
          frame.light_settings.hdr_spread = args.hdr_spread ;
          // if show_facet_dry is set, show solo images without applying
          // facet_brightness (facet_brightnes is 'un-applied')
          if ( frame.solo > -1 && ui::show_facet_dry )
            frame.light_settings.brighten /= ui::source_v[frame.solo].brightness ;
          frame.light_settings.white_balance = state.white_balance ;
          frame.light_settings.black_point = state.black_point ;
          frame.light_settings.white_point = state.white_point ;

          frame.sensor_settings = ui::sensor_settings ;
          blending_settings_type & bls ( frame.blending_settings ) ;
          bls.mode = SINGLE_IMAGE ; // may be overridden

          // if we're processing in linear RGB, we have to apply sRGB correction
          // to the frame to display it. If the frame buffer is srgb-capable
          // we can simply pass linear RGB and leave the final conversion
          // to srgb to the GPU, which saves time. If the frame buffer is
          // not srgb-capable, we have to do the conversion to srgb in the
          // last stage of rendering and pass the srgb data. If process_linear
          // is false anyway, the frame buffer is initially created without
          // srgb capability, meaning that it expects incoming data to be
          // in srgb already.

          frame.light_settings.apply_gradation
            = ui::process_linear && ! ui::p_screen->srgb_capable() ;

          frame.p_frame = 0 ;
          ui::job.target = target ;

          // TODO: member variables in the job_type object which are
          // initialized from CL args might be set earlier, which would
          // save time here, but the optimizer may figure that out anyway.
          // For now I don't disentangle the initializations - I've simply
          // copied the snapshot-relevant sections of the code to the
          // snapshot functions (_snapshot etc.) so there is some redundancy.

          int nfacets = ui::source_v.size() ;
          if (    ( nfacets && projection == FACET_MAP )
                || ui::faux_bracket )
          {
            bls.nfacets = nfacets ;
            bls.npartial = nfacets ;

            if ( ui::blending == BLEND_RANKED && args.snap_to_stitch )
              bls.mode = STITCH_IMAGES ;
            else if ( ui::blending == BLEND_HDR && args.snap_to_fusion )
              bls.mode = EXPOSURE_FUSION ;

            if ( ui::faux_bracket )
              bls.mode = FAUX_BRACKET ;

            bls.stacking = ui::stacking ;

            bls.exposure_weight = args.exposure_weight ;
            bls.contrast_weight = args.contrast_weight ;
            bls.exposure_sigma = args.exposure_sigma ;
            bls.exposure_mu = args.exposure_mu ;
            bls.scaling_step = args.exposure_scaling_step ;
            bls.pyramid_floor = args.exposure_pyramid_floor ;

            bls.i_spline_degree = args.bls_i_spline_degree ;
            bls.i_spline_decimator = args.bls_i_spline_decimator ;
            bls.i_spline_shift_to = args.bls_i_spline_shift_to ;

            bls.q_spline_degree = args.bls_q_spline_degree ;
            bls.q_spline_decimator = args.bls_q_spline_decimator ;
            bls.q_spline_shift_to = args.bls_q_spline_shift_to ;

            // if the user has passed faux_bracket_ev, we use these values,
            // if not, we calculate them from the current view's brightness
            // and the facet brightness factors

            if ( ui::faux_bracket && ( args.faux_bracket_ev.size() != 0 ) )
            {
              // derive number of partial images from faux_bracket_ev

              bls.npartial = args.faux_bracket_ev.size() ;
              bls.partial_brightness.clear() ;

              for ( int i = 0 ; i < bls.npartial ; i++ )
              {
                float pb = pow ( 2.0 , args.faux_bracket_ev [ i ] ) ;
                bls.partial_brightness.push_back ( pb ) ;
              }
            }
            else
            {
              // either it's not a faux bracket job, or the user hasn't
              // passed faux_bracket_ev

              assert ( nfacets ) ;
              bls.npartial = nfacets ;
              bls.partial_brightness.clear() ;

              for ( int i = 0 ; i < bls.npartial ; i++ )
              {
                float pb = state.brightness / ui::source_v[i].brightness ;
                bls.partial_brightness.push_back ( pb ) ;
              }
            }
            bls.process_linear = ui::process_linear ;
            bls.filename = std::string() ;
          }

          // if ui::capture_light is set; call ui::on_capture_light.
          // This triggers a show-again-sequence, so we break out
          // of the loop here before any jobs are created

          if ( ui::capture_light )
          {
            ui::capture_light = false ;
            ui::on_capture_light() ;
            break ;
          }

          // 'precipitate' the orientation in 'state' to ui::job

          if ( target.projection == MOSAIC )
          {
            // for MOSAIC target projection, we use the 'orientation'
            // quaternion, but only to 'ferry' dx, dy and dr to
            // the rendering code in it's first three components.
            // We simply overwrite frame.orientation - state.orientation
            // has a value but no useful meaning in this context.

            frame.orientation[0] = state.dx ;
            frame.orientation[1] = state.dy ;
            frame.orientation[2] = state.dr ;
            frame.orientation[3] = 0.0 ;
          }
          else
          {
            // for all other projections, we're working in 3D.

            frame.orientation = ui::bias ;
            frame.orientation *= state.preadjust ;
            frame.orientation *= state.orientation ;
          }


          // test for a focused zoom. The necessary alterations
          // can only be done now, with frame.orientation set,
          // because we use to_model, which needs the 'final'
          // orientation in ui::job to yield the correct
          // transformation

          if ( ui::zoom_locus != point_2d_d_type ( 0.0 , 0.0 ) )
          {
            // we have a 'focused zoom' where we want to keep the
            // ray stored in ui::zoom_focus 'pinned' to the screen
            // coordinates held in ui::zoom_locus. First we obtain
            // the transformation from target (window) coordinates
            // to 3D ray coordinates in model space. Note how the
            // transformation depends on the parametrization held
            // in ui::job:

            auto to_model = ui::get_to_model_tf() ;

            // find the 3D ray corresponding with the stored locus,
            // using the current orientation. The result will produce
            // a different focus than the one we intend to see,
            // because the zoom has already taken place and the
            // change is manifest in ui::job.

            auto off_focus = to_model ( ui::zoom_locus ) ;

            // calculate the difference between off_focus and the
            // intended value and encode it as a rotational quaternion,
            // or a shift in dx, dy for MOSAIC projection

            auto u = ui::zoom_focus - off_focus ;

            if ( target.projection == MOSAIC )
            {
              // this is easy - we're working on a plane

              auto dx = u[1] ; // note that to_model does not produce
              auto dy = u[2] ; // roman book order but 'conventional' 3D
              state.dx -= dx ;
              state.dy -= dy ;
              frame.orientation[0] = state.dx ;
              frame.orientation[1] = state.dy ;
            }
            else
            {
              double d = norm ( u ) ;

              // use the euclidian distance instead of the angle
              // via dot and acos for 'small' d, to avoid catastrophic
              // cancellation of dot() and because the values are
              // near identical for small u. for larger ones use
              // acos(dot(...)): for large values the dot is not
              // problematic, but the angle differs more from the
              // euclidean distance for larger d.
              // The threshold of .001 is heuristic and works even
              // for images with very small nominal fov. It could
              // be lowered further: If the engle is not entirely
              // precise, the error should be compensated for with
              // the next iteration. On the other hand, when the
              // dot product produces imprecise values due to
              // catastrophic cancellation, the view 'jumps around'
              // which is very annoying.

              double angle = d ;

              if ( fabs ( d ) > .001 )
              {
                double dt = dot ( ui::zoom_focus , off_focus ) ;
                angle = acos ( dt ) ;
              }

              // The magnitude of the angle is never very large;
              // zoom_focus and off_focus aren't far apart. Using
              // the cross product would be a problem if d == 2,
              // or, in other words, if zoom_focus == -off_focus,
              // but that would mean they are 180 degrees apart.
              // If this extreme situation arises, we bail out
              // and proceed without compensation. We also avoid
              // an attempt at compensation if the angle is very
              // small. Not compensating does 'postpone' the
              // compensation to the next iteration, when the
              // angle will have grown due to the focus having
              // moved further on.

              if (    angle > 0.0000001
                   && angle < ( 2.0 * M_PI - .0000001 ) )
              {
                // I assume that the cross product points the axis
                // so that creating the quaternion with it and the
                // angle takes care of the 'direction' of the angle

                auto axis = cross ( ui::zoom_focus , off_focus ) ;

                // compensate axis for bias and predajust: the axis
                // we get from the cross product above is in the
                // source image's coordinate system, but we want to
                // modify state.orientation, which is relative to
                // the view with bias and preadjust applied.
                // TODO: this doesn't work right after a few F10

                typedef vigra::Quaternion<double> qd_t ;

                qd_t to_view = conj ( ui::bias * state.preadjust ) ;
                axis = rotate_q ( axis , to_view ) ;

                // now we can calculate the compensation we need to
                // apply to state.orientation

                qd_t compensate = qd_t::createRotation ( -angle , axis ) ;

                // now we apply the correction of orientation to
                // move the virtual camera just so that zoom_focus
                // and off_focus coincide again.

                state.orientation = compensate * state.orientation ;

                if ( ui::lock_vertical )
                {
                  // TODO: normally okay, but if view center is close
                  // to a pole, the compensation results in a visible
                  // rotation of the view, and one might figure out an
                  // alternative compensation for such cases.

                  ui::on_readjust() ;
                }

                // now we recalculate frame.orientation, using the
                // compensated orientation in state.orientation

                frame.orientation = ui::bias ;
                frame.orientation *= state.preadjust ;
                frame.orientation *= state.orientation ;
              }
            }

            // if the focused zoom is over, reset zoom_locus and
            // zoom_focus

            if ( change.zoom_factor == 0.0 )
            {
              ui::zoom_locus = 0 ;
              ui::zoom_focus = 0 ;
            }
          } // end of fucused zoom processing

          // finally the job in ui::job is complete. We copy the
          // entire job_type object to freshly allocated memory
          // and add a few finishing touches which we don't want
          // in ui::job, but only in the copy which is sent to
          // the rendering thread

          job_type * p_send = mem_in() << new job_type ( ui::job ) ;

          if ( ui::mode == SINGLE )
          {
            // produce a frame for on-screen display in idle mode

            p_send->frame.hq = true ;
            p_send->frame.wait_for_stage = 3 ;
  
            // set 'aborted' to false. If the user leaves the event loop
            // prematurely, it will be set to true and cancel any jobs
            // which are rendering for on-screen display.

            aborted = false ;

            // in single mode, we don't use global scaling, since we're not
            // pressed for time: we needn't animate a quick succession of
            // different frames, but only one good quality frame. So we take
            // our time, rendering to the full size frame or even larger
            // (still_image_scaling)

            p_send->inflate ( ui::still_image_scaling ) ;
          }
          else // not a single mode job
          {
            // set 'aborted' to true. If a long-running background job
            // rendering for on-screen display is still running, we want
            // it to stop as soon as possible, to give all resources to
            // rendering an animated sequence. The result of the job
            // in the background would be discarded anyway, because
            // the frames which result from this current job will be
            // younger.

            aborted = true ;

            // permission to use PAN mode is an option in the ui; everything
            // else that determines whether the PAN mode route actually
            // will be taken is considered in pv_rendering.cc: it can
            // be inferred from the data in the jobs sent to the rendering
            // code (see struct geometry_type in pv_rendering.cc)

            p_send->may_pan = ui::allow_pan_mode ;
  
            // produce a frame for on-screen display during an animated
            // sequence

            p_send->frame.hq = false ;

            // as far as the target frame is concerned, we supply a method
            // for 'cheating': instead of calculating the full-sized target
            // frame, we may calculate a smaller version. When the frame
            // comes back to be displayed, we scale the result again,
            // now with the reciprocal factor. What do we gain?
            // Calculating a scaled-down frame is (potentially much)
            // faster, with the speed gain bought with deminished image
            // sharpness. So if the display stutters because the frames
            // aren't coming in quickly enough, we have a way of avoiding
            // the (very) annoying stutter by accepting a (slightly)
            // annoying lack of image quality. The rescaling at the
            // display end is done by the GPU (via SFML, which in turn
            // uses openGL), so it doesn't burden the CPU much. The
            // amount of 'global scaling' can either be fixed at startup
            // and then modified manually, or left to adapt atomatically,
            // which is the default.
            // note that this call to inflate sets the last parameter to
            // true (default is false), resulting in a frame size which is
            // a multiple of 16. These are the sizes which are fastest to
            // compute.

            p_send->inflate ( state.moving_image_scaling , 0.0 , true ) ;

            // apparently, human vision is less sensitive horizontally
            // than vertically, so applying proportionally more of the
            // global scaling to the vertical seems like a good idea,
            // fine-tuning the sacrifice of quality for speed.
            // we use ui::horizontal_squash, currently defaults to
            // 1/sqrt(2). Currently unused and possibly broken,
            // TODO: look into anisotropic scaling again
            // Another aspect to consider in this context is foveation.

            // p_send->inflate (   ui::horizontal_squash
            //                       / state.moving_image_scaling ,
            //                       1 / state.moving_image_scaling ) ;

            // force bls.mode to 'SINGLE_IMAGE' for this job; the 'mother'
            // job has the settings which pertain to an HQ job.

            p_send->frame.blending_settings.mode = SINGLE_IMAGE ;
          }

          // finally we enqueue the job

          {
            std::lock_guard < std::mutex > lk ( job_mutex ) ;
            job_queue.push ( p_send ) ;
          }

          {
            std::lock_guard < std::mutex > lk ( rendering_mutex ) ;
          }
          rendering_cv.notify_one() ;

          // we have managed to push a job to the job queue, so we clear
          // carry_draw, since we are now confident that user input has
          // been honoured.

          carry_draw = false ;
        }

        // now we reset 'change' to start with a clean slate for the next
        // round of gathering user input etc.
        // If we weren't able to push a job, we still start with 'change'
        // reset; If chronic input is no longer present now or has only
        // started now, so be it.

        change = no_change ;

        // if carry_draw is set to true here, this means that it was set
        // when a change was detected initially, but no job was issued
        // honouring the change, because of queue overcrowding.
        // In this case we carry change.draw to the next cycle, to
        // make sure that the user input will eventually be honoured.

        change.draw = carry_draw ;
      }

      // any 'next-after' requests are satisfied now:

      if ( ui::next_after_snapshot )
      {
        ui::next_after_snapshot = false ;
        ui::_snapshot() ;
      }
      else if ( ui::next_after_stitch )
      {
        ui::next_after_stitch = false ;
        ui::_stitch_images() ;
      }
      else if ( ui::next_after_fusion )
      {
        ui::next_after_fusion = false ;
        ui::_exposure_fusion() ;
      }

      // if this was a batch job, we end the cycle as soon as there are
      // no more background jobs and no more active popups.

      if (    ui::batch_flag
           && ( jobs_busy() == 0 )
           && ( ui::show_modal_dialog == false ) )
      {
        ui::mode = TERMINATE ;
        ui::relaunch = false ;
      }

      // if the user hasn't done anything for half a second,
      // switch the mouse cursor off, unless ImGui is active:
      // It's assumed that, while the ImGui is visible, the user will
      // want to interact with it and needs to see the mouse pointer
      // all the time to remain oriented. While a modal dialog is
      // displayed, the mouse cursor is switched on unconditionally.

      float since_activity = std::chrono::duration_cast<std::chrono::milliseconds>
           ( cycle_start - last_mouse_activity ).count() ;

      if ( since_activity == 0 )
      {
        // mouse events all set last_mouse_activity to cycle_start,
        // so if we detect since_activity == 0, we know the user
        // has actually triggered a mouse event, and we switch the
        // mouse cursor on. Most of the time we want it off, to not
        // clutter the view, but as soon as there is mouse activity,
        // but we switch it on so the user can see where the mouse is.
        // as the user has to move the mouse to trigger any GUI
        // activity, the mouse cursor is on and stays on (see below)
        // while there is any GUI activity, even if since_actvity
        // is longer than the 'mouse keep-visible interval'. Only
        // wen GUI activity stops (or there was none in the first
        // place, the 'else if' statement below results in the mouse
        // cursor being switched off.

        p_window->setMouseCursorVisible ( true ) ;
      }
      else if ( since_activity > 500 || since_activity < 0 )
      {
        // note: negative since_activity values occur if a new cycle
        // started and cycle_start was set to the then-current time.

        // is there ui activity in progress which needs to have the
        // mouse cursor visible?

        bool ui_in_use =    ui::show_modal_dialog
                         || ui::any_panel()
                         || ui::show_main_menu ;

        // if ui_in_use is true, we don't switch off the mouse
        // cursor, but if it's not on, we switch it off, because
        // we know that activity was 'long' ago, and we want the
        // mouse off after some time.

        if ( ! ui_in_use )
        {
          // no known ui activity, switch the mouse cursor off.
 
          p_window->setMouseCursorVisible ( false ) ;
        }
      }
    }  // ends body of main loop
  } // end of while ( ui::relaunch )

  if ( ui::file_select_pending )
  {
    // file-select triggered bu ImGui. This can't be handled
    // 'there and then' in full-screen mode because then the
    // tinyfiledialog window is not reliably visible, so the
    // window needs to be closed and reopened after the file
    // dialog. This can't happen during the execution of the
    // ImGui code bacuse that relies on the window remaining
    // the same until it's complete.
    ui::on_load_images() ;
    ui::file_select_pending = false ;
  }

  // wait for active rendering jobs to become ready, if there are any.
  // 'jobs_pending' pertains to all rendering jobs - regular 'immediate'
  // frame generation and snapshots etc. run in the background.

  int previous_pending = 0 ;
  ui::fade_factor = 1.0 ;

  while ( true )
  {
    // poll for finished frames. This is done especially for invocations
    // with next_after_snapshot and the likes, to display something to
    // keep the user amused while the snapshot is rendered.

    job_type * p_job = presenter.poll() ;
    if ( p_job != nullptr )
      cleanup ( p_job ) ;

    int pending = jobs_busy() ;

    // if there are pending jobs, we set 'aborted', which stops blending
    // jobs in the background which are rendering for on-screen display.
    // The result of such jobs would be discarded anyway, so it's better
    // to stop them quickly.

    if ( pending )
      aborted = true ;

    if ( pending != previous_pending )
    {
      previous_pending = pending ;

      std::cout << "waiting for " << pending
                << " background jobs" << std::endl ;

      // update the status line if necessary - if the status line changes,
      // gui.need_display will be set and we update the window.

      gui.draw_status_line ( p_window ) ;

      if ( gui.need_display )
      {
        presenter.show ( p_window ) ;
      }
    }

    // if there aren't any (more) jobs pending, break the loop

    if ( pending == 0 )
    {
      aborted = false ;
      break ;
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }

  // we're closing down now, there's some tidying up to do.

  // first, we remove any jobs which haven't been started yet from
  // the job queue. They are simply thrown away.

  std::cout << "emptying job queue" << std::endl ;

  job_mutex.lock() ;

  while ( job_queue.size() )
  {
    auto p_job = job_queue.front() ;
    job_queue.pop() ;
    cleanup ( p_job ) ;
  }

  job_mutex.unlock() ;

  // any frames for on-screen display are also discarded.

  std::cout << "emptying frame queue" << std::endl ;

  frame_mutex.lock() ;

  while ( frame_queue.size() )
  {
    auto p_job = frame_queue.front() ;
    frame_queue.pop() ;
    cleanup ( p_job ) ;
  }

  frame_mutex.unlock() ;

  std::cout << "displayed " << frames_showed << " frames, which took "
            << frame_time_cumulated / frames_showed
            << "ms on average to create" << std::endl ;

  ui::on_reset() ;

  if ( ui::show_again )
  {
    ui::save_hfov = source.hfov ;
    ui::save_projection = ui::projection ;

    // facet maps use vectors to store per-facet information.
    // if show_again is set, the rule is that arguments persist
    // but may be subject to overrides. 'listen' switches these
    // arguments to a receptive state: if some override argument
    // pertaining to a vector is found, the vector is cleared and
    // behaves as if it had been newly created. If no override
    // argument occurs, the old content is preserved.

    if ( projection == FACET_MAP )
    {
      args.listen() ;
    }
  }

  virgin = false ;

  // we stash the current image texture in 'previous_texture'
  // and set ui::fade_factor to zero. If crossfading is active,
  // the less-than-one ui::fade_factor will indicate that there is
  // a 'lingering' previous texture which can be layered on top
  // of the current image texture to create the crossfade effect.

  if ( ui::crossfade )
  {
    previous_texture = image_texture ;
    ui::fade_factor = 0.0 ;
  }

   return 0 ;
}

// process a file into it's internal representation. If the file
// is a 'proper' image file like JPG, TIFF etc., return true, else
// return false.

bool file_exists ( std::string & filename )
{
  // let's try and open the file to see if it's there at all
  // and we have permission to open it.

  std::ifstream ifs ( filename ) ;

  if ( ! ifs.good() )
  {
    // no good file

    std::string error ( "failed to access input file: " ) ;
    error += filename ;
    lux_error ( error ) ;
    return false ;
  }
  return true ;
}

bool to_ir ( const std::string & filename )
{
  if ( fileio::is_image ( filename ) )
  {
    // yes, it's an image file OIIO recognizes.
    // put it in args.image

    ini::args.image.push_back ( filename ) ;

    return true ; // it's a 'proper' image file
  }

  // if the argument is *not* an image file proper, it may be
  // a lux ini file or a PTO file.

  auto extension = filename.substr ( filename.size() - 3 , 3 ) ;

  // files with 'ini' extension are no longer accepted.

  // if (    extension == "ini" || extension == "INI"
  //      || extension == "lux" || extension == "LUX" )

  if ( extension == "lux" || extension == "LUX" )
  {
    // if overrides_active is not yet set, argument_backup must
    // be filled with the currently active arguments

    if ( ! ui::overrides_active )
      argument_backup = ini::args ;

    // read the lux file. If that fails, it's a non-recoverable
    // error.

    if ( ! ini::read_ini_file ( filename , true ) )
    {
      std::string error ( "reading lux file " ) ;
      error += filename ;
      error += " failed." ;
      abort_lux ( error ) ;
    }

    // we set overrides_active, so that any modifications to the
    // arguments are undone when the next cycle starts.

    ui::overrides_active = true ;

    // so this is not a 'proper' image file. but having read the
    // lux file successfully, we should now have at least one image
    // in args.image which the calling code will pick up.

    return false ;
  }

  // if the file is 'standing in' for an image file, it may be a PTO
  // file. PTO files are not permitted as the 'sole image'

  else if ( extension == "pto" || extension == "PTO" )
  {
    // if overrides_active is not yet set, argument_backup must
    // be filled with the currently active arguments

    if ( ! ui::overrides_active )
      argument_backup = ini::args ;

    // read the PTO file. If that fails, it's a non-recoverable
    // error.

    if ( ! ini::read_pto_file ( filename ) )
    {
      std::string error ( "reading PTO file " ) ;
      error += filename ;
      error += " failed." ;
      abort_lux ( error ) ;
    }

    // for PTO files, we unconditionally set a few arguments.

    ini::args.facet_lcs.push_back ( 1.0 ) ;
    ini::args.projection = "facet_map" ;
    ini::args.fully_covered = false ;
    ini::args.input_is_pto = true ;
    ini::args.process_linear = true ;

    // we set overrides_active, so that any modifications to the
    // arguments are undone when the next cycle starts.

    ui::overrides_active = true ;

    // this is not a 'proper' image file, but having read the PTO
    // file successfully, args.projection will now be facet_map
    // and the calling code will detect that.

    return false ;
  }

  // could not open the file, so it must be something
  // lux can't handle, because that would have been
  // dealt with previously.

  return false ;
}

// all ISA 'flavours' which are produced will have the code in
// a separate namespace, with a variable 'flavour' in that namespace
// which has the flavour-specific information, like the flavour's
// name, it's priority, viability test function and dispatcher.
// The set of flavours is defined in the cmake code, which sets
// the relevant FLV_... #defines. First we need declarations of
// the flavour variables:

#ifdef FLV_PLAIN
namespace PV_PLAIN
{
  extern flavour_type flavour ;
} ;
#endif

#ifdef FLV_SSSE3
namespace PV_SSSE3
{
  extern flavour_type flavour ;
} ;
#endif

#ifdef FLV_SSE42
namespace PV_SSE42
{
  extern flavour_type flavour ;
} ;
#endif

#ifdef FLV_AVX2
namespace PV_AVX2
{
  extern flavour_type flavour ;
} ;
#endif

#ifdef FLV_AVX
namespace PV_AVX
{
  extern flavour_type flavour ;
} ;
#endif

#ifdef FLV_AVX512f
namespace PV_AVX512f
{
  extern flavour_type flavour ;
} ;
#endif

// shut_down_rendering is used as an atexit handler, shutting down the
// rendering thread when lux terminates.

void shut_down_rendering()
{
   // set stay_alive false to signal the rendering thread to terminate

  job_mutex.lock() ;
  stay_alive = false ;
  job_mutex.unlock() ;

  // notify the rendering thread on rendering_cv - it's likely waiting
  // on that condition variable and can't react to stay_alive unless
  // it's wait is broken.

  {
    std::lock_guard < std::mutex > lk ( rendering_mutex ) ;
  }
  rendering_cv.notify_one() ;

  if ( p_rendering_thread != nullptr )
  {
    p_rendering_thread->join() ;
    delete p_rendering_thread ;
  }
}

int main ( int argc , const char * argv[] )
{
// here we push all defined flavours into flavour_set. This vector
// is subsequently sorted by priority to find the best viable
// flavour, and it can also be searched for a suitable flavour
// if the user overrides this choice.

#ifdef FLV_PLAIN
  flavour_set.push_back ( PV_PLAIN::flavour ) ;
#endif

#ifdef FLV_SSSE3
  flavour_set.push_back ( PV_SSSE3::flavour ) ;
#endif

#ifdef FLV_SSE42
  flavour_set.push_back ( PV_SSE42::flavour ) ;
#endif

#ifdef FLV_AVX
  flavour_set.push_back ( PV_AVX::flavour ) ;
#endif

#ifdef FLV_AVX2
  flavour_set.push_back ( PV_AVX2::flavour ) ;
#endif
  
#ifdef FLV_AVX512f
  flavour_set.push_back ( PV_AVX512f::flavour ) ;
#endif

  std::sort ( flavour_set.begin() , flavour_set.end() ,
              [] ( flavour_type a , flavour_type b )
                { return ( a.priority < b.priority ) ; }
            ) ;

  for ( auto & flavour : flavour_set )
  {
    if ( flavour.viability() )
    {
      std::cout << "flavour " << flavour.name << " is viable" << std::endl ;
      auto_dispatcher = flavour.p_dispatch ;
      break ;
    }
  }

  assert ( auto_dispatcher != nullptr ) ;

  // what type of full-screen graphics can we use?

  // I tried to use sf::VideoMode::getFullscreenModes()[0],
  // but it looks like on some macs I receive an empty vector
  // back, so relying on getting any fullscreen mode at all
  // is an error and the binary crashes.

  // desktop = sf::VideoMode::getFullscreenModes()[0] ;

  // This code does seem to work everywhere:
  
  desktop = sf::VideoMode::getDesktopMode() ; 

  std::cout << "first available fullscreen video mode: "
            << desktop.width << "X" << desktop.height
            << std::endl ;

  main_thread_id = std::this_thread::get_id() ;

  // we install a handler for SIGINT which triggers the same action as
  // pressing 'F1'. This can be used for 'tethering' pv to another program
  // to perform as it's display engine.

  std::signal ( SIGINT , sigint_signal_handler ) ;

  int pathlen ;

  for ( pathlen = strlen ( argv[0] ) - 1 ; pathlen >= 0 ; --pathlen )
  {
    char c = argv[0][pathlen] ;
    if ( c == '\\' || c == '/' )
      break ;
  }
  ++pathlen ;

  char path [ pathlen + 1 ] ;
  strncpy ( path , argv[0] , pathlen ) ;
  path [ pathlen ] = 0 ;
  ui::pwd = path ;

  std::cout << std::fixed << std::showpoint << std::setprecision(5)
            << "detected path: <" << path << ">" << std::endl ;

  // initialize the user interface

  std::cout << "initializing user interface" << std::endl ;
  ui::init() ;

  if ( argc > 1 && ( ! strcmp ( argv [ argc - 1 ] , "-" ) ) )
  {
    // if last argument is '-', we interpret the preceding arguments
    // as common arguments for all invocations and proceed with the
    // code for processing multiple images from a pipe.

    -- argc ;
    std::cout << "activating streaming mode" << std::endl ;
    ui::files_from_pipe = true ;
  }

  ini::synonym ( "hfov" , "horizontal_field_of_view" ) ;
  ini::synonym ( "hfov" , "horizontal_fov" ) ;
  ini::synonym ( "vfov" , "vertical_field_of_view" ) ;
  ini::synonym ( "vfov" , "vertical_fov" ) ;
  ini::synonym ( "projection" , "prj" ) ;

  ini::args = ini::state_type() ;
  std::cout << "processing command line arguments" << std::endl ;
  int success = ini::initialize ( argc , argv ) ;
  if ( ! success )
  {
    abort_lux ( "terminating due to argument error" ) ;
  }
  if ( ini::args.help )
  {
    ini::list_arguments ( std::cerr ) ;
    std::cerr
    << "for more documentation, please visit https://bitbucket.org/kfj/pv"
    << std::endl ;
    exit ( 0 ) ;
  }

  // move all image files from the command line to the file queue.
  // if the files come in as URIs, remove the prefix

  for ( auto const & file : ini::args.image )
  {
    if ( file.substr ( 0 , 8 ) == std::string ( "file:///" ) )
      ui::file_queue.push_back ( file.substr ( 7 ) ) ;
    else
      ui::file_queue.push_back ( file ) ;
  }
  ini::args.image.clear() ;

  // take note of the state of ini::args as it is now: we will use this
  // state to fall back to
  
  // ini::state_type arguments = ini::args ;
  // arguments = ini::args ;

  // if the user asks for the version on the CL, we echo it and terminate.

  if ( ini::args.version )
  {
    std::cout << pv_version << std::endl ;
    exit ( 0 ) ;
  }

  // If the user has passed a font file on the command line, that takes
  // preference, but if the font can't be found, we'll try and fall back
  // to the default options.
  
  std::string font_filename = ini::args.gui_font ;

  bool found_font = false ;

  if ( font_filename.size() )
  {
    if ( gui.set_font ( font_filename ) )
    {
      found_font = true ;
    }
    else
    {
      std::cerr << "can't open the font file " << font_filename << std::endl ;
      std::cerr << "passed on the command line." << std::endl ;
    }
  }

  // Next we'll first try to find the font in the present working directory.
  // This will work for invocations as stickware, like with the windows
  // bundle version.

  if ( ! found_font )
  {
    font_filename = ui::pwd + std::string ( "NotoSans-Regular.ttf" ) ;
    bool font_in_pwd = false ;

    std::ifstream test ( font_filename ) ;
    font_in_pwd = test.good() ;

    // if opening the font fails even if the file is there, we still don't
    // give up but try and glean the font's path from the environment

    if ( font_in_pwd )
    {
      if ( gui.set_font ( font_filename ) )
      {
        found_font = true ;
      }
      else
      {
        std::cerr << "can't open the font file " << font_filename << std::endl ;
        std::cerr << "in the base directory" << std::endl ;
      }
    }
  }
  
  if ( ! found_font )
  {
    // try and read the font's full path from an environment variable
    // LUX_GUI_FONT. If we can't find this variable or the font does
    // not load, we finally fail.

    const char * env_font_filename = getenv ( "LUX_GUI_FONT" ) ;

    if ( env_font_filename != nullptr )
    {
      font_filename = std::string ( env_font_filename ) ;
      if ( font_filename.size() )
      {
        if ( gui.set_font ( font_filename ) )
        {
          found_font = true ;
        }
        else
        {
          std::cerr << "can't open the font file " << font_filename << std::endl ;
          std::cerr << "gleaned form environment variable LUX_GUI_FONT" << std::endl ;
        }
      }
    }
  }
  
#if defined(PV_FONTDATADIR)

  if ( ! found_font )
  {
    // try and load the font from the hard-coded location CMake has provided
    // when building the binary

    auto font_filename =   std::string ( PV_FONTDATADIR )
                         + std::string ( "/NotoSans-Regular.ttf" ) ;

    if ( gui.set_font ( font_filename ) )
    {
      found_font = true ;
    }
    else
    {
      std::cerr << "can't open the font file " << font_filename << std::endl ;
      std::cerr << "hard-coded by CMake in the build process" << std::endl ;
    }
  }
  
#endif

  if ( ! found_font )
  {
    std::cerr << "can't find the GUI font." << std::endl ;
    std::cerr << "use '-w <font_filename> to pick a font" << std::endl ;
    std::cerr << "or set the LUX_GUI_FONT environment variable" << std::endl ;
    abort_lux ( "cannot find the GUI font. use -w <font_filename>" ) ;
  }

  image_texture.setSmooth ( true ) ;
  image_texture.setSrgb ( false ) ;	
  gui.reset() ;
  
  // override the ISA (instruction set architecture)

  std::string force_isa ;
  if ( ini::args.isa == "5" || ini::args.isa == "avx512f" )
  {
    std::cout << "forcing use of AVX512f ISA" << std::endl ;
    force_isa = "avx512f" ;
  }
  else if ( ini::args.isa == "2" || ini::args.isa == "avx2" )
  {
    std::cout << "forcing use of AVX2 ISA" << std::endl ;
    force_isa = "avx2" ;
  }
  else if ( ini::args.isa == "3" || ini::args.isa == "ssse3" )
  {
    std::cout << "forcing use of SSSE3 ISA" << std::endl ;
    force_isa = "ssse3" ;
  }
  else if ( ini::args.isa == "4" || ini::args.isa == "sse4.2" )
  {
    std::cout << "forcing use of SSE4.2 ISA" << std::endl ;
    force_isa = "sse4.2" ;
  }
  else if ( ini::args.isa == "a" || ini::args.isa == "avx" )
  {
    std::cout << "forcing use of AVX ISA" << std::endl ;
    force_isa = "avx" ;
  }
  else if (    ini::args.isa == "f"
            || ini::args.isa == "fallback"
            || ini::args.isa == "p"
            || ini::args.isa == "plain" )
  {
    std::cout << "forcing use of plain ISA" << std::endl ;
    force_isa = "plain" ;
  }
  else if ( ini::args.isa == "auto" || ini::args.isa == "" )
  {
    std::cout << "using auto-detect ISA" << std::endl ;
    dispatcher = auto_dispatcher ;
  }

  if ( force_isa != std::string() )
  {
    for ( auto & flavour : flavour_set )
    {
      if ( flavour.name == force_isa )
      {
        std::cout << "rendering code will be forced to use "
                  << flavour.name << " ISA" << std::endl ;
        dispatcher = flavour.p_dispatch ;
        break ;
      }
    }

    if ( dispatcher == nullptr )
    {
      std::cerr << "the requested ISA " << force_isa
                << " is not available in this binary" << std::endl ;
      exit ( -1 ) ;
    }
  }

  assert ( dispatcher != nullptr ) ;

  // now that the ISA is fixed, perform ISA-specific initializations

  dispatcher->init() ;

  auto payload = [&]() { dispatcher->generate_main() ; } ;

  // start the frame generating thread. We offload all rendering to that
  // thread, and only handle the UI and the display of finished frames
  // in this thread.

  // std::cout << "launching rendering thread" << std::endl ;

  stay_alive = true ;

  // std::thread generate_thread ( payload ) ;
  p_rendering_thread = new std::thread ( payload ) ;

  std::atexit ( shut_down_rendering ) ;

  // if user has specified gui scaling factor, take it.
  // else calculate it so that sizes in the GUI are relative
  // to fullHD values - so, e.g., if video mode is 3840X2160,
  // the scaling factor is two, and fixed sizes in the GUI
  // are given like '10*ui::fgui' which reads 'as large as
  // 10 pixels would look if the screen were full HD'
  // Note how going via the height is a defensive measure
  // against users having several screens: they will most
  // likely be next to each other rather than on top of
  // each other, so their desktop may be very wide but still
  // just as high as a single monitor.

  if ( ini::args.gui_scale_factor > 0 )
    ui::fgui = ini::args.gui_scale_factor ;
  else
  {
    // TODO: assumes aspect ratio is 16:9
    // calculate the desktop's aspect ratio

    double aspect_ratio = desktop.width ;
    aspect_ratio /= desktop.height ;

    // clamp that at 16:9 to avoid a huge GUI on
    // ultrawide screens - for smaller aspect ratios
    // (like 19:10, 3:2) we fill the width

    const double fhd_aspect_ratio = 16.0 / 9.0 ;
    if ( aspect_ratio > fhd_aspect_ratio )
      aspect_ratio = fhd_aspect_ratio ;

    ui::fgui = desktop.height / ( 1920.0 / aspect_ratio ) ;

    auto dfgui = ui::fgui - int ( std::round ( ui::fgui ) ) ;
    if ( fabs ( dfgui ) < 0.001 )
      ui::fgui = std::round ( ui::fgui ) ;
  }

  std::cout << "GUI scaling factor set to " << ui::fgui << std::endl ;
  
  while ( ui::show_more_images )
  {
    // are there any session-wide overrides? if so, modify the
    // values in ini::args accordingly. Any previous values in
    // ini::args are overwritten with the new sessionwide ones.

    if ( ui::sessionwide_args.size() )
    {
      int nargs = ui::sessionwide_args.size() ;
      const char * nargv [ nargs + 1 ] ;
      nargv[0] = "" ;

      for ( int i = 0 ; i < nargs ; i++ )
        nargv [ i + 1 ] = ui::sessionwide_args [ i ] . c_str() ;

      nargs++ ;
      for ( int i = 1 ; i < nargs ; i++ )
        std::cout << "sessionwide override: "
                  << nargv[i] << std::endl ;

      if ( ! ini::initialize ( nargs , nargv ) )
        abort_lux ( "error in command line argument processing" ) ;

      // the sessionwide overrides have been processed, so we can
      // clear the vector.

      ui::sessionwide_args.clear() ;
    }
    
    if ( ui::show_previous )
    {
      // If user wants to go back to the previous image, first we push
      // the current image (top of ui::used_files) back to the queue,
      // then the previous one. With the queue modified like this, we
      // can proceed with the normal next-file logic below. There must
      // be at least two files in used_files - ui::on_previous_image
      // checks, but to be on the safe side we assert this.

      assert ( ui::used_files.size() >= 2 ) ;

      ui::file_queue.push_front ( ui::used_files.back() ) ;
      ui::used_files.pop_back() ;
      ui::file_queue.push_front ( ui::used_files.back() ) ;
      ui::used_files.pop_back() ;
    }
    else if ( ui::show_again )
    {
      // show_again is similar, but only goes 'one step back'.
      // now the file queue has the current image at it's front
      // and the logic can proceed as usual

      if ( ui::used_files.size() >= 1 )
      {
        ui::file_queue.push_front ( ui::used_files.back() ) ;
        ui::used_files.pop_back() ;
      }
    }

    // now test whether we have at least one file in the file queue.
    // If not, we'll try and fill the queue - either until we have
    // a usable file in the queue or the user signals that lux should
    // terminate.

    if ( ui::file_queue.size() == 0 && ui::files_from_pipe == true )
    {
      // if the file queue is empty and streaming mode is on,
      // try to obtain a line from cin. This may or may not succeed:
      // if we can't get a non-empty string, the file queue will remain
      // empty and pv will terminate. Note that we'll only get here if
      // the file queue is in fact empty - any files in the queue will
      // be processed before the pipe is even considered.

      std::string str ;
      if ( std::cin.eof() )
      {
        std::cout << "pipe has reached EOF" << std::endl ;
      }
      else
      {
        if ( std::getline ( std::cin , str ) )
        {
          if ( ! str.empty() )
          {
            std::cout << "pipe yields '" << str << "'" << std::endl ;
            ui::file_queue.push_back ( str ) ;
          }
        }
      }
    }

    if ( ui::file_queue.size() == 0 )
    {
      // if the file queue is still empty, we optionally display
      // a file select dialog, unless the last cycle was a batch
      // job - in that case we terminate. If the user does not
      // select images, we also terminate.

      ui::show_more_images = false ;

      if ( ui::batch_flag == false )
      {
        if (    ui::p_screen != nullptr
             && ui::p_screen->p_window != nullptr )
        {
          if ( ui::run_fullscreen )
            ui::p_screen->p_window->setVisible ( false ) ;
        }

        auto selection = select_files ( ini::args.path ) ;

        if (    ui::p_screen != nullptr
             && ui::p_screen->p_window != nullptr )
        {
          if ( ui::run_fullscreen )
          {
            ui::p_screen->p_window->setVisible ( true ) ;
            ui::p_screen->p_window->requestFocus() ;
          }
        }

        if ( selection.size() )
        {
          std::cout << "user selected " << selection.size() << " files"
                    << std::endl ;

          ui::show_more_images = true ;
          for ( auto const & filename : selection )
          {
            ui::file_queue.push_back ( filename ) ;
          }
        }
      }
    }

    // reset ini::args to session-wide state

    // if ( ! ui::show_again )
    //   ini::args = arguments ;

    if ( ui::show_again || ui::override_args.size() )
    {
      // clear all images from ini::args; there may be images coming in
      // from the override args, but the current image will only come
      // back via the file queue

      ini::args.image.clear() ;

      gui.reset() ;

      // the user may have specified override arguments. These are extra
      // arguments which are processed after the persistent common args
      // These arguments are processed just once and discarded after use.
      // All command line arguments are allowed, including short arguments.
      // The set of strings is in fact processed like a command line, using
      // the same function ini::initialize().

      int nargs = ui::override_args.size() ;
      if ( nargs )
      {
        // save the current parameter set to argument_backup,
        // so that it can be restored after the cycle using the
        // overrides is over

        argument_backup = ini::args ;
        ui::overrides_active = true ;

        const char * nargv [ nargs + 1 ] ;
        nargv[0] = "" ;

        for ( int i = 0 ; i < nargs ; i++ )
          nargv [ i + 1 ] = ui::override_args [ i ] . c_str() ;

        nargs++ ;
        for ( int i = 1 ; i < nargs ; i++ )
          std::cout << "override: " << nargv[i] << std::endl ;

        if ( ! ini::initialize ( nargs , nargv ) )
         abort_lux ( "error in command line argument processing" ) ;

        // the overrides have been processed, clear the vector

        ui::override_args.clear() ;
      }

      // move all image files from the command line to the file queue.
      // This processes files specified with --image or as trailing args
      // and pushes them to the front of the file queue. This is done so
      // that the first image specified is displayed first. If no images
      // were found 'in' the override, we'll show the current image again,
      // because it is at the front of the file queue.

      if ( ini::args.image.size() )
      {
        // if the 'override' produced any files, we'll take the first
        // of them and display it instead of the current one. The current
        // one is pushed to used_files. Note that the current file is only
        // at the front of the file queue (and not in used_files) because
        // an override sets ui::show_again, and this has had the effect
        // to move the current file back from used_files to the file queue.

        ui::used_files.push_back ( ui::file_queue.front() ) ;
        ui::file_queue.pop_front() ;

        // now the 'new' files are pushed to the front of the file queue,
        // last one first, so that the first of the new files is next in line

        auto i = ini::args.image.begin() ;
        auto e = ini::args.image.end() ;

        while ( i != e )
        {
          --e ;
          ui::file_queue.push_front ( *e ) ;
        }

        // clean up the image vector in args

        ini::args.image.clear() ;
      }
    }

    if ( ui::file_queue.size() == 0 && ui::used_files.size() == 0 )
    {
      // all attempts at getting input have failed, both queues
      // are empty. we bail out and terminate lux.

      break ;
    }

    std::string candidate ;
    std::string filename ;

    // if the file queue isn't empty, we now take file names from
    // the queue until we have valid input, or until the queue is
    // empty.

    while ( ui::file_queue.size() )
    {
      // clear args.image and script_path

      ini::args.image.clear() ;
      ini::script_path = std::string() ;

      // pop the next entry off the file queue

      filename = candidate = ui::file_queue.front() ;
      ui::file_queue.pop_front() ;

      // we have the filename, but we still need to check
      // if the file exists. If it can't be found, locate_file
      // returns an empty string.

      filename = ini::locate_file ( filename ) ;

      if ( filename == std::string() )
      {
        // the file could not be located, we'll ignore it
        // and try our luck with the next file in the queue
        // if there are any left. If the queue is exhausted,
        // we'll drop out of this inner loop with empty filename
        // and the outer loop will deal with the situation.

        continue ;
      }
      else
      {
        // call to_ir tentatively: if it returns true, it's an
        // 'image proper', if not, it must be a lux ini file
        // standing in for an image or a PTO file. to_ir will
        // parse such files to their internal representation,
        // so that, e.g., images are put into args.image. If
        // the file is an image proper, it's now the sole entry
        // in args.image.

        bool is_image = to_ir ( filename ) ;

        if ( ! is_image )
        {
          // TODO: change the logic to avoid the goto

          noimage:

          // the file is not an 'image file proper', like JPG.
          // now we look for the content of args.image. This list
          // is used for 'individual' images, so here we only take
          // the first one if there are several. If we're dealing
          // with synoptic content (cubemaps or facet maps), the
          // participating images are in *not* in args.image, but
          // cubemaps or facet maps are allowed *as* individual
          // images: again, only the first one in args.image is
          // considered, the rest is ignored, and this first file
          // is consumed straight away.

          if ( ini::args.image.size() )
          {
            // we have files in args.image. keep only the first one:
            // input must be a lux file standing in for an image,
            // plus collateral arguments. PTO files don't populate
            // args.image, they only set args.facet.
            // TODO: this is the place where we could accept an image
            // set as well and submit it to e.g. registration.

            std::string first_image = ini::args.image[0] ;
            std::cout << "first entry in args.image: '"
                      << first_image
                      << "'" << std::endl ;

            ini::args.image.clear() ;
            first_image = ini::locate_file ( first_image ) ;

            if ( first_image == std::string() )
            {
              // the file does not exists.

              std::string error ( "error processing: " ) ;
              error += filename ;
              error += "\nstand-in script can't locate image" ;
              lux_error ( error ) ;

              continue ;
            }
            else
            {
              // the file exists. is it actually an image?
              // TODO: if first_image is another script (lux
              // or PTO) this should also be allowed, and that
              // file should be digested in turn.

              is_image = fileio::is_image ( first_image ) ;

              if ( is_image )
              {
                // yes. we'll work with this image. We put the
                // filename into the first slot of args.image

                filename = first_image ;
                ini::args.image.push_back ( filename ) ;
              }
              else
              {
                filename = first_image ;
                is_image = to_ir ( filename ) ;
                goto noimage ;
              }
            }
          }
        }

        // again we test is_image. If it's still not set, the previous
        // block of code has not come up with an image file, so now we
        // assume we are dealing with a cubemap or facet map. If it's
        // neither, this is a non-lethal error.

        if ( ! is_image )
        {
          if (    ini::args.projection != "cubemap"
               && ini::args.projection != "facet_map" )
          {
            // if we don't have a cubemap or facet map and args.image
            // is empty, this is a recoverable error. The error message
            // will only show if show_error_dialog is true (false by
            // default). The default behaviour is to simply ignore
            // files which are neither images nor 'lux special'.
            // PTO files or .lux files with missing images will
            // be treated as an error, though.

            std::string error ( "error processing: " ) ;
            error += filename ;
            error += "\nno image, cubemap or facet map found in this file" ;
            lux_error ( error ) ;

            filename = std::string() ;
            ini::args.image.clear() ;
            continue ;
          }
        }
      }

      // we're good. the input file was parsed successfully, parsing
      // failures would have caused program termination. break from
      // the loop, we now have a valid image or synopsis to work with,
      // or the file queue is exhausted - then filename is empty.
      // If filename isn't empty, we push the candidate to the used
      // file queue. Files which did not contain anything usable will
      // be effectively discarded, they won't be tried again.

      if ( filename != std::string() )
      {
        ui::used_files.push_back ( candidate ) ;
      }

      break ;

    } // while ( ui::file_queue.size() )

    // if 'filename' is empty at this point, they file queue did not
    // yield a usable file - either it was empty in the first place
    // or the filenames in the queue did not produce usable content.

    if ( filename == std::string() )
    {
      if (    ui::show_more_images
           && ui::used_files.size() >= 1
           && ui::batch_flag == false )
      {
        // we try and go back to the last used file, if there
        // is one in the used file queue - unless the previous
        // cycle was a batch job.

        ui::file_queue.push_front ( ui::used_files.back() ) ;
        ui::used_files.pop_back() ;
      }

      // now we continue with the next outer loop iteration.
      // If the queue is empty, it will try again to get input.

      continue ; // to: while ( ui::show_more_images )
    }

    int success = inner_main() ;

    if ( success != 0 )
      ui::show_again = false ;

    if ( ui::overrides_active )
    {
      // if the previous cycle was run with active override arguments,
      // we restore the sessionwide arguments which were active before
      // the overrides were applied

      ui::overrides_active = false ;
      ini::args = argument_backup ;
    }
  } // while ( ui::show_more_images )

  // destroy dynamically allocated objects. this is not technically
  // necessary, but after doing so, all memory allocations should
  // be balanced with deallocations.

  if ( ui::p_itp )
    dispatcher->destroy_itp ( ui::p_itp ) ;

  if ( ui::p_screen )
    memlog >> ui::p_screen ;

  rgb_mutex.lock() ;

  if ( rgb_queue.size() )
  {
    std::cout << "destroying " << rgb_queue.size()
              << " rgba frames from rgb_queue"
              << std:: endl ;

    while ( rgb_queue.size() )
    {
      auto p_frame = rgb_queue.front() ;
      rgb_queue.pop() ;
      memlog >> p_frame ;
    }
  }

  rgb_mutex.unlock() ;

  dispatcher->exit() ;

#ifdef USE_IMGUI

  ImGui::SFML::Shutdown() ;

#endif // #ifdef USE_IMGUI

#ifdef USE_MEMLOG

  std::cout << "memory log traced " << memlog.counter
            << " allocations" << std::endl ;

  if ( memlog_set.size() )
  {
    std::cout << "memory log detected undeleted memory:" << std::endl ;

    for ( const auto & entry : memlog_set )
      std::cout << entry.first
                << " allocated in " << entry.second.file
                << " line " << entry.second.line << std::endl ;
  }
  else
  {
    std::cout << "memory log balanced, no leaks detected" << std::endl ;
  }

#endif

  OIIO::geterror() ;
  OIIO::shutdown() ;

  std::cout << "exiting normally" << std::endl ;

  return EXIT_SUCCESS ;
}
