/************************************************************************/
/*                                                                      */
/*          pv - a viewer for panoramic images                          */
/*                                                                      */
/*          Copyright 2016 - 2023 by Kay F. Jahnke                      */
/*                                                                      */
/*  The git repository for this software is at                          */
/*                                                                      */
/*  https://bitbucket.org/kfj/pv                                        */
/*                                                                      */
/*  Please direct questions, bug reports, and contributions to          */
/*                                                                      */
/*  kfjahnke+pv@gmail.com                                               */
/*                                                                      */
/*  pv is free software: you can redistribute it and/or modify          */
/*  it under the terms of the GNU General Public License as published   */
/*  by the Free Software Foundation, either version 3 of the License,   */
/*  or (at your option) any later version.                              */
/*                                                                      */
/*  pv is distributed in the hope that it will be useful,               */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       */
/*  GNU General Public License for more details.                        */
/*                                                                      */
/*  You should have received a copy of the GNU General Public License   */
/*  along with pv.  If not, see <http://www.gnu.org/licenses/>.         */
/*                                                                      */
/************************************************************************/

// pv uses tinyfiledialogs (https://github.com/native-toolkit/tinyfiledialogs)
// to provide file selection on multiple platforms. tinyfiledialogs is slim
// and can just be 'dragged along' as source code, and it doesn't bloat the
// binary. Currently, tinyfiledialogs is used as-is, but I've written some
// code around it to provide file selection specifically for pv.
// Please refer to tinyfiledialogs.c or tinyfiledialogs.h for tinyfiledialog's 
// copyright and licensing information; this C++ wrapper is under GPLv3.
// This is a temporary measure - tinyfiledialogs has shortcomings, for example
// it doesn't handle very large numbers of files well (at least here) - but
// I have no access to an implementation of std::filesystem yet.

#ifdef USE_TINYFILEDIALOGS

// tinyfiledialogs is, as of yet, the only way we have in pv to select files

extern "C"
{
#include "tinyfiledialogs.c"
} ;

#endif

#include <vector>
#include <iostream>

// Occasionally, when starting lux, receiving a file selection fails
// consistently and lux terminates right away. This behaviour persists
// until lux is invoked with an image file argument on the command
// line. To figure out what's going wrong, I am trapping TFD's popen
// calls to see if maybe the popen returns useful information.

extern "C"
{
FILE * trap_popen(const char *command, const char *mode)
{
  std::cout << "TFD calls popen ( " << command << ", " << mode
            << " )" << std::endl ;
  FILE * result = popen ( command , mode ) ;
  if ( result == NULL )
  {
    std::cout << "popen returned NULL, errno = " << errno << std::endl ;
  }
  else
  {
    std::cout << "popen returns non-NULL pointer" << std::endl ;
  }
  return result ;
}
}

// add_file check the filename; if it's length 0 it does nothing.
// otherwise it creates a quoted string and pushes it to result

static void add_file ( std::vector < std::string > & result ,
                       const char * const filename )
{
  if ( ! *filename )
    return ;

  std::cout << "pushing filename " << filename << std::endl ;

  result.push_back ( std::string ( filename ) ) ;

//   std::string quote ( "\"" ) ;
// 
//   result.push_back (   quote
//                      + std::string ( filename )
//                      + quote ) ;
}

// select_files will simply not do anything if USE_TINYFILEDIALOGS is not
// present.

static std::string last_used_path ;
extern char tinyfd_response[1024];

std::vector < std::string > select_files ( std::string path )
{
  if ( path.empty() )
  {
    std::cout << "select_files called with empty path"
              << std::endl ;
    path = last_used_path ;
  }
  // memorize path
  last_used_path = path ;

  std::cout << "select_files tries to access path: "
            << path << std::endl ;

  std::vector < std::string > result ;

#ifdef USE_TINYFILEDIALOGS

  char const * FilterPatterns[]
    = { "*.jpg", "*.jpeg", "*.tif", "*.tiff" ,
        "*.JPG", "*.JPEG", "*.TIF", "*.TIFF",
        "*.png", "*.exr" , "*.pto" , "*.lux" ,
        "*.PNG", "*.EXR" , "*.PTO" , "*.LUX" } ;

  auto dummy
      = tinyfd_openFileDialog ( "tinyfd_query" ,
                                path.c_str() ,
                                16 ,
                                FilterPatterns ,
                                "image files" ,
                                1 ) ;

  std::cout << "tfd file dialog uses "
            << ( dummy ? "graphics" : "console" ) << "mode"
            << std::endl ;

  std::cout << "tfd file dialog helper program: "
            << tinyfd_response << std::endl ;

  try
  {
    // TODO: on linux, when viewer is in windowed mode, instead of just
    // showing the file dialog, I get a zenity message telling me
    // "select image file(s)" is ready when the viewer was launched by
    // doubleclicking on the cover image icon in Rhythmmbox or on an
    // image symbol in a Dolphin window

    // with OIIO, we can open all sorts of files, and it becomes
    // impractical to pass a list of patterns. applescript does not
    // offer to select 'All files' instead of the given patterns, we can
    // only omit the patterns in the first place to let the user select
    // any possible file. this will of course fail if the user picks
    // files which aren't supported by OIIO.
    // For now I switch to offering all files for selection on all
    // platforms - I have also changed the default behaviour to simply
    // ignoring non-image stand-alone files. 

    auto selection
      = tinyfd_openFileDialog ( "select image, .pto or .lux file(s)" ,
                                path.c_str() ,
#ifdef USE_OIIO
                                0 ,
                                NULL ,
#else
                                16 ,
                                FilterPatterns ,
#endif
                                "" ,
                                1 ) ;

    if ( selection )
    {
      std::cout << "received selection: " << selection << std::endl ;

      int length = strlen ( selection ) ;
      char buffer [ length + 1 ] ;

      const char * p_src = selection ;
      char * p_trg = buffer ;

      bool keep_on = true ;

      while ( keep_on )
      {
        switch ( *p_src )
        {
          case '|': // tinyfiledialog's file name separator
            ++p_src ;
            *p_trg = 0 ; // terminate string
            add_file ( result , buffer ) ;
            p_trg = buffer ;
            break ;
          case 0:
            *p_trg = 0 ;
            add_file ( result , buffer ) ;
            keep_on = false ;
            break ;
          default:
            *p_trg = *p_src ;
            ++p_trg ;
            ++p_src ;
            break ;
        }
      }
    }
    else
    {
      std::cout << "no file selection received" << std::endl ;
    }
  }
  catch ( ... )
  {
    std::cerr << "oops... file selection failed with an exception" << std::endl ;
  }

#endif // USE_TINYFILEDIALOGS

  if ( result.size() )
  {
    auto slashpos = result[0].rfind ( '/' ) ;
    if ( slashpos == std::string::npos )
      slashpos = result[0].rfind ( '\\' ) ;
    if ( slashpos != std::string::npos )
    {
      last_used_path = result[0].substr ( 0 , slashpos + 1 ) ;
    }
  }

  return result ;
}
